var j$ = jQuery.noConflict(),
	tankIDs = null;
		
function confirmSubmit(confirmationMessage) {
    var r = confirm(confirmationMessage);
    if (r == true) {
        initiateSaveAction();
    }
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function initiateTankDipEntryValidation(deadStockWarning,sflWarning) {
    var tankID;
    var isFieldVisible;
    tankIDs = ';';
    var blnBelowDeadstockLimit = false;
    var vDeadStock;
    var vDip;
    var intDeadStock;
    var intDip;
    var blnDisplayDeadStockValidation = true;
	var blnDisplaySFLValidation = true;
    var blnProceed = true;
    j$('.rowID').each(function() {
        tankID = j$(this).attr('id');
        isFieldVisible = j$('.rowSelector_' + tankID).is(':checked');
        if (isFieldVisible) {
            tankIDs += tankID + ';';
            intDeadStock = null;
            intDip = null;
            // Compare deadstock against entered tank dip
            vDeadStock = j$('#rowDeadstock_' + tankID).val();
            vDip = j$('.tankDipCapacity_' + tankID).val();
			var ele = document.getElementById('submitPage:frm:mainTablePanel:tankDipTable:'+tankID+':SFL').innerHTML;
			vSFL = ele;
			if (vDeadStock != null && vDip != null && vDeadStock != undefined && vDip != undefined && vDip != '') {
                // Remove decimal points (tank dips are meant to be integer)
                vDip = vDip.replace('.', '');
                vDip = vDip.replace(/,/g, '');
                intDeadStock = parseInt(vDeadStock);
				vSFL = vSFL.replace(/,/g, '');
                intDip = parseInt(vDip);
				intSFL = parseInt(vSFL);
                if ((intDeadStock > intDip) ) { 
					
                    
                    var response = openDeadStockMessage(deadStockWarning);
					blnProceed = false;
                    
                }
				if(intSFL < intDip){
					var response = openSFLMessage(sflWarning);
					blnProceed = false;
					
                    
					
				}
            }
        }
    }); // End each loop
	if (blnProceed)
        initiateNextAction(tankIDs);
    return false;
}
function resetAllRows() {
    j$('.rowID').each(function() {
        tankID = j$(this).attr('id');
        isFieldVisible = j$('.rowSelector_' + tankID).is(':checked');
        if (isFieldVisible) {
            j$('.tankDipCapacity_' + tankID).show();
            j$('.tankDipDate_' + tankID).show();
        }
    }); // End each loop
}
function isOneTankSelected() {
    var blnIsSelected = false;
    j$('.rowID').each(function() {
        tankID = j$(this).attr('id');
        isFieldVisible = j$('.rowSelector_' + tankID).is(':checked');
        if (isFieldVisible) {
            blnIsSelected = true;
        }
    }); // End each loop
    return blnIsSelected;
}
function toggleAllRows(show) {
    var tankID;
    j$('.rowID').each(function() {
        tankID = j$(this).attr('id');
        if (!show) {
            j$('.tankDipCapacity_' + tankID).hide();
            j$('.tankDipDate_' + tankID).hide();                             
             j$('.readingtime_' + tankID).hide();  
            j$('.rowSelector_' + tankID).prop('checked', false);
        } else {
            j$('.tankDipCapacity_' + tankID).show();
             j$('.readingtime_' + tankID).show();  
            j$('.tankDipDate_' + tankID).show();
            j$('.rowSelector_' + tankID).prop('checked', true);
        }
    }); // End each loop
    if (show) {
        j$('.nextButton').show();
    } else {
        j$('.nextButton').hide();
    }
}

function checkToggleRow(tankID) {
	var isFieldVisible = j$('.rowSelector_' + tankID).is(':checked');
	
    if (!isFieldVisible) {
		console.log('hi'+isFieldVisible);
        j$('.tankDipCapacity_' + tankID).hide();
        j$('.tankDipDate_' + tankID).hide();
          j$('.readingtime_' + tankID).hide();       
        if (isOneTankSelected()) {
            j$('.nextButton').show();
        } else {
            j$('.nextButton').hide();
        }
    } else {
		console.log('hi'+isFieldVisible);
      j$('.readingtime_' + tankID).show();       
        j$('.tankDipCapacity_' + tankID).show();
        j$('.tankDipDate_' + tankID).show();
        j$('.nextButton').show();
    }
    return false;
}

function openDeadStockMessage(deadStockWarning){   
	var sd = new SimpleDialog('hersh' + Math.random(), true);    
   sd.setTitle(deadStockLabel);   
   sd.createDialog();   
   window.parent.sd = sd;   
   sd.setWidth(350);
   sd.setupDefaultButtons();
   sd.setContentInnerHTML("<p align='center'><img src='/img/msg_icons/warning32.png' style='margin:0 5px;'/></p><p align='center'>"+deadStockWarning+"</p><p align='center'><br /><button class='btn' onclick='hideAndNext(sd);return true;'>"+okLabel+"</button>&nbsp;<button class='btn' onclick='window.parent.sd.hide(); return false;'>"+cancelLabel+"</button></p>");    
   sd.show();   
 } 
  
 function openSFLMessage(sflWarning){   
 
	var sd = new SimpleDialog('hersh' + Math.random(), true);    
   sd.setTitle(sflWarningLabel);   
   sd.createDialog();   
   window.parent.sd = sd;   
   sd.setWidth(350);
   sd.setupDefaultButtons();
   sd.setContentInnerHTML("<p align='center'><img src='/img/msg_icons/warning32.png' style='margin:0 5px;'/></p><p align='center'>"+sflWarning+"</p><p align='center'><br /><button class='btn' onclick='hideAndNext(sd);return true;'>"+okLabel+"</button>&nbsp;<button class='btn' onclick='window.parent.sd.hide(); return false;'>"+cancelLabel+"</button></p>");    
   sd.show();   
 } 

function hideAndNext(sd){   
   sd.hide();
   initiateNextAction(tankIDs);
   return true;
 }  