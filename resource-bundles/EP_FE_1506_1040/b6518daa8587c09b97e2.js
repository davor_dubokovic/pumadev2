/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	var parentJsonpFunction = window["webpackJsonp"];
/******/ 	window["webpackJsonp"] = function webpackJsonpCallback(chunkIds, moreModules, executeModule) {
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId])
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			modules[moduleId] = moreModules[moduleId];
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(chunkIds, moreModules);
/******/ 		while(resolves.length)
/******/ 			resolves.shift()();
/******/ 		if(executeModule + 1) { // typeof executeModule === "number"
/******/ 			return __webpack_require__(executeModule);
/******/ 		}
/******/ 	};

/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// objects to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		0: 0
/******/ 	};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}

/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		if(installedChunks[chunkId] === 0)
/******/ 			return Promise.resolve()

/******/ 		// an Promise means "currently loading".
/******/ 		if(installedChunks[chunkId]) {
/******/ 			return installedChunks[chunkId][2];
/******/ 		}
/******/ 		// start chunk loading
/******/ 		var head = document.getElementsByTagName('head')[0];
/******/ 		var script = document.createElement('script');
/******/ 		script.type = 'text/javascript';
/******/ 		script.charset = 'utf-8';
/******/ 		script.async = true;
/******/ 		script.timeout = 120000;

/******/ 		script.src = __webpack_require__.p + "" + chunkId + ".chunk.js";
/******/ 		var timeout = setTimeout(onScriptComplete, 120000);
/******/ 		script.onerror = script.onload = onScriptComplete;
/******/ 		function onScriptComplete() {
/******/ 			// avoid mem leaks in IE.
/******/ 			script.onerror = script.onload = null;
/******/ 			clearTimeout(timeout);
/******/ 			var chunk = installedChunks[chunkId];
/******/ 			if(chunk !== 0) {
/******/ 				if(chunk) chunk[1](new Error('Loading chunk ' + chunkId + ' failed.'));
/******/ 				installedChunks[chunkId] = undefined;
/******/ 			}
/******/ 		};
/******/ 		head.appendChild(script);

/******/ 		var promise = new Promise(function(resolve, reject) {
/******/ 			installedChunks[chunkId] = [resolve, reject];
/******/ 		});
/******/ 		return installedChunks[chunkId][2] = promise;
/******/ 	};

/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/resource/884/EP_FE_1506_1040/";

/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 805);
/******/ })
/************************************************************************/
/******/ ({

/***/ 206:
/***/ function(module, exports) {

	eval("//polyfill for IE\n(function () {\n\tif (typeof window.CustomEvent === 'function') return false;\n\n\tfunction CustomEvent(event, params) {\n\t\tparams = params || {\n\t\t\tbubbles: false,\n\t\t\tcancelable: false,\n\t\t\tdetail: undefined\n\t\t};\n\t\tvar evt = document.createEvent('CustomEvent');\n\t\tevt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);\n\t\treturn evt;\n\t}\n\n\tCustomEvent.prototype = window.Event.prototype;\n\n\twindow.CustomEvent = CustomEvent;\n})();\n\nfunction reqListener() {\n\twindow.sessionStorage.setItem('bootstrap', this.responseText);\n\n\tvar event = new CustomEvent('bootstrap', {\n\t\tbubbles: true,\n\t\tcancelable: false,\n\t\tdetail: this.responseText\n\t});\n\n\twindow.dispatchEvent(event);\n}\n\n//regex to get a cookie, source https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie\n\n/*var btReq = new XMLHttpRequest();\nbtReq.addEventListener('load', reqListener)\n\n//no sid and yet a sfdcSessionId => we are on the puma login page\nif(document.cookie.indexOf('sid=') === -1 && window.__sfdcSessionId !== '{!GETSESSIONID()}') {\n\tbtReq.open('GET', '/services/apexrest/FE/V1/Dictionary')\n} else {\n\t//localhost or on in the portal\n\tvar sid_cookie = window.__sfdcSessionId !== '{!GETSESSIONID()}' && window.__sfdcSessionId || JSON.parse(window.localStorage.getItem('sfForcetkClient')).sessionId\n\tbtReq.open('GET', '/services/apexrest/FE/V1/Bootstrap')\n\tbtReq.setRequestHeader('Authorization', 'OAuth ' + sid_cookie)\n}\nbtReq.send()*/\n\n/*****************\n ** WEBPACK FOOTER\n ** ./src/bootstrap.js\n ** module id = 206\n ** module chunks = 0\n **/\n\n//# sourceURL=webpack:///./src/bootstrap.js?");

/***/ },

/***/ 805:
/***/ function(module, exports, __webpack_require__) {

	eval("module.exports = __webpack_require__(206);\n\n\n/*****************\n ** WEBPACK FOOTER\n ** multi bootstrap\n ** module id = 805\n ** module chunks = 0\n **/\n\n//# sourceURL=webpack:///multi_bootstrap?");

/***/ }

/******/ });