<apex:component allowDML="true" controller="EP_ProductSearchComponentController">
    <apex:attribute name="productListID" description="This is the ID of the product list of the customer" type="String" required="true"/>
    <apex:attribute name="productFamily" description="This is the product family. If left blank the controller will search across all products" type="String" required="false"/>
    <apex:attribute name="customerCurrencyCode" description="This is currency code of the customer" type="String" required="true"/>
    <apex:attribute name="searchResultLimit" default="10" description="This attribute controls the max number of results returned by the search functionality. Default value is 10" type="Integer" required="false"/>
    <apex:attribute name="selectedProductID" description="This attribute returns the selected product ID" type="String" required="true"/>
    
    
    <apex:stylesheet value="{!URLFOR($Resource.EP_jQuery, 'jquery-ui.css')}" />
    
    <apex:includeScript value="{!URLFOR($Resource.EP_jQuery, 'jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.EP_jQuery, 'jquery-ui.min.js')}"/>
    
    <style>
        .ui-autocomplete-loading { 
            background:url('/img/loading32.gif') no-repeat right center;
            background-size: 16px 16px;
        }
    </style>
    
    <script>
        
        if(typeof jQuery != 'undefined'){
            $j = jQuery.noConflict();
        }
        
        $j(document).ready(function(){
            initialiseAutocomplete();
        });
        
        function initialiseAutocomplete(){
            $j(function() {            
                $j('input.productListField').autocomplete({
                    minLength: 2,
                    search: function(event, ui) { 
                        $j('.spinner').show();
                    },
                    response: function(event, ui) {
                        $j('.spinner').hide();
                    },
                    source: function(request, response){
                    getProductListData(response, request.term);
                    },
                    select: function(event, ui){
                    $j('input.productListField').val(ui.item.value);
                    assignSelectedProductID(ui.item.id);
                    return false;
                    }            
                });            
            });
        }
        
        function getProductListData(response, param){
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.EP_ProductSearchComponentController.getProducts}',
                '{!productListID}', '{!customerCurrencyCode}', '{!productFamily}', param, {!searchResultLimit}, 
                function(result, event){
                    if (event.status) {                        
                        var objList = [];
                        for(var i = 0; i < result.length; i++){
                            var obj = new Object();
                            obj.label = result[i].Name + ' [' + result[i].EP_Unit_of_Measure__c + '] [' + result[i].ProductCode + ']';
                            obj.value = result[i].Name + ' [' + result[i].EP_Unit_of_Measure__c + '] [' + result[i].ProductCode + ']';
                            obj.id = result[i].Id;
                            objList.push(obj);
                        }
                        response(objList);
                    }else {
                        alert(event.message);
                    }
                }, 
                {escape: true}
            );
        }
    </script>
    
    <apex:actionFunction name="assignSelectedProductID" reRender="product-search-panel" oncomplete="initialiseAutocomplete();">
        <apex:param name="paremOne" value="" assignTo="{!selectedProductID}" />
    </apex:actionFunction>
    
    <apex:componentBody >
        <apex:outputPanel id="product-search-panel">
            <div class="filterOverview">
                <div class="bFilterView">
                    <table border="0">                    
                        <tr>        
                            <td>
                                <div class="ui-widget">
                                    <apex:inputText id="product-list-text-field" 
                                            styleClass="productListField" value="{!selectedProductNameValue}" />      
                                            <!--                          
                                    <apex:inputText id="product-list-id-text-field" 
                                        styleClass="productIdField" 
                                            style="visibility:hidden" value="{!selectedProductIDValue}" /> 
                                            -->
                                </div>
                            </td>                
                        </tr>                
                                                    
                    </table>
                </div>
            </div>
        </apex:outputPanel>
        
    </apex:componentBody>
    
</apex:component>