/* @Author CloudSense
   @name <EPRouteAllocationTrigger>
   @CreateDate <23/11/2017>
   @Description <Trigger to update count of routeAllocation on shipto account for On/OffRun Order Configuration >
   @Version <1.0>
*/

trigger EPRouteAllocationTrigger on EP_Route_Allocation__c (after insert, after delete) {
     if(Trigger.isInsert && Trigger.isAfter){
        EP_RouteAcclocationTriggerHandler.doAfterInsertDelete( Trigger.new,true);
        
      }
      if(Trigger.isDelete && Trigger.isAfter){
        EP_RouteAcclocationTriggerHandler.doAfterInsertDelete( Trigger.old,false);
     }

}