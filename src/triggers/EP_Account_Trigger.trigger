/*
*  @Author <Accenture>
*  @Name <EP_Account_Trigger>
*  @CreateDate <09/02/2017>
*  @Description <Trigger for Account Object>
*  @Version <1.0>
*/
trigger EP_Account_Trigger on Account (before insert,before update, after insert, after update, before delete) {
    if(!EP_CheckRecursive.isBatchRunning && Trigger_Settings__c.getInstance('EP_Account_Trigger')!= null && Trigger_Settings__c.getInstance('EP_Account_Trigger').IsActive__c){
    system.debug('##InTrigger-EP_IntegrationUtil.isCallout-## ' + EP_IntegrationUtil.isCallout);
    system.debug('##trigger.new--> ' + trigger.new);
        if(trigger.isBefore){
            if(trigger.isInsert){
                for(Account account : trigger.new){
                    if(account.EP_Application_Record_Type_Name__c == Label.EP_Record_Type_Name){
                    EP_AccountDomainObject accDomainObj = new EP_AccountDomainObject(account);
                    accDomainObj.doActionBeforeInsert();
                }
            }
            }
            if(EP_CheckRecursive.runBeforeOnce()){
                if(trigger.isUpdate){
                    //Bulkification Implementation -- Start
                    EP_AccountMapper accMapper = new EP_AccountMapper(trigger.new);
                    EP_ActionMapper actMapper = new EP_ActionMapper(trigger.newMap.keyset());
                    EP_StockHoldingLocationMapper stockHoldingMapper = new EP_StockHoldingLocationMapper(trigger.newMap.keyset());
                    List<EP_State_Transitions__c> listStateTransitions = EP_State_Transitions__c.getall().values();                    
                    for(Account account : trigger.newMap.values()){
                        if(account.EP_Application_Record_Type_Name__c == Label.EP_Record_Type_Name){
                        system.debug('##ID--> ' + account.Id);
                        system.debug('##TYPE--> ' + account.EP_Account_Type__c);
                        system.debug('##Status--> ' + account.EP_Status__c);
                        system.debug('##SYNCStatus--> ' + account.EP_Synced_With_All__c);
                        EP_AccountDomainObject accDomainObj = new EP_AccountDomainObject(account,trigger.oldMap.get(account.Id));
                        accDomainObj.accountMapper = accMapper;
                        accDomainObj.listStateTrans = listStateTransitions;
                        accDomainObj.actionMapper = actMapper;
                        accDomainObj.stockHoldLocMapper = stockHoldingMapper;
                        accDomainObj.doActionBeforeUpdate();
                    }
                    }
                    //Bulkification Implementation -- End
                }
                system.debug('##BeforeUpdate-EP_IntegrationUtil.isCallout-## ' + EP_IntegrationUtil.isCallout);
            }
            //# L4- 45526 Start  
            if(trigger.isDelete){
                for(Account account : trigger.old){
                    if(account.EP_Application_Record_Type_Name__c == Label.EP_Record_Type_Name){
                    EP_AccountDomainObject accDomainObj = new EP_AccountDomainObject(account);
                    accDomainObj.doActionBeforeDelete();
                }
            }
            }
            //# L4- 45526 End   
        }
        if(trigger.isAfter){
        system.debug('##AfterUpdate-EP_IntegrationUtil.isCallout-## ' + EP_IntegrationUtil.isCallout);
            if(EP_CheckRecursive.runAfterOnce()){
                if(trigger.isUpdate){
                    for(Account account : trigger.newMap.values()){
                        if(account.EP_Application_Record_Type_Name__c == Label.EP_Record_Type_Name){
                        EP_AccountDomainObject accDomainObj = new EP_AccountDomainObject(account,trigger.oldMap.get(account.Id));
                        accDomainObj.doActionAfterUpdate();
                    }
                }                
            }
            }
            if(trigger.isInsert){
                for(Account account : trigger.new){
                    if(account.EP_Application_Record_Type_Name__c == Label.EP_Record_Type_Name){
                    EP_AccountDomainObject accDomainObj = new EP_AccountDomainObject(account);
                    accDomainObj.doActionAfterInsert();
                    }
                }                
            }
        } 
    }
}