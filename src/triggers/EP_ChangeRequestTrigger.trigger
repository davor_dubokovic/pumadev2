/**
 @Author <Vikas Malik>
 @Name <EP_ChangeRequestTrigger>
 @CreateDate <05/01/2016>
 @Description <Trigger for Change Request object. The purpose of this trigger is to commit request change to sObject when request gets completed>
 @Version <1.0>
*/
trigger EP_ChangeRequestTrigger on EP_ChangeRequest__c (before insert,after update){
    if(trigger.isInsert){
        if(trigger.isBefore){
            //THIS METHOD HANDLES BEFORE INSERT REQUESTS FROM CHANGE REQUEST TRIGGER
            EP_ChangeRequestTriggerHandler.doExecuteBeforeInsert(trigger.new);
        }
    }
    if(trigger.isUpdate){
        if(trigger.isAfter && !EP_ChangeRequestTriggerHandler.isExecuteAfterUpdate){
            //THIS METHOD HANDLES AFTER UPDATE REQUESTS FROM CHANGE REQUEST TRIGGER
            EP_ChangeRequestTriggerHandler.doExecuteAfterUpdate(trigger.old
                                                            ,trigger.new
                                                            ,trigger.oldMap
                                                            ,trigger.newMap);
        }
    }
}