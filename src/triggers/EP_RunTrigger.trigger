/* 
@Author <Accenture>
@name <EP_Run_Trigger>
@CreateDate <05/05/2017>
@Description <Trigger on EP_Run__c Object>
@Version <1.0>
*/
trigger EP_RunTrigger on EP_Run__c (before insert, before delete) { 
    if(trigger.isBefore){
        if(trigger.isInsert){               
            EP_RunDomainObject runDomainObj = new EP_RunDomainObject(trigger.new, null);                 
            runDomainObj.doActionBeforeInsert();
            
        }                             
     }
   
     if(trigger.isBefore){                        
        if(trigger.isDelete){
           EP_RunDomainObject runDomainObj = new EP_RunDomainObject(trigger.new, trigger.oldMap);
           runDomainObj.doActionBeforeDelete(); 
        }
     }
}