/* 
  @Author <Vikas Malik>
   @name <EP_CaseTrigger>
   @CreateDate <09/10/2015>
   @Description <Trigger class for Case object>
   @Version <1.0>
 
*/
trigger EP_CaseTrigger on Case (before update, before insert, after insert) {
    
    if (Trigger.isBefore)
    {
        if (Trigger.isInsert)
        {
            EP_CaseHelper.doBeforeInsert(Trigger.new);
        }
        if (Trigger.isUpdate)
        {
            EP_CaseHelper.doBeforeUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
    
    if (Trigger.isAfter)
    {
    	if (Trigger.isInsert)
        {
            EP_CaseHelper.doAfterInsert(Trigger.new);
        }
    }
    
}