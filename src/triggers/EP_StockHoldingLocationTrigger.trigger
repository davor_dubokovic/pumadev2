/* 
  @Author <Ashok Arora>
   @name <EP_StockHoldingLocationTrigger>
   @CreateDate <03/11/2015>
   @Description <This is trigger for EP_Stock_Holding_Location__c> 
   @Version <1.0>
 
*/
trigger EP_StockHoldingLocationTrigger on EP_Stock_Holding_Location__c (before delete, before insert,before update) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            EP_StorageLocationTriggerHandler.doBeforeInsert(trigger.new);
        }
        
        if(trigger.isUpdate
            && !EP_StorageLocationTriggerHandler.isExecuteBeforeUpdate){
            EP_StorageLocationTriggerHandler.doBeforeUpdate(trigger.new
                                                     ,trigger.OldMap
                                                     ,trigger.NewMap);
        }
         if(trigger.isInsert || trigger.IsUpdate){
            //to check ship-to of storage location & stock holding location
          //  EP_StorageLocationTriggerHandler.doBeforeInsertUpdate(trigger.new);
        }
        /**L4_45352_start**/
        if(trigger.isDelete){
          
            EP_StorageLocationTriggerHandler.doBeforeDelete(trigger.old);
            
        }
        /**L4_45352_start**/
        
        
    }
}