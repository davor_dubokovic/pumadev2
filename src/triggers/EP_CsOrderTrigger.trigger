trigger EP_CsOrderTrigger on csord__Order__c (after update) {

	if(trigger.isUpdate && trigger.isAfter){
		EP_CsOrderTriggerHandler.doAfterUpdate(trigger.newmap);
	}

}