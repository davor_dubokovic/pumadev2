/* 
  @Author <Ashok Arora>
   @name <EP_BankAccountTrigger>
   @CreateDate <28/10/2015>
   @Description <This is BankAccount Trigger > 
   @Version <1.0>
*/
trigger EP_BankAccountTrigger on EP_Bank_Account__c (before insert, after update, after insert, before update) {
    //if(EP_TriggerSwitch.isExecuteBankAccountTrigger) {
        if(trigger.isBefore){
            if(trigger.isInsert) {
                EP_BankAccountTriggerHandler.doBeforeInsert(trigger.new);
            }
            if(trigger.isupdate 
                && ! EP_BankAccountTriggerHandler.isExecuteBeforeUpdate){//IF FIRST TTIME EXECUTION IN THIS TRANSACTION
                EP_BankAccountTriggerHandler.doBeforeUpdate(trigger.old
                                                            ,trigger.new
                                                            ,trigger.oldMap
                                                            ,trigger.newMap);
            }
        }
    
        if(trigger.isAfter){
            if(trigger.isInsert){
                EP_BankAccountTriggerHandler.doAfterInsert(trigger.new);
            }
        }
    //}
    

    /*
    if(trigger.isAfter && (trigger.isInsert || (trigger.isUpdate && !EP_BankAccountTriggerHandler.isExecuteAfterUpdate))){
        EP_BankAccountTriggerHandler.doAfterInsertUpdate(trigger.old
                                                        ,trigger.new
                                                        ,trigger.oldMap
                                                        ,trigger.newMap);
    }*/
    
    /*if(trigger.isAfter && trigger.isUpdate && !EP_BankAccountTriggerHandler.isExecuteAfterUpdate){
        EP_BankAccountTriggerHandler.doAfterUpdate(trigger.old
                                                    ,trigger.new
                                                    ,trigger.oldMap
                                                    ,trigger.newMap);
    }*/
}