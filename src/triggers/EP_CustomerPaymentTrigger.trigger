/* 
  @Author <Pooja Dhiman>
   @name <EP_CustomerPaymentTrigger>
   @CreateDate <24/05/2015>
   @Description <This is Customer Payment Trigger > 
   @Version <1.0>
*/
trigger EP_CustomerPaymentTrigger on EP_Customer_Payment__c (after insert, after update) {
    if(trigger.isAfter && (trigger.isInsert)){
    	EP_PaymentTriggerHandler.doAfterInsert(trigger.new);
    }
}