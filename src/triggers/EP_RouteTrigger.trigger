/* 
  @Author <Accenture>
  @name <EP_RouteTrigger>
  @CreateDate <05/08/2016>
  @Description <Trigger on EP_Route__c>
  @Version <1.0>
*/
trigger EP_RouteTrigger on EP_Route__c (before update,before delete) {      
        if(trigger.isbefore){
            if(trigger.isUpdate){
               EP_RouteDomainObject routeDomainObj = new EP_RouteDomainObject(trigger.oldMap,trigger.newMap);
               routeDomainObj.doActionBeforeUpdate();
            }
            if(trigger.isDelete){
               EP_RouteDomainObject routeDomainObj = new EP_RouteDomainObject(trigger.oldMap);
               routeDomainObj.doActionBeforeDelete();
            }
        
        }
}