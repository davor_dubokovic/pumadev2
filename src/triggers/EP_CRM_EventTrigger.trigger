/* ================================================
 * @Class Name : EP_CRM_EventTrigger 
 * @author : Kamendra Singh
 * @Purpose: This trigger is used to update Last Activity date on opportunity.
 * @created date: 08/07/2016
 ================================================*/
trigger EP_CRM_EventTrigger on Event(before insert,before update,before delete) {
    try{
        if((trigger.isInsert ||trigger.isUpdate) && trigger.isbefore){
          EP_CRM_EventTriggerHandler.beforeInsertUpdateEvent(Trigger.New);
        }
        if(trigger.isDelete && trigger.isBefore){
            EP_CRM_EventTriggerHandler.beforeDeleteEvent(Trigger.Old);
        }
    }
    catch(exception ex){
        ex.getmessage();
    }
    
}