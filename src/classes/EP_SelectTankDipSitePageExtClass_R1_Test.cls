/* 
  @Author <Spiros Markantonatos>
   @name <EP_SelectTankDipSitePageExtensionClass_Test>
   @CreateDate <06/11/2014>
   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
   @Version <1.1>
    - 1.1 Updated the test class to implement site pagination
*/
@isTest
private class EP_SelectTankDipSitePageExtClass_R1_Test{

    private static Account accObj; 
    private static EP_Tank_Dip__c tankDip; 
    private static EP_Tank__c TankInstance;
    private static EP_Tank__c TankInstance1;
    
    /**************************************************************************
    *@Description : This method is used to create Data.                       *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/    
    private static void init(){
        EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy = EP_TestDataUtility.createCustomerHierarchyForNAV(2);
        TankInstance = customHierarchy.lTanks[0];
        accObj = customHierarchy.lShipToAccounts[0];
        updateAccountStatus();
        
        for (Account a : customHierarchy.lCustomerAccounts)
        {
            a.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;    
            a.EP_Tank_Dips_Schedule_Time__c = '09:00';
            a.EP_Recommended_Credit_Limit__c = 100000;
        }
        
        update customHierarchy.lCustomerAccounts;
        
        for (Account a : customHierarchy.lShipToAccounts)
        {
            a.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;    
        }
        
        update customHierarchy.lShipToAccounts;
        
        for (EP_Tank__c t : customHierarchy.lTanks)
        {
            t.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        }
        
        update customHierarchy.lTanks;
        
        for (EP_Tank__c t : customHierarchy.lTanks)
        {
            t.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        }
        
        update customHierarchy.lTanks;
        
        for (Account a : customHierarchy.lCustomerAccounts)
        {
            a.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;  
        }
        
        update customHierarchy.lCustomerAccounts;
        
        for (Account a : customHierarchy.lShipToAccounts)
        {
            a.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;  
            a.EP_Tank_Dips_Schedule_Time__c = '09:00';
        }
        
        update customHierarchy.lShipToAccounts;
        
    }
    
    private static void updateAccountStatus() {          
        EP_ActionTriggerHandler.isExecuteAfterUpdate = TRUE;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = TRUE;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = TRUE;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = TRUE;                         
    }
    
    /**************************************************************************
    *@Description : This method is used to update the Account record and Tank
                    record to meet the criteria for the page and asserts the 
                    url after clicking on back button.                        *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/
    private static testMethod void checkCancelButtonFunctionality(){
        
        // Create CSC user
        String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();   
        Profile cscAgent = [Select id from Profile Where Name = :EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        //Instantiating the Utility class
        EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy = EP_TestDataUtility.createCustomerHierarchyForNAV(2);
        updateAccountStatus();
        customHierarchy.lShipToAccounts[0].EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;    
        
        customHierarchy = setMandatoryAccountFieldsToTestHierarchy(customHierarchy);
        update customHierarchy.lShipToAccounts[0];
        
        //Updating the Account record with Active Status
        accObj = customHierarchy.lShipToAccounts[0];
        accObj.recordType = [SELECT Id, Name FROM RecordType WHERE sObjectType = 'Account' 
                             AND RecordType.developerName = 'EP_VMI_SHIP_TO' LIMIT 1];
        accObj.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE ;
        accObj.EP_Tank_Dips_Schedule_Time__c='09:00';
        update accObj;
        
        //Updating the tank record to be 'Basic Data Setup'
        TankInstance=customHierarchy.lTanks[0];
        TankInstance.EP_Tank_Status__c= EP_Common_Constant.BASIC_DATA_SETUP;
        update TankInstance;
        
        //Updating the tank record to be 'Operational'
        TankInstance.EP_Tank_Status__c= 'Operational';
        update TankInstance;
        
        //Creating a Tank Dip record in association with above Tank record
        EP_Tank_Dip__c tdip = new EP_Tank_Dip__c(recordtypeid = recType, 
                                                 EP_Unit_Of_Measure__c = 'LT', EP_Ambient_Quantity__c = 5, EP_Tank__c = TankInstance.Id,
                                                 EP_Reading_Date_Time__c = System.today() - 2,EP_Reading_Date_Time_Local__c = System.today()-2);
        insert tdip;
        System.runAs(cscUser) {
            
            Test.startTest();
                ApexPages.currentPage().getParameters().put('id', accobj.id);
                EP_SelectTankDipSitePageExtClass_R1 controller = new EP_SelectTankDipSitePageExtClass_R1();
                controller.retrieveSiteInformation();
                PageReference pgref=controller.cancel();
                System.assertEquals('/home/home.jsp', pgRef.getUrl()); //Asserting url after clicking on cancel button
            Test.stopTest();            
        }       
    }
    
    /**************************************************************************
    *@Description : Method used to update the account structure with 
                    mandatory details
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/
    private static EP_TestDataUtility.WrapperCustomerHierarchy setMandatoryAccountFieldsToTestHierarchy(EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy) {
        
        for (Account a : customHierarchy.lCustomerAccounts)
        {
            a.EP_Alternative_Payment_Method__c = 'Application in Currency';
        }
        
        return customHierarchy;
        
    }
    
    /**************************************************************************
    *@Description : If there is only one active tank and the user will 
                    redirect to tank date page by skiping selecting site page.
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/
    private static testMethod void checkIfOneActiveAccount(){
        //Create CSC User
        Profile cscAgent = [Select id from Profile Where Name =:EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy = EP_TestDataUtility.createCustomerHierarchyForNAV(1);
                
        updateAccountStatus();
        customHierarchy.lShipToAccounts[0].EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;    
        customHierarchy = setMandatoryAccountFieldsToTestHierarchy(customHierarchy);
        
        customHierarchy = setMandatoryAccountFieldsToTestHierarchy(customHierarchy);
        update customHierarchy.lShipToAccounts[0];
        
        customHierarchy.lShipToAccounts[0].EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;//Updating 1st account as Inactive
        update customHierarchy.lShipToAccounts[0];
        System.runAs(cscUser) { 
            
            Test.startTest();
            Account shippingAccount = [Select Id from Account where recordTypeId = :EP_Common_Util.fetchRecordTypeId('Account' ,EP_Common_Constant.VMI_SHIP_TO) limit 1];
            ApexPages.currentPage().getParameters().put('id', shippingAccount.id);
            EP_SelectTankDipSitePageExtClass_R1 controller = new EP_SelectTankDipSitePageExtClass_R1();
            PageReference pgrf=controller.retrieveSiteInformation();
            String strTestURL = '/apex/ep_selecttankdipdatepage_r1?id='+shippingAccount.id;
            strTestURL = strTestURL.toLowerCase();
            String strTargetURL = pgrf.getUrl().toLowerCase();
            System.assertEquals(strTestURL, strTargetURL);//Asserting tank date page for one active tank
            Test.stopTest();
            
        }
    }
    
    /**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/        
    private static testMethod void testNoOfDates(){
        //CREATE CSC USER
        String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();   
        Profile cscAgent = [SELECT Id FROM Profile WHERE Name =:EP_Common_Constant.CSC_AGENT_PROFILE LIMIT 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        
        init();   
        List<EP_Tank_Dip__c> tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{
                                                                                                new EP_Tank_Dip__c(),
                                                                                                new EP_Tank_Dip__c(),
                                                                                                new EP_Tank_Dip__c()},
                                                                                            TankInstance);
        
        Test.startTest();
        Account acc =[Select id from account where id=:accObj.id];
                   
        tankListDips[0].EP_Reading_Date_Time__c=System.today()-4;//Updating tank date as today minus 4days
        
        tankListDips[0].RecordTypeId = recType;
        update tankListDips[0]; 
        
        tankListDips[1].EP_Reading_Date_Time__c=System.today()-5;//updating tank date as today minus 20days
        tankListDips[1].RecordTypeId = recType;
        update tankListDips[1];
        
        tankListDips[2].EP_Reading_Date_Time__c=System.today()-6;//Updating tank date as today minus 6days
        tankListDips[2].RecordTypeId = recType;
        update tankListDips[2];
        System.runAs(cscUser) { 
            
            PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
            PageReference pgRef=controller.retrieveMissingSiteDates();
            Test.stopTest(); 
            System.assertEquals(true,tankListDips.size()==3); //Asserting actual tank dip size
            System.assertEquals(true,controller.tankDipDates.size()==4);//Assering missing tank dips
        }     
    }
     
    /**************************************************************************
        *@Description : This method is used to .                                  
        *@Params      : none                                                      
        *@Return      : void                                                         
    **************************************************************************/        
    private static testMethod void testCautionStatus(){ 
        String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();
        init();   
        List<EP_Tank_Dip__c> tankListDips = EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{
            new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c()},
                                                                                    TankInstance);
        Test.startTest(); 
        tankListDips[0].EP_Reading_Date_Time__c = System.today()-4;//Updating tank date as today minus 4days
        tankListDips[0].RecordTypeId = recType;
        tankListDips[0].EP_Tank__c = TankInstance.id;
        update tankListDips[0]; 
        
        tankListDips[1].EP_Reading_Date_Time__c = System.today()-5;//updating tank date as today minus 20days
        tankListDips[1].RecordTypeId = recType;
        update tankListDips[1];
        
        tankListDips[2].EP_Reading_Date_Time__c = System.today()-6;//Updating tank date as today minus 6days
        tankListDips[2].RecordTypeId = recType;
        update tankListDips[2];
        
        // Ensure that the test class has created some VMI sites
        List<Account> lVMISites = [SELECT ID FROM Account 
                                   WHERE RecordType.DeveloperName = :EP_Common_Constant.VMI_SHIP_TO_RECORD_TYPE_DEVELOPER_NAME 
                                   AND (EP_Status__c = :EP_Common_Constant.STATUS_ACTIVE OR EP_Status__c = :EP_Common_Constant.STATUS_BLOCKED)
                                   AND EP_Number_of_Non_Decommissioned_Tanks__c > 0];
        
        System.assertNotEquals(0, lVMISites.size());
        
        // Ensure that th sites have active tanks assigned to them
        List<EP_Tank__c> lTanks = [SELECT ID FROM EP_Tank__c WHERE EP_Ship_To__c IN :lVMISites AND EP_Tank_Status__c = :EP_Common_Constant.TANK_OPERATIONAL_STATUS];
        System.assertNotEquals(0, lTanks.size());
            
        //Create CSC User
        
        Profile cscAgent = [SELECT Id FROM Profile WHERE Name =:EP_Common_Constant.CSC_AGENT_PROFILE LIMIT 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
            
        System.runAs(cscUser) {  
            
            accObj = [Select Id from Account where recordTypeId = :EP_Common_Util.fetchRecordTypeId('Account' ,'VMI Ship To') limit 1];
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            EP_SelectTankDipSitePageExtClass_R1 siteObj = new EP_SelectTankDipSitePageExtClass_R1();
            EP_SelectTankDipSitePageExtClass_R1.shipToClass siteObj1 = new EP_SelectTankDipSitePageExtClass_R1.shipToClass();
            pageRef = siteObj.retrieveSiteInformation();
            System.assertNotEquals(0, siteObj.sscShipTos.getRecords().size());
            siteObj.populateSiteWrapper();
            System.assertNotEquals(0, siteObj.shipTos.size());
            System.assertEquals(TRUE, siteObj.shipTos[0].siteShowCautionStatus); 
            
            pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
            PageReference pgRef=controller.retrieveMissingSiteDates();
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here 
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            ApexPages.currentPage().getParameters().put('date',controller.tankDipDates[0].dateText);
            EP_TankDipSubmitPageControllerClass_R1 controller1 = new EP_TankDipSubmitPageControllerClass_R1();
            
            system.assert(controller1.blnShipToHasMissingOlderTankDips);
            EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass controller2 = new EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass();
            System.assertEquals(controller1.displayedTankDipLines[0].dipShowCautionStatus, TRUE); //Asserting actual tank dip size
            System.assertEquals(controller1.displayedTankDipLines.size(), 1);
            Test.stopTest();
        }   
    } 
  
    /**************************************************************************
    *@Description : This method is used to test WU 9 customer test case id 18134*
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/        
    private static testMethod void testCautionStatusNew(){
        //Create CSC User
        String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();
        Profile cscAgent = [SELECT Id FROM Profile WHERE Name =:EP_Common_Constant.CSC_AGENT_PROFILE LIMIT 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        init();   
        TankInstance.EP_Tank_Dip_Entry_Mode__c = 'Automatic Dip Entry';//Test Case ID 18131 WU 9
        update TankInstance;
        
        List<EP_Tank_Dip__c> tankListDips = EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance);
        Test.startTest();
        
        tankListDips[0].EP_Reading_Date_Time__c = System.today()-4;//Updating tank date as today minus 4days
        tankListDips[0].RecordTypeId = recType;
        tankListDips[0].EP_Tank__c = TankInstance.id;
        update tankListDips[0]; 
        
        List<RecordType> lVMIShipToRecordTypes = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND 
                                                  DeveloperName = :EP_Common_Constant.VMI_SHIP_TO_RECORD_TYPE_DEVELOPER_NAME];
        
        accObj.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        accObj.RecordTypeId = lVMIShipToRecordTypes[0].Id;
        update accObj;
        System.runAs(cscUser) {
            accObj = [Select Id from Account where recordTypeId = :EP_Common_Util.fetchRecordTypeId('Account' ,
                                                            EP_Common_Constant.VMI_SHIP_TO) limit 1];
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            EP_SelectTankDipSitePageExtClass_R1 siteObj = new EP_SelectTankDipSitePageExtClass_R1();
            pageRef = siteObj.retrieveSiteInformation();
            siteObj.populateSiteWrapper();
            
            System.assertNotEquals(0, siteObj.shipTos.size());
             
            System.assertEquals(siteObj.shipTos[0].siteShowCautionStatus, TRUE); //Test Case ID 18131 WU 9
            pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
            PageReference pgRef=controller.retrieveMissingSiteDates();
            System.debug('controller.tankDipDates'+controller.tankDipDates);
            System.debug('controller.tankDipDates[0].dateText'+controller.tankDipDates[0].dateText);
            System.debug('accObj.Id'+accObj.Id);
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here 
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            ApexPages.currentPage().getParameters().put('date',controller.tankDipDates[0].dateText);
            EP_TankDipSubmitPageControllerClass_R1 controller1=new EP_TankDipSubmitPageControllerClass_R1();         
            system.assert(controller1.blnShipToHasMissingOlderTankDips);
            
            EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass  controller2=new EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass();
            Test.stopTest(); 
            System.debug('controller2.displayedTankDipLines.size()  '+controller1.displayedTankDipLines.size()+' controller2.displayedTankDipLines   '+ controller1.displayedTankDipLines);
            System.assertEquals(controller1.displayedTankDipLines[0].dipShowCautionStatus,true); //Asserting actual tank dip size
            System.assertEquals(controller1.displayedTankDipLines.size(),1);
        }   
    }
    
    /**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/        
    private static testMethod void testDipWarningStatus(){
        //Create CSC User
        String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();
        Profile cscAgent = [SELECT Id FROM Profile WHERE Name =:EP_Common_Constant.CSC_AGENT_PROFILE LIMIT 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        init(); 
        List<EP_Tank_Dip__c> tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{
            new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c()}, TankInstance);
        Test.startTest();                                                                      
        tankListDips[0].EP_Reading_Date_Time__c = System.today()-4;//Updating tank date as today minus 4days
        tankListDips[0].RecordTypeId = recType;        
        update tankListDips[0]; 
        
        tankListDips[1].EP_Reading_Date_Time__c = System.today()-5;//Updating tank date as today minus 20days
        tankListDips[1].RecordTypeId = recType;
        update tankListDips[1];
        
        tankListDips[2].EP_Reading_Date_Time__c = System.today()-6;//Updating tank date as today minus 6days
        tankListDips[2].RecordTypeId = recType;
        update tankListDips[2];
        
        System.runAs(cscUser) {  
         
            accObj = [Select Id from Account where recordTypeId = :EP_Common_Util.fetchRecordTypeId('Account' ,EP_Common_Constant.VMI_SHIP_TO) limit 1];
            PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
            PageReference pgRef = controller.retrieveMissingSiteDates();
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here 
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            ApexPages.currentPage().getParameters().put('date', System.now().format('yyyyMMddHHmmss'));
            Test.stopTest(); 
        }   
    }
  
    /**************************************************************************
        *@Description : This method is used to .                                  *
        *@Params      : none                                                      *
        *@Return      : void                                                      *    
    **************************************************************************/        
    private static testMethod void testSiteWarningStatus(){ 
        //Create CSC User
        String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();  
        Profile cscAgent = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.CSC_AGENT_PROFILE LIMIT 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.Id);
        EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(2);
        customHierarchy.lShipToAccounts[0].EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update customHierarchy.lShipToAccounts[0];
        customHierarchy.lShipToAccounts[1].EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
         update customHierarchy.lShipToAccounts[1];
        customHierarchy.lShipToAccounts[0].EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; //Updating 1st account as Inactive
        customHierarchy.lShipToAccounts[1].EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; //Updating 1st account as Inactive
        customHierarchy.lShipToAccounts[1].EP_Tank_Dips_Schedule_Time__c = '09:00';
        customHierarchy.lShipToAccounts[0].EP_Tank_Dips_Schedule_Time__c = '09:00';
        update customHierarchy.lShipToAccounts[0];  
        update customHierarchy.lShipToAccounts[1];
        TankInstance=customHierarchy.lTanks[0];
        TankInstance1=customHierarchy.lTanks[1];
        List<EP_Tank_Dip__c> tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{
            new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c()},
                                                                                   TankInstance);
        List<EP_Tank_Dip__c> tankListDips1=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{
            new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c(),
                new EP_Tank_Dip__c()},
                                                                                    TankInstance1);
        Test.startTest();
        tankListDips[0].EP_Reading_Date_Time__c = System.today()-4; //Updating tank date as today minus 4days
        tankListDips[0].RecordTypeId = recType;
        update tankListDips[0]; 
        tankListDips[1].EP_Reading_Date_Time__c = System.today()-5; //updating tank date as today minus 20days
        tankListDips[1].RecordTypeId = recType;
        update tankListDips[1];
        tankListDips1[0].EP_Reading_Date_Time__c=System.today(); //Updating tank date as today minus 6days
        tankListDips1[0].RecordTypeId = recType;
        update tankListDips1[0];
        System.runAs(cscUser) {
            accObj = [Select Id from Account where recordTypeId = :EP_Common_Util.fetchRecordTypeId('Account' ,EP_Common_Constant.VMI_SHIP_TO) limit 1];
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            ApexPages.currentPage().getParameters().put('id', customHierarchy.lShipToAccounts[1].Id);
            EP_SelectTankDipSitePageExtClass_R1 siteObj = new EP_SelectTankDipSitePageExtClass_R1();
            EP_SelectTankDipSitePageExtClass_R1.shipToClass siteObj1 = new EP_SelectTankDipSitePageExtClass_R1.shipToClass();
            pageRef = siteObj.retrieveSiteInformation();
            Test.stopTest();
        }   
    }   
  
    /*
    Test standard controller
    */ 
    private static testMethod void teststdController(){ 
         //Create CSC User
         String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();  
         Profile cscAgent = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.CSC_AGENT_PROFILE LIMIT 1];
         User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
         EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy = EP_TestDataUtility.createCustomerHierarchyForNAV(2);
         customHierarchy.lShipToAccounts[0].EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
         update customHierarchy.lShipToAccounts[0];
         customHierarchy.lShipToAccounts[0].EP_Status__c = EP_Common_Constant.ACTIVE_SHIP_TO_STATUS;
         update customHierarchy.lShipToAccounts[0];
            
         System.runAs(cscUser) {
            
            // Fix for Defect 29809 - Start
            system.Test.setCurrentPage(Page.EP_SelectTankDipSitePage_R1);
            ApexPages.currentPage().getParameters().put('pageNumber', '1');
            
            EP_SelectTankDipSitePageExtClass_R1 siteObj = new EP_SelectTankDipSitePageExtClass_R1();
            siteObj.retrieveSiteInformation();
            ApexPages.StandardSetController stdC = siteObj.sscShipTos;
            Boolean next = siteObj.hasNext;
            Boolean previous = siteObj.hasPrevious;
            siteObj.next();
            siteObj.previous();
            
            system.assertEquals(NULL, siteObj.goToLast());
            system.assertEquals(NULL, siteObj.goToFirst());
            system.assertEquals(1, siteObj.sscShipTos.getPageNumber());
            // Fix for Defect 29809 - End
        }
    }
}