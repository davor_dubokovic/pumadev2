/* 
    @Author <Nicola Tassini>
    @name <EP_FE_Utils>
    @CreateDate <18/04/2016>
    @Description <This class to get list of all the active sell to accounts and related shiptos associated with the current user  >  
    @Version <1.0>
*/
public with sharing class EP_FE_Utils {

    private Integer NUMBER_OF_MAX_DAYS_ADVANCE_ORDER = 5;
    private static Map<String, String> cachedProfileTypeMap = null;
    private static Boolean isProfileTypeMapCached = false;

    public static final String CLASS_NAME = 'EP_FE_Utils';
    public static final String METHOD_NAME = 'profileNameProfileTypeMap';
    public static final String FALSE_VAL = 'false';
    public static final String TRUE_VAL = 'true';
    
    private static String pricingEngineTurnedOff = null;

    static Boolean checkElse = true;
    
    
    public static String userProfileName {
        get {
            if (userProfileName == null) {
                Profile userProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId() limit 100];
                userProfileName = userProfile.Name; 
            }
            return userProfileName;
        } private set;
    }
/*********************************************************************************************
     @Author <>
     @name <getUserProfileName>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/     
    public static String getUserProfileName(){
        return userProfileName;
    }
    
    
/*********************************************************************************************
     @Author <Iegor Nechyporenko>
     @name <isPricingEngineTurnedOff>
     @Description < Checks if pricing engine has been successfully turned off >  
     @Version <1.0>
    *********************************************************************************************/     
    public static Boolean isPricingEngineTurnedOff(){
        if (pricingEngineTurnedOff == null){
            List<EP_FE_Configuration__mdt> pricingMetadata = [SELECT DeveloperName ,EP_FE_Configuration_Value__c 
                FROM EP_FE_Configuration__mdt 
                WHERE DeveloperName = :EP_FE_Constants.PRICING_ENGINE_TURNED_OFF
                LIMIT 1];
                
            pricingEngineTurnedOff = (pricingMetadata.size() > 0) ? pricingMetadata.get(0).EP_FE_Configuration_Value__c : FALSE_VAL;
        }
        
        String positiveResponse = TRUE_VAL;
        return positiveResponse.equalsIgnoreCase(pricingEngineTurnedOff);
    }
    
    /*
    @Description : This method is to log error on the basis of set of parameters
    @Param : ClassName, methodName, area, functionality, issue, Exception, Status, Reponse
    */     
    public static void logError(String className, String methodName, String area, String functionality,
                                String issue, Exception exe, Integer status, EP_FE_Response response) {
        if (!Test.IsRunningTest() && Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()) {
            EP_LoggingService.logHandledException(exe == null ? new DummyException() : exe, 
                    EP_FE_Constants.FE_PROJECT_CODENAME, methodName, className, ApexPages.Severity.ERROR);
        }
        if (response != null) {
            response.status = status;
            if (exe != null) {
                response.addError(exe.getMessage());
            }
        }
    }
 
    /*
    *Class DummyException 
    */
    public with sharing class DummyException extends Exception {}
    
    /*
    @Description : This method is to log error on the basis of set of parameters
    @Param : ClassName, methodName, area, functionality, issue, Exception, Status, Reponse
    */
    public static void logError(EP_Exception_Log__c exObj, EP_FE_Response response, Integer status) {
        try {
            exObj.EP_Exception_Code__C = string.valueof(status);
            if (Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()) { EP_LoggingService.logException(exObj); }
            if (response != null){
                response.status = status; }
            //response.addError(exObj.EP_Exception_Description__c);
        } catch (exception ex) { 
            if (response != null) {
                response.addError(ex.getMessage());
            }
        }
    }
    
    /*
    @Description : This method is to log error on the basis of set of parameters
    @Param : Exception List, Response, Status, 
    */    
    public static void logError(List<EP_Exception_Log__c> exObjList, EP_FE_Response response, Integer status) {
        try {
            //exObj.EP_Exception_Code__C = string.valueof(status);
            
            if (Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()) { EP_LoggingService.logListException(exObjList);} 
            if (response != null){
                response.status = status; }
            //response.addError(exObj.EP_Exception_Description__c);
        } catch (exception ex) { 
            if (response != null) {
                response.addError(ex.getMessage());
            }
        }
    }        

    /*
    @Description : This method is to log error on the basis of set of parameters
    @Param : Exception Log Object
    */
    public static void logError(EP_Exception_Log__c exObj) {
        try{
            logError(exObj, null, null);
        }catch(Exception ex){
            throw ex;
        } 
    }
    
    /*
    @Description : This method is to log error on the basis of set of parameters
    @Param : Exception Log List
    */
    public static void logError(List<EP_Exception_Log__c> exObjList) {
        try{
            logError(exObjList, null, null);
        }catch(Exception ex){
            throw ex;
        } 
        
    }
    
    /*
    @Description: This method is to build error object for
    @Param: String className, String methodName,String severity, String transactionId, String description
    */
    public static EP_Exception_Log__c createLogErrorObject(String className, String methodName,
                                                           String severity, String transactionId, 
                                                           String description) {
    
        EP_Exception_Log__c exObj = new EP_Exception_Log__c();
        
        try{
            exObj.EP_Application_Name__c = EP_FE_Constants.FE_PROJECT_CODENAME;
            exObj.EP_Exception_Code__C = null;
            exObj.EP_Class_Name__c = (className != null) ? getInputValue(className) : null; //
            exObj.EP_Method_Name__c = (methodName != null) ? getInputValue(methodName) : null;
            exObj.EP_Severity__c = (severity != null) ? getInputValue(severity) : null; //
            exObj.EP_WS_Transaction_ID__c = (transactionId != null) ? getInputValue(transactionId) : null; //Identifer for API
            exObj.EP_Exception_Description__c = (description != null) ? getInputValue(description) : null; // des
            exObj.EP_Running_User__c = UserInfo.getUserId(); // running user id
            exObj.EP_Handled__c = true; // because this is known exception, alert, issue log
        }catch(Exception ex){
            throw ex;
        } 
        
        return exObj;
    }
    
    /*
    @Description : This method is to log error on the basis of set of parameters
    @Param : Exception Log Object
    */
    public static EP_Exception_Log__c createLogErrorObject(EP_FE_ErrorLogRequest objLogReq) {
    
        EP_Exception_Log__c exObj = new EP_Exception_Log__c();

        try{
            exObj.EP_Exception_Details__c = (objLogReq.errorDetail != null) ? objLogReq.errorDetail : null;
            exObj.EP_Exception_Description__c = (objLogReq.Description != null) ? objLogReq.Description : null;
            exObj.EP_Running_User__c = (objLogReq.userid != null) ? objLogReq.userid : null;
            exObj.EP_Component__c = (objLogReq.componentName != null) ? objLogReq.componentName : null;
            exObj.EP_PerformedAction__c = (objLogReq.performedAction != null) ? objLogReq.performedAction : null;
            exObj.EP_Handled__c = (objLogReq.wasHandled != null) ? objLogReq.wasHandled : null;
            exObj.EP_Browser__c = (objLogReq.browser != null) ? objLogReq.browser : null;
            exObj.EP_Device__c = (objLogReq.deviceInfo != null) ? objLogReq.deviceInfo : null;
            exObj.EP_OperatingSystem__c = (objLogReq.OperatingSystem != null) ? objLogReq.OperatingSystem : null;
        }catch(Exception ex){
            throw ex;
        }  
            
        return exObj;
    }
    
    
    /*
    @Description : This method is to log error on the basis of set of parameters
    @Param : Exception Log List
    */
    public static List<EP_Exception_Log__c> createLogErrorObject(List<EP_FE_ErrorLogRequest> objLogReqList) {
    
        List<EP_Exception_Log__c> expObjList = new List<EP_Exception_Log__c>();
        EP_Exception_Log__c exObj = null;
        
        try{
            for (EP_FE_ErrorLogRequest objLogReq : objLogReqList) {
            
            exObj = new EP_Exception_Log__c();
            
            exObj.EP_Exception_Details__c = (objLogReq.errorDetail != null) ? objLogReq.errorDetail : null;
            exObj.EP_Exception_Description__c = (objLogReq.Description != null) ? objLogReq.Description : null;
            exObj.EP_Running_User__c = (objLogReq.userid != null && objLogReq.userid !='{!$User.Id}') ? objLogReq.userid : userinfo.getuserid();
            exObj.EP_Component__c = (objLogReq.componentName != null) ? objLogReq.componentName : null;
            exObj.EP_PerformedAction__c = (objLogReq.performedAction != null) ? objLogReq.performedAction : null;
            exObj.EP_Handled__c = (objLogReq.wasHandled != null) ? objLogReq.wasHandled : null;
            exObj.EP_Browser__c = (objLogReq.browser != null) ? objLogReq.browser : null;
            exObj.EP_Device__c = (objLogReq.deviceInfo != null) ? objLogReq.deviceInfo : null;
            exObj.EP_OperatingSystem__c = (objLogReq.OperatingSystem != null) ? objLogReq.OperatingSystem : null;
            
            expObjList.add(exObj);
            }
        }catch(Exception ex){
            throw ex;
        }            
    
        return expObjList;
    }
    
    /*
    Raise an exception if a testing is running.
    Purpose: code coverage
    */
    public static void raiseExceptionIfTestIsRunning() {
        if (Test.IsRunningTest()) {
            Integer i = 0 / 0;
        }
    }
    
    /*
    Return company details based on Sell-To
    */
    public static Company__c returnSellToCompanyInfo(String sellToID) {
    
        Company__c company = NULL;
        
        try{
            if (sellToID != NULL) {
            List < Account > accounts = [SELECT EP_Puma_Company__c, EP_Puma_Company__r.EP_Additional_Tax__c,
                                        EP_Puma_Company__r.EP_Bulk_Non_Bulk_Mix_Enabled__c, EP_Puma_Company__r.EP_Company_Code__c
                                        FROM Account 
                                        WHERE ID = : sellToID limit 10000
                                    ];
        
                if (!accounts.isEmpty()) {
                /*
                company = new Company__c([Id = accounts[0].EP_Puma_Company__c,
                EP_Additional_Tax__c = accounts[0].EP_Puma_Company__r.EP_Additional_Tax__c,
                EP_Bulk_Non_Bulk_Mix_Enabled__c = accounts[0].EP_Puma_Company__r.EP_Bulk_Non_Bulk_Mix_Enabled__c,
                EP_Company_Code__c = accounts[0].EP_Puma_Company__r.EP_Company_Code__c]);
                
                */
                } // End account list check
            } // End sell-to ID check
        }catch(Exception ex){
            throw ex;
        }
        
        
        return company;
    }
    
    /*
    *   Build a map with actual profile names and types
    *   
    */
    public static Map<String, String> profileNameProfileTypeMap() {
        if (isProfileTypeMapCached) {
            return cachedProfileTypeMap;
        }
        
        List<EP_FE_Configuration__mdt> profileConfigurations =[SELECT DeveloperName, EP_FE_Configuration_Value__c 
            FROM EP_FE_Configuration__mdt 
            WHERE EP_FE_Configuration_Type__c = :EP_FE_Constants.CONFIG_PROFILE_NAME limit 10000 ];
           
        isProfileTypeMapCached = true;
        if(profileConfigurations == null || profileConfigurations.size() == 0) {
            logError(createLogErrorObject(CLASS_NAME, METHOD_NAME, EP_FE_BootstrapResponse.ERROR,
                EP_FE_BootstrapResponse.TRANSACTION_ID, EP_FE_BootstrapResponse.DESCRIPTION), null, EP_FE_BootstrapResponse.CONFIGURATION_NOT_SET);

            return null;
        } else {
            // Build a map of Real Profiles names vs Actual meaning as per the constant class
            Map<String, String> profileTypesMap = new Map<String,String>();
            String profileConfigKey;
            for(EP_FE_Configuration__mdt profileConfiguration : profileConfigurations) {
                profileConfigKey = null;

                if(profileConfiguration.DeveloperName.equalsIgnoreCase(EP_FE_Constants.PROFILE_BASIC_ACCESS)) {
                    profileConfigKey = EP_FE_Constants.PROFILE_BASIC_ACCESS;
                } else if(profileConfiguration.DeveloperName.equalsIgnoreCase(EP_FE_Constants.PROFILE_BASIC_ORDERING)) {
                    profileConfigKey = EP_FE_Constants.PROFILE_BASIC_ORDERING;
                } else if(profileConfiguration.DeveloperName.equalsIgnoreCase(EP_FE_Constants.PROFILE_MANAGE_ACCESS)) {
                    profileConfigKey = EP_FE_Constants.PROFILE_MANAGE_ACCESS;
                } else if(profileConfiguration.DeveloperName.equalsIgnoreCase(EP_FE_Constants.PROFILE_FINANCE_ACCESS)) {
                    profileConfigKey = EP_FE_Constants.PROFILE_FINANCE_ACCESS;
                } else if(profileConfiguration.DeveloperName.equalsIgnoreCase(EP_FE_Constants.PROFILE_SUPER_USER)) {
                    profileConfigKey = EP_FE_Constants.PROFILE_SUPER_USER;
                } else {
                    checkElse = false;
                }

                if(profileConfigKey != null) {
                    profileTypesMap.put(profileConfiguration.EP_FE_Configuration_Value__c.toLowerCase(), profileConfigKey);
                }
            }
            cachedProfileTypeMap = profileTypesMap;
            return profileTypesMap;
        }
    }
/*********************************************************************************************
     @Author <>
     @name <isUserProfileAccessEquals>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/    
    public static Boolean isUserProfileAccessEquals(String profileAccess){
        String profileString = userProfileName.toLowerCase();
        Map<String, String> profileAccessMap = profileNameProfileTypeMap(); 
        return (profileAccessMap == null) ? false : profileAccess.equalsIgnoreCase(profileAccessMap.get(profileString));
    }
    
    /*
    Return JSON
    */
    public String returnValue {
        get {
            return JSON.serialize(returnTerminalInfo());
        }
    }

    /*
    Return terminal information, including opening hours and available data
    */
    public static List < EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo > returnTerminalInfo() {

        // Initialise variables
        List < EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo > terminalInfoRecords = new List < EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo > ();
        EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo terminalInfoRecord;
        Set < ID > terminalIDs = new Set < ID > ();
        Set < ID > businessHoursIDs = new Set < ID > ();
        List < EP_Stock_Holding_Location__c > stockHoldingLocations;
        List < Account > accounts;
        //List<BusinessHours> stockLocationBusinessHours;
        //List < EP_Inventory__c > inventories;
        //List < Date > availableOrderDates;
        Map < String, BusinessHours > shlBusinessHoursMap;
        BusinessHours shlBusinessHours;
        List < String > availableProductIDs;
        DateTime dtTomorrow;
        DateTime dtEndDate;
        DateTime dt;
        Datetime dtTargetOpenDateTime;
        Time tOpeningTime;
        Time tClosingTime;
        EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime dateOpeningTime;
        List < EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime > dateOpeningTimes;
        Map < String, String > terminalMap = new Map < String, String > (); // This map is used to ensure that the method returns terminal once
        // In case the same terminal is used by multiple ship-to/sell-to

        // Step 1 - Return all Ship-To and Sell-To accounts
        accounts = [SELECT ID, RecordType.DeveloperName 
                    FROM Account
                    WHERE RecordType.DeveloperName IN ( : EP_Common_Constant.VMI_SHIP_TO_DEV_NAME, 
                        : EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME, 
                        : EP_Common_Constant.SELL_TO_DEV_NAME) limit 10000
                   ];

        if (!accounts.isEmpty()) {
            // Step 2 - Return all stock holding locations for the Sell-Tos and Ship-Tos that the customer has access to
            stockHoldingLocations = [SELECT ID, Stock_Holding_Location__c, Stock_Holding_Location__r.Name,
                                        Stock_Holding_Location__r.EP_Business_Hours__c, Stock_Holding_Location__r.EP_Slotting_Enabled__c
                                     FROM EP_Stock_Holding_Location__c
                                     WHERE EP_Sell_To__c IN: accounts OR EP_Ship_To__c IN: accounts limit 10000
                                    ];
                
            if (!stockHoldingLocations.isEmpty()) {
                // Build stock holding location SET and business hours map
                for (EP_Stock_Holding_Location__c shl: stockHoldingLocations) {
                    terminalIDs.add(shl.Stock_Holding_Location__c);
                    businessHoursIDs.add(shl.Stock_Holding_Location__r.EP_Business_Hours__c);
                } // End for
                
                // Step 3 - Retrieve all available terminal products and build a terminal/product map
                /*inventories = [SELECT ID, EP_Storage_Location__c, EP_Product__r.Name
                               FROM EP_Inventory__c WHERE EP_Storage_Location__c IN: terminalIDs limit 10000
                ];*/
                
                // Step 4 - Retrieve all business hours for the terminal locations
                shlBusinessHoursMap = new Map < String, BusinessHours > ([SELECT Id FROM BusinessHours WHERE Id IN: businessHoursIDs limit 10000]);
                
                for (EP_Stock_Holding_Location__c shl: stockHoldingLocations) {
                // Add terminals only if not returned before
                    if (!terminalMap.containsKey(shl.Stock_Holding_Location__c)) {
                        // Build the terminal info record
                        terminalInfoRecord = new EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo();
                        availableProductIDs = new List < String > ();
                
                        // Build the opening dates and times for each terminal
                        dateOpeningTimes = new List < EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime > ();
                
                    if (!shlBusinessHoursMap.isEmpty()) {
                        // Step 5 - Calculate the next available pick-up dates up to the max order date in advance
                        
                        // TODO - Need to add some logic to determine the cut-off time based on country, order type etc
                        // This needs to be designed in the back-end first
                        dtTomorrow = EP_PortalLibClass_R1.returnLocalDateTime(DateTime.newInstance(System.Today().Year(),
                        System.Today().Month(),
                        System.Today().Day(),
                        1, 0, 0));
                        
                        dtTomorrow = dtTomorrow.addDays(1);
                        dtEndDate = dtTomorrow.addDays(5);
                        dt = dtTomorrow;
                        
                    if (shlBusinessHoursMap != NULL) {
                        do {
                            if (shlBusinessHoursMap.containsKey(shl.Stock_Holding_Location__r.EP_Business_Hours__c)) {
                                shlBusinessHours = shlBusinessHoursMap.get(shl.Stock_Holding_Location__r.EP_Business_Hours__c);
                                
                                for (Integer i = 0; i < 24; i++) {
                                    dtTargetOpenDateTime = EP_PortalLibClass_R1.returnLocalDateTime(
                                    Datetime.newInstance(dt.Year(), dt.Month(), dt.Day(), i, 0, 0));
                                    
                                    dateOpeningTime = new EP_FE_NewOrderInfoResponse.EP_FE_DateOpeningTime(
                                                      EP_PortalLibClass_R1.returnLocalDate(dtTargetOpenDateTime));
                                                      
                                    if (BusinessHours.isWithin(shlBusinessHours.Id, dtTargetOpenDateTime)) {
                                        tOpeningTime = Time.newInstance(i, 0, 0, 0);
                                        tClosingTime = Time.newInstance(i, 59, 59, 0);
                                        dateOpeningTime.timePeriods.add(new EP_FE_NewOrderInfoResponse.EP_FE_TimePeriod(tOpeningTime, tClosingTime));
                                    }
                                    
                                    if (dateOpeningTime.timePeriods.size() > 0) {
                                        dateOpeningTimes.add(dateOpeningTime);
                                    }
                                } // End for
                            } // End map check
                        
                            dt = dt.addDays(1);
                            } while (dt < dtEndDate);
                        } // End map check
                    } // End business hours check
                    
                    // Build the product name list
                    for (EP_Inventory__c inventory: [SELECT ID, EP_Storage_Location__c, EP_Product__r.Name
                               FROM EP_Inventory__c WHERE EP_Storage_Location__c IN: terminalIDs limit 10000
                                ]) {
                        if (inventory.EP_Storage_Location__c == shl.Stock_Holding_Location__c) {
                            availableProductIDs.add(inventory.EP_Product__c);
                        } // End terminal ID check
                    } // End for
                    
                    terminalInfoRecord = new EP_FE_NewOrderInfoResponse.EP_FE_TerminalInfo();
                    terminalInfoRecord.terminalName = shl.Stock_Holding_Location__r.Name;
                    terminalInfoRecord.terminalId = shl.Stock_Holding_Location__c;
                    terminalInfoRecord.availableDateTimeList = dateOpeningTimes;
                    terminalInfoRecord.availableProductIds = availableProductIDs;
                    terminalInfoRecords.add(terminalInfoRecord);
                    
                    terminalMap.put(terminalInfoRecord.terminalId, terminalInfoRecord.terminalId);
                    } // End unique terminal check
                } //End for 
            } // End stock holding location check
        } // End account list check
        
        return terminalInfoRecords;
    }
    
    /*********************************************************************************************
     @Author <Jyotsna Tiwari>
     @name <EP_FE_DatePeriod>
     @CreateDate <02/09/2016>
     @Description < This method is used to capture startdate and end date >  
     @Version <1.0>
    *********************************************************************************************/
    public with sharing class EP_FE_DatePeriod{
        public Date startDate;
        public Date endDate;
        
   /*********************************************************************************************
     @Author <Jyotsna Tiwari>
     @name <EP_FE_DatePeriod>
     @CreateDate <02/09/2016>
     @Description < This method is used to capture startdate and end date >  
     @Version <1.0>
    *********************************************************************************************/     
        public EP_FE_DatePeriod(Date startDate, Date endDate){
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }

/*********************************************************************************************
     @Author <Iegor Nechyporenko>
     @name <getShipToPossibleTimes>
     @CreateDate <02/09/2016>
     @Description < For specific List of Ship To Accounts returns date period for which new order could be created >  
     @Version <1.0>
    *********************************************************************************************/        
    public static Map<Id, EP_FE_DatePeriod> getShipToDeliveryOrderPeriods(List<Account> shipToList){
        Map<Id, EP_FE_DatePeriod> shipToResponsesMap = new Map<Id, EP_FE_DatePeriod>();
    
        List<EP_Stock_Holding_Location__c> defaultSitesSupplyLocations = [
            SELECT Id, EP_Ship_To__c, EP_Trip_Duration__c,  Stock_Holding_Location__r.EP_Scheduling_Hours__c
            FROM EP_Stock_Holding_Location__c
            WHERE EP_Ship_To__c IN :shipToList
            AND EP_Location_Type__c = :EP_Common_Constant.PRIMARY
            LIMIT 49999
        ];
        
        Map<Id, EP_Stock_Holding_Location__c> siteDefaultLocationsMap = new Map<Id, EP_Stock_Holding_Location__c>();
        for (EP_Stock_Holding_Location__c defaultLocation : defaultSitesSupplyLocations){
            siteDefaultLocationsMap.put(defaultLocation.EP_Ship_To__c, defaultLocation);
        }

        
        // Prepare the output with the list of shipTos
        for (Account shipTo: shipToList) {
            Date startDate = Date.today(), endDate = null;
            EP_Stock_Holding_Location__c defaultSiteLocation = siteDefaultLocationsMap.get(shipTo.Id);
            if (defaultSiteLocation != null){
                if (defaultSiteLocation.Stock_Holding_Location__r != null 
                    && defaultSiteLocation.Stock_Holding_Location__r.EP_Scheduling_Hours__c != null){
                        Id businessHoursId = defaultSiteLocation.Stock_Holding_Location__r.EP_Scheduling_Hours__c;
                        DateTime nextWorkingDateTime = BusinessHours.nextStartDate(businessHoursId, DateTime.now());
                        startDate = Date.newInstance(nextWorkingDateTime.year(), nextWorkingDateTime.month(), nextWorkingDateTime.day()).addDays(1);
                }
                
                if (defaultSiteLocation.EP_Trip_Duration__c != null){
                    startDate = startDate.addDays(Integer.valueOf(defaultSiteLocation.EP_Trip_Duration__c));
                }
                
                if (shipTo.EP_Puma_Company__r != null && shipTo.EP_Puma_Company__r.EP_Order_Request_Window__c != null){
                    endDate = Date.valueOf(startDate).addDays(Integer.valueOf(shipTo.EP_Puma_Company__r.EP_Order_Request_Window__c));
                }
            }
           
            //tripDuration = shlTripDurationMap.get(shipTo.Id);
            //startDate = nextDateTime != null ? Date.valueOf(nextDateTime) + Integer.valueOf(tripDuration.EP_Trip_Duration__c) : system.Today();
            
            EP_FE_DatePeriod datePeriod = new EP_FE_DatePeriod(startDate, endDate);
            shipToResponsesMap.put(shipTo.Id, datePeriod);
        }

        return shipToResponsesMap; 
    }
    
    
        
    /*********************************************************************************************
        @Author <Iegor Nechyporenko>
        @name <getLatestTermsAndCondtions>
        @CreateDate <01/09/2016>
        @Description <Method is used to get information about terms and Conditions which should be presented to user>  
        @Version <1.0>
    *********************************************************************************************/ 
    public static EP_FE_Terms_and_Conditions__c getLatestTermsAndCondtions(Id countryId){
        List<EP_FE_Terms_and_Conditions__c> latestTermsAndConditions = [SELECT EP_FE_Terms_Condition__c, EP_FE_Country__c, Name, EP_FE_Version__c
            FROM EP_FE_Terms_and_Conditions__c
            WHERE EP_FE_Country__c = :countryId
            ORDER BY EP_FE_Version__c DESC 
            LIMIT 1];
        
        try{
            if (latestTermsAndConditions.size() == 0){
                latestTermsAndConditions = [SELECT EP_FE_Terms_Condition__c, EP_FE_Country__c, Name, EP_FE_Version__c
                    FROM EP_FE_Terms_and_Conditions__c
                    WHERE EP_FE_Country__c = null
                    ORDER BY EP_FE_Version__c DESC 
                    LIMIT 1];           
            }
        }catch(Exception ex){
            throw ex;
        } 
            
        return latestTermsAndConditions.size() > 0 ? latestTermsAndConditions.get(0) : null;
    }
    
    /*********************************************************************************************
        @Author <Jyotsna Tiwari>
        @name <getUserDateTimeFormat>
        @CreateDate <01/09/2016>
        @Description <Method is used to get locale of the logged in user>  
        @Version <1.0>
        // PR3 Defect : 28939  start
    *********************************************************************************************/
    public static String getUserDateTimeFormat() {
        String strUserLocale = UserInfo.getLocale();
        String strDateTimeLocale = 'dd/MM/yyyy hh:mm';

        try{
            if (mapLocaleToDateTime.containsKey(strUserLocale))
            strDateTimeLocale = mapLocaleToDateTime.get(strUserLocale);
            }catch(Exception ex){
                throw ex;
            }

        return strDateTimeLocale;
    }
 
    public static Map<String, String> mapLocaleToDateTime {
        get {
                if (mapLocaleToDateTime == NULL)
                    mapLocaleToDateTime = new Map<String, String> {
                        'ar'            => 'dd/MM/yyyy hh:mm a',
                        'ar_AE'         => 'dd/MM/yyyy hh:mm a',
                        'ar_BH'         => 'dd/MM/yyyy hh:mm a',
                        'ar_JO'         => 'dd/MM/yyyy hh:mm a',
                        'ar_KW'         => 'dd/MM/yyyy hh:mm a',
                        'ar_LB'         => 'dd/MM/yyyy hh:mm a',
                        'ar_SA'         => 'dd/MM/yyyy hh:mm a',
                        'bg_BG'         => 'yyyy-M-d H:mm',
                        'ca'            => 'dd/MM/yyyy HH:mm',
                        'ca_ES'         => 'dd/MM/yyyy HH:mm',
                        'ca_ES_EURO'    => 'dd/MM/yyyy HH:mm',
                        'cs'            => 'd.M.yyyy H:mm',
                        'cs_CZ'         => 'd.M.yyyy H:mm',
                        'da'            => 'dd-MM-yyyy HH:mm',
                        'da_DK'         => 'dd-MM-yyyy HH:mm',
                        'de'            => 'dd.MM.yyyy HH:mm',
                        'de_AT'         => 'dd.MM.yyyy HH:mm',
                        'de_AT_EURO'    => 'dd.MM.yyyy HH:mm',
                        'de_CH'         => 'dd.MM.yyyy HH:mm',
                        'de_DE'         => 'dd.MM.yyyy HH:mm',
                        'de_DE_EURO'    => 'dd.MM.yyyy HH:mm',
                        'de_LU'         => 'dd.MM.yyyy HH:mm',
                        'de_LU_EURO'    => 'dd.MM.yyyy HH:mm',
                        'el_GR'         => 'd/M/yyyy h:mm a',
                        'en_AU'         => 'd/MM/yyyy HH:mm',
                        'en_B'          => 'M/d/yyyy h:mm a',
                        'en_BM'         => 'M/d/yyyy h:mm a',
                        'en_CA'         => 'dd/MM/yyyy h:mm a',
                        'en_GB'         => 'dd/MM/yyyy HH:mm',
                        'en_GH'         => 'M/d/yyyy h:mm a',
                        'en_ID'         => 'M/d/yyyy h:mm a',
                        'en_IE'         => 'dd/MM/yyyy HH:mm',
                        'en_IE_EURO'    => 'dd/MM/yyyy HH:mm',
                        'en_NZ'         => 'd/MM/yyyy HH:mm',
                        'en_SG'         => 'M/d/yyyy h:mm a',
                        'en_US'         => 'M/d/yyyy h:mm a',
                        'en_ZA'         => 'yyyy/MM/dd hh:mm a',
                        'es'            => 'd/MM/yyyy H:mm',
                        'es_AR'         => 'dd/MM/yyyy HH:mm',
                        'es_BO'         => 'dd-MM-yyyy hh:mm a',
                        'es_CL'         => 'dd-MM-yyyy hh:mm a',
                        'es_CO'         => 'd/MM/yyyy hh:mm a',
                        'es_CR'         => 'dd/MM/yyyy hh:mm a',
                        'es_EC'         => 'dd/MM/yyyy hh:mm a',
                        'es_ES'         => 'd/MM/yyyy H:mm',
                        'es_ES_EURO'    => 'd/MM/yyyy H:mm',
                        'es_GT'         => 'd/MM/yyyy hh:mm a',
                        'es_HN'         => 'MM-dd-yyyy hh:mm a',
                        'es_MX'         => 'd/MM/yyyy hh:mm a',
                        'es_PE'         => 'dd/MM/yyyy hh:mm a',
                        'es_PR'         => 'MM-dd-yyyy hh:mm a',
                        'es_PY'         => 'dd/MM/yyyy hh:mm a',
                        'es_SV'         => 'MM-dd-yyyy hh:mm a',
                        'es_UY'         => 'dd/MM/yyyy hh:mm a',
                        'es_VE'         => 'dd/MM/yyyy hh:mm a',
                        'et_EE'         => 'd.MM.yyyy H:mm',
                        'fi'            => 'd.M.yyyy H:mm',
                        'fi_FI'         => 'd.M.yyyy H:mm',
                        'fi_FI_EURO'    => 'd.M.yyyy H:mm',
                        'fr'            => 'dd/MM/yyyy HH:mm',
                        'fr_BE'         => 'd/MM/yyyy H:mm',
                        'fr_CA'         => 'yyyy-MM-dd HH:mm',
                        'fr_CH'         => 'dd.MM.yyyy HH:mm',
                        'fr_FR'         => 'dd/MM/yyyy HH:mm',
                        'fr_FR_EURO'    => 'dd/MM/yyyy HH:mm',
                        'fr_LU'         => 'dd/MM/yyyy HH:mm',
                        'fr_MC'         => 'dd/MM/yyyy HH:mm',
                        'hr_HR'         => 'yyyy.MM.dd HH:mm',
                        'hu'            => 'yyyy.MM.dd. H:mm',
                        'hy_AM'         => 'M/d/yyyy h:mm a',
                        'is_IS'         => 'd.M.yyyy HH:mm',
                        'it'            => 'dd/MM/yyyy H.mm',
                        'it_CH'         => 'dd.MM.yyyy HH:mm',
                        'it_IT'         => 'dd/MM/yyyy H.mm',
                        'iw'            => 'HH:mm dd/MM/yyyy',
                        'iw_IL'         => 'HH:mm dd/MM/yyyy',
                        'ja'            => 'yyyy/MM/dd H:mm',
                        'ja_JP'         => 'yyyy/MM/dd H:mm',
                        'kk_KZ'         => 'M/d/yyyy h:mm a',
                        'km_KH'         => 'M/d/yyyy h:mm a',
                        'ko'            => 'yyyy. M. d a h:mm',
                        'ko_KR'         => 'yyyy. M. d a h:mm',
                        'lt_LT'         => 'yyyy.M.d HH.mm',
                        'lv_LV'         => 'yyyy.d.M HH:mm',
                        'ms_MY'         => 'dd/MM/yyyy h:mm a',
                        'nl'            => 'd-M-yyyy H:mm',
                        'nl_BE'         => 'd/MM/yyyy H:mm',
                        'nl_NL'         => 'd-M-yyyy H:mm',
                        'nl_SR'         => 'd-M-yyyy H:mm',
                        'no'            => 'dd.MM.yyyy HH:mm',
                        'no_NO'         => 'dd.MM.yyyy HH:mm',
                        'pl'            => 'yyyy-MM-dd HH:mm',
                        'pt'            => 'dd-MM-yyyy H:mm',
                        'pt_AO'         => 'dd-MM-yyyy H:mm',
                        'pt_BR'         => 'dd/MM/yyyy HH:mm',
                        'pt_PT'         => 'dd-MM-yyyy H:mm',
                        'ro_RO'         => 'dd.MM.yyyy HH:mm',
                        'ru'            => 'dd.MM.yyyy H:mm',
                        'sk_SK'         => 'd.M.yyyy H:mm',
                        'sl_SI'         => 'd.M.y H:mm',
                        'sv'            => 'yyyy-MM-dd HH:mm',
                        'sv_SE'         => 'yyyy-MM-dd HH:mm',
                        'th'            => 'M/d/yyyy h:mm a',
                        'th_TH'         => 'd/M/yyyy, H:mm ?.',
                        'tr'            => 'dd.MM.yyyy HH:mm',
                        'ur_PK'         => 'M/d/yyyy h:mm a',
                        'vi_VN'         => 'HH:mm dd/MM/yyyy',
                        'zh'            => 'yyyy-M-d ah:mm',
                        'zh_CN'         => 'yyyy-M-d ah:mm',
                        'zh_HK'         => 'yyyy-M-d ah:mm',
                        'zh_TW'         => 'yyyy/M/d a h:mm'
                    };
                return mapLocaleToDateTime;
        }
          set;
    }  // PR3 Defect : 28939  end
    
    /*********************************************************************************************
        @Author <Manish Sharma>
        @name <getInputValue>
        @CreateDate <01/31/2017>
        @Description <Method is used to remove the HTML tags and scripts from the input string>  
        @Version <1.0>
    *********************************************************************************************/ 
    public static String getInputValue(String strValue){
        return !String.isEmpty(strValue) ? strValue.stripHtmlTags() : null;
    }
}