/***************************************************************
*  @Author 		Accenture                                      *
*  @Name 		EP_HoldingAssociationHelper			           *
*  @CreateDate  9-Nov-2017                                     *
*  @Description This class is use as Helper class for 
				EP_HoldingAssociationController                *
*  @Version 	1.0                                            *
****************************************************************/
public with sharing class EP_HoldingAssociationHelper {
	
/****************************************************************
* @author       Accenture                                       *
* @name         getDetails                					    *
* @description  constructor of class                            *
* @param        Credit Holding Id				                *
* @return       list<HoldingWrapper>                            *
****************************************************************/
    public list<HoldingWrapper> getDetails(string holdingId){
    	string filterCondition =EP_Common_Constant.BLANK;
    	set<id> setAccounts = new set<id>();
    	map<id,list<order>> mapAccountIdnOrders = new map<id,list<order>>();
    	map<id,list<EP_Invoice__c>> mapOrdernInvoices = new map<id,list<EP_Invoice__c>>();
    	list<HoldingWrapper> lstHoldingWrap = new list<HoldingWrapper>();
    	if(!string.isEmpty(holdingId)){
    		filterCondition = 'where EP_Holding_Name__c='+'\''+ holdingId +'\'';
    	}
    	list <EP_Holding_Association__c> lstHoldAssociation = EP_HoldingAssociationMapper.getAllHoldingAsscRecord(filterCondition);
    	system.debug('getDetails:lstHoldAssociation-->'+lstHoldAssociation);
    	for(EP_Holding_Association__c objHoldingAss :lstHoldAssociation){
    		setAccounts.add(objHoldingAss.EP_Sell_To__c);
    	}
    	if(setAccounts.size()<=0){
    		return lstHoldingWrap;
    	}
		 mapAccountIdnOrders =getOrderRecords(setAccounts);
		 mapOrdernInvoices =getInvoiceRecords(createOrderList(mapAccountIdnOrders));
		 lstHoldingWrap =bindHoldingWrapperValues(lstHoldAssociation,mapAccountIdnOrders,mapOrdernInvoices); 
    	 return lstHoldingWrap; 
    }
    
/********************************************************************
* @author       Accenture                                       	*
* @name         getOrderRecords                						*
* @description  this method used to retrive orders associated to 
				Accounts and retun the mao of ordes per account     *
* @param        set<id>				                				*
* @return       map<id,list<order>>                          		*
********************************************************************/    
    private map<id,list<order>> getOrderRecords(set<id> AccountIdset){
    	map<id,list<order>> mapAccountnOrders = new map<id,list<order>>();
    	for(order objOrder :EP_OrderMapper.getOrdersForAccounts(AccountIdset)){
    		if(!mapAccountnOrders.containsKey(objOrder.AccountId)){
    			mapAccountnOrders.put(objOrder.AccountId, new list<Order>());
    		}
    		mapAccountnOrders.get(objOrder.AccountId).add(objOrder);
    	}
    	return mapAccountnOrders;
    }
    
/********************************************************************
* @author       Accenture                                       	*
* @name         getInvoiceRecords                					*
* @description  this method used to retrive Invoices associated to 
				orders and retun the mao of Invoices per Order      *
* @param        list<order>				                			*
* @return       map<id,list<EP_Invoice__c>>                         *
********************************************************************/  
    private map<id,list<EP_Invoice__c>> getInvoiceRecords(list<order> lstOrders){
    	map<id,list<EP_Invoice__c>> mapOrdernInvoices = new map<id,list<EP_Invoice__c>>();
    	 
    	if(lstOrders.size()>0){
	    	for(EP_Invoice__c objInvoice :EP_InvoiceMapper.getInvoicesForOrders(lstOrders)){
	    		if(!mapOrdernInvoices.containsKey(objInvoice.EP_Order__c)){
	    			mapOrdernInvoices.put(objInvoice.EP_Order__c, new list<EP_Invoice__c>());
	    		}
	    		mapOrdernInvoices.get(objInvoice.EP_Order__c).add(objInvoice);
	    	}
    	}
    	return mapOrdernInvoices;
    }
    
/**********************************************************************
* @author       Accenture                                       	  *
* @name         bindHoldingWrapperValues                			  *
* @description  this method used to create the holding wrapper to 
				bind on the page							          *
* @param        list<EP_Holding_Association__c> ,map<id,list<order>>,
				map<id,list<EP_Invoice__c>>				              *
* @return       list<HoldingWrapper>                         		  *
***********************************************************************/  
    
    private list<HoldingWrapper> bindHoldingWrapperValues(list<EP_Holding_Association__c> lstHoldAsociation,map<id,list<order>> mapAccountOrders ,map<id,list<EP_Invoice__c>> mapOrderInvoices){
    	map<id,HoldingWrapper> mapCreditHolding = new map<id,HoldingWrapper>();
    	map<id,list<CustomerWrapper>> mapHoldingnCustomer= bindMapCustomerWrapper(lstHoldAsociation,mapCreditHolding);
    	list<HoldingWrapper> lstHoldingWrapper = new list<HoldingWrapper>();
    	for(id HoldingId :mapCreditHolding.keyset()){
    		HoldingWrapper objHoldingWrapper = mapCreditHolding.get(HoldingId);
    		objHoldingWrapper.lstCustomerWrapper =bindCustomerWrapper(mapHoldingnCustomer.get(HoldingId),mapAccountOrders,mapOrderInvoices);
    		lstHoldingWrapper.add(objHoldingWrapper);
    	}
    	
    	return lstHoldingWrapper;
    }

/**********************************************************************
* @author       Accenture                                       	  *
* @name         bindCustomerWrapper                			  		  *
* @description  this method used to create the customer wrapper       *
* @param        list<CustomerWrapper> ,map<id,list<order>>,
				map<id,list<EP_Invoice__c>>				              *
* @return       list<CustomerWrapper>                      		      *
***********************************************************************/     
    private list<CustomerWrapper> bindCustomerWrapper(list<CustomerWrapper> CustomerWrapperlst,map<id,list<order>> mapAccountOrders ,map<id,list<EP_Invoice__c>> mapOrderInvoices){
    	list<CustomerWrapper> lstCustomerWrapper = new  list<CustomerWrapper>();
    	for( CustomerWrapper objCustomerWrapper :CustomerWrapperlst){
			objCustomerWrapper.lstOrderWrapper =  bindOrderWrapper(objCustomerWrapper.AccountId,mapAccountOrders,mapOrderInvoices);
		}
		return CustomerWrapperlst;
    }
/**********************************************************************
* @author       Accenture                                       	  *
* @name         bindOrderWrapper                			  		  *
* @description  this method used to create the Order wrapper          *
* @param        string AccountId ,map<id,list<order>> 
				,map<id,list<EP_Invoice__c>>,			              *
* @return       list<OrderWrapper>                      		      *
***********************************************************************/   
    private list<OrderWrapper> bindOrderWrapper(string AccountId ,map<id,list<order>> mapAccountOrders ,map<id,list<EP_Invoice__c>> mapOrderInvoices){
    	list<OrderWrapper> lstOrderWapper = new list<OrderWrapper>();
    	if(mapAccountOrders.containskey(AccountId)){
	    	for(order objOrder :mapAccountOrders.get(AccountId)){
				OrderWrapper objOrderWrapper = new OrderWrapper();
				objOrderWrapper.objOrder =objOrder;
				if(mapOrderInvoices.containskey(objOrder.id)){
					objOrderWrapper.lstInvoice = mapOrderInvoices.get(objOrder.id);
				}
				lstOrderWapper.add(objOrderWrapper);
			}
    	}
    	return lstOrderWapper;
    }

/**********************************************************************
* @author       Accenture                                       	  *
* @name         bindMapCustomerWrapper                			  	  *
* @description  this method used to create the Order wrapper          *
* @param        list<EP_Holding_Association__c>map<id,HoldingWrapper> *
* @return       map<id,list<CustomerWrapper>>                    	  *
***********************************************************************/
    private map<id,list<CustomerWrapper>> bindMapCustomerWrapper(list<EP_Holding_Association__c> lstHoldAsociation,map<id,HoldingWrapper> mapCreditHolding){
    	map<id,list<CustomerWrapper>> mapHoldingnAccounts = new map<id,list<CustomerWrapper>>();
    	for(EP_Holding_Association__c  objHoldAss: lstHoldAsociation){
    		if(!mapHoldingnAccounts.containsKey(objHoldAss.EP_Holding_Name__c)){
    			mapHoldingnAccounts.put(objHoldAss.EP_Holding_Name__c, new list<CustomerWrapper>());
    		}
    		mapCreditHolding.put(objHoldAss.EP_Holding_Name__c,new HoldingWrapper(objHoldAss.EP_Holding_Name__r.Name ,objHoldAss.EP_Holding_Name__r.EP_holding_number__c));
    		CustomerWrapper objCustomerWrapper = new CustomerWrapper();
    		objCustomerWrapper.AccountId = objHoldAss.EP_Sell_To__c  ;
    		objCustomerWrapper.CustomerName = objHoldAss.EP_Sell_To__r.name  ;
    		objCustomerWrapper.CustomerNo = objHoldAss.EP_Sell_To__r.EP_Account_Id__c ;
    		objCustomerWrapper.DisAssociationDate = objHoldAss.EP_Date_of_Dissociation__c ;
    		objCustomerWrapper.AssociationDate = objHoldAss.EP_Date_of_Association__c ;
    		mapHoldingnAccounts.get(objHoldAss.EP_Holding_Name__c).add(objCustomerWrapper);
    	}	
    	return mapHoldingnAccounts;
    
    }
/**********************************************************************
* @author       Accenture                                       	  *
* @name         createOrderList                			  	          *
* @description  this method used to create the Order list from map    *
* @param        map<id,list<order>>									  *
* @return       map<id,list<CustomerWrapper>>                    	  *
***********************************************************************/
    private List<order> createOrderList (map<id,list<order>> mapOrder){
    	list<order> orderList = new list<order>();
		for(id accId :mapOrder.keyset()){
		 	orderList.addAll(mapOrder.get(accId));
		}
		return  orderList;
    
    }

/**********************************************************************
* @author       Accenture                                       	  *
* @name         HoldingWrapper                			  	          *
* @description  this class used as wrapepr class 
				to bind values on Page    							  *
***********************************************************************/    
    public class HoldingWrapper{
    	
    	public string CreditHoldingName 				{get;set;}
    	public string CreditHoldingNumber 				{get;set;}
		public string CreditHoldingId 					{get;set;}
    	public list<CustomerWrapper> lstCustomerWrapper {get;set;}
    	public HoldingWrapper( string cName, string cNumber){
    		this.CreditHoldingName =cName;
    		this.CreditHoldingNumber =cNumber;
    		lstCustomerWrapper = new list<CustomerWrapper>();
    	}
    }
/**********************************************************************
* @author       Accenture                                       	  *
* @name         CustomerWrapper                			  	          *
* @description  this class used as wrapepr class 
				to bind values on Page    							  *
***********************************************************************/    
     public class CustomerWrapper{ 
     	public string AccountId 					{get;set;}   	
    	public string CustomerName 					{get;set;}
    	public string CustomerNo 					{get;set;}
    	public Date AssociationDate 				{get;set;}
    	public Date DisAssociationDate 				{get;set;}
    	public list<OrderWrapper> lstOrderWrapper 	{get;set;}
    	public CustomerWrapper(){
    		lstOrderWrapper= new list<OrderWrapper>();
    	}
    }
/**********************************************************************
* @author       Accenture                                       	  *
* @name         OrderWrapper                			  	          *
* @description  this class used as wrapepr class 
				to bind values on Page    							  *
***********************************************************************/    
     public class OrderWrapper{    	
    	public Order objOrder 					{get;set;}
    	public list<EP_Invoice__c> lstInvoice	{get;set;}
    	public OrderWrapper(){
    		lstInvoice= new list<EP_Invoice__c>();
    	}
    }
    
}