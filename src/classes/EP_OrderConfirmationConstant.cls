/**
*  @Author <Kamal Garg>
*  @Name <EP_OrderConfirmationConstant>
*  @CreateDate <22/08/2016>
*  @Description <This apex class contains Order Confirmation's specific constants>
*  @Version <1.0>
*/
public with sharing class EP_OrderConfirmationConstant {
    
    //Org Constants
    public static final string HEADEROPMS = 'headerOPMS';
    public static final string GENERATEDOCUMENTDETAIL= 'generateDocumentDetail';
    public static final string SOURCESYSTEM = 'sourceSystem';
    public static final string DOCUMENTTYPE = 'documentType';
    public static final string REGIONCODE = 'regionCode';
    public static final string COUNTRYCODE = 'countryCode';
    public static final string CONFIGURATION = 'configuration';
    public static final string LANGAUGEID = 'langaugeId';
    public static final string JURISDICTIONID = 'jurisdictionId';
    public static final string CUSTOMDATA = 'customData';
    public static final string CUSTOMDATA1 = 'customData1';
    public static final string CUSTOMDATA2 = 'customData2';
    public static final string CUSTOMDATA3 = 'customData3';
    public static final string PAYLOAD = 'Payload ';
    public static final string ORDERCONFIRMATIONS = 'orderConfirmations';
    public static final string ORDERCONFIRMATION = 'orderConfirmation';
    public static final string SEQID = 'seqId';
    public static final string COMMONHEADER = 'commonHeader';
    public static final string DOCTITLE = 'docTitle';
    public static final string DOCNR = 'docNr';
    public static final string CURRENCY_TAG = 'currency';
    public static final string DOCDATE = 'docDate';
    public static final string ISSUEDTO = 'issuedTo';
    public static final string ISSUEDTOID = 'issuedToId';
    public static final string NAME = 'name';
    public static final string NAME1 = 'name1';
    public static final string NAME2 = 'name2';
    public static final string NAME3 = 'name3';
    public static final string ADDRESS = 'address';
    public static final string ADDRESSLINE1= 'addressLine1';
    public static final string ADDRESSLINE2 = 'addressLine2';
    public static final string ADDRESSLINE3 = 'addressLine3';
    public static final string CITY = 'city';
    public static final string COUNTRY = 'country';
    public static final string PHONE = 'phone';
    public static final string POSTCODE = 'postCode';
    public static final string ISSUEDFROM = 'issuedFrom';
    public static final string ISSUEDFROMID = 'issuedFromId';
    public static final string EXTRAPARAMETERS = 'extraParameters';
    public static final string EXTRAPARAMETER1 = 'extraParameter1';
    public static final string EXTRAPARAMETER2 = 'extraParameter2';
    public static final string EXTRAPARAMETER3 = 'extraParameter3';
    public static final string EXTRAPARAMETER4 = 'extraParameter4';
    public static final string EXTRAPARAMETER5 = 'extraParameter5';
    public static final string EXTRAPARAMETER6 = 'extraParameter6';
    public static final string EXTRAPARAMETER7 = 'extraParameter7';
    public static final string EXTRAPARAMETER8 = 'extraParameter8';
    public static final string EXTRAPARAMETER9 = 'extraParameter9';
    public static final string EXTRAPARAMETER10 = 'extraParameter10';
    public static final string PHONENR = 'phoneNr';
    public static final string FAXNR = 'faxNr';
    public static final string VATREGNR = 'vatRegNr';
    public static final string EMAIL = 'email';
    public static final string CUSTORDERNR = 'custOrderNr';
    public static final string QUOTATIONNR = 'quotationNr';
    public static final string EXTRNLDOCNR = 'extrnlDocNr';
    public static final string CSCCONTACTNR = 'cscContactNr';
    public static final string STOCKLOADINGADDRESS = 'stockLoadingAddress';
    public static final string STOCKLOADINGID = 'stockLoadingId';
    public static final string SHIPTOADDRESS = 'shipToAddress';
    public static final string SHIPTOID = 'shipToId';
    public static final string SHIPMENTMETHOD = 'shipmentMethod';
    public static final string TRANSPORTERADDRESS = 'transporterAddress';
    public static final string TRANSPORTERID = 'transporterId';
    public static final string PAYMENTTERM = 'paymentTerm';
    public static final string PAYMENTMETHOD = 'paymentMethod';
    public static final string AMOUNTS = 'amounts';
    public static final string AMOUNT = 'amount';
    public static final string SEQNR = 'seqNr';
    public static final string AMOUNTTYPE = 'amountType';
    public static final string PCT = 'pct';
    public static final string LINENR = 'lineNr';
    public static final string APPROVER1 = 'approver1';
    public static final string APPROVER2 = 'approver2';
    public static final string ORDERBY = 'orderBy';
    public static final string ORDERSTATUS = 'orderStatus';
    public static final string LINES = 'lines';
    public static final string LINE = 'line';
    public static final string LINEREFNr = 'lineRefNr';
    public static final string ITEMID = 'itemId';
    public static final string ITEMDSCRPTN = 'itemDscrptn';
    public static final string DSCRPTN = 'dscrptn';
    public static final string VENDORITEMNR = 'vendorItemNr';
    public static final string QTY = 'qty';
    public static final string UOM = 'uom';
    public static final string UNITPRICE = 'unitPrice';
    public static final string LINETAXPCT = 'lineTaxPct';
    public static final string LINETAX = 'lineTax';
    public static final string LINETOTALUNITPRICE = 'lineTotalUnitPrice';
    public static final string LINETOTALPRICE = 'lineTotalPrice';
    public static final string OPMS = 'opms';
    public static final string DOCUMENTTEMPLATEINFO = 'documentTemplateInfo';
    public static final string TEMPLATENAME = 'templateName';
    public static final string ENTITYNAME = 'entityName';
    public static final string ENTITYGROUPID = 'entityGroupId';
    public static final string ENTITYID = 'entityId';
    public static final string COLLATIONINFO = 'collationInfo';
    public static final string PERFORMCOLLATION = 'performCollation';
    public static final string COLLATIONKEY = 'collationKey';
    public static final string EMAILDOCUMENT = 'emailDocument';
    public static final string EMAILBEHAVIOR = 'emailBehavior';
    public static final string EMAILDOCUMENTDETAIL = 'emailDocumentDetail';
    public static final string RETURNADDRESS = 'returnAddress';
    public static final string TOADDRESSES = 'toAddresses';
    public static final string CCADDRESSES = 'ccAddresses';
    public static final string BCCADDRESSES = 'bccAddresses';
    public static final string SUBJECT = 'subject';
    public static final string SAVEDOCUMENT = 'saveDocument';
    public static final string SAVEBEHAVIOR = 'saveBehavior';
    public static final string PRINTDOCUMENT = 'printDocument';
    public static final string PRINTBEHAVIOR = 'printBehavior';
    public static final string PRINTDOCUMENTDETAIL = 'printDocumentDetail';
    public static final string CITYNAME = 'cityName';
    public static final string LOCATIONCODE = 'locationCode';
    
    // Others Constants
    public static final string SFD = 'SFD';
    public static final string GBL = 'GBL';
    public static final string SFDC_STRING = 'SFDC';
    public static final string OPMS_STRING = 'OPMS';
    public static final string SFDC_OPMS = 'SFDC_OPMS';
    public static final string IPASS_OPMS_ORDER_CONFIRMATION = 'IPASS_OPMS_ORDER_CONFIRMATION';
    public static final string ORDER_CONFIRMATION_OPMS = 'Order Confirmation OPMS';
    public static final string ORDER_CONFIRMATION = 'ORDER_CONFIRMATION';
    public static final string ORDER_CONFIRMATION_NAV = 'Order Confirmation Nav';
    public static final string ORDER_CREATE_OBJECT = 'ORDER_CREATE_OBJECT';
    public static final string ONE_STRING = '1';
    public static final string TRANSPORTER = 'Transporter';
    public static final string PRICED = 'PRICED';
    public static final string ORDER_CONFIRMATION_TITLE = 'Order Confirmation';
    public static final string ORDER_QUOTE_TITLE = 'Quote Confirmation';
    public static final string ORDER_TOTAL = 'Order Total';
    
    /**
     *  @Author <Kamal Garg>
     *  @Name <EP_OrderConfirmationConstant>
     *  @CreateDate <22/08/2016>
     *  @Description <This method is used to cover 100% test coverage for this class>
     *  @Version <1.0>
     */
    public EP_OrderConfirmationConstant(){
    	
    }
}