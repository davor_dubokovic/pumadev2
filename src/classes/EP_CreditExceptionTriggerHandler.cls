/* 
  @Author <Accenture>
   @name <EP_CreditExcpTriggerHandler>
   @CreateDate <08-06-2016>
   @Description <This class handles request from EP_Credit_Exception_Request__c trigger> 
   @Version <1.0>
*/
public with sharing class EP_CreditExceptionTriggerHandler {

  //45435 Perform Credit Exception Review --Start
    /**
    * @Author       Accenture
    * @Name         doAfterInsert
    * @Date         19/12/2017
    * @Description  This method handles after insert requests from EP_Credit_Exception_Request__c trigger
    * @Param        List<EP_Credit_Exception_Request__c> , map<id,EP_Credit_Exception_Request__c>
    * @return       NA
    */  
    public static void doAfterInsert(map<id,EP_Credit_Exception_Request__c> mapNewCreditExcep){    
      try {

        EP_CreditExcpTriggerHelper.sendSyncRequestToNAV(mapNewCreditExcep, null ,mapNewCreditExcep.values()[0].EP_Company_Code__c);

      }catch(Exception ex){
        EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doAfterInsert' , 'EP_CreditExceptionTriggerHandler', ApexPages.Severity.ERROR);
      }           
      
    }

    /**
    * @Author       Accenture
    * @Name         doAfterUpdate
    * @Date         19/12/2017
    * @Description  This method handles after update requests from EP_Credit_Exception_Request__c trigger
    * @Param        map<id,EP_Credit_Exception_Request__c>, map<id,EP_Credit_Exception_Request__c>
    * @return       NA
    */   
    public static void doAfterUpdate(map<id,EP_Credit_Exception_Request__c> mapNewCreditExcep
                                        ,map<id,EP_Credit_Exception_Request__c> mapOldCreditExcep){
      try {

        EP_CreditExcpTriggerHelper.sendSyncRequestToNAV(mapNewCreditExcep,mapOldCreditExcep,mapNewCreditExcep.values()[0].EP_Company_Code__c); 
        
      }catch(Exception ex){
        EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doAfterUpdate' , 'EP_CreditExceptionTriggerHandler', ApexPages.Severity.ERROR);
      }
               
    }
  //45435 Perform Credit Exception Review --End  
}