@isTest

Public Class EP_ASTVMIShipToProspectToBasicDataSet_UT
{

    static final string EVENT_NAME = '01-ProspectTo02-BasicDataSetup';
    static final string INVALID_EVENT_NAME = '05-ActiveTo02-Basic Data Setup';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTVMIShipToProspectToBasicDataSetup ast = new EP_ASTVMIShipToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToProspectToBasicDataSetup ast = new EP_ASTVMIShipToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTVMIShipToProspectToBasicDataSetup ast = new EP_ASTVMIShipToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToProspectToBasicDataSetup ast = new EP_ASTVMIShipToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTVMIShipToProspectToBasicDataSetup ast = new EP_ASTVMIShipToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToProspectToBasicDataSetup ast = new EP_ASTVMIShipToProspectToBasicDataSetup();
        ast.setAccountContext(obj,ae);
        String recTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName('EP_Action__c',EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV);
        EP_Action__c pendingAction = new EP_Action__c(EP_Account__c = obj.localAccount.Id, EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS, RecordtypeId = recTypeId);
        insert pendingAction;       
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void doOnExit_test(){
        EP_ASTVMIShipToProspectToBasicDataSetup ast = new EP_ASTVMIShipToProspectToBasicDataSetup();
        ast.doOnExit();
        // No Processing Logic, hence adding a dummy assert.
        system.Assert(true); 
    }

    // #routeSprintWork starts
    // No longer required as guardian is always returning true
    /*static testMethod void isGuardCondition_negative_route_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTNonVMIShipToProspectToBasicData ast = new EP_ASTNonVMIShipToProspectToBasicData();
        ast.setAccountContext(obj,ae);
        
        Account account = obj.localAccount;
        Map<Id,EP_Route__c> routeRec = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c route = routeRec.values()[0];
        route.EP_Company__c = account.EP_Puma_Company__c;
        update route;
        account.EP_Default_Route__c = route.id;
        delete [Select Id FROM EP_Action__c];
        
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }*/
    // #routeSprintWork ends
    }