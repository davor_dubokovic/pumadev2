/* 
  @Author <Spiros Markantonatos>
  @name <EP_ProductSearchComponentController>
  @CreateDate <26/10/2016>
  @Description <This is controller of the re-usable product search component that can be used to allow users to search through products>
  @Version <1.0>
*/
public without sharing class EP_ProductSearchComponentController {
    
    public static String selectedProductNameValue {get;set;}
    public static String selectedProductIDValue {get;set;}
    private static final string CLASS_NAME = 'EP_ProductSearchComponentController';
    private static final string METHOD_GET_PRODUCTS = 'getProducts';
    private static final String strSlaWBrc = '\'';
    private static final String strPrdFam = ' AND Product2.Family = \'';
    private static final String strPrdName = ' ORDER BY Product2.Name';
    private static final String strPbId = ' WHERE Pricebook2Id = \'' ;
    private static final String strPrNmeLike =  ' AND (Product2.Name LIKE \'%';
    private static final String strPrCodeLike= ' OR Product2.ProductCode LIKE \'%';
    private static final String strPer = '%\'';
    private static final String strBrc= ')';
    private static final String strCurrCode = ' AND CurrencyIsoCode = \'' ;
    private static final String strIsAct = ' AND IsActive = TRUE';
    private static final string STR_QUERY1 = 'SELECT ID, Product2Id, Product2.ProductCode, Product2.Name, Product2.EP_Unit_of_Measure__c FROM PricebookEntry';
    /*
      RemoteAction method
    */
    @RemoteAction
    public static List<Product2> getProducts(String strProductListID, 
                                                String strCustomerCurrencyCode, 
                                                    String strProductFamilyValue,
                                                        String strSearchTerm,
                                                            Integer intSearchLimit) {
        List<Product2> lProducts = new List<Product2>();
        
        try{
            if (strProductListID != NULL && strCustomerCurrencyCode != NULL && strSearchTerm != NULL)
            {
                strProductListID = String.escapeSingleQuotes(strProductListID);
                strCustomerCurrencyCode = String.escapeSingleQuotes(strCustomerCurrencyCode);
                strSearchTerm = String.escapeSingleQuotes(strSearchTerm);
                
                String strSQL = STR_QUERY1 ;
                strSQL += strPbId + strProductListID + strSlaWBrc ; //'\'';
                strSQL += strPrNmeLike  + strSearchTerm + strPer ;
                strSQL += strPrCodeLike + strSearchTerm + strPer + strBrc;
                strSQL += strCurrCode  + strCustomerCurrencyCode + strSlaWBrc ; //'\'';
                strSQL += strIsAct ;
                
                if (String.isNotBlank(strProductFamilyValue) && strProductFamilyValue != EP_Common_Constant.BLANK) //removed strProductFamilyValue!=NULL
                {
                    strProductFamilyValue = String.escapeSingleQuotes(strProductFamilyValue);
                    strSQL += strPrdFam  + strProductFamilyValue + strSlaWBrc ;
                }
                
                strSQL += strPrdName;
                
                if (intSearchLimit != NULL)
                    strSQL += EP_Common_Constant.SPACE_STRING + EP_Common_Constant.limitException + EP_Common_Constant.SPACE_STRING + intSearchLimit; //removed ' LIMIT '
                
                System.debug('QUERY ' + strSQL);
                
                List<PricebookEntry> lPricebookEntries = (List<PricebookEntry>)Database.query(strSQL);
                
                for (PricebookEntry pbe : lPricebookEntries)
                {
                    lProducts.add(
                        new Product2(
                            Id = pbe.Product2Id, 
                                ProductCode = pbe.Product2.ProductCode, 
                                    Name = pbe.Product2.Name,
                                        EP_Unit_of_Measure__c = pbe.Product2.EP_Unit_of_Measure__c));
                }
            }
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, METHOD_GET_PRODUCTS, CLASS_NAME,apexPages.severity.ERROR);
        }
        return lProducts;
    }
    
}