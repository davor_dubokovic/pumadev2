/**
   @Author          CR Team
   @name            EP_OrderFieldsetSfdcWindmsMapper
   @CreateDate      12/1/2017
   @Description     This class contains all SOQLs related to EP_OrderFieldsetSfdcWindmsMapper Object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_OrderFieldsetSfdcWindmsMapper{
    private List<EP_Order_Fieldset_Sfdc_Windms_Intg__c> orderIntgList = null;
    
    /**
    *  Constructor. 
    *  @name            EP_OrderFieldsetSfdcWindmsMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */ 
    public EP_OrderFieldsetSfdcWindmsMapper() {
        
    } 
     
    /** This method returns OrderFieldsetSfdcWindim Records.
    *  @name            getOrderFieldsetSfdcWindmsRec
    *  @param           Integer 
    *  @return          list<EP_Order_Fieldset_Sfdc_Windms_Intg__c>
    *  @throws          NA
    */
    public list<EP_Order_Fieldset_Sfdc_Windms_Intg__c> getOrderFieldsetSfdcWindmsRec(Integer nRows) {    
      if(orderIntgList == null) {
          orderIntgList = new List<EP_Order_Fieldset_Sfdc_Windms_Intg__c>();
          for( List<EP_Order_Fieldset_Sfdc_Windms_Intg__c> orderCS : [Select e.Name,
              e.Respective_Tag_Name__c, 
              e.Parent_Tag__c, 
              e.Order_sequence__c, 
              e.Object_Name__c, 
              e.Field_API_Name__c 
              From EP_Order_Fieldset_Sfdc_Windms_Intg__c e 
              ORDER BY Order_sequence__c ASC 
              LIMIT: nRows]){   
              orderIntgList.addAll(orderCS);  
          }  
      }
      return orderIntgList; 
    }   
 }