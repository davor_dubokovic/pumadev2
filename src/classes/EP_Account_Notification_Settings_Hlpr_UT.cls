/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_Settings_Hlpr_UT        *
* @Created Date 2/1/2018                                        *
* @description                                                  *
****************************************************************/
@isTest
private class EP_Account_Notification_Settings_Hlpr_UT {
	private static Account acc;

	@testSetup
	private static void testData(){
		Folder foldr = EP_AccountNotificationTestDataUtility.getFolder(system.label.EP_Folder_Name);
		EP_AccountNotificationTestDataUtility.insertEmailTemplate(foldr.id);
	}

	private static void data(){
		EmailTemplate templateObj = [select id from EmailTemplate where developerName = 'UserTemplate'];
		acc = EP_TestDataUtility.getSellTo();
		Contact con = EP_TestDataUtility.createTestRecordsForContact(acc);
		EP_Notification_Type__c notifyType = EP_AccountNotificationTestDataUtility.createNotificationType(templateObj.id);
		EP_Notification_Account_Settings__c notifyAcc = EP_AccountNotificationTestDataUtility.createAccountNotification(acc.id,templateObj.id,
																														'mohitg12@yahoo.com',con.id,notifyType.id);
	}

	@isTest static void getDefaultAccountNotificationSetting_Positive() {
		data();
		Test.startTest();
		EP_Account_Notification_Settings_Context ctx = new EP_Account_Notification_Settings_Context(acc);
		EP_Account_Notification_Settings_Hlpr helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
		helperObj.getDefaultAccountNotificationSetting();
		Test.stopTest();
		system.assert(!ctx.notificationAccountList.isEmpty());
	}
	
	@isTest static void getDefaultAccountNotificationSetting_Negative() {
		acc = EP_TestDataUtility.getSellTo();
		Test.startTest();
		EP_Account_Notification_Settings_Context ctx = new EP_Account_Notification_Settings_Context(acc);
		EP_Account_Notification_Settings_Hlpr helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
		helperObj.getDefaultAccountNotificationSetting();
		Test.stopTest();
		system.assert(ctx.notificationAccountList.isEmpty());
	}

	@isTest static void setIds_Positive(){
		data();
		Test.startTest();
		EP_Account_Notification_Settings_Context ctx = new EP_Account_Notification_Settings_Context(acc);
		EP_Account_Notification_Settings_Hlpr helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
		helperObj.setIds();
		Test.stopTest();
		system.assert(!ctx.templateContactIdSet.isEmpty());
	}

	@isTest static void setIds_Negative(){
		acc = EP_TestDataUtility.getSellTo();
		Test.startTest();
		EP_Account_Notification_Settings_Context ctx = new EP_Account_Notification_Settings_Context(acc);
		EP_Account_Notification_Settings_Hlpr helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
		helperObj.setIds();
		Test.stopTest();
		system.assert(ctx.templateContactIdSet.isEmpty());
	}

	@isTest static void setMapData_Positive(){
		data();
		Test.startTest();
		EP_Account_Notification_Settings_Context ctx = new EP_Account_Notification_Settings_Context(acc);
		EP_Account_Notification_Settings_Hlpr helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
		helperObj.setMapData();
		Test.stopTest();
		system.assert(!ctx.mapIdName.isEmpty());
	}

	@isTest static void setMapData_Negative(){
		acc = EP_TestDataUtility.getSellTo();
		Test.startTest();
		EP_Account_Notification_Settings_Context ctx = new EP_Account_Notification_Settings_Context(acc);
		EP_Account_Notification_Settings_Hlpr helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
		helperObj.setMapData();
		Test.stopTest();
		system.assert(ctx.mapIdName.isEmpty());
	}

	@isTest static void save_Positive(){
		data();
		Test.startTest();
		EP_Account_Notification_Settings_Context ctx = new EP_Account_Notification_Settings_Context(acc);
		EP_Account_Notification_Settings_Hlpr helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
		helperObj.save();
		Test.stopTest();
		system.assert(true);
	}
	
}