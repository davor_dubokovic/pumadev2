/* 
  @Author <Spiros Markantonatos>
   @name <EP_TankDipSubmitPageControllerClass_R1_Test>
   @CreateDate <06/11/2014>
   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
   @Version <1.0>
 
*/
@isTest
private class EP_TankDipSubmitPageControllerCl_R1_Test {
    private static Account testShipToAccount; 
    private static EP_Tank_Dip__c tankDip; 
    private static EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=null;
    
    private static final String RET_PARAM_NAME = 'ret';
    
    private static User testCSCUser {
        get {
            if (testCSCUser == NULL)
            {
                Profile cscAgent = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.CSC_AGENT_PROFILE LIMIT 1];
        testCSCUser = EP_TestDataUtility.createUser(cscAgent.Id);
            }
            
            return testCSCUser;
        }
        set;
    }
    
    /**************************************************************************
    *@Description : This method is used to check the functionality to fire a 
                    validation rule
                    if user tries to enter future date as reading date.         *
    *@Params  : none                                                            *
    *@Return  : void                                                            *
    **************************************************************************/
    private static testMethod void validateDateTimeTankDip(){
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
      
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        
        Test.startTest();
            System.runAs(testCSCUser) {
                // Insert an account
                createTestAccount(strRecordTypeId);
               // Database.insert(testShipToAccount,false);
               insert testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                testShipToAccount.EP_Is_Valid_Address__c = true;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
               // Database.update(testShipToAccount, FALSE);
               update testShipToAccount;
               EP_Tank__c tankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.id, EP_Product__c = prod.id,EP_Safe_Fill_Level__c = 2500); 
            tankObj.EP_Tank_Status__c = 'New';
            //Database.insert(tankObj, FALSE);
             insert tankObj;
             tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
             update tankObj;
             tankObj.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
             tankObj.EP_Tank_Dip_Entry_Mode__c='Automatic Dip Entry';
             update tankObj;
           
            List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{
            new EP_Tank_Dip__c(EP_Tank__c=tankObj.id)},tankObj);
            tankList[0].EP_Ambient_Quantity__c=10;
            tankList[0].EP_Reading_Date_Time__c=System.now()-2;
           // Database.update(tankList[0],false);
             update tankList[0];
            }
        Test.stopTest();
    }

    /**************************************************************************
    *@Description : This method is used to check the functionality to fire a validation rule
    if user tries to  enter a reading that exceeds the safe fill level of the tank.  *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/
    private static testMethod void exceedsSafeFillLevel(){ 
    
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
            System.runAs(testCSCUser) {
                // Inserting an account
                createTestAccount(strRecordTypeId);
               // Database.insert(testShipToAccount, FALSE);
                insert testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                testShipToAccount.EP_Is_Valid_Address__c = true;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
               // Database.update(testShipToAccount, FALSE);
               update testShipToAccount;
                EP_Tank__c tankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.id, EP_Product__c = prod.Id,
                EP_Safe_Fill_Level__c = 2500,EP_Capacity__c=5000); 
                tankObj.EP_Tank_Status__c = 'New';
               // Database.insert(tankObj, FALSE);
                insert tankObj;  
              
                    testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                   // Database.update(testShipToAccount, FALSE);
                     update testShipToAccount;
                    tankObj.EP_Capacity__c = 6000;
                   // Database.update(tankObj, FALSE);
                    update tankObj;
                
            }
        Test.stopTest();
    }

    /**************************************************************************
    *@Description : This method is used to check the functionality to fire a validation rule
    if user tries to  enter a reading that is less than the tank deadstock level.  *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/   
    private static testMethod void cannotEnteReadingLessThanDeadLock(){
    
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
        EP_Common_Constant.NON_VMI_SHIP_TO);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        
        System.runAs(testCSCUser) {
            // Inserting an account
            createTestAccount(strRecordTypeId);
           // Database.insert(testShipToAccount, FALSE);
           insert testShipToAccount;
            testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
            update testShipToAccount;
            testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            testShipToAccount.EP_Is_Valid_Address__c = true;
            update testShipToAccount;
            testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
          //  Database.update(testShipToAccount, FALSE);
              update testShipToAccount;
            EP_Tank__c tankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.id, EP_Product__c = prod.id,EP_Safe_Fill_Level__c = 2500); 
            tankObj.EP_Tank_Status__c = 'New';
            //Database.insert(tankObj, FALSE);
             insert tankObj;
             tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
             update tankObj;
             tankObj.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
             tankObj.EP_Tank_Dip_Entry_Mode__c='Automatic Dip Entry';
             update tankObj;
           
            List<EP_Tank_Dip__c> tankList=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{
            new EP_Tank_Dip__c(EP_Tank__c=tankObj.id)},tankObj);
            tankList[0].EP_Ambient_Quantity__c=10;
            tankList[0].EP_Reading_Date_Time__c=System.now()-2;
           // Database.update(tankList[0],false);
             update tankList[0];
            Test.stopTest();
        }
    }

    /**************************************************************************
    *@Description : This method is used to check the functionality to fire a validation rule
    if user tries to enter a negative reading'.  *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/
     private static testMethod void cannotEnterNegativeReading(){ 
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
                                                                    EP_Common_Constant.NON_VMI_SHIP_TO);
        
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
            System.runAs(testCSCUser) {
                // Inserting an account
                createTestAccount(strRecordTypeId);
               // Database.insert(testShipToAccount, FALSE);
               insert testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                testShipToAccount.EP_Is_Valid_Address__c = true;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
               // Database.update(testShipToAccount, FALSE);
               update testShipToAccount;
                EP_Tank__c tankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.Id, EP_Product__c = prod.Id, EP_Safe_Fill_Level__c = 2500); 
                tankObj.EP_Tank_Status__c = EP_Common_Constant.NEW_TANK_STATUS;
              //Database.insert(tankObj, FALSE);
               insert tankObj; 
               tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
               update tankObj;
               tankObj.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
               tankObj.EP_Tank_Dip_Entry_Mode__c='Automatic Dip Entry';
               update tankObj;
                
              List<EP_Tank_Dip__c> tankList = EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{
                                                                                            new EP_Tank_Dip__c(EP_Tank__c = tankObj.Id)}, tankObj);
              tankList[0].EP_Ambient_Quantity__c = 31;
           // Database.update(tankList[0], FALSE);
              system.debug('--tankList.size()--'+tankList.size());
              update tankList[0];
              Test.stopTest();
              } 
       }
    
    /*************************************************************************
    *@Description : This method will validate that the controller is able       *
                    to handle the different user/site time zone offset cases    *
    *@Return  : void                                                            *
    **************************************************************************/
    private static testMethod void checkTimeZoneOffsetCases(){
        
        // Fix for Defect 29809 - Start
        
        // Insert a test account structure
        EP_TestDataUtility.WrapperCustomerHierarchy testAccountStructure = EP_TestDataUtility.createCustomerHierarchyForNAV(2);
        
        testShipToAccount = testAccountStructure.lShipToAccounts[0];
        
        PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('id', testShipToAccount.Id);
        ApexPages.currentPage().getParameters().put('Date', '20181015051719');
        ApexPages.currentPage().getParameters().put('pageNumber', '1');
        EP_TankDipSubmitPageControllerClass_R1 tankDipPageController = new EP_TankDipSubmitPageControllerClass_R1();
        
        tankDipPageController.dblUserOffset = 1;
        tankDipPageController.dblSiteOffset = 2;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(1, tankDipPageController.dblOffset);
        
        tankDipPageController.dblUserOffset = 1;
        tankDipPageController.dblSiteOffset = 1;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(0, tankDipPageController.dblOffset);
        
        tankDipPageController.dblUserOffset = 2;
        tankDipPageController.dblSiteOffset = 1;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(1, tankDipPageController.dblOffset);
        
        tankDipPageController.dblUserOffset = -1;
        tankDipPageController.dblSiteOffset = 1;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(-2, tankDipPageController.dblOffset);
        
        tankDipPageController.dblUserOffset = 1;
        tankDipPageController.dblSiteOffset = -1;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(-2, tankDipPageController.dblOffset);
        
        tankDipPageController.dblUserOffset = -1;
        tankDipPageController.dblSiteOffset = -2;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(1, tankDipPageController.dblOffset);
        
        tankDipPageController.dblUserOffset = -2;
        tankDipPageController.dblSiteOffset = -1;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(1, tankDipPageController.dblOffset);
        
        tankDipPageController.dblUserOffset = -2;
        tankDipPageController.dblSiteOffset = -2;
        tankDipPageController.initialiseTankDipData();
        System.assertEquals(0, tankDipPageController.dblOffset);
        
        // Fix for Defect 29809 - End
    }
    
    /*************************************************************************
    *@Description : This method will validate whethere the VF page
                    (EP_TankDipSubmitPage_R1) showing   
                    operatonal tanks which are in active or not.
    *@Return  : void                                                          *
    **************************************************************************/
    private static testMethod void checkOperationaLTanks(){
        
        // Fix for Defect 29809 - Start
        
        // Insert a test account structure
        EP_TestDataUtility.WrapperCustomerHierarchy testAccountStructure = EP_TestDataUtility.createCustomerHierarchyForNAV(2);
        
        // Change ship-to status to active
        for (Account a : testAccountStructure.lShipToAccounts)
        {
            a.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        }
        Database.update(testAccountStructure.lShipToAccounts);
        
        testShipToAccount = testAccountStructure.lShipToAccounts[0];
        testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
       // Database.update(testShipToAccount);
       update testShipToAccount;
        
        // Change tank status to basic setup
        for (EP_Tank__c t : testAccountStructure.lTanks)
        {
            t.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        }
       // Database.update(testAccountStructure.lTanks);
        update testAccountStructure.lTanks;
        
        // Change tank status to operational
        for (EP_Tank__c t : testAccountStructure.lTanks)
        {
            t.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
        }
       // Database.update(testAccountStructure.lTanks);
        update testAccountStructure.lTanks;
        
        //List<EP_Tank_Dip__c> tankList = EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c>{
        //                                                                          new EP_Tank_Dip__c(EP_Tank__c = tankObj.Id)}, tankObj);
        
        PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('id', testShipToAccount.Id);
        ApexPages.currentPage().getParameters().put('Date', '20181015051719');
        ApexPages.currentPage().getParameters().put('pageNumber', '1');
        EP_TankDipSubmitPageControllerClass_R1 tankDipPageController = new EP_TankDipSubmitPageControllerClass_R1();
        
        tankDipPageController.intUserSelectedHour = 1;
        tankDipPageController.intUserSelectedMinute = 0;
        
        System.assertEquals(1, tankDipPageController.intUserSelectedHour);
        System.assertEquals(0, tankDipPageController.intUserSelectedMinute);
        
        tankDipPageController.initialiseTankDipData();
        
        // Select the tanks
        tankDipPageController.strSelectedTanksIDs = EP_Common_Constant.SEMICOLON;
        
        for (EP_Tank__c t : testAccountStructure.lTanks)
        {
            tankDipPageController.strSelectedTanksIDs += string.valueOf(t.Id) + EP_Common_Constant.SEMICOLON;
        }
        
        System.debug('SELECT OUT ' + tankDipPageController.strSelectedTanksIDs);
        
        System.assertEquals(testShipToAccount.Name, tankDipPageController.shipToName);
        System.assertEquals(1, tankDipPageController.intPageNumber);
        
        tankDipPageController.updateTankDipReadingTime();
        
        // Set dip value
        for (EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass td : tankDipPageController.newTankDipRecords)
        {
            td.tankDip.EP_Ambient_Quantity__c = 100;
        }
        
        tankDipPageController.saveStockRecords();
        
        tankDipPageController.next();
               // tankDipPageController.blnRedirectToAccountPage();

         tankDipPageController.back(); // Added new
         tankDipPageController.toggleSiteInputControls() ;
        tankDipPageController.cancel();
                
        // Fix for Defect 29809 - End
        
    }

    /**************************************************************************
    *@Description : This method is used to check the functionality of backbutton.  *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/ 
    private static testMethod void checkBackButtonFunctionality(){
    
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
                                                                    EP_Common_Constant.NON_VMI_SHIP_TO);
        // Insert a product
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        
        Test.startTest();
            System.runAs(testCSCUser) {
                // Insert an account
                createTestAccount(strRecordTypeId);
               // Database.insert(testShipToAccount, FALSE);
                insert testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                testShipToAccount.EP_Is_Valid_Address__c = true;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
               // Database.update(testShipToAccount, FALSE);
               update testShipToAccount;
                EP_Tank__c tankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.Id, EP_Product__c = prod.Id, EP_Safe_Fill_Level__c = 2500); 
                tankObj.EP_Tank_Status__c = EP_Common_Constant.NEW_TANK_STATUS;
               // Database.insert(tankObj, FALSE);
               insert tankObj; 
                tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
                 update tankObj;
                 tankObj.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
                 tankObj.EP_Tank_Dip_Entry_Mode__c='Automatic Dip Entry';
                 update tankObj;
                   
                PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
                Test.setCurrentPage(pageRef);//Applying page context here  
                ApexPages.currentPage().getParameters().put('id', testShipToAccount.Id);
                ApexPages.currentPage().getParameters().put('Date', '20181015051719');
                ApexPages.currentPage().getParameters().put('pageNumber', '1');
                ApexPages.currentPage().getParameters().put(RET_PARAM_NAME, EP_Common_Constant.ACCOUNT_OBJ);
                
                EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
                Boolean b1 = tankDippageObj.blnShipToHasMissingOlderTankDips;
                Boolean b2 = tankDippageObj.blnShipToIsMissingTankDipsForToday;
                
                EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass tankDipInnerClass = new EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass();
                tankDippageObj.saveStockRecords();
                tankDippageObj.cancel();
                tankDippageObj.next();
                tankDippageObj.updateTankDipReadingTime();
                PageReference pgRef = tankDippageObj.back();
                tankDippageObj.strStepIndex = '2';
                PageReference pgRef1 = tankDippageObj.back();
                Boolean b3 = tankDippageObj.blnRedirectToAccountPage;
                
                tankDippageObj.toggleSiteInputControls();
                Time readingTime = tankDipInnerClass.ReadingTime;
                Integer hour = tankDipInnerClass.intHour;
                Integer minute = tankDipInnerClass.intMinute;
                String strShipToID = testShipToAccount.Id;
                strShipToID = strShipToID.toUpperCase();
                System.assertEquals(TRUE, pgRef.getUrl().toUpperCase().contains(strShipToID));
                System.assertEquals(TRUE, b3);
            }
        Test.stopTest();
    } 
    
    /**************************************************************************
    *@Description : This method is used to ensure that the controller 
                        is handling invalid dates                             *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/ 
    private static testMethod void checkInvalidDateHandlingFunctionality(){
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
                                                                    EP_Common_Constant.NON_VMI_SHIP_TO);
                                                                    
        createTestAccount(strRecordTypeId);
       // Database.insert(testShipToAccount, FALSE);
        insert testShipToAccount;
        
        PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', testShipToAccount.Id);
        ApexPages.currentPage().getParameters().put('Date', '20181aa5051719'); // Add invalid date
        
        EP_TankDipSubmitPageControllerClass_R1 tankDipPageController = new EP_TankDipSubmitPageControllerClass_R1();
        System.assertEquals(System.Today(), tankDipPageController.dSelectedDate);
    }
    
    /**************************************************************************
    *@Description : This method is used to validate that CSC is able to create tank for Non-VMI Ship to Account for a CSC User.  *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/ 
    private static testMethod void checkCreateTankForNonVMIshipToAccount(){
    
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
                                                                    EP_Common_Constant.NON_VMI_SHIP_TO);
        // Insert a product
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
            System.runAs(testCSCUser) {
                
                // Insert an account
                createTestAccount(strRecordTypeId);
                
              //  Database.insert(testShipToAccount, FALSE);
                 insert testShipToAccount;
                EP_Tank__c TankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.Id, EP_Product__c = prod.Id, EP_Safe_Fill_Level__c = 2500);  
                TankObj.EP_Tank_Status__c = 'New';
              //  Database.insert(TankObj, FALSE);
               insert TankObj;
                system.assertEquals(TankObj.EP_Tank_Status__c, 'New');
            }
        Test.stopTest();
    }

    /**************************************************************************
    *@Description : This method is used to update the ship-to Account tank details created by CSC user.  *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/ 
    private static testMethod void updateShipToAccountTankDetails(){
        //Instantiating the Utility class
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
                                                                    EP_Common_Constant.NON_VMI_SHIP_TO);
        // Inserting an account
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
            System.runAs(testCSCUser) {
                // Inserting an account
                createTestAccount(strRecordTypeId);
               // Database.insert(testShipToAccount, FALSE);
               // Database.insert(testShipToAccount, FALSE);
                insert testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                testShipToAccount.EP_Is_Valid_Address__c = true;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
               // Database.update(testShipToAccount, FALSE);
               update testShipToAccount;
                EP_Tank__c TankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.id, EP_Product__c = prod.Id, EP_Safe_Fill_Level__c = 2500); 
                TankObj.EP_Tank_Status__c = 'New';
               // Database.insert(TankObj, FALSE);
                insert TankObj;
                EP_Tank_Dip__c tankDip1 = EP_PortalLibClass_R1.createPlaceholderTankDipRecord(TankObj);
               tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
               update tankObj;
               tankObj.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
               update tankObj;
                // Update the tank record to be 'Decomissioned'
                TankObj.EP_Tank_Status__c = 'Decomissioned';
               // Database.update(TankObj, FALSE);
                update TankObj;
                
                system.assertEquals(TankObj.EP_Tank_Status__c, 'Decomissioned');
            }
        Test.stopTest();
    }

    /**************************************************************************
    *@Description : This method validates tank cannot be deleted by CSC user.  *
    *@Params  : none  *
    *@Return  : void  *
    **************************************************************************/ 
    private static testmethod void noTanksToBeDeleted(){
        EP_Tank__c TankObj;
        
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = TRUE;
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
        EP_Common_Constant.NON_VMI_SHIP_TO);
        Product2 testProduct = EP_TestDataUtility.createTestRecordsForProduct();
        EP_TestDataUtility.WrapperCustomerHierarchy accountHierarchy = EP_TestDataUtility.createCustomerHierarchyForNAV(2);
        Test.startTest();
            System.runAs(testCSCUser) {
                // Inserting an account
                createTestAccount(strRecordTypeId);
                insert testShipToAccount;// = [Select Id, EP_Status__c, EP_Tank_Dips_Schedule_Time__c  from Account where recordTypeId = :EP_Common_Util.fetchRecordTypeId('Account' ,EP_Common_Constant.VMI_SHIP_TO) limit 1];
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
                testShipToAccount.EP_Is_Valid_Address__c = true;
                update testShipToAccount;
                TankObj = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.Id, 
                                                    EP_Product__c = testProduct.Id, 
                                                        EP_Safe_Fill_Level__c = 2500, 
                                                            EP_Tank_Dip_Entry_Mode__c = EP_Common_Constant.TANK_DIP_ENTRY_MODE);  
                TankObj.EP_Tank_Status__c = EP_Common_Constant.NEW_TANK_STATUS;
                
             
                   // Database.insert(TankObj);
                   insert TankObj;
                    
                    testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                    testShipToAccount.EP_Tank_Dips_Schedule_Time__c = '09:00';
                    Database.update(testShipToAccount);
                    
                    TankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
                   // Database.update(TankObj);
                    update TankObj;
                    
                    TankObj.EP_Tank_Status__c = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
                   // Database.update(TankObj);
                    update TankObj;
                    // Delete the tank object
                   // database.delete(TankObj); 
                    //delete TankObj;
               
            }
        Test.stopTest();
    }
    
    /**************************************************************************
    *@Description : This method creates test Account record for CSC user.  *
    *@Params  : strRecordTypeId  *
    *@Return  : Account  *
    **************************************************************************/
    private static Account createTestAccount(Id strRecordTypeId) {
        testShipToAccount = new Account(Name='ZISCHKE FUEL ARATULA'
                                     ,EP_Ship_To_UTC_Timezone__c='UTC+00:00'
                                     ,RecordTypeId= strRecordTypeId
                                     ,ShippingCity = 'testCity'
                                     ,ShippingStreet = 'testStreet'
                                     ,ShippingState = 'testState'
                                     ,ShippingCountry = 'testCountry'
                                     ,ShippingPostalCode = '123456'
                                     ,phone = '9898989898'
                                     ,EP_Email__c = 'abc@test.xyz'
                                     ,EP_Tank_Dips_Schedule_Time__c = '10:00');
         
         return testShipToAccount;
    }
 
    /**************************************************************************
    *@Description : This method creates test Account record for CSC user.   *
    *@Params  : strRecordTypeId *
    *@Return  : Account *
    **************************************************************************/
    private static testMethod void checkNextButtonFunctionality(){
    
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,
        EP_Common_Constant.NON_VMI_SHIP_TO);
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        
        Test.startTest();
            System.runAs(testCSCUser) {
                // Insert an account
                createTestAccount(strRecordTypeId);
               // Database.insert(testShipToAccount, FALSE);
                insert testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
                testShipToAccount.EP_Is_Valid_Address__c = true;
                update testShipToAccount;
                testShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
               // Database.update(testShipToAccount, FALSE);
               update testShipToAccount;
                
                List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
                EP_Tank__c tankObj= new EP_Tank__c(EP_Ship_To__c = testShipToAccount.Id, EP_Product__c = prod.id,EP_Safe_Fill_Level__c = 2500); 
                tankObj.EP_Tank_Status__c = 'New';
                tankObj.EP_Tank_Code__c = '234';
                tanksToBeInsertedList.add(tankObj);
                
                EP_Tank__c tankObj1 = new EP_Tank__c(EP_Ship_To__c = testShipToAccount.Id, EP_Product__c = prod.Id, EP_Safe_Fill_Level__c = 3500); 
                tankObj1.EP_Tank_Status__c = 'New';
                tankObj.EP_Tank_Code__c = '345';
                tanksToBeInsertedList.add(tankObj1);
               // Database.insert(tanksToBeInsertedList,false);
                insert tanksToBeInsertedList;
                 
                PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
                Test.setCurrentPage(pageRef);//Applying page context here  
                ApexPages.currentPage().getParameters().put('id', testShipToAccount.Id);
                ApexPages.currentPage().getParameters().put('Date', '20181015051719');
                EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
                tankDippageObj.strStepIndex = '3';
                EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass tankDipblnAddTankDip = new EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass();
                tankDipblnAddTankDip.blnAddTankDip = TRUE;
                tankDippageObj.newTankDipRecords.add(tankDipblnAddTankDip);
                tankDippageObj.next();
                tankDippageObj.strStepIndex = '5';
                tankDippageObj.next();
                tankDippageObj.strStepIndex = '5';
                tankDippageObj.saveStockRecords();
                //****System.assertEquals(2, tankDippageObj.tanks.size());
            }
        Test.stopTest();
    }

}