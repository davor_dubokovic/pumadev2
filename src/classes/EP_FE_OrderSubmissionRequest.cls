/*********************************************************************************************
     @Author <>
     @name <EP_FE_OrderSubmissionRequest>
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
global with sharing class EP_FE_OrderSubmissionRequest{

    // Error list
    global static final Integer ERROR_MISSING_ORDERID = -1;
    global static final Integer ERROR_MISSING_REQUEST = -2;
    global static final String DESCRIPTION1 = 'Missing Request';
    global static final String DESCRIPTION2 = 'Missing OrderId/OrderId not found';

    public Id draftOrderId;
    public String truckPlate {get; set;}  
    public String customerReferenceNumber {get; set;}
    public String comments {get; set;}  
}