/*
  @Author <Accenture>
   @name <EP_TankDipTriggerHelper_R1>
   @Description <This class handles request from EP_TankDipTriggerHandler_R1> 
   @Version <1.0>
*/
public without sharing class EP_TankDipTriggerHelper_R1 {
    
    private static final string DATE_FORMAT = 'yyyyMMdd';
    private static final string PLACEHOLDER_VALUE = 'Placeholder';
    private static final string NUMERIC_VALUE = '100000';
    private static final string TANK_DIP_TRIGGER_HELPER = 'EP_TankDipTriggerHelper_R1';
    private static final string ADD_TANK_STATUS = 'addTankStatus';
    private static final string VAL_TDIPS_AGAINST_DAILY_EXISTDIPS = 'validateTankDipsAgainstsExistingDailyDips';
    private static final string REM_PLCHLDER_TANK_DIP_FROM_SYSTEM = 'removePlaceholderTankDipsFromSystem';
    private static final string UPDATE_USER_UTC_OFFSET = 'updateUserUTCOffsetOnReadingTimeChange';
    private static final string UPDATE_IS_EXPORTED = 'updateIsExported';
   
    /*
    @Description: Add tank status
    */
    public static void addTankStatus(List<EP_Tank_Dip__c> tankDips) {
        List<Id> tankIds = new List<Id>();
        Map<Id,String> tankStatusMap = new Map<Id,String>();
        List<EP_Tank_Dip__c> processDips = new List<EP_Tank_Dip__c>();
        Integer queryRows = EP_Common_Util.getQueryLimit();
        try{
            for (EP_Tank_Dip__c dip : tankDips){
                if (dip.EP_Tank_Status__c == null || dip.EP_Tank_Status__c.equals(EP_Common_Constant.BLANK)){
                    tankIds.add(dip.EP_Tank__c);
                    processDips.add(dip);
                }
            }
            
            if (null != processDips && processDips.size() > 0){
                queryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                for (EP_Tank__c tank :[SELECT Id,EP_Tank_Status__c FROM EP_Tank__c WHERE Id IN :tankIds limit :queryRows]){
                    tankStatusMap.put(tank.Id,tank.EP_Tank_Status__c);
                }
            
                for(EP_Tank_Dip__c dip : processDips){
                    dip.EP_Tank_Status__c = tankStatusMap.get(dip.EP_Tank__c);
                }
            }
       }
       catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                ADD_TANK_STATUS, TANK_DIP_TRIGGER_HELPER, ApexPages.Severity.ERROR);
       }    
        
    }
    
    /*
    @Description: Validate Tank Dips Against Exsisting Daily Dips
    */
    public static void validateTankDipsAgainstsExistingDailyDips(List<EP_Tank_Dip__c> tankDips) {
        Map<ID, List<EP_Tank_Dip__c>> existingTankDipsMap = new Map<ID, List<EP_Tank_Dip__c>>();
        Set<ID> tankIDSet = new Set<ID>();
        Set<ID> tankDipIDSet = new Set<ID>();
        List<EP_Tank_Dip__c> emptyTankDipList = new List<EP_Tank_Dip__c>();
        Integer queryRows = EP_Common_Util.getQueryLimit();
        Boolean blnProceedWithValidation = FALSE;
        Date dMin;
        Date dMax;
        String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(PLACEHOLDER_VALUE).getRecordTypeId();
        try{
            // Build the tank dip set and determine the min and max reading date/time
            for (EP_Tank_Dip__c t : tankDips) {
                if (t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c != NULL) {
                    // Populate the sort reading date/time field which is used for the quick entry URL
                
                    t.EP_Tank_Dip_Sort_Reading_Date_Time__c = t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c.format(DATE_FORMAT) + NUMERIC_VALUE;
                
                    if (t.RecordTypeId != recType) {
                        // Fix for Defect 38939 - Start
                        tankIDSet.add(t.EP_Tank__c);
                        if (t.ID != NULL){
                             tankDipIDSet.add(t.ID);
                        }
                        // Fix for Defect 38939 - End
                        if (dMin == NULL){
                         dMin = EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c);
                        }
                        if (dMax == NULL){
                         dMax = EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c);
                        }
                        blnProceedWithValidation = TRUE;
                        
                        if (EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c) < dMin){
                         dMin = EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c);
                        }
                        if (EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c) > dMax){
                         dMax = EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c);
                        }
                    } // End tank dip date/time check
                } // End placeholder tank dip check
            } // End for
    
            
            //System.debug('MIN ' + dMin);
            //System.debug('MAX ' + dMax);
            
            
            
            // Only proceed with the validation if there are tank dips in the system
            if (blnProceedWithValidation) {        
                // Retrieve all existing tank dips for the related tanks entered for the same day
                // ordered by reading date in DESC order
                // Initialise the existing tank dip map
                for (EP_Tank_Dip__c t : [SELECT ID, Name, EP_Tank_Dip_Reading_Ship_To_Date_Time__c, EP_Tank__c 
                                                            FROM EP_Tank_Dip__c 
                                                                WHERE EP_Tank__c IN :tankIDSet
                                                                    AND ID NOT IN :tankDipIDSet
                                                                        AND EP_Tank_Dip_Reading_Ship_To_Date_Time__c > :dMin.addDays(-2)
                                                                            AND EP_Tank_Dip_Reading_Ship_To_Date_Time__c <= :dMax.addDays(1)
                                                                                AND RecordTypeId !=: recType
                                                                                    ORDER BY EP_Tank_Dip_Reading_Ship_To_Date_Time__c DESC limit :queryRows]) {
                    // Initialise the new tank dip map
                    if (!existingTankDipsMap.containsKey(t.EP_Tank__c)){
                        existingTankDipsMap.put(t.EP_Tank__c, emptyTankDipList); //new List<EP_Tank_Dip__c>());
                    }
                    existingTankDipsMap.get(t.EP_Tank__c).add(t);
                } // End for
                
                // Validate the new tank dips. Users are not allowed to enter a tank dip for a tank with a dip for a later time at the same day
                for (EP_Tank_Dip__c t : tankDips) {
                    // By default reset the error flag to FALSE
                    t.EP_Tank_Dip_Reading_Date_Time_Error__c = FALSE;
                    
                    
                    if (t.RecordTypeId != recType) {
                        if (existingTankDipsMap.containsKey(t.EP_Tank__c)) {
                            for (EP_Tank_Dip__c et : existingTankDipsMap.get(t.EP_Tank__c)) {
                                if (et.ID != t.ID) {
                                    //system.debug('EXI D ' + et.Name + ' ' + EP_PortalLibClass_R1.returnLocalDate(et.EP_Tank_Dip_Reading_Ship_To_Date_Time__c));
                                    //system.debug('NEW D ' + t.Name + ' ' + EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c));
                                    //system.debug('EXI DT ' + et.Name + ' ' + EP_PortalLibClass_R1.returnLocalDateTime(et.EP_Tank_Dip_Reading_Ship_To_Date_Time__c));
                                    //system.debug('NEW DT ' + t.Name + ' ' + EP_PortalLibClass_R1.returnLocalDateTime(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c));
                                    if (EP_PortalLibClass_R1.returnLocalDate(et.EP_Tank_Dip_Reading_Ship_To_Date_Time__c) == EP_PortalLibClass_R1.returnLocalDate(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c) &&
                                            EP_PortalLibClass_R1.returnLocalDateTime(et.EP_Tank_Dip_Reading_Ship_To_Date_Time__c) >= EP_PortalLibClass_R1.returnLocalDateTime(t.EP_Tank_Dip_Reading_Ship_To_Date_Time__c)) {
                                        //System.debug('SET ERROR');
                                        t.EP_Tank_Dip_Reading_Date_Time_Error__c = TRUE;
                                        // A validation rule will kick in
                                        break;
                                    } // End date/time validation
                                } // End same record validation check
                            } // End existing tank dip for
                        } // End existing tank dip check
                    } // End placeholder tank dip check
                } // End new tank dip for
            } // End proceed with validation check
       }
       catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                VAL_TDIPS_AGAINST_DAILY_EXISTDIPS, TANK_DIP_TRIGGER_HELPER, ApexPages.Severity.ERROR);
       } 
    }
    
    /*
    @Description: Remove Placeholder TankDips from System
    */
    public static void removePlaceholderTankDipsFromSystem(List<EP_Tank_Dip__c> tankDips) {
        Set<ID> tankIDSet = new Set<ID>();
        List<EP_Tank_Dip__c> deletedPlaceholderTankDips = new List<EP_Tank_Dip__c>();
        Map<String, String> tankDipMap = new Map<String, String>();
        String strKey;
        String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(PLACEHOLDER_VALUE).getRecordTypeId();
        Integer queryRows = EP_Common_Util.getQueryLimit();
        EP_Tank_Dip__c epTankDip;
        try{
        // Fix for Defect 29809 - Start
        
        // Build the tank list
        for (EP_Tank_Dip__c tankDip : tankDips) {
            if (tankDip.EP_Tank_Dip_Reading_Ship_To_Date_Time__c != NULL && 
                                                            tankDip.EP_Tank__c != NULL && 
                                                                tankDip.RecordTypeId != recType) {
                tankIDSet.add(tankDip.EP_Tank__c);
                strKey = tankDip.EP_Tank__c + EP_Common_Constant.UNDERSCORE_STRING + tankDip.EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c.format(DATE_FORMAT);
                tankDipMap.put(strKey, strKey);
            } // End reading data check
        } // End for
        //queryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        // Retrieve all placeholder tank dips for the affected tanks
        List<EP_Tank_Dip__c> placeholderTankDips = [SELECT ID, EP_Tank__c,RecordType.id, EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c 
                                                        FROM EP_Tank_Dip__c 
                                                            WHERE EP_Tank__c IN :tankIDSet AND 
                                                                RecordTypeId =: recType limit :queryRows];
        
        for (EP_Tank_Dip__c ptd : placeholderTankDips) {
            epTankDip = new EP_Tank_Dip__c();
            strKey = ptd.EP_Tank__c + EP_Common_Constant.UNDERSCORE_STRING + ptd.EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c.format(DATE_FORMAT);
            
            // Check if the tank dip has a placeholder tank dip
            if (tankDipMap.containsKey(strKey)) {
                epTankDip.ID = ptd.Id;
                deletedPlaceholderTankDips.add(epTankDip);
            } // End map check
        } // End for
        
        // Fix for Defect 29809 - End
        
            // Delete any placeholder tank dips (if any)
            if (!deletedPlaceholderTankDips.isEmpty()) {
                Database.delete(deletedPlaceholderTankDips);
                //delete deletedPlaceholderTankDips;
            }
        }
       catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                REM_PLCHLDER_TANK_DIP_FROM_SYSTEM, TANK_DIP_TRIGGER_HELPER, ApexPages.Severity.ERROR);
       }
    
    }
    /*
    @Description: update User UTCOffset On Reading Time Change
    */
    public static void updateUserUTCOffsetOnReadingTimeChange(List<EP_Tank_Dip__c> NewTankDips, 
                                                                    Map<Id, EP_Tank_Dip__c> mapOldTankDips) { 
        try{                                                        
            User currentUser = [SELECT ID, EP_User_UTC_Offset__c FROM User
                                            WHERE ID = :UserInfo.getUserId() LIMIT 1];
    
            for(EP_Tank_Dip__c currentDip : NewTankDips) {
                if(currentDip.EP_Reading_Date_Time__c != NULL && 
                    currentDip.EP_Reading_Date_Time__c != mapOldTankDips.get(currentDip.ID).EP_Reading_Date_Time__c) {
                
                    currentDip.EP_LastModified_User_offset__c = currentUser.EP_User_UTC_Offset__c;
                } // End reading time check
            } // End for
       }
       catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                UPDATE_USER_UTC_OFFSET, TANK_DIP_TRIGGER_HELPER, ApexPages.Severity.ERROR);
       }
    }
    
    /* Author:Gautam Manchanda
       Description: This method validates that if the TankDip Integration Status has been changed to any value then mark the IS EXPORTED
                         checkbox accordingly
                         Thread 7b3
    */
    /** DEFECT PE-2417 - Start **/
    public static void updateIsExported() {
        Integer queryRows = EP_Common_Util.getQueryLimit();
        queryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        try{ 
            //Too many results changes
            for(EP_Tank_Dip__c td : [SELECT ID, EP_Integration_Status__c, EP_Tank_Dip_Exported__c 
                                        FROM EP_Tank_Dip__c 
                                        WHERE RecordType.DeveloperName = :EP_Common_Constant.ACTUAL LIMIT :queryRows])
            {
                td.EP_Tank_Dip_Exported__c = FALSE;
                //Too many results changes
                if (String.isNotBlank(td.EP_Integration_Status__c))
                {
                    if(td.EP_Integration_Status__c.equalsIgnoreCase(EP_Common_Constant.SYNC_STATUS))
                    { 
                        td.EP_Tank_Dip_Exported__c = TRUE;
                    }
                }
            } // End for
        
          // DEFECT PE-2417 - End
        }
       catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                UPDATE_IS_EXPORTED, TANK_DIP_TRIGGER_HELPER, ApexPages.Severity.ERROR);
       }
    }
}