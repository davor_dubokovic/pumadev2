/**
* @author       Accenture
* @Class name         EP_ASTSellToNewToProspect
*/ 
public class EP_ASTSellToNewToProspect extends EP_AccountStateTransition {
    /**
    * @author       Accenture
    * @name         EP_ASTSellToNewToProspect
    * @param        NA
    */ 
    public EP_ASTSellToNewToProspect() {
        finalState = EP_AccountConstant.PROSPECT;
    }
   /**
    * @author       Accenture
    * @name         isTransitionPossible
    * @param        NA
    */  
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToNewToProspect','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /**
    * @author       Accenture
    * @name         isRegisteredForEvent
    * @param        NA
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToNewToProspect','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /**
    * @author       Accenture
    * @name         isGuardCondition
    * @param        NA
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToNewToProspect','isGuardCondition');
        //L4_45352_Start
        /*
        EP_AccountService service = new EP_AccountService(this.account);
        if(service.isProductListAttached()){
            if(!service.isCompanySameOnProductList()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Company_Mismatch_For_Sell_To_and_ProductList);
                return false;
            }
            system.debug('## 2. Checking if Company on Products is same as that on Sell To.....');
            if(!service.isCompanySameOnProducts()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Productlist_has_Product_with_different_Company);
                return  false;
            }
            system.debug('## 3. Checking if Product list is attached to any other customer.....');
            if(service.isProductListLinkedWithDifferentSellTo()){
                accountEvent.isError = true;
                accountEvent.setEventMessage('Product List attached to different company');
                return  false;
            }*/
            /**DEFECT 43409 START**/
            /*if(!service.isCurrencySameOnProducts()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Pricebook_Cstmr_Currency_Mismatch);
                return  false;
            }*/
            /**DEFECT 43409 START**/
        
        //}
        //L4_45352_END
        return true;
    }
}