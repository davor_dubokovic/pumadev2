@isTest
public class EP_IntegrationException_UT {
	
	static testMethod void integrationException_test1() {
		string message ='Cannot create integration service';
		EP_IntegrationException exp = new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE,message);
		system.assertEquals(true,exp.code == EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE);
	}
	
	static testMethod void integrationException_test2() {
		string message ='Cannot create integration service';
		EP_IntegrationException exp= new EP_IntegrationException(EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE,message,null);
		system.assertEquals(true,exp.code == EP_IntegrationException.EP_IntegrationErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE);
	}
    
}