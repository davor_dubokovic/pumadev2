@isTest
public class EP_StorageShipToASInactive_UT{
    
   /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static final string EVENT_NAME = '07-InactiveTo07-Inactive';
    static final string INVALID_EVENT_NAME = '07-InactiveTo01-Prospect';
    
    static testMethod void setAccountDomainObject_test() {
        EP_StorageShipToASInactive localObj = new EP_StorageShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASInactiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
        
    }
    static testMethod void doOnEntry_test() {
        EP_StorageShipToASInactive localObj = new EP_StorageShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASInactiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method
        
    }
    static testMethod void doOnExit_test() {
        EP_StorageShipToASInactive localObj = new EP_StorageShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASInactiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method
    }
    
    static testMethod void doTransition_PositiveScenariotest() {
        EP_StorageShipToASInactive localObj = new EP_StorageShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void doTransition_NegativeScenariotest() {
        EP_StorageShipToASInactive localObj = new EP_StorageShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASInactiveDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        string errorMessage ;
        Test.startTest();
        try{
        Boolean result = localObj.doTransition();
        }catch(Exception e){
          errorMessage = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(errorMessage));
    }
    
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_StorageShipToASInactive localObj = new EP_StorageShipToASInactive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

}