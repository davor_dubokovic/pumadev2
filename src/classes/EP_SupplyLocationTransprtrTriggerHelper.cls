/*  
  @Author <Jai Singh>
   @name <EP_SupplyLocationTransprtrTriggerHelper>
   @CreateDate <16/06/2016>
   @Description <Helper class for trigger EP_SupplyLocationTransporterTrigger on object EP_Supply_Location_Transporter__c>  
   @Version <1.0>
*/
public without sharing class EP_SupplyLocationTransprtrTriggerHelper {
    private static final string EP_SUPPLY_LOCATION_TRANSPT_TRIGGER_HELPER = 'EP_SupplyLocationTransprtrTriggerHelper ';
    private static final string CHECK_EXIS_DEF_TRANSP = 'checkExistingDefaultTransporter';
    private static final string HANDLE_DEF_TRAN_CHANGE = 'handleDefaultTransporterChange';
    private static final string ERROR_MESSAGE = 'Please ensure atleast one default transporter is associated with this Supply Location';
    private static Integer queryRows = null;
     /*
     **  This method updates old EP_Supply_Location_Transporter record to non default
     **
     */
    public static void handleDefaultTransporterChange( map<Id,list<EP_Supply_Location_Transporter__c>> newSltMap ) {
        try{
        if( !newSltMap.keyset().isEmpty() ) {
            list<EP_Supply_Location_Transporter__c> removeDefaultSltList = new list<EP_Supply_Location_Transporter__c>();
            queryRows = EP_Common_Util.getQueryLimit();
            for( EP_Stock_Holding_Location__c  supplyLocObj : [ Select Id, 
                                                                ( select Id, EP_Is_Default__c from EP_Supply_Location_Transporters__r where EP_Is_Default__c = true ) 
                                                                from EP_Stock_Holding_Location__c 
                                                                where Id IN: newSltMap.keySet() LIMIT: queryRows
                                                              ] ) {
                for( EP_Supply_Location_Transporter__c sltObj : supplyLocObj.EP_Supply_Location_Transporters__r ) {
                    sltObj.EP_Is_Default__c = false;
                    removeDefaultSltList.add( sltObj );
                }
                
                //record of current before context  
                //if there is more then one record for same supply location only keep as default
                if( newSltMap.get( supplyLocObj.Id ).size() > 1 ) {
                    Integer counter = 0;
                    for( EP_Supply_Location_Transporter__c sltObj : newSltMap.get( supplyLocObj.Id ) ) {
                        if( counter != 0 ) {
                            sltObj.EP_Is_Default__c = false;
                        }
                        counter++;
                    }
                }
            }
            if( !removeDefaultSltList.isEmpty() ) 
            {
                //set context variable to prevent trigger code execution
                EP_SupplyLocationTransprtrTriggerHandler.isExecuteUpdate = false;
                database.update( removeDefaultSltList );
            }  
         } 
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                HANDLE_DEF_TRAN_CHANGE, EP_SUPPLY_LOCATION_TRANSPT_TRIGGER_HELPER, ApexPages.Severity.ERROR);
        }         
    }
    
    /*
     **  This method to check if new Supply Location Transporter
     **
     */
    public static void checkExistingDefaultTransporter( set<Id> supplyLocSet, list<EP_Supply_Location_Transporter__c> nondefaultList  )
    {
       try{
            queryRows = EP_Common_Util.getQueryLimit();
            map<Id, EP_Stock_Holding_Location__c> supplyLcoMap = new map<Id, EP_Stock_Holding_Location__c>([ Select Id, ( select id from EP_Supply_Location_Transporters__r where EP_Is_Default__c = true ) from EP_Stock_Holding_Location__c where Id IN: supplyLocSet limit :queryRows]);
            for( EP_Supply_Location_Transporter__c sltObj : nondefaultList )
            {
                if( !supplyLcoMap.isEmpty() && supplyLcoMap.containsKey( sltObj.EP_Supply_Location__c ) )
                {
                    if( supplyLcoMap.get( sltObj.EP_Supply_Location__c ).EP_Supply_Location_Transporters__r.isEmpty() )
                    {
                        sltObj.AddError( ERROR_MESSAGE );
                    }
                }
            }
            }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, 
                CHECK_EXIS_DEF_TRANSP, EP_SUPPLY_LOCATION_TRANSPT_TRIGGER_HELPER, ApexPages.Severity.ERROR);
        }
    }

}