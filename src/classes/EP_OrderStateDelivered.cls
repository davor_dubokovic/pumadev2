/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateDelivered.cls>     
     @Description <Order State for Delivered status, common for all classes>   
     @Version <1.1> 
     */

     public class EP_OrderStateDelivered extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }

        public override boolean doTransition(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDelivered','doTransition');
            return super.doTransition();
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDelivered','doOnEntry');
            
        }
        public override boolean isInboundTransitionPossible(){
            EP_GeneralUtility.Log('Public','EP_OrderStateDelivered','isInboundTransitionPossible');
            //Override this with all possible guard conditions that permits the state transition possible
            //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
            return super.isInboundTransitionPossible();
        }
        
        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Delivered;
        }
    }