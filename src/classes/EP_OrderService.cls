/**
  * @Author      : Accenture
  * @name        : EP_OrderService
  * @CreateDate  : 31/01/2017
  * @Description : This class which executes the different strategies
  * @Version     : <1.0>
  * @reference   : N/A
  */

  global with sharing class EP_OrderService{  
    EP_OrderDomainObject orderDomainObj;
    EP_OrderState orderState;
    @testvisible
    EP_OrderType orderType;
    csord__Order__c orderObj;   
    EP_AccountMapper accountMapper = new EP_AccountMapper();
	//Changes for #60147
	public boolean enqueueJob  = false;
    public EP_OrderService(EP_OrderDomainObject orderDomainObj){        
        this.orderDomainObj = orderDomainObj;
        this.orderObj  = orderDomainObj.getOrder();
        orderType =  orderDomainObj.getTypeObject();
        orderType.setOrderDomain(orderDomainObj);
    }

    /** This method performs all submit actions
    *  @date      03/02/2017
    *  @name      doSubmitActions
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public void doSubmitActions(){
        EP_GeneralUtility.Log('Public','EP_OrderService','doSubmitActions');
        setSellToBillTo();
        orderDomainObj.setOrderTypeField();
        orderDomainObj.updatePaymentTermAndMethod();
        orderDomainObj.doUpdateTransportFields(orderObj);//Defect 57591
        orderType.updateTransportService();
        //doUpdatePaymentTermAndMethod();
    }

    /*
    public void doUpdateOrderTypeField() {
        String strOrderType = orderDomainObj.getConsignmentType();
        this.orderObj.Type = strOrderType;
        if (strOrderType == EP_AccountConstant.NON_CONSIGNMENT) {
            this.orderObj.Type = EP_Common_Constant.NON_CONSIGNMENT;
        }
        System.debug('**orderObj*'+orderObj.Type);
    }*/

    /** This method updates payment terms
    *  @date      03/02/2017
    *  @name      doUpdatePaymentTermAndMethod
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public void doUpdatePaymentTermAndMethod(){
        EP_GeneralUtility.Log('Public','EP_OrderService','doUpdatePaymentTermAndMethod');

        orderType.doUpdatePaymentTermAndMethod();
    }

    /** This method validates the Credit status of the customer
    *  @date      03/02/2017
    *  @name      hasCreditIssue
    *  @param     NA
    *  @return    Boolean 
    *  @throws    NA
    */
    public Boolean hasCreditIssue(){
        EP_GeneralUtility.Log('Public','EP_OrderService','hasCreditIssue');
        return orderType.hasCreditIssue();
    }
    //Defect 57278 Start 
	/** This method gets the total amount of all open orders
	*  @date      04/28/2017
	*  @name      getCreditAmount
	*  @param     NA
	*  @return    Double 
	*  @throws    NA
	*/
	public Double getCreditAmount(){
		EP_GeneralUtility.Log('Public','EP_OrderService','getOpenCreditAmount');
		return orderType.getCreditAmount();
	}
	//Defect 57278 End

	/** This method get the ship to of the customer 
	*  @date      03/02/2017
	*  @name      getShipTos
	*  @param     Id accountId
	*  @return    List<Account>
	*  @throws    NA
	*/
	/*public List<Account> getShipTos(Id accountId){
		EP_GeneralUtility.Log('Public','EP_OrderService','getShipTos');
		return orderType.getShipTos(accountId);
	} */ 

	/** This method getOperationalTanks Based on the Order 
	*  @date      22/02/2017
	*  @name      getOperationalTanks
	*  @param     Id accountId
	*  @return    Map<Id, EP_Tank__c>
	*  @throws    NA
	*/
	public Map<Id, EP_Tank__c> getOperationalTanks(Id accountId) {
		return orderType.getOperationalTanks(accountId);
	}

	/** This method getTanks Based on the Order 
	*  @date      22/02/2017
	*  @name      getOperationalTanks
	*  @param     Id accountId
	*  @return    Map<Id, EP_Tank__c>
	*  @throws    NA
	*/
	public Map<Id, EP_Tank__c> getTanks(Id accountId) {
		return orderType.getTanks(accountId);
	}

	/** This method checks for correct loading date for Order 
	*  @date      22/02/2017
	*  @name      isValidLoadingDate
	*  @param     NA
	*  @return    Boolean 
	*  @throws    NA
	*/
	/*public Boolean isValidLoadingDate(){
		EP_GeneralUtility.Log('Public','EP_OrderService','isValidLoadingDate');
		return orderType.isValidLoadingDate(orderObj);
	}*/

	/** This method checks for correct expected delivery date of the Order 
	*  @date      22/02/2017
	*  @name      isValidExpectedDate
	*  @param     NA
	*  @return    Boolean 
	*  @throws    NA
	*/
	/*public Boolean isValidExpectedDate(){
		EP_GeneralUtility.Log('Public','EP_OrderService','isValidExpectedDate');
		return orderType.isValidExpectedDate(orderObj);
	}
*/
	/** This method determines if customer PO should be applicable and shown for input. 
	*  @date      24/02/2017
	*  @name      showCustomerPO
	*  @param     NA
	*  @return    Boolean 
	*  @throws    NA
	*/
	/*public Boolean showCustomerPO(){
		EP_GeneralUtility.Log('Public','EP_OrderService','showCustomerPO');
		return orderType.showCustomerPO();
	}
*/
	/** This method finds the correct record type if based on the Order. 
	*  @date      24/02/2017
	*  @name      findRecordType
	*  @param     NA
	*  @return    Id
	*  @throws    NA
	*/
	/*public Id findRecordType() {
		EP_GeneralUtility.Log('Public','EP_OrderService','findRecordType');
		return orderType.findRecordType();
	}*/

	/** This method associates the correct custemer(sell to or bill to) with the Order.
	*  @date      24/02/2017
	*  @name      setSellToBillTo
	*  @param     NA
	*  @return    Order 
	*  @throws    NA
	*/
	public void setSellToBillTo(){
		EP_GeneralUtility.Log('Public','EP_OrderService','setSellToBillTo');
		Account accountObj =  accountMapper.getAccountByAcountId(orderObj.AccountId__c);    
		EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(accountObj);
		EP_AccountService accountService = new EP_AccountService(accountDomainObj);        
		orderDomainObj.setSellTo(accountService.getSellTo());
		orderDomainObj.setBillTo(accountService.getBillTo()); 
	}

	/** This method returns the list of the payment terms applicable for the order.
	*  @date      24/02/2017
	*  @name      getApplicablePaymentTerms
	*  @param     Account invoicedAccountObject
	*  @return    List<String>
	*  @throws    NA
	*/  
	public List<String> getApplicablePaymentTerms(Account invoicedAccountObject){
		EP_GeneralUtility.Log('Public','EP_OrderService','getApplicablePaymentTerms');
		return orderType.getApplicablePaymentTerms(invoicedAccountObject);
	}

	/** This method returns the pricebookentries
	*  @date      26/02/2017
	*  @name      findPriceBookEntries
	*  @param     Id pricebookId, csord__Order__c orderObj
	*  @return    List<PriceBookEntry>
	*  @throws    NA
	*/  
	public List<PriceBookEntry> findPriceBookEntries(Id pricebookId, csord__Order__c orderObj) {
		EP_GeneralUtility.Log('Public','EP_OrderService','findPriceBookEntries');
		System.debug('***ordertype**'+ordertype+'***orderType*'+orderType.deliveryType);
		return ordertype.findPriceBookEntries(pricebookId,orderObj);
	}


	/** This method validates the csord__Order__c Start Date
	*  @date      26/02/2017
	*  @name      isValidOrderDate
	*  @param     NA
	*  @return    Boolean 
	*  @throws    NA
	*/  
	public Boolean isValidOrderDate() {
		EP_GeneralUtility.Log('Public','EP_OrderService','isValidOrderDate');
		return ordertype.isValidOrderDate(orderObj);
	}


	/** This method validates the csord__Order__c Start Date
	*  @date      26/02/2017
	*  @name      setRequestedDateTime
	*  @param     csord__Order__c orderObj,String selectedDate,String availableSlots
	*  @return    csord__Order__c
	*  @throws    NA
	*/
	public csord__Order__c setRequestedDateTime(csord__Order__c orderObj,String selectedDate,String availableSlots) {
		EP_GeneralUtility.Log('Public','EP_OrderService','setRequestedDateTime');
		return ordertype.setRequestedDateTime(orderObj,selectedDate,availableSlots);
	}


	/** This method validates the Order Start Date
	*  @date      26/02/2017
	*  @name      calculatePrice
	*  @param     NA
	*  @return    NA
	*  @throws    NA
	*/
	public void calculatePrice(){
		EP_GeneralUtility.Log('Public','EP_OrderService','calculatePrice');
		orderType.calculatePrice();    
	}


	/** This method validates the Order Start Date
	*  @date      26/02/2017
	*  @name      getDeliveryTypes
	*  @param     NA
	*  @return    List<String>
	*  @throws    NA
	*/
	/*public List<String> getDeliveryTypes() {
		EP_GeneralUtility.Log('Public','EP_OrderService','getDeliveryTypes');
		return ordertype.getDeliveryTypes();
	}*/

	/** This method is used to populate "Pipeline" in delivery type when default trannsporter is "No Transporter"
      *  @date      02/03/2017
      *  @name      checkForNoTransporter
      *  @param     String transporterName 
      *  @return    csord__Order__c
      *  @throws    NA
      */ 
      public csord__Order__c checkForNoTransporter(String transporterName) {
      	EP_GeneralUtility.Log('Public','EP_OrderService','checkForNoTransporter');
      	return ordertype.checkForNoTransporter(transporterName);
      }

	/** returns routes of ship to and location
	*  @date      22/02/2017
	*  @name      getAllRoutesOfShipToAndLocation
	*  @param     Id shipToId,Id stockholdinglocationId
	*  @return    List<EP_Route__c>
	*  @throws    NA
	*/
	public List<EP_Route__c> getAllRoutesOfShipToAndLocation(Id shipToId,Id stockholdinglocationId) {
		EP_GeneralUtility.Log('Public','EP_OrderService','getAllRoutesOfShipToAndLocation');
		return ordertype.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId); 
	}

	 /** This method sets retrospective orders
     *  @date      07/03/2017
     *  @name      setRetroOrderDetails
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     /*public void setRetroOrderDetails(){
     	EP_GeneralUtility.Log('Public','EP_OrderService','setRetroOrderDetails');
     	ordertype.setRetroOrderDetails();
     }*/

    /** This method find if Order Entry is Valid
     *  @date      07/03/2017
     *  @name      isOrderEntryValid
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public Boolean isOrderEntryValid(integer numOfTransportAccount){
     	EP_GeneralUtility.Log('Public','EP_OrderService','isOrderEntryValid');
     	return orderType.isOrderEntryValid(numOfTransportAccount);
     }

    /** This method sets Consumption values on fields
     *  @date      07/03/2017
     *  @name      setConsumptionOrderDetails
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public void setConsumptionOrderDetails() {
     	EP_GeneralUtility.Log('Public','EP_OrderService','setConsumptionOrderDetails');
     	orderType.setConsumptionOrderDetails();
    }

     /**Status isChanged need to be changed.
     *  @date      06/02/2017
     *  @name      doSyncStatusWithNav
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */     
     public void doSyncStatusWithNav() {
        EP_GeneralUtility.Log('Public','EP_OrderService','doSyncStatusWithNav');
        String companyName = EP_Common_Constant.BLANK;
        Map<String,String> mapOrderItems = new map <String,String> ();
        List<Id> lstOrderItemIds = new List<Id>();
        companyName = orderObj.csord__Account__r.EP_Puma_Company_Code__c;
        for(csord__Order_Line_Item__c orderItemObj: orderObj.csord__Order_Line_Items__r) {
            lstOrderItemIds.add(orderItemObj.Id);
        }
        
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(orderObj.Id, 'SFDC_TO_NAV_ORDER_SYNC', companyName);
        //Changes for #60147
        //set enqueue Job flag for outbound message Service to enqueue a JOB for outbound hit instead of future callout
        //For VMI order Creation, A Pricing callout will be a future outbound call out and after successfully pricing sync, State machine will hit another future outbound callout for order sync with NAV and SFDC does not allow to do a future callout from futrue call out
        if(enqueueJob) {
        	outboundService.setEnqueueJob(enqueueJob);
        }
        outboundService.sendOutboundMessage('ORDER_UPDATE',lstOrderItemIds);
        //Sync flag should be set on positive acknowledgement
       // orderObj.EP_Sync_with_NAV__c = true;
       // update orderObj;
    }

    /**Status isChanged need to be changed.
     *  @date      06/02/2017
     *  @name      doSyncStatusWithWindms
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */     
    public void doSyncStatusWithWindms() {
        EP_GeneralUtility.Log('Public','EP_OrderService','doSyncStatusWithWindms');
        String companyName = EP_Common_Constant.BLANK;
        Map<String,String> mapOrderItems = new map <String,String> ();
        List<Id> lstOrderItemIds = new List<Id>();
        companyName = orderObj.csord__Account__r.EP_Puma_Company_Code__c;
        for(csord__Order_Line_Item__c orderItemObj: orderObj.csord__Order_Line_Items__r) {
            lstOrderItemIds.add(orderItemObj.Id);
        } 
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(orderObj.Id, 'SFDC_TO_WINDMS_ORDER_SYNC', companyName);
        outboundService.sendOutboundMessage(EP_Common_Constant.ORDER_WINDMS,lstOrderItemIds);
        //Sync flag should be set on positive acknowledgement
        //orderObj.EP_Sync_With_LS__c = true;
        //update orderObj;
    }

    // State machine invocation
    public boolean setOrderStatus(EP_OrderEvent orderEvent){
    	EP_GeneralUtility.Log('Public','EP_OrderService','setOrderStatus');
    	boolean isStatusChanged = true;
    	/*EP_OrderTypeStateMachineFactory orderTypeStateMachineFactory = new EP_OrderTypeStateMachineFactory();
        orderState = orderTypeStateMachineFactory.getOrderStateMachine(this.orderDomainObj).getOrderState(orderEvent);
        orderState.doOnEntry();
        if(orderState.doTransition()){
        	isStatusChanged = true;
        	orderState = orderState.transitionedState;
        }*/
        return isStatusChanged;
    }

    //Executes on exit state actions
    public void doPostStatusChangeActions(){
    	EP_GeneralUtility.Log('Public','EP_OrderService','doPostStatusChangeActions');
 		//Changes for #60147
 		//Assign enqueue Job flag this service to State Machine OrderService to send outbound message for OrderSync with Nav when Pricing Sync
        //For VMI order Creation, A Pricing callout will be a future outbound call out and after successfully pricing sync, State machine will hit another future outbound callout for order sync with NAV and SFDC does not allow to do a future callout from futrue call out   	
    	orderState.orderService.enqueueJob = enqueueJob;
    	orderState.doOnExit();
    }

    @InvocableMethod(label='SubmitAfterInventoryApproval' description='Performs Submit Actions When Awaiting Inventory Approved')
	public static void doSubmitAfterInventoryApproval(List<ID> ids) {
		
	}
    /**VMI Order Credit Check Sync With WINDMS.
     *  @date      06/02/2017
     *  @name      doSyncCreditCheckWithWINDMS
     *  @param     NA
     *  @return    NA
     *  @throws    NA
    */     
    public void doSyncCreditCheckWithWINDMS() {
    	EP_GeneralUtility.Log('Public','EP_OrderService','doSyncCreditCheckWithWINDMS');
        String companyName = orderObj.csord__Account__r.EP_Puma_Company_Code__c;
        list<id> orderLineIdList = new list<id>();
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(orderObj.Id, 'SEND_ORDER_CREDIT_CHECK', companyName);
        //Changes for #60147
        //set enqueue Job flag for outbound message Service to enqueue a JOB for outbound hit instead of future callout
        //For VMI order Creation, A Pricing callout will be a future outbound call out and after successfully pricing sync, State machine will hit another future outbound callout for order sync with NAV and SFDC does not allow to do a future callout from futrue call out
        if(enqueueJob) {
        	outboundService.setEnqueueJob(enqueueJob);
        }
        outboundService.sendOutboundMessage('ORDER_CREDIT_CHECK',orderLineIdList);
    }


	public void doSyncCreditCheckInProgressWithWINDMS() {
		EP_GeneralUtility.Log('Public','EP_OrderService','doSyncCreditCheckInProgressWithWINDMS');
        String companyName = orderObj.csord__Account__r.EP_Puma_Company_Code__c;
        list<id> orderLineIdList = new list<id>();
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(orderObj.Id, 'SEND_ORDER_CREDIT_CHECK_IN_PROGRESS', companyName);
        outboundService.sendOutboundMessage('ORDER_CREDIT_CHECK_CP',orderLineIdList);
    }

    /** Method to trigger the approval process when inventory is low -- Order Approval
     *  Called from AwaitingInventory review order state
     *  @date      10/02/2017
     *  @name      submitForInventoryApproval
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */   
     public void submitForInventoryApproval(){
        EP_GeneralUtility.Log('Public','EP_OrderService','submitForInventoryApproval');
        if(!Approval.isLocked(orderObj.id)) {
            // create the new approval request to submit    
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();   
            req.setComments(EP_Common_Constant.str_comment);
            req.setObjectId(orderObj.Id);
            Approval.ProcessResult result = Approval.process(req);       
        }   
    }
 }