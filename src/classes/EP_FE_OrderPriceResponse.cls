/*********************************************************************************************
     @Author <Iegor Nechyporenko>
     @name <EP_FE_OrderPriceResponse >
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/
global with sharing class EP_FE_OrderPriceResponse extends EP_FE_Response {
    // Price for each item
   // public List<EP_FE_OrderItem> orderItems;

    // Order Id
    public Id draftOrderId;
    // Returns true if prices are ready for the calculation data
    public Boolean isPriceCalculated;
    public Boolean canSeePrices;
    public EP_FE_OrderProductData priceData;


/*********************************************************************************************
     @Author <Iegor Nechyporenko>
     @name <EP_FE_OrderPriceResponse>
     @CreateDate <>
     @Description <Constructor>  
     @Version <1.0>
    *********************************************************************************************/
    global EP_FE_OrderPriceResponse() {
        this.isPriceCalculated = true;
    }
}