@isTest
private class EP_GenerateVMINotfsBatchHelper_R1_UT {
	static EP_Tank__c tankObj;
	static Account shipTo;
	static EP_AccountDomainObject obj;
	static Account sellTo;

	@TestSetup
	static void setupData(){
		
		
	}

	static void data(){
		EP_Simulated__c simulateObj = new EP_Simulated__c();
		simulateObj.Name = 'Test';
		simulateObj.EP_Simulated_Date_Time__c=  System.Now().addDays(-1);
		insert simulateObj;
		sellTo = EP_TestDataUtility.getSellTo();
		shipTo = EP_TestDataUtility.getShipTo();
		shipTo.EP_Ship_To_UTC_Offset__c  = 1;
		update shipTo;
		obj = new EP_AccountDomainObject(shipTo.Id);
		tankObj = EP_TestDataUtility.createTestEP_Tank(obj.localAccount);
		tankObj.EP_Last_Dip_Ship_To_Date_Time__c = System.Now().addDays(-1);
		tankObj.EP_Tank_Status__c= EP_Common_Constant.OPERATIONAL_TANK_STATUS;
		update tankObj;
		EP_TestDataUtility.createTestRecordsForTankDip(tankObj.id,System.Now().addDays(-1));
	}

	@isTest static void retrieveVMIShipToAccountsForCompliance_Positive() {
		data();
		Test.startTest();
		Database.QueryLocator tankList = EP_GenerateVMINotfsBatchHelper_R1.retrieveVMIShipToAccountsForCompliance();
		Test.stopTest();
		system.assert(tankList!=null);
	}
	
	@isTest static void returnShipToDateTime_Positive() {
		EP_Simulated__c simulateObj = new EP_Simulated__c();
		simulateObj.Name = 'Test';
		simulateObj.EP_Simulated_Date_Time__c=  System.Now().addDays(-1);
		insert simulateObj;
		
		Test.startTest();
		DateTime currentShipToDateTimet = EP_GenerateVMINotfsBatchHelper_R1.returnShipToDateTime(System.Now());
		Test.stopTest();
		system.assert(currentShipToDateTimet!=null);
	}
	
	@isTest static void checkShipToAccountsForCompliance_Positive() {
		data();
		EP_Tank__c tank = [SELECT ID, EP_Ship_To__c,EP_Last_Dip_Ship_To_Date_Time__c, EP_Ship_To__r.Name, EP_Last_Place_Dip_Reading_Date_Time__c,
									 EP_Capacity__c, EP_Deadstock__c,EP_Last_Dip_Reading_Date__c,EP_Tank_Status__c,  EP_UnitOfMeasure__c,
									 EP_Ship_To__r.EP_Country_Code__c,EP_Ship_To__r.EP_Ship_To_UTC_Timezone__c,EP_Last_Placeholder_Ship_To_Date_Time__c,
									 EP_Missing_Placeholder_Dip_For_Today__c,EP_Ship_To__r.parentId,EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c,
									 EP_Ship_To__r.EP_Tank_Dips_Schedule_Time__c,EP_Ship_To__r.EP_Tank_Dips_Submission_Time__c,
									 EP_Ship_To__r.EP_Reminder_Notification_Interval_in_hrs__c 
			    					FROM EP_Tank__c where id =: tankObj.id];
		Test.startTest();
		EP_GenerateVMINotfsBatchHelper_R1.checkShipToAccountsForCompliance(new List<EP_Tank__c>{tank});
		Test.stopTest();
		//system.assert(currentShipToDateTimet==simulateObj.EP_Simulated_Date_Time__c);
	}

	@isTest static void isDipReminderToBeSent_Positive(){
		data();
		EP_Notification_Type__c typeObj = new EP_Notification_Type__c();
		typeObj.EP_Default_Sign_Up__c = true;
		typeObj.EP_Notification_Code__c = 'DIP REMINDERS';
		typeObj.EP_Notification_Template_Contact__c = '89789';
		insert typeObj;
		EP_Notification_Account_Settings__c notObj = new EP_Notification_Account_Settings__c();
		notObj.EP_Notification_Type__c = typeObj.Id;
		notObj.EP_Enabled__c = true;
		notObj.EP_Sell_To__c = sellTo.id;
		notObj.EP_Notification_Template_Contact__c = '89789';
		insert notObj;
		map<Id,Account> mapShipTos = new map<Id,Account>();
		map<Id,Id> mapAccParentId = new map<Id,Id>();
		mapShipTos.put(obj.localAccount.id,obj.localAccount);
		mapAccParentId.put(obj.localAccount.id,sellTo.id);
		Test.startTest();
		EP_GenerateVMINotfsBatchHelper_R1.isDipReminderToBeSent(mapShipTos,mapAccParentId);
		Test.stopTest();
		system.assert(!mapShipTos.isEmpty());
	}

	@isTest static void isDipReminderToBeSent_Negative(){
		data();
		EP_Notification_Type__c typeObj = new EP_Notification_Type__c();
		typeObj.EP_Default_Sign_Up__c = true;
		typeObj.EP_Notification_Code__c = 'DIP REMINDERS';
		typeObj.EP_Notification_Template_Contact__c = '89789';
		insert typeObj;
		EP_Notification_Account_Settings__c notObj = new EP_Notification_Account_Settings__c();
		notObj.EP_Notification_Type__c = typeObj.Id;
		notObj.EP_Enabled__c = False;
		notObj.EP_Sell_To__c = sellTo.id;
		notObj.EP_Notification_Template_Contact__c = '89789';
		insert notObj;
		map<Id,Account> mapShipTos = new map<Id,Account>();
		map<Id,Id> mapAccParentId = new map<Id,Id>();
		mapShipTos.put(obj.localAccount.id,obj.localAccount);
		mapAccParentId.put(obj.localAccount.id,sellTo.id);
		Test.startTest();
		EP_GenerateVMINotfsBatchHelper_R1.isDipReminderToBeSent(mapShipTos,mapAccParentId);
		Test.stopTest();
		system.assert(mapShipTos.isEmpty());
	}

	@isTest static void getAccNotify_Positive(){
		data();
		EP_Notification_Type__c typeObj = new EP_Notification_Type__c();
		typeObj.EP_Default_Sign_Up__c = true;
		typeObj.EP_Notification_Code__c = 'DIP REMINDERS';
		typeObj.EP_Notification_Template_Contact__c = '89789';
		insert typeObj;
		EP_Notification_Account_Settings__c notObj = new EP_Notification_Account_Settings__c();
		notObj.EP_Notification_Type__c = typeObj.Id;
		notObj.EP_Enabled__c = False;
		notObj.EP_Sell_To__c = sellTo.id;
		notObj.EP_Notification_Template_Contact__c = '89789';
		insert notObj;
		EP_Notification_Account_Settings__c notificationObj = [select id,EP_Notification_Code__c 
															from EP_Notification_Account_Settings__c where id=:notObj.id];
		Test.startTest();
		EP_Notification_Account_Settings__c obj 
			= EP_GenerateVMINotfsBatchHelper_R1.getAccNotify(new List<EP_Notification_Account_Settings__c>{notificationObj},'DIP REMINDERS');
		Test.stopTest();
		system.assert(obj.Id!=null);
	}

	@isTest static void getAccNotify_Negative(){
		Test.startTest();
		EP_Notification_Account_Settings__c obj 
			= EP_GenerateVMINotfsBatchHelper_R1.getAccNotify(new List<EP_Notification_Account_Settings__c>(),'DIP REMINDERS');
		Test.stopTest();
		system.assert(obj.Id==null);
	}

	@isTest static void createAccount_Positive(){
		data();
		Test.startTest();
		Account acc = EP_GenerateVMINotfsBatchHelper_R1.createAccount(tankObj);
		Test.stopTest();
		system.assert(acc.id == shipTo.Id);
	}

	@isTest static void createAccount_Negative(){
		EP_Tank__c tankOb = new EP_Tank__c();
		Test.startTest();
		Account acc = EP_GenerateVMINotfsBatchHelper_R1.createAccount(tankOb);
		Test.stopTest();
		system.assert(acc != null);
	}

	@isTest static void generatePlaceholderTankDip_Positive(){
		data();
		Test.startTest();
		EP_Tank_Dip__c tankDip = EP_GenerateVMINotfsBatchHelper_R1.generatePlaceholderTankDip(tankObj,System.Now());
		Test.stopTest();
		system.assert(tankDip!=null);
	}
}