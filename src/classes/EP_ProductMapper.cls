/**
   @Author          Accenture
   @name            EP_ProductMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to Account Object
   @Version         1.0
   @reference       NA
   */

public with sharing class EP_ProductMapper {

    public list<Product2> getRecordsProductCode(set<string> productCodeSet) {
        list<Product2> product2List = new list<Product2>(); 
        for (List<Product2> productObjList:  [SELECT Id,EP_Product_Sold_As__c,ProductCode FROM Product2 WHERE ProductCode IN : productCodeSet]) {
           product2List.addAll(productObjList); 
        }
        return product2List; 
    }
    
    /**
    * @Author       Accenture
    * @Name         getProductSoldAsInfo
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    public map<string,string> getProductSoldAsInfo(Set<string> itemIdSet) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHelper','getProductSoldAsInfo');
        map<string,string> productMap = new map<string,string>();
        for(Product2 productObj : getRecordsProductCode(itemIdSet)) {
            productMap.put(productObj.ProductCode, productObj.EP_Product_Sold_As__c);
        }
        return productMap;
    }
    
    public list<Product2> getRecordsByName(set<string> productNameSet) {
        list<Product2> product2List = new list<Product2>(); 
        for (List<Product2> productObjList:  [Select id, name, EP_Is_System_Product__c from Product2 where EP_Is_System_Product__c = TRUE AND Name in :productNameSet]) {
           product2List.addAll(productObjList); 
        }
        return product2List; 
    }
}