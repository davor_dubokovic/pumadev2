@isTest
public class EP_PricingResponseHandler_UT {

    static EP_OrderMapper orderMapperObj = new EP_OrderMapper();
    static String ERROR_DEC = 'Null Pointer Exception';
    static String SEQ_ID = '12';
    static String ERROR_CODE = '500';
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }

    static testMethod void processRequest_test() {        
        EP_PricingResponseHandler pricingResponse = new EP_PricingResponseHandler();
        EP_PricingResponseStub pricingResponseStub = EP_TestDataUtility.createPricingResponseStub();
        csord__Order__c orderObject = orderMapperObj.getCSRecordById(EP_TestDataUtility.getNonConsignmentOrderPositiveScenario().id);
        LIST<EP_PricingResponseStub.LineItem> priceLineItems = new LIST<EP_PricingResponseStub.LineItem>{EP_TestDataUtility.createPriceLineItem()};
        OrderItem ordItem = [SELECT Id , EP_OrderItem_Number__c,orderId FROM OrderItem WHERE OrderId =:orderObject.id LIMIT 1];
        //#60147 User Story Start
        pricingResponseStub.Payload.any0.PricingResponse.priceDetails.priceLineItems.orderId = orderObject.OrderNumber__c;
        for(EP_PricingResponseStub.LineItem priceLineItem : pricingResponseStub.Payload.any0.PricingResponse.priceDetails.priceLineItems.lineItem) {
            priceLineItem.lineItemInfo.orderLineItem = ordItem;
        }
        //#60147 User Story End
        String jsonBody = System.JSON.serialize(pricingResponseStub);
        
        Test.startTest();
            pricingResponse.processRequest(jsonBody,orderObject.OrderNumber__c);
        Test.stopTest();
        
        System.assertNotEquals(null, jsonBody);
    }
    
    static testMethod void processPricingResponse_test() {
        EP_PricingResponseHandler localObj = new EP_PricingResponseHandler();
        csord__Order__c orderObject = EP_TestDataUtility.getSalesOrder();
        list<OrderItem> orderItems = [select Id,priceBookEntry.ProductCode, EP_OrderItem_Number__c,
                                        Quantity,orderId,UnitPrice,EP_WinDMS_Line_ItemId__c, 
                                        order.Status,order.EP_Pricing_Status__c,priceBookEntryId,PricebookEntry.Product2.Name
                                        from OrderItem where order.Id = : orderObject.id];     
        String prodCode = orderItems[0].priceBookEntry.ProductCode;
        EP_PricingResponseStub.PriceDetails priceDetails = EP_TestDataUtility.createPriceDetails();
        for(EP_PricingResponseStub.LineItem lineitem :priceDetails.priceLineItems.lineItem ){                    
                    lineitem.lineItemInfo.lineItemId = String.valueOf(orderItems[0].EP_OrderItem_Number__c);
                    lineitem.lineItemInfo.itemId = prodCode;                    
        }
                
        Test.startTest();
            EP_PricingResponseHandler.processPricingResponse(priceDetails, orderObject.OrderNumber__c);            
        Test.stopTest();
        /*No assert required calling other methods*/
        system.assert(true);
    }
    
    static testMethod void processPricingResponse_NegativeTest() {
        EP_PricingResponseHandler localObj = new EP_PricingResponseHandler();
        csord__Order__c orderObject = EP_TestDataUtility.getSalesOrder();
        list<OrderItem> orderItems = [select Id,priceBookEntry.ProductCode, EP_OrderItem_Number__c,
                                        Quantity,orderId,UnitPrice,EP_WinDMS_Line_ItemId__c, 
                                        order.Status,order.EP_Pricing_Status__c,priceBookEntryId,PricebookEntry.Product2.Name
                                        from OrderItem where order.Id = : orderObject.id];     
        String prodCode = orderItems[0].priceBookEntry.ProductCode;
        EP_PricingResponseStub.PriceDetails priceDetails = EP_TestDataUtility.createPriceDetails();
        priceDetails.Error.ErrorDescription = 'Error';
             
        Test.startTest();
            EP_PricingResponseHandler.processPricingResponse(priceDetails, orderObject.OrderNumber__c);            
        Test.stopTest();
        //No assert required calling other methods ,hence adding a dummy assert.
        system.Assert(true);
    }    
    
    static testMethod void processOrderItems_test() {
        csord__Order__c orderObject = EP_TestDataUtility.getSalesOrder();
        LIST<EP_PricingResponseStub.LineItem> priceLineItems = new List<EP_PricingResponseStub.LineItem>();
        OrderItem orderItem = [select Id,Quantity from OrderItem where order.Id = : orderObject.id limit 1];     
        EP_PricingResponseStub.PriceDetails priceDetails = EP_TestDataUtility.createPriceDetails();
        for(EP_PricingResponseStub.LineItem lineitem : priceDetails.priceLineItems.lineItem ){
            lineitem.lineItemInfo.invoiceDetails.invoiceComponent = new list<EP_PricingResponseStub.InvoiceComponent>();
            orderItem.Quantity = 300;
            lineitem.lineItemInfo.lineItemId = String.valueOf(orderItem.EP_OrderItem_Number__c);
            lineitem.lineItemInfo.orderLineItem  = orderItem;
            break;
        } 
        Test.startTest();
            EP_PricingResponseHandler.processOrderItems(priceDetails, orderObject.OrderNumber__c);
        Test.stopTest();
        
        csord__Order_Line_Item__c orderItemUpdate = [select Id, Quantity__c from csord__Order_Line_Item__c where csord__Order__r.Id = : orderObject.id limit 1];     
        System.assertEquals(300 , orderItemUpdate.Quantity__c);
    }
    
    static testMethod void upsertOrderLineItems_test() { // Insufficient access on cross obj
        csord__Order__c orderObject = EP_TestDataUtility.getSalesOrder();
        LIST<EP_PricingResponseStub.LineItem> priceLineItems = new List<EP_PricingResponseStub.LineItem>();
        OrderItem orderItem = [select Id,Quantity from OrderItem where order.Id = : orderObject.id limit 1];     
        EP_PricingResponseStub.PriceDetails priceDetails = EP_TestDataUtility.createPriceDetails();
        for(EP_PricingResponseStub.LineItem lineitem : priceDetails.priceLineItems.lineItem ){
            lineitem.lineItemInfo.invoiceDetails.invoiceComponent = new list<EP_PricingResponseStub.InvoiceComponent>();
            orderItem.Quantity = 300;
            lineitem.lineItemInfo.lineItemId = String.valueOf(orderItem.EP_OrderItem_Number__c);
            lineitem.lineItemInfo.orderLineItem  = orderItem;
            priceLineItems.add(lineitem);
            break;
        }     
        
        Test.startTest();
            EP_PricingResponseHandler.upsertOrderLineItems(priceLineItems, SEQ_ID);
        Test.stopTest();  
        
        csord__Order_Line_Item__c orderItemUpdate = [select Id, Quantity__c from csord__Order_Line_Item__c where csord__Order__r.Id = : orderObject.id limit 1];     
        System.assertEquals(300 , orderItemUpdate.Quantity__c);  
    }

    static testMethod void logExceptions_test() {
        EP_Error_Handler_CS__c setting = new EP_Error_Handler_CS__c();
        setting.Name = 'Error';
        setting.Severity__c = 'Error';
        setting.Value__c = 5;
        Database.insert(setting); 
        EP_Error_Handler_CS__c threshold  = new EP_Error_Handler_CS__c();
        threshold.Name = 'Threshold';
        threshold.Severity__c = 'threshold';
        threshold.Value__c = 3;
        insert threshold; 
        Exception exp;
        try {
            integer quot = 1/0;
        } catch(Exception e) {
            exp = e;
        }
        
        Test.startTest();
            EP_PricingResponseHandler.logExceptions(exp, SEQ_ID);
        Test.stopTest();      
        
        List<EP_Exception_Log__c> exLog = [Select Id from EP_Exception_Log__c limit 1];  
        system.assertEquals(true,exLog.size()> 0);
    }
    
    static testMethod void createResponse_test() {
        Test.startTest();
            EP_PricingResponseHandler.createResponse(SEQ_ID, ERROR_CODE, ERROR_DEC);
        Test.stopTest();
        
        system.assertEquals(true,EP_PricingResponseHandler.ackResponseList.size()> 0);
    }

    static testMethod void deletetExistingInvoiceItems_test() {
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        csord__Order__c orderObject = EP_TestDataUtility.getSalesOrder();
        OrderItem orderItems = [select Id,priceBookEntry.ProductCode, EP_OrderItem_Number__c,
                                        Quantity,orderId,UnitPrice,EP_WinDMS_Line_ItemId__c, 
                                        order.Status,order.EP_Pricing_Status__c,priceBookEntryId,PricebookEntry.Product2.Name
                                        from OrderItem where order.Id = : orderObject.id][0];  
        orderItems.EP_Is_Standard__c = false;
        Database.update(orderItems);
        string orderNumber = orderObject.OrderNumber__c;
        
        Test.startTest();
            EP_PricingResponseHandler.deletetExistingInvoiceItems(orderNumber);
        Test.stopTest(); 
        
        list<orderItem> invoiceItemsDelList = orderItemMapper.getInvoiceItemsByOrderNumber(new set<string>{orderNumber});
        System.assertEquals(0,invoiceItemsDelList.size());
    }

    static testMethod void updatePricingResponse_test() {
        csord__Order__c orderObject = EP_TestDataUtility.getSalesOrder();
        string orderNumber = orderObject.OrderNumber__c;
        
        Test.startTest();
            EP_PricingResponseHandler.updatePricingResponse(orderNumber, ERROR_DEC, SEQ_ID);
        Test.stopTest();
        
        //String result = [select id,EP_Pricing_Error__c from OrderItem where csord__Order__c.OrderNumber =: orderObject.OrderNumber][0].EP_Pricing_Error__c;
        //System.assertNotEquals(null,result);
    }

    static testMethod void updatePricingOnOrderItems_test() {
        csord__Order__c orderObject = EP_TestDataUtility.getSalesOrder();
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        String orderNumber = orderObject.OrderNumber__c;
        ERROR_DEC = 'An error message alerts users of a problem that has already occurred. By contrast, a warning message alerts users of a condition that might cause a problem in the future. Error messages can be presented using modal dialog boxes, in-place messages, notifications, or balloons.';
        
        Test.startTest();
            EP_PricingResponseHandler.updatePricingOnOrderItems(orderNumber, ERROR_DEC);
        Test.stopTest();
        
        //String result = [select Id, EP_Pricing_Error__c from csord__Order_Line_Item__c where OrderNumber__c =: orderObject.OrderNumber__c][0].EP_Pricing_Error__c;
        //System.assertNotEquals(null,result);
    }


    static testMethod void updatePricingOnOrder_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_PricingResponseHandler localObj = new EP_PricingResponseHandler();        
        csord__Order__c orderObject =  EP_TestDataUtility.getCurrentVMIOrder();
        orderObject.Status__c = EP_OrderConstant.OrderState_Planned;
        update orderObject;
        
        Test.startTest();
            EP_PricingResponseHandler.updatePricingOnOrder(orderObject.OrderNumber__c, SEQ_ID, '');
        Test.stopTest();
        
        csord__Order__c result = [SELECT Id, OrderNumber__c, EP_Pricing_Status__c FROM csord__Order__c WHERE Id =: orderObject.Id];
        System.assertEquals(true, result.EP_Pricing_Status__c != null);
    }
    
    static testMethod void updatePricingOnOrder_NegativeTest() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_PricingResponseHandler localObj = new EP_PricingResponseHandler();        
        csord__Order__c orderObject =  EP_TestDataUtility.getCurrentVMIOrder();
        orderObject.Status__c = EP_OrderConstant.OrderState_Submitted;
        update orderObject;
        ERROR_DEC = 'An error message alerts users of a problem that has already occurred. By contrast, a warning message alerts users of a condition that might cause a problem in the future. Error messages can be presented using modal dialog boxes, in-place messages, notifications, or balloons.';
        
        Test.startTest();
            EP_PricingResponseHandler.updatePricingOnOrder(orderObject.OrderNumber__c, SEQ_ID, ERROR_DEC);
        Test.stopTest();
        
        csord__Order__c result = [SELECT Id , OrderNumber__c, EP_Pricing_Status__c  FROM csord__Order__c WHERE Id =: orderObject.id limit 1];
        System.assertEquals(null, result.EP_Pricing_Status__c);
    }
}