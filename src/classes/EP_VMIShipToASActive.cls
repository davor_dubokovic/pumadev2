/*
 *  @Author <Accenture>
 *  @Name <EP_VMIShipToASActive>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 05-Active Status>
 *  @NovaSuite Fix -- comments added
 *  @Version <1.0>
 */
 public with sharing class EP_VMIShipToASActive extends EP_AccountState{
    /***NOvasuite fix constructor removed**/

    /*
     *  @Author <Accenture>
     *  @Name setAccountDomainObject
     */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASActive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
    /*
     *  @Author <Accenture>
     *  @Name doOnEntry
     */
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASActive','doOnEntry');
        EP_AccountService service = new EP_AccountService(this.account);
         system.debug('account.isUpdatedFromNAV()>>'+account.isUpdatedFromNAV());
            system.debug('EP_IntegrationUtil.ISERRORSYNC>>'+EP_IntegrationUtil.ISERRORSYNC);
        if(this.account.isStatuschanged()){
          service.doActionInsertPlaceHolderTankDips();
          //code Changes for #45362 Start
          if(this.account.isUnblocked()) {
            service.doActionSendUpdateRequestToNavAndLomo();
          }
          //code Changes for #45362 End
        }
        // Changes made for CUSTOMER MODIFICATION L4 Start
        else if (!account.isUpdatedFromNAV() && !EP_IntegrationUtil.ISERRORSYNC){
           
            service.doActionSendRequest();
        }
        // Changes made for CUSTOMER MODIFICATION L4 End
    }
    /*
     *  @Author <Accenture>
     *  @Name doOnExit
     */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASActive','doOnExit');
        
    }
     /*
     *  @Author <Accenture>
     *  @Name doTransition***/     
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASActive','doTransition');
        return super.doTransition();
    }
    /*
     *  @Author <Accenture>
     *  @Name isInboundTransitionPossible*/     
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_VMIShipToASActive','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}