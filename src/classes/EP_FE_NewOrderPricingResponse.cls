/* 
   @Author <Nicola Tassini>
   @name <EP_FE_NewOrderPricingResponse>
   @CreateDate <24/04/2016>
   @Description <Response for the Pricing API>  
   @Version <1.0>
*/
global with sharing class EP_FE_NewOrderPricingResponse extends EP_FE_Response {


    global static final Integer ERROR_CREATING_ORDER = -1;
    global static final Integer ERROR_INSERTING_ORDER = -2;
    global static final Integer ERROR_INSERTING_ORDER_ITEM = -3;
    global static final Integer ERROR_DELETING_ORDER_ITEM = -4;
    global static final Integer ERROR_FETCHING_TIME = -5;
    global static final Integer ERROR_ORDER_DATE_CANNOT_BE_NULL = -6;

    global static final String CLASSNAME = 'EP_FE_NewOrderPricingEndpoint';
    global static final String METHOD1 = 'calculateOrderPrice';
    global static final String METHOD2 = 'validateRequestParameters';
    global static final String METHOD3 = 'createNewOrderDelivery';
    global static final String METHOD4 = 'createNewOrderExRack';
    global static final String METHOD5 = 'dropOldOrderItems';
    global static final String METHOD6 = 'createNewOrderItem';
    global static final String METHOD7 = 'getRequestedDeliveryDate';
    global final static String SEVERITY = 'ERROR';
    global final static String TRANSACTION_ID = 'PE-191';
    global final static String DESCRIPTION1 = 'Error occured while inserting/creating Order';
    global final static String DESCRIPTION2 = 'Error occured while inserting/creating Order Item';
    global final static String DESCRIPTION3 = 'Error occured while deleting Order Item';
    global final static String DESCRIPTION4 = 'Error in fetching Time';
    global final static String DESCRIPTION5 = 'Order Date can not be null';
    
    // Price for each item
    public List<EP_FE_OrderItem> orderItems;

    // Order Id
    public Id draftOrderId;
    // Returns true if prices are ready for the calculation data
    public Boolean isPriceCalculated;
    // Returns false if user don't 
    public Boolean canSeePrices;

    // Discount
    public Decimal discountPercentage;
    public Decimal discountAmount;

    // Total
    public Decimal totalAmount;
    public Decimal totalTax;
/* 
   @Author <Nicola Tassini>
   @name <>
   @CreateDate <//2016>
   @Description <>  
   @Version <1.0>
*/
    global with sharing class EP_FE_OrderItem {
        public Id productId {get; set;}
        public String productName {get; set;}
        public String productUnitOfMeasure {get; set;}
        public Decimal price {get; set;}
        public Decimal amount {get; set;}
        public Decimal pricePerUnit {get; set;}
        public Decimal freightPrice {get; set;}
        public List<EP_FE_TaxItem> taxItems;

/* 
   @Author <Nicola Tassini>
   @name <>
   @CreateDate <//2016>
   @Description <>  
   @Version <1.0>
*/
        public EP_FE_OrderItem(Product2 product, Decimal price, Decimal amount, 
                Decimal pricePerUnit, Decimal freightPrice, List<EP_FE_TaxItem> taxItems) {
            if(product != null) {
                this.productId = product.Id;
                this.productName = product.Name;
                this.productUnitOfMeasure = product.EP_Unit_of_Measure__c;
            }
            
            this.amount = amount;
            this.price = price;
            this.pricePerUnit = pricePerUnit;
            this.freightPrice = freightPrice;
            this.taxItems = taxItems;
        }       
    } 
/* 
   @Author <Nicola Tassini>
   @name <>
   @CreateDate <//2016>
   @Description <>  
   @Version <1.0>
*/
    global with sharing class EP_FE_TaxItem {
        public String name;
        public Decimal amount;
/* 
   @Author <Nicola Tassini>
   @name <>
   @CreateDate <//2016>
   @Description <>  
   @Version <1.0>
*/
        public EP_FE_TaxItem(String name, Decimal amount) {
            this.name = name;
            this.amount = amount;
        }
    }

/* 
   @Author <Nicola Tassini>
   @name <>
   @CreateDate <//2016>
   @Description <>  
   @Version <1.0>
*/    
global EP_FE_NewOrderPricingResponse() {
        this.orderItems = new List<EP_FE_OrderItem>();
        this.isPriceCalculated = true;
    }

}