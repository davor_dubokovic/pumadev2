/* 
  @Author <Spiros Markantonatos>
   @name <EP_SelectTankDipSitePageExtensionClass_Test>
   @CreateDate <06/11/2014>
   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
   @Version <1.0>
 
*/
@isTest
private class EP_SelectTankDipSitePageExtClass_R1_Tes{

    private static Account accObj; 
    private static EP_Tank_Dip__c tankDip; 
    private static EP_Tank__c TankInstance;
    private static EP_Tank__c TankInstance1;
    /**************************************************************************
    *@Description : This method is used to create Data.                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/    
    private static void init(){
     EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(2);
     accObj=customHierarchy.lShipToAccounts[0];
     TankInstance=customHierarchy.lTanks[0];     
     
    }
    
    /**************************************************************************
    *@Description : This method is used to update the Account record and Tank
                    record to meet the criteria for the page and asserts the url after clicking on back button.                                 *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/
    
   /* private static testMethod void checkCancelButtonFunctionality(){
        //CREATE CSC USER
         Profile cscAgent = [Select id from Profile Where Name =:EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
         User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
         System.runAs(cscUser) { 
            //Instantiating the Utility class
            EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(2);
            //Updating the Account record with Active Status
            accObj=customHierarchy.lShipToAccounts[0];
            accObj.recordType = [Select id,name from RecordType where sObjectType='Account' and RecordType.developerName= 'EP_VMI_SHIP_TO' limit 1];
            accObj.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE ;
            update accObj;
            //Updating the tank record to be 'Operational'
            TankInstance=customHierarchy.lTanks[0];
            TankInstance.EP_Tank_Status__c= 'Operational';
            update TankInstance;
            //Creating a Tank Dip record in association with above Tank record
            EP_Tank_Dip__c tdip = new EP_Tank_Dip__c(EP_Unit_Of_Measure__c = 'LT',EP_Ambient_Quantity__c = 5,EP_Tank__c=TankInstance.id,EP_Reading_Date_Time__c=System.today() - 2,EP_Reading_Date_Time_Local__c = System.today()-2);
            insert tdip;
            Test.startTest();
            ApexPages.currentPage().getParameters().put('id', accobj.id);
            EP_SelectTankDipSitePageExtClass_R1 controller = new EP_SelectTankDipSitePageExtClass_R1();
            controller.retrieveSiteInformation();
            PageReference pgref=controller.cancel();
            Test.stopTest();
            System.assertEquals('/home/home.jsp',pgRef.getUrl()); //Asserting url after clicking on cancel button
        }       
        
        
    } */
 
   /**************************************************************************
    *@Description : If there is only one active tank and the user will redirect to tank date page by skiping selecting site page.                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/
    private static testMethod void checkIfOneActiveAccount(){
        //CREATE CSC USER
     Profile cscAgent = [Select id from Profile Where Name =:EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
     User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
     System.runAs(cscUser) { 
        EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(1);
        customHierarchy.lShipToAccounts[0].EP_Status__c =EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update customHierarchy.lShipToAccounts[0];
        customHierarchy.lShipToAccounts[0].EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;//Updating 1st account as active
        update customHierarchy.lShipToAccounts[0];
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id', customHierarchy.lShipToAccounts[0].id);
        EP_SelectTankDipSitePageExtClass_R1 controller = new EP_SelectTankDipSitePageExtClass_R1();
        PageReference pgrf=controller.retrieveSiteInformation();
        Test.stopTest();
        System.assertEquals(true,customHierarchy.lShipToAccounts.size()==1);
        System.assertEquals('/apex/EP_SelectTankDipDatePage_R1?id='+customHierarchy.lShipToAccounts[0].id,pgrf.getUrl());//Asserting tank date page for one active tank
        
        }
        
    }
    
    /**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/        
   /*private static testMethod void testNoOfDates(){ 
     //CREATE CSC USER
     Profile cscAgent = [Select id from Profile Where Name =:EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
     User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
     System.runAs(cscUser) {    
         init();   
         List<EP_Tank_Dip__c> tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance);
         Test.startTest();
         Account acc =[Select id from account where id=:accObj.id];
         acc.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
         update acc;
         System.debug('test class list size'+tankListDips.size());
         tankListDips[0].EP_Reading_Date_Time__c=System.today()-4;//Updating tank date as today minus 4days
         tankListDips[0].EP_Is_Placeholder_Tank_Dip__c=true;
         update  tankListDips[0]; 
         tankListDips[1].EP_Reading_Date_Time__c=System.today()-5;//updating tank date as today minus 20days
         tankListDips[1].EP_Is_Placeholder_Tank_Dip__c=true;
         update  tankListDips[1];
         tankListDips[2].EP_Reading_Date_Time__c=System.today()-6;//Updating tank date as today minus 6days
         tankListDips[2].EP_Is_Placeholder_Tank_Dip__c=true;
         update  tankListDips[2];
         PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
         Test.setCurrentPage(pageRef);//Applying page context here  
         ApexPages.currentPage().getParameters().put('id', accObj.Id);
         EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
         PageReference pgRef=controller.retrieveMissingSiteDates();
         Test.stopTest(); 
         System.debug('size dates'+controller.tankDipDates.size());
         System.debug('Tank dip dates  '+controller.tankDipDates);
         System.assertEquals(true,tankListDips.size()==3); //Asserting actual tank dip size
         System.assertEquals(true,controller.tankDipDates.size()==4);//Assering missing tank dips
      }   
      
  }*/
     
/**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
**************************************************************************/        
  /*  private static testMethod void testCautionStatus(){ 
     //CREATE CSC USER
     Profile cscAgent = [Select id from Profile Where Name =:EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
     User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
     System.runAs(cscUser) {    
         init();   
         List<EP_Tank_Dip__c> tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance);
         Test.startTest();
         Account acc =[Select id from account where id=:accObj.id];
         acc.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
         update acc;
         System.debug('test class list size'+tankListDips.size());
         tankListDips[0].EP_Reading_Date_Time__c=System.today()-4;//Updating tank date as today minus 4days
         tankListDips[0].EP_Is_Placeholder_Tank_Dip__c=true;
         update  tankListDips[0]; 
         tankListDips[1].EP_Reading_Date_Time__c=System.today()-5;//updating tank date as today minus 20days
         tankListDips[1].EP_Is_Placeholder_Tank_Dip__c=true;
         update  tankListDips[1];
         tankListDips[2].EP_Reading_Date_Time__c=System.today()-6;//Updating tank date as today minus 6days
         tankListDips[2].EP_Is_Placeholder_Tank_Dip__c=true;
         update  tankListDips[2];
         PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
         ApexPages.currentPage().getParameters().put('id', accObj.Id);
         EP_SelectTankDipSitePageExtClass_R1 siteObj=new EP_SelectTankDipSitePageExtClass_R1();
         EP_SelectTankDipSitePageExtClass_R1.shipToClass siteObj1=new EP_SelectTankDipSitePageExtClass_R1.shipToClass();
         pageRef=siteObj.retrieveSiteInformation();
         System.assertEquals(siteObj.shipTos[0].siteShowCautionStatus,true); 
         pageRef = Page.EP_SelectTankDipDatePage_R1;
         Test.setCurrentPage(pageRef);//Applying page context here  
         ApexPages.currentPage().getParameters().put('id', accObj.Id);
         EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
         PageReference pgRef=controller.retrieveMissingSiteDates();
         System.debug('controller.tankDipDates'+controller.tankDipDates);
         System.debug('controller.tankDipDates[0].dateText'+controller.tankDipDates[0].dateText);
         System.debug('accObj.Id'+accObj.Id);
         pageRef = Page.EP_TankDipSubmitPage_R1;
         Test.setCurrentPage(pageRef);//Applying page context here 
         ApexPages.currentPage().getParameters().put('id', accObj.Id);
         ApexPages.currentPage().getParameters().put('date',controller.tankDipDates[0].dateText);
         EP_TankDipSubmitPageControllerClass_R1 controller1=new EP_TankDipSubmitPageControllerClass_R1();
         EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass  controller2=new EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass();
         Test.stopTest(); 
         System.debug('controller2.displayedTankDipLines.size()  '+controller1.displayedTankDipLines.size()+' controller2.displayedTankDipLines   '+ controller1.displayedTankDipLines);
         System.assertEquals(controller1.displayedTankDipLines[0].dipShowCautionStatus,true); //Asserting actual tank dip size
         System.assertEquals(controller1.displayedTankDipLines.size(),1);
      }   
      
  } */
  /**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
**************************************************************************/        
    private static testMethod void testDipWarningStatus(){ 
     //CREATE CSC USER
     Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_OrderStatusNAVMapping__c.SobjectType,'EP_OrderStatusUpdateNAV');
     Profile cscAgent = [Select id from Profile Where Name =:EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
     User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
     System.runAs(cscUser) {    
         init(); 
         Account acc =[Select id from account where id=:accObj.id];
         acc.EP_Status__c =EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
         update acc;  
         acc.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
         update acc;
         List<EP_Tank_Dip__c> tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance);
         Test.startTest();
         //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
         //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
         //EP_IntegrationRecordTriggerHandler.isExecuteAfterInsert = true;
         System.debug('test class list size'+tankListDips.size());
         PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
         Test.setCurrentPage(pageRef);//Applying page context here  
         ApexPages.currentPage().getParameters().put('id', accObj.Id);
         EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
         PageReference pgRef=controller.retrieveMissingSiteDates();
         System.debug('controller.tankDipDates'+controller.tankDipDates);
         System.debug('controller.tankDipDates[0].dateText'+ system.now().format('yyyyMMddHHmmss'));
         System.debug('accObj.Id'+accObj.Id);
         pageRef = Page.EP_TankDipSubmitPage_R1;
         Test.setCurrentPage(pageRef);//Applying page context here 
         ApexPages.currentPage().getParameters().put('id', accObj.Id);
         System.debug('controller.tankDipDates'+controller.tankDipDates);
         ApexPages.currentPage().getParameters().put('date',system.now().format('yyyyMMddHHmmss'));
         EP_TankDipSubmitPageControllerClass_R1 controller1=new EP_TankDipSubmitPageControllerClass_R1();
         EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass  controller2=new EP_TankDipSubmitPageControllerClass_R1.tankDipRecordClass();
         Test.stopTest(); 
         System.debug('controller2.displayedTankDipLines.size()  '+controller1.displayedTankDipLines.size()+' controller2.displayedTankDipLines   '+ controller1.displayedTankDipLines);
         System.assertEquals(controller1.displayedTankDipLines.size(),1);
         //System.assertEquals(controller1.displayedTankDipLines[0].dipShowWarningStatus,true); //Asserting actual tank dip size
         
      }   
      
  }
/**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
**************************************************************************/        
    private static testMethod void testSiteWarningStatus(){ 
     //CREATE CSC USER
         Test.startTest();
         //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
         //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
         EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = true;
        
     Profile cscAgent = [Select id from Profile Where Name =:EP_Common_Constant.CSC_AGENT_PROFILE  Limit 1];
     User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
     ID RECORDTYPEID_PLACEHOLDER = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();
     System.runAs(cscUser) {   
         EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(2);
         customHierarchy.lShipToAccounts[0].EP_Status__c =EP_Common_Constant.STATUS_BASIC_DATA_SETUP;//Updating 1st account as Inactive
         customHierarchy.lShipToAccounts[1].EP_Status__c =EP_Common_Constant.STATUS_BASIC_DATA_SETUP;//Updating 1st account as Inactive
         customHierarchy.lShipToAccounts[0].EP_Tank_Dips_Schedule_Time__c = '06:00';
         customHierarchy.lShipToAccounts[1].EP_Tank_Dips_Schedule_Time__c = '06:00';
         customHierarchy.lShipToAccounts[0].EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
         customHierarchy.lShipToAccounts[1].EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
         update customHierarchy.lShipToAccounts[0];  
         update customHierarchy.lShipToAccounts[1];
         customHierarchy.lShipToAccounts[0].EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;//Updating 1st account as Inactive
         customHierarchy.lShipToAccounts[1].EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;//Updating 1st account as Inactive
         update customHierarchy.lShipToAccounts[0];  
         update customHierarchy.lShipToAccounts[1]; 
         TankInstance=customHierarchy.lTanks[0];
         TankInstance1=customHierarchy.lTanks[1];
         List<EP_Tank_Dip__c> tankListDips=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance);
         List<EP_Tank_Dip__c> tankListDips1=EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),new EP_Tank_Dip__c(),new EP_Tank_Dip__c()},TankInstance1);
         tankListDips[0].EP_Reading_Date_Time__c=System.today()-4;//Updating tank date as today minus 4days
         tankListDips[0].RecordTypeId = RECORDTYPEID_PLACEHOLDER ;
         update  tankListDips[0]; 
         tankListDips[1].EP_Reading_Date_Time__c=System.today()-5;//updating tank date as today minus 20days
         tankListDips[1].RecordTypeId = RECORDTYPEID_PLACEHOLDER ;
         update  tankListDips[1];
         tankListDips1[2].EP_Reading_Date_Time__c=System.today();//Updating tank date as today minus 6days
         tankListDips1[2].RecordTypeId = RECORDTYPEID_PLACEHOLDER ;
         update  tankListDips[2];
         PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
         ApexPages.currentPage().getParameters().put('id', customHierarchy.lShipToAccounts[1].Id);
         EP_SelectTankDipSitePageExtClass_R1 siteObj=new EP_SelectTankDipSitePageExtClass_R1();
         EP_SelectTankDipSitePageExtClass_R1.shipToClass siteObj1=new EP_SelectTankDipSitePageExtClass_R1.shipToClass();
         pageRef=siteObj.retrieveSiteInformation(); 
         Test.stopTest();
         System.assertEquals(siteObj.shipTos[0].siteshowCautionStatus,true); 
 
      }   
      
  }    
   
}