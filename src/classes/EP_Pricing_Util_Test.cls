/**
 * @author <Prateek Jain>
 * @name <EP_OrderStatus>
 * @createDate <20/10/2016>
 * @description
 * @version <1.0>
 */
@isTest
public  with sharing class EP_Pricing_Util_Test {
    private static Account sellToAccount,nonVmiShipToAccount;
    private static Order order1;
    public static Account sellTo,billTo,nonVMIship,storageLoc,vendor1;
    public static EP_Country__c country1 = new EP_Country__c();
    public static String COUNTRY_NAME = 'Australia';
    public static String COUNTRY_CODE = 'GBP';
    public static String COUNTRY_REGION = 'Australia';
    public static String REGION_NAME = 'North-Australia';
    public static EP_Freight_Matrix__c f1 = new EP_Freight_Matrix__c();
    public static EP_Stock_Holding_Location__c supplyOption;
    public static EP_Stock_Holding_Location__c supplyOption1;



 private static testMethod void calculatePriceForOrderLineItems_Test22(){
     
                 System.runAs(EP_TestDataUtility.createRunAsUser()) {

        Id orderVMIRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();
        
        
        //EP_OrderUpdateHandler.AfterUpdateOrder = true;
        
         EP_Payment_Method__c paymentMethod = EP_TestDataUtility.createPaymentMethod('Cash Payment'
                                                                                        ,'CP');
        insert paymentMethod;
        
        EP_Payment_Term__c paymentTerm =  EP_TestDataUtility.createPaymentTerm();
        insert paymentTerm;
        
        
        
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet'); 
        /////////Insert freightMatrix //////
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix, false);
        
        Id pricebookId = Test.getStandardPricebookId();
        
         // create sell to Account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, freightMatrix.id);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_PriceBook__c = pricebookId;
        insert sellToAccount;
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Company__c comp = EP_TestDataUtility.createCompany('Aun44');
        comp.EP_Disallow_DD_for_PP_customers__c = true;
        database.insert(comp);
                     
                     
        Company__c comp2 = EP_TestDataUtility.createCompany('AUN22');
        comp2.EP_Disallow_DD_for_PP_customers__c = true;
        database.insert(comp2);
     
        // create pricebook
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true,EP_Company__c=comp.ID);
        insert customPB;
        
        // insert product
       
        
        Product2  productPetrolObj = new product2(Name = 'FreightPrice', CurrencyIsoCode = 'GBP', 
        isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Blended__c = false,
        EP_Company_lookup__c = comp.id);                           
        insert productPetrolObj;
        
      
        
     
        country1 = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country1);
        //Creating Data for Sell To Account 
        Decimal dec = 19.23;
            Pricebook2 customPB1 = new Pricebook2(Name='myPB123',CurrencyIsoCode='GBP', isActive=true,EP_Company__c=comp.ID);
            insert customPB1; 
            PricebookEntry prodPricebookEntry = new PricebookEntry(
            Pricebook2Id = customPB1.Id, Product2Id = productPetrolObj.Id,
            UnitPrice = 12000,EP_Additional_Taxes__c= dec,CurrencyIsoCode='GBP', IsActive = false);
            insert prodPricebookEntry;

            //prodPricebookEntry.IsActive=true;
                     
            //update prodPricebookEntry;
            Pricebook2 customPB2 = new Pricebook2(Name='myPBSellTo123',CurrencyIsoCode='GBP', isActive=true,EP_Company__c=comp2.ID);
            insert customPB2; 
            PricebookEntry prodSellToPricebookEntry = new PricebookEntry(
            Pricebook2Id = customPB2.Id,EP_Additional_Taxes__c= dec, Product2Id = productPetrolObj.Id,
            UnitPrice = 12000,CurrencyIsoCode='GBP', IsActive = false);
            insert prodSellToPricebookEntry;
    Test.startTest();
            //Bill-to
            billTo = EP_TestDataUtility.createBillToAccount();
            billTo.EP_Country__c = country1.id;
            billTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
     
            billTo.EP_Puma_Company__c = comp.id;
            //billTo.EP_PriceBook__c = pricebookId;
            billTo.EP_PriceBook__c  = customPB.id;
            insert billTo;
            system.debug('--recordtype--'+billTo.recordtypeid);
            billTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(billTo,true);
            billTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(billTo,true);
            f1 = EP_TestDataUtility.createFreightMatrix();
            Database.insert(f1,true);
            sellTo = EP_TestDataUtility.createSellToAccount(billTo.Id,f1.Id);
            sellTo.EP_Country__c = country1.id;
            sellTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
            sellTo.EP_Puma_Company__c = comp.id;
            sellTo.EP_Payment_Method__c = paymentMethod.id;
            sellTo.EP_Requested_Payment_Terms__c = 'Credit';
            sellTo.EP_Recommended_Credit_Limit__c = 100;
            sellTo.EP_Bill_To_Account__c = billTo.Id;
            //sellTo.EP_Alternative_Payment_Method__c = 'Paylink';
            sellTo.EP_Excise_duty__c = 'Excise Free';
            sellTo.EP_PriceBook__c = customPB1.id;
            // sellTo.EP_PriceBook__c = pricebookId;
            Database.saveResult sv = Database.insert(sellTo);
        Test.stopTest();
          Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        order1 = EP_TestDataUtility.createOrder(sellTo.Id, orderRecTypeId, customPB1.id);
        insert order1;
        
        System.AssertEquals(True, [Select id , isActive from PricebookEntry where id=:prodPricebookEntry.id].IsActive);
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = EP_TestDataUtility.createProduct('Diesel');
        Product2 taxProductObj = EP_TestDataUtility.createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        insert (productsToBeInserted);
        
        List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = EP_TestDataUtility.createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = EP_TestDataUtility.createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
       // insert pricebookEntryToBeInserted;
      
                 
       
        List<OrderItem> orditemList = new List<OrderItem>();
        OrderItem orderItemObj = EP_TestDataUtility.createOrderItemFromPriceBookEntry(prodPricebookEntry.Id, order1.Id);
        orderItemObj.EP_Is_Standard__c = true;
        insert orderItemObj;
        orditemList.add(orderItemObj);
        order1.EP_WinDMS_Status__c='Delivered';
        update order1;
        
        
        // Creating ShipTo Account
 
        Id recordtypeid = EP_Common_Util.fetchRecordTypeId('Account','Non-VMI Ship To');
        nonVMIship = EP_TestDataUtility.createShipToAccount(sellTo.id,recordtypeid);
        nonVMIship.name = 'NVMI1';
        nonVMIship.EP_Puma_Company__c = comp.id;
        nonVMIship.EP_Country__c = country1.id;
        //nonVMIship.EP_PriceBook__c = pricebookId;
        nonVMIship.EP_PriceBook__c = customPB.id;
        nonVMIship.EP_Ship_To_Type__c = 'Consignment';
        nonVMIship.EP_Duty__c = 'Excise Free';
        insert nonVMIship;
        nonVMIship.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVMIship);
        
        
        //Creating SupplyLocation
        
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true LIMIT : EP_Common_Constant.ONE];
        
        storageLoc = EP_TestDataUtility.createStorageLocAccount(country1.Id,bhrs.Id);
        insert storageLoc;
                    
         Id supplyLocSellToRecordtypeid = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.EX_RACK_REC_TYPE_NAME);
        supplyOption1 = EP_TestDataUtility.createSellToStockLocation(sellTo.id,true,storageLoc.Id,supplyLocSellToRecordtypeid); //Add one SHL
        supplyOption1.EP_Trip_Duration__c = 1;
        //supplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
        supplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
        supplyOption1.EP_Is_Pickup_Enabled__c = False;
        //supplyOption1.EP_Duty__c = 'Excise Paid';       
        Database.insert(supplyOption1,true);
        /*         
        //Adding Code for Testing Util_Pricing class
        EP_Pricing_Util.calculatePriceForOrderLineItems(orditemList,sellToAccount.id,order1.Id,customPB1.id,'GBP',EP_Common_Constant.DELIVERY,nonVMIship.id,supplyOption1.id);
        */
                 }   
    }
    
    
    private static testMethod void calculatePriceForOrderLineItems_Test(){
     
                 System.runAs(EP_TestDataUtility.createRunAsUser()) {

        Id orderVMIRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();
        
        
        //EP_OrderUpdateHandler.AfterUpdateOrder = true;
        
         EP_Payment_Method__c paymentMethod = EP_TestDataUtility.createPaymentMethod('Cash Payment'
                                                                                        ,'CP');
        insert paymentMethod;
        
        EP_Payment_Term__c paymentTerm =  EP_TestDataUtility.createPaymentTerm();
        insert paymentTerm;
        
        
        
        Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet'); 
        /////////Insert freightMatrix //////
        EP_Freight_Matrix__c freightMatrix = EP_TestDataUtility.createFreightMatrix();
        database.insert(freightMatrix, false);
        
        Id pricebookId = Test.getStandardPricebookId();
        
         // create sell to Account
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, freightMatrix.id);
        sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
        sellToAccount.EP_PriceBook__c = pricebookId;
        insert sellToAccount;
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Company__c comp = EP_TestDataUtility.createCompany('Aun44');
        comp.EP_Disallow_DD_for_PP_customers__c = true;
        database.insert(comp);
                     
                     
        Company__c comp2 = EP_TestDataUtility.createCompany('AUN22');
        comp2.EP_Disallow_DD_for_PP_customers__c = true;
        database.insert(comp2);
     
        // create pricebook
        Pricebook2 customPB = new Pricebook2(Name='Standard Price Book', isActive=true,EP_Company__c=comp.ID);
        insert customPB;
        
        // insert product
       
        
        Product2  productPetrolObj = new product2(Name = 'TaxProduct', CurrencyIsoCode = 'GBP', 
        isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Blended__c = false,
        EP_Company_lookup__c = comp.id);                           
        insert productPetrolObj;
        
      
        
     
        country1 = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country1);
        //Creating Data for Sell To Account 
        Decimal dec = 19.23;
            Pricebook2 customPB1 = new Pricebook2(Name='myPB123',CurrencyIsoCode='GBP', isActive=true,EP_Company__c=comp.ID);
            insert customPB1; 
            PricebookEntry prodPricebookEntry = new PricebookEntry(
            Pricebook2Id = customPB1.Id, Product2Id = productPetrolObj.Id,
            UnitPrice = 12000,EP_Additional_Taxes__c= dec,CurrencyIsoCode='GBP', IsActive = false);
            insert prodPricebookEntry;

            //prodPricebookEntry.IsActive=true;
                     
            //update prodPricebookEntry;
            Pricebook2 customPB2 = new Pricebook2(Name='myPBSellTo123',CurrencyIsoCode='GBP', isActive=true,EP_Company__c=comp2.ID);
            insert customPB2; 
            PricebookEntry prodSellToPricebookEntry = new PricebookEntry(
            Pricebook2Id = customPB2.Id,EP_Additional_Taxes__c= dec, Product2Id = productPetrolObj.Id,
            UnitPrice = 12000,CurrencyIsoCode='GBP', IsActive = false);
            insert prodSellToPricebookEntry;
            Test.startTest(); 
            //Bill-to
            billTo = EP_TestDataUtility.createBillToAccount();
            billTo.EP_Country__c = country1.id;
            billTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
     
            billTo.EP_Puma_Company__c = comp.id;
            //billTo.EP_PriceBook__c = pricebookId;
            billTo.EP_PriceBook__c  = customPB.id;
            insert billTo;
            system.debug('--recordtype--'+billTo.recordtypeid);
            billTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(billTo,true);
            billTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(billTo,true);
            f1 = EP_TestDataUtility.createFreightMatrix();
            Database.insert(f1,true);
            sellTo = EP_TestDataUtility.createSellToAccount(billTo.Id,f1.Id);
            sellTo.EP_Country__c = country1.id;
            sellTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
            sellTo.EP_Puma_Company__c = comp.id;
            sellTo.EP_Payment_Method__c = paymentMethod.id;
            sellTo.EP_Requested_Payment_Terms__c = 'Credit';
            sellTo.EP_Recommended_Credit_Limit__c = 100;
            sellTo.EP_Bill_To_Account__c = billTo.Id;
            //sellTo.EP_Alternative_Payment_Method__c = 'Paylink';
            sellTo.EP_Excise_duty__c = 'Excise Free';
            sellTo.EP_PriceBook__c = customPB1.id;
            // sellTo.EP_PriceBook__c = pricebookId;
            Database.saveResult sv = Database.insert(sellTo);
        
          Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        order1 = EP_TestDataUtility.createOrder(sellTo.Id, orderRecTypeId, customPB1.id);
        insert order1;
        
        System.AssertEquals(True, [Select id , isActive from PricebookEntry where id=:prodPricebookEntry.id].IsActive);
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = EP_TestDataUtility.createProduct('Diesel');
        Product2 taxProductObj = EP_TestDataUtility.createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        insert (productsToBeInserted);
        
        List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = EP_TestDataUtility.createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = EP_TestDataUtility.createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
       // insert pricebookEntryToBeInserted;
      
                 
       
        List<OrderItem> orditemList = new List<OrderItem>();
        OrderItem orderItemObj = EP_TestDataUtility.createOrderItemFromPriceBookEntry(prodPricebookEntry.Id, order1.Id);
        orderItemObj.EP_Is_Standard__c = true;
        insert orderItemObj;
        orditemList.add(orderItemObj);
        order1.EP_WinDMS_Status__c='Delivered';
        update order1;
        
        
        // Creating ShipTo Account

        Id recordtypeid = EP_Common_Util.fetchRecordTypeId('Account','Non-VMI Ship To');
        nonVMIship = EP_TestDataUtility.createShipToAccount(sellTo.id,recordtypeid);
        nonVMIship.name = 'NVMI1';
        nonVMIship.EP_Puma_Company__c = comp.id;
        nonVMIship.EP_Country__c = country1.id;
        //nonVMIship.EP_PriceBook__c = pricebookId;
        nonVMIship.EP_PriceBook__c = customPB.id;
        nonVMIship.EP_Ship_To_Type__c = 'Consignment';
        nonVMIship.EP_Duty__c = 'Excise Free';
        insert nonVMIship;
        nonVMIship.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVMIship);
        
        
        //Creating SupplyLocation
        
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true LIMIT : EP_Common_Constant.ONE];
        
        storageLoc = EP_TestDataUtility.createStorageLocAccount(country1.Id,bhrs.Id);
        insert storageLoc;
                   
         Id supplyLocSellToRecordtypeid = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.EX_RACK_REC_TYPE_NAME);
        supplyOption1 = EP_TestDataUtility.createSellToStockLocation(sellTo.id,true,storageLoc.Id,supplyLocSellToRecordtypeid); //Add one SHL
        supplyOption1.EP_Trip_Duration__c = 1;
        //supplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
        supplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
        supplyOption1.EP_Is_Pickup_Enabled__c = False;
        //supplyOption1.EP_Duty__c = 'Excise Paid';       
        Database.insert(supplyOption1,true);
                 
        //Adding Code for Testing Util_Pricing class
        //EP_Pricing_Util.calculatePriceForOrderLineItems(orditemList,sellToAccount.id,order1.Id,customPB1.id,'GBP',EP_Common_Constant.DELIVERY,nonVMIship.id,supplyOption1.id);
        Test.stopTest();
        List<Order> ordlist = new List<Order>();
                 }   
    }
    
    
    }