/**
  @Author : Accenture
  @Name : EP_OrderConfirmationXMLCtrlExtn
  @CreateDate : 02/15/2017
  @Description : This Controller Extension is used to generate XML message to send Order Confirmation to OPMS
  @Version 1.0
*/
public with sharing class EP_OrderConfirmationXMLCtrlExtn {
    @testvisible private map<id, Account> relatedAccountsMap = new map<id, Account>();
    private csord__Order__c ord;
    private string messageType;
    private string secretCode;
    
    public string messageId{get;set;}
    public string docTitle {get;set;}
    public Boolean isPriceDisplayed{get;set;}
    public csord__Order__c orderDetails{get;set;}
    
    public Company__c companyDetails;
    public Account storageLocAcct;
    public Account shipToAcct;
    public Account transporterAcct;
    public list<csord__Order_Line_Item__c> orderLineItems;

   /**
    * @Author       Accenture
    * @Name         EP_OrderConfirmationXMLCtrlExtn
    * @Date         02/08/2017
    * @Description  The extension constructor initializes the members variable orderObject by using the getRecord method from the standard controller. This method will be use setup the values for Header in XML message
    * @Param        ApexPages.StandardController
    * @return       
    */
    public EP_OrderConfirmationXMLCtrlExtn (ApexPages.StandardController stdController) {
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        Id orderId = stdController.getRecord().Id;
        this.ord = [SELECT Id, csord__Identification__c, EP_Puma_Company_Code__c, EP_Document_Title__c, OrderNumber__c FROM csord__Order__c WHERE Id = :orderId];
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        docTitle = EP_Common_Constant.BLANK;
        setOrderDetails();
        setRelatedAccountDetails();
    }
    
    /**
    * @Author       Accenture
    * @Name         getCompanyDetails
    * @Date         02/08/2017
    * @Description  This method is used to Company details for Order's Company
    * @Param        Sobject
    * @return       
    */
    public Company__c getCompanyDetails() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getCompanyDetails');
        EP_CompanyMapper mapperObj = new EP_CompanyMapper();
        companyDetails = mapperObj.getCompanyDetailsByCompanyCode(this.ord.EP_Puma_Company_Code__c);
        return companyDetails;
    }
    
   /**
    * @Author       Accenture
    * @Name         setOrderDetails
    * @Date         02/08/2017
    * @Description  This method is used set Order details
    * @Param        
    * @return       
    */
    @TestVisible
    private void setOrderDetails() {
        EP_GeneralUtility.Log('Private','EP_OrderConfirmationXMLCtrlExtn','setOrderDetails');
        this.orderDetails = new csord__Order__c();
        EP_OrderMapper mapperObj = new EP_OrderMapper();
        List<csord__Order__c> ordList = new List<csord__Order__c>();
        ordList = mapperObj.getCSRecordsByIds(new Set<Id>{this.ord.Id});
        if(!ordList.isEmpty()){
            this.orderDetails = ordList[0];
            isPriceDisplayed =  this.orderDetails.EP_Sell_To__r.EP_Display_Price__c;
            setDocTitle(); 
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         setRelatedAccountDetails
    * @Date         02/08/2017
    * @Description  This method is used to set all Accounts detail which are associated with Order
    * @Param :
    * @return       
    */
    @TestVisible
     private void setRelatedAccountDetails() {
        EP_GeneralUtility.Log('Private','EP_OrderConfirmationXMLCtrlExtn','setRelatedAccountDetails');
        set<id> accountIdSet = new set<id>();
        relatedAccountsMap = new map<id, Account>();
        
        accountIdSet.add(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c);
        accountIdSet.add(this.orderDetails.EP_ShipTo__c);
        accountIdSet.add(this.orderDetails.EP_Transporter__c);
        accountIdSet.add(this.orderDetails.csord__Account__c);
        
        EP_AccountMapper mapperObj = new EP_AccountMapper();
        for(Account accObj : mapperObj.getRecordsByIds(accountIdSet)) {
            relatedAccountsMap.put(accObj.id, accObj);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         getStorageLocAcct
    * @Date         02/08/2017
    * @Description  This method is used to get Storage Location details from Account
    * @Param :
    * @return        Account
    */
     public Account getStorageLocAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getStorageLocAcct');
        Account storageLocAcct = new Account();
        if(relatedAccountsMap.containsKey(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c)) {
            storageLocAcct = relatedAccountsMap.get(this.orderDetails.Stock_Holding_Location__r.Stock_Holding_Location__c);
        }
        return storageLocAcct;
    }

    /**
    * @Author       Accenture
    * @Name         getShipToAcct
    * @Date         02/08/2017
    * @Description  This method is used get ShipTo details from the Order
    * @Param :
    * @return        Account
    */
     public Account getShipToAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getShipToAcct');
        Account shipToAcct = new Account();
        if(this.orderDetails.Ep_ShipTo__c == null && relatedAccountsMap.containsKey(this.orderDetails.EP_Pickup_Location__c)){
            shipToAcct = relatedAccountsMap.get(this.orderDetails.EP_ShipTo__c);
        } else {
            if(relatedAccountsMap.containsKey(this.orderDetails.EP_ShipTo__c)) {
            shipToAcct = relatedAccountsMap.get(this.orderDetails.EP_ShipTo__c);
            }
        }
        return shipToAcct;
    }
    
    /**
    * @Author       Accenture
    * @Name         getTransporterAcct
    * @Date         02/08/2017
    * @Description  This method is used to get Transporter details for Order
    * @Param :
    * @return        Account
    */
    public Account getTransporterAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getTransporterAcct');
        Account transporterAcct = new Account();
        if(relatedAccountsMap.containsKey(this.orderDetails.EP_Transporter__c)) {
            transporterAcct = relatedAccountsMap.get(this.orderDetails.EP_Transporter__c);
        }
        return transporterAcct;
    }
     
   /**
    * @Author       Accenture
    * @Name         getOrderLineItems
    * @Date         02/08/2017
    * @Description  This method is used to get Order Items details for Order
    * @Param :
    * @return        list<OrderItem>
    */
    public List<csord__Order_Line_Item__c> getOrderLineItems() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getOrderLineItems');
        this.orderLineItems = new List<csord__Order_Line_Item__c>();
        EP_OrderItemMapper ordItemMapper = new EP_OrderItemMapper();
         //Repeat this for every Child node
        List<Attachment> lineItems = [select Id, Body from Attachment where ParentId = :this.ord.csord__Identification__c];

        System.debug('getOrderLineItems lineItems = ' + lineItems);
        EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(lineItems.get(0).Body.toString() , EP_PricingResponseStub.class);

        for(csord__Order_Line_Item__c orderLineitem : ordItemMapper.getCSOrderById(new set<Id>{this.ord.Id})) {
            for (EP_PricingResponseStub.LineItem li : stub.Payload.any0.pricingResponse.priceDetails.priceLineItems.lineItem) {
                List<String> quantityList = li.lineItemInfo.quantity.split(',');
                    String receivedQuantity = '';
                    for (Integer i = 0; i < quantityList.size(); i++) {
                        receivedQuantity = receivedQuantity + quantityList[i];
                    }
                    System.debug('getOrderLineItems receivedQuantity = ' + receivedQuantity);
                    System.debug('getOrderLineItems orderLineitem.Quantity__c = ' + orderLineitem.Quantity__c);
                    System.debug('getOrderLineItems orderLineitem.EP_Product_Code__c = ' + orderLineitem.EP_Product_Code__c);
                    System.debug('getOrderLineItems li.lineItemInfo.itemId = ' + li.lineItemInfo.itemId);
                    
                if (li.lineItemInfo.itemId.equals(String.valueOf(orderLineitem.EP_Product_Code__c)) && (orderLineitem.Quantity__c == Decimal.valueOf(receivedQuantity))) {
                    //receivedQuantity.equals(String.valueOf(orderLineitem.Quantity__c))
                    System.debug('getOrderLineItems entered IF statement');
                    System.debug('getOrderLineItems Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxPercentage) = ' + Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxPercentage));
                    System.debug('getOrderLineItems li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount = ' + li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount);
                        orderLineitem.EP_Tax_Percentage__c = Decimal.valueOf(li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxPercentage);
                        orderLineitem.csord__Line_Description__c = li.lineItemInfo.invoiceDetails.invoiceComponent[0].amount;
                        orderLineitem.UnitPrice__c = orderLineItem.ListPrice__c + orderLineItem.EP_VAT__c;
                    }
                System.debug('getOrderLineItems orderLineitem.EP_Tax_Percentage__c = ' + orderLineitem.EP_Tax_Percentage__c);
                System.debug('getOrderLineItems orderLineitem.csord__Line_Description__c = ' + orderLineitem.csord__Line_Description__c);
                System.debug('getOrderLineItems orderLineitem.UnitPrice__c = ' + orderLineitem.UnitPrice__c);
            //li.lineItemInfo.invoiceDetails.invoiceComponent[0].taxAmount;
            }
            orderLineItems.add(orderLineitem);
        }

        return orderLineItems;
    }

    /**
    * @Author       Accenture
    * @Name         getSellToAcct
    * @Date         02/08/2017
    * @Description  This method is used to get SellTo details for Order
    * @Param :
    * @return        Account
    */
    public Account getSellToAcct() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','getSellToAcct');
        Account sellToAcct = new Account();
        system.debug('pep sell to' + sellToAcct);
        if(relatedAccountsMap.containsKey(this.orderDetails.csord__Account__c)) {
            sellToAcct = relatedAccountsMap.get(this.orderDetails.csord__Account__c);
            system.debug('pep ' + sellToAcct);
        }
        return sellToAcct;
    }
    
   /**
    * @Author       Accenture
    * @Name         setDocTitle
    * @Date         02/08/2017
    * @Description  This method is used to set Doc Title for ORDER_CONFIRMATION on VF page
    * @Param        
    * @return       NA
    */   
    public void setDocTitle() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','setDocTitle');
        this.docTitle = ord.EP_Document_Title__c;
        if(this.orderDetails.csord__Status2__c != null && this.orderDetails.csord__Status2__c.equals(EP_Common_Constant.ORDER_DRAFT_STATUS) ){
            this.docTitle = EP_OrderConfirmationConstant.ORDER_QUOTE_TITLE;
        }
    }
    
   /**
    * @Author       Accenture
    * @Name         checkPageAccess
    * @Date         02/08/2017
    * @Description  This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
    * @Param        
    * @return       PageReference
    */
    public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_OrderConfirmationXMLCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}