/*
   @Author          Accenture
   @Name            EP_ScheduleAutomaticRetry - #59186
   @CreateDate      02/07/2017
   @Description     This scheduled class is used to retry the outbound call which failed due to some technical reasons
   @Version         1.0
*/
global class EP_ScheduleAutomaticRetry implements Schedulable  {
	@TestVisible
    private static final integer RECORDS_TOBE_PROCESSED = 1;
	
	/**
    * @Author       Accenture
    * @Name         execute
    * @Date         02/07/2017
    * @Description  This methods gets all the records which needs to be processed
    * @Param        SchedulableContext
    * @return       NA
    */
    global void execute(SchedulableContext sc) {
        EP_AutomaticTechnicalRetryBatch  atr = new EP_AutomaticTechnicalRetryBatch();
        //Processing one record at a time because, if we have multiple records processing, then 
        //we cannot make DML as it won't allow to make further callouts after DML.
        Database.executeBatch(atr,RECORDS_TOBE_PROCESSED);
    }
}