/* 
@Author      Accenture
@name        EP_ASTSellToActiveToDeactivate
@CreateDate  11/29/2017
@Description Account(SellTo) State Transitions class for Active to Deactivate State - L4 #45361
@NovaSuite Fix -- comments added
@Version     1.0
*/
public with sharing class EP_ASTSellToActiveToDeactivate  extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTSellToActiveToDeactivate
    */
    public EP_ASTSellToActiveToDeactivate() {
        finalState = EP_AccountConstant.DEACTIVATE;
    }
/* 
@Author      Accenture
@name        isGuardCondition
*/    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToDeactivate','isGuardCondition');
        //No guard conditions for Active to Deactivate
        return true;
    }
}