/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToBasicDataToAccSetup>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 02-Basic Data Setup to 04-AccountSet-up>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToBasicDataToAccSetup extends EP_AccountStateTransition {

    public EP_ASTStorageShipToBasicDataToAccSetup () {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToAccSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToAccSetup', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToAccSetup',' isGuardCondition');
        EP_AccountService service =  new EP_AccountService(this.account);  
		if(!service.isPrimarySupplyOptionAttached()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(System.label.EP_One_Primary_Location_Reqd);
            return false;
        }      
        return true;
    }
}