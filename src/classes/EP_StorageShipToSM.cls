/* 
   @Author      Accenture
   @name        EP_StorageShipToSM
   @CreateDate  02/28/2017
   @Description Class for storage shipTo stateMachine
   @Version     1.1
*/
 public class EP_StorageShipToSM extends EP_AccountStateMachine{
 	public override EP_AccountState getAccountState(){
 		EP_GeneralUtility.Log('Public','EP_StorageShipToSM','getAccountState');
 		return super.getAccountState();
 	}
 }