public class ProductConfigurationCloner {

    private static final Integer RECURRENCE_MONTHLY = 12;
    private static final Integer RECURRENCE_QUATERLY = 4;
    private static final Integer RECURRENCE_YEARLY = 1;

    private static final String ATTR_TYPE_RELATED_PRODUCT = 'Related Product';

    private static final Set<String> FIELDS_TO_MERGE_ON_ATTR = new Set<String> {
        'cscfga__cascade_value__c',
        'cscfga__hidden__c',
        'cscfga__is_active__c',
        'cscfga__is_line_Item__c',
        'cscfga__is_rate_line_item__c',
        'cscfga__is_read_only__c',
        'cscfga__is_Required__c',
        'cscfga__line_item_description__c',
        'cscfga__line_item_sequence__c',
        'cscfga__list_price__c',
        'cscfga__price__c',
        'cscfga__recurring__c',
        'cscfga__value__c'
    };

    private static final List<Schema.DescribeFieldResult> ATTRIBUTE_FIELDS = new List<Schema.DescribeFieldResult>();

    static {
        for (Schema.SObjectField sObjF : Schema.SObjectType.cscfga__Attribute__c.fields.getMap().values()) {
            ATTRIBUTE_FIELDS.add(sObjF.getDescribe());
        }
    }

  /**  public static Id mergeOfferToBasket(Id offerId, Id basketId) {
        List<Configuration> offerRootConfigurations = buildContainerConfigurationTrees(offerId);
        if (offerRootConfigurations.isEmpty()) {
            return null;
        }

        Set<Id> rootProductDefinitionIds = new Set<Id>();
        for (Configuration cfg : offerRootConfigurations) {
            rootProductDefinitionIds.add(cfg.definitionId);
        }

        List<Configuration> storedConfigurations = mergeConfigurationTrees(
            offerRootConfigurations,
            buildContainerConfigurationTrees(basketId, rootProductDefinitionIds),
            new MergeInformationProvider(offerId, basketId)
        );

        cscfga.ProductConfigurationBulkActions.calculateTotals(new Set<Id>{ basketId });

        List<Id> storedConfigurationIds = new List<Id>();
        for (Configuration cfg : storedConfigurations) {
            storedConfigurationIds.add(cfg.sObj.id);
        }

        createAndStorePCRs(storedConfigurationIds);

        if (storedConfigurationIds.isEmpty()) {
            return null;
        } else {
            Id retVal = storedConfigurationIds.remove(0);
            // we dont want to revalidate first configuration. It is expected that whoever is using this to revalidate it
            if (!storedConfigurationIds.isEmpty()) {
                cscfga.ProductConfigurationBulkActions.revalidateConfigurationsAsync(new Set<Id>(storedConfigurationIds));
            }
            return retVal;
        }
    }
*/
    public static Set<Id> cloneOfferConfigsToBasket(Id offerId, Id basketId) {
        return cloneConfigurations(offerId, basketId);
    }

    public static Set<Id> cloneConfigurations(Id fromContainerId, Id toContainerId) {
        List<Configuration> storedConfigurations = mergeConfigurationTrees(
            buildContainerConfigurationTrees(fromContainerId),
            new List<Configuration>(),
            new MergeInformationProvider(fromContainerId, toContainerId)
        );

        if (toContainerId.getSObjectType() == cscfga__Product_Basket__c.sObjectType) {
            //cscfga.ProductConfigurationBulkActions.calculateTotals(new Set<Id>{ toContainerId });
        }

        Set<Id> storedConfigurationIds = new Set<Id>();
        for (Configuration cfg : storedConfigurations) {
            storedConfigurationIds.add(cfg.sObj.id);
        }
        //createAndStorePCRs(storedConfigurationIds);

        return storedConfigurationIds;
    }

    public static Id createBasketAndCloneOfferToBasket(Id accountId, Id offerId) {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
            name = 'Basket from offer: ' + offerId,
            csbb__account__c = accountId
        );
        insert basket;

        cloneOfferConfigsToBasket(offerId, basket.id);

        return basket.id;
    }

    private static Set<Id> getRootConfigurationIds(Id linkedObjectId) {
        if (linkedObjectId == null) {
            return new Set<Id>();
        }

        return new Map<Id, SObject>([
            select
                id
            from cscfga__Product_Configuration__C
            where (cscfga__product_basket__c = :linkedObjectId or cscfga__configuration_offer__c = :linkedObjectId)
        ]).keySet();
    }

 /**   public static void createAndStorePCRs(List<Id> configurationIds) {
        List<cscfga__Product_Configuration__c> rootConfigurations = getConfigurationsForPCRs(configurationIds);
        Map<Id, csbb__Product_Configuration_Request__c> requestsToUpsert = new Map<Id, csbb__Product_Configuration_Request__c>();

        Map<Id, Id> existingPCRsByConfigId = new Map<Id, Id>();
        {
            List<csbb__Product_Configuration_Request__c> existingPCRs = getExistingPCRs(configurationIds);
            for (csbb__Product_Configuration_Request__c pcr : existingPCRs) {
                existingPCRsByConfigId.put(pcr.csbb__product_configuration__c, pcr.id);
            }
        }

        for (cscfga__Product_Configuration__c rootConfig : rootConfigurations) {
            Decimal recurringToBillingFactor = 0;
            if (rootConfig.cscfga__billing_frequency__c != null && rootConfig.cscfga__billing_frequency__c != 0) {
                recurringToBillingFactor = (rootConfig.cscfga__recurrence_frequency__c / rootConfig.cscfga__billing_frequency__c).setScale(2);
            }

            Decimal recurringToMonthlyFactor = 0;
            if (rootConfig.cscfga__recurrence_frequency__c != null) {
                recurringToMonthlyFactor = rootConfig.cscfga__recurrence_frequency__c / RECURRENCE_MONTHLY;
            }

            Decimal billingCharge = (recurringToBillingFactor * rootConfig.cscfga__quantity__c * rootConfig.cscfga__total_recurring_charge__c).setScale(2);

            Decimal totalSOV = rootConfig.cscfga__quantity__c * (rootConfig.cscfga__total_one_off_charge__c + (rootConfig.cscfga__total_recurring_charge__c * recurringToMonthlyFactor).setScale(2));

            requestsToUpsert.put(
                rootConfig.id,
                new csbb__Product_Configuration_Request__c(
                    id = existingPCRsByConfigId.get(rootConfig.id),
                    csbb__product_basket__c = rootConfig.cscfga__product_basket__c,
                    csbb__product_category__c = rootConfig.cscfga__product_definition__r.cscfga__product_category__c,
                    csbb__product_configuration__c = rootConfig.id,
                    csbb__total_oc__c = rootConfig.cscfga__quantity__c * rootConfig.cscfga__total_one_off_charge__c,
                    csbb__total_mrc__c = rootConfig.cscfga__billing_frequency__c == RECURRENCE_MONTHLY ? billingCharge : null,
                    csbb__total_qrc__c = rootConfig.cscfga__billing_frequency__c == RECURRENCE_QUATERLY ? billingCharge : null,
                    csbb__total_yrc__c = rootConfig.cscfga__billing_frequency__c == RECURRENCE_YEARLY ? billingCharge : null,
                    csbb__total_sov__c = totalSOV
                )
            );
        }

        if (!requestsToUpsert.isEmpty()) {
            upsert requestsToUpsert.values();
        }
    }

    private static List<cscfga__Product_Configuration__c> getConfigurationsForPCRs(List<Id> configurationIds) {
        return [
            select
                id,
                cscfga__billing_frequency__c,
                cscfga__recurrence_frequency__c,
                cscfga__product_basket__c,
                cscfga__quantity__c,
                cscfga__total_contract_value__c,
                cscfga__total_one_off_charge__c,
                cscfga__total_recurring_charge__c,
                cscfga__product_definition__r.cscfga__product_category__c
            from cscfga__Product_Configuration__c
            where id in :configurationIds and cscfga__parent_configuration__c = null
        ];
    }

    private static List<csbb__Product_Configuration_Request__c> getExistingPCRs(List<Id> configurationIds) {
        return [
            select
                id,
                csbb__product_basket__c,
                csbb__product_category__c,
                csbb__product_configuration__c,
                csbb__total_oc__c,
                csbb__total_mrc__c,
                csbb__total_qrc__c,
                csbb__total_yrc__c,
                csbb__total_sov__c
            from csbb__Product_Configuration_Request__c
            where csbb__product_configuration__c in :configurationIds
        ];
    }
*/
    private static List<Configuration> mergeConfigurationTrees(List<Configuration> originalRootConfigurations, List<Configuration> targetRootConfigurations, MergeInformationProvider mergeInfoProvider) {
        List<Configuration> configsToBeStored = new List<Configuration>();
        for (Configuration rootConfig : originalRootConfigurations) {
            Configuration configToMergeInto = findMergableConfiguration(rootConfig, targetRootConfigurations);
            if (configToMergeInto != null) {
                configsToBeStored.add(configToMergeInto.mergeConfiguration(mergeInfoProvider, rootConfig));
            } else {
                configsToBeStored.add(rootConfig.clone(mergeInfoProvider, null));
            }
        }

        storeConfigurations(configsToBeStored);

        return configsToBeStored;
    }

    private static void storeConfigurations(List<Configuration> rootConfigs) {
        List<cscfga__Product_Configuration__c> toUpsertConfigs = new List<cscfga__Product_Configuration__c>();
        for (Configuration cfg : rootConfigs) {
            toUpsertConfigs.addAll(cfg.getAllConfigurationsToUpsert());
        }

        debug('Number of configs to upsert: ' + toUpsertConfigs.size());

        if (!toUpsertConfigs.isEmpty()) {
            upsert toUpsertConfigs;

            for (Configuration cfg : rootConfigs) {
                cfg.buildChildConfigurationReferences();
            }

            toUpsertConfigs = new List<cscfga__Product_Configuration__c>();
            for (Configuration cfg : rootConfigs) {
                toUpsertConfigs.addAll(cfg.getAllConfigurationsToUpsert());
            }
            upsert toUpsertConfigs;
        }

        // attributes
        {
            List<cscfga__Attribute__c> attrsToUpsert = new List<cscfga__Attribute__c>();
            for (Configuration cfg : rootConfigs) {
                attrsToUpsert.addAll(cfg.getAllAttributesToUpsert());
            }

            if (!attrsToUpsert.isEmpty()) {
                upsert attrsToUpsert;
            }
        }

        // attribute fields
        {
            List<cscfga__Attribute_Field__c> attrFieldsToUpsert = new List<cscfga__Attribute_Field__c>();
            for (Configuration cfg : rootConfigs) {
                attrFieldsToUpsert.addAll(cfg.getAllAttributeFieldsToUpsert());
            }

            if (!attrFieldsToUpsert.isEmpty()) {
                upsert attrFieldsToUpsert;
            }
        }
    }

    private static Configuration findMergableConfiguration(Configuration forConfig, List<Configuration> inConifigrations) {
        for (Configuration cfg : inConifigrations) {
            if (!cfg.isMerged && forConfig.definitionId == cfg.definitionId) {
                return cfg;
            }
        }
        return null;
    }

    private static List<Configuration> buildContainerConfigurationTrees(Id containerId) {
        return buildContainerConfigurationTrees(containerId, null);
    }

    private static List<Configuration> buildContainerConfigurationTrees(Id containerId, Set<Id> rootProductDefinitionIds) {
        List<cscfga__Product_Configuration__c> configurations = readContainerConfigurations(containerId, rootProductDefinitionIds);
        if (configurations == null || configurations.isEmpty()) {
            return new List<Configuration>();
        }

        debug('Number of configurations: ' + configurations.size());

        Set<Id> configIds = new Map<Id, SObject>(configurations).keySet();

        return buildConfigurationTrees(
            configurations,
            readConfigAttributes(configIds),
            readAttributeFields(configIds)
        );
    }

    private static List<Configuration> buildConfigurationTrees(List<cscfga__Product_Configuration__c> productConfigurations, List<cscfga__Attribute__c> attributes, List<cscfga__Attribute_Field__c> attributeFields) {
        Map<Id, Configuration> configsById = new Map<Id, Configuration>();
        Set<Id> rootConfigurationIds = new Set<Id>();

        for (cscfga__Product_Configuration__c cfg : productConfigurations) {
            configsById.put(cfg.id, new Configuration(cfg));
            if (cfg.cscfga__parent_configuration__c == null) {
                rootConfigurationIds.add(cfg.id);
            }
        }

        Map<Id, Attribute> attributesById = new Map<Id, Attribute>();
        for (cscfga__Attribute__c attrSObj : attributes) {
            Configuration attrConfig = configsById.get(attrSObj.cscfga__product_configuration__c);
            if (attrConfig == null) {
                continue;
            }
            Attribute attr = attrConfig.addAttribute(attrSObj);
            attributesById.put(attr.attributeId, attr);

            // related product attributes
            if (attr.type == ATTR_TYPE_RELATED_PRODUCT) {
                if (!String.isBlank(attr.attrValue)) {
                    String[] childConfigurationIds = attr.attrValue.split(',');
                    for (String childConfigId : childConfigurationIds) {
                        Configuration childConfig = configsById.get(childConfigId);
                        if (childConfig != null) {
                            attr.addChildConfiguration(childConfig);
                        }
                    }
                }
            }
        }

        debug('Number of configs in trees: ' + configsById.size());

        for (Configuration possibleReferenceOnlyChild : configsById.values()) {
            if (possibleReferenceOnlyChild.sObj.cscfga__parent_configuration__c != null && possibleReferenceOnlyChild.parentConfiguration == null) {
                debug('Found config without associated parent: ' + possibleReferenceOnlyChild);
                configsById.get(possibleReferenceOnlyChild.sObj.cscfga__parent_configuration__c).addReferenceOnlyChildConfiguration(possibleReferenceOnlyChild);
            }
        }

        for (cscfga__Attribute_Field__c attrFieldSObj : attributeFields) {
            Attribute targetAttribute = attributesById.get(attrFieldSObj.cscfga__attribute__c);
            if (targetAttribute != null) {
                targetAttribute.addAttributeField(attrFieldSObj);
            }
        }

        List<Configuration> retVal = new List<Configuration>();
        for (Id rootConfigId : rootConfigurationIds) {
            retVal.add(configsById.get(rootConfigId));
        }

        return retVal;
    }

    private static List<cscfga__Product_Configuration__c> readContainerConfigurations(Id containerId, Set<Id> rootProductDefinitionIds) {
        if (containerId == null) {
            return null;
        }
        SOQLBuilder bldr = new SOQLBuilder(cscfga__Product_Configuration__c.sObjectType);

        if (rootProductDefinitionIds != null) {
            // currently 3 level is supported only
            bldr.addWhereClauseExpression('cscfga__product_definition__c in :rootProductDefinitionIds or cscfga__parent_configuration__r.cscfga__product_definition__c in :rootProductDefinitionIds or cscfga__parent_configuration__r.cscfga__parent_configuration__r.cscfga__product_definition__c in :rootProductDefinitionIds');
        }

        if (containerId.getSObjectType() == cscfga__Product_Basket__c.sObjectType) {
            bldr.addWhereClauseExpression('cscfga__product_basket__c = :containerId');
        } else if (containerId.getSObjectType() == cscfga__Configuration_Offer__c.sObjectType) {
            bldr.addWhereClauseExpression('cscfga__configuration_offer__c = :containerId');
        } else {
            // TODO - throw exception
        }

        bldr.addSortExpression('cscfga__parent_configuration__c nulls first, createdDate');

        return (List<cscfga__Product_Configuration__c>) Database.query(bldr.getSOQL());
    }

    private static List<cscfga__Attribute__c> readConfigAttributes(Set<Id> configIds) {
        SOQLBuilder bldr = new SOQLBuilder(cscfga__Attribute__c.sObjectType);

        bldr.addWhereClauseExpression('cscfga__product_configuration__c in :configIds');
        bldr.addSortExpression('cscfga__product_configuration__c');

        return (List<cscfga__Attribute__c>) Database.query(bldr.getSOQL());
    }

    private static List<cscfga__Attribute_Field__c> readAttributeFields(Set<Id> configIds) {
        SOQLBuilder bldr = new SOQLBuilder(cscfga__Attribute_Field__c.sObjectType);

        bldr.addWhereClauseExpression('cscfga__attribute__r.cscfga__product_configuration__c in :configIds');
        bldr.addSortExpression('cscfga__attribute__c');

        return (List<cscfga__Attribute_Field__c>) Database.query(bldr.getSOQL());
    }

    private class Configuration {
        public Id offerId;
        public Id basketId;
        public cscfga__Product_Configuration__c sObj;
        public Attribute parentAttribute;
        public Configuration parentConfiguration;
        public Boolean isMerged;
        public Boolean modified;
        public Boolean hasEmptyRoot;

        public Id definitionId {
            get {
                return this.sObj.cscfga__product_definition__c;
            }
        }

        public Id configId {
            get {
                return this.sObj.id;
            }
        }

        public String parentAttributeName {
            get {
                return this.sObj.cscfga__attribute_name__c;
            }
            set {
                this.sObj.cscfga__attribute_name__c = value;
            }
        }

        public Decimal configurationIndex {
            get {
                return this.sObj.cscfga__index__c;
            }
            set {
                this.sObj.cscfga__index__c = value;
            }
        }

        public List<Attribute> attributes;
        public Map<Id, Attribute> attributesByDefinitionId;
        public Map<Id, Attribute> attributesById;
        public List<Configuration> referenceOnlyConfigurationChildren;

        public Configuration(cscfga__Product_Configuration__c sObj) {
            this.sObj = sObj;
            this.basketId = sObj.cscfga__product_basket__c;
            this.offerId = sObj.cscfga__configuration_offer__c;
            this.referenceOnlyConfigurationChildren = new List<Configuration>();

            this.attributes = new List<Attribute>();
            this.attributesByDefinitionId = new Map<Id, Attribute>();
            this.isMerged = false;
        }

        public Attribute addAttribute(cscfga__Attribute__c attrSObj) {
            return this.addAttribute(new Attribute(attrSObj));
        }

        public Attribute addAttribute(Attribute attr) {
            attr.config = this;
            this.attributes.add(attr);
            this.attributesByDefinitionId.put(attr.definitionId, attr);

            return attr;
        }

        /**
         * Normal cfg should have everything via related attribute,
         * but to support some hacks created for trafigura we allow direct child reference which is not cfg child by definition
         */
        public void addReferenceOnlyChildConfiguration(Configuration cfg) {
            this.referenceOnlyConfigurationChildren.add(cfg);
            cfg.parentConfiguration = this;
        }

        public Configuration clone(MergeInformationProvider mergeInfoProvider, Attribute parentAttribute) {
            Configuration clonedCfg = new Configuration(this.sObj.clone(false, false, false, false));
            clonedCfg.sObj.cscfga__configuration_offer__c = null;
            clonedCfg.sObj.cscfga__product_basket__c = null;
            clonedCfg.hasEmptyRoot = clonedCfg.sObj.cscfga__root_configuration__c == null;
            clonedCfg.sObj.cscfga__root_configuration__c = null;
            clonedCfg.sObj.cscfga__parent_configuration__c = null;

            // currently we only support basket to basket or offer to basket clone/merge
            if (mergeInfoProvider.fromContainerId.getSObjectType() == cscfga__Configuration_Offer__c.sObjectType) {
                clonedCfg.sObj.cscfga__originating_offer__c = clonedCfg.sObj.cscfga__configuration_offer__c;
            }

            if (mergeInfoProvider.toContainerId.getSObjectType() == cscfga__Configuration_Offer__c.sObjectType) {
                clonedCfg.sObj.cscfga__configuration_offer__c = mergeInfoProvider.toContainerId;
            } else {
                clonedCfg.sObj.cscfga__product_basket__c = mergeInfoProvider.toContainerId;
            }

            clonedCfg.sObj.cscfga__key__c = TextUtils.generateGuid();
            clonedCfg.modified = true;

            for (Attribute attr : this.attributes) {
                attr.clone(mergeInfoProvider, clonedCfg);
            }

            if (parentAttribute != null) {
                parentAttribute.addChildConfiguration(clonedCfg);
            }

            for (Configuration cfg : this.referenceOnlyConfigurationChildren) {
                clonedCfg.addReferenceOnlyChildConfiguration(cfg.clone(mergeInfoProvider, null));
            }

            return clonedCfg;
        }

        public List<cscfga__Product_Configuration__c> getAllConfigurationsToUpsert() {
            List<cscfga__Product_Configuration__c> retVal = new List<cscfga__Product_Configuration__c>();
            if (this.modified == true) {
                retVal.add(this.sObj);
            }

            for (Attribute attr : this.attributes) {
                for (Configuration childCfg : attr.childConfigurations) {
                    retVal.addAll(childCfg.getAllConfigurationsToUpsert());
                }
            }

            for (Configuration cfg: this.referenceOnlyConfigurationChildren) {
                retVal.addAll(cfg.getAllConfigurationsToUpsert());
            }

            return retVal;
        }

        public void buildChildConfigurationReferences() {
            Configuration parentConfig = this.parentConfiguration;
            if (parentConfig != null) {
                this.sObj.cscfga__parent_configuration__c = parentConfig.sObj.id;
            }

            // root configuration finding
            // ignores root configuration reference if it has not been defined
            // this allows some hacks introduced for trafigura
            while (parentConfig != null && !this.hasEmptyRoot) {
                if (parentConfig.parentConfiguration == null) {
                    this.sObj.cscfga__root_configuration__c = parentConfig.sObj.id;
                    break;
                }
                parentConfig = parentConfig.parentConfiguration;
            }

            for (Attribute attr : this.attributes) {
                attr.buildChildConfigurationReferences();
            }

            debug('config references -> reference only: ' + this.referenceOnlyConfigurationChildren.size());

            for (Configuration cfg : this.referenceOnlyConfigurationChildren) {
                cfg.buildChildConfigurationReferences();
            }
        }

        public List<cscfga__Attribute__c> getAllAttributesToUpsert() {
            List<cscfga__Attribute__c> attributesToUpsert = new List<cscfga__Attribute__c>();
            for (Attribute attr : this.attributes) {
                attributesToUpsert.addAll(attr.getAllAttributesToUpsert());
            }
            return attributesToUpsert;
        }

        public List<cscfga__Attribute_Field__c> getAllAttributeFieldsToUpsert() {
            List<cscfga__Attribute_Field__c> attrFieldsToUpsert = new List<cscfga__Attribute_Field__c>();

            for (Attribute attr : this.attributes) {
                attrFieldsToUpsert.addAll(attr.getAllAttributeFieldsToUpsert());
            }

            return attrFieldsToUpsert;
        }

        public Configuration mergeConfiguration(MergeInformationProvider mergeInfoProvider, Configuration configToMerge) {
            // attributes are merged / config data is not touched
            for (Id attrDefId : configToMerge.attributesByDefinitionId.keySet()) {
                Attribute attrToMerge = configToMerge.attributesByDefinitionId.get(attrDefId);
                Attribute attrToMergeInto = this.attributesByDefinitionId.get(attrDefId);
                if (attrToMergeInto == null) {
                    attrToMerge.clone(mergeInfoProvider, this);
                } else if (mergeInfoProvider.shouldOverrideAttribute(attrToMergeInto.sObj)) {
                    attrToMergeInto.mergeAttribute(mergeInfoProvider, attrToMerge);
                }
            }
            configToMerge.isMerged = true;
            this.modified = true;
            this.isMerged = true;

            return this;
        }
    }

    private class Attribute {
        public Id definitionId {
            get {
                return this.sObj.cscfga__attribute_definition__c;
            }
        }

        public Id attributeId {
            get {
                return this.sObj.id;
            }
        }

        public String type {
            get {
                return this.sObj.attribute_type__c;
            }
        }

        public String attrValue {
            get {
                return this.sObj.cscfga__value__c;
            }
            set {
                this.sObj.cscfga__value__c = value;
            }
        }

        public String name {
            get {
                return this.sObj.name;
            }
        }

        public Configuration config;
        public cscfga__Attribute__c sObj;

        public List<AttributeField> attributeFields;
        public Map<String, AttributeField> attrFieldsByName;

        public List<Configuration> childConfigurations;

        public Boolean modified;
        public Boolean isMerged;

        public Attribute(cscfga__Attribute__c sObj) {
            this.sObj = sObj;
            this.attributeFields = new List<AttributeField>();
            this.attrFieldsByName = new Map<String, AttributeField>();
            this.childConfigurations = new List<Configuration>();
            this.modified = false;
            this.isMerged = false;
        }

        public AttributeField addAttributeField(cscfga__Attribute_Field__c attrFieldSObj) {
            return this.addAttributeField(new AttributeField(attrFieldSObj));
        }

        public AttributeField addAttributeField(AttributeField attrF) {
            attrF.attr = this;

            this.attributeFields.add(attrF);
            this.attrFieldsByName.put(attrF.name, attrF);

            return attrF;
        }

        public void addChildConfiguration(Configuration cfg) {
            if (this.type == ATTR_TYPE_RELATED_PRODUCT) {
                this.childConfigurations.add(cfg);
                cfg.parentConfiguration = this.config;
                cfg.parentAttributeName = this.name;
                cfg.configurationIndex = this.childConfigurations.size();
                cfg.parentAttribute = this;
                cfg.modified = true;
                this.modified = true;
            }
        }

        public Attribute clone(MergeInformationProvider mergeInfoProvider, Configuration toParentConfig) {
            Attribute clonedAttribute = new Attribute(this.sObj.clone(false, false, false, false));
            clonedAttribute.sObj.cscfga__key__c =TextUtils.generateGuid();
            clonedAttribute.config = toParentConfig;
            clonedAttribute.modified = true;

            for (Configuration childCfg : this.childConfigurations) {
                childCfg.clone(mergeInfoProvider, clonedAttribute);
            }

            for (AttributeField attrField : this.attributeFields) {
                attrField.clone(mergeInfoProvider, clonedAttribute);
            }

            if (toParentConfig != null) {
                toParentConfig.addAttribute(clonedAttribute);
            }

            return clonedAttribute;
        }

        public void buildChildConfigurationReferences() {
            if (this.sObj.cscfga__product_configuration__c != this.config.sObj.id) {
                this.sObj.cscfga__product_configuration__c = this.config.sObj.id;
                this.modified = true;
            }

            if (this.type == ATTR_TYPE_RELATED_PRODUCT) {
                List<String> childConfigurationIds = new List<String>();
                for (Configuration childConfig : this.childConfigurations) {
                    childConfigurationIds.add(childConfig.sObj.id);
                    childConfig.buildChildConfigurationReferences();
                }
                this.attrValue = String.join(childConfigurationIds, ',');
                this.modified = true;
            }
        }

        public List<cscfga__Attribute__c> getAllAttributesToUpsert() {
            List<cscfga__Attribute__c> attributesToUpsert = new List<cscfga__Attribute__c>();
            if (this.config.sObj.id != this.sObj.cscfga__product_configuration__c) {
                this.sObj.cscfga__product_configuration__c = this.config.sObj.id;
                this.modified = true;
            }

            if (this.modified) {
                attributesToUpsert.add(this.sObj);
            }

            for (Configuration childCfg : this.childConfigurations) {
                attributesToUpsert.addAll(childCfg.getAllAttributesToUpsert());
            }
            return attributesToUpsert;
        }

        public List<cscfga__Attribute_Field__c> getAllAttributeFieldsToUpsert() {
            List<cscfga__Attribute_Field__c> attrFieldsToUpsert = new List<cscfga__Attribute_Field__c>();

            for (AttributeField attrField : this.attributeFields) {
                if (attrField.sObj.cscfga__attribute__c != this.sObj.id) {
                    attrField.sObj.cscfga__attribute__c = this.sObj.id;
                    attrField.modified = true;
                }

                if (attrField.modified) {
                    attrFieldsToUpsert.add(attrField.sObj);
                }
            }

            for (Configuration childCfg : this.childConfigurations) {
                attrFieldsToUpsert.addAll(childCfg.getAllAttributeFieldsToUpsert());
            }

            return attrFieldsToUpsert;
        }

        public void mergeAttribute(MergeInformationProvider mergeInfoProvider, Attribute attr) {
            for (Schema.DescribeFieldResult sObjFieldDescribe : ATTRIBUTE_FIELDS) {
                if (sObjFieldDescribe.isUpdateable()
                    && !sObjFieldDescribe.isExternalId() // external ids are ignored
                    && !sObjFieldDescribe.isIdLookup() // kookups are ignored
                    && !sObjFieldDescribe.isNameField() // name is ignored
                    && !sObjFieldDescribe.isUnique() // unique fields are ignored
                    && FIELDS_TO_MERGE_ON_ATTR.contains(sObjFieldDescribe.getName().toLowerCase())
                ) {
                    this.sObj.put(sObjFieldDescribe.getName(), attr.sObj.get(sObjFieldDescribe.getName()));
                }
            }

            this.modified = true;
            attr.isMerged = true;
            this.isMerged = true;

            for (Configuration toMergeConfig : attr.childConfigurations) {
                Configuration configToMergeInto = findMergableConfiguration(toMergeConfig, this.childConfigurations);
                if (configToMergeInto != null) {
                    configToMergeInto.mergeConfiguration(mergeInfoProvider, toMergeConfig);
                } else {
                    toMergeConfig.clone(mergeInfoProvider, this);
                }
            }

            for (AttributeField attrField : attr.attributeFields) {
                if (this.attrFieldsByName.containsKey(attrField.name)) {
                    AttributeField attrFieldToOverride = this.attrFieldsByName.get(attrField.name);
                    attrFieldToOverride.attrFValue = attrField.attrFValue;
                    attrFieldToOverride.modified = true;
                } else {
                    attrField.clone(mergeInfoProvider, this);
                }
            }
        }
    }

    private class AttributeField {
        public Attribute attr;

        public String name {
            get {
                return this.sObj.name;
            }
        }

        public String attrFValue {
            get {
                return this.sObj.cscfga__value__c;
            }
            set {
                this.sObj.cscfga__value__c = value;
            }
        }

        public cscfga__Attribute_Field__c sObj;

        public Boolean modified;

        public AttributeField(cscfga__Attribute_Field__c sObj) {
            this.sObj = sObj;
            this.modified = false;
        }

        public AttributeField clone(MergeInformationProvider mergeInfoProvider, Attribute toAttribute) {
            AttributeField attrField = new AttributeField(this.sObj.clone(false, false, false, false));
            attrField.modified = true;
            if (toAttribute != null) {
                toAttribute.addAttributeField(attrField);
            }
            return attrField;
        }
    }

    private class MergeInformationProvider {
        public Id fromContainerId { get; private set; }
        public Id toContainerId { get; private set; }

        private Map<Id, Boolean> shouldDisableOverrideAttributeByDefId {
            get {
                if (this.shouldDisableOverrideAttributeByDefId == null) {
                    this.shouldDisableOverrideAttributeByDefId = new Map<Id, Boolean>(); 
                }
                return this.shouldDisableOverrideAttributeByDefId;
            }
            set;
        }

        public MergeInformationProvider(Id fromContainerId, Id toContainerId) {
            this.fromContainerId = fromContainerId;
            this.toContainerId = toContainerId;
        }

        public Boolean shouldOverrideAttribute(cscfga__Attribute__c attribute) {
            return true;
        }
    }

    private static void debug(String msg) {
        System.debug(LoggingLevel.Error, msg);
    }
}