@isTest
public class EP_NAVOrderUpdateHelper_UT
{
    //For state machine to give proper status
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    } 
    static testMethod void createDataMaps_test() {
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper();
        List<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = EP_TestDataUtility.getOrderWrapper();
        
        Test.startTest();
        	localObj.createDataMaps(orderWrapperList);
        Test.stopTest();
        
        System.AssertEquals(true, localObj.orderStatusMap  != null); 
    }
    static testMethod void createDataSets_test() {
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper();
        LisT<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = EP_TestDataUtility.getOrderWrapper();      
        
        Test.startTest();
        	localObj.createDataSets(orderWrapperList);
        Test.stopTest();
        
        System.AssertEquals(true,localObj.orderNumberSet != NULL);
    }
    static testMethod void setOrderObject_test() {
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper();   
        List<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = EP_TestDataUtility.getOrderWrapper();
        orderWrapperList[0].identifier.orderId = orderWrapperList[0].sforder.OrderNumber__c;
        localObj.orderNumberOrderMap.put(orderWrapperList[0].sforder.OrderNumber__c,orderWrapperList[0].sforder);     
        orderWrapperList[0].sfOrder.EP_SeqId__c = null;
        Test.startTest();
        	localObj.setOrderObject(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertEquals(orderWrapperList[0].seqId,orderWrapperList[0].sfOrder.EP_SeqId__c);     
    }
    static testMethod void setOrderAttributes_test() {
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper();   
        List<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = EP_TestDataUtility.getOrderWrapper();              
        orderWrapperList[0].identifier.orderId = orderWrapperList[0].sforder.OrderNumber__c;
        localObj.orderNumberOrderMap.put(orderWrapperList[0].sforder.OrderNumber__c,orderWrapperList[0].sforder);            
        
        Test.startTest();
        	localObj.setOrderAttributes(orderWrapperList);
        Test.stopTest();    
        
        System.AssertEquals(true, localObj.orderStatusMap  != null);
    }
    static testMethod void isOrderNumberValid_PositiveScenariotest() {
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper();   
        List<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = EP_TestDataUtility.getOrderWrapper();  
        orderWrapperList[0].identifier.orderId = orderWrapperList[0].sforder.OrderNumber__c;
        localObj.orderNumberOrderMap.put(orderWrapperList[0].sforder.OrderNumber__c,orderWrapperList[0].sforder);            
        
        Test.startTest();
        	Boolean result = localObj.isOrderNumberValid(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }
    static testMethod void isOrderNumberValid_NegativeScenariotest() {
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper();            
        List<EP_NAVOrderUpdateStub.orderWrapper> orderWrapperList = EP_TestDataUtility.getOrderWrapperForNegativeScenario();              
        
        Test.startTest();
        	Boolean result = localObj.isOrderNumberValid(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }
    static testMethod void setOrderStatus_test() { 
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper(); 
        csord__Order__c sfOrder = EP_TestDataUtility.getcurrentvmiorder();
        sfOrder.Status__c = EP_Common_Constant.delivered;
        update sfOrder;
        String newStatus = EP_Common_Constant.INVOICED_STATUS;    
        
        Test.startTest();
        	localObj.setOrderStatus(sfOrder,newStatus); 
        Test.stopTest();
        
        System.AssertEquals(newStatus, sfOrder.Status__c);
        
    }
    static testMethod void doPostActions_test() {
        EP_NAVOrderUpdateHelper localObj = new EP_NAVOrderUpdateHelper(); 
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        csord__Order__c sfOrder = EP_TestDataUtility.getcurrentvmiorder();
        sfOrder.Status__c = EP_Common_Constant.delivered;
        update sfOrder;
        String newStatus = EP_Common_Constant.INVOICED_STATUS;
        Set<id> orderIdSet = new Set<id>{sfOrder.id };
        Test.startTest();
        	localObj.setOrderStatus(sfOrder,newStatus); 
        	localObj.doPostActions(orderIdSet);
        Test.stopTest();
        //No Need to Put Assertion Here since this method is calling other Method.
        system.assert(true);
    }
}