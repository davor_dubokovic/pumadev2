/**
 * @author <Sandeep Kumar>
 * @name <EP_OrderPricingRequestTest>
 * @createDate <17/12/2015>
 * @description <Test class for EP_OrderPricingRequest class> 
 * @version <1.0>
 */
@isTest
public class EP_OrderPricingRequestTest {
    private static final string SERVICE_SLASH_APEXREST  = '/services/apexrest/v1/PricingResponse/';
    
    /*
     *  intialise data
     */ 
    @testSetup static  void init() {
        EP_TestDummyData.intializePricingHandlerData();
    }

    /*
     *   Test method for generate pricing request
     */
    static testMethod void unitTestCase1 (){
        EP_PROCESS_NAME_CS__c processNames = new EP_PROCESS_NAME_CS__c();
        processNames.Name = 'EP_PRICING_ENGINE_REQUEST';
        processNames.EP_Process_Name__c = 'PER';
        insert processNames;

        Test.loadData(EP_Pricing_Engine_CS__c.SobjectType,'EP_Pricing_Engine_CS');
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.startTest();
        Map<Id, Order> orderMap = new Map<Id, Order>([Select Id from Order]); 
        List<OrderItem> orderItemList = [Select EP_Is_Freight_Price__c, EP_Is_Taxes__c, EP_Is_Standard__c from orderItem where orderId in: orderMap.keySet()];
        for(OrderItem o : orderItemList) {
            o.EP_Is_Freight_Price__c = false;
            o.EP_Is_Taxes__c = false;
            o.EP_Is_Standard__c = true;
        }
        update orderItemList;
        EP_OrderPricingRequest.price_req_generator(orderMap.keySet(), '1');
        List<Id> orderIds = new List<Id>();
        orderIds.addAll(orderMap.keySet());
        EP_OrderPricingRequest.price_req_generator(orderIds[0]);  
        
        
        String jsonBody ='{  "name": "1600 Amphitheatre Parkway, Mountain View, CA",  "Status": {    "code": 200,    "request": "geocode"  },  "Placemark": [ {    "id": "p1",    "address": "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",    "AddressDetails": {   "Accuracy" : 8,   "Country" : {      "AdministrativeArea" : {         "AdministrativeAreaName" : "CA",         "SubAdministrativeArea" : {            "Locality" : {               "LocalityName" : "Mountain View",               "PostalCode" : {                  "PostalCodeNumber" : "94043"               },               "Thoroughfare" : {                  "ThoroughfareName" : "1600 Amphitheatre Pkwy"               }            },         '+
            '   "SubAdministrativeAreaName" : "Santa Clara"         }      },      "CountryName" : "USA",      "CountryNameCode" : "US"   }},    "ExtendedData": {      "LatLonBox": {        "north": 37.4251466,        "south": 37.4188514,        "east": -122.0811574,        "west": -122.0874526      }    },    "Point": {      "coordinates": [ -122.0843700, 37.4217590, 0 ]    }  } ]}   ';

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse(); 
        req.requestURI = SERVICE_SLASH_APEXREST;  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type
         
        req.requestBody = Blob.valueof(jsonBody);
        RestContext.request = req;
        RestContext.response= res; 
        EP_OrderPricingRequest.PRICINGSync();
        
        //You will get exception if pass orderIds as null
        EP_OrderPricingRequest.price_req_generator(null, ''); 
        Test.stopTest();
    }
}