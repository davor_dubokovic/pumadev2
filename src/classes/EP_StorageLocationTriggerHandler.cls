/**
   * @Author <Ashok Arora>
   * @name <EP_StorageLocationHandler>
   * @CreateDate <03/11/2015>
   * @Description <This class handles requests from Stock Holding Location Trigger>  
   * @Version <1.0>
   */
   public with sharing class EP_StorageLocationTriggerHandler {

    public static Boolean isExecuteBeforeUpdate = false;
    
    private static final String DO_BEFORE_INSERT = 'doBeforeInsert';
    private static final String DO_BEFORE_UPDATE = 'doBeforeUpdate';
    private static final String DO_BEFORE_DELETE = 'doBeforeDelete';
    private static final String DO_BEFORE_INSERT_UPDATE = 'doBeforeInsertUpdate';
    
    public static boolean executeStorageLocationTrigger = true;
    /**
     * @author <Ashok Arora>
     * @date <03/11/2015>
     * @description <This method performs logic which need to pre execute 
    *               on before insert event of supply option record.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return NONE
     */
     public static void doBeforeInsert(List<EP_Stock_Holding_Location__c>listOfNewStorageLocations){
        EP_GeneralUtility.Log('Public','EP_StorageLocationTriggerHandler','doBeforeInsert');
        if(executeStorageLocationTrigger){
	        try{
		        //Restrict csc user from adding stock holding location once ship to is synced with windms
		        /*
		        Restrict csc user from adding stock holding location once ship to is synced with windms
		        EP_StorageLocationTriggerHelper.restrictOnePrimaryLocation(listOfNewStorageLocations);
		        */
		        // Common Method to Validate Supply Option
		        EP_StorageLocationTriggerHelper.validateSupplyOption(listOfNewStorageLocations);
		        /*//validation - one primary location
		        EP_StorageLocationTriggerHelper.restrictOnePrimaryLocation(listOfNewStorageLocations);
		        //validation - one secondary location
		        EP_StorageLocationTriggerHelper.restrictOneSecondaryLocation(listOfNewStorageLocations);*/
		        //Set Supply Location Status
		        /**DEFECT 43913 FIX START**/ 
		        //EP_StorageLocationTriggerHelper.setSupplyLocationStatus(listOfNewStorageLocations);
		        /**DEFECT 43913 FIX END**/
		    }
		    catch(Exception e){
		        EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_INSERT, EP_StorageLocationTriggerHandler.class.getName(), ApexPages.severity.ERROR);
		    }
        }
	}
    /**
     * @author <Ashok Arora>
     * @date <03/11/2015>
     * @description <This method performs logic which need to pre execute 
    *               on before update event of supply option record.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return NONE
     */
     public static void doBeforeUpdate(List<EP_Stock_Holding_Location__c>listOfNewStorageLocations
      ,map<id,EP_Stock_Holding_Location__c>mapOfOldStorageLocations
      ,map<id,EP_Stock_Holding_Location__c>mapOfNewStorageLocations
      ){
        EP_GeneralUtility.Log('Public','EP_StorageLocationTriggerHandler','doBeforeUpdate');
        if(executeStorageLocationTrigger){
                isExecuteBeforeUpdate = true;//set true to avoid re-execution
            try{
                // Common Method to Validate Supply Option
                EP_StorageLocationTriggerHelper.validateSupplyOption(listOfNewStorageLocations);
                /*//validation - one primary location
                EP_StorageLocationTriggerHelper.restrictOnePrimaryLocation(listOfNewStorageLocations);
                //validation - one secondary location
                EP_StorageLocationTriggerHelper.restrictOneSecondaryLocation(listOfNewStorageLocations);*/
                //Set Supply Location Status
                //cannot change status to inactive
                //L4_45352_START
                //EP_StorageLocationTriggerHelper.cannotChangeStatusToInactive(mapOfNewStorageLocations,mapOfOldStorageLocations);
                //L4_45352_END
            }
            catch(Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_UPDATE, EP_StorageLocationTriggerHandler.class.getName(), ApexPages.severity.ERROR);
            }
        }
}

    /**
     * @author <Ashok Arora>
     * @date <03/11/2015>
     * @description <This method performs logic which need to execute 
    *               on before and after insert event of supply option record.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return NONE
     */
     public static void doBeforeInsertUpdate(List<EP_Stock_Holding_Location__c>listOfStorageLocations)
     {
        EP_GeneralUtility.Log('Public','EP_StorageLocationTriggerHandler','doBeforeInsertUpdate');
        if(executeStorageLocationTrigger){
	        try{
	            //to check route allocation has been set on ship to 
	            EP_StorageLocationTriggerHelper.checkShipToRouteAllocation(listOfStorageLocations);
	        }
	        catch(Exception e){
	            EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_INSERT_UPDATE, EP_StorageLocationTriggerHandler.class.getName(), ApexPages.severity.ERROR);
	        }
        }
    }
     /**L4_45352_start**/
     /**
     * @author <Accenture>
     * @description <This method performs logic which need to execute 
    *               on before delete event of supply option record.>
     * @param List<EP_Stock_Holding_Location__c>
     * @return NONE
     */
     public static void doBeforeDelete(List<EP_Stock_Holding_Location__c> listOfStorageLocations)
     {
         system.debug('called >>>');
        EP_GeneralUtility.Log('Public','EP_StorageLocationTriggerHandler','doBeforeDelete');
        if(executeStorageLocationTrigger){
                system.debug('executeStorageLocationTrigger >>>'+executeStorageLocationTrigger);
            try{
                EP_StorageLocationTriggerHelper.isSellToSupplyOptionPrimaryonShipTo(listOfStorageLocations);
            }
            catch(Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_DELETE, EP_StorageLocationTriggerHandler.class.getName(), ApexPages.severity.ERROR);
            }
        }
    }
    /**L4_45352_end**/
    
}