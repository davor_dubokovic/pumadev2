/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationBlockedToActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 06-Blocked to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationBlockedToActive extends EP_AccountStateTransition {

    public EP_ASTStorageLocationBlockedToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBlockedToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBlockedToActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBlockedToActive',' isGuardCondition');        
        return true;
    }
}