@isTest
private class EP_AccountState_UT {
    
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        //List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        //List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    //Method has no implementation, hence adding dummy assert  
    static testMethod void doOnEntry_test() {
        EP_AccountState localObj = new EP_AccountState();
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
    }
    //Method has no implementation, hence adding dummy assert  
    static testMethod void doOnExit_test() {
        EP_AccountState localObj = new EP_AccountState();
        Test.startTest();
        localObj.doOnExit();  
        Test.stopTest();
        System.assertEquals(true, true);
    }
    static testMethod void isInboundTransitionPossible_test() {
        EP_AccountState localObj = new EP_AccountState();
        Test.startTest();
        Boolean isInboundTranPossible =localObj.isInboundTransitionPossible();  
        Test.stopTest();
        System.assertEquals(true, isInboundTranPossible);
    }
    
    static testMethod void doTransition_PositiveScenariotest() {
        EP_AccountState localObj = new EP_AccountState();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void doTransition_ProspectNegativeScenariotest() {
        EP_AccountState localObj = new EP_AccountState();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EP_AccountConstant.PROSPECT+'To'+ EP_AccountConstant.ACCOUNTSETUP);
        localObj.setAccountContext(obj,oe);
        string errorMessage;
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }Catch(Exception e){
            errorMessage = e.getMessage();
        }
        Test.stopTest();
        System.assertEquals(true,string.isNotBlank(errorMessage));
        system.debug('---errorMessage--'+errorMessage);
        System.assertEquals(EP_AccountConstant.PROSPECT_TO_BASICDATA_MESSAGE, errorMessage);
    }
    
    static testMethod void doTransition_OtherThanProspectNegativeScenariotest() {
        EP_AccountState localObj = new EP_AccountState();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EP_AccountConstant.BASICDATASETUP+'To'+ EP_AccountConstant.PROSPECT);
        localObj.setAccountContext(obj,oe);
        string errorMessage;
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }Catch(Exception e){
            errorMessage = e.getMessage();
        }
        Test.stopTest();
        System.assertEquals(true,string.isNotBlank(errorMessage));
        system.debug('---errorMessage--'+errorMessage);
        System.assert(errorMessage.contains('Cannot change status'));
    }

    static testMethod void setAccountDomainObject_test() {
        EP_AccountState localObj = new EP_AccountState();
        EP_AccountDomainObject accountDomainObj = EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario();
        Test.startTest();
        localObj.setAccountDomainObject(accountDomainObj);  
        Test.stopTest();
        System.assertEquals(accountDomainObj.getAccount().Id, localObj.account.getAccount().Id);
    }

    static testMethod void setAccountContext_test() {
        EP_AccountState localObj = new EP_AccountState();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
        Test.startTest();
        localObj.setAccountContext(obj,oe);
        Test.stopTest();
        System.AssertEquals(oe,localObj.accountEvent);
        System.AssertEquals(obj,localObj.account );
    }

    static testMethod void ConvertCustomSettingsToObject_test() {
        EP_AccountState localObj = new EP_AccountState();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
        String stateStr = '01-Prospect';
        Test.startTest();
        localObj.setAccountContext(obj,oe);
        List<EP_AccountStateTransition> result = localObj.ConvertCustomSettingsToObject(stateStr);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    
    static testMethod void getStateTransitions_Test(){
        EP_AccountState localObj = new EP_AccountState();
        Test.startTest();
        List<EP_State_Transitions__c> result = localObj.getStateTransitions('SellTo', '02-Basic Data Setup', '02-BasicDataSetupTo08-Rejected');
        Test.stopTest();
        system.assertEquals(true, result.size() > 0);
    }
    
    static testMethod void convertCustomSetting_Test(){
        EP_AccountState localObj = new EP_AccountState();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
        Test.startTest();
        localObj.account= obj;
        localObj.accountEvent = oe;
        List<EP_State_Transitions__c> transitionList = localObj.getStateTransitions('SellTo', '01-Prospect', '01-ProspectTo01-Prospect');
        List<EP_AccountStateTransition> result = localObj.convertCustomSetting(transitionList);
        Test.stopTest();
        system.assertEquals(true, result.size() > 0);
    }
    
    static testMethod void convertCustomSetting_ExceptionTest(){
        EP_AccountState localObj = new EP_AccountState();
        EP_State_Transitions__c invalidTransition = new EP_State_Transitions__c();
        invalidTransition.Name = 'InvalidTransition';
        invalidTransition.Transition_Class_Type__c = 'test';
        invalidTransition.EntityType__c = 'aaa';
        invalidTransition.Event__c = 'xyz';
        invalidTransition.State_Name__c = 'abc';
        
        List<EP_State_Transitions__c> transitionList = new List<EP_State_Transitions__c>{invalidTransition};
        string errorMsg ;
        Test.startTest();
        try{
        List<EP_AccountStateTransition> result = localObj.convertCustomSetting(transitionList);
        }catch(Exception e){
            errorMsg  = e.getMessage();
        }
        Test.stopTest();
        system.assertequals(true,  string.isNotBlank(errorMsg));
    }
}