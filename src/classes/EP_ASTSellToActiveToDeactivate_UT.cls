@isTest
public class EP_ASTSellToActiveToDeactivate_UT
{
    private static testmethod void isGuardCondition_test()
    {    EP_ASTSellToActiveToDeactivate localobj = new EP_ASTSellToActiveToDeactivate();
         Test.startTest();
         boolean result = localobj.isGuardCondition();
         Test.stopTest();
         System.AssertEquals(true,result);
    }
}