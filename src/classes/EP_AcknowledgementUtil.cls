public with sharing class EP_AcknowledgementUtil {
   private static final string METHOD_NAME = 'createDataSet';
   private static final string CLASS_NAME = 'EP_AcknowledgementUtil';
   
    /** This method creates dataset for acknowledgement
    *  @author          Accenture
    *  @date            4/1/2016
    *  @name            createDataSet
    *  @param           String Name, String seqId, String errorCode, String errorDescription
    *  @return          EP_AcknowledmentGenerator.cls_dataset
    *  @throws          NA 
    */
    public static EP_AcknowledgementStub.dataSet createDataSet(String Name, String seqId, String errorCode, String errorDescription) {
        EP_GeneralUtility.Log('Public','EP_AcknowledgementUtil','createDataSet');
        EP_AcknowledgementStub.dataSet dataSet;
        try {
            dataSet = new EP_AcknowledgementStub.dataSet();
            dataSet.name = name;
            dataSet.seqId = seqId;
            dataSet.errorCode = errorCode;
            dataSet.errorDescription = errorDescription;
            } catch (Exception exp) {
            // exception handling logic
            EP_LoggingService.logHandledException(exp,
                EP_Common_Constant.EPUMA,
                METHOD_NAME, CLASS_NAME, ApexPages.Severity.ERROR);
        }
        return dataset;
    }
    
    /**
	* @author 			Accenture
	* @name				doDML
	* @date 			03/13/2017
	* @description 		Will perform the DML operations and sends back the appropriate acknowledgement datasets
	* @param 			List<Account>,List<EP_AcknowledgementStub.dataset>
	* @return 			List<EP_AcknowledgementStub.dataset>
	*/
    public static boolean doDML(List<Account> customersToUpdate, List<EP_AcknowledgementStub.dataset> ackDatasets){
    	EP_GeneralUtility.Log('Public','EP_AcknowledgementUtil','doDML');
    	boolean recordFailed = false;
    	EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
    	string errorDescription='';
    	if(!customersToUpdate.isEmpty()){
            List<Database.SaveResult> results =  DataBase.update(customersToUpdate,false);
            for(Integer count = 0; count < results.size(); count++){
                if(!results[count].isSuccess()){
                	recordFailed= true;
                	errorDescription = EP_GeneralUtility.getDMLErrorMessage(results[count]);
                }
                ackDataset = EP_AcknowledgementUtil.createDataset(EP_COMMON_CONSTANT.CUSTOMER,customersToUpdate[count].EP_Source_Seq_Id__c,errorDescription,errorDescription);
                ackDatasets.add(ackDataset);
            }
        }
        return recordFailed;
    }
    
    public static boolean doDML(List<EP_Bank_Account__c> banksToUpsert, List<EP_AcknowledgementStub.dataset> ackDatasets){
    	EP_GeneralUtility.Log('Public','EP_AcknowledgementUtil','doDML');
    	boolean recordFailed = false;
    	EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
    	string errorDescription='';
    	if(!banksToUpsert.isEmpty()){
            EP_BankAccountTriggerHandler.isExecutedByIntegrationUser = true;
            List<DataBase.UpsertResult> results = DataBase.Upsert(banksToUpsert,false);
            for(Integer count = 0; count < results.size(); count++){
                if(!results[count].isSuccess()){
                    recordFailed = true;
                    errorDescription = EP_GeneralUtility.getDMLErrorMessage(results[count]);
                }
                ackDataset = EP_AcknowledgementUtil.createDataset(EP_COMMON_CONSTANT.BANK_ACCOUNT,banksToUpsert[count].EP_Source_Seq_Id__c,errorDescription,errorDescription);
                ackDatasets.add(ackDataset);
            }
        }
        return recordFailed;
    }
}