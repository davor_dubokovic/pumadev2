@isTest
public class EP_PortalCancelOrderPageController_Test {

    @isTest 
    static void testOrderNull() {
        String result = EP_PortalCancelOrderPageController.cancelOrder('');
        System.assertEquals(Label.EP_Order_Id_cannot_be_null, result);
    }

    @isTest 
    static void testOrderNotFound() {
    	String orderId = '12345';
        String result = EP_PortalCancelOrderPageController.cancelOrder(orderId);
        System.assertEquals(string.format(Label.EP_Order_Id_not_found, new List<String>{orderId}), result);
    }

    @isTest 
    static void testOrderLocked() {
		csord__Order__c newOrder = createTestOrder();
		newOrder.EP_Delivery_Type__c  = 'Delivery';
		newOrder.csord__Status2__c = 'Delivered';
		insert newOrder;

        String result = EP_PortalCancelOrderPageController.cancelOrder(newOrder.Id);
        System.assertEquals(Label.EP_Order_Cancel_Msg, result);
    }

    @isTest 
    static void testOrderCancellationCheckDone() {
		csord__Order__c newOrder = createTestOrder();
		newOrder.Cancellation_Check_Done__c = true;
		insert newOrder;

        String result = EP_PortalCancelOrderPageController.cancelOrder(newOrder.Id);
        System.assertEquals(Label.EP_Order_Already_Checked, result);
    }

    @isTest 
    static void testOrderCutOffPassed() {
		csord__Order__c newOrder = createTestOrder();
		newOrder.EP_Delivery_Type__c  = 'Consumption';
		insert newOrder;

        String result = EP_PortalCancelOrderPageController.cancelOrder(newOrder.Id);
        System.assertEquals(Label.EP_Cut_off_Matrix_Passed, result);
    }

    @isTest 
    static void testOrderException() {
		csord__Order__c newOrder = createTestOrder();
		insert newOrder;

        String result = EP_PortalCancelOrderPageController.cancelOrder(newOrder.Id);
        System.assert(result != '');
    }

    private static csord__Order__c createTestOrder(){
    	EP_Freight_Matrix__c fm = Ep_TestDataUtility.createFreightMatrix();
		insert fm;

    	Account newAccount = Ep_TestDataUtility.createSellToAccount(null, fm.Id);
    	insert newAccount;
    	
		csord__Order__c newOrder = EP_TestDataUtility.createCSOrder(newAccount.Id, null, null);
		return newOrder;
    }
}