/* 
   @Author 			Accenture
   @name 			EP_CustomerOtherAdjustmentMapper_UT
   @CreateDate 		05/05/2017
   @Description		Test class for EP_CustomerOtherAdjustmentMapper
   @Version 		1.0
*/
@isTest
public class EP_CustomerOtherAdjustmentMapper_UT {
	
	/**
	* @author 			Accenture
	* @name				getCustomerOtherAdjustmentRecordsByUniqueId_test
	* @date 			02/08/2017
	* @description 		Test method used to get the customer adjustment records by unique Id
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void getCustomerOtherAdjustmentRecordsByUniqueId_test() {
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(sellTo.Id,paymentTerm.Id,sellTo.EP_Puma_Company_Code__c);
		Database.insert(custAdjust);
		
		EP_Customer_Other_Adjustment__c custAdj =[select id,Name,EP_Client_ID__c,EP_Bill_To__r.AccountNumber from EP_Customer_Other_Adjustment__c limit 1];
		Set<string> setOfCustomerAdjustments = new Set<string>();
		setOfCustomerAdjustments.add((custAdj.EP_Client_ID__c+ '-' + custAdj.EP_Bill_To__r.AccountNumber + '-' + custAdj.Name).toUpperCase());
		
		EP_CustomerOtherAdjustmentMapper localObj = new EP_CustomerOtherAdjustmentMapper();
		Test.startTest();
		List<EP_Customer_Other_Adjustment__c> result = localObj.getCustomerOtherAdjustmentRecordsByUniqueId(setOfCustomerAdjustments);
		Test.stopTest();
		System.AssertEquals(result.size(), 1);
	}
	
	/**
	* @author 			Accenture
	* @name				getCustomerOtherAdjustmentMapByUniqueId_test
	* @date 			02/08/2017
	* @description 		Test method used to get the customer adjustment map by unique Id
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void getCustomerOtherAdjustmentMapByUniqueId_test() {
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(sellTo.Id,paymentTerm.Id,sellTo.EP_Puma_Company_Code__c);
		Database.insert(custAdjust);
		
		EP_Customer_Other_Adjustment__c custAdj =[select id,Name,EP_Client_ID__c, EP_Bill_To__r.AccountNumber from EP_Customer_Other_Adjustment__c limit 1];
		Set<string> setOfCustomerAdjustments = new Set<string>();
		setOfCustomerAdjustments.add((custAdj.EP_Client_ID__c+ '-' +custAdj.EP_Bill_To__r.AccountNumber + '-' + custAdj.Name).toUpperCase());
		
		EP_CustomerOtherAdjustmentMapper localObj = new EP_CustomerOtherAdjustmentMapper();
		Test.startTest();
		Map<String,EP_Customer_Other_Adjustment__c> result = localObj.getCustomerOtherAdjustmentMapByUniqueId(setOfCustomerAdjustments);
		Test.stopTest();
		System.AssertEquals(result.size(), 1);
	}
    
}