@isTest
private class EP_ApprovalWorkflowShipTo_Test {
    private static Contact contactRec;
    private static Account sellToAccount;
    private static Account shipTo;
    private static EP_Tank__c tank;
    private static EP_Freight_Matrix__c freightMatrix;
    private static EP_Country__c country;
    private static EP_Region__c region;
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia'; 
    
    /* Method is creating test data 
     **/
    private static void loadData(){
        country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country,false);
        
        region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        Database.insert(region,false);
        
        sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        Database.insert(sellToAccount, false);
        contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
        shipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null);
        shipTo.EP_Country__c = country.id;
        Database.insert(shipTo, false);
    }
    
     /*
        CHeck for Initiated Pricing Review
        Update Pricebook On Sell To before review completion
     */
    static testMethod void pricingReviewTest() {
        Id pricebookId = Test.getStandardPricebookId();
        Profile SMAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.SM_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        UserRole SMTMRole = [Select id from UserRole where developerName = 'EP_SM_TM_Team_AU' limit 1];  
        UserRole internalReviewUserRole = [Select id from UserRole where developerName = 'EP_Internal_Review_Team_AU' limit 1];                 
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
        smTMUser.UserRoleId =  SMTMRole.id;        
        system.runAs(SMTMuser){
            loadData();
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        
            tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
            database.insert(tank);
            
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Ship TO status
            shipTo.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(shipTo);
            
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = shipTo.Id
            );
            Insert att;
        }
        
        Profile logisticsAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.LOGISTICS_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User logisticsUser = EP_TestDataUtility.createUser(logisticsAgent.id);
        logisticsUser.UserRoleId =  internalReviewUserRole.id;
        database.insert(logisticsUser);
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SHIPTO limit 1];
        system.debug('====action=='+action);
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = logisticsUser.Id
        );
        insert gm;
        
        system.runAs(logisticsUser)
        {           
            action.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(action, false);
            EP_Action__c act1 = [Select Id, Name,EP_Status__c FROM EP_Action__c WHERE EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SHIPTO limit 1];
            system.assertEquals(EP_Common_Constant.ACT_ASSIGNED_STATUS,act1.EP_Status__c);
            shipTo.EP_PriceBook__c = pricebookId; 
            Database.update(shipTo);
            action.EP_Site_Visit__c = 'Visit Not Required';
            action.EP_Reason_No_Visit__c = 'TBD';
            action.EP_Site_Visit_Comments__c = 'test';
            action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(action, false);
            system.assert(sv.isSuccess());
        }
         
        system.runAs(SMTMuser){
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false; 
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            Database.saveresult sv = database.update(shipTo,false);
            system.assert(!sv.isSuccess()); 
            List<EP_Action__c> actions = [Select Id, OwnerId from EP_Action__c  where  RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            system.assert(actions != Null);
            system.assert(!actions.isEmpty());
        }
        
        Profile pricingAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.PRICING_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User pricingUser = EP_TestDataUtility.createUser(pricingAgent.id);
        database.insert(pricingUser);
        EP_Action__c act;
        system.runAs(SMTMuser){
            act = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            g = [Select Id from Group where Id =: act.OwnerId ];
            gm = new GroupMember(
                GroupId = g.Id,
                UserOrGroupId = pricingUser.Id
            );
            insert gm;
        }
        system.runAs(pricingUser){
            
            act.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(act, false);
            EP_Action__c act1 = [Select Id, Name,EP_Status__c FROM EP_Action__c WHERE EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            system.assertEquals(EP_Common_Constant.ACT_ASSIGNED_STATUS,act1.EP_Status__c);
            act.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(act, false);
            system.assertEquals(pricingUser.id,[Select id, OwnerId from EP_Action__c where id=: act.id].ownerID);
            system.assert(sv.isSuccess());
        }
        system.runAs(SMTMuser){
            //19825:Customer_HLBR-E2E 002.0_R_To check that Ship-To can be set to Account Setup State without the CSC Review.
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_SET_UP; //Change Ship To Status Account Set up
            Database.saveresult sv = database.update(shipTo);
            system.assert(sv.isSuccess());
            
            // 19824- To check that CSC review is not triggered on the Ship To, but logistics and Pricing are triggered.
            List<EP_Action__c> accList = new List<EP_Action__c>([Select Id, EP_Action_Name__c, EP_Record_Type_Name__c, 
                    EP_Country__c, EP_Region__c, OwnerId from EP_Action__c where EP_Account__c =: shipTo.Id order by createdDate ASC]);
            
            system.assertEquals(2,accList.size());
            system.assertEquals(EP_Common_Constant.ACT_SHIP_TO_LOGISTICS_REVIEW_RT,accList[0].EP_Action_Name__c);
            system.assertEquals(EP_Common_Constant.ACT_PRICING_REVIEW_RT,accList[1].EP_Action_Name__c);
            
        }
        
        
    }
    
    /*
        CHeck for wrong Pricing Review user
        Update Fright price On Ship To before review completion
        Test Cases - 18427 Work Unit 11
     */
    static testMethod void wrongUserTest() {
        Id pricebookId = Test.getStandardPricebookId();
        Profile SMAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.SM_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        UserRole SMTMRole = [Select id from UserRole where developerName = 'EP_SM_TM_Team_AU' limit 1];  
        UserRole internalReviewUserRole = [Select id from UserRole where developerName = 'EP_Internal_Review_Team_AU' limit 1];                 
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
        smTMUser.UserRoleId =  SMTMRole.id;        
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        system.runAs(SMTMuser){
            loadData();
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        
            tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
            database.insert(tank);
            
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Ship TO status
            shipTo.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(shipTo);
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = shipTo.Id
            );
            Insert att;
        }
        
        Profile logisticsAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.LOGISTICS_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User logisticsUser = EP_TestDataUtility.createUser(logisticsAgent.id);
        logisticsUser.UserRoleId =  internalReviewUserRole.id;
        
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SHIPTO limit 1];
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
        GroupId = g.Id
        
        );
        
        System.runAs(adminUser){
            database.insert(logisticsUser);
            gm.UserOrGroupId = logisticsUser.Id;
            insert gm;
        }
        
        freightMatrix = EP_TestDataUtility.createFreightMatrix();
        System.runAs(adminUser){
             database.insert(freightMatrix);
        }
       
        system.runAs(logisticsUser)
        {           
            action.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(action, false);
            EP_Action__c act1 = [Select Id, Name,EP_Status__c FROM EP_Action__c WHERE EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SHIPTO limit 1];
            system.assertEquals(EP_Common_Constant.ACT_ASSIGNED_STATUS,act1.EP_Status__c);
            shipTo.EP_Freight_Matrix__c = freightMatrix.id; 
            Database.update(shipTo);
            action.EP_Site_Visit__c = 'Visit Not Required';
            action.EP_Reason_No_Visit__c = 'TBD';
            action.EP_Site_Visit_Comments__c = 'test';
            action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(action, false);
            system.assert(sv.isSuccess());
        }
         
        system.runAs(SMTMuser){
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false; 
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            Database.saveresult sv = database.update(shipTo,false);
            system.assert(!sv.isSuccess()); 
            List<EP_Action__c> actions = [Select Id, OwnerId from EP_Action__c  where  RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            system.assert(actions != Null);
            system.assert(!actions.isEmpty());
        }
        
        Profile pricingAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.PRICING_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User pricingUser = EP_TestDataUtility.createUser(pricingAgent.id);
        database.insert(pricingUser);
        EP_Action__c act;
        system.runAs(SMTMuser){
            act = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            g = [Select Id from Group where Id =: act.OwnerId ];
            gm = new GroupMember(
                GroupId = g.Id,
                UserOrGroupId = logisticsUser.Id
            );
            insert gm;
        }
        system.runAs(pricingUser){
            
            act.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(act, false);
            act.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(act, false);
            system.debug('==========='+sv);
            system.assertNotEquals(pricingUser.id,[Select id, OwnerId from EP_Action__c where id=: act.id].ownerID);
            system.assert(!sv.isSuccess());
        }
    }
    
    /*
        Cannot change status back after review marked as completed
     */
    static testMethod void cannotChangeStatusBackAfterCompletionTest() {
        Id pricebookId = Test.getStandardPricebookId();
        Profile SMAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.SM_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        UserRole SMTMRole = [Select id from UserRole where developerName = 'EP_SM_TM_Team_AU' limit 1];  
        UserRole internalReviewUserRole = [Select id from UserRole where developerName = 'EP_Internal_Review_Team_AU' limit 1];                 
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
        smTMUser.UserRoleId =  SMTMRole.id;        
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        system.runAs(SMTMuser){
            loadData();
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        
            tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
            database.insert(tank);
            
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Ship TO status
            shipTo.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(shipTo);
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = shipTo.Id
            );
            Insert att;
        }
        
        Profile logisticsAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.LOGISTICS_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User logisticsUser = EP_TestDataUtility.createUser(logisticsAgent.id);
        logisticsUser.UserRoleId =  internalReviewUserRole.id;
        
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SHIPTO limit 1];
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
        GroupId = g.Id
        
        );
        
        System.runAs(adminUser){
            database.insert(logisticsUser);
            gm.UserOrGroupId = logisticsUser.Id;
            insert gm;
            
        }
        
        
        freightMatrix = EP_TestDataUtility.createFreightMatrix();
        System.runAs(adminUser){
             database.insert(freightMatrix);
        }
       
        system.runAs(logisticsUser)
        {    
            Account acc = [Select id, BSM_GM_Review_Required_On_Company__c,  EP_BSM_GM_Review_Completed__c, EP_KYC_Review_Required__c, EP_CSC_Review_Completed__C,
                            EP_KYC_Review_Completed__c,EP_Logistics_Review_Completed__C, EP_Pricing_Review_Completed__c from Account where 
                            id=: shipTo.id];
            system.debug('==acc======'+acc);                
            action.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(action, false);
            EP_Action__c act1 = [Select Id, Name,EP_Status__c FROM EP_Action__c WHERE EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SHIPTO limit 1];
            system.assertEquals(EP_Common_Constant.ACT_ASSIGNED_STATUS,act1.EP_Status__c);
            shipTo.EP_Freight_Matrix__c = freightMatrix.id; 
            Database.update(shipTo);
            action.EP_Site_Visit__c = 'Visit Not Required';
            action.EP_Reason_No_Visit__c = 'TBD';
            action.EP_Site_Visit_Comments__c = 'test';
            action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(action, false);
            system.assert(sv.isSuccess());
        }
         
        system.runAs(SMTMuser){
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false; 
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            Database.saveresult sv = database.update(shipTo,false);
            system.assert(!sv.isSuccess()); 
            List<EP_Action__c> actions = [Select Id, OwnerId from EP_Action__c  where  RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            system.assert(actions != Null);
            system.assert(!actions.isEmpty());
        }
        
        Profile pricingAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.PRICING_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User pricingUser = EP_TestDataUtility.createUser(pricingAgent.id);
        database.insert(pricingUser);
        EP_Action__c act;
        system.runAs(SMTMuser){
            act = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: shipTo.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            g = [Select Id from Group where Id =: act.OwnerId ];
            gm = new GroupMember(
                GroupId = g.Id,
                UserOrGroupId = pricingUser.Id
            );
            insert gm;
        }
        system.runAs(pricingUser){
            
            act.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(act, false);
            act.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(act, false);
            act.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
            sv = database.update(act, false);
            system.assertEquals(pricingUser.id,[Select id, OwnerId from EP_Action__c where id=: act.id].ownerID);
            system.assert(!sv.isSuccess());
        }
    }
}