@isTest
/*
	Test class to cover test scenarios of change request controller.
*/
private class EP_ChangeRequestController_CSCUser_Test {
    private static Account nonVmiShipToAccount;
    private static Account sellToAccount;
    private static string TANKOBJ = 'EP_Tank__c';
    private static EP_Tank__C tankObj1;
    private static Product2 product;
    private static Contact contactRec;
    private static string ID = 'id';
    private static string TYPE = 'type';
    private static string RETURN_TYPE = 'returl';
    private static string OPERATION = 'op';
    private static string UPDATE_OPERATION = 'UPDATE';
    private static string NEW_OPERATION = 'NEW';
    private static string RECORD_TYPE = 'rtype';
    private static String ACCOUNTSTR = 'Account';
    private static String SHIPTONONVMIRECORDTYPE = 'EP_Non_VMI_Ship_To';
    private static String SELLTORECORDTYPE = 'EP_Sell_To';
    private static String BANKACCOUNTOBJ = 'EP_Bank_Account__c';
    private static String TANK_RECORD_TYPE = 'EP_Retail_Site_Tank';
    private static string RECORD_TYPE_NAME = 'recTypeName';
   /* 
      Create common Data for all methods 
    */
    private static void loadTestData() {
        
        EP_ExceptionEmailTo__c exEm = new EP_ExceptionEmailTo__c(SetupOwnerId=UserInfo.getOrganizationId(), Email__c = 'test@xyz.com');
        Database.insert(exEm);
        
        sellToAccount =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
        Database.insert(sellToAccount, false);
        contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
           
        EP_Freight_Matrix__c FreightMat = EP_TestDataUtility.createFreightMatrix();
        Database.insert(FreightMat);
        
        Account billAcc = EP_TestDataUtility.createBillToAccount();
        billAcc.EP_Freight_Matrix__c = FreightMat.id;
        Database.insert(billAcc);
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; 
        sellToAccount.EP_Bill_To_Account__c = billAcc.id;   
        Database.update(sellToAccount, false); 
        
        
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        nonVmiShipToAccount.EP_Delivery_Type__c = 'Ex-Rack';
        nonVmiShipToAccount.EP_Pumps__c= 10;
        //database.insert(nonVmiShipToAccount,false);
        Database.insert(nonVmiShipToAccount);
        
        //create Product
        product = EP_TestDataUtility.createTestRecordsForProduct();
        // insert tanks in order to mark vmi account as Active
        tankObj1 = EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,product.id);
        tankObj1.EP_Tank_Code__c='2';
        database.insert(tankObj1,false);
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(nonVmiShipToAccount);
        // Make non Vmi account active
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(nonVmiShipToAccount);
        
        EP_Bank_Account__c bankAccObj =  EP_TestDataUtility.createBankAccount(sellToAccount.id);
        Database.insert(bankAccObj);  
    }
    
     /* 
      update operation on sell to account
     */
    static testMethod void testChangeRequestSellTo(){
        //CREATE CSC USER
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){
        test.startTest();   
        loadTestData();
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        //Update Operation For Sell To Account
        ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(TYPE, ACCOUNTSTR); 
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
		ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, EP_Common_Constant.SELL_TO);
            
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        controller.allFieldValues[0].ischanged = true;
        test.stopTest();
        //system.assertEquals('Ordered',controller.allFieldValues[0].crlobj.EP_Original_Value__c);
        controller.allFieldValues[0].crlobj.EP_New_Value__c = '123';
        controller.submit();
        controller.cancel();
        system.assert(controller.request.id != null);
        }   
    }
    //13775-RW166_005a_To validate that all forms contain the same validations on fields as it was while creating the accounts through standard screens.
    /* 
        validation check on page
    */
    static testMethod void testCheckValidationOnBillingBasis(){
        //CREATE CSC USER
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){ 
        test.startTest();    
        loadTestData();
            //Check Validation Rule on sell to account
        ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(TYPE, ACCOUNTSTR);
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
		ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, EP_Common_Constant.SELL_TO);
        EP_ChangeRequestController controller4 = new EP_ChangeRequestController();
        set<string> availableFieldSet = new set<string>();
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        integer i=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller4.allFieldValues){
            fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), i);
            i++;
        }
        test.stopTest();
        system.assert(fieldByIndexMap.containskey('EP_Billing_Basis__c'.toLowerCase()));
        controller4.allFieldValues[fieldByIndexMap.get('EP_Billing_Basis__c'.toLowerCase())].crlobj.EP_New_Value__c = 'Periodic';
        controller4.submit();
        system.assert(controller4.request.id == null); //13775
        }
       } 
   
       // Bank Account New Record and record update using CSC and Portal user
        /* 
            Bank account fields check for CSC user
        */
        static testMethod void testCheckAllBankAccountFields(){
        //CREATE CSC USER
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        /*System.runAs(cscUser){ 
        test.startTest();        
        loadTestData();
        EP_Bank_Account__c bankAccObj =  EP_TestDataUtility.createBankAccount(sellToAccount.id);
        Database.insert(bankAccObj,false);
        // Bank Account update operation for CSC user
			
        ApexPages.currentPage().getParameters().put(ID, bankAccObj.Id);
        ApexPages.currentPage().getParameters().put(TYPE, BANKACCOUNTOBJ);
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+bankAccObj.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        integer i=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
            fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), i);
            i++;
        }
        test.stopTest();
        system.assert(fieldByIndexMap.containskey('EP_Bank_Account_No__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Bank_Branch_No__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Bank_Name__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_IBAN__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Swift_Code__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Bank_Account_Status__c'.toLowerCase()));
        system.assertEquals(false,fieldByIndexMap.containskey('EP_PostCode__c'.toLowerCase()));
        controller.allFieldValues[0].ischanged = true;
        controller.allFieldValues[0].crlobj.EP_New_Value__c = '132456';
        controller.submit();
        system.assert(controller.request.id != null);
        }  */
        
     }
     
     //Tank New and Update operation for CSC and Portal user
      /* 
            Tank fields check for CSC user
      */
      static testMethod void testFieldsInTankFieldSet(){
        //CREATE CSC USER
        
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        List<EP_Address_Fields__mdt> queryMetaData = new List<EP_Address_Fields__mdt>([select id,MasterLabel from EP_Address_Fields__mdt]);                   
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){
        test.startTest();         
        loadTestData();
        ApexPages.currentPage().getParameters().put(ID, tankObj1.Id);
        ApexPages.currentPage().getParameters().put(TYPE, TANKOBJ);
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+tankObj1.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Retail Site Tank');
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        Map<string, integer> fieldByIndexMap1 = new Map<string, integer>();
        integer count=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
            fieldByIndexMap1.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count);
            count++;
        }
        test.stopTest();
        system.assert(fieldByIndexMap1.containskey('EP_Capacity__c'.toLowerCase()));
        system.assert(fieldByIndexMap1.containskey('EP_Deadstock__c'.toLowerCase()));
        system.assert(fieldByIndexMap1.containskey('EP_Safe_Fill_Level__c'.toLowerCase()));
        system.assert(fieldByIndexMap1.containskey('EP_Tank_Status__c'.toLowerCase()));
        system.assert(fieldByIndexMap1.containskey('EP_Tank_Status__c'.toLowerCase()));
        system.assert(fieldByIndexMap1.containskey('EP_Reason_Blocked__c'.toLowerCase()));
        controller.allFieldValues[0].ischanged = true;
        controller.allFieldValues[0].crlobj.EP_New_Value__c = '132456';
        controller.submit();
        system.assert(controller.request.id != null);
        }
    }
 }