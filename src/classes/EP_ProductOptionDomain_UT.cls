/* 
      @Author <Kamendra Singh>
       @name <EP_ProductOptionDomain_UT>
       @CreateDate <24/11/2016>
       @Description  <This is apex test class for EP_ProductOptionDomain> 
       @Version <1.0>
    */
    @isTest
    public class EP_ProductOptionDomain_UT{
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description create test data for all required object in test methods
        */
        @TestSetup
        static void initData(){
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPriceBookWithCompany();
            
            //Create Sell To Account
            Account tempSellToAcc = EP_TestDataUtility.createSellToAccount(null,null);
            tempSellToAcc.EP_Puma_Company__c = temppricebook.EP_Company__c;
            tempSellToAcc.EP_Status__c= EP_Common_Constant.STATUS_ACTIVE;
            tempSellToAcc.CurrencyIsoCode = 'AUD';
            database.insert(tempSellToAcc);
            
            //create Ship To Account
            Account tempShipToAcc = EP_TestDataUtility.createShipToAccount(tempSellToAcc.Id,EP_TestDataUtility.VMI_SHIP_RT );
            database.insert(tempShipToAcc);
            
            //create Product2 
            Product2 tempProduct = EP_TestDataUtility.createProduct2(true); 
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = true;
            database.insert(tempPriceBookEntry);
            
            //Create Tank...
            EP_Tank__c  tempTank = EP_TestDataUtility.createTestEP_Tank(tempShipToAcc.id,tempProduct.id);
            database.insert(tempTank);
            
            //Create Product Option
            //EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempSellToAcc.Id,temppricebook.id);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check doActionBeforeInsert method.
        */
        static testMethod void doActionBeforeInsert_PositiveTest(){
            Account tempAccount  =  [select id,CurrencyIsoCode from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT];
            tempAccount.CurrencyIsoCode = 'AUD';
            database.update(tempAccount);
            
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
                ProductOptionDomain.doActionBeforeInsert();
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
        }
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check doActionBeforeUpdate method.
        */
        static testMethod void doActionBeforeUpdate_Test(){
            
            map<Id,EP_Product_Option__c> triggerOldMap =  new map<Id,EP_Product_Option__c>();
            Product2 tempProduct = [select id from Product2];
            
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPricebook2(false);
            tempPricebook.Name = 'PriceBook B';
            database.insert(tempPricebook);
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = true;
            database.insert(tempPriceBookEntry);
            
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            
            for(EP_Product_Option__c prodOption : lstProdOption){
                EP_Product_Option__c objProdOption = new EP_Product_Option__c(id=prodOption.Id,Price_List__c = tempPricebook.id);
                if(!triggerOldMap.containskey(objProdOption.id)){               
                    triggerOldMap.put(objProdOption.id,objProdOption);
                }
            }
            
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption,triggerOldMap);
            
            Test.startTest();
                ProductOptionDomain.doActionBeforeUpdate();
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
        }
        /**
        * @author kamendra.singh@accenture.com
        * @date 24/11/2017
        * @description Test to check doActionOnBeforeDelete method.
        */
        static testMethod void doActionOnBeforeDelete_Test(){
            list<EP_Product_Option__c> lstProdOption = [select id,Sell_To__c ,Price_List__c  from EP_Product_Option__c];
            EP_ProductOptionDomain ProductOptionDomain = new EP_ProductOptionDomain(lstProdOption);
            
            Test.startTest();
                ProductOptionDomain.doActionOnBeforeDelete();
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
        }
    }