/**
   @Author          Swapnil Gorane
   @name            EP_GroupMapper 
   @CreateDate      11/01/2017 
   @Description     This class contains all SOQLs related to Group Object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    11/01/2017          Swapnil Gorane          Added Method -> getGroupMembrRecordsAsPerGrpNames(Set<String>)
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_GroupMapper {
    /**  Constructor.
    *  @name             EP_GroupMapper 
    *  @param            NA
    *  @return           NA
    *  @throws           NA
    */  
    public EP_GroupMapper() {
        
    }
    
     /** This method is used to get group Members details from the given groups
    *  @author          Swapnil Gorane
    *  @date            11/01/2017
    *  @name            getGroupRecordsAsPerGrpNames
    *  @param           Set<String>
    *  @return          Group 
    *  @throws          NA
    */
    public Group getGroupMembrRecordsAsPerGrpNames(Set<String> grpName) { 
        Group grpObj = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name In :grpName];
        return grpObj ;
    }
    
    public list<Group> getGroupByUserRoles(List<UserRole> userRoles){
    	list<group> grouplist = new list<group>();
    	grouplist = [SELECT Id, RelatedId FROM Group WHERE RelatedId IN :userRoles ];
    	return grouplist;
    }
}