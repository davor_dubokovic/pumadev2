@isTest
public class EP_CustomerFundsUpdateHandler_UT
{
	@testSetup static void init() {
      List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
	static testMethod void processRequest_ValidJSON() {
		EP_CustomerFundsUpdateHandler localObj = new EP_CustomerFundsUpdateHandler();
		Account accountObj = EP_TestDataUtility.getSellTo();
		EP_CustomerFundsUpdateStub stub = EP_TestDataUtility.createInboundCustomerFundsMessage();
	    List<EP_CustomerFundsUpdateStub.customer> customers = stub.MSG.payload.any0.AvailableFunds.customer;
	    EP_CustomerFundsUpdateStub.customer customer = customers[0];
	    customer.identifier.billTo = string.valueOf(accountObj.AccountNumber);
	    customer.identifier.clientId = string.valueOf(accountObj.EP_Account_Company_Name__c);    
	    string jsonRequest = JSON.serialize(stub);       
		Test.startTest();
		String result = localObj.processRequest(jsonRequest);
		Test.stopTest();
		System.assertEquals(true,result != null);
	}
	
	static testMethod void processRequest_InvalidJSON() {
		EP_CustomerFundsUpdateHandler localObj = new EP_CustomerFundsUpdateHandler();
		Account accountObj = EP_TestDataUtility.getSellTo();
		EP_CustomerFundsUpdateStub stub = EP_TestDataUtility.createInboundCustomerFundsMessage();
	    List<EP_CustomerFundsUpdateStub.customer> customers = stub.MSG.payload.any0.AvailableFunds.customer;
	    EP_CustomerFundsUpdateStub.customer customer = customers[0];
	    customer.identifier.billTo = string.valueOf(accountObj.AccountNumber);
	    customer.identifier.clientId = string.valueOf(accountObj.EP_Account_Company_Name__c);    
	    string jsonRequest = JSON.serialize(stub); 
	    jsonRequest= jsonRequest.substring(0,jsonRequest.length()-2);      
		Test.startTest();
		String result = localObj.processRequest(jsonRequest);
		Test.stopTest();
		System.assertEquals(true,result != null);
	}
	
	static testMethod void parse_test() {
		EP_CustomerFundsUpdateStub stub = EP_TestDataUtility.createInboundCustomerFundsMessage();
		String jsonString = JSON.serialize(stub);
		Test.startTest();
		EP_CustomerFundsUpdateStub result = EP_CustomerFundsUpdateHandler.parse(jsonString);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	static testMethod void createAcknowledgementDatasets_WithoutErrors() {
		LIST<EP_CustomerFundsUpdateStub.Customer> customers = EP_TestDataUtility.createInboundListOfCustomerFunds();
		customers[0].SFAccount = new Account();
		Test.startTest();
		List<EP_AcknowledgementStub.dataset> result = EP_CustomerFundsUpdateHandler.createAcknowledgementDatasets(customers);
		Test.stopTest();
		System.assertEquals(true,result!=null);
	}
	
	static testMethod void createAcknowledgementDatasets_WithErrors() {
		LIST<EP_CustomerFundsUpdateStub.Customer> customers = EP_TestDataUtility.createInboundListOfCustomerFunds();
		customers[0].SFAccount = new Account();
		Test.startTest();
		customers[0].errorDescription ='Error';
		List<EP_AcknowledgementStub.dataset> result = EP_CustomerFundsUpdateHandler.createAcknowledgementDatasets(customers);
		Test.stopTest();
		System.assertEquals(true,result[0].errorDescription ==customers[0].errorDescription);
	}
}