@isTest
private class EP_LookUp_Controller_UT {
	
        @isTest static void goButton_Test() {
        	EP_AccountNotificationTestDataUtility.createLookUPData('select id,name from Contact ','Contact','Contact','where accountid= ');
        	Account acc = EP_TestDataUtility.getSellTo();
        	PageReference pageRef = Page.EP_LookUP;
                Test.setCurrentPage(pageRef);
                apexpages.CurrentPage().getParameters().put(EP_Common_Constant.OBJECTNAME, 'Contact');
                apexpages.CurrentPage().getParameters().put(EP_Common_Constant.ID, acc.id);
                Test.startTest();
                EP_LookUp_Controller lookUpObj = new EP_LookUp_Controller();
                lookUpObj.goButton();
                Test.stopTest();
                // Adding dummy assert
                system.assert(true);
	}

	@isTest static void hideClearSearchButton_Test() {
        	EP_AccountNotificationTestDataUtility.createLookUPData('select id,name from Contact ','Contact','Contact','where accountid= ');
        	Account acc = EP_TestDataUtility.getSellTo();
        	PageReference pageRef = Page.EP_LookUP;
                Test.setCurrentPage(pageRef);
                apexpages.CurrentPage().getParameters().put(EP_Common_Constant.OBJECTNAME, 'Contact');
                apexpages.CurrentPage().getParameters().put(EP_Common_Constant.ID, acc.id);
                Test.startTest();
                EP_LookUp_Controller lookUpObj = new EP_LookUp_Controller();
                lookUpObj.hideClearSearchButton();
                Test.stopTest();
                // Adding dummy assert
                system.assert(true);
	}
	
}