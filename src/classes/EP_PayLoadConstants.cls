/**
   @Author          CR Team
   @name            39  
   @CreateDate      01/13/2016
   @Description     This class contains constants for payload related classes.
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_PayLoadConstants {
    public static final string NEW_STR = 'New';
    public static final String NAV_CUSTOMER_FIELDS = 'Nav_CustomerFields';
    public static final String NAV_SHIP_TO_FIELDS = 'Nav_ShipToFields';
    public static final String NAV_BANK_FIELDS = 'Nav_BankAccountFields';
    public static final String LOMO_TANK_FIELDS = 'LM_TankFields';
    public static final String LOMO_SUPPLY_LOCATION_FIELDS = 'LOMO_SUPPLY_LOCATION_FIELDS';
    public static final String FROM_CUSTOMER_SHIP_TOS = ' ,ParentId from ChildAccounts where recordType.developerName IN:shipTOAdressTypes AND EP_Status__c =:statusSetUp),';
    public static final String FROM_CUSTOMER_BANK_ACCOUNTS = ' ,EP_Account__c from Bank_Accounts__r ) from Account where ID IN:sCustomerId' ;
    public static final String FROM_PRCNG_SHIP_TOS = ' ,ParentId from ChildAccounts where ID IN:sShipTo) from Account where ID IN:sCustomerId' ;
    public static final String FROM_SHIP_TO_TANKS = ' ,EP_Ship_To__c from Tank__r) ';
    public static final String FROM_SHIP_TO_SUPPLY_LOCATIONS = ' ,EP_Ship_To__c,RecordTypeId from Stock_Holding_Locations1__r),EP_Storage_Location__r.EP_Nav_Stock_Location_Id__c  from Account where Id IN :sShipToId';
    public static final String FROM_CUSTOMER_EDIT_NAV = ',EP_Puma_Company_Code__c FROM ACCOUNT WHERE ID IN:sCustomer';
    public static final String FROM_SHIP_TO_EDIT_LOMO = ' , EP_Puma_Company_Code__c FROM ACCOUNT WHERE ID IN :sShipToId';
    /**START - Defect 27961 Constant Variables changes - Added query constans to sync tanks and supply of Account set up ship to for active customer with WINDMS**/
    public static final String FROM_SHIP_TO_EDIT_TANKS = ' ,EP_Ship_To__c from Tank__r Where EP_Ship_To__r.EP_Status__c =:accountSetupStatus ) ';
    public static final String FROM_SHIP_TO_EDIT_SUPPLY_LOCATIONS = ' ,EP_Ship_To__c,RecordTypeId from Stock_Holding_Locations1__r Where EP_Ship_To__r.EP_Status__c =:accountSetupStatus ),EP_Storage_Location__r.EP_Nav_Stock_Location_Id__c  from Account where Id IN :sShipToId';
    /**END - Defect 27961 Constant Variable changes**/
    public static final String FROM_SHIP_TO_EDIT_NAV = ' ,EP_Puma_Company_Code__c FROM ACCOUNT WHERE ID IN :mShipToIds';
    
    public static final String FROM_SHIP_TANKS_EDIT_LS = ' , EP_Ship_To__c  FROM Tank__r WHERE ID IN :sShipToIds),EP_Puma_Company_Code__c FROM Account WHERE ID IN:mUpdateTanksShipTo.sTankIds';
    public static final String WHERE_RECORD_ID = ' where Id =:recordId';
    public static final String DATE_TIME_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'';
    public static final string RECORDTYPEID = 'RecordTypeId';
    public static final string ACCOUNTOBJ = 'Account';
    public static final string EP_STOCK_HOLDING_LOCATION_OBJ = 'EP_Stock_Holding_Location__c';
    public static final string EP_BANK_ACCOUNT_OBJ = 'EP_Bank_Account__c';
    public static final string EP_TANK_OBJ = 'EP_Tank__c';
    public static final string IDFIELD = 'Id';
    public static final string IDENTIFIER = 'Idenfier';
    public static final string ONE_POINT_ZERO = '1.0';
    public static final string CUSTOMER_STR = 'Customer';
    public static final string EP_ACCOUNT_COMPANY_NAME = 'EP_Puma_Company_Code__c';
    
    // VARIABLES TO TRACK EXCEPTION DETAILS
    public static final string EPUMA = 'ePuma';
    public static final string EP_PAYLOADHANDLER = 'EP_PayLoadHandler';
    // Method Name string for exception tracking
    public static final string CREATE_NAV_BULK_CUSTOMER_PAYLOAD_MTD = 'createNavBulkCustomerPayLoad';
    public static final string CREATE_NAV_CUSTOMER_PAYLOAD_MTD = 'createNavCustomerPayLoad';
    public static final string CREATE_NAV_SHIPTO_PAYLOAD_MTD = 'createNavShipToPayload';
    public static final string CREATE_NAV_BULK_SHIPTO_PAYLOAD_MTD =  'createNavBulkShipToPayload';
    public static final string CREATE_NAV_BULK_BANKACCOUT_PAYLOAD_MTD ='createNavBulkBankAccountPayLoad';
    public static final string CREATE_NAV_BANKACCOUT_PAYLOAD_MTD = 'createNavBankAccountPayload';
    public static final string CREATE_NAV_BULK_CSTMR_EDIT_PAYLOAD_MTD = 'createNAVBulkCstmrEditPayLoad';
    public static final string CREATE_NAV_BULK_SHIP_TO_EDIT_PAYLOAD_MTD = 'createNAVBulkShipToEditPayLoad';
    public static final string CREATE_NAV_CSTMR_EDIT_PAYLOAD_MTD = 'createNavCustomerEditPayLoad';
    public static final string CREATE_LOMO_BULK_SHIP_TO_EDIT_PAYLOAD_MTD = 'createLomoSoftBulkShipToEditPayLoad';
    public static final string CREATE_LOMOSOFT_SHIP_TO_EDIT_PAYLOAD_MTD = 'createLomoSoftShiptoEditPayLoad';
    public static final string CREATE_LOMOSOFT_BULK_SHIPTO_PAYLOAD_MTD = 'createLomoSoftBulkShiptoPayLoad';
    public static final string CREATE_LOMOSOFT_SHIPTO_PAYLOAD_MTD = 'createLomoSoftShiptoPayLoad';
    public static final string CREATE_LOMOSOFT_BULK_TANKS_PAYLOAD_MTD = 'createLomoSoftBulkTanksPayLoad';
    public static final string CREATE_LOMOSOFT_TANK_PAYLOAD_MTD = 'createLomoSoftTankPayload'; 
    public static final string CREATE_LOMOSOFT_STOCK_LOCATION_PAYLOAD_MTD = 'createLomoSoftStockLocationPayload';
    public static final String CREATE_LOMOSOFT_BULK_STOCK_LOCATIONS_PAYLOAD_MTD = 'createLomoSoftBulkStockLocationsPayLoad';
    public static final string CREATE_RECORD_PAYLOAD = 'createRecordPayload';
    public static final string CREATE_PAYLOAD = 'createPayLoad';
    public static final string CREATE_PAYLOAD_WITH_IDENTIFIER = 'createPayLoadWithIdentifier';
    public static final string CREATE_OBJECT_FIELD_NODEMAP_MTD = 'createObjectFieldNodeMap';
    public static final string CREATE_OBJECT_RECORDMAP = 'createObjectRecordMap';
    //public static final string CREATE_FIELDS_WRAPPER_MTD = 'createFieldsWrapper';
    public static final string CREATE_LOMOSOFT_BULK_TANKEDIT_MTD = 'createLomosoftBulkTankEdit';
    public static final string CREATE_BULK_CREDIT_CHECK_ORDER_MTD = 'createBulkCreditCheckOrders';
    public static final string  CREATE_CUSTOMER_REQ_MTD = 'createCustomerRequest';
    // map<String,map<String,map<String,String>>>mObjectFieldNode;
    //static map<String,map<String,map<String,set<String>>>>mParentNodeField = new map<String,map<String,map<String,set<String>>>>();
    //static String externalSystem;
    
    //public static map<String,set<Id>>mObjectRecordId = new map<String,set<Id>>();
    //public static map<String,String> mChildParent = new map<String,String>();
    public static final String EP_Transportation_Management = 'EP_Transportation_Management__c';
    
    public static FINAL String ORDERITWM_QUERY_ADD = ' PriceBookEntry.Product2.Name';
    public static FINAL String ORDERITM_WHERECLAUSE = ' AND EP_Is_Taxes__c = FALSE AND EP_Is_Freight_Price__c = FALSE';
    public static FINAL String ORDERIDS_STRING = ' orderids ';
    public static FINAL Integer ORDERITEMNUMBER_LENGTH = 6;
    public static FINAL map<String,String> creditStatuConverMap = new map<String,String>{EP_Common_Constant.Credit_Okay=> EP_Common_Constant.OK,EP_Common_Constant.CREDIT_BLOCK=>EP_Common_Constant.BLOCKED};
    
    public static final string ORDER_ID = 'orderid';
    public static final string EP_ACCOUNT_C= 'EP_Account__c';
    public static final string PARENT_ID = 'ParentId';
    public static final string COMMA = ', ';
    public static final string DUMMY_SHIP_TO_ID = 'Dummy ShipTo Id';
    
    
    public static final string DELETE_STR = 'Delete';
    public static final string FREIGHT_STR = 'Freight';
    public static final string ACTION_STR = 'action';
    public static final string CUSID_TAG = '<custId>';
    public static final string CUSID_END_TAG = '<custId/>';
    public static final string CUSID_START_TAG = '</custId>';
    public static final string PAR_CUS_FLD = 'EP_Parent_Customer_Nr__c';
    public static final string ACC_ID_FLD = 'EP_Account_Id__c';
    public static final string STO_LOC_FLD = 'EP_Storage_Location__r';
    public static final string SHIPTO_ID_TAG = '<shipToId>';
    public static final string SHIPTO_ID_START_TAG = '</shipToId>';
    public static final string NAV_STOCK_FLD = 'EP_Nav_Stock_Location_Id__c';
    public static final string PRO_TYPE_STR = 'productType';
    public static final string ORDER_SHIPID_STR = 'order_shipToId';
    public static Boolean isDeleteFor3rdPartyOrder = false; //defect #29683
    
    public static final string STR_QUERY1 = ',EP_Exception_Number__c,EP_Freight_Only__c,EP_Last_Modified_By__c,Account.EP_Is_Dummy__c';
    public static final string STR_QUERY2 = ' AND EP_Is_Standard__c = True ';
    public static final string ORDER_ID_1 = ', orderid ';
    public static final string SELECT_QUERY= ' Select ';
    public static final string ORDER_WHERE_COND = 'id in : orderIds';
    public static final string STR_QUERY4 = ',EP_WinDMS_Line_Item_Reference_Number__c,Order.EP_Order_Category__c,Order.EP_Ship_To_ID__c, ( Select ';
    public static final string STR_QUERY5 = ' From Order_Products__r  order by OrderItemNumber ASC ) from OrderItem where OrderId  in : orderIds and EP_Parent_Order_Line_Item__c = NULL';
    public static final string STR_QUERY6 = ',EP_Sync_with_NAV__c ,EP_Last_Modified_By__c,Account.EP_Is_Dummy__c';
    /* DefectFix CCO Starts */
    public static final string CONSUMPTION_ORDER = 'Consumption Order';
    public static final string SHIP_TO_ID = 'EP_Ship_To_ID__c';
    public static final string ORDERITEM_ORDER_CATEGORY = 'Order.EP_Order_Category__c';
    public static final string PRICING_STHL_LOC_ID = 'pricingStockHldngLocId';
    public static final string STHL_LOC_ID = 'logisticStockHldngLocId';
    public static final string ORDER_CATEGORY  = 'EP_Order_Category__c';
    
    public static final string ACCOUNT_IS_DUMMY = 'Account.EP_Is_Dummy__c';
    public static final string LAST_MODIFIED_BY = 'EP_Last_Modified_By__c';
    public static final string SYNC_WITH_NAV = 'EP_Sync_with_NAV__c';
    public static final string ORDER = 'Order';
}