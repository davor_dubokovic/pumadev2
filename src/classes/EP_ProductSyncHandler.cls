/**
* @author <Jai Singh>
* @name <EP_ProductSyncHandler>
* @createDate <22/04/2016>
* @description <This apex class is to handle JSON body sent to EP_ProductSyncNAVtoSFDC REST RESOURCE>
* @version <1.0>
*/
public with sharing class EP_ProductSyncHandler{
    
    private static String responseBody = EP_Common_Constant.BLANK;
    
    private static final String PRODUCT_SYNC = 'PRODUCT SYNC FROM NAV TO SFDC';
    private static final String DUPLICATE_ITEMS = 'Duplicate itemId and clientId combination in current syncing items.';
    private static final String ITEMID_BLANK = 'itemId is blank,';
    private static final String CLIENTID_BLANK = 'clientId is blank,';
    private static final String ITEMSOLDAS_BLANK = 'itemSoldAs is blank,';
    @testVisible private static final String NAME_BLANK = 'name is blank,';
    @testVisible private static final String UOM_BLANK= 'uom is blank,';
    @testVisible private static final String CATEGORY_BLANK = 'category is blank,';
    @testVisible private static final String COMPANY_NOT_EXISTS = 'company does not exist in salesforce,';

    /**
    * @author <Jai Singh>
    * @description <This method is used to parse request JSON, create or update product, initiate and send acknowledgement back to requester service>
    * @name <createUpdateProduct>
    * @date <22/04/2016>
    * @param String
    * @return String
    */
    public static String createUpdateProduct(String requestBody){
        List<WrapperProduct> listWrapperProducts = new List<WrapperProduct>();
        Map<String,String> mapProductSeqIDErrorMessages = new Map<String,String>();
        Map<String,String> mapProductSeqIDStatusCodes = new Map<String,String>();
        Map<String,String> mapProductSeqIDs = new Map<String,String>();
        EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon = new EP_AcknowledmentGenerator.cls_HeaderCommon();

        EP_DebugLogger.printDebug(requestBody);

        //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        //1: Get Header Details from the JSON String
        Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(requestBody);
        
        //2: Check for Idempotency - Check the message Id in integration Record if already exists then return old Response
        if(EP_IntegrationService.isIdempotent(mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID))) {
            return EP_IntegrationService.processResponse;
        }
        //3: Log Request - Log the JSON String and other details from Header into the integration record for inbound Request        
        EP_IntegrationRecord__c integrationRecord = EP_IntegrationService.logInboundRequest('PRODUCT_SYNC',mapOfHeaderCommonDetails,requestBody);
        //Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)


        try{
            
            //check if the request body is not null
            if(String.isNotBlank(requestBody)){
                JSONParser parser = JSON.createParser( requestBody );

                parser.nextToken();
                while(parser.hasCurrentToken()){
                    if(parser.getCurrentToken() == JSONTOKEN.FIELD_NAME){
                        //To get header from Request
                        if(EP_Common_Constant.HEADER_COMMON.equalsIgnoreCase(parser.getCurrentName())){
                            parser.nextToken();
                            headerCommon = (EP_AcknowledmentGenerator.cls_HeaderCommon)parser.readValueAs(EP_AcknowledmentGenerator.cls_HeaderCommon.class);
                            parser.skipChildren(); 
                        }
                    }
                    //look for items object
                    if(EP_Common_Constant.ITEMS_NODE.equalsIgnoreCase(parser.getCurrentName()) && parser.getCurrentToken() == JSONToken.START_OBJECT){
                        //parse items items object into WrapperProducts
                        WrapperProducts obj =  (WrapperProducts)parser.readValueAs(WrapperProducts.class);  
                        listWrapperProducts.addAll(obj.item); 
                        //come out of the while loop
                        break;
                    }
                    else{
                        //go to next token in JSON
                        parser.nextToken();
                    }
                }

                Set<String> setCompanyCodes = new Set<String>(); //Set for company codes
                Set<String> setProductCodes = new Set<String>(); //Set for product codes

                //map of product code and company code to check if in current request any duplicate combination is not sent
                Map<String,Boolean> mapProductCompanyCombinations = new Map<String,Boolean>();

                //iterate over sent items and assign values to Set and map
                for(WrapperProduct wrapperProductObject : listWrapperProducts){
                    WrapperIdentifier identifier = wrapperProductObject.identifier;
                    setProductCodes.add(identifier.itemId);
                    setCompanyCodes.add(identifier.clientId);
                    String key = identifier.itemId + EP_Common_Constant.STRING_HYPHEN + identifier.clientId;
                    if(mapProductCompanyCombinations.ContainsKey(key)){
                        mapProductCompanyCombinations.put(key, true);
                    }
                    else{
                        mapProductCompanyCombinations.put(key, false);
                    }
                }

                //map for Exting companies key as company code
                Map<String, Company__c> mapExistingCompanyCodeExistingCompanies = new Map<String, Company__c>();

                //query existing companies by comparing recevied company codes and prepare the mapExistingCompanyCodeExistingCompanies map
                Integer queryRowLimit = EP_Common_Util.getQueryLimit();
                for(Company__c existingCompany : [Select Id, EP_Company_Code__c from Company__c where EP_Company_Code__c IN: setCompanyCodes LIMIT: queryRowLimit]){
                    mapExistingCompanyCodeExistingCompanies.put( existingCompany.EP_Company_Code__c, existingCompany );
                }

                //map for Exting companies key as company code
                Map<String, Product2> mapExistingProductCompanyKeyExistingProduct2s = new Map<String, Product2>();

                //query existing products by comparing recevied product code and company code combinations and prepare the mapExistingProductCompanyKeyExistingProduct2s map
                for( Product2 existingProduct2 : [Select Id, ProductCode, EP_Company__c, EP_NAV_Product_Company__c from Product2 
                where EP_NAV_Product_Company__c IN: mapProductCompanyCombinations.keyset() LIMIT: queryRowLimit]){
                    String uniqueKey = existingProduct2.ProductCode + EP_Common_Constant.STRING_HYPHEN + existingProduct2.EP_Company__c;
                    mapExistingProductCompanyKeyExistingProduct2s.put( uniqueKey, existingProduct2 );
                }

                //product2 object list on which upsert DML will be performed
                List<Product2> listProduct2s = new List<Product2>();

                //WrapperProduct class's object list which will be used to prepare maps used in Acknowledgement preparation
                List<WrapperProduct> listProductsforDML = new List<WrapperProduct>();

                //iteate over received products/items
                for(WrapperProduct wrapperProductObject : listWrapperProducts){
                    //instance of identifier wrapper
                    WrapperIdentifier identifier = wrapperProductObject.identifier;
                    //unique key String
                    String uniqueKey = identifier.itemId + EP_Common_Constant.STRING_HYPHEN + identifier.clientId;
                    String wrapperProductErrorMessage = EP_Common_Constant.BLANK;
                    String wrapperProductSeqId = wrapperProductObject.seqId;

                    if(mapProductCompanyCombinations != null && mapProductCompanyCombinations.ContainsKey(uniqueKey)){
                        //check if duplicate product code and company code combination received in current request
                        if( mapProductCompanyCombinations.get( uniqueKey ) )
                        wrapperProductErrorMessage += DUPLICATE_ITEMS;
                    }
                    //check if required fields are missing
                    if( String.isBlank( identifier.itemId ) ){
                        wrapperProductErrorMessage += ITEMID_BLANK;
                    }

                    if( String.isBlank( identifier.clientId ) ){
                        wrapperProductErrorMessage += CLIENTID_BLANK;
                    }

                    if( String.isBlank( wrapperProductObject.name ) ){
                        wrapperProductErrorMessage += NAME_BLANK;
                    }

                    if( String.isBlank( wrapperProductObject.uom ) ){
                        wrapperProductErrorMessage += UOM_BLANK;
                    }

                    if( String.isBlank( wrapperProductObject.category ) ){
                        wrapperProductErrorMessage += CATEGORY_BLANK;
                    }

                    if( String.isBlank( wrapperProductObject.itemSoldAs ) ){
                        wrapperProductErrorMessage += ITEMSOLDAS_BLANK;
                    }
                    
                    //check if company code received exists in SFDC 
                    if( mapExistingCompanyCodeExistingCompanies != null && !mapExistingCompanyCodeExistingCompanies.ContainsKey( identifier.clientId ) ){
                        wrapperProductErrorMessage += COMPANY_NOT_EXISTS;
                    }

                    //check if Error in current product if not add to DML list else add to Error Map
                    if( String.isBlank( wrapperProductErrorMessage ) ){
                        listProductsforDML.add( wrapperProductObject );
                        //L4 - #45514,#45515 Code change start
                        Product2 product = getProduct(mapExistingProductCompanyKeyExistingProduct2s,uniqueKey,mapExistingCompanyCodeExistingCompanies, identifier);
                        setProductAttributes(wrapperProductObject , product, identifier);
                        listProduct2s.add(product);
                        //L4 - #45514,#45515 Code change end
                    }
                    else{
                        wrapperProductErrorMessage = wrapperProductErrorMessage.removeEnd(EP_Common_Constant.COMMA_STRING.trim());
                        mapProductSeqIDErrorMessages.put( wrapperProductSeqId, wrapperProductErrorMessage);
                    }
                }

                //check if product2 list have any value if so then perform DML
                if( !listProduct2s.isEmpty() ){
                    List<Database.upsertResult> listUpsertResults = database.upsert(listProduct2s,false);

                    //iterate over the upsert result and prepare success map and Error Map
                    for( integer index = 0 ; index < listUpsertResults.size() ; index++ ){
                        if( listUpsertResults[index].isSuccess()  ){
                            mapProductSeqIDs.put( listProductsforDML[index].seqId, EP_Common_Constant.BLANK );
                        }
                        else
                        {
                            for( Database.Error dbError : listUpsertResults[index].getErrors() ){
                                mapProductSeqIDErrorMessages.put( listProductsforDML[index].seqId , dbError.getMessage() );
                                mapProductSeqIDStatusCodes.put( listProductsforDML[index].seqId , String.valueOf( dbError.getStatusCode() ) );
                            }
                        }
                    }
                }

                //initiate acknowledgement creation
                sendAcknowledgement( mapProductSeqIDs, mapProductSeqIDErrorMessages, mapProductSeqIDStatusCodes, listWrapperProducts, headerCommon, null );
                EP_DebugLogger.printDebug(responseBody);
            }       
        }
        catch( Exception ex ){
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), 
            EP_Common_constant.EPUMA, PRODUCT_SYNC, EP_ProductSyncHandler.Class.getName(), 
            EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, 
            EP_Common_Constant.BLANK);
            String exceptionMessage = ex.getMessage();
            //initiate acknowledgement creation
            sendAcknowledgement( mapProductSeqIDs, mapProductSeqIDErrorMessages, mapProductSeqIDStatusCodes, listWrapperProducts, headerCommon, exceptionMessage );
            EP_DebugLogger.printDebug(' ------ responseBody ------ ' + responseBody); 
        }

        //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        //4: Log the Proccessing Response
        EP_IntegrationService.logProcessResponse(integrationRecord, responseBody);
        //Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        return responseBody;
    }

    /**
    * @author <Jai Singh>
    * @description <This method is used to parse request JSON, create or update product, initiate and send acknowledgement back to requester service>
    * @name <sendAcknowledgement>
    * @date <22/04/2016>
    * @param Map<String, String>, Map<String, String>, Map<String, String>, List<WrapperProduct>, EP_AcknowledmentGenerator.cls_HeaderCommon, String
    * @return void
    */
    private static void sendAcknowledgement(Map<String, String> mapProductSeqIDs, Map<String, String> mapProductSeqIDErrorMessages,
    Map<String, String> mapProductSeqIDStatusCodes, List<WrapperProduct> listWrapperProducts, EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon, 
    String exceptionMessage){
        
        //list to create acknoledgement data
        List<EP_AcknowledmentGenerator.cls_dataset> listDataSets = new List<EP_AcknowledmentGenerator.cls_dataset>();
        //map to create response JSON header 
        Map<String, String> mapSendHeaderCommon = new Map<String, String>();
        
        //create message generate name to prepare messageid
        EP_Message_Id_Generator__c objectMsgIdGenerator = new EP_Message_Id_Generator__c();
        insert objectMsgIdGenerator;
        String strMsgGeneratorName = [Select Name from EP_Message_Id_Generator__c where Id =: objectMsgIdGenerator.Id Limit 1].Name;
        String strMsgID = EP_IntegrationUtil.getMessageId(EP_Common_Constant.strSource,
                                                            EP_Common_Constant.strLocation,
                                                            EP_Common_Constant.STRPRODUCT,
                                                            System.NOW(),
                                                            strMsgGeneratorName); 

        mapSendHeaderCommon.put(EP_Common_Constant.MSG_ID,strMsgID);

        //if there was any Error in JSON parsing add PROCESS_STATUS and ERROR_DESCRIPTION in Header of response JSON
        if( String.isNotBlank( exceptionMessage )){
            mapSendHeaderCommon.put(EP_Common_Constant.ERROR_DESCRIPTION, exceptionMessage );
            mapSendHeaderCommon.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.FAILURE );
        }
        else if( !mapProductSeqIDErrorMessages.isEmpty() ){
            mapSendHeaderCommon.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.FAILURE );
            mapSendHeaderCommon.put(EP_Common_Constant.ERROR_DESCRIPTION,headerCommon.ErrorDescription);
        }
        else if( !mapProductSeqIDs.isEmpty() ){
            mapSendHeaderCommon.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.SUCCESS );
            mapSendHeaderCommon.put(EP_Common_Constant.ERROR_DESCRIPTION,headerCommon.ErrorDescription);
        }

        mapSendHeaderCommon.put(EP_Common_Constant.CORR_ID,headerCommon.msgId);
        mapSendHeaderCommon.put(EP_Common_Constant.INTERFACE_TYPE,headerCommon.InterfaceType);
        mapSendHeaderCommon.put(EP_Common_Constant.SOURCE_GROUP_COMPANY,headerCommon.SourceGroupCompany);
        mapSendHeaderCommon.put(EP_Common_Constant.DESTINATION_GROUP_COMPANY,headerCommon.DestinationGroupCompany);
        mapSendHeaderCommon.put(EP_Common_Constant.SOURCE_COMPANY,headerCommon.SourceCompany);
        mapSendHeaderCommon.put(EP_Common_Constant.DESTINATION_COMPANY,headerCommon.DestinationCompany);
        mapSendHeaderCommon.put(EP_Common_Constant.DESTINATION_ADDRESS,headerCommon.DestinationAddress);
        mapSendHeaderCommon.put(EP_Common_Constant.SOURCE_RESP_ADD,headerCommon.SourceResponseAddress);
        mapSendHeaderCommon.put(EP_Common_Constant.SOURCE_UPDATE_STTS_ADD,headerCommon.SourceUpdateStatusAddress);
        mapSendHeaderCommon.put(EP_Common_Constant.DESTINATION_UPDATE_STATUS_ADDRESS,headerCommon.DestinationUpdateStatusAddress);
        mapSendHeaderCommon.put(EP_Common_Constant.MIDDLEWARE_URL_FOR_PUSH,headerCommon.MiddlewareUrlForPush);
        mapSendHeaderCommon.put(EP_Common_Constant.EMAIL_NOTIFICATION,headerCommon.EmailNotification);
        mapSendHeaderCommon.put(EP_Common_Constant.ACK_ERROR_CODE,headerCommon.ErrorCode);
        mapSendHeaderCommon.put(EP_Common_Constant.PROCESSING_ERROR_DESCRIPTION,headerCommon.ProcessingErrorDescription);
        mapSendHeaderCommon.put(EP_Common_Constant.CONT_ON_ERR_STTS,headerCommon.ContinueOnError);
        mapSendHeaderCommon.put(EP_Common_Constant.COMP_LGNG,headerCommon.ComprehensiveLogging);
        mapSendHeaderCommon.put(EP_Common_Constant.TRANSPORT_STATUS,headerCommon.TransportStatus);
        mapSendHeaderCommon.put(EP_Common_Constant.UPDT_SRC_ON_RCV,headerCommon.UpdateSourceOnReceive);
        mapSendHeaderCommon.put(EP_Common_Constant.UPDT_SRC_ON_DLVRY,headerCommon.UpdateSourceOnDelivery);
        mapSendHeaderCommon.put(EP_Common_Constant.UPDT_SRC_AFTER_PRCSNG,headerCommon.UpdateSourceAfterProcessing);
        mapSendHeaderCommon.put(EP_Common_Constant.UPDT_DEST_ON_DLVRY,headerCommon.UpdateDestinationOnDelivery);
        mapSendHeaderCommon.put(EP_Common_Constant.CALL_DEST_FOR_PRCSNG,headerCommon.CallDestinationForProcessing);
        mapSendHeaderCommon.put(EP_Common_Constant.OBJECT_TYPE,headerCommon.ObjectType);
        mapSendHeaderCommon.put(EP_Common_Constant.OBJECT_NAME,headerCommon.ObjectName);
        mapSendHeaderCommon.put(EP_Common_Constant.COMMUNICATION_TYPE,headerCommon.CommunicationType);
        mapSendHeaderCommon.put(EP_Common_Constant.OBJECT_NAME,headerCommon.ObjectName);

        //prepare acknowledgement data
        for(WrapperProduct wrapperProductObject : listWrapperProducts ){  
            EP_AcknowledmentGenerator.cls_dataset objectDataSet = new EP_AcknowledmentGenerator.cls_dataset();
            objectDataSet.name = EP_Common_Constant.STRITEM;
            objectDataSet.seqId = wrapperProductObject.seqId;
            if( mapProductSeqIDs != null && mapProductSeqIDs.containsKey( wrapperProductObject.seqId ) ){
                objectDataSet.errorCode = EP_Common_Constant.BLANK;
                objectDataSet.errorDescription = EP_Common_Constant.BLANK;
            }
            else if( mapProductSeqIDErrorMessages != null && mapProductSeqIDErrorMessages.containsKey( wrapperProductObject.seqId ) ){
                objectDataSet.errorCode = mapProductSeqIDStatusCodes != null && mapProductSeqIDStatusCodes.containsKey( wrapperProductObject.seqId ) ? mapProductSeqIDStatusCodes.get( wrapperProductObject.seqId ) : EP_Common_Constant.BLANK;
                objectDataSet.errorDescription = mapProductSeqIDErrorMessages.get( wrapperProductObject.seqId );
            }
            listDataSets.add( objectDataSet );
        }
        //send header and acknowledgement data to convert into JSON String
        responseBody = EP_AcknowledmentGenerator.createacknowledgement( mapSendHeaderCommon , listDataSets, null );
    }

    /**
    * @author <Jai Singh>
    * @name <WrapperIdentifier>
    * @createDate <22/04/2016>
    * @description <Wrapper class for identifier>
    * @version <1.0>
    */
    public with sharing class WrapperIdentifier{
        public String itemId;
        public String clientId;
    }

    /**
    * @author <Jai Singh>
    * @name <WrapperProduct>
    * @createDate <22/04/2016>
    * @description <Wrapper class for product>
    * @version <1.0>
    */
    public with sharing class WrapperProduct{
        public String seqId;
        public WrapperIdentifier identifier;    
        public String name;
        //L4 - #45514,#45515 Code change start
        // public String name2;
        public String uom;
        public String category;
        public String itemSoldAs;
        // public String dgClassification;
        public String description;
        public String block;
        public String enterpriseId;
        public String isClean;
		/*78436 start*/
        public String globalUomFctr;
		/*78436 end*/
        //L4 - #45514,#45515 Code change end
    }
    
    /**
    * @author <Jai Singh>
    * @name <WrapperProducts>
    * @createDate <22/04/2016>
    * @description <top level wrapper (items)>
    * @version <1.0>
    */
    public with sharing class WrapperProducts{
        public List<WrapperProduct> item;
    }
    
   /**
    * @author       Accenture
    * @name         isProductUpdate
    * @date         12/18/2017
    * @description  This method check if Product is for update or create
    * @param        Map<String, Product2>, String
    * @return       boolean
    */
    //L4 - #45514,#45515 Code change start
    @testVisible
    private static boolean isProductUpdate(Map<String, Product2> mapExistingProductCompanyKeyExistingProduct2s, string uniqueKey) {
    	EP_GeneralUtility.Log('private','EP_ProductSyncHandler','isProductUpdate');
    	Boolean isProductUpdate = false;
		if( mapExistingProductCompanyKeyExistingProduct2s != null && mapExistingProductCompanyKeyExistingProduct2s.ContainsKey( uniqueKey ) ){
		    isProductUpdate = true;
		}
		return isProductUpdate;
    }
    
   /**
    * @author       Accenture
    * @name         getProduct
    * @date         12/18/2017
    * @description  This method will Return the Instance of Product Object based on Product Attributes 
    * @param        Map<String, Product2>, string, Map<String, Company__c>, WrapperIdentifier
    * @return       Product2
    */
    @testVisible
    private static Product2 getProduct(Map<String, Product2> mapExistingProductCompanyKeyExistingProduct2s, string uniqueKey, Map<String, Company__c> mapExistingCompanyCodeExistingCompanies , WrapperIdentifier identifier) {
        EP_GeneralUtility.Log('private','EP_ProductSyncHandler','getProduct');
        Product2 product = new Product2();
        if(isProductUpdate(mapExistingProductCompanyKeyExistingProduct2s,uniqueKey)){
            product.Id  =  mapExistingProductCompanyKeyExistingProduct2s.get( uniqueKey ).Id;
        }
        else{
            product.EP_NAV_Product_Company__c = uniqueKey;
            if(mapExistingCompanyCodeExistingCompanies.ContainsKey( identifier.clientId)) {
            	product.EP_Company_Lookup__c = mapExistingCompanyCodeExistingCompanies.get( identifier.clientId).Id;
            }
        }
        return product;
    }
    
    /**
    * @author       Accenture
    * @name         setProductAttributes
    * @date         12/18/2017
    * @description  This method will be use to set the product attributes from the JSON String
    * @param        Map<String, Product2>, string, Map<String, Company__c>, WrapperIdentifier
    * @return       NA
    */
    @testVisible
    private static void setProductAttributes (WrapperProduct wrapperProductObject, Product2 product, WrapperIdentifier identifier) {
     	EP_GeneralUtility.Log('private','EP_ProductSyncHandler','setProductAttributes');
    	product.Name = wrapperProductObject.name;
        product.ProductCode = identifier.itemId;
        product.Family = wrapperProductObject.category;
        product.Description = wrapperProductObject.description;
        product.EP_Unit_of_Measure__c = wrapperProductObject.uom;
        product.EP_Product_Sold_As__c = wrapperProductObject.itemSoldAs;
        product.isActive = EP_GeneralUtility.stringToBoolenConversion(wrapperProductObject.block) ? false : true ;  
        product.EP_Enterprise_ID__c = decimal.valueOf(wrapperProductObject.enterpriseId);
        product.EP_Dirty_Product__c = EP_GeneralUtility.stringToBoolenConversion(wrapperProductObject.isClean) ? false : true;
        product.EP_Seq_ID__c = wrapperProductObject.seqId;
		/*78436 start*/
		product.EP_Global_UoM_Factor__c = String.isNotEmpty(wrapperProductObject.globalUomFctr) ? decimal.valueOf(String.valueOf(wrapperProductObject.globalUomFctr).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : 0;
		/*78436 end*/
   }
    //L4 - #45514,#45515 Code change End     
}