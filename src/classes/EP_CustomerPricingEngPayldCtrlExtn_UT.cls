@isTest
public class EP_CustomerPricingEngPayldCtrlExtn_UT
{

	static testMethod void checkPageAccess_positivetest() {
		Account acc = EP_TestDataUtility.getSellToPositiveScenario();
		
		PageReference pageRef = Page.EP_ShipToPricingEngineSyncWithNAVXML;
	    Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE, '1234');
		ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, acc.Id);
			
		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		EP_CustomerPricingEnginePayloadCtrlExtn localObj = new EP_CustomerPricingEnginePayloadCtrlExtn(sc);
		Test.startTest();
		PageReference result = localObj.checkPageAccess();
		Integer deferUpperLimit = localObj.deferUpperLimit;
		Test.stopTest();
		System.AssertEquals(true,result == NULL);
	}
	
	static testMethod void checkPageAccess_negativetest() {
		Account acc = EP_TestDataUtility.getSellToPositiveScenario();
		 
		PageReference pageRef = Page.EP_ShipToPricingEngineSyncWithNAVXML;
	    Test.setCurrentPage(pageRef);
	    ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, acc.Id);			
			
		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		EP_CustomerPricingEnginePayloadCtrlExtn localObj = new EP_CustomerPricingEnginePayloadCtrlExtn(sc);
		Test.startTest();
		PageReference result = localObj.checkPageAccess();
		Test.stopTest();
		System.AssertEquals(true,result != NULL);
		System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
	}
}