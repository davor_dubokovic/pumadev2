/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToActiveToActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 05-Active to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToActiveToActive extends EP_AccountStateTransition {

    public EP_ASTStorageShipToActiveToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToActive',' isGuardCondition');        
        return true;
    }
}