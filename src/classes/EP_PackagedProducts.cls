/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_PackagedProducts
  * @CreateDate  : 16/02/2017
  * @Description : This is Class Contains logic Packaged Order
  * @Version     : <1.0>
  * @reference   : N/A
  */
public with sharing class EP_PackagedProducts extends EP_ProductSoldAs {

    public EP_PackagedProducts() {
        
    }
    
    
    /** This method is returns Accounts for Packaged Order
     *  @date      16/02/2017
     *  @name      getShipTos
     *  @param     List<Account> lstAccount
     *  @return    List<Account>
     *  @throws    NA
     */
public override List<Account> getShipTos(List<Account> lstAccount) {
      EP_GeneralUtility.Log('Public','EP_PackagedProducts','getShipTos');
        
        List<Account> lstShipTos = new List<Account>();
        
        for (Account objAccount : lstAccount) {

            if (objAccount.RecordType.DeveloperName == EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME) {

                lstShipTos.add(objAccount);
            }
        }    
        
        return lstShipTos;
    }
}