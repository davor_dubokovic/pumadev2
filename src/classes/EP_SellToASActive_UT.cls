@isTest
public class EP_SellToASActive_UT{
    static final string EVENT_NAME = '05-ActiveTo05-Active';
    static final string EVENT_ACCOUNT_SET= '04-AccountSet-upTo05-Active';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void setAccountDomainObject_test() {
        EP_SellToASActive localObj = new EP_SellToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    //Delegates to other methods. Adding dummy assert  

    // 45361 and 45362 start
    static testMethod void doOnEntry_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_SellToASActive localObj = new EP_SellToASActive();
        Account newAccount = EP_TestDataUtility.getSellTo();
        newAccount.EP_SENT_TO_NAV_WINDMS__c = true;
        newAccount.EP_Delivery_Type__c = EP_COMMON_CONSTANT.DELIVERY;
        update newAccount;
        
        Account acc = EP_TestDataUtility.getShipTo();
        acc.parentID = newAccount.id;
        acc.EP_SENT_TO_NAV_WINDMS__c = true;
        update acc;

        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        EP_AccountDomainObject domainObj = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(domainObj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //Delegates to other methods. Adding dummy assert  
        system.assert(true);
    }

    static testMethod void doOnEntryUnblocked_test(){
        EP_SellToASActive localObj = new EP_SellToASActive();
        Account newAccount = EP_TestDataUtility.getSellTo();
        newAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BLOCKED;
        EP_AccountDomainObject domainObj = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(domainObj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //Delegates to other methods. Adding dummy assert  
        system.assert(true);
    }
    // 45361 and 45362 end

    //Delegates to other methods. Adding dummy assert   
    static testMethod void doOnEntry_AccountSetupToActive_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_SellToASActive localObj = new EP_SellToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObject();
        Account oldAccount = obj.localaccount.clone(false, false, false, false);
        oldAccount.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        obj.oldAccount = oldAccount;
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_ACCOUNT_SET);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
    }   
    //Method has no implementation, hence adding dummy assert  
    static testMethod void doOnExit_test() {
        EP_SellToASActive localObj = new EP_SellToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest(); 
        System.assertEquals(true, true);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_SellToASActive localObj = new EP_SellToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_SellToASActive localObj = new EP_SellToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        String message;
        Test.startTest();       
        try{
            Boolean result = localObj.doTransition();
        }catch(Exception e){
            message = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(message));
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_SellToASActive localObj = new EP_SellToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    // Changes made for CUSTOMER MODIFICATION L4 Start
    //Pricing Callout Commented as it will be part of WP2
    static testMethod void doOnEntry_EP_Synced_PE_test(){
        EP_SellToASActive localObj = new EP_SellToASActive();
        Account acc = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assert(!acc.EP_Synced_PE__c);
    }

    static testMethod void doOnEntry_EP_Synced_NAV_test(){
        EP_SellToASActive localObj = new EP_SellToASActive();
        Account acc = EP_TestDataUtility.getSellTo();
        acc.EP_Synced_PE__c = true;
        update acc;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assert(!acc.EP_Synced_NAV__c);
    }

    static testMethod void doOnEntry_EP_Synced_WinDMS_test(){
        EP_SellToASActive localObj = new EP_SellToASActive();
        Account acc = EP_TestDataUtility.getSellTo();
        acc.EP_Synced_PE__c= true;
        acc.EP_Synced_NAV__c = true;
        update acc;
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assert(!acc.EP_Synced_WinDMS__C);
    }
// Changes made for CUSTOMER MODIFICATION L4 End
 
 // 45361 and 45362 start   
    static testMethod void doOnEntry_isShipToActive_test(){
        EP_SellToASActive localObj = new EP_SellToASActive();
        Account acc = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject obj = new EP_AccountDomainObject(acc.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assert(!acc.EP_Synced_WinDMS__C);
    }
// 45361 and 45362 end
    /** 
        This isInboundTransitionPossible() method is returning only true for all cases.
        So only positive scenario applicable. 
    **/
}