/*   
     @Author Aravindhan Ramalingam
     @name <EP_RetroNonVMINonConsignmentSM.cls>     
     @Description <Retro Non VMI Non Consignment Statemachine >   
     @Version <1.1> 
*/

public class EP_RetroNonVMINonConsignmentSM extends EP_OrderStateMachine {
    
    public EP_RetroNonVMINonConsignmentSM(){

    }
    
public override EP_OrderState getOrderState(EP_OrderEvent currentEvent){
		EP_GeneralUtility.Log('Public','EP_RetroNonVMINonConsignmentSM','getOrderState');
        return super.getOrderState(currentEvent);
    }
}