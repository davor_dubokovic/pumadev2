@isTest
public class EP_OrderStateInvoiced_UT
{

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_OrderStateInvoiced.getTextValue();
        Test.stopTest();    
        System.AssertEquals(EP_OrderConstant.OrderState_Invoiced, result);
    }
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnEntry_test() {
        EP_OrderStateInvoiced localObj = new EP_OrderStateInvoiced();
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true); 
    }
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnExit_test() {
        EP_OrderStateInvoiced localObj = new EP_OrderStateInvoiced();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInvoicedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assertEquals(true, true); 
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_OrderStateInvoiced localObj = new EP_OrderStateInvoiced();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateLoadedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void doTransition_PositiveScenariotest() {
        EP_OrderStateInvoiced localObj = new EP_OrderStateInvoiced();         
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateAcceptedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        csord__Order__c ord = obj.getOrder();
        ord.Status__c =  EP_Common_Constant.INVOICED_STATUS;
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_OrderStateInvoiced localObj = new EP_OrderStateInvoiced();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInvoicedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        List<Exception> excList = new List<Exception>();
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }
        Catch(Exception ex){
            excList.add(ex);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    static testMethod void setOrderContext_test() {
        EP_OrderStateInvoiced localObj = new EP_OrderStateInvoiced();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInvoicedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.setOrderContext(obj,oe);
        Test.stopTest();
        System.AssertEquals(true,localObj.order != null);
    }
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateInvoiced localObj = new EP_OrderStateInvoiced();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateInvoicedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);         
        Test.startTest();
        localObj.setOrderDomainObject(obj);
        Test.stopTest();
        System.assertEquals(true, localObj.order == obj);
    }
 }