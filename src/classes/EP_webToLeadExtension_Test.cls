/* 
  @Author <Brajesh Tiwary>
  @name <EP_webToLeadExtension_Test>
  @CreateDate <05/10/2015>
  @Description <This is the test class for Web To Lead Page >
  @Version <1.0>
 
*/
@isTest
private class EP_webToLeadExtension_Test {
    
    private static final String EMAIL = 'sample@test.com';
    private static final String COMPANY = 'Test Company';
    private static final String TITLE = 'Test Title';
    private static final String STATE = 'Test state';
    private static final String CITY = 'Test city';
    private static final String STREET = 'Test street';
    private static final String FAX = 'Test fax';
    private static final String SERVICES_INTERESTED_IN = 'TBD';
    private static final String COMPANY_TAX_NUMBER = 'TestN';
    private static final String IBAN = 'Test IBAN';
    private static final String BANK = 'Test Name';
    private static final String PREFERRED_MODE_OF_COMM = 'Email';
    private static final String PHONE = '987654321';
    private static final String FUEL = 'Fuel';
    private static final string VERIFY_LEAD_MTD = 'verifyTestLeadData';
    private static final string CLASSNAME = 'EP_webToLeadExtension_Test'; 
    
/*
        This method will test for successful insertion of Lead and case
        User Story- US_001
        Test Script- 5
    */  
    static testMethod void testEmailToLeadSucess() 
    {
        try{
            test.setcurrentpage(page.EP_ProspectCustomerWebApplicationPage);
            Integer i;
            list<EP_Country__c>lCountries = new list<EP_Country__c>();
            list<EP_Region__c>lRegions = new list<EP_Region__c>();
            for(i = 0; i < 5; i++){
                lCountries.add(EP_TestDataUtility.createCntry('Country'+ i
                                                                ,true
                                                                ,'cntry'+ i
                                                                ,'USD'
                                                                ,''));
            }
            insert lCountries;
            for(i = 0; i < 10; i++){
                lRegions.add(EP_TestDataUtility.createRegion('region' + i
                                                              ,i >= 5 ? lCountries[i-5].id:lCountries[i].id));
            }
            insert lRegions;
            Profile sysAdmin = [Select id from Profile
                                    Where Name = 'System Administrator' 
                                    limit :EP_Common_Constant.ONE];
            User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
            EP_webToLeadExtension handler=EP_TestDataUtility.EP_createTestLead();
            Test.startTest();
            System.runAs(adminUser){
                
                handler.createCountryPicklist();
                handler.country = lCountries[0].id;
                handler.region = lRegions[0].id;
                handler.mapCrncyCountry();
                handler.saveLead();  
            }
            Test.stopTest();              
            System.assertNotEquals(null,handler.caseNumber); // This will check created case id is assigned to lead    
            List<Lead> listLead=[Select id from Lead limit :EP_Common_Constant.ONE];
            System.assertEquals(1,listLead.size());
        }
        catch(Exception ex){
        }
    
    }
        
    /*
        This method will test for successful insertion of Lead and case
        User Story- US_001
        Test Script- 6,7
    */      
    static testMethod void verifyTestLeadData(){
        
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
        Integer i;
        list<EP_Country__c>lCountries = new list<EP_Country__c>();
        list<EP_Region__c>lRegions = new list<EP_Region__c>();
        for(i = 0; i < 5; i++){
            lCountries.add(EP_TestDataUtility.createCntry('Country'+ i
                                                            ,true
                                                            ,'cntry'+ i
                                                            ,'USD'
                                                            ,''));
        }
        insert lCountries;
        for(i = 0; i < 10; i++){
            lRegions.add(EP_TestDataUtility.createRegion('region' + i
                                                          ,i >= 5 ? lCountries[i-5].id:lCountries[i].id));
        }
        insert lRegions;
        System.runAs(adminUser)
        {           
         EP_webToLeadExtension handler = EP_TestDataUtility.EP_createTestLead();  
         try{
            handler.createCountryPicklist();
            handler.country = lCountries[0].id;
            handler.region = lRegions[0].id;
            handler.mapCrncyCountry();
            handler.saveLead(); 
            Test.stopTest(); 
        }catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, VERIFY_LEAD_MTD , CLASSNAME, ApexPages.Severity.ERROR);
            System.assert(handledException.getMessage().contains('FIELD_CUSTOM_Field_EXCEPTION'),handledException.getMessage());
        } 
      }
        List<Lead> createdLeads = [SELECT Id,FirstName,LastName,Email
                                            ,Company,Title, State, City, Street
                                            ,currencyISOCode,EP_CountryLookUp__c
                                            ,EP_CountryLookUp__r.currencyISOCode
                                            ,EP_Delivery_Country__c
                                            ,Fax,Case__c,EP_Services_Interested_In__c
                                            ,EP_Company_Tax_Number__c
                                            ,EP_Requested_Payment_Method__c,EP_Requested_Payment_Terms__c
                                            ,EP_IBAN__c,EP_Bank_Name__c
                                            ,EP_Preferred_Mode_of_Communication__c
                                            ,Phone,EP_Preferred_Time_of_Communication__c
                                            ,EP_Products_Interested_In__c,EP_Indicative_Fuel_Volume__c
                                            ,EP_Indicative_Lubes_Volume__c
                                            ,LeadSource FROM Lead limit :EP_Common_Constant.ONE];
        List<Case> createdCases = [SELECT Id,Owner.Name,Origin,Status,Subject
                                            ,SuppliedEmail,SuppliedName,Priority
                                            ,Description,RecordTypeId 
                                            FROM Case limit :EP_Common_Constant.ONE];
        System.assertEquals(true, createdCases != null); //To check case is created 
        System.assertEquals(true, createdLeads != null);
        System.assertEquals(1, createdLeads.size());
        System.assertEquals(EP_Common_Constant.LASTNAME, createdLeads[0].LastName);
        System.assertEquals(EP_Common_Constant.FIRSTNAME, createdLeads[0].FirstName);
        System.assertEquals(Email, createdLeads[0].EMAIL);
        System.assertEquals(Company, createdLeads[0].COMPANY);
        System.assertEquals(Title, createdLeads[0].TITLE);
        System.assertEquals(State, createdLeads[0].STATE);
        System.assertEquals(City, createdLeads[0].CITY);
        System.assertEquals(STREET, createdLeads[0].STREET);
        System.assertEquals(FAX, createdLeads[0].FAX);
        System.assertEquals(SERVICES_INTERESTED_IN, createdLeads[0].EP_Services_Interested_In__c);
        System.assertEquals(EP_Common_Constant.ONLINE_PREPAYMENT , createdLeads[0].EP_Requested_Payment_Terms__c);
        System.assertEquals(EP_Common_Constant.LEAD_DD_PAYMENT_METHOD, createdLeads[0].EP_Requested_Payment_Method__c);
        System.assertEquals(IBAN, createdLeads[0].EP_IBAN__c);
        System.assertEquals(BANK, createdLeads[0].EP_Bank_Name__c);
        System.assertEquals(PREFERRED_MODE_OF_COMM, createdLeads[0].EP_Preferred_Mode_of_Communication__c);
        System.assertEquals(PHONE, createdLeads[0].Phone );
        System.assertEquals(EP_Common_Constant.MORNING, createdLeads[0].EP_Preferred_Time_of_Communication__c);
        System.assertEquals(FUEL, createdLeads[0].EP_Products_Interested_In__c);
        System.assertEquals(45.00, createdLeads[0].EP_Indicative_Fuel_Volume__c);
        System.assertEquals(32.00, createdLeads[0].EP_Indicative_Lubes_Volume__c);              
        System.assertEquals(EP_Common_Constant.LEAD_ORIGIN_WEB , createdLeads[0].LeadSource);
        System.assertEquals(createdLeads[0].currencyISOCode,createdLeads[0].EP_CountryLookUp__r.currencyISOCode);
        System.assertEquals(createdLeads[0].EP_CountryLookUp__c,createdLeads[0].EP_Delivery_Country__c);
        //handler.saveLead(); 
        
        }
        
     /*
        To check Lead save failure
     */  
    static testMethod void testLeadSaveFailure()
    {
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Test.startTest();
        System.runAs(adminUser)
        {       
                      
        EP_webToLeadExtension handler = EP_TestDataUtility.EP_createTestLead();     
        
         handler.leadObj.FirstName = '';
       handler.leadObj.LastName = '';
       handler.leadObj.Email = '';
       handler.leadObj.Title = '';
       handler.leadObj.Company = '';       
       handler.leadObj.State = '';
       handler.leadObj.City = '';
       handler.leadObj.Street = '';       
       handler.leadObj.EP_Services_Interested_In__c = ''; 
       handler.leadObj.EP_Requested_Payment_Terms__c = '';      
       handler.leadObj.EP_Requested_Payment_Method__c = '';
       handler.leadObj.EP_Preferred_Mode_of_Communication__c = '';
       handler.leadObj.Phone = ''; 
       handler.leadObj.EP_Preferred_Time_of_Communication__c = ''; 
       handler.leadObj.EP_Products_Interested_In__c = '';
       handler.leadObj.EP_Indicative_Total_Pur_Value_Per_Yr__c = '';
       handler.leadObj.EP_Delivery_Type__c = '';
       handler.leadObj.PostalCode = '';
       try{
        handler.saveLead();
       
        system.assertEquals(null,handler.leadObj.id);
        system.assert(String.isBlank(handler.caseNumber));
          }      
        catch(DMLException e){
            EP_LoggingService.logHandledException (e, 'ePuma', 'testLeadSaveFailure', 'EP_webToLeadExtension_Test', ApexPages.Severity.ERROR);
            
            }
         }
        Test.stopTest();
    }
    /*
        To check case number after submition of form
     */  
    static testMethod void testCaseNumber(){
        try{
            Profile cscAgentProfile = [Select id from Profile
                                Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE
                                limit :EP_Common_Constant.ONE];
        
            User CSCAgent = EP_TestDataUtility.createUser(cscAgentProfile.id);
            EP_webToLeadExtension handler = new EP_webToLeadExtension();
            System.runAs(CSCAgent){                 
                handler = EP_TestDataUtility.EP_createTestLead();
                handler.saveLead();
                
                List<Case> caseList = [Select CaseNumber from case limit :EP_Common_Constant.ONE];
                system.debug('===caseList===='+caseList);
                if(!caseList.isEmpty()){
                system.assertEquals(handler.caseNumber, caseList[0].CaseNumber);
                }
            }
        }
        catch(Exception ex){
        }
    }
}