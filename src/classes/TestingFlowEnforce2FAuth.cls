public with sharing class TestingFlowEnforce2FAuth {
	

	@InvocableMethod(label='Enforce High Assurance Session Security' description='Enforce High Assurance Session Security - No check or validation hard coded')
	public static void enforceHighAssuranceSession() {
		System.debug('enforceHighAssuranceSession before: ' + Auth.SessionManagement.getCurrentSession().get('SessionSecurityLevel') + ' ' + Auth.SessionManagement.getCurrentSession());
		Auth.SessionManagement.setSessionLevel(Auth.SessionLevel.HIGH_ASSURANCE);
		//Auth.SessionManagement.setSessionLevel(Auth.SessionLevel.STANDARD);
		System.debug('enforceHighAssuranceSession  after AAA: ' + Auth.SessionManagement.getCurrentSession().get('SessionSecurityLevel') + ' ' + Auth.SessionManagement.getCurrentSession());
	}
}