/**
 *  @Author <Kamal Garg>
 *  @Name <EP_ManageInvoiceDocument>
 *  @CreateDate <04/07/2016>
 *  @Description <This is the apex class used to generate PDF for Invoice Record>
 *  @Version <1.0>
 */
public with sharing class EP_ManageInvoiceDocument {

    private static final string CLASS_NAME = 'EP_ManageInvoiceDocument';
    private static final string GENERATE_AT_METHOD = 'generateAttachmentForInvoice';
    
    /**
     * @author <Pooja Dhiman>
     * @name <generateAttachmentForInvoice>
     * @date <04/07/2016>
     * @description <This method is used to generate document for Customer Invoice>
     * @version <1.0>
     * @param EP_ManageDocument.Documents
     */
    public static void generateAttachmentForInvoice(EP_ManageDocument.Documents documents) {
        List<EP_Invoice__c> invoices = new List<EP_Invoice__c>();
        Map<String, List<Attachment>> invKeyAttachListMap = new Map<String, List<Attachment>>();
        //set containg 'AccountNumber' field values of 'Account' standard object received from Input JSON
        Set<String> acctNumSet = new Set<String>();
        //map containg 'EP_Invoice_Key__c' field value of 'EP_Invoice__c' as key and 'AccountNumber' field value of 'Account' object as value
        Map<String, String> invKeyBillToMap = new Map<String, String>();
        //Map<String, String> oldNewInvoiceKeyMap = new Map<String, String>();
        try{
            List<Attachment> invKeyAttachment = null;
            for(EP_ManageDocument.Document doc : documents.document) {
                EP_ManageDocument.CustomerInvoiceWrapper custInvWrap = EP_DocumentUtil.fillCustomerInvoiceWrapper(doc.documentMetaData.metaDatas);
                EP_Invoice__c inv = generateCustomerInvoice(custInvWrap);
                invoices.add(inv);
                
                String invoiceKey = inv.EP_Invoice_Key__c;
                String compositKey = custInvWrap.issuedFromId + EP_Common_Constant.HYPHEN + custInvWrap.BillTo;
                acctNumSet.add(compositKey);
                invKeyBillToMap.put(invoiceKey, compositKey.toUpperCase());
                
                Attachment att = EP_DocumentUtil.generateAttachment(doc);
                if(!(invKeyAttachListMap.containsKey(invoiceKey))) {
                    invKeyAttachment = new List<Attachment>();
                    invKeyAttachListMap.put(invoiceKey, invKeyAttachment);
                }
                invKeyAttachListMap.get(invoiceKey).add(att);
            }
            
            Map<String, Id> acctNumIdMap = EP_DocumentUtil.getAccountCompositIdMap(acctNumSet, 
            new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
            List<EP_Invoice__c> invoicesToUpdate = new List<EP_Invoice__c>();
            //L4 #45304 Changes Start - Collect all BillTo to get existing invoice records
            set<String> setOfBillTo = new set<String>();
            for(EP_Invoice__c inv : invoices) {
                String invoiceKey = inv.EP_Invoice_Key__c;
                inv.EP_Bill_To__c = acctNumIdMap.get(invKeyBillToMap.get(inv.EP_Invoice_Key__c));
                
                invoicesToUpdate.add(inv);
                setOfBillTo.add(inv.EP_Bill_To__c);
            }
             //L4 #45304 Changes End
            Schema.SObjectField f = EP_Invoice__c.Fields.EP_Invoice_Key__c;
            List<Database.upsertResult> uResults = Database.upsert(invoicesToUpdate, f, false);
            for(Database.upsertResult result : uResults) {
                if(result.isSuccess() && result.isCreated()) {
                    System.debug('Successfully inserted/updated Invoice. Invoice ID: ' + result.getId());
                } else {
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Invoice fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            Map<String, Id> invNrIdMap = new Map<String, Id>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            //L4 #45304 Changes Start - Old Code Fix - Added billTo filter because Can't fire SOQL without any filter on Object
            invoicesToUpdate = [SELECT Id, EP_Invoice_Key__c FROM EP_Invoice__c where EP_Bill_To__c In : setOfBillTo limit: nRows];
            //L4 #45304 Changes End
            for(EP_Invoice__c inv : invoicesToUpdate) {
                invNrIdMap.put(inv.EP_Invoice_Key__c, inv.Id);
            }
            List<Attachment> attachments = new List<Attachment>();
            for(String key : invKeyAttachListMap.keySet()) {
                List<Attachment> attachList = invKeyAttachListMap.get(key);
                for(Attachment att : attachList) {
                    //att.ParentId = invNrIdMap.get(oldNewInvoiceKeyMap.get(key));
                    att.ParentId = invNrIdMap.get(key);
                    attachments.add(att);
                }
            }
            
            EP_DocumentUtil.deleteAttachments(attachments);
            
            Database.SaveResult[] results = Database.insert(attachments, false);
            for (Integer i = 0; i < results.size(); i++) {
                if (results[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted attachment. Attachment ID: ' + results[i].getId());
                    EP_ManageDocument.successRecIdMap.put(attachments[i].Name, results[i].getId());
                } else {
                    // Operation failed, so get all errors
                    for(Database.Error err : results[i].getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Attachment fields that affected this error: ' + err.getFields());
                        EP_ManageDocument.errDesMap.put(attachments[i].Name, err.getMessage());
                        EP_ManageDocument.errCodeMap.put(attachments[i].Name, String.valueOf(err.getStatusCode()));
                    }
                }
            }
        }
        catch(Exception e){
             EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, GENERATE_AT_METHOD , CLASS_NAME,apexPages.severity.ERROR);
             throw e;
        }
    }
    
    /**
     * @author <Kamal Garg>
     * @name <generateCustomerInvoice>
     * @date <18/06/2016>
     * @description <This method is used to generate 'EP_Invoice__c' custom object record>
     * @version <1.0>
     * @param EP_ManageDocument.CustomerInvoiceWrapper
     * @return EP_Invoice__c
     */
    private static EP_Invoice__c generateCustomerInvoice(EP_ManageDocument.CustomerInvoiceWrapper custInvWrap) {
        EP_Invoice__c inv = new EP_Invoice__c();
        inv.Name = custInvWrap.invoiceNr;
        inv.EP_NavSeqId__c = custInvWrap.SeqId;
        /** TR Datetime 59846 Start **/
        inv.EP_Invoice_Issue_Date__c = EP_DateTimeUtility.convertStringToDate(custInvWrap.InvoiceDate);//Date.valueOf(custInvWrap.InvoiceDate);
        inv.EP_Invoice_Due_Date__c = EP_DateTimeUtility.convertStringToDate(custInvWrap.InvoiceDueDate);//Date.valueOf(custInvWrap.InvoiceDueDate);
        /** TR Datetime 59846 End **/
        //L4 #45304 Changes Start - Added entryNr in composit key formation
        inv.EP_Invoice_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{custInvWrap.BillTo, custInvWrap.invoiceNr, custInvWrap.issuedFromId, custInvWrap.entryNr});
        //L4 #45304 Changes End
        return inv;
    }
    
}