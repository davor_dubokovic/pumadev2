/* 
  @Author <Accenture>
   @name <EP_PricingRecordTriggerHandler >
   @CreateDate <06/07/2016>
   @Description <This is helper class of PricingRecord Trigger> 
   @Version <1.0>
*/
public with sharing class EP_PricingRecordTriggerHandler {
    
    private static final string CLASS_NAME = 'EP_PricingRecordTriggerHandler';
    private static final string doBeforeInsert = 'doBeforeInsert';
    private static final string doAfterUpdate= 'doAfterUpdate';
    
     /* 
      @Description <This method is used to send response to LS on after update of trigger> 
    */
    public static void doAfterUpdate(Map<Id,EP_Pricing_Engine__c> oldPricingRecordMap
                                     ,Map<Id,EP_Pricing_Engine__c>newPricingRecordMap){
        try{                             
            EP_PricingRecordTriggerHelper.sendResponseToLS(oldPricingRecordMap,newPricingRecordMap);
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, doAfterUpdate, CLASS_NAME,apexPages.severity.ERROR);
        }
     }   
}