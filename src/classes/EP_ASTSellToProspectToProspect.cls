/*
 *  @Author <Accenture>
 *  @Name <EP_ASTSellToProspectToProspect>
 *  @CreateDate <08/02/2017>
 *  @Description <Handles Account status change from Prospect to Prospect>
 *  @Version <1.0>
 */
 public class EP_ASTSellToProspectToProspect extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTSellToProspectToProspect
    */
    public EP_ASTSellToProspectToProspect() {
        finalState = EP_AccountConstant.PROSPECT;
    }
    /*
     *  @Author <Accenture>
     *  @Name isTransitionPossible
     */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToProspectToProspect','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /*
     *  @Author <Accenture>
     *  @Name isRegisteredForEvent
     */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToProspectToProspect','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /*
     *  @Author <Accenture>
     *  @Name isGuardCondition
     */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToProspectToProspect','isGuardCondition');
        //L4_45352_START
        /*
        EP_AccountService service = new EP_AccountService(this.account);
        system.debug('## 1. Checking if Company .....');
        If(service.isProductListAttached()){
            if(!service.isCompanySameOnProductList()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Company_Mismatch_For_Sell_To_and_ProductList);
                return false;
            }
            system.debug('## 2. Checking if Company on Products is same as that on Sell To.....');
            if(!service.isCompanySameOnProducts()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Productlist_has_Product_with_different_Company);
                return  false;
            }
            system.debug('## 3. Checking if Product list is attached to any other customer.....');
            if(service.isProductListLinkedWithDifferentSellTo()){
                system.debug('=ERRoR====Product List attached to different company');
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Product_List_Linked_with_another_Customer);
                return  false;
            }
        }*/
        //L4_45352_END
        return true;
    }
    
    
}