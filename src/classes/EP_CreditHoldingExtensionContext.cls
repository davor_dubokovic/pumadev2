/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_CreditHoldingExtensionContext>                    *
*  @CreateDate <3/11/2017>                                     *
*  @Description <Context class to declare and set variables>   *
*  @Version <1.0>                                              *
****************************************************************/
public with sharing class EP_CreditHoldingExtensionContext {
	public string searchName{get;set;}
	public list<Account> searchResult;
	public List<creditCustomerWrapper> searchCustomerWrapper{get;set;}
	public List<creditCustomerWrapper> associatedCustomer{get;set;}
	public EP_Credit_Holding__c holdingObj{get;set;}
	public String returnURL;
	public boolean custSearch{get;set;}
	public string searchCriteria{get;set;}
	public boolean searchInputBlank{get;set;}
	public string searchInputBlankError{get;set;}
	
/****************************************************************
* @author       Accenture                                       *
* @name         EP_CreditHoldingExtensionContext                *
* @description  constructor of class                            *
* @param        Credit Holding, String                          *
* @return       NA                                              *
****************************************************************/
	public EP_CreditHoldingExtensionContext(EP_Credit_Holding__c holdingObj) {
		try{
		//this.searchName = 'Test';
		this.holdingObj = EP_CreditHoldingMapper.getHoldingObject(holdingObj.id);
		init();
		}catch(exception ex){
			system.debug('Error = '+ex.getMessage()+' at line = '+ex.getLineNumber());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage() + ' at line = '+ex.getLineNumber())); 

		}
		
	}

/****************************************************************
* @author       Accenture                                       *
* @name         init                                            *
* @description  method to initialize variables                  *
* @param        NA 					                            *
* @return       NA                                              *
****************************************************************/
	public void init(){
		searchResult = new list<Account>();
		searchCustomerWrapper = new List<creditCustomerWrapper>();
		associatedCustomer = new List<creditCustomerWrapper>();
		custSearch = false;
		searchInputBlank = false;
	}


/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <creditCustomerWrapper>                               *
*  @CreateDate <3/11/2017>                                     *
*  @Description <wrapper class of account>                     *
*  @Version <1.0>                                              *
****************************************************************/	
	public class creditCustomerWrapper{
		public boolean customerSelected{get;set;}
		public Account customerAcc{get;set;}
		public creditCustomerWrapper(Account customerAcc,Boolean customerSelected){
			this.customerAcc = customerAcc;
			this.customerSelected = customerSelected;
		}
	}
}