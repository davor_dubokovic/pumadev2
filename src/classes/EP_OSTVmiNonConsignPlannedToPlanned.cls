/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTVmiNonConsignmentPlannedToPlanned>
*  @CreateDate <03/02/2017>
*  @Description <Handles order status change for VMI Order status change to Planned>
*  @Version <1.0>
*/

public class EP_OSTVmiNonConsignPlannedToPlanned extends EP_OrderStateTransition{
    
    static string classname = 'EP_OSTVmiNonConsignmentPlannedToPlanned ';

    public EP_OSTVmiNonConsignPlannedToPlanned(){
        finalState =EP_OrderConstant.OrderState_Planned;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTVmiNonConsignmentPlannedToPlanned','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTVmiNonConsignmentPlannedToPlanned','isRegisteredForEvent');

        return super.isRegisteredForEvent();       
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTVmiNonConsignmentPlannedToPlanned','isGuardCondition');
        EP_OrderService orderService = new EP_OrderService(order);

         
        // when no pricing status, the state should be planned   
        if(String.IsBlank(this.order.getOrder().EP_Pricing_Status__c)){
            orderService.calculatePrice();
            return true;  
        }

        if(EP_Common_constant.PRICING_STATUS_PRICED.equalsIgnoreCase(this.order.getOrder().EP_Pricing_Status__c) && !this.order.getOrder().EP_Sync_with_NAV__c){
            if(!orderService.hasCreditIssue()){
                this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.Blank;
                return true;
            }else if(this.order.isOrderItemQuantityChanged()){
                this.order.revertOrderItemQuantity();
            }
        }
        
        return false;
    }
}