@isTest
public class EP_OrderStateDelivered_UT
{
    static final String EVENT_NAME = 'Delivered To Invoiced';
    @testSetup static void init() {
            List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
            List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
            List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
            List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }

    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_OrderStateDelivered.getTextValue();
        Test.stopTest();
        System.AssertEquals(EP_OrderConstant.OrderState_Delivered,result );     
    }

    //Method has no implementation, hence adding dummy assert  
    static testMethod void doOnEntry_test() {
        EP_OrderStateDelivered localObj = new EP_OrderStateDelivered();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateDeliveredDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assertEquals(true, true);
    }

    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_OrderStateDelivered localObj = new EP_OrderStateDelivered();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateDeliveredDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void doTransition_PositiveScenariotest() {
        EP_OrderStateDelivered localObj = new EP_OrderStateDelivered();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateDeliveredDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EVENT_NAME);
        System.debug('event##'+oe.getEventName());
        System.debug('status##'+obj.getStatus());
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_OrderStateDelivered localObj = new EP_OrderStateDelivered();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateDeliveredDomainObjectNegativeScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_EDIT);
        localObj.setOrderContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }
        catch(Exception ex){
            excList.add(ex);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    static testMethod void setOrderContext_test() {
        EP_OrderStateDelivered localObj = new EP_OrderStateDelivered();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateDeliveredDomainObjectPositiveScenario();
        EP_OrderEvent currentEvent = new EP_OrderEvent(EVENT_NAME);    
        Test.startTest();
        localObj.setOrderContext(obj,currentEvent);
        Test.stopTest();
        System.AssertEquals(true,localObj.order != null);
    }
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateDelivered localObj = new EP_OrderStateDelivered();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateDeliveredDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EVENT_NAME);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.setOrderDomainObject(obj);
        Test.stopTest();
        System.AssertEquals(obj.getOrder().Id, localObj.order.getOrder().Id);
    }
}