/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTAwaitingCreditReviewToSubmitted>
*  @CreateDate <03/02/2017>
*  @Description <Order state transitions from review to submitted>
*  @Version <1.0>
*/

public class EP_OSTAwaitingCreditReviewToSubmitted extends EP_OrderStateTransition{
  
      public EP_OSTAwaitingCreditReviewToSubmitted(){
        finalState = EP_OrderConstant.OrderState_Submitted;
      }
      
      public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTAwaitingCreditReviewToSubmitted','isTransitionPossible');
        return super.isTransitionPossible();
      }
      
      public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTAwaitingCreditReviewToSubmitted','isRegisteredForEvent');
        //system.debug('In isRegisteredForEvent of EP_OSTAwaitingCreditReviewToSubmitted');
        return super.isRegisteredForEvent();       
      }

      public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTAwaitingCreditReviewToSubmitted','isGuardCondition');
           return true;               
      } 
  }