/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToInActiveToActive>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 07-Inactive to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToInActiveToActive extends EP_AccountStateTransition {

    public EP_ASTVMIShipToInActiveToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToActive','doOnExit');

    }
}