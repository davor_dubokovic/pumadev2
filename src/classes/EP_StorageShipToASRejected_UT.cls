@isTest
public class EP_StorageShipToASRejected_UT
{
	static final string EVENT_NAME = '08-RejectedTo08-Rejected';
	static testMethod void setAccountDomainObject_test() {
		EP_StorageShipToASRejected localObj = new EP_StorageShipToASRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASRejectedDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.setAccountDomainObject(obj);
		Test.stopTest();
		system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
	}
	static testMethod void doOnEntry_test() {
		EP_StorageShipToASRejected localObj = new EP_StorageShipToASRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASRejectedDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnEntry();
		Test.stopTest();
		//Method has no implementation, hence adding dummy assert
		system.assert(true);
		
	}
	static testMethod void doOnExit_test() {
		EP_StorageShipToASRejected localObj = new EP_StorageShipToASRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASRejectedDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnExit();
		Test.stopTest();
		//Method has no implementation, hence adding dummy assert
		system.assert(true);
		
	}
	static testMethod void doTransition_PositiveScenariotest() {
		List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
		List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
		EP_StorageShipToASRejected localObj = new EP_StorageShipToASRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASRejectedDomainObjectPositiveScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.doTransition();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	static testMethod void doTransition_NegativeScenariotest() {
		EP_StorageShipToASRejected localObj = new EP_StorageShipToASRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASRejectedDomainObjectNegativeScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		string errormsg = null;
		try{
			Boolean result = localObj.doTransition();
		}catch (exception e){
			errormsg = e.getmessage();
		}
		Test.stopTest();
		System.AssertEquals(true,errormsg != null);
	}
	static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
		EP_StorageShipToASRejected localObj = new EP_StorageShipToASRejected();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASRejectedDomainObjectPositiveScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.isInboundTransitionPossible();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
}