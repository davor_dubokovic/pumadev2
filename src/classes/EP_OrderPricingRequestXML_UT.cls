@isTest
private class EP_OrderPricingRequestXML_UT {
  private static final string MESSAGE_TYPE = 'SEND_ORDER_CREDIT_CHECK_IN_PROGRESS';

    //#60147 User Story Start
    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }
    //#60147 User Story End

   static testMethod void createXML_Test() {
       EP_OrderPricingRequestXML localObj = new EP_OrderPricingRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

   static testMethod void createPayload_Test() {
       EP_OrderPricingRequestXML localObj = new EP_OrderPricingRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createPayload();  
       Test.stopTest();
       //no assert needed. Value are only being set
   }

   static testMethod void createStatusPayLoad_Test() {
       EP_OrderPricingRequestXML localObj = new EP_OrderPricingRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createStatusPayLoad();  
       Test.stopTest();
       //no assert needed. Value are only being set
   }
   
   //#60147 User Story Start
   static testMethod void init_Test() {
       EP_GenerateOrderRequestXML localObj = new EP_GenerateOrderRequestXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       Test.startTest();
       localObj.init();  
       Test.stopTest();
       //This method delegates to other methods, adding dummy assert
       system.assert(true);
   }
   //#60147 User Story End
}