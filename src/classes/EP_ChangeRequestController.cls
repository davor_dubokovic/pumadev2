/* 
	@Author <Vikas Malik>
	@name <EP_ChangeRequestController>
	@CreateDate <05/01/2016>
	@Description <This class acts as the controller class for EP_ChangeRequest VF page>
	@Version <1.0>

*/
public without sharing class EP_ChangeRequestController { 

	public EP_ChangeRequest__c request {get;set;}
	public List<EP_ChangeRequestLine__c> requestLines {get;set;}
	public List<cRWrapperFields> allFieldValues {get;set;}
	public Boolean isError {get;set;}
	public sObject newObjInstance {get; set;}
	public schema.DisplayType refType {get; set;}
	
	private EP_ChangeRequestService.ApplicableFieldset applicableFieldSet;
	//TODO Currently defining these here. Will merge later
	private static final String OBJ_ID = 'id';
	private static final String OBJ_TYPE = 'type';
	private static final String RET_URL = 'returl';
	private static final String OP_TYPE = 'op';
	private static final String REQ_OPEN_STATUS = 'Open';
	private static final String OPT_NONE = 'None';
	private static final String OPT_BLNK = '';
	private static final String REC_TYPE_FIELD = 'RecordTypeId';
	private static final String QUERY_WHERE = ' Where ';
	private static final String VAL_REQ_MSG = 'Value is required for ';
	private static final String ACCOUNTSTR = 'Account';
	private static final String BANKACCOUNTOBJ = 'EP_Bank_Account__c';
	private static final String TANKOBJ = 'EP_Tank__c';
	private static final String UPDATEOP = 'UPDATE';
	private static final string REC_TYPE = 'rtype';
	private static final string OBJ_NAME = 'objname';
	private static final string REC_TYPEID = 'rtypeID';
	private static final string ACC_STATUS_FLD = 'EP_Status__c';
	private static final String REC_TYPE_NAME = 'recTypeName';
	private static final String STEP1 = 'Step 1';
	private static final String STEP2 = 'Step 2';
	private static final String STEP3 = 'Step 3';
	private static final String STEP4 = 'Step 4';
	private static final String STEP5 = 'Step 5';
	private static final String KEY_STR = 'Key';
	
	private string recTypeName ;
	public sobject recobj{GET; SET;}
	private static Map<String,String> fieldLabelMap = new Map<String,String>();
	private string retURL;
	private string evntType;
	private static final string CLASS_NAME = 'EP_ChangeRequestController';
	private static final string SUBMIT_MTD = 'submit';
	private static final string ID_CONDITON_STR = ' Id = \'';
	private static final string SINGLE_QUOTE_STR ='\'';
	private integer nRows;
	private boolean isPortalUser;
	private map<String, Schema.SObjectField>mFields;

	/**
	* @author <Vikas Malik>
	* @name <EP_ChangeRequestController>
	* @date <05/01/2016>
	* @description <Constructor>
	* @version <1.0>
	* @param none
	* @return void
	*/
	public EP_ChangeRequestController(){
		isError = false;
		isPortalUser = false;
		refType = schema.DisplayType.REFERENCE;
		try{
			initialize();
		}catch (Exception e){
			// DML Not allowed during constructor hence cannot use Logging Service here
			isError = true;
			addPageMEssages(ApexPages.Severity.Error,Label.EP_Change_Request_Page_Error_Msg);
		}
	}
	
	/**
	* @author <Vikas Malik>
	* @name <initialize>
	* @date <05/01/2016>
	* @description <This method is called from constructor to initialize the variables.>
	* @version <1.0>
	* @param none
	* @return void
	*/
	private void initialize(){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','initialize');
		String objId = Apexpages.currentpage().getparameters().get(OBJ_ID);
		String objType = Apexpages.currentpage().getparameters().get(OBJ_TYPE);
		evntType = Apexpages.currentpage().getparameters().get(OP_TYPE);
		retURL = Apexpages.currentpage().getparameters().get(RET_URL);
		recTypeName = Apexpages.currentpage().getparameters().get(REC_TYPE_NAME);
		
		mFields = Schema.getGlobalDescribe().get(objType).getDescribe().fields.getMap();
		
		isPortalUser = EP_Common_Util.checkPortalUser(UserInfo.getUserId());
		if (objId != null && string.isNotBlank(objId)){
			string respectiVeFIeld = EP_ChangeRequestService.getRequestFIeldMapping(objType);
			request = EP_ChangeRequestService.createChangeRequestRecord(objId, objType, recTypeName, evntType, respectiVeFIeld);
			newObjInstance = Schema.getGlobalDescribe().get(request.EP_Object_Type__c).newSObject() ;
			getRecordObject();
			requestLines = new List<EP_ChangeRequestLine__c>{new EP_ChangeRequestLine__c()};
			objectFields();
		}
	}
	
	/**
	* @author <Vikas Malik>
	* @name <getEditFields>
	* @date <05/01/2016>
	* @description <This method returns the list of editable fields per object based on a configurable object in salesforce>
	* @version <1.0>
	* @param none
	* @return editFields
	*/
	/*public List<SelectOption> getEditFields(){
		editFields = new List<SelectOption>();
		editFields.add(new SelectOption(OPT_BLNK,OPT_NONE));
		String recordTypeId = String.valueOf(recObj.get(REC_TYPE_FIELD));
		RecordType recType = [SELECT SobjectType, Name, Id, DeveloperName FROM RecordType WHERE Id = :recordTypeId AND SobjectType = :request.EP_Object_Type__c];
		for(FieldSetMember fsm : Schema.getGlobalDescribe().get(request.EP_Object_Type__c)
										.getDescribe().FieldSets.getMap().get(recordTypeFieldSetMap.get(recType.DeveloperName)).getFields()){
				
				editFields.add(new SelectOption(fsm.getFieldPath(),fsm.getLabel()));
				fieldLabelMap.put(fsm.getFieldPath(),fsm.getLabel());
			}
		return editFields;
	}*/
	
	/**
	* @author <Vikas Malik>
	* @name <objectFields>
	* @date <05/01/2016>
	* @description <This method is creating list of available fields from fieldset for editing based on profile and object>
	* @version <1.0>
	* @param none
	* @return void
	*/
	private void objectFields(){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','objectFields');
		allFieldValues = new List<cRWrapperFields>();
		String recordTypeId ;
		string rtypeName;
		String recTypeDevName;
		
		// Check If object Cotains recordType or not
		if(EP_Common_Util.checkIsRecordsDefined(request.EP_Object_Type__c)){
			recordTypeId = String.valueOf(recObj.get(REC_TYPE_FIELD));
		}
		
		// If record TypeId is not null query recordType Dev Name
		if(string.isNotBlank(recordTypeId)){
			rtypeName = EP_Common_Util.getRecodTypeDevNameFromID(recordTypeId); 
			recTypeDevName = rtypeName;
		}
		
		// If recordType name is passed from parameters then use that
		if(string.isNotBlank(recTypeName)){
			rtypeName = recTypeName;
		}
		system.debug('rectype'+rtypeName);
		//Finf field Set Name
		//string fieldSetName = EP_ChangeRequestService.findApplicableFieldset(request.EP_Object_Type__c, rtypeName, evntType);
		applicableFieldSet = new EP_ChangeRequestService.ApplicableFieldset(false);
		applicableFieldSet = EP_ChangeRequestService.findApplicableFieldset(request.EP_Object_Type__c, rtypeName, evntType);
		if(string.isNotBlank(applicableFieldSet.fieldSetName)){
			iterateFieldSet(applicableFieldSet.fieldSetName,recTypeDevName);
		}else{
			addPageMEssages(ApexPages.Severity.ERROR,Label.EP_Change_Request_FieldSet_Error_MSG);
		}
	}

	/**
	* @author <Vikas Malik>
	* @name <iterateFieldSet>
	* @date <05/01/2016>
	* @description <This method iterates over a fieldset and adding field wrapper element in field list, which will show on VF page. DEFECT#23071>
	* @version <1.0>
	* @param String,String
	* @return void
	*/
	private void iterateFieldSet(string fieldSetName,String recTypeDevName){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','iterateFieldSet');
		Boolean isAllowedBlocked = false;
		cRWrapperFields rbcrw;
		map<String,String>mPicklist = new map<String,String>();
		nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
		Profile pricingProfile = [Select Id from Profile where name =:EP_Common_Constant.PRICING_PROFILE limit 1 ];
		if(String.isNotBlank(recTypeDevName)){
			for(CR_Picklist_Mapping__mdt picklistMapping :  [Select EP_Field_Name__c
			,EP_Picklist_Values__c
			,EP_Record_Type__c
			,EP_Controlling_Field__c
			,EP_Controlling_Value__c
			From CR_Picklist_Mapping__mdt
			Where EP_Object_Type__c = :request.EP_Object_Type__c
			limit :nRows ]){
				System.debug(recTypeDevName);
				if(picklistMapping.EP_Record_Type__c.containsIgnoreCase(recTypeDevName)){
					
					mPicklist.put( picklistMapping.EP_Controlling_Field__c.toUpperCase()
					+picklistMapping.EP_Controlling_Value__c.toUpperCase()
					+picklistMapping.EP_Field_Name__c.toUpperCase(),picklistMapping.EP_Picklist_Values__c); 
				}
			}
		}
		
		for(FieldSetMember fsm : Schema.getGlobalDescribe().get(request.EP_Object_Type__c)
		.getDescribe().FieldSets.getMap().get(fieldSetName).getFields()){
			
			
			isAllowedBlocked = false;
			boolean isUpdatable = Schema.getGlobalDescribe().get(request.EP_Object_Type__c).getDescribe().fields.getMap().get(fsm.getFieldPath()).getDescribe().isUpdateable();
			if(isUpdatable){
				String strFieldName = fsm.getLabel();
				
				fieldLabelMap.put(fsm.getFieldPath(), strFieldName);
				
				cRWrapperFields crw = createChangeRequestItemRec(fsm.getFieldPath(), strFieldName, fsm.getType());
				crw.isRestrictedPickist = FALSE;
				
				//****Special case***
				// IF Field is - Account's Status field.
				// do not show standard input field on VF page, because we need to show only 2 option in picklist -Active and Inactive
				// creating list of selectoption for custom picklist 
				if (ACC_STATUS_FLD.equalsIgnoreCase(fsm.getFieldPath()) && ACCOUNTSTR.equalsIgnoreCase(request.EP_Object_Type__c)) {
					
					string stval = String.valueOF(recobj.get(ACC_STATUS_FLD));
					//IF PRICING USER THEN CAN CHANGE STATUS FROM ACTIVE TO INACTIVE
					if(UserInfo.getProfileId() == pricingProfile.Id &&  (EP_Common_Constant.STATUS_INACTIVE.equalsIgnoreCase(stval ) || EP_Common_Constant.STATUS_BLOCKED.equalsIgnoreCase(stval ))){
						crw.restrictedPicklist = new list<selectOption>();
						crw.restrictedPicklist.add(createoption(stval, stval));
					}
					else{
						crw.restrictedPicklist = createRestrictedAccStatusPickList();
						if(!EP_Common_constant.STATUS_ACTIVE.equalsIgnoreCase(stval) && !EP_Common_constant.STATUS_INACTIVE.equalsIgnoreCase(stval)){
							crw.restrictedPicklist.add(0, createoption(stval,stval));
						}
						if(!EP_Common_Constant.STATUS_BLOCKED.equalsIgnoreCase(stval) 
								&& applicableFieldSet.isBlockedAllowed){
							crw.restrictedPicklist.add(0, createoption(EP_Common_Constant.STATUS_BLOCKED,EP_Common_Constant.STATUS_BLOCKED));
						}
					}                    
					
					crw.restrictedPicklist.sort();   
					
					crw.isRestrictedPickist = TRUE;
					if(EP_Common_Constant.STATUS_BLOCKED.equalsIgnoreCase(stval)
							|| applicableFieldSet.isBlockedAllowed){
						
						Schema.DescribeFieldResult dfr = Schema.SObjectType.Account.fields.EP_Reason_Blocked__c;
						
						fieldLabelMap.put(dfr.getName(), dfr.getLabel());
						
						rbcrw = createChangeRequestItemRec(dfr.getName(), dfr.getLabel(), dfr.getType());
						rbcrw.isRestrictedPickist = TRUE;
						rbcrw.restrictedPicklist.add(createoption(EP_Common_Constant.BLANK,EP_Common_Constant.PICKLIST_NONE));
						if(mPicklist.containsKey(   EP_Common_Constant.EP_STATUS.toUpperCase()
									+EP_Common_Constant.STATUS_BLOCKED.toUpperCase()
									+dfr.getName().toUpperCase())){
							for(String pickOption : mPicklist.get(EP_Common_Constant.EP_STATUS.toUpperCase()
							+EP_Common_Constant.STATUS_BLOCKED.toUpperCase()
							+dfr.getName().toUpperCase()).split(EP_Common_Constant.COMMA)){
								rbcrw.restrictedPicklist.add(createoption(pickOption,pickOption));      
							}
						}
						isAllowedBlocked = true;
					}
				}
				
				if(fsm.getDBRequired() || fsm.getRequired()){
					crw.isfieldRequired = true;
				}
				allFieldValues.add(crw);
				if(isAllowedBlocked){
					allFieldValues.add(rbcrw);
					isAllowedBlocked = false;
				}
			}
		}        
	}


	/**
	* @author <Vikas Malik>
	* @name <createoption>
	* @date <05/01/2016>
	* @description <this method return selectoption object Instance>
	* @version <1.0>
	* @param String,String
	* @return SelectOption
	*/
	private static selectOption createoption(string oValue, string oLabel){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','createoption');
		return new SelectOption(oValue,oLabel);
	}

	/**
	* @author <Vikas Malik>
	* @name <createRestrictedAccStatusPickList>
	* @date <05/01/2016>
	* @description <Create selectOption picklist for showing specific status Values. Appliicable in case of Account's Status Field.>
	* @version <1.0>
	* @param none
	* @return List<SelectOption>
	*/
	private static List<SelectOption> createRestrictedAccStatusPickList(){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','createRestrictedAccStatusPickList');
		List<SelectOption> restrictedPicklist = new List<SelectOption>();
		restrictedPicklist.add(createoption(EP_Common_constant.STATUS_ACTIVE, EP_Common_constant.STATUS_ACTIVE));
		restrictedPicklist.add(createoption(EP_Common_constant.STATUS_INACTIVE, EP_Common_constant.STATUS_INACTIVE));
		return restrictedPicklist;
	}
	
	/**
	* @author <Vikas Malik>
	* @name <createChangeRequestItemRec>
	* @date <05/01/2016>
	* @description <Creating wrapper element for field list>
	* @version <1.0>
	* @param String,String,Schema.DisplayType
	* @return cRWrapperFields
	*/
	private cRWrapperFields createChangeRequestItemRec( String fieldAPIName, 
	String fieldLabel,
	Schema.DisplayType dt){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','createChangeRequestItemRec');
		EP_ChangeRequestLine__c crObj = new EP_ChangeRequestLine__c();
		crObj.EP_Source_Field_API_Name__c = fieldAPIName;
		crObj.EP_Source_Field_Label__c = fieldLabel;
		if(mFields.get(crObj.EP_Source_Field_API_Name__c).getDescribe().getType().equals(Schema.DisplayType.Reference)){
			crObj.EP_Original_Reference_Value__c     = String.valueOF(recobj.get(fieldAPIName));
		}
		else{
			crObj.EP_Original_Value__c = String.valueOF(recobj.get(fieldAPIName));
		}
		cRWrapperFields wrapObj = new cRWrapperFields(crObj);
		wrapObj.fieldType = dt;
		return wrapObj;
	}
	
	/**
	* @author <Vikas Malik>
	* @name <getRecordObject>
	* @date <05/01/2016>
	* @description <This method will query and return the field values for the record for which the Change Request s being created>
	* @version <1.0>
	* @param none
	* @return void
	*/
	private void getRecordObject(){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','getRecordObject');
		SObjectType objToken = Schema.getGlobalDescribe().get(request.EP_Object_Type__c);
		DescribeSObjectResult objDef = objToken.getDescribe();
		Map<String, SObjectField> fields = objDef.fields.getMap();
		Set<String> fieldSet = fields.keySet();
		String queryStr=EP_Common_Constant.BLANK;
		
		for(String s:fieldSet){
			SObjectField fieldToken = fields.get(s);
			DescribeFieldResult selectedField = fieldToken.getDescribe();
			//if (selectedField.isUpdateable())
			queryStr = queryStr + selectedField.getName() + EP_Common_Constant.COMMA;
		}
		
		if(queryStr.length() > =1){
			queryStr=queryStr.substring(0, queryStr.length()-1);
		}
		
		if(UPDATEOP.equalsIgnoreCase(evntType)){
			queryStr = EP_Common_Constant.SELECT_STRING+queryStr+EP_Common_Constant.FROM_STRING+request.EP_Object_Type__c+QUERY_WHERE+ ID_CONDITON_STR + request.EP_Object_ID__c+ SINGLE_QUOTE_STR;
			recObj = Database.query(queryStr);
			newObjInstance = recObj.clone(true, true, true, true);//Database.query(queryStr); //recObj.clone(false, true);
		}else{
			recObj = Schema.getGlobalDescribe().get(request.EP_Object_Type__c).newSObject() ;
		}
	}
	
	/**
	* @author <Vikas Malik>
	* @name <getCurrentFieldvalue>
	* @date <05/01/2016>
	* @description <This method will return the current value for the selected field on the page>
	* @version <1.0>
	* @param none
	* @return null
	*/
	/*
	public PageReference getCurrentFieldvalue(){
		//System.debug('request Sent--'+request);
		for (EP_ChangeRequestLine__c line: requestLines){
			//System.debug('line.EP_Source_Field_API_Name__c--'+line.EP_Source_Field_API_Name__c);
			if (null != line.EP_Source_Field_API_Name__c && !''.equals(line.EP_Source_Field_API_Name__c)){
				//System.debug('Value is---'+String.valueOf(recObj.get(line.EP_Source_Field_API_Name__c)));
				line.EP_Original_Value__c = String.valueOf(recObj.get(line.EP_Source_Field_API_Name__c));
			}
			//System.debug('line--'+line);
		}
		
		return null;
	}*/
	
	
	/**
	* @author <Vikas Malik>
	* @name <submit>
	* @date <05/01/2016>
	* @description <This method will validate and create the change request object along with associated lines.>
	* @version <1.0>
	* @param none
	* @return null
	*/
	public PageReference submit(){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','submit');
		set<String> step1Review = new set<String>();
		set<String> step2Review = new set<String>();
		set<String> step3Review = new set<String>();
		set<String> step4Review = new set<String>();
		set<String> step5Review = new set<String>();
		map<String,Id>mNameAction = new map<String,Id>();
		map<String,EP_CR_Field_Approval_Mapping__mdt> mapStatusField = new map<String,EP_CR_Field_Approval_Mapping__mdt>();
		EP_ChangeRequest__c reqAfterInsert; 
		
		try{
			requestLines = new List<EP_ChangeRequestLine__c>();
			for (cRWrapperFields line: allFieldValues){
				if(line.isChanged){
					requestLines.add(line.crlobj);
				}
			}
			
			if(requestLines.isEmpty()){
				addPageMEssages(ApexPages.Severity.Error,Label.EP_CR_No_Change_Found_Error_Msg);
				return null;
			}
			
			List<String> valMessages = EP_ChangeRequestService.validateRequestedChanges(request,requestLines);
			
			system.debug('======valMessages ======='+valMessages );
			if (!valMessages.isEmpty()){
				for (String valmsg: valMessages){
					addPageMEssages(ApexPages.Severity.Error,valmsg);
				}
			}else{
				Set<String> flds = new Set<String>();
				for(EP_ChangeRequestLine__c line : requestLines){
					flds.add(line.EP_Source_Field_API_Name__c);
				}
				
				if(!flds.isEmpty()){
					
					
					nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
					Profile profile = [SELECT Name FROM Profile WHERE id =: Userinfo.getProfileId() limit 1];
					EP_CR_Profile_Mapping__mdt crpmapping  = [Select Id, EP_Profile_Group__c, DeveloperName, MasterLabel 
					from EP_CR_Profile_Mapping__mdt 
					where MasterLabel =:  profile.Name
					limit :nRows];
					string profileGroupName = EP_Common_Constant.BLANK;                                   
					if(crpmapping != null){
						profileGroupName    = crpmapping.EP_Profile_Group__c;
					}
					
					/*Map<string, Map<string, Map<String, Map<String, Map<String,EP_CR_Field_Approval_Mapping__mdt>>>>> fieldMapping = 
							EP_ChangeRequestService.fetchObjectFieldActionMapping(new Set<string>{request.EP_Object_Type__c},
																				flds,
																				new Set<String>{EP_Common_Constant.ANYSTRING, request.EP_Record_Type__c});*/
					
					String key;
					Set<string> rtypeStrSet = new Set<string>{EP_Common_Constant.ANYSTRING};
					if(string.isNotBlank(request.EP_Record_Type__c)) {
						rtypeStrSet.add(request.EP_Record_Type__c);
					}
					map<String,EP_CR_Field_Approval_Mapping__mdt>fieldMapping = EP_ChangeRequestService.fetchActionMapping(new Set<string>{request.EP_Object_Type__c},
					flds,
					rtypeStrSet);
					System.debug(fieldMapping);
					system.debug('===profileGroupName==='+profileGroupName);
					
					
					
					for (EP_ChangeRequestLine__c line: requestLines){
						System.debug('**here**');   
						System.debug('**here**'+line.EP_Source_Field_API_Name__c);
						System.debug(line.EP_Source_Field_API_Name__c);
						if(string.isNotBlank(line.EP_Source_Field_API_Name__c) /*&& 
								fieldMapping.get(request.EP_Object_Type__c).containsKey(line.EP_Source_Field_API_Name__c) && 
									fieldMapping.get(request.EP_Object_Type__c).get(line.EP_Source_Field_API_Name__c).containsKey(profileGroupName)*/
								){
							
							string newval = string.isNotBlank(line.EP_New_Value__c) ? line.EP_New_Value__c.toUpperCase(): EP_Common_Constant.BLANK;
							string oldVal = string.isNotBlank(line.EP_Original_Value__c) ? line.EP_Original_Value__c.toUpperCase(): EP_Common_Constant.BLANK;
							if(!fieldMapping.isEmpty() /*&& fieldMapping.containskey(request.EP_Object_Type__c)*/){
								string RType;
								string dataval;
								EP_CR_Field_Approval_Mapping__mdt fldMDT ;
								
								key = (request.EP_Object_Type__c
								+EP_Common_Constant.TIME_SEPARATOR_SIGN
								+line.EP_Source_Field_API_Name__c
								+EP_Common_Constant.TIME_SEPARATOR_SIGN
								+profileGroupName
								+EP_Common_Constant.TIME_SEPARATOR_SIGN
								+request.EP_Record_Type__c
								).toUpperCase();
								
								RType= (string.isNotBlank(request.EP_Record_Type__c) 
								&& 
								(fieldMapping.containsKey((key + EP_Common_Constant.TIME_SEPARATOR_SIGN + newVal + EP_Common_Constant.TIME_SEPARATOR_SIGN + oldVal).toUppercase())
								|| fieldMapping.containsKey((key + EP_Common_Constant.TIME_SEPARATOR_SIGN + newVal).toUppercase())
								|| fieldMapping.containsKey((key + EP_Common_Constant.TIME_SEPARATOR_SIGN + oldVal).toUppercase())
								|| fieldMapping.containsKey(key)
								)  )?request.EP_Record_Type__c 
								:EP_Common_Constant.ANYSTRING ;
								key = (request.EP_Object_Type__c
								+EP_Common_Constant.TIME_SEPARATOR_SIGN
								+line.EP_Source_Field_API_Name__c
								+EP_Common_Constant.TIME_SEPARATOR_SIGN
								+profileGroupName
								+EP_Common_Constant.TIME_SEPARATOR_SIGN
								+RType
								+(String.isNotBlank(newVal)? EP_Common_Constant.TIME_SEPARATOR_SIGN + newVal:EP_Common_Constant.BLANK)
								+(String.isNotBlank(oldVal)? EP_Common_Constant.TIME_SEPARATOR_SIGN + oldVal:EP_Common_Constant.BLANK)).toUpperCase();
								System.debug(KEY_STR+key);
								system.debug('line.EP_New_Value__c===='+line.EP_New_Value__c);                  
								if(!fieldMapping.containsKey(key)){
									key = key.removeEnd(EP_Common_Constant.TIME_SEPARATOR_SIGN + oldVal.toUppercase());
									System.debug(KEY_STR+key);
									if(!fieldMapping.containsKey(key)){
										key = key.removeEnd(newVal.toUpperCase()) + oldVal.toUppercase();
										System.debug(KEY_STR+key);
										if(!fieldMapping.containsKey(key)){
											key = key.removeEnd(EP_Common_Constant.TIME_SEPARATOR_SIGN + oldVal.toUppercase());
											System.debug(KEY_STR+key);
										}
									}
								}
								/*dataval = (fieldMapping.get(request.EP_Object_Type__c).
																	get(line.EP_Source_Field_API_Name__c).
																		get(profileGroupName).
																			containsKey(RType) 
													&& 
													fieldMapping.get(request.EP_Object_Type__c).
																	get(line.EP_Source_Field_API_Name__c).
																		get(profileGroupName).
																			get(RType).containsKey(newval)
													)
													?
													newval
													:
													null;*/  
								
								system.debug('==RType==='+RType);   
								system.debug('==dataval==='+dataval);
								//system.debug('==1111==='+fieldMapping.get(request.EP_Object_Type__c).get(line.EP_Source_Field_API_Name__c).get(profileGroupName));                      
								//system.debug('==222==='+fieldMapping.get(request.EP_Object_Type__c).get(line.EP_Source_Field_API_Name__c).get(profileGroupName).get(RType));                        
								//system.debug('==1111==='+fieldMapping.get(request.EP_Object_Type__c).get(line.EP_Source_Field_API_Name__c).get(profileGroupName));                      
								system.debug('==1111==='+fieldMapping.get(key));
								if(/*fieldMapping.get(request.EP_Object_Type__c).get(line.EP_Source_Field_API_Name__c).get(profileGroupName).containsKey(RType)
									&& 
									fieldMapping.get(request.EP_Object_Type__c).get(line.EP_Source_Field_API_Name__c).get(profileGroupName).get(RType).containsKey(dataval)*/
										fieldMapping.containsKey(key)   
										){
									
									fldMDT = fieldMapping.get(key);
									if(fldMDT != null){
										system.debug('====fldMDT==='+fldMDT);
										mapStatusField.put(fldMDT.EP_Field_API_Name__c.toUpperCase(),fldMDT);
										
										if(String.isNotBlank(fldMDT.EP_Step1__c)){
											step1Review.add(fldMDT.EP_Step1__c);
										}
										if(String.isNotBlank(fldMDT.EP_Step2__c)){
											step2Review.add(fldMDT.EP_Step2__c);
										}
										if(String.isNotBlank(fldMDT.EP_Step3__c)){
											step3Review.add(fldMDT.EP_Step3__c);
										}
										if(String.isNotBlank(fldMDT.EP_Step4__c)){
											step4Review.add(fldMDT.EP_Step4__c);
										}
										if(String.isNotBlank(fldMDT.EP_Step5__c)){
											step5Review.add(fldMDT.EP_Step5__c);
										} 
									}   
								}               
								
							}       
						}
					}
				}
				system.debug('===step1Review==='+step1Review);
				system.debug('===EP_Step2__c==='+step2Review);
				system.debug('===step3Review==='+step3Review);
				system.debug('===step4Review==='+step4Review);
				system.debug('===step5Review==='+step5Review);
				Database.insert(request);
				nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
				reqAfterInsert = [Select Id
				,EP_Region__c
				,EP_Delivery_Country__c 
				From EP_ChangeRequest__c
				Where Id = :request.Id
				limit :nRows];
				
				String fieldName;
				Integer count=0;
				for (EP_ChangeRequestLine__c line: requestLines){
					line.EP_Change_Request__c = request.Id;
					fieldName = line.EP_Source_Field_API_Name__c.toUpperCase();
					if(mapStatusField.containsKey(fieldName)){
						
						if(!string.isBlank(mapStatusField.get(fieldName).EP_Step1__c)){
							line.EP_Review_Step__c = mapStatusField.get(fieldName).EP_Step1__c;
						}
						else if(!string.isBlank(mapStatusField.get(fieldName).EP_Step2__c)){
							line.EP_Review_Step__c = mapStatusField.get(fieldName).EP_Step2__c;
						}
						else if(!string.isBlank(mapStatusField.get(fieldName).EP_Step3__c)){
							line.EP_Review_Step__c = mapStatusField.get(fieldName).EP_Step3__c;
						}
						else if(!string.isBlank(mapStatusField.get(fieldName).EP_Step4__c)){
							line.EP_Review_Step__c = mapStatusField.get(fieldName).EP_Step4__c;
						}
						else if(!string.isBlank(mapStatusField.get(fieldName).EP_Step5__c)){
							line.EP_Review_Step__c = mapStatusField.get(fieldName).EP_Step5__c;
						} 
						
						else{}
						//IF no step defined set status as Accepted
						// Step 1 is required in case of portal user 
						system.debug('==line.EP_Review_Step__c=='+line.EP_Review_Step__c);
						system.debug('======='+mapStatusField.get(fieldName));
						if(string.isBlank(mapStatusField.get(fieldName).EP_Step1__c) 
								&& string.isBlank(mapStatusField.get(fieldName).EP_Step2__c)
								&& string.isBlank(mapStatusField.get(fieldName).EP_Step3__c)
								&& string.isBlank(mapStatusField.get(fieldName).EP_Step4__c)
								&& string.isBlank(mapStatusField.get(fieldName).EP_Step5__c)){
							line.EP_Request_Status__c = EP_Common_Constant.CRLITEM_STATUS_APPROVED;
							count++;
						}
					}     
				}
				if(count == requestLines.size()){
					request.EP_Request_Status__c = EP_Common_Constant.COMPLETED;
					EP_IntegrationUtil.isCallout = false;
					EP_TankTriggerHandler_R1.isAfterUpdate = false;
					//EP_AccountTriggerHandler.isExecuteBeforeUpdate  = false;
					//EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
					EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
					EP_BankAccountTriggerHandler.isExecutedByIntegrationUser = true;
					EP_CheckRecursive.isBeforeRunning = true;
					EP_CheckRecursive.isAfterRunning = true;
					SObject sobj = EP_ChangeRequestService.commitChangeRequest(request,requestLines);
					System.debug(sobj);
					EP_IntegrationUtil.isCallout = false;
					system.debug('-----EP_IntegrationUtil.isCallout---');
					Database.upsertResult result = Database.upsert(sobj,false);
					
				}
				
				
				id actionRecordType; 
				actionRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACT_OBJ, EP_Common_Constant.ACT_CR_RT);
				List<EP_Action__c> listOfReview = new List<EP_Action__c>();
				string processName = EP_Common_Constant.BLANK;
				//Check if the logged in User is portal user
				if( !step1Review.isEmpty()){
					listOfReview = EP_ChangeRequestService.createAction(step1Review,request.id,actionRecordType);
					processName = STEP1;
				}else if(!step2Review.isEmpty()){
					listOfReview = EP_ChangeRequestService.createAction(step2Review,request.id,actionRecordType);
					processName = STEP2;
				}else if(!step3Review.isEmpty()){
					listOfReview = EP_ChangeRequestService.createAction(step3Review,request.id,actionRecordType);
					processName = STEP3;
				}else if(!step4Review.isEmpty()){
					listOfReview = EP_ChangeRequestService.createAction(step4Review,request.id,actionRecordType);
					processName = STEP4;
				}else if(!step5Review.isEmpty()){
					processName = STEP5;
					listOfReview = EP_ChangeRequestService.createAction(step5Review,request.id,actionRecordType);
				}else{
					processName = EP_Common_Constant.BLANK;
				}
				request.EP_Process_Step__c = processName;
				
				
				if(!listOfReview.isEmpty()){
					for(EP_Action__c action : listOfReview){
						if(String.isNotBlank(reqAfterInsert.EP_Delivery_Country__c)){
							action.EP_Country__c = reqAfterInsert.EP_Delivery_Country__c;
						}
						if(String.isNotBlank(reqAfterInsert.EP_Delivery_Country__c)){
							action.EP_Region__c = reqAfterInsert.EP_Region__c;
						}   
					}
					database.insert(listOfReview);
					for(EP_Action__c action : listOfReview){
						
						mNameAction.put(action.EP_Action_Name__c.toUpperCase(),action.Id);
					}
					System.debug('******');
					for(EP_ChangeRequestLine__c requestLine : requestLines){
						if(String.isNotBlank(requestLine.EP_Review_Step__c)
								&& mNameAction.containsKey(requestLine.EP_Review_Step__c.toUpperCase())){
							requestLine.EP_Action__c = mNameAction.get(requestLine.EP_Review_Step__c.toUpperCase());
						}
					}
				}
				
				Database.insert(requestLines);
				//Added on 31-03-2017 By Amit
				EP_IntegrationUtil.isCallout = false;
				database.update(request);
				if(string.isBlank(returl)){
					returl = EP_Common_Constant.SLASH+request.id;
				}
				
				
				PageReference ref = new PageReference(returl);
				//ref.setRedirect(true);
				return ref;
			}
		}
		catch (Exception e){
			system.debug('======'+e);
			EP_LoggingService.logHandledException (e, EP_Common_constant.EPUMA, SUBMIT_MTD, CLASS_NAME, ApexPages.Severity.ERROR);
			addPageMEssages(ApexPages.Severity.Error,Label.EP_Change_Request_Submit_Error_Msg);
		}
		return null;
	}
	
	/**
	* @author <Vikas Malik>
	* @name <addPageMEssages>
	* @date <05/01/2016>
	* @description <Common method to show page Messages, based on severity and message  string>
	* @version <1.0>
	* @param ApexPages.Severity,String
	* @return void
	*/
	private void addPageMEssages(ApexPages.Severity sevrty, string message){
		apexpages.Message msg = new Apexpages.Message(sevrty,message);
		Apexpages.addmessage(msg);
	}
	
	/**
	* @author <Vikas Malik>
	* @name <cancel>
	* @date <05/01/2016>
	* @description <This method get called from vf page , when user clicks "Cancel" button>
	* @version <1.0>
	* @param none
	* @return PageReference
	*/
	public PageReference cancel(){
		EP_GeneralUtility.Log('Private','EP_ChangeRequestController','cancel');
		PageReference ref ;
		try{
			ref= new PageReference(returl);
		}Catch(Exception e){
			Apexpages.addmessages(e);
		}
		return ref;
	}

	/**
	* @author <Vikas Malik>
	* @name <CRWrapperFields>
	* @date <05/01/2016>
	* @description <Wrapper class for Request line Items>
	* @version <1.0>
	*/
	public without sharing class CRWrapperFields{
		public EP_ChangeRequestLine__c crlobj {get;set;}
		public boolean isChanged{get;set;}
		public List<SelectOption> restrictedPicklist {get;set;}
		public schema.DisplayType fieldType {get; set;}
		public boolean isfieldRequired {get; set;}
		public boolean isRestrictedPickist {get;set;}
		
		/**
		* @author <Vikas Malik>
		* @name <cRWrapperFields>
		* @date <05/01/2016>
		* @description <Constructor for Wrapper>
		* @version <1.0>
		* @param none
		* @return none
		*/
		public cRWrapperFields(EP_ChangeRequestLine__c  crl){
			crlobj = crl;
			
			isChanged = false;
			isfieldRequired = false;
			restrictedPicklist = new List<SelectOption>();
			
		}
	}
}