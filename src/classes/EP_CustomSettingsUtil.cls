/* 
	@Author        	Accenture
	@name			EP_CustomSettingsUtil
	@CreateDate		03/07/2017
	@Description	This class will contains common utility methods to access and operate the Custom Settings
	@Version		1.0
*/
public with sharing class EP_CustomSettingsUtil {
    /**
	* @Author		Accenture
	* @Name         getOutboundMessageSetting
	* @Date			02/08/2017
	* @Description	This method will be used to get data from EP_CS_OutboundMessageSetting__c custom setting
	* @Param		string
	* @return		EP_CS_OutboundMessageSetting__c
	*/
    public static EP_CS_OutboundMessageSetting__c getOutboundMessageSetting(string messageType) {
        EP_GeneralUtility.Log('Public','EP_CustomSettingsUtil','getOutboundMessageSetting');
        EP_CS_OutboundMessageSetting__c msgSetting = EP_CS_OutboundMessageSetting__c.getValues(messageType);
        return msgSetting;
    }
   /**
	* @Author       Accenture
	* @Name         createIntgStatusByObjAPIName
	* @Date			02/08/2017
	* @Description	This method will be used to get Intgration Status By Object API Name
	* @Param		string
	* @return		map<String,String>
	*/
    public static map<String,String> createIntgStatusByObjAPIName(){
        EP_GeneralUtility.Log('Public','EP_CustomSettingsUtil','createIntgStatusByObjAPIName');
        map<String,String> mObjectNamePickVal = new map<String,String>();
        String strErrMsg;
        for(EP_Integration_Status_Update__c integrationCS : EP_Integration_Status_Update__c.getAll().values()){
            mObjectNamePickVal.put(integrationCS.EP_API_Name__c.toUpperCase() ,integrationCS.Name);
        }
        return mObjectNamePickVal;
    }
    
   /**
	* @Author       Accenture
	* @Name         getOrderStatusFromCustomSetting
	* @Date			02/08/2017
	* @Description 	This method will be use to get status value for order status for NAV from EP_OrderStatusNAVMapping__c custom setting
	* @Param		NA
	* @return		map<String,String>
	*/
    public static Map < String, String > getOrderStatusFromCustomSetting() {
        EP_GeneralUtility.Log('Public','EP_CustomSettingsUtil','getOrderStatusFromCustomSetting');
        map<String, String> csMap = new Map <String, String> ();
        for (EP_OrderStatusNAVMapping__c cs: EP_OrderStatusNAVMapping__c.getall().values()) {
            csMap.put(cs.name, cs.EP_Value__c);
        }
        return csMap;
    }
}