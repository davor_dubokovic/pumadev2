@isTest
/* 
  @Author <Spiros Markantonatos>
  @name <EP_AccountSharingHandlerTest>
  @CreateDate <09/08/2016>
  @Description <This is test class includes the test method for the EP_AccountSharingHandler class>
  @Version <1.1>
  - Version 1.1: Updated the test class to reflect the change of the sharing rule structure from user to portal role
*/
public class EP_AccountSharingHandlerTest {
    
    private static Account testSellToAccount;
    private static Account testShipToAccount;
    private static Account testShipToAccount2;
    private static Contact testSellToContact;
    private static Contact testShipToContact;
    private static User runAsUser;
    private static User testSellToPortalUser;
    private static User testShipToPortalUser;
    
    private static final String PORTAL_SUPER_USER_NAME = 'EP_Portal_Super_User';
    private static final String CASH_PAYMENT_ALTERNATIVE_PAYMENT_METHOD = 'Cash Payment';
    
    private static void createTestData() {
        // Create Sell-To Account
        testSellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        testSellToAccount.EP_Alternative_Payment_Method__c = CASH_PAYMENT_ALTERNATIVE_PAYMENT_METHOD;
        Database.Insert(testSellToAccount);
        
        // Create Ship-To Account
        testShipToAccount = EP_TestDataUtility.createShipToAccount(testSellToAccount.Id, NULL);
        Database.Insert(testShipToAccount);
        
        // Create Contact against Sell-To Account
        testSellToContact = EP_TestDataUtility.createTestRecordsForContact(testSellToAccount);
        
        // Create Contact against Ship-To Account
        testShipToContact = EP_TestDataUtility.createTestRecordsForContact(testShipToAccount);
    }
    
    static testmethod void createSharingRuleForNewPortalUser() {
        List<UserRole> lTestRoles = [SELECT ID FROM UserRole WHERE PortalType = 'None' LIMIT 1];
        System.assertEquals(1, lTestRoles.size());
        
        runAsUser = EP_TestDataUtility.createTestRecordsForUser();
        runAsUser.UserRoleId = lTestRoles[0].Id;
        Database.Update(runAsUser);
        
        Test.startTest();
            System.runAs(runAsUser) {
                List<Profile> ltestSellToPortalUserProfiles = [SELECT ID FROM Profile 
                                                            WHERE Name = :PORTAL_SUPER_USER_NAME LIMIT 1];
                System.assertEquals(1, ltestSellToPortalUserProfiles.size());
                
                createTestData();
                
                // Activate portal access for the Sell-To Contact.
                // This should trigger the manual sharing rule creation
                testSellToPortalUser = EP_TestDataUtility.createPortalUser(ltestSellToPortalUserProfiles[0].Id, testSellToContact.Id);
                Database.Insert(testSellToPortalUser);
                
                // Activate portal access for the Ship-To Contact
                testShipToPortalUser = EP_TestDataUtility.createPortalUser(ltestSellToPortalUserProfiles[0].Id, testShipToContact.Id);
                Database.Insert(testShipToPortalUser);
                
                // Create a new Ship-To for the test Sell-To. The system should add a sharing rule for this Ship-To as well
                testShipToAccount2 = EP_TestDataUtility.createShipToAccount(testSellToAccount.Id, NULL);
                Database.Insert(testShipToAccount2);
                
                // Update the existing portal user. The system should NOT create another sharing rule
                testSellToPortalUser.Phone = '12345';
                Database.Update(testSellToPortalUser);
            }
        Test.stopTest();
        
        // Get the portal role records for the sell-to portal roles
        List<UserRole> lAccountUserRoles = [SELECT Id, PortalAccountId FROM UserRole 
                                                    WHERE PortalAccountId = :testSellToAccount.Id];
        
        // Get the group records for the sell-to portal roles
        List<Group> lSellToPortalUserRoleGroups = [SELECT Id 
                                                    FROM Group 
                                                        WHERE RelatedId IN :lAccountUserRoles];
        
        // Check that there are sharing rules in the system for the Sell-To
        List<AccountShare> accountShareRecords = [SELECT Id, UserOrGroupId 
                                                    FROM AccountShare 
                                                        WHERE AccountId = :testSellToAccount.Id
                                                            AND UserOrGroupId IN :lSellToPortalUserRoleGroups];
        
        // There should be only FOUR sharing rules for the Sell-To Portal Roles:
        // One per Sell-To portal role (3)
        // The 2nd Ship-To does not have a portal role, therefore no sharing rules will be added
        // Plus one for the standard Sell-To portal role
        System.assertEquals(4, accountShareRecords.size());
        
        // There should be SEVEN sharing rules for the Ship-To Portal Roles:
        // One per Sell-To portal role (3)
        // One per Ship-To protal role (3)
        // Plus one for the standard Ship-To portal role
        lAccountUserRoles = [SELECT Id, PortalAccountId FROM UserRole 
                                                    WHERE PortalAccountId = :testShipToAccount.Id];
        
        System.assertEquals(3, lAccountUserRoles.size());
        
        List<Group> lShipToPortalUserRoleGroups = [SELECT Id 
                                                    FROM Group 
                                                        WHERE RelatedId IN :lAccountUserRoles];
        
        accountShareRecords = [SELECT Id, UserOrGroupId 
                                        FROM AccountShare 
                                            WHERE AccountId = :testShipToAccount.Id
                                                AND (UserOrGroupId IN :lShipToPortalUserRoleGroups 
                                                    OR UserOrGroupId IN :lSellToPortalUserRoleGroups)];
        
        System.assertEquals(7, accountShareRecords.size());
        
        // There should be only THREE sharing rules for the Ship-To Portal Roles sharing the Sell-To to the Ship-To users:
        accountShareRecords = [SELECT Id, UserOrGroupId 
                                        FROM AccountShare 
                                            WHERE AccountId = :testShipToAccount2.Id
                                                AND UserOrGroupId != :runAsUser.Id];
        
        List<AccountShare> accountShipToShareRecords = [SELECT Id, UserOrGroupId 
                                                            FROM AccountShare 
                                                                WHERE AccountId = :testShipToAccount2.Id
                                                                    AND UserOrGroupId IN :lSellToPortalUserRoleGroups];
        
        // Ensure that all account sharing rules are related to the Sell-To portal roles
       // System.assertEquals(1, accountShareRecords.size());
    }
    
    static testmethod void testAccountSharingErrorHandling() {
        String strInvalidID = '5002600000BpHkz'; // Random ID
        
        List<String> lAccountIDs = new List<String>();
        lAccountIDs.add(strInvalidID); // Add random account ID that does not exist to raise an error
        List<String> lPortalRoleGroupIDs = new List<String>();
        lPortalRoleGroupIDs.add(strInvalidID);  // Add random group ID that does not exist to raise an error
        List<Boolean> lEditAccountValues = new List<Boolean>();
        lEditAccountValues.add(TRUE);
        List<Boolean> lEditCaseValues = new List<Boolean>();
        lEditCaseValues.add(TRUE);
        
        try {
            EP_AccountSharingHandler.insertManualSharingRecords(lAccountIDs, 
                                                                    lPortalRoleGroupIDs, 
                                                                        lEditAccountValues, 
                                                                            lEditCaseValues);
        } catch(exception e) {
            System.assertEquals(TRUE, e.getMessage().toUpperCase().contains(strInvalidID.toUpperCase()));
        }
        
    }
}