public with sharing class EP_OrderModificationControllerHelper{

    public EP_OrderPageContext ctx;
    public EP_OrderControllerHelper hlp;
    public EP_OrderLinesAndSummaryHelper  ordLineSummaryHlpr;
    public static final String CLASSNAME = 'EP_OrderModificationControllerHelper';

    
    public EP_OrderModificationControllerHelper(EP_OrderPageContext ctx){
       this.ctx = ctx;
       this.hlp = new EP_OrderControllerHelper(ctx);
       this.ordLineSummaryHlpr = new EP_OrderLinesAndSummaryHelper(ctx);
    }

    public Double getTotalOrderCost(){
    	Double dblTotalCost = 0;
    	for (EP_OrderPageContext.OrderWrapper o: ctx.listofOrderWrapper) {
            if (o.oliTotalPrice != NULL) {
                dblTotalCost = dblTotalCost + o.oliTotalPrice;
            }
        }
        return dblTotalCost;
    }

    
    public void loadStep2(){
        EP_GeneralUtility.Log('Private','EP_OrderModificationControllerHelper','loadStep2');
        ctx.listOfSupplyLocationOptions = new List < SelectOption > ();
        ctx.isPackagedOrder = ctx.orderDomainObj.isPackaged();
        //setShipToTank();
        ctx.setshiptodetailsonOrder();
        Account applicableAccount = (ctx.isDeliveryOrder || ctx.isDummy) ? ctx.newOrderRecord.EP_ShipTo__r : ctx.orderAccount;
        ctx.mapOfShipTo = new Map <Id, Account>();
        ctx.mapOfShipTo.put(applicableAccount.Id, applicableAccount);
        EP_AccountService shipToAccountService = new EP_AccountService(new EP_AccountDomainObject(applicableAccount));
        List < EP_Stock_Holding_Location__c > listOfStockHoldingLocations = shipToAccountService.getSupplyLocations(); 
        hlp.populatesupplylocations(listOfStockHoldingLocations);               
        hlp.doActionValidateStockHolidingLocations();
    }


    public void loadStep3(){
        EP_GeneralUtility.Log('Private','EP_OrderModificationControllerHelper','loadStep3');
        ctx.strSelectedPickupLocationID = ctx.newOrderRecord.Stock_Holding_Location__c;
        ctx.selectedPickupDetail = hlp.getStockHolderLocation(ctx.strSelectedPickupLocationID);
        ctx.isSlottingEnabled = ctx.selectedPickupDetail.Stock_Holding_Location__r.EP_Slotting_Enabled__c;
        if(ctx.isDeliveryOrder || ctx.strSelectedDeliveryType.equalsIgnoreCase(EP_Common_Constant.DELIVERY)){
            ctx.transportAccount = new List<SelectOption>();
            ctx.transportAccount.add(EP_Common_Util.createOption(ctx.selectedPickupDetail.EP_Transporter__r.Id,ctx.selectedPickupDetail.EP_Transporter__r.Name));//Defect 43933
            ctx.startDate = EP_PortalOrderUtil.getDeliveryStartDate(ctx.selectedPickupDetail); 
        }else{
            ordLineSummaryHlpr.getAllAvailableDates(ctx.selectedPickupDetail);
        }
        ctx.newOrderRecord.EP_Stock_Holding_Location__c = ctx.selectedPickupDetail.Stock_Holding_Location__c;
        ctx.newOrderRecord.EP_Supply_Location_Name__c =  ctx.selectedPickupDetail.Stock_Holding_Location__r.Name;
        if(ctx.isDeliveryOrder && ctx.transportAccount.size() < 1){
            ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, LABEL.EP_NoTransporterAvailable));   
            ctx.intOrderCreationStepIndex = 3;
            return;                         
        } 
        prepareProductDetails();
        ctx.listofOrderWrapper= new list<EP_OrderPageContext.OrderWrapper>();
        // ctx.isConsignmentOrder = ((EP_PortalOrderUtil.identifyConsignmentOrder(ctx.strSelectedShipToID, ctx.strSelectedDeliveryType)) && !ctx.newOrderRecord.EP_Order_Category__c.equalsIgnoreCase(EP_Common_Constant.CONSUMPTION_ORDER)); // defect fix 26530
        ctx.isConsignmentOrder  = ctx.orderDomainObj.isConsignment();
        ctx.itemIdTotalPriceMap = calculateTotalPrice();//Defect 57495
        ctx.loadExistingOrderItems();
        ctx.availableRoutes = ordLineSummaryHlpr.setRunAndRoute(ctx.strSelectedShipToID,ctx.newOrderRecord.EP_Stock_Holding_Location__c);
        ctx.setRun();
        ordLineSummaryHlpr.checkForNoTransporter(ctx.newOrderRecord,ctx.selectedPickupDetail.EP_Transporter__r.Name);  
        ordLineSummaryHlpr.setOrderRecordType();                     
    }

    
    public void loadStep4(){
        EP_GeneralUtility.Log('Private','EP_OrderModificationControllerHelper','loadStep4');
        hlp.setOrderDetails();
    }

    /*
This method is used to get the product details and validate quantities for the retrieved products.
*/
    @TestVisible
    private void prepareProductDetails() {
        EP_GeneralUtility.Log('Private','EP_OrderModificationControllerHelper','prepareProductDetails');
        ctx.strLoadedPickupLocationID = ctx.strSelectedPickupLocationID;
        ctx.strLoadedDeliveryType = ctx.strSelectedDeliveryType;
        ordLineSummaryHlpr.setProductDetails();
        ordLineSummaryHlpr.setQuantityRestrictions();
    }

     //Defect #58835 Start
    public void doCancel(){
        EP_GeneralUtility.Log('public','EP_OrderModificationControllerHelper','doCancel');
        csord__Order_Line_Item__c oItem;
        List<csord__Order_Line_Item__c> orderItemList = new List<csord__Order_Line_Item__c>();
     //   List<OrderItem> recToUpdate = new List<OrderItem>();
        List<csord__Order_Line_Item__c> recToDelete = new EP_OrderDomainObject(ctx.newOrderRecord.Id).getOrder().csord__Order_Line_Items__r;
        for(csord__Order_Line_Item__c oldItem : ctx.oldOrderRecord.csord__Order_Line_Items__r){
            oItem = new csord__Order_Line_Item__c();
            oItem = oldItem;
            oItem.Id = null;
            orderItemList.add(oItem);
        }
        if(!recToDelete.isEmpty()){
            delete recToDelete;
        }
        if( !ctx.oldOrderRecord.csord__Order_Line_Items__r.isEmpty() ){
            upsert orderItemList;//ctx.oldOrderRecord.OrderItems;
        }
        update ctx.oldOrderRecord;
    }
    //Defect #58835 End

    public void loadExistingProductSet(){
        EP_GeneralUtility.Log('Private','EP_OrderModificationControllerHelper','loadExistingProductSet');
        Set<String> sOrderExtraItems = new Set<String>{EP_Common_Constant.TAX, EP_Common_Constant.DISCOUNT, EP_Common_Constant.FUEL_PRICE, EP_Common_Constant.FREIGHT}; // #defectFix 27933 
        for( csord__Order_Line_Item__c ot : ctx.newOrderRecord.csord__Order_Line_Items__r){
            /*if(!sOrderExtraItems.contains(ot.PriceBookEntry.Product2.Name)){
                ctx.existingOrderProductSet.add(ot.PriceBookEntry.Product2.Id);
            }*/
        }
    }

     /*
    Calculate total price
    */
    @TestVisible
    //Defect 57495 Start
    public Map<Id,Double> calculateTotalPrice(){
        EP_GeneralUtility.Log('Private','EP_OrderModificationControllerHelper','calculateTotalPrice');
        Double totalPrice;
        Map<Id,Double> itemIdTotalPriceMap = new Map<Id,Double>();
        system.debug('!!!!'+ctx.newOrderRecord.csord__Order_Line_Items__r); 
        for( csord__Order_Line_Item__c oItem : ctx.newOrderRecord.csord__Order_Line_Items__r ){
          if( !(oItem.EP_Is_Standard__c) && itemIdTotalPriceMap.containsKey(oItem.EP_Parent_Order_Line_Item__c)){
            totalPrice = itemIdTotalPriceMap.get(oItem.EP_Parent_Order_Line_Item__c)+oItem.EP_Total_Price__c;
            itemIdTotalPriceMap.put( oItem.EP_Parent_Order_Line_Item__c,totalPrice );
          }else{
            totalPrice = 0;
            itemIdTotalPriceMap.put( oItem.Id,totalPrice );
          }
        }
        system.debug('!!!!'+itemIdTotalPriceMap);    
        return itemIdTotalPriceMap;
        //Defect 57495 End
    }

    @TestVisible
    private void setOrderWrapperAttributes(EP_OrderPageContext.OrderWrapper orWrapper){
        orWrapper.oliProductName = ctx.productNameMap.get(orWrapper.oliPricebookEntryID);
        orWrapper.oliQuantityRange = EP_Common_Constant.NA;
        orWrapper.oliIndex = ctx.intOrderLineIndex;
        //if (itemIdTotalPriceMap.containsKey(orWrapper.orderLineItem.Id)) {
        //    orWrapper.oliTotalPrice = itemIdTotalPriceMap.get(orWrapper.orderLineItem.Id);
        //}
        ctx.intOrderLineIndex++;
        // Update the order line item
        if(orWrapper.orderLineItem.Id == NULL ){
            orWrapper.orderLineItem.PriceBookEntryId__c = orWrapper.oliPricebookEntryID;
            orWrapper.orderLineItem.OrderId__c = ctx.newOrderRecord.Id;
        }
        
        orWrapper.orderLineItem.EP_Is_Standard__c = true;
        orWrapper.orderLineItem.EP_Pricing_Total_Amount__c = 0;
        orWrapper.orderLineItem.UnitPrice__c = ctx.productPriceMap.get(orWrapper.orderLineItem.PriceBookEntryId__c);
        orWrapper.orderLineItem.EP_Order_Line_Relationship_Index__c = orWrapper.oliIndex;
        orWrapper.orderLineItem.Quantity__c = orWrapper.oliQuantity;
        System.debug('orWrapper.oliQuantity:---'+orWrapper.oliQuantity);
        // Reset the UoM
        orWrapper.orderLineItem.EP_Quantity_UOM__c = EP_Common_Constant.BLANK;
        orWrapper.oliProductUoM = NULL;
        
        // Reset the additional charges list
        orWrapper.listChildOrderLineItems = new List < csord__Order_Line_Item__c > ();
        
        if(ctx.childItemsMap.containsKey( orWrapper.orderLineItem.Id )){
            orWrapper.listChildOrderLineItems.addAll(ctx.childItemsMap.get(orWrapper.orderLineItem.Id));    
        }
    }

    public void displayProductNameAndSafeFillLevel() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationControllerHelper','displayProductNameAndSafeFillLevel');
        
        List<OrderItem> itemsToDelete = new List<OrderItem>();
        EP_OrderPageContext.OrderWrapper oWrapper;
        
        ctx.isPriceCalculated = false;
        for (EP_OrderPageContext.OrderWrapper orWrapper: ctx.listofOrderWrapper) {
            updatePricebookEntryId(orWrapper);
            setTanksOrderDetails(orWrapper);
             
        }            
    }
    @TestVisible
    private void setTanksOrderDetails(EP_OrderPageContext.OrderWrapper orWrapper){
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','setTanksOrderDetails');
        String strProductID;
        ctx.settankdetailsonOrder(orWrapper);
        strProductID = ctx.productPriceBookEntryMap.get(orWrapper.oliPricebookEntryID);                    
        ctx.setOperationTankdetails(orWrapper);
        // Product name
        orWrapper.oliProductName = ctx.productNameMap.get(orWrapper.oliPricebookEntryID);
        // Set the index of the OLI
        orWrapper.oliIndex = ctx.intOrderLineIndex;
        ctx.intOrderLineIndex++;
    }
    @TestVisible
    private void updatePricebookEntryId(EP_OrderPageContext.OrderWrapper orWrapper){
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','setTanksOrderDetails');
        List<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper = new List<EP_OrderPageContext.OrderWrapper>();
        csord__Order_Line_Item__c OrderItemObj = new csord__Order_Line_Item__c();
        if( orWrapper.oliPricebookEntryID == orWrapper.orderLineItem.PriceBookEntryId__c || orWrapper.orderLineItem.PriceBookEntryId__c == null){
            OrderItemObj = orWrapper.orderLineItem;
            updatedOrderWrapper.add(orWrapper);
        }

        if( OrderItemObj.Id == NULL ){
            OrderItemObj.PriceBookEntryId__c = orWrapper.oliPricebookEntryID;
            setPriceBookEntryIdByTankID(orWrapper,OrderItemObj);
            OrderItemObj.orderId__c = ctx.newOrderRecord.Id;
        }
    }
    @TestVisible
    private void setPriceBookEntryIdByTankID(EP_OrderPageContext.OrderWrapper orWrapper, csord__Order_Line_Item__c OrderItemObj){
        if(orWrapper.oliTanksID != null){                           
            OrderItemObj.PriceBookEntryId__c = ctx.PriceBookEntryproductMap.get(ctx.mapShipToOperationalTanks.get(orWrapper.oliTanksID).EP_Product__C);
        }
    }

     /*
    This method is used to perform validations on order line item.
    */
    public void updateOrderItemLines() {
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','updateOrderItemLines');
        List < csord__Order_Line_Item__c > listValidatedOrderLines = new List < csord__Order_Line_Item__c > ();
        List<csord__Order_Line_Item__c> listItemsToDelete = new List<csord__Order_Line_Item__c>();
        String strProductID;
        String strKey;
        EP_OrderPageContext.OrderWrapper oWrapper;
        List<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper = new List<EP_OrderPageContext.OrderWrapper>();
        dateChange();
        
        if(isDeliveryDateChanged()){
            ctx.isPriceCalculated = false;
        }
        validatePrepaymentCustomer();
        if (isDeliveryDateMissing()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.EP_Please_Select_Date_Label));
        }
        for (EP_OrderPageContext.OrderWrapper orWrapper: ctx.listofOrderWrapper) {
            ordLineSummaryHlpr.setDeleteListAndUpdatedOrderWrapper(oWrapper,orWrapper.orderLineItem,orWrapper,listItemsToDelete,updatedOrderWrapper);
            ordLineSummaryHlpr.setTankDetailsofOrderWrapper(orWrapper);
            //strProductID = ctx.productPriceBookEntryMap.get(orWrapper.oliPricebookEntryID);
            setOrderWrapperAttributes(orWrapper);
            // Retrieve UoM information
            ordLineSummaryHlpr.setProductQuantity(orWrapper.orderLineItem,orWrapper);
            ordLineSummaryHlpr.setOrderWrapperAndValidateOrderLines(orWrapper.orderLineItem,oWrapper,orWrapper,listValidatedOrderLines);
            deletelineitems(listItemsToDelete);
            updateContextWrapper(updatedOrderWrapper,orWrapper,oWrapper);
            populateProductInventory(listValidatedOrderLines);
            validateinventory(orWrapper);
               
        } 
    }
    @TestVisible
    private boolean isDeliveryDateChanged(){
        return ctx.newOrderRecord.EP_Requested_Delivery_Date__c <> ctx.oldOrderRecord.EP_Requested_Delivery_Date__c;
    }
    @TestVisible
    private boolean isDeliveryDateMissing(){
        return (ctx.newOrderRecord.EP_Requested_Delivery_Date__c == NULL && ctx.isDeliveryOrder);
    }
    @TestVisible
    private void validatePrepaymentCustomer(){
        if(ctx.isPrepaymentCustomer){
            Boolean showPrepayCustomerPOBlankError = EP_PortalOrderUtil.checkPrepayForPoNumber(ctx.newOrderRecord,ctx.isPrepaymentCustomer,ctx.isConsignmentOrder,false,ctx.isDummy,ctx.orderAccount); // #customerPoWork
            if(showPrepayCustomerPOBlankError){
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, LABEL.EP_ProvideCustomerPurchaseOrderNumber));
            }
        }
    }
    @TestVisible
    private void validateinventory(EP_OrderPageContext.OrderWrapper orWrapper){
        if(!ctx.isRoOrder && !ctx.isErrorInPage) {
            ordLineSummaryHlpr.validateInventory(ctx.listofOrderWrapper,orWrapper.orderLineItem);
        } 
    }
    @TestVisible
    private void deletelineitems(List<csord__Order_Line_Item__c> listItemsToDelete){
        if(!listItemsToDelete.isEmpty()){
            delete listItemsToDelete;
        }
    }
    @TestVisible
    private void updateContextWrapper(List<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper,EP_OrderPageContext.OrderWrapper orWrapper, EP_OrderPageContext.OrderWrapper oWrapper){
        if (ctx.isConsignmentOrder || ctx.isDummy) {
            orWrapper.orderLineItem.UnitPrice__c = 0;
        }
        if(oWrapper != NULL){
            oWrapper.orderLineItem = orWrapper.orderLineItem;
            updatedOrderWrapper.add(oWrapper);            
        }
        ctx.listofOrderWrapper= new List<EP_OrderPageContext.OrderWrapper>();
        ctx.listofOrderWrapper= updatedOrderWrapper;
        

    }
    @TestVisible
    private void populateProductInventory(List < csord__Order_Line_Item__c > listValidatedOrderLines){
        if (!listValidatedOrderLines.isEmpty()) {
                Boolean isAvailable = false;
                ctx.productInventoryMap = EP_PortalOrderUtil.getInventoryMap(listValidatedOrderLines, ctx.newOrderRecord, ctx.strSelectedPickupLocationID);
            }

    }
    public void dateChange(){
        EP_GeneralUtility.Log('Public','EP_OrderModificationPageController','dateChange');
        ctx.newOrderRecord.EP_Requested_Delivery_Date__c = EP_GeneralUtility.convertSelectedDateToDateInstance(ctx.strSelectedDate);
            ctx.newOrderRecord.EP_Requested_Pickup_Time__c = String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(ctx.availableSlots)).subString(0,8);         
    }

}