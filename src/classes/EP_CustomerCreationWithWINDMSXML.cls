/* 
  @Author : Accenture
  @name : EP_GenerateCreditHoldingXMLWithNav
  @CreateDate :13/Nov/2017
  @Description :This class is use to created the  Account Creatation XML for Nav
  @ Novasuite Fixes -- hardcoding removed
  @Version : 1.0
*/
public with sharing class EP_CustomerCreationWithWINDMSXML extends EP_AccountRequestXML {
    public string TobeReplace ='Test'; 
    public list<Account> lstAccount ;
	/*Defect 78513 start*/
    Map<id,List<EP_Tank__c>> shipToAndTankMap = new Map<id,List<EP_Tank__c>>();
     /*Defect 78513 end*/
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             13/Nov/2017
    * @description      This method is used to create Payload for Account to Sync with WINDMS
    * @param            NA
    * @return           NA
    */  
    public override void createPayload(){ 
        EP_GeneralUtility.Log('Public','EP_CustomerCreationWithWINDMSXML','createPayload');
        Dom.XMLNode payloadNode = MSGNode.addChildElement(EP_Common_Constant.PAYLOAD_TAG ,null,null);
        Dom.XmlNode anyNode = payloadNode.addChildElement(EP_Common_Constant.ANYZERO_NODE ,null, null);
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode shipToAddressesNode = tempDoc.createRootElement('shipToAddresses',null, null);          
        bindAccountRecordsInXML(shipToAddressesNode);  
        // Encoding payload by calling encode XML method in superclass
        AnyNode.addTextNode(encodeXML(tempDoc));        
        system.debug('KKK::-->'+doc.toXmlString());
    }
    
    /**
    * @author           Accenture
    * @name             init
    * @date             14/Nov/2017
    * @description      retrive the Account record to bind the values in XML
    * @param            NA
    * @return           NA
    */
    public  override void init(){
        EP_GeneralUtility.Log('Public','EP_AccountRequestXML','init');
        MSGNode = doc.createRootElement(EP_Common_Constant.MSG, null, null);
        EP_AccountMapper objAccMap = new EP_AccountMapper();
		/*Defect 78513 start*/
        EP_TankMapper TankMapper = new EP_TankMapper();
        /*Defect 78513 end*/		
        objAccount = objAccMap.getAccountRecord(recordId);
        lstAccount = new list<account>();  
        List <String>shipTOAdressTypes = new List<String>();
        //BUG-FIX-80805-START
        shipTOAdressTypes.add(EP_Common_Constant.VMI_SHIP_TO_DEV_NAME);
        shipTOAdressTypes.add(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME);
        lstAccount = objAccMap.getAccountWithShipToTanksById(objAccount.Id,shipTOAdressTypes,EP_Accountconstant.PROSPECT);
        /*Defect 78513 start*/
        shipToAndTankMap = TankMapper.getTanksRecordsByShipToIds(lstAccount);
        /*Defect 78513 end*/
        //BUG-FIX-80805-END
        super.setEncryption();
    }
    
    /**
    * @author           Accenture
    * @name             bindAccountRecordsInXML
    * @date             14/Nov/2017
    * @description      create the xml ralated to Accounts
    * @param            Dom.XMLNode ,list<Account>
    * @return           NA
    */  
    @testvisible 
    private void bindAccountRecordsInXML(Dom.XMLNode shipToAddressesNode){      
        For(Account objShipToAccount :lstAccount){   
            Dom.XMLNode shipToAddressNode = shipToAddressesNode.addChildElement(EP_AccountConstant.SHIPTOADDRESS,null,null);    
            String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objShipToAccount.id); 
            shipToAddressNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid)); 
            
            Dom.XMLNode IdentifierNode = shipToAddressNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
            IdentifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Account_Id__c));
            string custId = objShipToAccount.ParentId==null?objShipToAccount.EP_Account_Id__c:objShipToAccount.Parent.EP_Account_Id__c ;
            IdentifierNode.addChildElement(EP_AccountConstant.CUSTID,null,null).addTextNode(getValueforNode(custId));
            IdentifierNode.addChildElement(EP_AccountConstant.ENTRPRSID,null,null).addTextNode(getValueForNode(objShipToAccount.EP_EnterpriseId__c)); 
            
            shipToAddressNode.addChildElement(EP_AccountConstant.NAME,null,null).addTextNode(getValueforNode(objShipToAccount.Name));
            shipToAddressNode.addChildElement(EP_AccountConstant.NAME2,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Account_Name_2__c));
            shipToAddressNode.addChildElement(EP_AccountConstant.CNTRYCODE,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Country_Code__c));
            shipToAddressNode.addChildElement(EP_AccountConstant.LANGCODE,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Language_Code__c));
            string address = objShipToAccount.ParentId!=null?objShipToAccount.ShippingStreet:objShipToAccount.BillingStreet;
            shipToAddressNode.addChildElement(EP_AccountConstant.ADDRESS,null,null).addTextNode(getValueforNode(address));
            string strPostCode =  objShipToAccount.ParentId!=null?objShipToAccount.ShippingPostalCode:objShipToAccount.BillingPostalCode;
            shipToAddressNode.addChildElement(EP_AccountConstant.POSTCODE,null,null).addTextNode(getValueforNode(strPostCode)); 
            string strCity = objShipToAccount.ParentId!=null?objShipToAccount.ShippingCity:objShipToAccount.BillingCity;
            shipToAddressNode.addChildElement(EP_AccountConstant.CITY,null,null).addTextNode(getValueforNode(strCity));
            shipToAddressNode.addChildElement(EP_AccountConstant.PHONE,null,null).addTextNode(getValueforNode(objShipToAccount.Phone));
            shipToAddressNode.addChildElement(EP_AccountConstant.FAX,null,null).addTextNode(getValueforNode(objShipToAccount.Fax));
            shipToAddressNode.addChildElement(EP_AccountConstant.EMAIL,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Email__c));
            shipToAddressNode.addChildElement(EP_AccountConstant.SHIPTOTYPE,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Ship_To_Type__c)); 
            //code changes for L4 #45362 Start
            //shipToAddressNode.addChildElement(EP_AccountConstant.STATUS,null,null).addTextNode(getValueforNode(getShipToStatus(objShipToAccount.EP_Status__c)));
            shipToAddressNode.addChildElement(EP_AccountConstant.STATUS,null,null).addTextNode(getValueforNode(objShipToAccount.EP_ShipTo_Status_WINDMS__c)); 
            //code changes for L4 #45362 End
            shipToAddressNode.addChildElement(EP_AccountConstant.ISVMI,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Is_VMI_ShipTo__c));
            shipToAddressNode.addChildElement(EP_AccountConstant.DLVRYTYPE,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Delivery_Type__c));
            shipToAddressNode.addChildElement(EP_AccountConstant.MODIFYON,null,null).addTextNode(formatDateAsString((objShipToAccount.LastModifiedDate)));
            shipToAddressNode.addChildElement(EP_AccountConstant.MODIFYBY,null,null).addTextNode(getValueforNode(objShipToAccount.LastModifiedBy.Alias));
            //SIT-Bug-74051
            shipToAddressNode.addChildElement(EP_AccountConstant.XPOS,null,null).addTextNode(getValueforNode(getGeoCordinateValue(objShipToAccount.EP_Position__longitude__s)));
			shipToAddressNode.addChildElement(EP_AccountConstant.YPOS,null,null).addTextNode(getValueforNode(getGeoCordinateValue(objShipToAccount.EP_Position__latitude__s)));
            //SIT-Bug-74051
            //for data versioning
            shipToAddressNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
			/*Defect 78513 start*/
            if(shipToAndTankMap!=null && shipToAndTankMap.containsKey(objShipToAccount.id) && shipToAndTankMap.get(objShipToAccount.id)!=null)
            bindTankRecordsInXML(shipToAddressNode,shipToAndTankMap.get(objShipToAccount.id));
            /*Defect 78513 end*/
            
            bindSupplyLocationRecordsInXML(shipToAddressNode,objShipToAccount.Stock_Holding_Locations1__r);
            
            shipToAddressNode.addChildElement(EP_AccountConstant.CLIENTID,null,null).addTextNode(getValueforNode(objShipToAccount.EP_Puma_Company_Code__c));
        }
    }
    
    /**
    * @author           Accenture
    * @name             bindTankRecordsInXML
    * @date             14/Nov/2017
    * @description      create the xml ralated to Tanks
    * @param            Dom.XMLNode ,list<EP_Tank__c>
    * @return           NA
    */ 
    @testvisible 
    private void bindTankRecordsInXML(Dom.XMLNode shipToAddressNode, list<EP_Tank__c> lstTanks){
        Dom.XMLNode tanksNode = shipToAddressNode.addChildElement(EP_AccountConstant.TANKS,null,null);
        For(EP_Tank__c objTank :lstTanks){  
            Dom.XMLNode tankNode = tanksNode.addChildElement(EP_AccountConstant.TANK,null,null);        
            String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objTank.id); 
            tankNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid));
            Dom.XMLNode tankIdentifierNode = tankNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
            tankIdentifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(objTank.EP_Ship_To_Nr__c));
            tankIdentifierNode.addChildElement(EP_AccountConstant.TANKID,null,null).addTextNode(getValueforNode(objTank.EP_Tank_Code__c));
            //76105-Sending Tank Alias
            tankNode.addChildElement(EP_AccountConstant.TANKALIAS,null,null).addTextNode(getValueforNode(objTank.EP_Tank_Alias__c));
            tankNode.addChildElement(EP_AccountConstant.UOM,null,null).addTextNode(getValueforNode(objTank.EP_UnitOfMeasure__c));
            tankNode.addChildElement(EP_AccountConstant.PRODUCTID,null,null).addTextNode(getValueforNode(objTank.EP_Product_Code__c));
            tankNode.addChildElement(EP_AccountConstant.CAPACITY,null,null).addTextNode(getValueforNode(objTank.EP_Capacity__c));
            tankNode.addChildElement(EP_AccountConstant.SAFEFILLLEVEL,null,null).addTextNode(getValueforNode(objTank.EP_Safe_Fill_Level__c));
            tankNode.addChildElement(EP_AccountConstant.DEADSTOCK,null,null).addTextNode(getValueforNode(objTank.EP_Deadstock__c));
            tankNode.addChildElement(EP_AccountConstant.TANKSTATUS,null,null).addTextNode(getValueforNode(objTank.EP_TANK_STATUS_TO_LS__c));
            tankNode.addChildElement(EP_AccountConstant.TANKDIPENTRYMODE,null,null).addTextNode(getValueforNode(objTank.EP_Tank_Dip_Entry_Mode__c)); 
            tankNode.addChildElement(EP_AccountConstant.MODIFYON,null,null).addTextNode(formatDateAsString(objTank.LastModifiedDate));
            tankNode.addChildElement(EP_AccountConstant.MODIFYBY,null,null).addTextNode(getValueforNode(objTank.EP_Last_Modified_By_Name__c)); 
            tankNode.addChildElement(EP_AccountConstant.ACTIVEFROM,null,null); 
            tankNode.addChildElement(EP_AccountConstant.ACTIVETO,null,null);
            tankNode.addChildElement(EP_AccountConstant.AVAILABLESTOCKLASTUPDATED,null,null);
            //for data versioning
            tankNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
        }
    }
    
     /**
    * @author           Accenture
    * @name             bindSupplyLocationRecordsInXML
    * @date             14/Nov/2017
    * @description      create the xml ralated to Supply locations
    * @param            Dom.XMLNode ,list<EP_Stock_Holding_Location__c>
    * @return           NA
    */ 
    @testvisible
    private void bindSupplyLocationRecordsInXML(Dom.XMLNode shipToAddressNode, list<EP_Stock_Holding_Location__c> lstStockHldingLocations){
        Dom.XMLNode supplyLocationsNode = shipToAddressNode.addChildElement(EP_AccountConstant.SUPPLYLOCATIONS,null,null);
        For(EP_Stock_Holding_Location__c objSHLocations :lstStockHldingLocations){    
            Dom.XMLNode supplyLocNode = supplyLocationsNode.addChildElement(EP_AccountConstant.SUPPLYLOCATION,null,null);   
            String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objSHLocations.id);  
            supplyLocNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid)); 
            Dom.XMLNode supplyLocIdentifierNode = supplyLocNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
            supplyLocIdentifierNode.addChildElement(EP_AccountConstant.STOCKHLDNGLOCID,null,null).addTextNode(getValueforNode(objSHLocations.EP_Stock_Location_Id__c));
            supplyLocIdentifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(objSHLocations.EP_Ship_To_Id__c));
            
            supplyLocNode.addChildElement(EP_AccountConstant.DISTANCE,null,null).addTextNode(getValueforNode(objSHLocations.EP_Distance__c));
            supplyLocNode.addChildElement(EP_AccountConstant.UOM,null,null).addTextNode(getValueforNode(objSHLocations.EP_UOM__c));
            supplyLocNode.addChildElement(EP_AccountConstant.DEFAULTSTR,null,null).addTextNode(getValueforNode(objSHLocations.EP_Default__c));
            supplyLocNode.addChildElement(EP_AccountConstant.MODIFYON,null,null).addTextNode(formatDateAsString(objSHLocations.LastModifiedDate)); 
            supplyLocNode.addChildElement(EP_AccountConstant.MODIFYBY,null,null).addTextNode(getValueforNode(objSHLocations.EP_Last_Modified_By_Name__c));
            //for data versioning
            supplyLocNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
        }
    }
    
    /**
    * @author           Accenture
    * @name             getShipToStatus
    * @date             14/Nov/2017
    * @description      Sets the shipToStatus based on the account status
    * @param            string 
    * @return           string
    */ 
    @testvisible
    public string getShipToStatus(string strStatus) {
          string shipToStatusValue = (EP_Common_Constant.STATUS_SET_UP.equalsIgnoreCase(String.valueOf(strStatus))
           || EP_Common_Constant.STATUS_ACTIVE.equalsIgnoreCase(String.valueOf(strStatus))
           ) ? EP_Common_Constant.STRING_YES_LS:EP_Common_Constant.STRING_NO_LS;
          return shipToStatusValue ; 
    }
    
    @testvisible
	private Decimal getGeoCordinateValue(Decimal cordinate){
        system.debug('**cordinate**' + cordinate);
        return (cordinate == null)?0.00000:cordinate;
    }
}