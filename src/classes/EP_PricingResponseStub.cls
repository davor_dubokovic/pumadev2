/* 
   @Author <Accenture>
   @name <EP_PricingResponseStub>
   @CreateDate <03/03/2017>
   @Description <These are the stub classes to map JSON string for Pricing Response Rest Service Request from NAV> 
   @Version <1.0>
   */
   public class EP_PricingResponseStub {
   	/* Class for MSG */
   	//public MSG MSG;
   	public Payload Payload;

   	/* Stub Class for MSG */
   	//public class MSG {
   	//		public Payload Payload;
    //		public String StatusPayload;
    //		public EP_MessageHeader HeaderCommon;
   	//}

   	/* Stub Class for Payload */
   	public class Payload {
   		public Any0 any0;
   	}

   	/* Stub Class for Any0 */
   	public class Any0 {
   		public PricingResponse pricingResponse;
   	}

   	/* Stub Class for PricingResponse */
   	public class PricingResponse {
   		public PriceDetails priceDetails;
   	}

   	/* Stub Class for PriceDetails */
   	public class PriceDetails {
   		public String seqId;
   		public String priceRequestSource;
   		public String companyCode;
   		public String priceType;
   		public String deliveryType;
         public String currencyCode;
   		public String priceDate;
   		public String applyOrderQuantity;
   		public String totalOrderQuantity;
   		public String customerId;
   		public String shipToId;
   		public String supplyLocationId;
   		public String transporterId;
   		public String onRun;
         public PriceDetails priceDetails;
   		public PriceLineItems priceLineItems;
   		public Error Error;
   	}

   	/* Stub Class for PriceLineItems */
   	public class PriceLineItems {
   		public String orderId;
		public List<LineItem> lineItem;
   	}

   	/* Stub Class for LineItem */
   	public class LineItem {
   		public LineItemInfo lineItemInfo;
   		public AccountingDetails accountingDetails;
   	}

   	/* Stub Class for LineItemInfo */
   	public class LineItemInfo {
   		public OrderItem orderLineItem;
   		public String lineItemId;
   		public String itemId;
   		public String quantity;
   		public String unitPrice;
         public InvoiceDetails invoiceDetails;
         public AcctComponents acctComponents;
	}
	
	/* Stub Class for InvoiceDetails */
	public class InvoiceDetails {
		//public InvoiceComponents invoiceComponents;
      public List<InvoiceComponent> invoiceComponent;
	}
	
	/* Stub Class for PriceLineItems */
	public class InvoiceComponents {
		public List<InvoiceComponent> invoiceComponent;
	}
	
	/* Stub Class for InvoiceComponent */
	public class InvoiceComponent {
		public OrderItem invoiceItem;
      public String seqId;
		public String type;
		public String name;
		public String amount;
      public String amountLCY;
		public String taxPercentage;
		public String taxAmount;
      public String taxAmountLCY;
		public String totalAmount;
      public String totalAmountLCY;
	}
	
	/* Stub Class for AccountingDetails */
	public class AccountingDetails {
		public AcctComponents acctComponents;
	}
	
	/* Stub Class for AcctComponent */
	public class AcctComponent {
      public String seqId;
		public String componentCode;
		public String glAccountNr;
		public String baseAmount;
      public String baseAmountLCY;
		public String taxRate;
		public String taxAmount;
      public String taxAmountLCY;
		public String totalAmount;
      public String totalAmountLCY;
		public String isVAT;
	}
	
	/* Stub Class for AcctComponents */
	public class AcctComponents {
		public List<AcctComponent> acctComponent;
	}
	
	/* Stub Class for Error */
	public class Error {
		public String ErrorCode;
		public String ErrorDescription;
	}
 }