/* 
  @Author <Cihan Sunar>
   @name <EP_FE_ProductListResponse>
   @CreateDate <25/04/2016>
   @Description <This class >  
   @Version <1.0>
*/
global with sharing class EP_FE_ProductListResponse extends EP_FE_Response {

	/* 
	  @Author <Cihan Sunar>
	   @name <EP_FE_ProductListResponse>
	   @CreateDate <25/04/2016>
	   @Description <This class >  
	   @Version <1.0>
	*/
    public EP_FE_ProductListResponse () {
       
        productList  = new List<Product2>();
    }

    public List<Product2> productList  {get;set;}   
}