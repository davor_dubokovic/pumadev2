/**
   @Author Varsha Patil
   @name <EP_OrderFieldsetSfdcNavMapper_UT>
   @CreateDate 19/1/2017
   @Description This class will be use to test/cover unit test cases for EP_OrderFieldsetSfdcNavMapper class
   @Version <1.0>
   @reference <Referenced program names>
*/
@isTest
private class EP_OrderFieldsetSfdcNavMapper_UT {
    private static final Integer COUNT = 1;
    /**
       *  This method will used to get OrderFieldsetSfdcNavRec records.
       *  @name <getOrderFieldsetSfdcNavRec_posiTest>
       *  @param NA
       *  @return NA
       *  @throws NA
    */
    Private static testMethod void getOrderFieldsetSfdcNavRec_posiTest(){
     // prepare data for custom setting EP_Order_Fieldset_Sfdc_Nav_Intg
     List<EP_Order_Fieldset_Sfdc_Nav_Intg__c> OrderFieldsetSfdcNavList = new List<EP_Order_Fieldset_Sfdc_Nav_Intg__c> ();
        for(Integer i = 0; i < COUNT; i++){
            EP_Order_Fieldset_Sfdc_Nav_Intg__c OrderNav = new EP_Order_Fieldset_Sfdc_Nav_Intg__c();
            OrderNav.Name='Delivery Date';
            OrderNav.Field_API_Name__c='EP_Delivery_Date__c';
            OrderNav.Object_Name__c = 'Order';
            OrderNav.Order_sequence__c= 8;            
            OrderFieldsetSfdcNavList.add(OrderNav);
        }
        insert OrderFieldsetSfdcNavList; 
        
    System.runAs(new User(Id=userinfo.getUserId())){
    test.startTest();
    //integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
    EP_OrderFieldsetSfdcNavMapper OrderMapper=new EP_OrderFieldsetSfdcNavMapper();
    List<EP_Order_Fieldset_Sfdc_Nav_Intg__c> returnOrderCSList= OrderMapper.getOrderFieldsetSfdcNavRec(1);
    test.stopTest();
   	System.debug('*****'+returnOrderCSList.size());
   	System.assertEquals(OrderFieldsetSfdcNavList.size(),  returnOrderCSList.size());
    }
  }
    /**
       *  This method will test Negative scenario to test if Integer value is Zero.
       *  @name <getOrderFieldsetSfdcNavRec_posiTest>
       *  @param NA
       *  @return NA
       *  @throws NA
    */
    /*
    Private static testMethod void getOrderFieldsetSfdcNavRec_NegTest(){
        
    }*/
}