@isTest
public class EP_CreditExceptionPayloadXML_UT {
	@testSetup static void init() {
		List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
      	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
      	List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
      	List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    	
    }
    static testMethod void init_testPositive() {
    	EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        EP_CreditExceptionPayloadXML objLocal = new EP_CreditExceptionPayloadXML();
        objLocal.recordId = creditExp.id;
        Test.startTest();
        objLocal.init();
        Test.stopTest();        
        System.assertNotEquals(null,objLocal.credExpObj);         
    }
	static testMethod void createPayload_testPositive() {
    	EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        EP_CreditExceptionPayloadXML objLocal = new EP_CreditExceptionPayloadXML();
        objLocal.recordId =creditExp.Id;
        Test.startTest();
        objLocal.init();
        objLocal.isEncryptionEnabled =true;
        objLocal.createPayload();
        Test.stopTest();         
        System.assert(objLocal.MSGNode.getChildElements().size()>0);         
    }
	static testMethod void createStatusPayLoad_testPositive() {
        EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
        EP_CreditExceptionPayloadXML objLocal = new EP_CreditExceptionPayloadXML();
        objLocal.recordId = creditExp.id;
        Test.startTest();
        objLocal.init();
        objLocal.isEncryptionEnabled =true;
        objLocal.createPayload();
        objLocal.createStatusPayLoad();
        Test.stopTest();         
        System.assert(objLocal.MSGNode.getChildElements().size()>0);      
    }

}