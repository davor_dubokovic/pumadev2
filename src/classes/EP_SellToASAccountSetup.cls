/*
 *  @Author <Accenture>
 *  @Name <EP_SellToASAccountSetup>
 *  @CreateDate <20/02/2017>
 *  @Description <State Class for Sell To Account Set up  Status>
 *  @Version <1.0>
 */
 public class EP_SellToASAccountSetup extends EP_AccountState{
    public EP_SellToASAccountSetup() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount)
    {
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_SellToASAccountSetup','doOnEntry');
        EP_AccountService service = new EP_AccountService(this.account);
        if(this.account.isStatuschanged()){
        	service.doActionSendCreateRequestToNavAndLomo();
        }
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_SellToASAccountSetup','doOnExit');
        
    }

    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_SellToASAccountSetup','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_SellToASAccountSetup','isInboundTransitionPossible');
        return super.isInboundTransitionPossible();
    }
}