@isTest
public class EP_MessageIdGeneratorMapper_UT
{
    static testMethod void getRecordsByIds_test() {
    	EP_MessageIdGeneratorMapper localObj = new EP_MessageIdGeneratorMapper();
    	list<Id> messIdGenObjList = new list<Id>();
    	EP_Message_Id_Generator__c msgIdGen1 = new EP_Message_Id_Generator__c();
    	EP_Message_Id_Generator__c msgIdGen2 = new EP_Message_Id_Generator__c();
    	Insert msgIdGen1;
    	Insert msgIdGen2;
    	messIdGenObjList.add(msgIdGen1.Id);
    	messIdGenObjList.add(msgIdGen2.Id);
    	System.debug('************MsgGen Id List***************'+messIdGenObjList);
    	Test.startTest();
    	list<EP_Message_Id_Generator__c> result = localObj.getRecordsByIds(messIdGenObjList);
    	Test.stopTest();
    	System.Assert(result.size()>0);
    }

    static testMethod void getNameOfMesgIdGenerator_test() {
    	EP_MessageIdGeneratorMapper localObj = new EP_MessageIdGeneratorMapper();    	
    	EP_Message_Id_Generator__c msgGenId = new EP_Message_Id_Generator__c();
        Insert msgGenId;
        Id mesIdGenId = msgGenId.Id;
        Test.startTest();
        String result = localObj.getNameOfMesgIdGenerator(mesIdGenId);
        Test.stopTest();
        System.Assert(result<>null);
    }
}