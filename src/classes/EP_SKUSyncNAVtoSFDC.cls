/* 
   @Author: Balraj
   @Description: <This is apex RESTful WebService for SKV Sync from NAV to SFDC> 
   @name: EP_SKUSyncNAVtoSFDC
   @CreateDate <22/04/2016>
   @Version <1.0>
*/
@RestResource(urlMapping='/v1/SKUSync/*')
global with sharing class EP_SKUSyncNAVtoSFDC{
	/* 
	   @Author: Balraj
	   @Description: <This is the method that handles HttpPost request for SKU sync> 
	   @Method Name: skuSync
	   @CreateDate <22/04/2016> 
	   @Params: No argument
	   @Return Type: void
	*/
    @HttpPost
    global static void skuSync(){
		//Create instance of RestRequest and initialize from received REST request context value
        RestRequest request = RestContext.request;
        //Capture REST body
        String requestBody = request.requestBody.toString();
       	//Process received REST body
        EP_SKUHandler.SKUwrapper skuRecords = EP_SKUHandler.parse(requestBody);
        //Do DML on on processed SKU 
        EP_SKUHandler.createUpdateSKU(skuRecords); 
    }   
}