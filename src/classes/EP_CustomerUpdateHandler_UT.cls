@isTest
public class EP_CustomerUpdateHandler_UT
{
	@testSetup static void init() {
      List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
	static testMethod void processRequest_Customer() {
		EP_CustomerUpdateHandler localObj = new EP_CustomerUpdateHandler();
		Account accountObj = EP_TestDataUtility.getSellTo();
		EP_CustomerUpdateStub stub = EP_TestDataUtility.createInboundCustomerMessageWithMultipleBanks();
		stub.MSG.HeaderCommon.ObjectName = EP_COMMON_CONSTANT.CSTMR_OUTBOUND_STGNG;
        List<EP_CustomerUpdateStub.customer> customers = stub.MSG.payload.any0.customers.customer;
        EP_CustomerUpdateStub.customer customer = customers[0];
        customer.billToCustId = string.valueOf(accountObj.AccountNumber);
        customer.clientId = string.valueOf(accountObj.EP_Account_Company_Name__c);
        customer.identifier.custId = string.valueOf(accountObj.AccountNumber);        
        List<EP_CustomerUpdateStub.custBank> custBanks = customer.custBanks.custBank;
        EP_CustomerUpdateStub.custBank custBank = custBanks[0];
        custBank.identifier.custId = string.valueOf(accountObj.AccountNumber);        
        string jsonRequest = JSON.serialize(stub);
		Test.startTest();
		String result = localObj.processRequest(jsonRequest);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	
	static testMethod void processRequest_BankAccounts() {
		EP_CustomerUpdateHandler localObj = new EP_CustomerUpdateHandler();
		Account accountObj = EP_TestDataUtility.getSellTo();
		EP_CustomerUpdateStub stub = EP_TestDataUtility.createInboundCustomerMessageWithMultipleBanks();
		stub.MSG.HeaderCommon.ObjectName = EP_COMMON_CONSTANT.CSTMR_BANK_OUT_STGNG;
        List<EP_CustomerUpdateStub.customer> customers = stub.MSG.payload.any0.customers.customer;
        EP_CustomerUpdateStub.customer customer = customers[0];
        customer.billToCustId = string.valueOf(accountObj.AccountNumber);
        customer.clientId = string.valueOf(accountObj.EP_Account_Company_Name__c);
        customer.identifier.custId = string.valueOf(accountObj.AccountNumber);        
        List<EP_CustomerUpdateStub.custBank> custBanks = customer.custBanks.custBank;
        EP_CustomerUpdateStub.custBank custBank = custBanks[0];
        custBank.identifier.custId = string.valueOf(accountObj.AccountNumber);        
        string jsonRequest = JSON.serialize(stub);
		Test.startTest();
		String result = localObj.processRequest(jsonRequest);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	static testMethod void parse_test() {
		EP_CustomerUpdateHandler localObj = new EP_CustomerUpdateHandler();
		EP_CustomerUpdateStub stub = EP_TestDataUtility.createInboundCustomerMessageWithMultipleBanks();
		String jsonString = JSON.serialize(stub);
		Test.startTest();
		EP_CustomerUpdateStub result = EP_CustomerUpdateHandler.parse(jsonString);
		Test.stopTest();
		System.assertEquals(true, result != null);
	}
	static testMethod void createAcknowledgementDatasets_test() {
		List<EP_CustomerUpdateStub.customer> customers = EP_TestDataUtility.createInboundCustomers();
		customers[0].SFAccount = new Account();
		Account account = EP_TestDataUtility.getSellToPositiveScenario(); 
		customers[0].SFAccount = account;
		EP_Bank_Account__c bankAccount = [select id,EP_Source_Seq_Id__c from EP_Bank_Account__c where EP_Account__c=: account.Id limit 1];
		customers[0].custBanks.custBank[0].SFBankAccount = bankAccount;
		Test.startTest();
		LisT<EP_AcknowledgementStub.dataset> result = EP_CustomerUpdateHandler.createAcknowledgementDatasets(customers);
		Test.stopTest();
		System.assertEquals(true,result!=null);
	}
// 45361 and 45362 Start
	static testMethod void createAcknowledgementDatasetsNeg_test() {
		List<EP_CustomerUpdateStub.customer> customers = EP_TestDataUtility.createInboundCustomers();
		customers[0].SFAccount = new Account();
		Account account = EP_TestDataUtility.getSellToPositiveScenario(); 
		customers[0].SFAccount = account;
		EP_Bank_Account__c bankAccount = [select id,EP_Source_Seq_Id__c from EP_Bank_Account__c where EP_Account__c=: account.Id limit 1];
		customers[0].custBanks.custBank[0].SFBankAccount = bankAccount;
		customers[0].errorDescription = 'Error';
		Test.startTest();
		LisT<EP_AcknowledgementStub.dataset> result = EP_CustomerUpdateHandler.createAcknowledgementDatasets(customers);
		Test.stopTest();
		System.assertEquals(true,result!=null);
	}

	static testMethod void getShipToUpdate_test(){
		Account acc = EP_TestDataUtility.getSellTo();
		List<Account> accList = new List<Account>{acc};
		List<EP_CustomerUpdateStub.shipToAddress> listshipToAddress = new List<EP_CustomerUpdateStub.shipToAddress>();
		EP_CustomerUpdateStub.shipToAddress shipToAddressObj = new EP_CustomerUpdateStub.shipToAddress();
		shipToAddressObj.SFShipToObject = acc;
		listshipToAddress.add(shipToAddressObj);
		EP_CustomerUpdateStub.shipToAddresses shipToAddressesObj = new EP_CustomerUpdateStub.shipToAddresses();
		shipToAddressesObj.shipToAddress = listshipToAddress;
 		Test.startTest();
        EP_CustomerUpdateHandler.getShipToUpdate(shipToAddressesObj,accList);
		Test.stopTest();


	}
//45361 and 45362 End
	static testMethod void getErrorDatasetOfBankAccounts_test() {
		EP_CustomerUpdateStub.custBanks custBanks = EP_TestDataUtility.createInboundCustomerListOfBanks();
		List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
		String errorDescription = 'Error Test';
		Test.startTest();
		EP_CustomerUpdateHandler.getErrorDatasetOfBankAccounts(custBanks,ackDatasets,errorDescription);
		Test.stopTest();
		System.assertEquals(true, ackDatasets!=null);
	}
	static testMethod void getBankAccountsToUpsert_test() {
		EP_CustomerUpdateStub.custBanks custBanks = EP_TestDataUtility.createInboundCustomerListOfBanks();
		custBanks.custBank[0].SFBankAccount = new EP_Bank_Account__c(); 
		List<EP_Bank_Account__c> banksToUpsert = new List<EP_Bank_Account__c>();
		Test.startTest();
		EP_CustomerUpdateHandler.getBankAccountsToUpsert(custBanks,banksToUpsert);
		Test.stopTest();
		System.assertEquals(true, banksToUpsert != null);
	}
}