@isTest
public class EP_VMIConsignmentSM_UT
{
	@testSetup static void init() {
      	List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }

	static testMethod void getOrderState_test() {
		EP_VMIConsignmentSM localObj = new EP_VMIConsignmentSM();
		EP_OrderDomainObject obj = EP_TestDataUtility.getVmiConsignmentOrderStatePlannedDomainObject();
		EP_OrderEvent currentEvent = new EP_OrderEvent(EP_OrderConstant.PLANNED_STATUS);
		localObj.setOrderDomainObject(obj);
		Test.startTest();
		EP_OrderState result = localObj.getOrderState(currentEvent);
		Test.stopTest();
		System.AssertNotEquals(null,result);
	}
}