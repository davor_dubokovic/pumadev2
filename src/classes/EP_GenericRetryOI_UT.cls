/*
   @Author          Accenture
   @Name            EP_GenericRetryOI_UT - #59186
   @CreateDate      02/07/2017
   @Description     This Test class for EP_GenericRetryOI Class
   @Version         1.0
*/
@isTest
private class EP_GenericRetryOI_UT {

	@testSetup static void init() {
		List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
      	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
      	List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
      	List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    	
    }

	@isTest
	private static void EP_GenericRetryOIPositive_Test() {
		
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null);  
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		Test.startTest();
		System.enqueueJob(new EP_GenericRetryOI(intRecord.EP_Message_ID__c));
		Test.stopTest();
		intRecord = [SELECT EP_Attempt__c FROM EP_IntegrationRecord__c WHERE Id=: intRecord.Id LIMIT 1];
		System.assertEquals(intRecord.EP_Attempt__c,2.0);
		
	}

	
	@isTest
	private static void EP_GenericRetryOINegative_Test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
    	EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null);  
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
		Test.startTest();
		System.enqueueJob(new EP_GenericRetryOI('Test'));
		Test.stopTest();
		intRecord = [SELECT EP_Attempt__c FROM EP_IntegrationRecord__c WHERE Id=: intRecord.Id LIMIT 1];
		System.assertNotEquals(intRecord.EP_Attempt__c,2.0);
	}
}