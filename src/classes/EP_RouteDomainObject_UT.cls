@isTest
public class EP_RouteDomainObject_UT{

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
    public static final String ROUTE_INACTIVE = 'InActive';
    public static final String ROUTE_ACTIVE = 'Active';
    
    static testMethod void EP_RouteDomainObject_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        Test.startTest();
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        Test.stopTest();
        System.assert(routeDomObj.service != NULL);
   }
   
   static testMethod void EP_RouteDomainObjectConstructor_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        Test.startTest();
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords);
        Test.stopTest();
        System.assert(routeDomObj.service != NULL);
   }
   
   static testMethod void getNewRoutes_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        Test.startTest();
        Map<Id,EP_Route__c> result = routeDomObj.getNewRoutes();
        Test.stopTest();
        System.assertEquals(5,result.size());
   }
   
   static testMethod void getOldRoutes_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        Test.startTest();
        Map<Id,EP_Route__c> result = routeDomObj.getOldRoutes();
        Test.stopTest();
        System.assertEquals(5,result.size());
   }
   
     static testMethod void doActionBeforeUpdate_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        Test.startTest();
        routeDomObj.doActionBeforeUpdate();
        Test.stopTest();
        
        //No Asserts requred , As this methdods delegates to other methods  
        System.assert(true);
           
   }
   
   static testMethod void doActionBeforeDelete_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords);     
        Test.startTest();
          routeDomObj.doActionBeforeDelete();
        Test.stopTest(); 
        
         //No Asserts requred , As this methdods delegates to other methods  
        System.assert(true);
   }
   
   static testMethod void isRouteNameChanged_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        Map<Id,EP_Route__c> routeOriginal = new Map<Id,EP_Route__c>([SELECT Id,EP_Route_Code__c,EP_Route_Name__c FROM EP_Route__c WHERE Id IN :  routeRecords.keySet()]);
        for(EP_Route__c route : routeRecords.values()){
            route.EP_Route_Code__c = route.EP_Route_Name__c+'test';
            routeList.add(route);
        }
        Test.startTest();
        Boolean result = routeDomObj.isRouteNameChanged(routeList[0],routeRecords.get(routeList[0].id));
        Test.stopTest();
        System.assertNotEquals(true,result);

           
   }
   
   static testMethod void isRoutecodeChanged_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        Map<Id,EP_Route__c> routeOriginal = new Map<Id,EP_Route__c>([SELECT Id,EP_Route_Code__c,EP_Route_Name__c FROM EP_Route__c WHERE Id IN :  routeRecords.keySet()]);
        for(EP_Route__c route : routeRecords.values()){
            route.EP_Route_Code__c = route.EP_Route_Code__c+'test';
            routeList.add(route);
        }
        Test.startTest();
        Boolean result = routeDomObj.isRouteCodeChanged(routeList[0],routeRecords.get(routeList[0].id));
        Test.stopTest();       
        System.assertEquals(false,result);

           
   }
   
   static testMethod void isRouteStatusChanged_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);

        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            route.EP_Status__c = ROUTE_ACTIVE;
            routeList.add(route);
        }
        Test.startTest();
        Boolean result = routeDomObj.isRouteStatusChanged(routeList[0],routeRecords.get(routeList[0].id));
        Test.stopTest();
        System.assertEquals(false,result);
           
   }
   
     



}