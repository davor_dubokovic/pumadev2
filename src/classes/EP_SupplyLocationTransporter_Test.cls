/**
 * @Author <Jai Singh>
 * @name <EP_SupplyLocationTransporter_Test>
 * @CreateDate <20/06/2016>
 * @Description <This is test class for Supply Location Transporter and Vendor Account Record Type Trigger>
 * @Version <1.0>
 */
@isTest
private class EP_SupplyLocationTransporter_Test {

private static Account testShipToAccount;
    private static Account testSellToAccount;
    private static Account vmiShipToAccount = new Account();
    private static order order1 = new Order();
    private static Account lCustomerAccounts = new Account();
    private static Account lBillTo = new Account();
    private static Account lStogrageLocation = new Account();
    private static EP_Stock_Holding_Location__c lSupplyLocation = new EP_Stock_Holding_Location__c();
    private static EP_Stock_Holding_Location__c lSupplyLocation1 = new EP_Stock_Holding_Location__c();
    private static final string DELIVERYTYPE_SUPPLY_LOCATION_RT = 'Delivery Supply Location';
    private static final string VENDORTYPE = 'Transporter';
    private static final string VENDORTYPE_SUPPLIER = '3rd Party Stock Supplier';
    private static final String CANNOT_BLOCK_ERROR = 'Cannot block the Transporter or 3rd Party Stock Supplier as there is one or more open Orders assigned to this record';
    private static final string ERROR_MESSAGE = 'Please ensure atleast one default transporter is associated with this Supply Location';
    private static final string ERROR_MESSAGE_VENDOR_MODIFY = 'Vendor (Transporter or 3rd Party Stock Supplier) can only be created or modified by system admin';
    private static final string ERROR_MESSAGE_DEFAULT_TRANSPORTER = 'Transporter is set as default for one or more Supply Locations. Before you can proceed with the blocking';
    private static final string ERROR_MESSAGE_REQUIED_FIELD_MISSING = 'Billing Address or State or City or Postal Code or Country Is required';
    
    private static final String TRANSPORTER_COORDINATOR_PROFILE = 'Puma Logistics Agent_R1';
    private static final String NAV_VENDOR_ID_1 = '00001';
    private static final String NAV_VENDOR_ID_2 = '00002';
    private static final String COMPANY_CODE_1 = 'AUN2';
    private static final String COMPANY_CODE_2 = 'EPUMA1';
        
    private static Profile sysAdmin = [Select id from Profile
                                    Where Name =: EP_Common_Constant.ADMIN_PROFILE  
                                    limit :EP_Common_Constant.ONE];
    private static User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
    
    
    /*
        This method tests success for creating transporter type vendor Test Case#20100 WU15(Freight)
    */ 
    static testMethod void createVendorSuccess_Failure()
    {
        createTestData(); 
        
        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMPANY_CODE_1);
                Database.insert( vendorAccount,false );  
                system.assertNotEquals(null,vendorAccount.Id);  
            }
            
            Profile transporterCoordinatorProfile = [Select id from Profile
                                        Where Name =: TRANSPORTER_COORDINATOR_PROFILE 
                                        limit :EP_Common_Constant.ONE];
            User transporterCoordinatorUser = EP_TestDataUtility.createUser(transporterCoordinatorProfile.id);
            
            System.runAs(transporterCoordinatorUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_2;
                Account vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_2,COMPANY_CODE_2);
                DataBase.SaveResult SR = Database.insert( vendorAccount,false );  
                system.assertEquals(null,vendorAccount.Id);
                System.assertEquals(false,SR.isSuccess());  
                system.debug('Error=='+SR.getErrors()[0].getMessage() );
                System.assert(SR.getErrors()[0].getMessage().containsignorecase( ERROR_MESSAGE_VENDOR_MODIFY ));
            }
        Test.stopTest();
        
    }
    /*
        This method tests success for creating 3rd Party Stock Supplier type vendor Test Case#23318,23346,23344  WU23(Freight)
    */ 
    static testMethod void createSupplierVendorSuccess_Failure()
    {
        createTestData(); 
        
        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_1,COMPANY_CODE_1);
                Database.insert( vendorAccount,false );  
                system.assertNotEquals(null,vendorAccount.Id);  
                
                vendorAccount = [Select Id, BillingStreet from Account where Id =: vendorAccount.Id LIMIT 1];
                vendorAccount.BillingStreet = 'Test Street';
                DataBase.SaveResult SR3 = Database.update( vendorAccount, false );
                System.assertEquals(true,SR3.isSuccess()); 
            }
            
            Profile transporterCoordinatorProfile = [Select id from Profile
                                        Where Name =: TRANSPORTER_COORDINATOR_PROFILE 
                                        limit :EP_Common_Constant.ONE];
            User transporterCoordinatorUser = EP_TestDataUtility.createUser(transporterCoordinatorProfile.id);
            
            System.runAs(transporterCoordinatorUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_2;
                Account vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_2,COMPANY_CODE_2);
                DataBase.SaveResult SR = Database.insert( vendorAccount,false );  
                system.assertEquals(null,vendorAccount.Id);
                System.assertEquals(false,SR.isSuccess());  
                system.debug('Error=='+SR.getErrors()[0].getMessage() );
                System.assert(SR.getErrors()[0].getMessage().containsignorecase( ERROR_MESSAGE_VENDOR_MODIFY ));
            }
        Test.stopTest();
        
    }
    
    /*
        This method tests fialure of creating 3rd Party Stock Supplier type vendor for duplicate Vendor Id Test Case#23386  WU23(Freight)
    */ 
    static testMethod void createDuplicateSupplierVendor_Failure()
    {
        createTestData(); 
        
        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount1 = EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_1,COMPANY_CODE_1);
                Database.insert( vendorAccount1,false );  
                system.assertNotEquals(null,vendorAccount1.Id);  
                
                Account vendorAccount2 = EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_1, COMPANY_CODE_1 );
                
                DataBase.SaveResult SR3 = Database.insert( vendorAccount1,false ); 
                System.assertEquals(false,SR3.isSuccess()); 
            }            
        Test.stopTest();
        
    }
    
    /*
        This method tests failuer missing required fields for creating 3rd Party Stock Supplier type vendor Test Case# 23319  WU23(Freight)
    */ 
    static testMethod void createSupplierVendor_Failure()
    {
        createTestData(); 
        
        Test.startTest();
            System.runAs(adminUser) 
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_1,COMPANY_CODE_1);
                vendorAccount.BillingStreet = EP_Common_Constant.BLANK;
                vendorAccount.BillingState = EP_Common_Constant.BLANK;
                vendorAccount.BillingPostalCode= EP_Common_Constant.BLANK;
                vendorAccount.BillingCity = EP_Common_Constant.BLANK;
                vendorAccount.BillingCountry = EP_Common_Constant.BLANK;
                
                DataBase.SaveResult SR = Database.insert( vendorAccount, false );
                System.assertEquals(false,SR.isSuccess()); 
                system.debug( 'Error=='+SR.getErrors()[0].getMessage());  
                System.assert(SR.getErrors()[0].getMessage().containsignorecase( ERROR_MESSAGE_REQUIED_FIELD_MISSING ));
            }
        Test.stopTest(); 
        
    }
    
    
    
    
    /*
        This method tests success for creating transporter type vendor Test Case#20118 WU15(Freight)
    */ 
    static testMethod void updateVendorSuccess_Failure()
    {
        createTestData();
        //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
        Account vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMPANY_CODE_1);
        
        Test.startTest();
            System.runAs(adminUser)
            {
                Database.insert( vendorAccount,false );
                system.assertNotEquals(null,vendorAccount.Id);
                
                vendorAccount.BillingStreet = 'New street';
                DataBase.SaveResult SR = Database.update( vendorAccount,false );
                System.assert(SR.isSuccess());
            }
            
            Profile transporterCoordinatorProfile = [Select id from Profile
                                        Where Name =: TRANSPORTER_COORDINATOR_PROFILE 
                                        limit :EP_Common_Constant.ONE];
            User transporterCoordinatorUser = EP_TestDataUtility.createUser(transporterCoordinatorProfile.id);
        
            System.runAs(transporterCoordinatorUser)
            {
                vendorAccount.BillingStreet = 'New street Co-ordinaor';
                DataBase.SaveResult SR = Database.update( vendorAccount,false );
                System.assert(!SR.isSuccess());
            }
        Test.stopTest();
    }
    
    /*
        This method tests success for linking transporter with supply location Case#20105,20119, 20120 WU15(Freight)
    */ 
    static testMethod void linkTransporterWithSupplyLocationSuccess()
    {
        createTestData();

        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount1 = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMPANY_CODE_1);
                Database.insert( vendorAccount1,false );
                system.assertNotEquals(null,vendorAccount1.Id);
                
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_2;
                Account vendorAccount2 = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_2,COMPANY_CODE_2);
                Database.insert( vendorAccount2,false );
                system.assertNotEquals(null,vendorAccount2.Id);
                
                EP_Supply_Location_Transporter__c slpObj = new EP_Supply_Location_Transporter__c(
                    EP_Is_Default__c = false,
                    EP_Supply_Location__c  = lSupplyLocation.Id,
                    EP_Transporter__c = vendorAccount1.Id
                );
                DataBase.SaveResult SR = Database.insert( slpObj,false );
                System.assert(!SR.isSuccess());
                System.assert(SR.getErrors()[0].getMessage().containsignorecase( ERROR_MESSAGE ));              
                
                EP_Supply_Location_Transporter__c slpObj1 = new EP_Supply_Location_Transporter__c(
                    EP_Is_Default__c = True,
                    EP_Supply_Location__c  = lSupplyLocation1.Id,
                    EP_Transporter__c = vendorAccount1.Id
                );
                DataBase.SaveResult SR1 = Database.insert( slpObj1);
                System.assertNotEquals(slpObj1.Id, null);
                System.assertEquals(slpObj1.EP_Is_Default__c, true);
                
                EP_Supply_Location_Transporter__c slpObj2 = new EP_Supply_Location_Transporter__c(
                    EP_Is_Default__c = True,
                    EP_Supply_Location__c  = lSupplyLocation1.Id,
                    EP_Transporter__c = vendorAccount2.Id
                );
                DataBase.SaveResult SR2 = Database.insert( slpObj2 );
                System.assertNotEquals(slpObj2.Id, null);
                System.assertEquals(slpObj2.EP_Is_Default__c, true);
                
                Integer nRows = EP_Common_Util.getQueryLimit();
                list<EP_Supply_Location_Transporter__c> defaultTransporter = [Select Id, EP_Is_Default__c, EP_Transporter__c from  EP_Supply_Location_Transporter__c where EP_Is_Default__c = True AND EP_Supply_Location__c  =: lSupplyLocation1.Id LIMIT: nRows];
                System.assert(defaultTransporter.size() == 1);//Only one default Transporter
                System.assertEquals( defaultTransporter[0].EP_Transporter__c, vendorAccount2.Id );//Default Transporter changed to vendorAccount2
            }
        Test.stopTest();
    }
    
    /*
        This method tests for blocking a vendor Case#20109 WU15(Freight)
    */ 
    static testMethod void cannotBlockDefaultTransporter_Failure()
    {
        createTestData();
        
        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount1 = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMPANY_CODE_1 );
                Database.insert( vendorAccount1,false );
                system.assertNotEquals(null,vendorAccount1.Id);
                
                EP_Supply_Location_Transporter__c slpObj = new EP_Supply_Location_Transporter__c(
                    EP_Is_Default__c = True,
                    EP_Supply_Location__c  = lSupplyLocation1.Id,
                    EP_Transporter__c = vendorAccount1.Id
                );
                DataBase.SaveResult SR = Database.insert( slpObj, false);
                System.assertNotEquals(slpObj.Id, null);
                System.assertEquals(slpObj.EP_Is_Default__c, true);                 
                
                vendorAccount1.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                DataBase.SaveResult SR2 = Database.update( vendorAccount1,false );
                System.assert(!SR2.isSuccess()); 
                System.assert(SR2.getErrors()[0].getMessage().containsignorecase( ERROR_MESSAGE_DEFAULT_TRANSPORTER ));
            }
        Test.stopTest();
            
    }
    
    /*
        This method tests for blocking a transporter vendor success Case#20112,20117 WU15(Freight)
    */ 
    static testMethod void canBlockUnblockTransporter_Success()
    {
        createTestData();
        
        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount1 = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMPANY_CODE_1 );
                Database.insert( vendorAccount1,false );
                system.assertNotEquals(null,vendorAccount1.Id);
                
                vendorAccount1.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                DataBase.SaveResult SR = Database.update( vendorAccount1,false );
                System.assert(SR.isSuccess()); 
                
                vendorAccount1.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                DataBase.SaveResult SR2 = Database.update( vendorAccount1,false );
                System.assert(SR2.isSuccess()); 
            }
        Test.stopTest();
            
    }
    
    /*
        This method tests for blocking a supplier vendor success testcase# 23331,23336 WU23(Freight)
    */ 
    static testMethod void canBlockUnblockSupplier_Success()
    {
        createTestData();
        
        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount1 = EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_1,COMPANY_CODE_1 );
                Database.insert( vendorAccount1,false );
                system.assertNotEquals(null,vendorAccount1.Id);
                
                vendorAccount1.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                DataBase.SaveResult SR = Database.update( vendorAccount1,false );
                System.assert(SR.isSuccess()); 
                
                vendorAccount1.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                DataBase.SaveResult SR2 = Database.update( vendorAccount1,false );
                System.assert(SR2.isSuccess()); 
            }
        Test.stopTest();
            
    }
    
    /*
        This method tests for blocking a transporter vendor failure open order Case#23328 WU15(Freight)
    */ 
    static testMethod void blockTransporterWithOpenOrder()
    {
        createTestData();
            
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tankObj.EP_Tank_Status__c = 'New';
        insert tankObj;
        
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update vmiShipToAccount;
        
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;
        
        tankObj.EP_Tank_Status__c = 'Stopped';
        tankObj.EP_Reason_Blocked__c = 'Maintenance';
        update tankObj;
        
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update vmiShipToAccount;
        
        //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
        Account vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMPANY_CODE_1 );
        Database.insert( vendorAccount,false );
        system.assertNotEquals(null,vendorAccount.Id);
        
        Test.startTest();
            System.runAs(adminUser)
            {
                String orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get( EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME ).getRecordTypeId();
                Id pricebookId = Test.getStandardPricebookId();
                order1 = EP_TestDataUtility.createOrder(lCustomerAccounts.Id,orderRecType,pricebookId);
                order1.Status = EP_Common_Constant.SUBMITTED;
                order1.EP_Integration_Status__c = 'SENT';
                order1.EP_Transporter__c = vendorAccount.Id;
                insert order1;
                
                system.assertNotEquals(null,order1.Id);
                system.assertEquals(EP_Common_Constant.SUBMITTED,[Select Status from Order where Id =: order1.Id].Status);
                
                //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
                vendorAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                DataBase.SaveResult SR2 = Database.update( vendorAccount,false );
                System.assert(!SR2.isSuccess()); 
                system.debug('Error=='+SR2.getErrors()[0].getMessage() );
                System.assert(SR2.getErrors()[0].getMessage().containsignorecase( CANNOT_BLOCK_ERROR ));
            }
        Test.stopTest();
        
        
    }
    
    /*
        This method tests for blocking a supplier vendor failure open order WU23(Freight)
    */ 
    static testMethod void blockSupplierWithOpenOrder()
    {
        createTestData();
            
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tankObj.EP_Tank_Status__c = 'New';
        insert tankObj;
        
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update vmiShipToAccount;
        
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;
        
        tankObj.EP_Tank_Status__c = 'Stopped';
        tankObj.EP_Reason_Blocked__c = 'Maintenance';
        update tankObj;
        
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update vmiShipToAccount;
        
        //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
        Account vendorAccount; //= EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_1,COMPANY_CODE_1 );
        //Database.insert( vendorAccount,false );
        //system.assertNotEquals(null,vendorAccount.Id);
        
        Test.startTest();
            System.runAs(adminUser)
            {
                vendorAccount = EP_TestDataUtility.createTestVendor( VENDORTYPE_SUPPLIER, NAV_VENDOR_ID_1,COMPANY_CODE_1 );
                Database.insert( vendorAccount,false );
                system.assertNotEquals(null,vendorAccount.Id);
                String orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get( EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME ).getRecordTypeId();
                Id pricebookId = Test.getStandardPricebookId();
                String CurrencyIsoCode = [Select Id, CurrencyIsoCode from Account where Id =: lCustomerAccounts.Id LIMIT 1 ].CurrencyIsoCode;
                
                list<PriceBookEntry> pbeList = [Select Id from PriceBookEntry where Product2Id =: prod.Id AND Pricebook2Id =: pricebookId AND CurrencyIsoCode =: CurrencyIsoCode  LIMIT 1];
                PriceBookEntry pbe = new PriceBookEntry();
                if( pbeList.isEmpty() )
                {
                    pbe = new PriceBookEntry( Product2Id = prod.Id, PriceBook2Id = pricebookId, CurrencyIsoCode = CurrencyIsoCode );
                    Database.insert( pbe );
                }
                else
                {
                    pbe = pbeList[0];
                } 
                order1 = EP_TestDataUtility.createOrder(lCustomerAccounts.Id,orderRecType,pricebookId);
                order1.Status = EP_Common_Constant.SUBMITTED;
                //order1.EP_Transporter__c = vendorAccount.Id;
                order1.EP_Integration_Status__c = 'SENT';
                order1.CurrencyIsoCode = CurrencyIsoCode;
                insert order1;
                OrderItem oi = new OrderItem(
                    OrderId = order1.Id,
                    PriceBookEntryId = pbe.Id,
                    UnitPrice = 100,
                    Quantity = 1,
                    EP_3rd_Party_Stock_Supplier__c = vendorAccount.Id
                );
                Database.insert(oi);
                system.assertNotEquals(null,order1.Id);
                system.assertEquals(EP_Common_Constant.SUBMITTED,[Select Status from Order where Id =: order1.Id].Status);
                
                //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
                vendorAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                DataBase.SaveResult SR2 = Database.update( vendorAccount,false );
                System.assert(!SR2.isSuccess()); 
                system.debug('Error=='+SR2.getErrors()[0].getMessage() );
                System.assert(SR2.getErrors()[0].getMessage().containsignorecase( CANNOT_BLOCK_ERROR ));
            }
        Test.stopTest();
        
        
    }
    
    
    /*
        This method is for test coverage
    */ 
    static testMethod void updateDeleteSupplyLocationTranporter()
    {
        createTestData();
        Test.startTest();
            System.runAs(adminUser)
            {
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_1;
                Account vendorAccount1 = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_1,COMPANY_CODE_1 );
                Database.insert( vendorAccount1,false );
                system.assertNotEquals(null,vendorAccount1.Id);
                
                //EP_TestDataUtility.COMPANY_CODE = COMPANY_CODE_2;
                Account vendorAccount2 = EP_TestDataUtility.createTestVendor( VENDORTYPE, NAV_VENDOR_ID_2,COMPANY_CODE_2 );
                Database.insert( vendorAccount2,false );
                system.assertNotEquals(null,vendorAccount2.Id);
                
                EP_Supply_Location_Transporter__c slpObj1 = new EP_Supply_Location_Transporter__c(
                    EP_Is_Default__c = True,
                    EP_Supply_Location__c  = lSupplyLocation1.Id,
                    EP_Transporter__c = vendorAccount1.Id
                );
                EP_Supply_Location_Transporter__c slpObj2 = new EP_Supply_Location_Transporter__c(
                    EP_Is_Default__c = True,
                    EP_Supply_Location__c  = lSupplyLocation1.Id,
                    EP_Transporter__c = vendorAccount2.Id
                );
                Database.insert( new list<EP_Supply_Location_Transporter__c>{slpObj1,slpObj2});
                system.assertNotEquals(null,slpObj1.Id);
                system.assertNotEquals(null,slpObj2.Id);
                
                slpObj2.EP_Is_Default__c = true;
                EP_SupplyLocationTransprtrTriggerHandler.isExecuteUpdate = true;
                Database.update( slpObj2,false );
                Database.DeleteResult DR = Database.Delete( slpObj2, false );
                System.assert(!DR.isSuccess()); 
                System.assert(DR.getErrors()[0].getMessage().containsignorecase( ERROR_MESSAGE ));
                
            }
        Test.stopTest();
            
    }
    
    /*
        This method is for creating test data for WU15(Freight)
    */ 
    static void createTestData() 
    {
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        
        
       /** EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy = 
                                      EP_TestDataUtility.createCustomerHierarchyForNAV(1);
                                      
        
        testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
        testShipToAccount = testAccountHierarchy.lShipToAccounts[0];***/        
        system.debug('$$$$$$$$$$$'+testSellToAccount );
        Integer count;
        EP_Freight_Matrix__c freightMatrix;
        
        ////EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        ////EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
        

        //CREATE BILL TO
        lBillTo = EP_TestDataUtility.createBillToAccount();
        Database.insert(lBillTo);
        
        //Set Bill To Status as basic data setup
        lBillTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(lBillTo);
        
        //COMPLETE ACTIONS
        EP_TestDataUtility.CmpltApproveActions();
        
        //SETTING BILL TO STATUS TO ACTIVE
        lBillTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(lBillTo);
          
        //CREATE FREIGHT MATRIX
        freightMatrix = EP_TestDataUtility.createFreightMatrix();
        Database.insert(freightMatrix);
        
        //CREATE A CUSTOMER ACCOUNTS WITH ACTIVE BILL TO ACCOUNT
        lCustomerAccounts = EP_TestDataUtility.createSellToAccount(lBillTo.id, freightMatrix.id);
        Database.insert(lCustomerAccounts,false);
        
        //Set Sell To Status as basic data setup
        lCustomerAccounts.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(lCustomerAccounts);
        
        //create Storage location
        lStogrageLocation = EP_TestDataUtility.createStockHoldingLocation();
        Database.insert(lStogrageLocation,false);
        
        //Set Storage location Status as basic data setup
        lStogrageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(lStogrageLocation,false);
        
        //COMPLETE ACTIONS
        EP_TestDataUtility.CmpltApproveActions();
        
        //SETTING Sell TO STATUS TO ACTIVE
        lCustomerAccounts.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; 
        Database.update(lCustomerAccounts);
        
        //SETTING STORAGE LOCATION STATUS TO ACTIVE
        lStogrageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(lStogrageLocation);
        
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
        vmiShipToAccount = EP_TestDataUtility.createShipToAccount(lCustomerAccounts.Id, strRecordTypeId);
        vmiShipToAccount.EP_Email__c = 'test@test.com';
        insert vmiShipToAccount;
        //test.starttest();
        
        ID sellToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Sell-To Supply Location');
        lSupplyLocation = EP_TestDataUtility.createSellToStockLocation( lCustomerAccounts.id , True, lStogrageLocation.Id, sellToSHLRTID);
        Database.insert(lSupplyLocation); 
        
        Id DeliverySupplyLocationRT = Schema.SObjectType.EP_Stock_Holding_Location__c.getRecordTypeInfosByName().get(DELIVERYTYPE_SUPPLY_LOCATION_RT).getRecordTypeId();
        lSupplyLocation1 = EP_TestDataUtility.createShipToStockLocation( vmiShipToAccount.id, True, lStogrageLocation.id, DeliverySupplyLocationRT );
        Database.insert(lSupplyLocation1); 
        
        //test.stoptest();
            
    }
    
    
}