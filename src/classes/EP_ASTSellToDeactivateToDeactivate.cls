/* 
@Author      Accenture
@name        EP_ASTSellToDeactivateToDeactivate
@CreateDate  11/29/2017
@Description Account(SellTo) State Transitions class for Deactivate to Deactivate State - L4 #45361
@NovaSuite Fix -- comments added
@Version     1.0
*/
public with sharing class EP_ASTSellToDeactivateToDeactivate  extends EP_AccountStateTransition {
    /* 
    @Author      Accenture
    @name        constructor of class EP_ASTSellToDeactivateToDeactivate
    */
    public EP_ASTSellToDeactivateToDeactivate() {
        finalState = EP_AccountConstant.DEACTIVATE;
    }
    /* 
    @Author      Accenture
    @name        isGuardCondition
    @Return      boolean
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToDeactivate','isGuardCondition');
        //No guard conditions for Active to Inactive
        return true;
    }
}