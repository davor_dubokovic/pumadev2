/*********************************************************************************************
     @Author <Nicola Tassini>
     @name <EP_FE_DataService>
     @CreateDate <18/05/2016>
     @Description <Service to enable without sharing queries on the data model>  
     @Version <1.0>
    *********************************************************************************************/
global without sharing class EP_FE_DataService {

    global static final String CLASSNAME = 'EP_FE_DataService';
    global static final String METHODNAME = 'retrieveAccountPricebook';
    global static final String SEVERITY = 'ERROR';
    global static final String TRANSACTION_ID = 'PE-188';
    global static final String DESCRIPTION1 = 'PRICEBOOK NOT FOUND';
    global static final String DESCRIPTION2 = 'SELLTO NOT FOUND FOR GIVEN SELLTOID';
    global static final String DESCRIPTION3 = 'NO SHIPTO FOUND FOR GIVEN SHIPTOID';
    global static final String TEST2 = 'test2';

    /*
     *  Get a list of Sell-Tos
     *
     *  @param sellToIds list of ids
     *  @return sell-to details
     */
    public static Map<Id, Account> fetchSellToList(Set<Id> sellToIds) {
        System.debug('Account Query'+'SELECT ' + getSellToQueryFields() 
            + ' FROM Account '
            //+ ' WHERE RecordType.DeveloperName IN (\'' + EP_Common_Constant.SELL_TO_DEV_NAME + ','+ EP_Common_Constant.RT_BILL_TO_DEV_NAME + '\') '
            + ' WHERE RecordType.DeveloperName = \'' + EP_Common_Constant.SELL_TO_DEV_NAME + '\' '
          //  + ' AND EP_Status__c = \'' + EP_Common_Constant.STATUS_ACTIVE + '\' ' //Make inactive SellTo's available
            + ' AND Id IN :sellToIds ');
        
        return sellToIds != null && sellToIds.size() > 0 
            ? new Map<Id, Account>((List<Account>) Database.query('SELECT ' + getSellToQueryFields() 
            + ' FROM Account '
            //+ ' WHERE RecordType.DeveloperName IN (\'' + EP_Common_Constant.SELL_TO_DEV_NAME + ','+ EP_Common_Constant.RT_BILL_TO_DEV_NAME + '\') '
            + ' WHERE RecordType.DeveloperName = \'' + EP_Common_Constant.SELL_TO_DEV_NAME + '\' '
          //  + ' AND EP_Status__c = \'' + EP_Common_Constant.STATUS_ACTIVE + '\' ' //Make inactive SellTo's available
            + ' AND Id IN :sellToIds '))
            //+ ' LIMIT 10 '))
            //+ ' LIMIT ' + EP_FE_Constants.SQL_LIMIT ))
        : new Map<Id, Account>();
    }
    /*
     *
     */       
    public static void upsertOrderWithoutSharing(csord__Order__c orderObject){
        Database.upsert(orderObject);
    }

    /*
     *  Return the details of a Sell-to
     *
     *  @param sellToId ID of the sell To
     *  @return the sell to details or null if not found
     */
    public static Account getSellTo(Id sellToId) {
        List<Account> sellToList = null;
        try {
            sellToList = Database.query('SELECT ' + getSellToQueryFields() 
                + ' FROM Account ' 
                + ' WHERE Id = :sellToId AND (RecordType.DeveloperName = \'' + EP_Common_Constant.SELL_TO_DEV_NAME + '\' OR RecordType.DeveloperName = \'' + EP_Common_Constant.RT_BILL_TO_DEV_NAME + '\') ');
                
        } catch(Exception e) {
            throw e;
        }

        return (sellToList != null && sellToList.size() > 0) ? sellToList.get(0) : null;
    }

    /*
     *  Return the fields to query for a SellTo
     *
     *  @return list of fields to query on a sell-to
     */
     public static String getSellToQueryFields() {
        return ' Id, Name, AccountNumber, ParentId, EP_Slotting_Enabled__c, EP_PriceBook__c, BillingAddress, EP_Billing_Method__c, RecordType.DeveloperName, '
            + ' EP_Billing_Frequency__c, EP_Company_Tax_Number__c, EP_Delivery_Type__c, EP_Email__c, EP_Mobile_Phone__c, EP_Status__c, '
            + ' EP_Bill_To_Account__c, EP_Payment_Term_Name__c, EP_Ship_To_Type__c, EP_Reason_Blocked__c, Fax, Phone, Website, EP_Payment_Term_Lookup__c, '
            + ' EP_Bill_To_Account__r.Id, EP_Bill_To_Account__r.Name, EP_Bill_To_Account__r.AccountNumber, EP_Bill_To_Account__r.ParentId, EP_Bill_To_Account__r.BillingAddress, '
            + ' EP_Bill_To_Account__r.EP_Billing_Method__c, EP_Bill_To_Account__r.RecordType.DeveloperName, EP_Is_Pre_Pay__c, '
            + ' EP_Bill_To_Account__r.EP_Billing_Frequency__c, EP_Bill_To_Account__r.EP_Company_Tax_Number__c, EP_Bill_To_Account__r.EP_Delivery_Type__c, '
            + ' EP_Bill_To_Account__r.EP_Email__c, EP_Bill_To_Account__r.EP_Mobile_Phone__c, EP_Bill_To_Account__r.EP_Status__c, EP_Bill_To_Account__r.CurrencyISOCode, '
            + ' EP_Bill_To_Account__r.EP_Payment_Term_Name__c, EP_Bill_To_Account__r.EP_Reason_Blocked__c, EP_Bill_To_Account__r.Fax, EP_Bill_To_Account__r.Phone, '
            + ' EP_Bill_To_Account__r.Website, EP_Puma_Company__r.EP_Window_End_Hours__c, EP_Puma_Company__r.EP_Window_Start_Hours__c, '
            + ' EP_Bill_To_Account__r.EP_Payment_Method__r.Name, EP_Bill_To_Account__r.EP_Payment_Method__r.EP_Payment_Method_Code__c, '
            + ' EP_Country__c, EP_PriceBook__r.CurrencyIsoCode, EP_Business_Hours__c, EP_Credit_Limit__c, EP_AvailableFunds__c,EP_Bill_To_Account__r.EP_Credit_Limit__c, '
            + ' EP_Bill_To_Account__r.EP_AvailableFunds__c, CurrencyISOCode, EP_Is_Customer_Reference_Mandatory__c, '
            + ' EP_Puma_Company__r.EP_Is_Customer_Reference_Visible__c, EP_Puma_Company__r.EP_Is_Truck_Plate_Visible__c, '
            + ' EP_Puma_Company__r.EP_Is_Truck_Plate_Mandatory__c, EP_Puma_Company__r.EP_Is_Comments_Field_Visible__c, EP_Language_Code__c, '
            + ' BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, EP_Bill_To_Account__r.BillingStreet, '
            + ' EP_Bill_To_Account__r.BillingCity, EP_Bill_To_Account__r.BillingState, EP_Bill_To_Account__r.BillingPostalCode, EP_Bill_To_Account__r.BillingCountry, '
            + ' EP_Payment_Method__r.Name, EP_Payment_Method__r.EP_Payment_Method_Code__c,  EP_Scheduling_Hours__c, EP_Puma_Company__r.EP_Order_Request_Window__c, EP_Puma_Company__r.EP_Order_By_Tank_Enabled__c,'
            + '	EP_Sell_To_Holding_Account__c,EP_Sell_To_Holding_Account__r.EP_AvailableFunds__c,EP_Sell_To_Holding_Account__r.EP_Credit_Limit__c,EP_Sell_To_Holding_Account__r.CurrencyISOCode'; 
    }
    
    /*
     *  Return map of Accesses which recognize if User can edit Order
     *
     *  @return Map<Id, Boolean> Map of User Read accessess
     */
    public static Map<Id, Boolean> getOrderEditAccessMap(Set<Id> orderIds){
        Map<Id, Boolean> orderAccessesMap = new Map<Id, Boolean>();
        List<Id> orderIdsList = new List<Id>(orderIds), orderIdsQueryList = new List<Id>();
        Integer index = 0;
        for (index = 0; index < orderIdsList.size(); ){
            while (index < orderIdsList.size() && orderIds.size() < 200){
                orderIdsQueryList.add(orderIdsList.get(index));
                index++;
            }
            
            if (orderIds.size() > 0){
                UserRecordAccess[] orderAccessList = [SELECT RecordId
                                      FROM UserRecordAccess 
                                      WHERE UserId = :UserInfo.getUserId() AND RecordId IN :orderIdsQueryList AND HasEditAccess = true];
                                      
                if (UserInfo.getUserName().containsIgnoreCase(TEST2)){
                    System.AssertEquals(''+UserInfo.getUserId(), JSON.serialize(orderAccessList));
                }
                for (UserRecordAccess orderAccess : orderAccessList){
                    orderAccessesMap.put(orderAccess.RecordId, orderAccess.HasEditAccess);
                }
                orderIdsQueryList.clear();
            }
        }
        return orderAccessesMap;
    }

    /*
     *  Given a set of shipTo Ids, return the map of ShipToId & Pricebook ID
     *  Overall:
     *  1. Return the pricebook associated to the ShipTo
     *  2. If there is no pricebook at the ShipTo level, return the pricebook on the Sell-To
     */
    
    /*
    public static Map<Id, Id> retrieveAccountPricebook(Set<Id> shipToIds) {
     System.debug('=====shipToIds===='+shipToIds);
        Map<Id, Id> shipToPricebookMap = new Map<Id, Id>();

        // Ship-tos with a pricebook
        Map<Id, Account> shipTosMapWithPricebook = new Map<Id,Account>([SELECT Id, EP_PriceBook__c 
            FROM Account 
            WHERE Id IN :shipToIds AND EP_PriceBook__c <> NULL limit 10000 ]);


         System.debug('=====shipTosMapWithPricebook===='+shipTosMapWithPricebook);
        // Ids of the shipTos without a pricebook
        Set<Id> shipToIdsWithoutPricebook = new Set<Id>(shipToIds);
        shipToIdsWithoutPricebook.removeAll(shipTosMapWithPricebook.keySet());
        
        System.debug('=====shipToIdsWithoutPricebook===='+shipToIdsWithoutPricebook);

        // Get the set of SellToIds for the ShipTos without a pricebook
        Map<Id, Account> shipTosMapWithoutPricebook = new Map<Id,Account>([SELECT Id, 
            ParentId, Parent.EP_PriceBook__c
            FROM Account 
            WHERE Id IN :shipToIdsWithoutPricebook limit 10000]);
            
        System.debug('=====shipTosMapWithoutPricebook===='+shipTosMapWithoutPricebook);

        // Set with all the Ids of all the sell-tos associated to a shipTo that don't have a pricebook
        Set<Id> sellToIdForShipTosWithoutPricebook = new Set<Id>();
        for(Account shipToWithoutPricebook : shipTosMapWithoutPricebook.values()) {
            sellToIdForShipTosWithoutPricebook.add(shipToWithoutPricebook.ParentId);
        }


         System.debug('=====shipTosMapWithoutPricebook===='+shipTosMapWithoutPricebook);
         
        // Get a map that contains the bill to info for all the ship to without a specified pricebook
        Map<Id, Account> sellTosMapForShipTosWithoutPricebook = new Map<Id,Account>([SELECT Id, 
            EP_PriceBook__c
            FROM Account 
            WHERE Id IN :sellToIdForShipTosWithoutPricebook limit 20000]);

         System.debug('=====sellTosMapForShipTosWithoutPricebook===='+sellTosMapForShipTosWithoutPricebook);
         
          System.debug('=====shipToIds===='+shipToIds);
         
        // Iterate on the original set of ShipToIds and build the result
        for(Id shipToId : shipToIds) {
            // Ship-to with an associated pricebook
            if(shipTosMapWithPricebook.containsKey(shipToId) 
                && shipTosMapWithPricebook.get(shipToId) != null) {
                    shipToPricebookMap.put(shipToId, shipTosMapWithPricebook.get(shipToId).EP_PriceBook__c);
            } else {
                // Get the SellTo associated to the ShipTo 
                Account shipTo = shipTosMapWithoutPricebook.get(shipToId);
                if(shipTo != null) {
                    Id sellToId = shipTo.ParentId;

                    // Get the Bill-To associated to the Sell-To, if any
                    Account sellTo = sellTosMapForShipTosWithoutPricebook.get(sellToId);
                    if(sellTo != null) {    
                        if(sellTo.EP_PriceBook__c != null) {
                            shipToPricebookMap.put(shipToId, sellTo.EP_PriceBook__c);
                        } else {
                            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(CLASSNAME, METHODNAME,SEVERITY,TRANSACTION_ID,DESCRIPTION1));
                        }
                    } else {
                        EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(CLASSNAME, METHODNAME,SEVERITY,TRANSACTION_ID,DESCRIPTION2));
                    }

                } else {
                    EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(CLASSNAME, METHODNAME,SEVERITY,TRANSACTION_ID,DESCRIPTION3));
                }
            }
        }
        
        System.debug('======shipToPricebookMap====='+shipToPricebookMap);
        return shipToPricebookMap;
    }

    public static Map<Id, Id> retrieveAccountPricebook(List<Account> shipTos) {
        system.debug('>>>> ShipTos size :' + shipTos.size());
        system.debug('>>>> ShipTos :' + shipTos);
        if(shipTos != null && shipTos.size() > 0) {
            Set<Id> shipToIds = new Set<Id>();
            for(Account shipTo : shipTos) {
                shipToIds.add(shipTo.Id);
            }
            system.debug('>>>> ShipToIds : ' + shipToIds);
            return retrieveAccountPricebook(shipToIds);
        } else {
            return null;
        }
    }
     */
    
    
    //---------------------------------Added by Manisha On 04 Jan 2018-------------------------------
    public static Map<Id, Set<Id>> retrieveAccountPricebook(Set<Id> shipToIds) {
        
        List<Account> shipTos=[SELECT ParentID FROM ACCOUNT WHERE ID IN :shipToIds];
        
       Set<Id> sellToIDs = new Set<Id>();
        for(Account shipTo : shipTos) {
            sellToIDs.add(shipTo.ParentId);
        }
        Map<Id, Set<Id>> selltoPriceBookIdMap = new Map<Id, Set<Id>>();

     for(EP_Product_Option__c productOptions : [SELECT  Sell_To__c,Price_List__r.Id FROM EP_Product_Option__c WHERE Sell_To__c IN :sellToIDs]) {
         if(selltoPriceBookIdMap.containsKey(productOptions.Sell_To__c)) {
             Set<Id> pricebookId = selltoPriceBookIdMap.get( productOptions.Sell_To__c);
             pricebookId.add(productOptions.Price_List__c);
             selltoPriceBookIdMap.put(productOptions.Sell_To__c, pricebookId);
         } else {
             selltoPriceBookIdMap.put(productOptions.Sell_To__c, new Set<Id> { productOptions.Price_List__c });
         }
     }
     Map<Id,Set<ID>> shipToPricebookIdMap =new  Map<Id,Set<ID>>();
     for(Account shipTo : shipTos) {
            if(selltoPriceBookIdMap.containsKey(shipTo.ParentId))
            {
                 Set<Id> pricebookId = selltoPriceBookIdMap.get(shipTo.ParentId);
                shipToPricebookIdMap.put(shipTo.Id,pricebookId);
            }
        }

        return shipToPricebookIdMap;
        
    }
    
    public static Map<Id, Set<Id>> retrieveAccountPricebook(List<Account> shipTos) {
        system.debug('>>>> ShipTos size :' + shipTos.size());
        system.debug('>>>> ShipTos :' + shipTos);
        if(shipTos != null && shipTos.size() > 0) {
            Set<Id> shipToIds = new Set<Id>();
            for(Account shipTo : shipTos) {
                shipToIds.add(shipTo.Id);
            }
            system.debug('>>>> ShipToIds : ' + shipToIds);
            return retrieveAccountPricebook(shipToIds);
        } else {
            return null;
        }
    }
    //---------------------------------------------------END-----------------------------------------------
    
    
    
    /**
     *  List of Ship-Tos, return the related pricebooks
     */


    /*
     *  Return the details of Bill-Tos
     *
     *  @param billToIds Set of IDs of bill Tos
     *  @return map of bill to ID and bill to info
     */
    public static Map<Id, Account> getBillTos(Set<Id> billToIds) {
        Map<Id, Account> billToList = null;
        try {
            billToList = new Map<Id, Account>((List<Account>) 
                Database.query('SELECT Name, AccountNumber, EP_Account_Company_Name__c, '
                + ' BillingAddress, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet ' 
                + ' FROM Account '
                + ' WHERE Id IN :billToIds')); // Some of the BillTos might be SellTo RT
        } catch(Exception e) {
            throw e;
        }
        return billToList;
    }
    
        /*
     *  Return the details of Bill-Tos by AccountNumber
     *
     *  @param billToAccountNumbers Set of AccountNumbers of bill Tos
     *  @return map of bill to AccountNumber and bill to info
     */
    /*public static Map<String, Account> getBillTosByAccountNumber(Set<String> billToAccountNumbers) {
        Map<String, Account> billToList = new Map<String, Account>();
        try {
            // Some of the BillTos might be SellTo RT
            for(Account billTo : Database.query('SELECT Name, AccountNumber, EP_Account_Company_Name__c, '
                + ' BillingAddress, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet ' 
                + ' FROM Account '
                + ' WHERE AccountNumber IN :billToAccountNumbers')) {
                
                billToList.put(billTo.AccountNumber, billTo);
            }
        } catch(Exception e) {
            throw e;
        }
        return billToList;
    }*/

    /*
     *  Return a set of orders
     *
     *  @param orderIds set of order ids
     *  @return map of orders
     */
    public static Map<Id, csord__Order__c> getOrders(Set<Id> orderIds) {
        
        try {
            return new Map<Id, csord__Order__c>([SELECT TotalAmount__c, EP_Expected_Delivery_Date__c, EP_Requested_Delivery_Date__c, 
                                        OrderNumber__c, EP_Delivery_Type__c, EP_ShipTo__r.ShippingAddress, EP_ShipTo__r.ShippingCity, EP_ShipTo__r.ShippingCountry,
                                        EP_ShipTo__r.ShippingPostalCode, EP_ShipTo__r.ShippingState, EP_ShipTo__r.ShippingStreet, EP_Sell_To__c, EP_Is_Pre_Pay__c,
                                        EP_Bill_To__c, Stock_Holding_Location__r.EP_Ship_To__r.Name, EP_ShipTo__r.Name,
                                        (SELECT EP_Unit__c, Quantity__c, EP_Quantity_UOM__c, Product__r.Name, 
                                            Product__r.EP_Unit_of_Measure__c FROM csord__Order_Line_Items__r
                                            WHERE EP_Is_Taxes__c = FALSE AND EP_Is_Freight_Price__c = FALSE),
                                        Stock_Holding_Location__r.Stock_Holding_Location__r.Name,
                                        Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Slotting_Enabled__c
                                        FROM csord__Order__c
                                        WHERE Id IN :orderIds
                                        ORDER BY EP_Expected_Delivery_Date__c DESC
                                        LIMIT :EP_Common_Util.getQueryLimit()]);
        } catch(Exception e) {
            throw e;
        }
    }

    /*
     *  Return a set of orders
     *
     *  @param orderIds set of order ids
     *  @return map of orders
     */
    public static Database.SaveResult[] submitTankDips(List<EP_Tank_Dip__c> tankDips) {
        try {
            return Database.insert(tankDips, false);
        } catch(Exception e) {
            throw e;
        }
    }
 
 
    /*
     *  Give ability user to Edit Order, which hasn't been created by this User, but which has access to see it
     *
     *  @param Order orderToUpsert
     */    
    public static void upsertOrder(csord__Order__c orderToUpsert){
        Database.upsert(orderToUpsert);
    }
    
     /*********************************************************************************************
    *@Description : This is the method to return contactId From UserId                  
    *@Params      : 
    *@Return      :  string                                                                            
    *********************************************************************************************/ 
    public static string getContactIdFromUserId(){
        Profile userProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1];
        String userProfileName = userProfile.Name.toLowerCase();
        string ContactID;
          string userid=System.Label.EP_FE_User_Id;
        if(userProfileName.containsIgnoreCase(EP_FE_Constants.PROFILE_SYSTEM_ADMINISTRATOR))
        {
            ContactID=  [SELECT ContactId FROM USER WHERE ID =:userid].ContactId;
        }else
        {
            ContactID=  [SELECT ContactId FROM USER WHERE ID = :userinfo.getUserId() limit 1].ContactId;
            
        }
        //string ContactID=  [SELECT ContactId FROM USER WHERE ID ='0050k0000011fMU'].ContactId;
        return ContactID;
    }
    
 /*********************************************************************************************
    *@Description : This is the method to return associated AccountIds and AccountIds from Junction Object based on parameter 'SELLTO' or 'SHIPTO' 
          and AccountId and Called From Bootstrap or Orderlist .                  
    *@Params      : string , string , string 
    *@Return      :  Set<ID>                                                                             
    *********************************************************************************************/ 
    
    public static Set<ID> getAccountIds(string accountType, string accid, string strCalledFrom)
    {        
        Set<ID> accntIDs=new  Set<ID>();
        string ContactId=getContactIdFromUserId();
    List<EP_FE_PortalUserAccountAccess__c> lstPortalAccAcces;
        //string accid=[SELECT NAME,ACCOUNTID FROM CONTACT WHERE ID=: ContactId].ACCOUNTID;
        if(strCalledFrom=='Bootstrap')
        {
        lstPortalAccAcces =[SELECT EP_FE_Customer_Name__c,
                                                                   EP_FE_Sellto_Name__c,
                                                                   EP_FE_Shipto_Name__c 
                                                                   FROM EP_FE_PortalUserAccountAccess__c 
                                                                   WHERE  EP_FE_Customer_Name__c=:ContactId];
        }
        
        else if(strCalledFrom=='OrderList'){
        lstPortalAccAcces =[SELECT EP_FE_Customer_Name__c,
                                                                   EP_FE_Sellto_Name__c,
                                                                   EP_FE_Shipto_Name__c 
                                                                   FROM EP_FE_PortalUserAccountAccess__c 
                                                                   WHERE  EP_FE_Customer_Name__c=:ContactId AND EP_FE_Sellto_Name__c=:accid]; 
        }
        
        Map<Id, EP_FE_PortalUserAccountAccess__c> portalAccAccesMap = new Map<Id, EP_FE_PortalUserAccountAccess__c>();
        for(EP_FE_PortalUserAccountAccess__c oh : lstPortalAccAcces)
        {
            portalAccAccesMap.put(oh.EP_FE_Sellto_Name__c, oh);               
        }
        
        //Query all the associated shipto from  accid and add to the set accntIDs
        List<Account> lstshipToaccids;
        if((!portalAccAccesMap.containsKey(accid) && lstPortalAccAcces.size()>0) || lstPortalAccAcces.isEmpty())
        {
           
            lstshipToaccids=[SELECT ID FROM ACCOUNT WHERE PARENTID=: accid];
            for(Account accData : lstshipToaccids) {
                accntIDs.add(accData.ID);
            }
          
        }
        
        if(portalAccAccesMap.containsKey(accid))
        {
            lstshipToaccids=[SELECT ID FROM ACCOUNT WHERE PARENTID=: accid and OwnerId=:userinfo.getUserId()];
            for(Account accData : lstshipToaccids) {
                accntIDs.add(accData.ID);
            }
        }
        
        
       if(accountType=='SELLTO')
          accntIDs.add(accid);
        
        if(lstPortalAccAcces.size()!=0)
        {
            for(EP_FE_PortalUserAccountAccess__c accData : lstPortalAccAcces) {
                if(accountType=='SELLTO'){
                    accntIDs.add(accData.EP_FE_Sellto_Name__c);
                }else{
                    accntIDs.add(accData.EP_FE_Shipto_Name__c);
                }
                
            }
        }
        system.debug('==accntIDs=='+ accntIDs);
        return accntIDs;
        
    }  
    /*********************************************************************************************
    *@Description : This is the method to return AssociatedAccountId from USERID             
    *@Params      : 
    *@Return      :  string                                                                            
    *********************************************************************************************/ 
    public static string getAssociatedAccIDFromUserId(){
        Profile userProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId() LIMIT 1];
        String userProfileName = userProfile.Name.toLowerCase();
        string ContactID=getContactIdFromUserId();
           string Mainaccid=[SELECT NAME,ACCOUNTID FROM CONTACT WHERE ID=: ContactID].ACCOUNTID;
        return Mainaccid;
    }
}