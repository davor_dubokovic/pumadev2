/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToAccountSetupToActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 04-Account Set-up to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToAccountSetupToActive extends EP_AccountStateTransition {

    public EP_ASTStorageShipToAccountSetupToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToAccountSetupToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToAccountSetupToActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToAccountSetupToActive',' isGuardCondition');
        if(EP_Common_constant.SYNC_STATUS.equalsignorecase(this.account.localaccount.EP_Integration_Status__c)){
        	return true;  
        }    
        return false;
    }
}