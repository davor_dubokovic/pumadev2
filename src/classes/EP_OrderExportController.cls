/**
 * @author <Spiros Markantonatos>
 * @name EP_OrderExportController 
 * @description <This class is used as the controller of the 3rd party managed export page> 
 * @version <1.0>
*/
public with sharing class EP_OrderExportController 
{
    // Properties
    public String selectedOrderIds {get;set;}
    public List<String> lOrderIDs {get;set;}
    public List<Order> lSelectedOrders {get;set;}
    public List<Document> lExportDocuments {get;set;}
    
    public List<EP_Export_File_Type__mdt> lExportFileTypes {get;set;}
    public List<EP_Export_File_Column_Mapping__mdt> lExportFileColumns {get;set;}
    public List<SelectOption> lFileTypeOptions {get;set;}
    public String strSelectedFileType {get;set;}
    public static final String TXT_PLAIN = 'text/plain';
    public static final String CSV_TYPE ='csv';
    public static final String DATE_FRMT = 'dd-MM-yyyy HH:mm:ss';
    
    public Boolean blnDisplayExportButton {
        get {
            if (blnDisplayExportButton == NULL)
                blnDisplayExportButton = FALSE;
                
            return blnDisplayExportButton;
        }
        set;
    }
    
    public Boolean blnDisplayEmailButton {
        get {
            if (blnDisplayEmailButton == NULL)
                blnDisplayEmailButton = FALSE;
            return blnDisplayEmailButton;
        }
        set;
    }
    
    public String strEmailTemplateID {
        get {
            if (strEmailTemplateID == NULL)
            {
                List<EmailTemplate> lEmailTemplates = [SELECT Id FROM EmailTemplate 
                                                        WHERE DeveloperName = :STR_TEMPLATE_DEV_NAME 
                                                            LIMIT :EP_COMMON_CONSTANT.ONE];
                if (!lEmailTemplates.isEmpty())
                    strEmailTemplateID = lEmailTemplates[0].Id;
            }
            return strEmailTemplateID;
            
        }
        set;
    }
    
    public String strExportScope {
        get {
            if (strExportScope == NULL)
            {
                strExportScope = EXPORT_SCOPE;
            }
            return strExportScope;
        }
        set;
    }
    
    // Const
    private final String TRANSPORTER_ERROR_MSG = Label.EP_Third_Party_Transporter_Error;
    private final String NO_EXPORT_FILES_FOUND = Label.EP_No_Third_Party_Export_Definitions_Found + 
                                                        EP_Common_Constant.SPACE + 
                                                            Label.EP_Contact_Admin_Generic_Error;
    private final String STR_SUCCESS_MSG = Label.EP_Third_Party_Order_Export_Email_Success;
    private final String STR_ERROR_MSG = Label.EP_Third_Party_Order_Update_Fail_Message;
    private final String STR_TEMPLATE_DEV_NAME = Label.EP_Third_Party_Email_Template_Name;
    private final String STR_OIDS = Label.EP_Selected_Order_Variable_Name;
    private final String EXPORT_SCOPE = EP_Common_Constant.ORDER;
    
    // Error handling const variables
    private static final String CLASS_NAME = 'EP_OrderExportController';
    private static final String UPDATE_ORDERS_METHOD = 'updateOrders';
    private static final String EMAIL_EXPORT_FILES_METHOD = 'emailExportFiles';
    private static final String INITIALISE_METHOD = 'initialiseExportController';
    private static final String RETRIEVE_METHOD = 'retrieveFileDefinitions';
    private static final String GENERATE_CSV_METHOD = 'generateCSV';
    private static final String UPDATEORDER_METHOD = 'updateOrders';
    private static final String PREPARE_METHOD = 'prepareEmail';
    private static final String DISPLAYORDER_METHOD = 'displayOrderUpdateConfirmation';
    private static final String HANDLEEMAILERRORS_METHOD = 'handleEmailErrors';
    /* 
     Constructor
    */
    public EP_OrderExportController(Apexpages.StandardSetController stdSetCon) {
        
    } 
    
    /*
    * @ Description: This method will initialise the controller during page loading
    */
    public void initialiseExportController() {
        try{
            // Retrieve the selected order IDs from the relevant page variable
            String selectedOids = Apexpages.currentPage().getParameters().get(STR_OIDS);
            
            // If there are orders selected then retrieve the details
            // If there there are no orders selected then the VF page will display 
            // the appropriate message (logic part of the actual page)
            if(String.isNotBlank(selectedOids))
            {
                blnDisplayExportButton = TRUE; // If the user has selected orders, then display the export button
                
                // Initialise record variables
                lOrderIDs = selectedOids.split(EP_Common_Constant.COMMA);
                selectedOrderIds = EP_Common_Constant.BLANK;
                Set<ID> setTrasporterIDs = new Set<ID>();
                
                // Retrieve the file definitions from the custom metadata objects
                retrieveFileDefinitions();
                
                Integer nRows = EP_Common_Util.getQueryLimit();
                // Retrieve some generic order details to be displayed on the export page order table
                lSelectedOrders = [SELECT Id, OrderNumber, EffectiveDate, EP_Requested_Delivery_Date__c, 
                                    EP_Transporter__c, EP_Transporter__r.Name, EP_ShipTo__r.Name, Stock_Holding_Location__c
                                        FROM Order
                                            WHERE Id IN :lOrderIDs Limit :nRows];
                
                // Ensure that all selected orders are for the same transporter
                for(Order o : lSelectedOrders)
                {
                    setTrasporterIDs.add(o.EP_Transporter__c);
                }
                
                if(setTrasporterIDs.size() > 1)
                {
                    Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, TRANSPORTER_ERROR_MSG));  
                    blnDisplayExportButton = FALSE;
                } 
            } 
        }catch( Exception ex ){
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            INITIALISE_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }  
    }
    
    /*
    * @ Description: This method will retrieve the file definitions based on the selected file type
    */
    public void retrieveFileDefinitions() {
        try{
            // Initialise record variables
            lExportFileTypes = new List<EP_Export_File_Type__mdt>();
            lFileTypeOptions = new List<SelectOption>();
            lExportDocuments = NULL;
            
            List<String> lOrderExportTypeAPINames = new List<String>();
            Map<String, String> mapFileTypeMap = new Map<String, String>();
            
            Integer nRows = EP_Common_Util.getQueryLimit();
            // Retrieve the different types of export files for 3rd party managed orders
            for (EP_Export_File_Type__mdt ft : [SELECT Id, DeveloperName, MasterLabel, EP_Export_Type__c 
                                                        FROM EP_Export_File_Type__mdt
                                                            WHERE EP_Scope__c = :strExportScope Limit :nRows])
            {
                // Preselect the export file type if the selected one is blank
                if (String.isBlank(strSelectedFileType))
                {
                    strSelectedFileType = ft.EP_Export_Type__c;
                }
                // Populate the export file type list
                lExportFileTypes.add(ft);
                // Store the file type developer names, which is then used to retrieve the file columns 
                if (strSelectedFileType == ft.EP_Export_Type__c)
                {
                    lOrderExportTypeAPINames.add(ft.DeveloperName);
                }
                // Populate the export file type picklist values
                if (!mapFileTypeMap.containsKey(ft.EP_Export_Type__c))
                {
                    lFileTypeOptions.add(EP_Common_Util.createOption(ft.EP_Export_Type__c, ft.EP_Export_Type__c));
                    mapFileTypeMap.put(ft.EP_Export_Type__c, NULL);
                }
            }
            
            nRows = EP_Common_Util.getQueryLimit();
            // Retrieve the columsn for each type of export file
            lExportFileColumns = [SELECT Id
                                    FROM EP_Export_File_Column_Mapping__mdt
                                        WHERE EP_Export_File_Type_API_Name__c IN :lOrderExportTypeAPINames
                                            ORDER BY EP_Column_Order__c ASC Limit :nRows];
            
            // Throw an error if the system is not able to find any relevant file definations
            if (lExportFileColumns.isEmpty() || lOrderExportTypeAPINames.isEmpty())
            {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, NO_EXPORT_FILES_FOUND));  
                blnDisplayExportButton = FALSE;  
            }
        }catch( Exception ex ){
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            RETRIEVE_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }
    }
    
    /*
    * @ Description: This method will construct the CSV files and save them as documents
    */
    public void generateCSV() {
        try{
            EP_ExportCSVController fileExporter = new EP_ExportCSVController();
            lExportDocuments = new List<Document>();
            String strFileSuffix = System.Now().format(DATE_FRMT);
            String strBody;
            
            for (EP_Export_File_Type__mdt ft : lExportFileTypes)
            {
                fileExporter.strFileTypeID = ft.Id;
                fileExporter.listParams = lOrderIDs;
                strBody = fileExporter.generateCSV();
                if (String.isNotBlank(strBody))
                    // Store the CSV as a document to the running user's personal folder     
                    lExportDocuments.add(new Document(Name = ft.MasterLabel + EP_Common_Constant.SPACE + strFileSuffix + EP_Common_Constant.DOT+CSV_TYPE , 
                                                    Body = Blob.valueOf(strBody), 
                                                        ContentType = TXT_PLAIN , 
                                                            Type = CSV_TYPE ,
                                                                FolderId = UserInfo.getUserId()));
            }
            
            if (!lExportDocuments.isEmpty())
            {
                Database.insert(lExportDocuments);
                // Display the email button (if there are any documents avaialable)
                blnDisplayEmailButton = TRUE;
            }
        }catch( Exception ex ){
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            GENERATE_CSV_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }
    }
    
    /*
    * @ Description: This method will email the documents to the running user as attachments
    */
    public void emailExportFiles()
    {
        try{
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage eMail = prepareEmail();
            Messaging.SendEmailResult[] eMailResults = Messaging.sendEmail(new list<Messaging.SingleEmailMessage>{email});
            
            Boolean blnHasEmailFailed = handleEmailErrors(eMailResults);
            
            if(!blnHasEmailFailed)
            {
                Boolean blnOrderUpdatedSuccessfully = updateOrders();
                displayOrderUpdateConfirmation(blnOrderUpdatedSuccessfully);
            }
        } catch(exception ex) {
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            EMAIL_EXPORT_FILES_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }
    
    }
    
    /*
    * @ Description: This method is used to capture errors during email send
    */
    public Boolean handleEmailErrors(Messaging.SendEmailResult[] eMailResults) {
        Boolean blnHasEmailFailed = FALSE;
        try{
            String strErrorMessage;
            for(Messaging.SendEmailResult eMailResult : eMailResults)
            {
                if(!eMailResult.isSuccess())
                {
                    blnHasEmailFailed = TRUE;
                    strErrorMessage = EP_Common_Constant.BLANK;
                    for(Messaging.SendEmailError eMailError : eMailResult.getErrors())
                    {
                        strErrorMessage += eMailError.getMessage() + EP_Common_Constant.COMMA + EP_Common_Constant.SPACE;
                    }
                    strErrorMessage = strErrorMessage.removeEnd(EP_Common_Constant.COMMA + EP_Common_Constant.SPACE);
                    Apexpages.addMessage(EP_Common_Util.createApexMessage(Apexpages.Severity.ERROR, strErrorMessage));
                }
            }
        }catch( Exception ex ){
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            HANDLEEMAILERRORS_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }
        return blnHasEmailFailed;
    }
    
    /*
    * @ Description: This method is used to display the confirmation/upadte message update the order update operated has been completed
    */
    public void displayOrderUpdateConfirmation(Boolean blnOrderUpdatedSuccessfully) {
        try{
            if(blnOrderUpdatedSuccessfully)  
            {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.CONFIRM, STR_SUCCESS_MSG));
            } else {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, STR_ERROR_MSG));
            }
        }catch( Exception ex ){
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            DISPLAYORDER_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }
    }
    
    /*
    * @ Description: This method is called by the page when the user presses on the download file link and is used to call the method updating
     the order as exported. Change completed as part of defect 45019
    */
    // Defect 45019 - Start
    public PageReference markOrdersAsExported() {
      
      updateOrders();
      
      return NULL;
    }
    // Defect 45019 - End
    
    /*
    * @ Description: This method will update the "Is Exported" flag of the selected order, once they have been emailed to the running user
    */
    public Boolean updateOrders()
    {
        Boolean blnIsSuccess = FALSE;
        try{
            if (!lSelectedOrders.isEmpty())
            {
                for (Order o : lSelectedOrders)
                {
                    o.EP_3rd_Party_Managed_Exported__c = TRUE;
                }
                
                Database.update(lSelectedOrders);
                blnIsSuccess = TRUE;
            }
        }catch( Exception ex ){
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            UPDATE_ORDERS_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }
        return blnIsSuccess;
    }
    
    /*
    * @ Description: This method will prepare the email body and attach the files as attachments
    */
    public Messaging.SingleEmailMessage prepareEmail() {
        
        Messaging.SingleEmailMessage eMail = new Messaging.SingleEmailMessage();
        try{
            if (String.isNotBlank(strEmailTemplateID))
            {
                if (!lExportDocuments.isEmpty())
                {
                    eMail.setTargetObjectId(Userinfo.getUserId());
                    eMail.setTemplateId(strEmailTemplateID);
                    // Add attachements
                    List<Messaging.EmailFileAttachment> lAttachemnts = new List<Messaging.EmailFileAttachment>();
                    
                    for (Document d : lExportDocuments)
                    {
                        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                        attachment.setInline(FALSE);
                        attachment.setFileName(d.Name);
                        attachment.setContentType(d.ContentType);
                        attachment.setBody(d.Body);
                        lAttachemnts.add(attachment);
                    }
                    
                    eMail.setFileAttachments(lAttachemnts);
                    eMail.setSaveAsActivity(FALSE);
                }
            }
        }catch( Exception ex ){
            Apexpages.addMessages(ex);
            EP_LoggingService.logHandledException(ex, 
                                                    EP_Common_Constant.EPUMA, 
                                                        CLASS_NAME, 
                                                            PREPARE_METHOD, 
                                                                ApexPages.Severity.ERROR);
        }
        
        return eMail;
    }
}