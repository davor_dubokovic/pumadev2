/*********************************************************************************************
@Author <Prayank Sahu>
@name <EP_FE_PowerBIEndpoint>
@CreateDate <01/11/2018>
@Description <This class will provide AccessToken,Embed Token and Refresh token to get Report from Power BI  >  
@Version <1.0>
*********************************************************************************************/

@RestResource(urlMapping='/FE/V1/PowerBI/*')  

global with sharing class EP_FE_PowerBIEndpoint {
    
    public Static String validateResult;
    public Static String embed_tokenTest;
    global static final String CLASSNAME = 'EP_FE_PowerBIEndpoint';
    global static final String METHODNAME = 'generateEmbedToken';
    global static final String SEVERITY = 'ERROR';
    global static final String TRANSACTION_ID = '1';
    global static final String DESCRIPTION1 = 'ACCESS TOKEN NOT FOUND';
    
    /** API Call */
    
    @HttpGet
    webservice static EP_FE_PowerBIResponse GetEmbedToken() {
        
        EP_FE_PowerBIResponse embedTkn=new EP_FE_PowerBIResponse();
        DateTime dtexpdate;
        List<EP_FE_PowerBIToken__c> lstpbToken  = [SELECT Token_Expires_On__c FROM EP_FE_PowerBIToken__c ];
        if (lstpbToken.size()== 0)
        {
            system.debug('3');
            OAuthResult result = getAccessToken();
            embedTkn = generateEmbedToken(result.access_token); 
            try {
                DateTime dtExpirationDate = (DateTime) JSON.deserialize('"'+embedTkn.expiration+'"', DateTime.class);
                EP_FE_PowerBIToken__c powerBIToken = new EP_FE_PowerBIToken__c(Token__c=embedTkn.token,
                                                                               Token_Expires_On__c=dtExpirationDate,
                                                                               Token_Is_Active__c=true);
                insert powerBIToken;
                
                
            } catch(DmlException e) {
                System.debug('An unexpected error has occurred: ' + e.getMessage());
            }
            
        }else
        {
            
            
            EP_FE_PowerBIToken__c pbToken=[SELECT Token_Expires_On__c FROM EP_FE_PowerBIToken__c WHERE Token_Is_Active__c=true LIMIT 1 ];
            
            dtexpdate=pbToken.Token_Expires_On__c;//token expiration datetime
            
            DateTime dtNow=System.now();// currrent datetime
            
            system.debug('dtexpdate'+ dtexpdate);
            system.debug('==System.now()=='+ System.now());
            
            /*
           DateTime dt = (DateTime) JSON.deserialize('"2018-01-24T09:45:36.933"', DateTime.class);
			System.debug(dt); 
            */
            
            if(dtNow<=dtexpdate)
            {
                string activeToken=[SELECT Token_Is_Active__c, Token__c,
                                    Token_Expires_On__c FROM EP_FE_PowerBIToken__c 
                                    WHERE Token_Is_Active__c=true LIMIT 1 ].Token__c;// Active Token
                embedTkn.token=activeToken;
                system.debug('==token=='+ activeToken);
                system.debug('1');
            }
            else
            {
                system.debug('2');
                OAuthResult result = getAccessToken();
                embedTkn = generateEmbedToken(result.access_token); 
                system.debug('==embedTkn=='+ embedTkn);
                try {
                    DateTime dtExpirationDate = (DateTime) JSON.deserialize('"'+embedTkn.expiration+'"', DateTime.class);
                    
                    EP_FE_PowerBIToken__c powerBITokenToUpdate=[SELECT Token_Is_Active__c, Token_Expires_On__c 
                                                                FROM EP_FE_PowerBIToken__c WHERE Token_Is_Active__c=true LIMIT 1];
                    
                    powerBITokenToUpdate.Token_Is_Active__c=false;
                    update powerBITokenToUpdate;
                    
                    EP_FE_PowerBIToken__c powerBIToken = new EP_FE_PowerBIToken__c(Token__c=embedTkn.token,
                                                                                   Token_Expires_On__c=dtExpirationDate,
                                                                                   Token_Is_Active__c=true);
                    insert powerBIToken;
                    
                    
                } catch(DmlException e) {
                    System.debug('An unexpected error has occurred: ' + e.getMessage());
                }
                
            }
        }
        embedTkn.PowerBIURL=EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).PowerBI_URL__c + EP_FE_Constants.Groups + EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Group_Id__c + EP_FE_Constants.Reports + EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Report_Id__c ;
                embedTkn.GROUPID=EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Group_Id__c;
                embedTkn.REPORTID=EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Report_Id__c;
                system.debug('==embedTokenObj.PowerBIURL=='+ embedTkn.PowerBIURL);
                system.debug('== embedTokenObj.GROUPID=='+  embedTkn.GROUPID);
                system.debug('==embedTokenObj.REPORTID=='+ embedTkn.REPORTID);

        return embedTkn; 
    }    
    /*********************************************************************************************
    @Author <>
    @name <getAccessToken>
    @CreateDate <>
    @Description < >  
    @Version <1.0>
    *********************************************************************************************/    
    public Static OAuthResult getAccessToken() {
        EP_FE_PowerBIResponse powerBIres=new EP_FE_PowerBIResponse();
        
        String access_token_url = EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Access_Token_URL__c;
        String grant_type = EP_FE_Constants.GRANT_TYPE+EP_FE_Constants.EQUAL + EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).grant_type__c;
        String client_id = EP_FE_Constants.PBI_CLIENTID +EP_FE_Constants.EQUAL+ EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Client_Id__c;     
        String username = EP_FE_Constants.USERNAME +EP_FE_Constants.EQUAL+ EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).User_Name__c;
        String password = EP_FE_Constants.PASSWORD +EP_FE_Constants.EQUAL+ EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Password__c;
        String resource = EP_FE_Constants.RESOURCE +EP_FE_Constants.EQUAL+ EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Resource_URI__c;
        
        
        List<String> urlParams = new List<String> {grant_type, client_id ,username  ,password, resource };
            
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(access_token_url);
        req.setMethod(EP_FE_Constants.POST);
        req.setHeader(EP_FE_Constants.CONTENTTYPE, EP_FE_Constants.APPLICATIONENCODED);
        req.setHeader(EP_FE_Constants.ACCEPT, EP_FE_Constants.APPLICATIONJSON);
        String body = String.join(urlParams, EP_FE_Constants.AMPERSAND);
        req.setBody(body);
        HttpResponse res = h.send(req);
        OAuthResult oAinstance =new OAuthResult();
        if(res==null)
        {
            
            EP_FE_Utils.logError(EP_FE_PowerBIResponse.CLASS_NAME, EP_FE_PowerBIResponse.METHOD_GETACCESSTOKEN,
                                 EP_FE_PowerBIResponse.AREA, EP_FE_PowerBIResponse.FUNCTIONACCESSTOKEN, EP_FE_PowerBIResponse.ISSUE,
                                 null, EP_FE_PowerBIResponse.ERROR_FETCHING_PowerBIResponse, null); 
        }else
        {
            validateResult = res.getBody();
            
            oAinstance = (OAuthResult)(JSON.deserialize(res.getBody(), OAuthResult.class));
            
            String AccessToken = oAinstance.access_token;
            String RefreshToken = oAinstance.refresh_token;
            //powerBIres= generateEmbedToken(AccessToken);
            oAinstance= (OAuthResult)(JSON.deserialize(res.getBody(), OAuthResult.class));
        }
        
        return oAinstance;        
    }
    
    /*********************************************************************************************
    @Author <Manisha Chaudhary>
    @name <generateEmbedToken>
    @CreateDate <>
    @Description < >  
    @Version <1.0>
    *********************************************************************************************/    
    private  Static EP_FE_PowerBIResponse generateEmbedToken(String accessToken) {
        EP_FE_PowerBIResponse embedTokenObj=new EP_FE_PowerBIResponse();
        if(accessToken==null)
        {
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(CLASSNAME, METHODNAME,SEVERITY,TRANSACTION_ID,DESCRIPTION1));
        }else
        {
            String authorization = 'Bearer '+accessToken;
            system.debug('########'+authorization );
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            string Endpoint2=EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).PowerBI_URL__c + EP_FE_Constants.Groups + EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Group_Id__c + EP_FE_Constants.Reports + EP_FE_OAuthApps__c.getValues(EP_FE_Constants.ApplicationName).Report_Id__c + EP_FE_Constants.GENERATETOKEN; 
            //req.setEndpoint('https://api.powerbi.com/v1.0/myorg/groups/40205e46-30b9-4a67-9f7d-9a17ead9f438/reports/e31b1c21-f882-4f92-8539-fa72eeab2722/GenerateToken');
            system.debug('==Endpoint==' + Endpoint2);
            req.setEndpoint(Endpoint2);
            req.setMethod(EP_FE_Constants.POST);
            req.setHeader(EP_FE_Constants.CONTENTTYPE, EP_FE_Constants.APPLICATIONJSON);
            req.setHeader(EP_FE_Constants.Authorization, authorization);
            String body = '{"accessLevel": "View"}';
            req.setBody(body);
            HttpResponse res = h.send(req);
            
            if(res==null)
            {

                EP_FE_Utils.logError(EP_FE_PowerBIResponse.CLASS_NAME, EP_FE_PowerBIResponse.METHOD_GENERATEEMBEDTOKEN,
                                     EP_FE_PowerBIResponse.AREA, EP_FE_PowerBIResponse.FUNCTION, EP_FE_PowerBIResponse.ISSUE,
                                     null, EP_FE_PowerBIResponse.ERROR_FETCHING_PowerBIResponse, null); 
            }else
            {
                embedTokenObj = (EP_FE_PowerBIResponse)(JSON.deserialize(res.getBody(), EP_FE_PowerBIResponse.class));
                
                embed_tokenTest = embedTokenObj.token;
                
            }
        }
        
        return embedTokenObj;  
    }
    
    
    
    public class OAuthResult {
        /** The access token */
        public String access_token {get; set;}
        
        /** The refresh token */
        public String refresh_token {get; set;}
        
        /** The token expiry date*/
        public String expires_on {get;set;}
    }
    
    /** The JSON result from a successful oauth call */
    
    
    
}