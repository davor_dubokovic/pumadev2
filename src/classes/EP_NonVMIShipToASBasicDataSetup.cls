/*
*  @Author <Accenture>
*  @Name <EP_NonVMIShipToASBasicDataSetup>
*  @CreateDate <>
*  @Description <VMI Ship To Account State for 02-Basic Data Setup Status>
*  @Version <1.0>
*/
public with sharing class EP_NonVMIShipToASBasicDataSetup extends EP_AccountState{
    
    public EP_NonVMIShipToASBasicDataSetup() {
        
    }
    
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBasicDataSetup','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
    /**
    * @author <Accenture>
    * @name doOnEntry
    */ 
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBasicDataSetup','doOnEntry');
        //L4_45352_START
         /*defect 78616 start*/
         EP_AccountService service = new EP_AccountService(this.account);
        /*defect 78616 end*/
        if(service.isSellToSynced() && !EP_IntegrationUtil.ISERRORSYNC){
            if(!this.account.localaccount.EP_Synced_PE__c){//WP2-Pricing Engine Callout changes
                service.doActionSyncCustomerToPricingEngine();
            }            
            else if(!this.account.localaccount.EP_Synced_NAV__c){//WP2-Pricing Engine Callout changes
                system.debug('-Calling--NAV--');
                service.doActionSendCreateRequestToNav();
            }
            else if(this.account.localaccount.EP_Synced_NAV__c && !this.account.localaccount.EP_Synced_WinDMS__C){
                system.debug('-Calling--WinDMS--');
                service.doActionSendCreateRequestToWinDMS();
            }
        }
    }
    //L4_45352_END 
    /**
    * @author <Accenture>
    * @name doOnExit
    */ 
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBasicDataSetup','doOnExit');
        
    }
    /**
    * @author <Accenture>
    * @name doTransition
    */ 
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBasicDataSetup','doTransition');
        return super.doTransition();
    }
    /**
    * @author <Accenture>
    * @name isInboundTransitionPossible
    */ 
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBasicDataSetup','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();
        
    }
    
}