/*
 *  @Author <Accenture>
 *  @Name <EP_AccountStateTransition >
 *  @CreateDate <03/02/2017>
 *  @Description <Account State transitions has virtual methods to get registered events and transitions from the custom setting.
 *                Validates if the state transition is possible>
 *  @Version <1.0>
 */
 
 public virtual class EP_AccountStateTransition {
    public EP_AccountDomainObject account;
    public EP_AccountEvent accountEvent;
    Integer priority;
    public String finalState;
    
    public EP_AccountStateTransition(){
    }

    //To override finalState
    public EP_AccountStateTransition(String resultState){
        this.finalState = resultState;
    }
    
    public virtual boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_AccountStateTransition','isTransitionPossible');
        //Evaluate the Guard conditions of whether this transition should be fired or not.
        if (isRegisteredForEvent()){
            return isGuardCondition();
        }
        return false;        
    }
    
    public virtual boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_AccountStateTransition','isRegisteredForEvent');
        string entityType =  account.localaccount.EP_Entity_Type__c;
		string stateName = account.localaccount.EP_Status__c;
		string eventname = accountEvent.getEventName();
        //Bulkification Implementation -- Start
        EP_CustomSettingMapper customSettingMapper = new EP_CustomSettingMapper();
        customSettingMapper.listStateTransitions = account.listStateTrans;
        List<EP_State_Transitions__c> transitionList = customSettingMapper.getStateTransitionsAccount(entityType, stateName, eventname);
        //Bulkification Implementation -- End
        if(!transitionList.isEmpty() && transitionList.size() > 0){
            return true;
        } 
        return false;       
    }

    //Check if the destination state allows this transition
    public virtual boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_AccountStateTransition','isGuardCondition');
        return false;       
    }

    public void setAccountContext(EP_AccountDomainObject currentAccount,EP_AccountEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_AccountStateTransition','setAccountContext');
        account = currentAccount;
        accountEvent = currentEvent;
    }
    
    public void setPriority(Integer newValue){
        EP_GeneralUtility.Log('Public','EP_AccountStateTransition','setPriority');
        priority = newValue;
    }

    public Integer getPriority(){
        EP_GeneralUtility.Log('Public','EP_AccountStateTransition','getPriority');
        return priority;
    }
    
    public virtual void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_AccountStateTransition','doOnExit');        
    }    
}