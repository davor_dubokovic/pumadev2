public class EP_WINDMSOrderUpdateStub {
/* Class for MSG */
	public MSG MSG;

	/* Stub Class for MSG */
	public class MSG {
		public HeaderCommon HeaderCommon;
		public Payload Payload;
		public String StatusPayload;
	}

	/* Stub Class for HeaderCommon */
	public class HeaderCommon extends EP_MessageHeader {}

	/* Stub Class for Payload */
	public class Payload {
		public Any0 any0;
	}

	/* Stub Class for Any0 */
	public class Any0 {
		public OrderStatuses OrderStatuses;
	}

	/* Stub Class for OrderStatus */
	public class OrderWrapper {
		public csord__Order__c sfOrder;
		public string recordType;
		public integer currentNoOfLineItems;
		public string errorCode;
		public string errorDescription;
		public String seqId;
		public Identifier Identifier;
		public String noOfLineItems;
		public String orderStatusWinDms;
		public String orderDlvryStartDt;
		public String reasonCode;
		public String reason ;
		public String clientId;
	}
    
    /* Stub Class for OrderStatuses */
    public class OrderStatuses {
        public String seqId;
        public String tripId;
        public List<OrderWrapper> orderStatus;
    }

    /* Stub Class for Identifier */
    public class Identifier {
        public String orderIdWinDMS;
        public String orderIdSf;
        public String orderRefNr;
    }
}