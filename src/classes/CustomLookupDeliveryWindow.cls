global  with sharing class CustomLookupDeliveryWindow extends cscfga.ALookupSearch {
    //private String OBJECT_NAME_CONST = 'Company__c';
    private static final String COMPANY = 'Company';
    
      
   	public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
	    String companyId = searchFields.get(COMPANY);
	    List <Company__c> deliveryWindowList = [SELECT id,name,EP_Delivery_Window__c FROM Company__c WHERE id =: companyId];
        System.debug('*******companyId' + companyId);
        System.debug('*******' + JSON.serializePretty(searchFields));
        if(deliveryWindowList == null){
            return null;
        }
        return deliveryWindowList;
    }  
 global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
       String companyId = searchFields.get(COMPANY);
	    List <Company__c> deliveryWindowList = [SELECT id,name,EP_Delivery_Window__c FROM Company__c WHERE id =: companyId];
        System.debug('*******companyId' + companyId);
        System.debug('*******' + JSON.serializePretty(searchFields));
        if(deliveryWindowList == null){
            return null;
        }
        return deliveryWindowList;
    }
    public override String getRequiredAttributes(){
        return '["' + COMPANY + '"]';
       // return '["Company"]'; 
    }
}