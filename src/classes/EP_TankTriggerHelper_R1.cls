/* 
@Author <Ashok Arora>
@name <EP_TankTriggerHelper>
@CreateDate <03/11/2015>
@Description <This class handles requests from Tank Trigger Handler> 
@Version <1.0>
*/
public with sharing class EP_TankTriggerHelper_R1 {
    private static final String LS = 'LS';
    private static final String TANK_EDIT = 'TANK_EDIT';
    private static final string SFDC_TO_WINDMS_TANK_CREATION ='SFDC_TO_WINDMS_TANK_CREATION';
    private static final String SYNC_TANKS_WITH_WINDMS_MTD = 'syncTanksWithWinDMS';
    private static final String SYNC_SFDC_TO_WINDMS_TANKS_MTD = 'syncSFDCToWinDMSTanks';
    private static final String CLASSNAME = 'EP_TankTriggerHelper_R1';
    private static final String SET_CURRENCY_ON_TANK_MTD = 'setCurrencyOnTank';
    private static final String HANDLE_PLACEHOLDER_MTD = 'handlePlaceholderTankDipsBasedOnTankStatus';
    private static final String VALIDATE_TANK_CODE = 'validateTankCode';
    private static final String IS = ' is';
    private static final String ARE = ' are';
    private static final String NOT_ALLWD_FOR_MODIFICATION = ' not allowed for modification';
    private static final string CMPLT_CSC_RVW_MTD = 'CmpltCscReviewBeforeBasicSetUp';
    private static final string UPDATE_MISSING_TANK_DIP_DAYS = 'updateMissingTankDipDays';
     /**** Defect 44565 Fix START****/
    private static final string RESTRICT_USER_TO_UPDATE_TANK_METHD= 'restrictUserToUpdateTankWhenActionNotAssigned';
    /**** Defect 44565 Fix END****/
    private static set<String> actionStatus  = new set<String>{EP_Common_Constant.ACT_NEW_STATUS, 
        EP_Common_Constant.ACT_ASSIGNED_STATUS};
    
    /*
    This method maps currency on Tank from Ship To
    */
    public static void setCurrencyOnTank(List<EP_Tank__c>lNewTanks){
        map<Id,String>mShipToCurrencyCode = new map<id,String>();
        set<Id>sShipTo = new set<Id>();
        try{
            for(EP_Tank__c tank:lNewTanks ){
                sShipTo.add(tank.EP_Ship_To__c);
            }
            integer nRows = EP_Common_Util.getQueryLimit();   
            for(Account shipTo : [SELECT Id 
            ,CurrencyISOCode
            FROM Account 
            WHERE Id IN :sShipTo LIMIT: nRows]){
                mShipToCurrencyCode.put(shipTo.Id,shipTo.CurrencyISOCode);
            }
            for(EP_Tank__c tank:lNewTanks ){
                tank.currencyISOCode = mShipToCurrencyCode.get(tank.EP_Ship_To__c);
            }
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA,SET_CURRENCY_ON_TANK_MTD, CLASSNAME, ApexPages.Severity.ERROR );
        }
        
    }
    
    /**** Defect 44565 Fix START****/
    /**
     * @author <Amit Singh>
     * @description <This method is used to restrict user to edit tank record if there is any open action which is not assigned to him/her>
     * @name <restrictUserToUpdateTankWhenActionNotAssigned>
     * @date <03/01/2017>
     * @param 1.List<EP_Tank__c> lNewTanks - List Of Tanks(Trigger.New)
     *		  2.Map<Id, EP_Tank__c> mapOldTank - trigger.oldMap
     * @return void
     */
    public static void restrictUserToUpdateTankWhenActionNotAssigned(List<EP_Tank__c> lNewTanks, 
    																	Map<Id, EP_Tank__c> mapOldTank){
    
    	//There will be single open action at a time on tank so creating Map<Id, EP_Tank> 
    	Map<Id, EP_Action__c> mapTankAction = new Map<Id, EP_Action__c>();
    	Set<Id> setTankIds  = new Set<Id>();
    	try{
	    	if(mapOldTank != null && !mapOldTank.isEmpty()){
	    		
	    		for(EP_Tank__c tnk : lNewTanks){
	    			//1.If Parent Owner Email Field is updating do not run Validation
	    			//2.Do Not run validation for New ANd Basic Data setup Status
	    			if(tnk.EP_Tank_Parent_Owner_Email__c == mapOldTank.get(tnk.id).EP_Tank_Parent_Owner_Email__c 
	    				&& !EP_Common_Constant.NEW_TANK_STATUS.EqualsIgnoreCase(tnk.EP_Tank_Status__c) 
	    				&& !EP_Common_Constant.BASIC_DATA_SETUP.EqualsIgnoreCase(tnk.EP_Tank_Status__c)){
	    				setTankIds.add(tnk.id);
	    				  
	    			}
	    		}
				if(!setTankIds.isEmpty()){
			    	Integer nRows = EP_Common_Util.getQueryLimit();
			    	// Query Open Action related to Tanks
			    	for(EP_Action__c act: [select id, Name, EP_Tank__c, EP_Status__c, EP_Account__c, OwnerID
			    							 from EP_Action__c 
			    							Where EP_Tank__c in : setTankIds
					    					  AND EP_Status__c in : actionStatus
					    						  Limit: nRows ]){
			    		mapTankAction.put(act.EP_Tank__c, act);
			    	}
			    	
			    	if(!mapTankAction.isEmpty()){
			    		Id loggedInUserId = UserInfo.getUserId();
			    		Id loggedInUserProfileId  = UserInfo.getProfileId();
			    		Map<Id, Profile> profileMap = new Map<Id, profile>([Select id, Name from Profile
			                                    Where Name = :EP_Common_Constant.ADMIN_PROFILE or Name =: EP_Common_Constant.INTEG_USER_PROFILE
			                                    limit 10000]);            
			    		if(!profileMap.containsKey(loggedInUserProfileId)){
				    		for(EP_Tank__c tnk : lNewTanks){
				    			if(setTankIds.contains(tnk.id)
					    				&& mapTankAction.containsKey(tnk.id) 
					    				&& mapTankAction.get(tnk.id).ownerId != loggedInUserId){
				    				tnk.addError(system.label.EP_Tank_Edit_Error_Message_If_Action_Not_Assigned);
				    			}
				    		}
			    		}
			    	}
				}
			}
    	}Catch(Exception handledException){
    		EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA, RESTRICT_USER_TO_UPDATE_TANK_METHD, CLASSNAME, ApexPages.Severity.ERROR );
    	}
    	
    }
    /**** Defect 44565 Fix END****/
    
    /**
    * @description : This method validates the selected product on tank record.
    *                selected prodct should be available in pridcut list(Pricebook) 
    *                which are attached on sell to.
    **/ 
    public static void validateProduct(List<EP_Tank__c>lNewTanks){
        integer nRows;
        set<ID> shipToIDs = new  set<ID>();
        //Add ship to IDs in a set
        for(EP_Tank__c tnk : lNewTanks){
            shipToIDs.add(tnk.EP_Ship_To__c);                                   
        }   
        // Map for ship-to and Pricebook mapping
        Map<ID, ID> pbShipToMap = new Map<ID, ID>();
        
        if(!shipToIDs.isEmpty()){
            nRows = EP_Common_Util.getQueryLimit();
            for(Account acc: [Select id, ParentID, Parent.EP_PriceBook__c from Account where id in : shipToIDs  LIMIT :nRows]){
                if(acc.parentID != null && acc.Parent.EP_PriceBook__c != null){
                    pbShipToMap.put(acc.id, acc.Parent.EP_PriceBook__c);
                }
            }
            // Map for Pricebook and its related Products
            Map<Id, Set<Id>> pbProductMap = new Map<Id, Set<Id>>();
            Set<Id> emptyIdSet = new Set<Id>();
            if(!pbShipToMap.isEmpty()){
                nRows = EP_Common_Util.getQueryLimit();
                for(PricebookEntry  pbe : [Select id, IsActive, Pricebook2Id, Product2Id 
                from PricebookEntry 
                where Pricebook2Id in : pbShipToMap.values()  LIMIT :nRows]){
                    if(!pbProductMap.containsKey(pbe.Pricebook2Id)){
                        pbProductMap.put(pbe.pricebook2Id, emptyIdSet ); //new Set<Id>()); -> commented as per novasuite fix
                    }
                    pbProductMap.get(pbe.pricebook2Id).add(pbe.Product2Id);
                }
            }
            try{
            for(EP_Tank__c tnk : lNewTanks){
                
                if(pbShipToMap.containsKey(tnk.EP_Ship_To__c) && tnk.EP_Product__c != null && 
                        pbProductMap.containsKey(pbShipToMap.get(tnk.EP_Ship_To__c)) &&
                        !pbProductMap.get(pbShipToMap.get(tnk.EP_Ship_To__c)).contains(tnk.EP_Product__c)){
                    tnk.addError(system.label.EP_Tank_Product_Not_In_Product_List_Error);
                }                   
            }
            }
            catch (exception exp){
                string str= (exp.getMessage());  
          }
            
        }
    }

    
    /*This method is used to allow modification on Tank but only for the fields which are specified in the field set*/
    public static void allowModificationOnTank(List<EP_Tank__c>lNewTanks
    ,map<id,EP_Tank__c>mOldTanks){
        //String objectAPIName;
        //String objectName = EP_Common_Constant.TANK_OBJ;
        //Id TankRecordType ;
        SObjectType objToken = Schema.getGlobalDescribe().get(EP_Common_Constant.TANK_OBJ);
        DescribeSObjectResult objDef = objToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap();
        Set<String> fieldSet = new set<String>();
        //List<String> fieldNames = new List<String>();
        //TankRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.TANK_OBJ,EP_Common_Constant.RETAIL_SITE_TANK);
        Set<string> fieldsToCheck = new Set<string>();
        
        for(Schema.FieldSetMember fld :SObjectType.EP_Tank__c.FieldSets.EP_AllowedfieldsforTank.getFields()) {
            fieldSet.add(fld.getFieldPath());
        }
        Map<String,String> fieldLabelMap = new Map<String,String>();
        for(String s : fields.keySet()){
            SObjectField fieldToken = fields.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();
            if (selectedField.isUpdateable()){
                fieldsToCheck.add(selectedField.getName());
                fieldLabelMap.put(selectedField.getName(),selectedField.getLabel());
            }
        }
        //boolean updateFlag = true;
        //String FieldToName = EP_Common_Constant.BLANK;
        try{
        for(EP_Tank__c tank : lNewTanks){
            if(EP_Common_Constant.TANK_OPERATIONAL_STATUS.equalsIgnoreCase(tank.EP_Tank_Status__c) ){
                tank.EP_Reason_Blocked__c  = EP_Common_Constant.BLANK;
            }
            
            
            //updateFlag = true;
            //FieldToName = EP_Common_Constant.BLANK;
            /*if(tank.recordTypeID == TankRecordType){
                for(String s : fieldsToCheck){
                    if(!fieldSet.contains(s) && tank.get(s) != mOldTanks.get(tank.id).get(s)){
                        updateFlag = false;
                        FieldToName += EP_Common_Constant.COMMA+ FieldLabelMap.get(s);    
                    }
                }
            }
            if(!updateFlag){
                FieldToName = FieldToName.substring(1);
                if(FieldToName.split(EP_Common_Constant.COMMA).size() == 1){
                    FieldToName = FieldToName +   IS;
                }else{
                    FieldToName = FieldToName + ARE;
                }
                tank.addError(FieldToName +  NOT_ALLWD_FOR_MODIFICATION);
            }*/
        }
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }
    } 
    
    /*This method will create place holder tank dips based on the tank status*/
    public static void handlePlaceholderTankDipsBasedOnTankStatus(Map<ID, EP_Tank__c> mapOldTanks, List<EP_Tank__c> lOldTanks) {
        List<EP_Tank_Dip__c> lTankDipsForInsertion = new List<EP_Tank_Dip__c>(); 
        List<EP_Tank_Dip__c> lTankDipsForDeletion = new List<EP_Tank_Dip__c>(); 
        //List<EP_Tank_Dip__c> lTankDipsForUpdation = new List<EP_Tank_Dip__c>(); 
        List<EP_Tank_Dip__c> lTankDipsForStatusUpdation = new List<EP_Tank_Dip__c>();
        //List<Order> lOrderStatusUpdation = new List<Order>();   
        integer nRows = EP_Common_Util.getQueryLimit();
        Set<ID> setTankIDs = new Set<ID>();
        List<ID> lTankStatusDecommissonIDs = new List<ID>();
        
        // Fix for Defect 30169 - Start
        Boolean blnInsertPlaceholderTankDip;
        // Fix for Defect 30169 - End
        
        try{
            // Handle existing placeholder tank dips only when the status of the tank is changed
            for (EP_Tank__c t : lOldTanks) {
                if (mapOldTanks.get(t.Id).EP_Tank_Status__c != t.EP_Tank_Status__c) {
                    // Get all the tanks which having new status as Decomissioned
                    if(t.EP_Tank_Status__c.equalsIgnoreCase(EP_Common_Constant.TANK_DECOMISSIONED_STATUS))
                    {
                        lTankStatusDecommissonIDs.add(t.Id);
                    }
                    setTankIDs.add(t.ID);
                }
            } // End for
            
            if (!setTankIDs.isEmpty()) {
                // Retrieve tank details needed to create placeholder tank dips
                List<EP_Tank__c> lTanks = [SELECT ID, EP_Tank_Status__c, /* #L4_45352_Start*/ EP_UnitOfMeasure__c /* #L4_45352_END*/, EP_Ship_To__r.EP_Status__c, EP_Ship_To__r.EP_Is_Ship_To_Open_Today__c,
                EP_Tank_Dip_Entry_Mode__c, EP_Ship_To__r.EP_Ship_To_Current_Date_Time__c
                FROM EP_Tank__c WHERE ID IN :lOldTanks limit: nRows];
                
                
                // Fix for Defect 30169 - Start
                for (EP_Tank__c t : lTanks) {
                    blnInsertPlaceholderTankDip = FALSE; // Initialise the add placeholder dip variable
                    
                    // If the new status of the tank is "Operational" and the Ship-To is "Active" and the Ship-To Tank Dip Entry mode is "Portal
                    // then create a placeholder tank dip
                    if ((!t.EP_Tank_Status__c.equalsIgnoreCase(EP_Common_Constant.TANK_DECOMISSIONED_STATUS)) 
                            && (t.EP_Ship_To__r.EP_Status__c.equalsIgnoreCase(EP_Common_Constant.ACTIVE_SHIP_TO_STATUS) || t.EP_Ship_To__r.EP_Status__c.equalsIgnoreCase(EP_Common_Constant.STATUS_BLOCKED))
                            && t.EP_Tank_Dip_Entry_Mode__c.equalsIgnoreCase(EP_Common_Constant.SHIP_TO_TANK_DIP_PORTAL_ENTRY_MODE)
                            && t.EP_Ship_To__r.EP_Is_Ship_To_Open_Today__c) {
                        
                        blnInsertPlaceholderTankDip = TRUE;
                        
                    }
                    
                    // Ensure that the previous status was not "Operational"
                    if (mapOldTanks.containsKey(t.Id))
                    {
                        if (mapOldTanks.get(
                                    t.Id).EP_Tank_Status__c.equalsIgnoreCase(
                                    EP_Common_Constant.OPERATIONAL_TANK_STATUS))
                        {
                            blnInsertPlaceholderTankDip = FALSE;
                        }
                    }
                    
                    if (blnInsertPlaceholderTankDip)
                    {
                        lTankDipsForInsertion.add(EP_PortalLibClass_R1.createPlaceholderTankDipRecord(t));
                    }
                } // End for
                // Fix for Defect 30169 - End
            } // End set size check
            
            
            // 1. Create any new placeholder tank dips for operational tanks
            if (!lTankDipsForInsertion.isEmpty()) {
                Database.SaveResult[] result = Database.insert(lTankDipsForInsertion, FALSE);
            } // End insertion record count check
            
            
            // Deletion of tank dips if the status of Tank is changed to Decommissioned  
            if(!lTankStatusDecommissonIDs.isEmpty())
            {   nRows = EP_Common_Util.getQueryLimit();      
                lTankDipsForDeletion = [SELECT ID From EP_Tank_Dip__c where EP_Tank__c IN: lTankStatusDecommissonIDs limit : nRows];
                if (!lTankDipsForDeletion.isEmpty())
                {           
                    Database.DeleteResult[] result = Database.delete(lTankDipsForDeletion, FALSE);
                }
            }
            
            
            // Update the status of Tank Dips (which having record type as placeholder) of Today if Tank status is changed to Operational or Stopped
            if (!setTankIDs.isEmpty()) {
                ID placeHolderRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.TANK_DIP_OBJ,EP_Common_Constant.PLACEHOLDER);
                //lTankDipsForUpdation  = [SELECT ID,EP_Tank_Status__c,EP_Tank__r.EP_Tank_Status__c From EP_Tank_Dip__c where EP_Tank__c IN: setTankIDs AND EP_Reading_Date_Time__c >= TODAY AND RecordTypeId =: placeHolderRecordTypeId  ];
                nRows = EP_Common_Util.getQueryLimit();     
                for(EP_Tank_Dip__c tdip : [SELECT ID,EP_Tank_Status__c,EP_Tank__r.EP_Tank_Status__c From EP_Tank_Dip__c 
                where EP_Tank__c IN: setTankIDs AND EP_Reading_Date_Time__c >= TODAY 
                AND RecordTypeId =: placeHolderRecordTypeId limit :nRows ] )
                {
                    tdip.EP_Tank_Status__c = tdip.EP_Tank__r.EP_Tank_Status__c;
                    lTankDipsForStatusUpdation.add(tdip);
                }
                if(!lTankDipsForStatusUpdation.isEmpty())
                Database.SaveResult[] result = Database.update(lTankDipsForStatusUpdation, FALSE);
            }
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA,HANDLE_PLACEHOLDER_MTD, CLASSNAME, ApexPages.Severity.ERROR );
        }
        
    }
    /*
        This method validates tank code for tanks associated to a ship to are unique
    */
    public static void validateTankCode(list<EP_Tank__c>lNewTanks){
        map<String,list<EP_Tank__c>>mShipTankCode = new map<String,List<EP_Tank__c>>();
        //list<String>lTankCodes;
        list<EP_Tank__c>lTanks = new list<EP_Tank__c>();
        // Removed New Status from Below Set EP_Common_Constant.DEFAULT_NON_VMI_ORDER_STATUS                                                 
        list<String>lTankStatus = new list<String>{EP_Common_Constant.BASIC_DATA_SETUP
            ,EP_Common_Constant.TANK_OPERATIONAL_STATUS
            ,EP_Common_Constant.TANK_STOPPED_STATUS};
        set<Id>sShipTo = new set<Id>();
        set<Id>sTanks = new set<Id>();
        set<String>sTankCode = new set<String>();
        Integer nRows;
        try{
            for(EP_Tank__c tank : lNewTanks){
                
                //RUN VALIDATION FOR OPERATIONAL/STOPPED TANK
                // Removed Validation In case of New Status : EP_Common_Constant.DEFAULT_NON_VMI_ORDER_STATUS.equalsIgnoreCase(tank.EP_Tank_Status__c)
                if(EP_Common_Constant.BASIC_DATA_SETUP.equalsIgnoreCase(tank.EP_Tank_Status__c)
                        || EP_Common_Constant.TANK_OPERATIONAL_STATUS.equalsIgnoreCase(tank.EP_Tank_Status__c)
                        || EP_Common_Constant.TANK_STOPPED_STATUS.equalsIgnoreCase(tank.EP_Tank_Status__c)){
                    //IF MULTIPLE TANKS ARE BEING ASSOCIATED TO A SHIP TO WITH SAME TANK CODE THEN THROW ERROR
                    if(tank.id != null){
                        sTanks.add(tank.id);
                    }
                    if(mShipTankCode.containsKey(tank.EP_Ship_To__c + tank.EP_Tank_Code__c)){
                        
                        mShiptankCode.get(tank.EP_Ship_To__c + tank.EP_Tank_Code__c).add(tank);
                        
                        
                    }
                    else{
                        lTanks = new list<EP_Tank__c>();
                        lTanks.add(tank);
                        mShipTankCode.put(tank.EP_Ship_To__c + tank.EP_Tank_Code__c,lTanks);
                        sShipTo.add(tank.EP_Ship_To__c);
                        sTankCode.add(tank.EP_Tank_Code__c);
                    }
                }
            }
            nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            if(!mShipTankCode.isEmpty()){
                for(EP_Tank__c tank : [SELECT EP_Tank_Code__c
                ,EP_Ship_To__c
                FROM EP_Tank__c
                WHERE EP_Ship_To__c IN:sShipTo
                AND EP_Tank_Code__c IN :sTankCode
                AND EP_Tank_Status__c IN :lTankStatus
                limit :nRows]){
                    //IF TANK WITH SAME TANK CODE ALREADY ASSOCIATED TO A SHIP TO THEN THROW ERROR 
                    if(mShipTankCode.containsKey(tank.EP_Ship_To__c + tank.EP_Tank_Code__c)
                            && !sTanks.contains(tank.id)){
                        mShipTankCode.get(tank.EP_Ship_To__c + tank.EP_Tank_Code__c).add(tank);
                    }
                }
            }
            for(EP_Tank__c tankRec : lNewTanks){
                if(mShipTankCode.containsKey(tankRec.EP_Ship_To__c + tankRec.EP_Tank_Code__c)
                        
                        && mShipTankCode.get(tankRec.EP_Ship_To__c + tankRec.EP_Tank_Code__c).size() > 1){
                    tankRec.addError(System.label.EP_Duplicate_Tank_Code_Err);
                    
                }
            }
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA,VALIDATE_TANK_CODE, CLASSNAME, ApexPages.Severity.ERROR );
        }
        
        
    }
    
    //L4_45352_Start
    //THIS METHOD SYNCS TANKS WITH WINDMS ON INSERT OF TANK IF ITS PARENT IS ALREADY ACTIVE    
    
    public static void syncTankOnInsert(map<Id,EP_Tank__c>mNewTanks){
        set<string> shipToStatus = new set<string>{EP_Common_Constant.STATUS_BASIC_DATA_SETUP,EP_Common_Constant.STATUS_SET_UP,EP_Common_Constant.STATUS_ACTIVE};
		/*defect 78513 start*/
        set<string> TankToStatus = new set<string>{EP_Common_Constant.NEW_TANK_STATUS,EP_Common_Constant.TANK_STOPPED_STATUS,EP_Common_Constant.OPERATIONAL_TANK_STATUS};
        /*defect 78513 end*/
        map<id,id>mTankShipTo = new map<id,id>();
        try{
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            //CHECK IF THE RELATED SHIP TO OF TANK HAS STATUS ACTIVE 
            for(EP_tank__c tank : [Select id
                                    ,EP_Ship_To__c
                                    From EP_Tank__c
                                    Where Id IN:mNewTanks.keyset()
                                    //L4_45352_Start
                                    AND EP_Ship_To__r.EP_Status__c IN: shipToStatus
                                    //L4_45352_End
									/*defect 78513 start*/
                                    AND EP_Tank_Status__c in : TankToStatus
                                    /*defect 78513 end*/
                                    Limit: nRows]){
                mTankShipTo.put(tank.id,tank.EP_Ship_To__c);
            }   
            //SEND ALL NEW TANKS FOR CREATION TO WINDMS FOR WHICH SHIP TO IS ACTIVE 
            if(!mTankShipTo.isEmpty()){
                //L4_45352_Start
                /*
                //INSERT A RECORD TO GENERATE UNIQUE ID FOR MESSAGE
                EP_Message_Id_Generator__c msgIdgenerator = new EP_Message_Id_Generator__c();
                Database.insert(msgIdgenerator);
                
                //SEND TANKS FOR SYNC TO WINDMS
                
                syncSFDCToWinDMSTanks(mTankShipTo
                                        ,msgIdgenerator.Id);
                */                  
                EP_Tank__c tank = [select Id,EP_Ship_To__r.EP_Puma_Company_Code__c from EP_Tank__c where Id in : mTankShipTo.keyset() LIMIT 1];
                List<id> childRecords = new List<id>();
                system.debug('tank.ID = '+tank.ID);
                system.debug('SFDC_TO_WINDMS_TANK_CREATION = '+SFDC_TO_WINDMS_TANK_CREATION);
                system.debug('tank.EP_Ship_To__r.EP_Puma_Company_Code__c = '+tank.EP_Ship_To__r.EP_Puma_Company_Code__c);
                EP_OutboundMessageService outservice = new EP_OutboundMessageService(tank.ID,SFDC_TO_WINDMS_TANK_CREATION,tank.EP_Ship_To__r.EP_Puma_Company_Code__c);
                outservice.sendOutboundMessage(TANK_EDIT,childRecords); 
                //L4_45352_End      
            }
            
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA,SYNC_TANKS_WITH_WINDMS_MTD, CLASSNAME, ApexPages.Severity.ERROR );
        }
    }
    //L4_45352_End
    
    /*
    This method syncs tank updates with WINDMS
     */
    public static void syncTankUpdatesWithWINDMS(map<Id,EP_Tank__c>mOldTanks
    ,map<Id,EP_Tank__c>mNewTanks){
        map<Id,Id>mTankShipTo = new map<Id,Id>();
        map<Id,Account>mShipTo = new map<Id,Account>();
        Integer nRows;
        
        map<String,String>createIntegrationStatusByObjAPIName = new map<String,String>();
        
        map<String,String>mChildParent_LS= new map<String,String>();
        
        
        List<String>lObjectType_LS = new list<String>();
        List<Id>lObjectId_LS = new list<Id>();
        /* list<EP_Tank__c>lTanks = new list<EP_Tank__c>(); commented as a novasuite fix as this is unused*/
        
        list<EP_IntegrationRecord__c>lIntegrationRecordsToInsert = new list<EP_IntegrationRecord__c>();
        
        list<Id>lIntegRecId = new list<Id>();
        String messageId;
        
        String transactionId;
        
        //String payload;
        try{
            createIntegrationStatusByObjAPIName = EP_Common_Util.createIntegrationStatusByObjAPIName();
            for(Id tankId : mNewTanks.keySet()){
                //CHECK IF USER HAS UPDATED PRODUCT/UOM/CAPACITY/DEADSTOCK LEVEL/SAFE FILL LEVEL/STATUS TO DECOMMISIONED ON TANKS
                if( (moldTanks.get(tankId).EP_Tank_Status__c != mNewTanks.get(tankId).EP_Tank_Status__c
                            ||(!(moldTanks.get(tankId).EP_Tank_Status__c == mNewTanks.get(tankId).EP_Tank_Status__c
                                    && EP_Common_Constant.TANK_DECOMISSIONED_STATUS.equalsIgnoreCase(mNewTanks.get(tankId).EP_Tank_Status__c) 
                                    )
                                && (moldTanks.get(tankId).EP_Capacity__c != mNewTanks.get(tankId).EP_Capacity__c
                                    || moldTanks.get(tankId).EP_Deadstock__c != mNewTanks.get(tankId).EP_Deadstock__c
                                    || moldTanks.get(tankId).EP_Safe_Fill_Level__c != mNewTanks.get(tankId).EP_Safe_Fill_Level__c
                                    ) 
                                ))&&(!EP_Common_Constant.BASIC_DATA_SETUP.equalsIgnoreCase(mNewTanks.get(tankId).EP_Tank_Status__c)
                            //Defect #57617 START
                            && !EP_Common_Constant.TANK_DECOMISSIONED_STATUS.equalsIgnoreCase(mNewTanks.get(tankId).EP_Tank_Status__c)
                            //Defect #57617 END
                            && !EP_Common_Constant.DEFAULT_NON_VMI_ORDER_STATUS.equalsIgnoreCase(mNewTanks.get(tankId).EP_Tank_Status__c))
                        )
                {
                    
                    mTankShipTo.put(tankId,mNewTanks.get(tankId).EP_Ship_To__c);                    
                }
            }
            system.debug('**EP_IntegrationUtil.isCallout1' + EP_IntegrationUtil.isCallout);
            if(!mTankShipTo.isEmpty()
                    && !EP_IntegrationUtil.isCallout){
                //INSERT A RECORD TO GENERATE UNIQUE ID FOR MESSAGE
                nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                mShipTo = new map<Id,Account>([Select id 
                ,EP_Status__c
                from Account 
                Where Id IN:mTankShipTo.values() LIMIT :nRows]);
                if(!mShipTo.isEmpty()){
                    for(Id tankId : mTankShipTo.keySet()){
                        if(mShipTo.containsKey(mTankShipTo.get(tankId))
                                && mShipTo.get(mTankShipTo.get(tankId)).EP_Status__c.SubString(0,2).isNumeric()
                                && Integer.valueOf(mShipTo.get(mTankShipTo.get(tankId)).EP_Status__c.SubString(0,2)) >= EP_Common_Constant.FIVE){
                            
                            lObjectId_LS.add(tankId);
                            lObjectType_LS.add(createIntegrationStatusByObjAPIName.get(tankId.getSObjectType().getDescribe().getName().toUpperCase()));
                            
                        }
                        else{
                            mTankShipTo.remove(tankId);
                        }
                    }
                }
            }  
            if(!mTankShipTo.isEmpty()
                    && !EP_IntegrationUtil.isCallout){
                        /*   	   
                EP_Message_Id_Generator__c msgIdgenerator = new EP_Message_Id_Generator__c();
                database.insert( msgIdgenerator);
                msgIdgenerator = [Select name from EP_Message_Id_Generator__c where id = :msgIdgenerator.id Limit :EP_Common_Constant.ONE];
                transactionId = EP_IntegrationUtil.getTransactionID(LS,TANK_EDIT);
                messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.SFD
                ,EP_Common_Constant.GBL
                ,EP_PROCESS_NAME_CS__c.getValues(EP_Common_Constant.EDIT_TANKS_LS).EP_Process_Name__c//INTERFACE NAME
                ,DateTime.now()
                ,msgIdgenerator.name);
                lIntegrationRecordsToInsert.addAll(EP_IntegrationUtil.sendBulkOk( transactionId,messageId,lObjectId_LS, lObjectType_LS,
                EP_Common_Constant.BLANK, DateTime.now(), LS, mChildParent_LS,true)); //replaced 'LS' string with var LS
                
                insert lIntegrationRecordsToInsert;
                for(EP_IntegrationRecord__c intgRec : lIntegrationRecordsToInsert){
                    lIntegRecId.add(intgRec.Id);
                }
                //SEND UPDATED TANKS FOR SYNC TO WINDMS
                syncSFDCToWinDMSTanks(mTankShipTo
                ,messageId
                ,transactionId
                ,lIntegRecId);*/
                EP_Tank__c tank = [select Id,EP_Ship_To__r.EP_Puma_Company_Code__c from EP_Tank__c where Id in : mTankShipTo.keyset() LIMIT 1];
                List<id> childRecords = new List<id>();
		        EP_OutboundMessageService outservice = new EP_OutboundMessageService(tank.ID,SFDC_TO_WINDMS_TANK_CREATION,tank.EP_Ship_To__r.EP_Puma_Company_Code__c);
		        outservice.sendOutboundMessage(TANK_EDIT,childRecords);
            }
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA,SYNC_TANKS_WITH_WINDMS_MTD, CLASSNAME, ApexPages.Severity.ERROR );
        }
    }
    
    /*
        THIS FUTURE METHOD SYNCS UPDATE ON TANKS WITH WINDMS
    */      
    @future(callout = true)
    static void syncSFDCToWinDMSTanks(map<id,id> mTankShipTo
                                            ,String messageId
                                            ,String transactionId
                                            ,list<Id>lIntegRecId){
        //String success;
        //String transactionId;
        //String messageId;
        String payload;
        String requestXML;
        //String endpoint;
        //DateTime dtSent;
        //List<String>lObjectType_LS = new list<String>();
        //List<Id>lObjectId_LS = new list<Id>();
        //map<String,String>createIntegrationStatusByObjAPIName = new map<String,String>();
        //createIntegrationStatusByObjAPIName = EP_Common_Util.createIntegrationStatusByObjAPIName();  
        //EP_Message_Id_Generator__c msgIdgenerator;
        EP_IntegrationUtil.isCallout = true;
        try{
            //EP_AccountTriggerHelper.callOutExceptions = new list<Exception>();
            if(Test.isRunningTest()){
                
                EP_IntegrationUtil.lIntegrationRecordsToInsert = new List<EP_IntegrationRecord__c>();
            }
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            //msgIdgenerator = [Select Name from EP_Message_Id_Generator__c where Id = :msgIdgeneratorId limit: nRows];
            
            payload = EncodingUtil.base64Encode
            (Blob.valueOf
            (EP_PayloadHandler.createLomosoftBulkTankEdit(mTankShipTo)));
            
            System.debug(payload);
            /*for(String objectType : EP_PayLoadHandler.mObjectRecordId.keySet()){
                    lObjectId_LS.addAll(EP_PayLoadHandler.mObjectRecordId.get(objectType));
            }
            for(Id objectId : lObjectId_LS){
                lObjectType_LS.add(createIntegrationStatusByObjAPIName.get(objectId.getSObjectType().getDescribe().getName().toUpperCase()));
            }*/
            
            //DATETIME OF SYNC
            //dtSent = DateTime.now();
            /*transactionId = EP_IntegrationUtil.getTransactionID(LS,TANK_EDIT);
            
            messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.SFD
                                                        ,EP_Common_Constant.GBL
                                                        ,EP_PROCESS_NAME_CS__c.getValues(EP_Common_Constant.EDIT_TANKS_LS).EP_Process_Name__c//INTERFACE NAME
                                                        ,dtSent
                                                        ,msgIdgenerator.name);*/
            
            //CREATE REQUEST XML FOR LOMOSOFT
            requestXML = EP_PayloadHandler.createCustomerRequest( messageId//MESSAGE ID
            ,EP_PROCESS_NAME_CS__c.getValues(EP_Common_Constant.EDIT_TANKS_LS).EP_Process_Name__c//INTERFACE NAME
            ,EP_COMMON_CONSTANT.BLANK//COMPANY BLANK
            ,payload
            ,EP_Common_Constant.CSTMR_CREATE_OBJECT//OBJECT NAME
            );
            //CALLOUT THROUGH IPASS
            
            EP_IntegrationUtil.callSfdcToIPASS(EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(EP_Common_Constant.IPASS_LS_CUSTOMER).EP_Value__c
            ,EP_COMMON_CONSTANT.POST
            ,requestXML
            ,lIntegRecId
            ,CLASSNAME
            ,messageId
            ,true);        
            
            
            
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA,SYNC_SFDC_TO_WINDMS_TANKS_MTD, CLASSNAME, ApexPages.Severity.ERROR );
        }
        
    }
    /*
        THIS METHOD VALIDATES CSC REVIEW ON TANK IS COMPLETED BEFORE CHANGING STATUS TO BASIC DATA SET-UP
    */
    public static void CmpltCscReviewBeforeBasicSetUp(map<Id,EP_Tank__c>mOldTanks
                                                        ,map<Id,EP_Tank__c>mNewTanks){
                                                            set<Id>sTanks = new set<Id>();
        try{
            //IF STATUS CHANGES TO BASIC DATA SET-UP
            for(Id tankId : mNewTanks.keySet()){
                if(mNewTanks.get(tankId).EP_Tank_Status__c != mOldTanks.get(tankId).EP_Tank_Status__c
                        && EP_Common_Constant.BASIC_DATA_SETUP.equalsIgnoreCase(mNewTanks.get(tankId).EP_Tank_Status__c)){
                    sTanks.add(tankId);
                }
            }
            //GET CSC REVIEWS PENDING FOR COMPLETION
            if(!sTanks.isEmpty()){
                for(EP_Tank__c tank : [Select Id 
                ,(select id
                ,EP_Status__c 
                from TankActions__r
                where recordtype.developerName =:EP_Common_Constant.ACT_TANK_CSC_REVIEW_RT_DEV
                and EP_Status__c IN :actionStatus 
                Limit :EP_Common_Constant.ONE /* removed hardcoded 1 as per novasuite fix  */
                )
                From EP_Tank__c
                Where Id IN:sTanks  LIMIT :EP_Common_Util.getQueryLimit()]){
                    //THROW ERROR
                    if(!tank.TankActions__r.isEmpty()){
                        mNewTanks.get(tank.id).addError(System.Label.EP_CSC_Review_is_not_completed);
                    }
                }
            }
        }catch(Exception handledException){
            EP_LoggingService.logHandledException( handledException, EP_Common_Constant.EPUMA,CMPLT_CSC_RVW_MTD, CLASSNAME, ApexPages.Severity.ERROR );
        }
    }
    
    /*
    * This method is used for updating missing days for tank dip
    */
    public static void updateMissingTankDipDays(List<EP_Tank__c> tListNew){
        Id actualRecTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.TANK_DIP_OBJ, EP_Common_Constant.ACTUAL); //'EP_Tank_Dip__c','Actual');
        Id placholderRecTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.TANK_DIP_OBJ, EP_Common_Constant.PLACEHOLDER); //'EP_Tank_Dip__c','Placeholder');
        Set<Id> tankIdSet = new Set<Id>();
        List<EP_Tank__c> tankList = new List<EP_Tank__c>();
        Integer i;
        datetime d;
        Date dtActual;
        Date dtPlaceholder;
        try{
            for(EP_Tank__c t : tListNew){
                tankIdSet.add(t.id);
            }
            List<EP_Tank__c> tList = new List<EP_Tank__c>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            /*tList = [Select id,EP_Missing_Dips_From_Days__c,RecordTypeId,(Select id,RecordTypeId,EP_Reading_Date_Time__c 
            from Tank_Dips__r order by EP_Reading_Date_Time__c ASC) 
            from EP_Tank__c where id IN: tankIdSet  LIMIT :nRows];*/
            for(EP_Tank__c tank : [Select id,EP_Missing_Dips_From_Days__c,RecordTypeId,(Select id,RecordTypeId,EP_Reading_Date_Time__c  from Tank_Dips__r order by EP_Reading_Date_Time__c ASC)  from EP_Tank__c where id IN: tankIdSet  LIMIT :nRows]){
                for(EP_Tank_Dip__c tDip : tank.Tank_Dips__r){
                    if(tDip.RecordTypeId == actualRecTypeId){
                        dtActual = tDip.EP_Reading_Date_Time__c.date();    
                    }
                    if(tDip.RecordTypeId == placholderRecTypeId){
                        if(dtPlaceholder == null){
                            dtPlaceholder = tDip.EP_Reading_Date_Time__c.date();
                        }
                        continue;
                    }
                }
                if(dtActual <> null){
                    tank.EP_Missing_Dips_From_Days__c= dtActual.daysBetween(System.today());
                }
                else if(dtActual == null && dtPlaceholder <> null){
                    tank.EP_Missing_Dips_From_Days__c= dtPlaceholder.daysBetween(System.today());
                }
                else{}    
                tankList.add(tank);                        
            }
            if(!tankList.isEmpty()){
                database.update(tankList);
            }
        }
        catch(Exception e){
            EP_LoggingService.logHandledException( e, EP_Common_Constant.EPUMA,UPDATE_MISSING_TANK_DIP_DAYS , CLASSNAME, ApexPages.Severity.ERROR );
        }
    }   
}