//Commented by Rahul Jain - Moved this call in New Class - EP_PricingResponseWS
//@RestResource(urlMapping ='/v1/PricingResponse/*' )
/* 
   @Author <Accenture>
   @name <EP_OrderPricingRequest>
   @Version <1.0>
 
   */
   global without sharing class EP_OrderPricingRequest{
    public static Map < String, Set < Id >> mObjectMap = new Map < String, Set < Id >> ();
    public static final string PRICING_REQUEST_SENT='PRICING REQUEST SENT';
    public static final string PRICING_ENGNE_REQUEST='EP_PRICING_ENGINE_REQUEST';
    public static final string PRICING_URL='EP_Pricing_URL';
    public static final string PRICING_ENGINE = 'PricingEngine';
    public static final string NAV_STR = 'NAV';
    public static final string PRICING_REQ = 'PRICING_REQUEST';
    public static final string CLASSNAME = 'EP_OrderPricingRequest';
    public static final string REQ_LINES = 'requestLines';
    public static final string ORDER_CATEGORY = 'EP_Order_Category__c';
    public static final string ORDER_ID = ', orderid ';
    public static final string FROM_ORDER_ID = ' From Order where id in : orderIds ';
    public static final string FROM_ORDER_ID_CONDITION = ' from OrderItem where OrderId  in : orderIds and EP_Is_Freight_Price__c = false and EP_Is_Taxes__c = false and EP_Is_Standard__c= true';
    public static final string REQ_LINE_TAG = '<requestLines>';
    public static final string PRI_REQUEST = 'pricingRequest';
    public static final string REQUEST_HEADER = 'requestHeader';
    public static final string SUPPLY_OPTION_ID = 'supplyLocationId';
    public static final string PRICE_DATE = 'priceDate';
    public static final string PRICING_CALLOUT_METHOD = 'pricing_callout';
    public static final string PRICING_REQ_GEN_METHOD = 'price_req_generator';
    public static final string PRICING_REQ_PAYLOAD_METHOD = 'pricingRequestPayload';
    public static final string PRICING_SYNC_METHOD = 'PRICINGSync';
    public static final string UPDATE_PRICING_METHOD = 'updatePricingRecords';
    
    /*
        This method is used to generate pricing request
        */
        public static void price_req_generator(Set<Id> ordId,String seqId){
            EP_GeneralUtility.Log('Public','EP_OrderPricingRequest','price_req_generator');
            try{
                EP_Message_Id_Generator__c msgGenId = new EP_Message_Id_Generator__c();
                insert msgGenId;
                if(!ordId.isEmpty()){
                    /*** Defect #28272 ***/
                //pricing_callout(ordId,seqId,msgGenId.id,EP_Common_Constant.PER);
                pricing_process( ordId , seqId, msgGenId.id, EP_Common_Constant.PER ); 
                /*** Defect #28272 ***/
            }
        }
        catch (Exception handledException) {
          EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, PRICING_REQ_GEN_METHOD,
              CLASSNAME, ApexPages.Severity.ERROR);
      }
      
  }
  
    /*
        This method is used to generate pricing request
        */
        public static void price_req_generator(Id ordId){
            EP_GeneralUtility.Log('Public','EP_OrderPricingRequest','price_req_generator');
            try{
                EP_Message_Id_Generator__c msgGenId = new EP_Message_Id_Generator__c();
                insert msgGenId;
                if(ordId != NULL){
                    pricing_callout(new Set<Id>{ordId},ordId,msgGenId.id,EP_Common_Constant.PER);
                }
            }
            catch (Exception handledException) {
              EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, PRICING_REQ_GEN_METHOD,
                  CLASSNAME, ApexPages.Severity.ERROR);
          }
      }
      @TestVisible
      /*** Defect #28272 ***/
      private static void pricing_process( set<Id> orderIds, String seq, Id msg, String processName ){
        EP_GeneralUtility.Log('Private','EP_OrderPricingRequest','pricing_process');
        String requestXML;
        String endpoint;
        String companyName;
        dateTime dtSent;
        String transactionId;
        String messageId;
        String payload;
        Integer nRows = EP_Common_Util.getQueryLimit();
        set < Id > lObjectId_NAV = new set< Id > ();
        //Defect 46731 Start
        List < String > lObjectType_NAV = new List< String > ();
        //Defect 46731 End
        EP_Message_Id_Generator__c msgIdgenerator;
        Set<Id> idOItem = new Set<Id>();
        String finalOrderString = EP_Common_Constant.BLANK;
        
        if (!orderIds.isEmpty() ) 
        {
            finalOrderString = pricingRequestPayload(orderIds, seq);
            map < String, String > createIntegrationStatusByObjAPIName = new map < String, String > ();
            createIntegrationStatusByObjAPIName = EP_Common_Util.createIntegrationStatusByObjAPIName();
            Map<Id,OrderItem> orderItemMap = new map<Id,OrderItem>([Select id from OrderItem where Order.id IN: orderIds and EP_IS_Standard__c = true limit :EP_COMMON_CONSTANT.ONE]);
            idOitem.addAll(orderItemMap.keySet());
            Order o = [Select id,AccountId,Account.EP_Puma_Company_Code__c from Order where Id In : orderIds LIMIT:nRows ];
            if(o.Account.EP_Puma_Company_Code__c != null || o.Account.EP_Puma_Company_Code__c != EP_Common_Constant.BLANK){
                companyName = o.Account.EP_Puma_Company_Code__c;
            }
            
            dtSent = DateTime.now();
            msgIdgenerator = [SELECT Name
            FROM EP_Message_Id_Generator__c
            WHERE ID = :msg
            limit :EP_COMMON_CONSTANT.ONE];
            
            messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.SFD
                ,EP_Common_Constant.GBL
                ,processName
                ,dtSent
                ,msgIdgenerator.name);
            
            payload = EncodingUtil.base64Encode( Blob.valueOf(finalOrderString) );
            system.debug('lObjectId_NAV'+lObjectId_NAV);
            for (String objectType: mObjectMap.keySet()) {
                lObjectId_NAV.addAll(mObjectMap.get(objectType));
            }
            for (Id objectId: lObjectId_NAV) {
                lObjectType_NAV.add(createIntegrationStatusByObjAPIName.get(objectId.getSObjectType().getDescribe().getName().toUpperCase()));
            }
            requestXML = EP_PayloadHandler.createCustomerRequest(   messageId//MESSAGE ID
                                                                    ,EP_PROCESS_NAME_CS__c.getValues(PRICING_ENGNE_REQUEST).EP_Process_Name__c//INTERFACE NAME
                                                                    ,companyName//COMPANY NAME
                                                                    ,payload //Payload
                                                                    ,PRICING_ENGINE
                                                                    );
            //GET IPASS ENDPOINT FROM CUSTOM SETTING
            endpoint = EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(PRICING_URL).EP_Value__c;
            dtSent = DateTime.now();
            
            transactionId = EP_IntegrationUtil.getTransactionID(NAV_STR, PRICING_REQ);
            list<EP_IntegrationRecord__c> lIntegrationRecordsToInsert = new list<EP_IntegrationRecord__c>();
            system.debug('lObjectId_NAV'+lObjectId_NAV);
            lIntegrationRecordsToInsert.addAll(EP_IntegrationUtil.sendBulkOk( transactionId,messageId,new list<Id>(lObjectId_NAV), new list<String>(lObjectType_NAV),
                companyName, dtSent, NAV_STR , EP_PayloadHandler.mChildParent,true ) );
            
            database.insert(lIntegrationRecordsToInsert);
            set<Id> lIntegRecId_NAV = new set<Id>();
            for(EP_IntegrationRecord__c intgRec : lIntegrationRecordsToInsert){
                lIntegRecId_NAV.add(intgRec.Id);
            }  
             //Defect 47368 Start
            //Added seq to pricing_callout method
            pricing_callout(requestXML, messageId, lIntegRecId_NAV , seq);
            //Defect 47368 End
        }
    }
    @future(callout = True)
    /*
        This method is used for pricing callout
        */
        public static void pricing_callout( String requestXML, String msg, set<Id> setIntegRecIds, String sequenceId ){
            EP_GeneralUtility.Log('Public','EP_OrderPricingRequest','pricing_callout');
            try{
                String endpoint = EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(PRICING_URL).EP_Value__c;
                list<Id> lIntegRecIds = new list<Id>(setIntegRecIds);
                EP_IntegrationUtil.callSfdcToIPASS( endpoint
                    ,EP_COMMON_CONSTANT.POST
                    ,requestXML
                    ,lIntegRecIds
                    ,CLASSNAME
                    ,msg
                    ,true);
            //Defect 47368 Start                                   
            updatePricingRecords(sequenceId);    
            //Defect 47368 End                                      
            
            } catch (Exception handledException) {
                EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, PRICING_CALLOUT_METHOD,
                  CLASSNAME, ApexPages.Severity.ERROR);
            }
            
            
        }
        /*** Defect #28272 ***/
        
        @future(callout = True)
    /*
        This method is used for pricing callout
        */
        public static void pricing_callout(set < id > orderIds,String seq,Id msg,String processName){
            EP_GeneralUtility.Log('Public','EP_OrderPricingRequest','pricing_callout');
            String requestXML;
            String endpoint;
            String companyName;
            dateTime dtSent;
            string transactionId;
            string messageId;
            String payload;
            Integer nRows = EP_Common_Util.getQueryLimit();
            List < Id > lObjectId_NAV = new list < Id > ();
            List < String > lObjectType_NAV = new list < String > ();
            EP_Message_Id_Generator__c msgIdgenerator;
            Set<Id> idOItem = new Set<Id>();
            String finalOrderString = EP_Common_Constant.BLANK;
            finalOrderString = pricingRequestPayload(orderIds,seq);
            map < String, String > createIntegrationStatusByObjAPIName = new map < String, String > ();
            createIntegrationStatusByObjAPIName = EP_Common_Util.createIntegrationStatusByObjAPIName();
            Map<Id,OrderItem> orderItemMap =new map<Id,OrderItem>([Select id from OrderItem where Order.id IN: orderIds and EP_IS_Standard__c = true limit :EP_COMMON_CONSTANT.ONE]);
            idOitem.addAll(orderItemMap.keySet());
            Order o = [Select id,AccountId,Account.EP_Puma_Company_Code__c from Order where Id In : orderIds LIMIT: nRows];
            if(o.Account.EP_Puma_Company_Code__c != null || o.Account.EP_Puma_Company_Code__c != EP_Common_Constant.BLANK){
                companyName = o.Account.EP_Puma_Company_Code__c;
            }
            try{
                dtSent = DateTime.now();
                msgIdgenerator = [SELECT Name
                FROM EP_Message_Id_Generator__c
                WHERE ID = :msg
                limit :EP_COMMON_CONSTANT.ONE];
                
                messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.SFD
                    ,EP_Common_Constant.GBL
                    ,processName
                    ,dtSent
                    ,msgIdgenerator.name);
            //MAKE CALLOUT TO NAV
            if (!orderIds.isEmpty()) {
                payload = EncodingUtil.base64Encode(
                    Blob.valueOf(finalOrderString));
                for (String objectType: mObjectMap.keySet()) {
                    lObjectId_NAV.addAll(mObjectMap.get(objectType));
                }
                for (Id objectId: lObjectId_NAV) {
                    lObjectType_NAV.add(createIntegrationStatusByObjAPIName.get(objectId.getSObjectType().getDescribe().getName().toUpperCase()));
                }
                requestXML = EP_PayloadHandler.createCustomerRequest( messageId//MESSAGE ID
                                     ,EP_PROCESS_NAME_CS__c.getValues(PRICING_ENGNE_REQUEST).EP_Process_Name__c//INTERFACE NAME
                                     ,companyName//COMPANY BLANK
                                     ,payload
                                     ,PRICING_ENGINE
                                     );
                //GET IPASS ENDPOINT FROM CUSTOM SETTING
                endpoint = EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(PRICING_URL).EP_Value__c;
                dtSent = DateTime.now();
                transactionId = EP_IntegrationUtil.getTransactionID(NAV_STR, PRICING_REQ);
                //successNav = callSfdcToNav_orderUpdate(payload);
               // requestXML = '<?xml version="1.0"?><MSG><HeaderCommon><MsgID>SFD-GBL-PER-09022017-13:37:08-024835</MsgID><InterfaceType>PER</InterfaceType><SourceGroupCompany/><DestinationGroupCompany/><SourceCompany>AAF</SourceCompany><DestinationCompany/><CorrelationID/><DestinationAddress/><SourceResponseAddress>https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2</SourceResponseAddress><SourceUpdateStatusAddress>https://cs31.salesforce.com:8443/services/apexrest/v2/AcknowledgementWS2</SourceUpdateStatusAddress><DestinationUpdateStatusAddress/><MiddlewareUrlForPush/><EmailNotification/><ErrorCode/><ErrorDescription/><ProcessingErrorDescription/><ContinueOnError>TRUE</ContinueOnError><ComprehensiveLogging>TRUE</ComprehensiveLogging><TransportStatus>New</TransportStatus><ProcessStatus/><UpdateSourceOnReceive>FALSE</UpdateSourceOnReceive><UpdateSourceOnDelivery>FALSE</UpdateSourceOnDelivery><UpdateSourceAfterProcessing>FALSE</UpdateSourceAfterProcessing><UpdateDestinationOnDelivery>TRUE</UpdateDestinationOnDelivery><CallDestinationForProcessing>true</CallDestinationForProcessing><ObjectType>Table</ObjectType><ObjectName>pricingDetails</ObjectName><CommunicationType>Async</CommunicationType></HeaderCommon><Payload type="request" encrypted="no"><any>PHByaWNpbmdSZXF1ZXN0Pg0KICAgIDxyZXF1ZXN0SGVhZGVyPg0KICAgICAgICA8c2VxSWQ+ODAxcDAwMDAwMDBBTFRuQUFPLTA5MDIyMDE3MTMzNzA3LTAyNDgzNDwvc2VxSWQ+DQogICAgICAgIDxwcmljZVJlcXVlc3RTb3VyY2U+PC9wcmljZVJlcXVlc3RTb3VyY2U+DQogICAgICAgIDxjb21wYW55Q29kZT5BQUY8L2NvbXBhbnlDb2RlPg0KICAgICAgICA8cHJpY2VUeXBlPkZ1ZWwgYW5kIEZyZWlnaHQgUHJpY2U8L3ByaWNlVHlwZT4NCiAgICAgICAgPGRlbGl2ZXJ5VHlwZT5EZWxpdmVyeTwvZGVsaXZlcnlUeXBlPg0KICAgICAgICA8cHJpY2VEYXRlPjIwMTctMDItMDkgMDA6MDA6MDA8L3ByaWNlRGF0ZT4NCiAgICAgICAgPHRvdGFsT3JkZXJRdWFudGl0eT42NzguMDA8L3RvdGFsT3JkZXJRdWFudGl0eT4NCiAgICAgICAgPGN1c3RvbWVySWQ+MDAwMDc1MzI8L2N1c3RvbWVySWQ+DQogICAgICAgIDxzaGlwVG9JZD4wMDE3NTQ5Njwvc2hpcFRvSWQ+DQogICAgICAgIDxzdXBwbHlMb2NhdGlvbklkPklOVFJBTlNJVDwvc3VwcGx5TG9jYXRpb25JZD4NCiAgICAgICAgPHRyYW5zcG9ydGVySWQ+PC90cmFuc3BvcnRlcklkPg0KICAgICAgICA8b25SdW4+ZmFsc2U8L29uUnVuPg0KICAgICAgICA8cHJpY2VDb25zb2xpZGF0aW9uQmFzaXM+PC9wcmljZUNvbnNvbGlkYXRpb25CYXNpcz4NCiAgICA8L3JlcXVlc3RIZWFkZXI+DQogICAgPHJlcXVlc3RMaW5lcz4NCiAgICAgICAgPG9yZGVySWQ+MDAwMjU3Mzg8L29yZGVySWQ+DQogICAgICAgIDxsaW5lPg0KICAgICAgICAgICAgPGxpbmVJdGVtSWQ+MDAwMDAwNjc1MzwvbGluZUl0ZW1JZD4NCiAgICAgICAgICAgIDxpdGVtSWQ+MTAwMDcwMDc8L2l0ZW1JZD4NCiAgICAgICAgICAgIDxxdWFudGl0eT42NzguMDA8L3F1YW50aXR5Pg0KICAgICAgICA8L2xpbmU+DQogICAgPC9yZXF1ZXN0TGluZXM+DQo8L3ByaWNpbmdSZXF1ZXN0Pg==</any></Payload><StatusPayload/></MSG>';
               EP_IntegrationUtil.callSfdcToIPASS(endpoint
                ,EP_COMMON_CONSTANT.POST
                ,requestXML
                ,lObjectId_NAV
                ,lObjectType_NAV 
                ,transactionId 
                ,messageId
                ,NAV_STR
                ,EP_Common_Constant.BLANK
                ,dtSent
                ,EP_PayloadHandler.mChildParent
                ,CLASSNAME
                ,true);  
               updatePricingRecords(seq);                                 
               
           }
           
       } 
       catch (Exception handledException) {
        EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, UPDATE_PRICING_METHOD,
            CLASSNAME, ApexPages.Severity.ERROR);
    }
} 

    /*
    This method is used to update Pricing Records
    */
    public static void updatePricingRecords(String s){
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequest','updatePricingRecords');
        try{
            if(EP_IntegrationUtil.updateVariable == true){
                EP_Pricing_Engine__c priceRecord = new EP_Pricing_Engine__c();
                priceRecord.EP_Sequence_Id__c = s;
                priceRecord.EP_Pricing_In_Progress__c = true;
                priceRecord.EP_Is_Latest__c = true;
                priceRecord.EP_Order__c = s.substring(0,18);
                insert priceRecord;
                Order order = new Order ( Id = priceRecord.EP_Order__c,EP_Pricing_Status__c = PRICING_REQUEST_SENT );
                update order;
            }
        }
        catch (Exception handledException) {
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, UPDATE_PRICING_METHOD,
                CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
   /*
        This method is used to create pricing request payload
        */
        public static string pricingRequestPayload(Set<Id> orderIds, String sequence){
            EP_GeneralUtility.Log('Public','EP_OrderPricingRequest','pricingRequestPayload');
            Map<Id,List<OrderItem>> orderItemMap = new Map<Id,List<OrderItem>>();
            Set<String> orderFields = new Set<String>();
            Set<String> orderItemFields = new Set<String>();
            String ordlixml = EP_Common_Constant.BLANK;
            String xml = EP_Common_Constant.BLANK;
            String finalxml = EP_Common_Constant.BLANK;
            String STR_requestLines = REQ_LINES;
            Set<String> nodeDone = new Set<String>();
            List<EP_Pricing_Engine_CS__c> orderList = new List<EP_Pricing_Engine_CS__c>();
            List<EP_Pricing_Engine_CS__c> orderItemList = new List<EP_Pricing_Engine_CS__c>();
            Map<string,List<EP_Pricing_Engine_CS__c>> orderChildTagMap = new Map<string,List<EP_Pricing_Engine_CS__c>>();
            Map<string,List<EP_Pricing_Engine_CS__c>> orderItemChildTagMap = new Map<string,List<EP_Pricing_Engine_CS__c>>();
            
            try{
                Integer nRows = EP_Common_Util.getQueryLimit();
                for(EP_Pricing_Engine_CS__c priceCS :  [Select e.Name,e.EP_Respective_Tag_Name__c, e.EP_Parent_Tag__c, e.EP_Order_sequence__c, e.EP_Object_Name__c, e.EP_Field_API_Name__c From EP_Pricing_Engine_CS__c e ORDER BY EP_Order_sequence__c ASC LIMIT: nRows]){
                    if (priceCS.EP_Object_Name__c != Null && priceCS.EP_Object_Name__c.equalsIgnoreCase(EP_Common_Constant.ORDER_STRING)) {
                        if (priceCS.EP_Field_API_Name__c != null) {
                            orderFields.add(priceCS.EP_Field_API_Name__c.toLowerCase());
                        }
                        else{}
                        orderList.add(priceCS);
                        if (!string.isBlank(priceCS.EP_Parent_Tag__c)) {
                            if (!orderChildTagMap.containsKey(priceCS.EP_Parent_Tag__c)) {
                                orderChildTagMap.put(priceCS.EP_Parent_Tag__c, new List<EP_Pricing_Engine_CS__c>());
                            }
                            orderChildTagMap.get(priceCS.EP_Parent_Tag__c).add(priceCS);
                        }
                    }
                    else if(priceCS.EP_Object_Name__c != Null && priceCS.EP_Object_Name__c.equalsIgnoreCase(EP_Common_Constant.ORDER_ITEM_STRING)){
                        if(priceCS.EP_Field_API_Name__c != null){
                            orderItemFields.add(priceCS.EP_Field_API_Name__c.toLowerCase());
                        }
                        orderItemList.add(priceCS);
                        if (!string.isBlank(priceCS.EP_Parent_Tag__c)) {
                            if (!orderItemChildTagMap.containsKey(priceCS.EP_Parent_Tag__c.toLowerCase())) {
                                orderItemChildTagMap.put(priceCS.EP_Parent_Tag__c.toLowerCase(), new List<EP_Pricing_Engine_CS__c>());
                            }
                            orderItemChildTagMap.get(priceCS.EP_Parent_Tag__c.toLowerCase()).add(priceCS);
                        }
                    }
                    else{}
                }
                String ordQuery = EP_Common_Constant.BLANK;
                string orditemQuery = EP_Common_Constant.BLANK;
                for (String Sa: orderFields) {
                    ordQuery += EP_Common_Constant.COMMA+ Sa;
                }
                System.debug(ordQuery);
                if (ordQuery != EP_Common_Constant.BLANK) {
                    ordQuery = ordQuery.substring(1);
                }
                for (String Sa: orderItemFields) {
                    orditemQuery += EP_Common_Constant.COMMA+ Sa;
                }
                if (orditemQuery != EP_Common_Constant.BLANK) {
                    orditemQuery = orditemQuery.substring(1);
                }
                
                if (ordQuery != EP_Common_Constant.BLANK && orditemQuery != EP_Common_Constant.BLANK) {
                    if (!orderItemFields.contains(EP_Common_Constant.OrderID_STRING)) {
                        orditemQuery += ORDER_ID;
                    }
                    ordQuery = EP_Common_Constant.SELECT_STRING + ordQuery + FROM_ORDER_ID;
                    orditemQuery = EP_Common_Constant.SELECT_STRING  + orditemQuery + FROM_ORDER_ID_CONDITION ;
                }    
                for (OrderItem oi: Database.query(orditemQuery) ) {
                   if (!orderItemMap.containsKey(oi.orderid)) {
                       orderItemMap.put(oi.orderid, new List<OrderItem>());
                   }
                   orderItemMap.get(oi.orderid).add(oi);
               }     

               Set<String> nodeComplete = new Set<String>();
               ordlixml += REQ_LINE_TAG;
               for (id ordId: orderItemMap.keyset()) {
                nodeComplete = new Set<String>();
                EP_Payloadhandler.createObjectRecordMap(EP_Common_Constant.ORDER_STRING, ordId);
                
                for (OrderItem ordItem: orderItemMap.get(ordId)) {
                    if (!nodeDone.contains(EP_Common_Constant.LINE_TAG) ) {
                        if (!mObjectMap.containsKey(EP_Common_Constant.ORDER_ITEM_STRING)) {
                            mObjectMap.put(EP_Common_Constant.ORDER_ITEM_STRING, new Set < Id > ());
                        }
                        mObjectMap.get(EP_Common_Constant.ORDER_ITEM_STRING).add(ordItem.id);
                        for (EP_Pricing_Engine_CS__c orItfd: orderItemList) {
                          if(string.isBlank(orItfd.EP_Parent_Tag__c)){
                              if(!nodeDone.contains(orItfd.EP_Respective_Tag_Name__c)){
                                ordlixml += EP_Common_Constant.ANG_LEFT_OPEN + orItfd.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT
                                + ((ordItem.get(orItfd.EP_Field_API_Name__c)) != null ? ordItem.get(orItfd.EP_Field_API_Name__c) : EP_Common_Constant.BLANK) 
                                + EP_Common_Constant.ANG_LEFT_CLOSE + orItfd.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT;
                            }
                            nodeDone.add(orItfd.EP_Respective_Tag_Name__c);
                        }
                        if (!string.isBlank(orItfd.EP_Parent_Tag__c)) {
                            if (orderItemChildTagMap.containskey(orItfd.EP_Parent_Tag__c.toLowerCase())) {                         
                                ordlixml += EP_Common_Constant.ANG_LEFT_OPEN + orItfd.EP_Parent_Tag__c + EP_Common_Constant.ANG_RIGHT;
                                for (EP_Pricing_Engine_CS__c childNode: orderItemChildTagMap.get(orItfd.EP_Parent_Tag__c.toLowerCase())) {
                                    ordlixml += EP_Common_Constant.ANG_LEFT_OPEN + childNode.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT
                                    + ((ordItem.get(childNode.EP_Field_API_Name__c)) != null ? ordItem.get(childNode.EP_Field_API_Name__c) : EP_Common_Constant.BLANK)    
                                    + EP_Common_Constant.ANG_LEFT_CLOSE + childNode.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT;
                                }
                                ordlixml += EP_Common_Constant.ANG_LEFT_CLOSE + orItfd.EP_Parent_Tag__c + EP_Common_Constant.ANG_RIGHT;
                                break;
                            }                         
                        }
                    }
                } 
                }nodeDone.add(EP_Common_Constant.INVOICELINE_LINE);
                }   ordlixml += EP_Common_Constant.ANG_LEFT_CLOSE + STR_requestLines + EP_Common_Constant.ANG_RIGHT;                 
                
                set <string > ordTagset = new set < string > ();
                string finalOrderString = EP_Common_Constant.BLANK;
                finalxml = EP_Common_Constant.ANG_LEFT_OPEN + PRI_REQUEST + EP_Common_Constant.ANG_RIGHT;
                for (Order ord: Database.query(ordQuery)) {
                    xml = EP_Common_Constant.ANG_LEFT_OPEN + REQUEST_HEADER + EP_Common_Constant.ANG_RIGHT;
                    if (!mObjectMap.containsKey(EP_Common_Constant.ORDER_STRING)) {
                        mObjectMap.put(EP_Common_Constant.ORDER_STRING, new Set < ID > ());
                    }
                    mObjectMap.get(EP_Common_Constant.ORDER_STRING).add(ord.id);
                    for (EP_Pricing_Engine_CS__c ordTag: orderList) {
                      if(ordTag.EP_Field_API_Name__c != null){
                        if(!ordTagset.contains(ordTag.EP_Respective_Tag_Name__c)){
                            if(ordTag.EP_Respective_Tag_Name__c <> EP_Common_Constant.SEQID_STRING){
                        //inner if else added to send Ex-Rack when Delivery Type is Consumption 
                        if(EP_Common_Constant.XML_DELIVERY_TYPE.equalsIgnoreCase(ordTag.EP_Respective_Tag_Name__c)){

                            // defect fix 26450
                            xml += EP_Common_Constant.ANG_LEFT_OPEN + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT  
                            + ((EP_Common_Constant.DELIVERY == ord.get(ordTag.EP_Field_API_Name__c)) || (EP_Common_Constant.PIPELINE== ord.get(ordTag.EP_Field_API_Name__c))  ? ord.get(ordTag.EP_Field_API_Name__c) : EP_Common_Constant.EX_RACK) 
                            + EP_Common_Constant.ANG_LEFT_CLOSE + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT ;
                        }
                        else if(ordTag.EP_Respective_Tag_Name__c.equalsIgnoreCase(PRICE_DATE)){
                            xml += EP_Common_Constant.ANG_LEFT_OPEN + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT  
                            + ((ord.get(ordTag.EP_Field_API_Name__c)) != null ? EP_PortalLibClass_R1.returnLocalDate((DateTime)ord.get(ordTag.EP_Field_API_Name__c))  : null) 
                            + EP_Common_Constant.ANG_LEFT_CLOSE + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT ; 
                        }
                        else{
                            xml += EP_Common_Constant.ANG_LEFT_OPEN + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT  
                            + ((ord.get(ordTag.EP_Field_API_Name__c)) != null? ord.get(ordTag.EP_Field_API_Name__c) : EP_Common_Constant.BLANK) 
                            + EP_Common_Constant.ANG_LEFT_CLOSE + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT ;  
                        }               
                    }
                    else{
                        xml += EP_Common_Constant.ANG_LEFT_OPEN + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT  
                        + sequence 
                        + EP_Common_Constant.ANG_LEFT_CLOSE + ordTag.EP_Respective_Tag_Name__c + EP_Common_Constant.ANG_RIGHT ;
                    }
                }
            }  
            ordTagset.add(ordTag.EP_Respective_Tag_Name__c);
        } 
        xml += EP_Common_Constant.ANG_LEFT_CLOSE + REQUEST_HEADER + EP_Common_Constant.ANG_RIGHT;
    }  
    finalxml += xml 
    + ordlixml
    + EP_Common_Constant.ANG_LEFT_CLOSE + PRI_REQUEST + EP_Common_Constant.ANG_RIGHT;
    
}
catch (Exception handledException) {
  EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, PRICING_REQ_PAYLOAD_METHOD,
      CLASSNAME, ApexPages.Severity.ERROR);
}
return finalxml;    
}


    //@HttpPost
    /*
        Method For PRICINGSync
        */ 
        global static void PRICINGSync(){
            try{
                RestRequest request = RestContext.request;        
                String requestBody = request.requestBody.toString();
                EP_PricingHandler.PRICINGwrapper skuRecords = EP_PricingHandler.parse(requestBody);
                EP_PRICINGHandler.UpdatePRICING(skuRecords);       
            }
            catch (Exception handledException) {
              EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, PRICING_SYNC_METHOD,
                  CLASSNAME, ApexPages.Severity.ERROR);
          }
      }   
  }