/*
    This is the controller class for account tank and bank account VF page
*/
public without sharing class EP_CustomerEditRedirectController{
    private static final String AND_VAR = '&';
    private static final String ID_VAR = 'Id';
    
    string urls = EP_Common_Constant.BLANK;
    string recordID ;
    Integer nRows;
    private static final string RECTYPESTR = 'RecordType';
    private static final String CRID = '/apex/EP_ChangeRequest?id=';
    private static final String CustomerEditVFPageURL = '/apex/EP_CustomerEdit?id=';
    private static final String RETURNURL = '&returl=/';
    private static final String RECORDTYPE = '&recTypeName=';
    private static final String TANKTYPE = '&op=UPDATE&type=EP_Tank__c';
    private static final String ACCRECTYPE = '&op=UPDATE&type=Account&RecordTypeId=';
    private static final string BANKACCTYPE = '&op=UPDATE&type=EP_Bank_Account__c';
    private static final String NOOVERRIDE = '/e?nooverride=1';
    private static final String EQUALSSTR = '=';
    private static final String NEWSTR = 'New';
    private static final String REDIRECT_BANK_ACCOUNT = 'redirectBankAccount';
    private static final String REDIRECT_ACCOUNT = 'redirectAccount';
    private static final String REDIRECT_TANK = 'redirectTank';
    private static final String CLASS_NAME = 'CustomerEditRedirectController';
    string rtId ;
    private static final String TNKPROLIST = '/apex/EP_Tank_Product_Page?id=';
    private static final set<String> recordTypeNameSet = new set<String>{EP_Common_Constant.SELL_TO_DEV_NAME,EP_Common_Constant.RT_BILL_TO_DEV_NAME
                                        ,EP_Common_Constant.VMI_SHIP_TO_DEV_NAME,EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME};
    //Code Changes for #45361 Start
    private static final set<String> accStatus = new set<String>{EP_Common_Constant.STATUS_ACTIVE,EP_Common_Constant.STATUS_BLOCKED,
                                            EP_Common_Constant.STATUS_INACTIVE,EP_Common_Constant.STATUS_ARCHIVED
                                            ,EP_Common_Constant.STATUS_REJECTED, EP_AccountConstant.DEACTIVATE};
    private static final EP_CS_Validation_Rule_Settings__c valRuleSetting = EP_CS_Validation_Rule_Settings__c.getInstance(UserInfo.getProfileId());
 
    //Code Changes for #45361 End
    private static final set<String> sellToaccStatus = new set<String>{EP_Common_Constant.STATUS_ACTIVE,EP_Common_Constant.STATUS_BLOCKED
                                                ,EP_Common_Constant.STATUS_REJECTED ,EP_Common_Constant.STATUS_ARCHIVED};
          
    
    /*
        COnstrutor for the class
    */
    public EP_CustomerEditRedirectController(ApexPages.StandardController controller){
        recordID = apexPages.currentPage().getParameters().get(ID_VAR);
        urls = EP_Common_Constant.BLANK;
        rtId  = apexPages.currentPage().getParameters().get(RECTYPESTR );
        for(string s: apexPages.currentPage().getParameters().Keyset()){
            urls += AND_VAR+s+EQUALSSTR+apexPages.currentPage().getParameters().get(s);
        }
    }
    /*
        To set standard page and VF page variables 
    */
    public pageReference redirectTank(){
        pageReference page;
        try{
        nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        Set<string> tankStatus = new Set<String>{EP_Common_Constant.BASIC_DATA_SETUP, NEWSTR};
        //Code Changes for #45361 Start
        EP_Tank__c tank = [Select id, EP_Ship_To__c,EP_Ship_To__r.Parent.EP_Status__c, EP_Ship_To__r.EP_Status__c, EP_Tank_Status__c,RecordType.Name
                             from EP_Tank__c 
                            Where id=: recordID limit :nRows];
        //Code Changes for #45361 End
        if(tank.EP_Tank_Status__c  != EP_Common_Constant.TANK_DECOMISSIONED_STATUS){
            // Changes made for CUSTOMER MODIFICATION L4
            //Code Changes for #45361 Start
            if(EP_AccountConstant.DEACTIVATE.equalsIgnoreCase(tank.EP_Ship_To__r.Parent.EP_Status__c) && !valRuleSetting.EP_Skip_Tank_Rules__c)  {
            	page = null;
	            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,system.label.EP_Tank_modification_error_msg_for_deactivated_customer);
	            Apexpages.addmessage(msg);
            }  else {
            	page =  new PageReference(TNKPROLIST+tank.Id+urls).setRedirect(true);
            }
            //Code Changes for #45361 End
            // Changes made for CUSTOMER MODIFICATION L4
        }else{
            page = null;
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,Label.EP_Decommisioned_tank_cannot_be_modified);
            Apexpages.addmessage(msg);
        }
        }
        Catch(Exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, REDIRECT_TANK, CLASS_NAME, ApexPages.Severity.ERROR);
        }
      
        return page;
    }
    /*
        Redirect Standard and VF page for account
    */
    public pageReference redirectAccount(){
        pageReference page;
        try{
        nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        /*DEFECT 30073 26338 30002 START*/                                        
        Account acc = [Select id
        					, Parent.EP_Status__c
        					, EP_Status__c
        					, RecordType.DeveloperName
        					, RecordType.Name
        					, ParentId
        					, EP_Is_Dummy__c 
                            From Account
                            Where id=: recordID limit :nRows];
        if(!acc.EP_Is_Dummy__c && recordTypeNameSet.contains(acc.RecordType.DeveloperName)){
        /*DEFECT 30073 26338 30002 END*/
            system.debug('====Inn Iff====='+acc.RecordType.DeveloperName);
            
            if(string.isNotBlank(rtId) && accStatus.contains(acc.EP_Status__c)){
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,system.label.EP_CR_RecordType_Modification_Error);
                Apexpages.addmessage(msg);
                return null;
            }
         	// Changes made for CUSTOMER MODIFICATION L4
            if(EP_Common_Constant.SELL_TO_DEV_NAME.EqualsIgnoreCase(acc.RecordType.DeveloperName)
                    && sellToaccStatus .Contains(acc.EP_Status__c)) {
                    page = new PageReference(CustomerEditVFPageURL+acc.id+RETURNURL+acc.id+ACCRECTYPE+acc.RecordTypeId+RECORDTYPE+acc.RecordType.Name);
            // Changes made for CUSTOMER MODIFICATION L4
             //Code Changes for #45361 Start
            }else if( (EP_AccountConstant.DEACTIVATE.equalsIgnoreCase(acc.EP_Status__c) || 
            		 EP_Common_Constant.STATUS_REJECTED.equalsIgnoreCase(acc.EP_Status__c) ||
            		 EP_AccountConstant.DEACTIVATE.equalsIgnoreCase(acc.Parent.EP_Status__c) ) 
            		 && !valRuleSetting.EP_Skip_Account_Rules__c
                    ) {
                    apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Error,EP_AccountConstant.DEACTIVATE + ' Or ' + EP_Common_Constant.STATUS_REJECTED + EP_Common_Constant.SPACE_STRING  + label.EP_Cstmr_Not_Allwd_Modify);
                    Apexpages.addmessage(msg);
                    page =  null; 
            }
            //Code Changes for #45361 End
            else{
                page =  new PageReference(EP_Common_Constant.SLASH+acc.Id+NOOVERRIDE+urls); 
            }                    
        }else{
            page =  new PageReference(EP_Common_Constant.SLASH+acc.Id+NOOVERRIDE+urls);
        }
        }Catch(Exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, REDIRECT_ACCOUNT, CLASS_NAME, ApexPages.Severity.ERROR);
           }
        return page;
    }
    
    /*
        Redirect Standard and VF page for Bank account
    */
    public pageReference redirectBankAccount(){
        pageReference page;
        try{
                nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                EP_Bank_Account__c bankAcc = [Select id, Name, EP_Account__c, EP_Account__r.EP_Status__c 
                                                From EP_Bank_Account__c where id=:recordID limit :nRows];
                
                if(accStatus.contains(bankAcc.EP_Account__r.EP_Status__c)){
                    page = new PageReference(CRID+bankAcc.id+ RETURNURL+ bankAcc.id+ BANKACCTYPE);
                }else{
                    page = new PageReference(EP_Common_Constant.SLASH+bankAcc.id+NOOVERRIDE+urls);
                } 
            }Catch(Exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, REDIRECT_BANK_ACCOUNT, CLASS_NAME, ApexPages.Severity.ERROR);
           }                              
        return page;
    }   
}