@isTest
public class EP_VmiNonConsignmentOrderStatePlanned_UT
{
    
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_VmiNonConsignmentOrderStatePlanned.getTextValue();
        Test.stopTest();
        System.AssertEquals(true,result==EP_OrderConstant.OrderState_Planned);
    }
    static testMethod void doOnEntry_test() {
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStatePlannedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method only delegates to domain ,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doOnExit_NoCallout_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStatePlannedDomainObject();
        csord__Order__c orderobj = obj.getorder();
        orderobj.EP_Pricing_Status__c = null;
        orderobj.EP_Order_Credit_Status__c = null;
        orderobj.EP_Sync_with_NAV__c = false;
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        list<EP_IntegrationRecord__c> EP_IntegrationRecordlist = [SELECT Id FROM EP_IntegrationRecord__c where EP_Target__c =:EP_Common_Constant.NAV];
        System.AssertEquals(true,EP_IntegrationRecordlist.size() == 0);
        list<EP_IntegrationRecord__c> EP_IntegrationRecordlist1 = [SELECT Id FROM EP_IntegrationRecord__c where EP_Target__c =:EP_Common_Constant.NAV];
        System.AssertEquals(true,EP_IntegrationRecordlist1.size() == 0);
    }
    
    static testMethod void doOnExit_Winddmssync_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStatePlannedDomainObject();
        csord__Order__c orderobj = obj.getorder();
        orderobj.EP_Pricing_Status__c = EP_Common_constant.PRICING_STATUS_PRICED;
        orderobj.EP_Order_Credit_Status__c = EP_Common_constant.Credit_Okay;
        orderobj.EP_Credit_Status__c = EP_Common_constant.OK;
        orderobj.EP_Sync_with_NAV__c = true;
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        list<EP_IntegrationRecord__c> EP_IntegrationRecordlist = [SELECT Id FROM EP_IntegrationRecord__c where EP_Target__c =:EP_Common_Constant.WINDMS];
        System.AssertEquals(true,EP_IntegrationRecordlist.size() > 0);
    }
    static testMethod void doOnExit_NavSync_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStatePlannedDomainObject();
        csord__Order__c orderobj = obj.getorder();
        orderobj.EP_Pricing_Status__c = EP_Common_constant.PRICING_STATUS_PRICED;
        orderobj.EP_Order_Credit_Status__c = EP_Common_Constant.Blank;
        orderobj.EP_Sync_with_NAV__c = false;
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        list<EP_IntegrationRecord__c> EP_IntegrationRecordlist = [SELECT Id FROM EP_IntegrationRecord__c where EP_Target__c =:EP_Common_Constant.NAV];
        System.AssertEquals(true,EP_IntegrationRecordlist.size() > 0);
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStatePlannedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getCurrentVmiNonConsignmentOrderStatePlannedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent('Planned To Planned');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getCurrentVmiNonConsignmentOrderStatePlannedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent('INVALID');
        localObj.setOrderContext(obj, oe);
        string errorMsg;
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }catch(exception e){
            errorMsg = e.getmessage();  
        }
        Test.stopTest();
        system.debug('===errorMsg==='+errorMsg);
        system.assertEquals(true,errorMsg!=null);
    }
    static testMethod void setOrderContext_test() {
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStatePlannedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        Test.startTest();
        localObj.setOrderContext(obj,oe);
        Test.stopTest();
        System.AssertEquals(true,localObj.order != null);
        System.AssertEquals(true,localObj.orderEvent.getEventName() == 'UserSubmit');
    }
    static testMethod void setOrderDomainObject_test() {
        EP_VmiNonConsignmentOrderStatePlanned localObj = new EP_VmiNonConsignmentOrderStatePlanned();
        EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStatePlannedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.setOrderDomainObject(obj);
        Test.stopTest();
        System.assertEquals(true, localObj.order == obj);
    }
}