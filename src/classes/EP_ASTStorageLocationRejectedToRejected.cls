/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationRejectedToRejected>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 08-Rejected to 08-Rejected>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationRejectedToRejected extends EP_AccountStateTransition {

    public EP_ASTStorageLocationRejectedToRejected () {
        finalState = EP_AccountConstant.REJECTED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationRejectedToRejected','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationRejectedToRejected', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationRejectedToRejected',' isGuardCondition');        
        return true;
    }
}