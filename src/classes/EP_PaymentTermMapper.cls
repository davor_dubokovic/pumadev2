/**
   @Author          CR Team
   @name            EP_PaymentTermMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to EP_PaymentTerm__c Object
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    10/01/2017          Swapnil Gorane          Added Method -> getPaymentTermRecordsAsPerName(String )
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_PaymentTermMapper {
        
    /**
    *  Constructor. 
    *  @name            EP_PaymentTermMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */ 
    public EP_PaymentTermMapper() {}


   /** Returns Payment Term Record by Id.
    *  @name             getRecordsById
    *  @param            paymentTermId
    *  @return           EP_Payment_Term__c
    *  @throws           NA
    */ 
    public EP_Payment_Term__c getRecordsByName(String paymentTermName) {
        EP_GeneralUtility.Log('Public','EP_PaymentTermMapper','getRecordsByName');

        List<EP_Payment_Term__c> objPaymentTerm = new List<EP_Payment_Term__c>([SELECT Id, Name,EP_Payment_Term_Code__c 
        FROM EP_Payment_Term__c 
        WHERE Name =:paymentTermName]);
        if(!objPaymentTerm.isEmpty()){
        	return objPaymentTerm[0];
        }
        return null;
    }
    
    /* Returns Records by Payment Term Code.
    *  @name             getRecordsByPayTermCodes
    *  @param            set<string>, Integer
    *  @return           list<EP_Payment_Term__c>
    *  @throws           NA
    */ 
    public list<EP_Payment_Term__c> getRecordsByPayTermCodes(set<string> stringSet) {
        EP_GeneralUtility.Log('Public','EP_PaymentTermMapper','getRecordsByPayTermCodes');
        list<EP_Payment_Term__c> payTermObjList = new list<EP_Payment_Term__c>();
        for(list<EP_Payment_Term__c> payTermList:[      SELECT 
            Id 
            , EP_Payment_Term_Code__c 
            FROM EP_Payment_Term__c 
            WHERE EP_Payment_Term_Code__c IN :stringSet 
                                                //LIMIT :limitVal
                                                ]){
            payTermObjList.addAll(payTermList);
        }                                                  
        return payTermObjList;
    }
       
    /** This method returns All  EP_PaymentTermMapper Records.
    *  @name            getOrderFieldsetSfdcWindmsRec
    *  @param           Integer 
    *  @return          list<EP_Order_Fieldset_Sfdc_Windms_Intg__c>
    *  @throws          NA
    */
    public list<EP_Payment_Term__c> getAllPaymentTerms (Integer nRows) {
        EP_GeneralUtility.Log('Public','EP_PaymentTermMapper','getAllPaymentTerms');
        List<EP_Payment_Term__c> paymentTermList = new List<EP_Payment_Term__c>();
        for( List<EP_Payment_Term__c> paymentTerm:[Select Id,Name,
            EP_Payment_Term_Code__c 
            from EP_Payment_Term__c 
            Limit :nRows]){
         paymentTermList.addAll(paymentTerm);
     }
     return  paymentTermList;
 }
	/**
	* @author 			Accenture
	* @name				getPaymentTermsMapByCodes
	* @date 			03/13/2017
	* @description 		Will return the map of payment terms records for the set of payment terms.
	* @param 			Set<String>
	* @return 			Map<String,Id>
	*/
    public Map<String,Id> getPaymentTermsMapByCodes(Set<String> setCustomerPaymentTerm){
        EP_GeneralUtility.Log('Private','EP_PaymentTermMapper','getPaymentTermsMapByCodes');
        Map<String,Id> mapPaymentTermPaymentIDs = new Map<String,Id>();
        if(!setCustomerPaymentTerm.isEmpty()){
        	for(EP_Payment_Term__c pymntTrm : getRecordsByPayTermCodes(setCustomerPaymentTerm)){
                mapPaymentTermPaymentIDs.put(pymntTrm.EP_Payment_Term_Code__c.toUpperCase(),pymntTrm.id);
            }
        }
        return mapPaymentTermPaymentIDs;
    } 

     /** This method returns All  EP_PaymentTermMapper Records.
    *  @name            getAllPaymentTerms
    *  @param           Integer 
    *  @return          list<EP_Order_Fieldset_Sfdc_Windms_Intg__c>
    *  @throws          NA
    */
    public list<EP_Payment_Term__c> getAllPaymentTerms () {
        EP_GeneralUtility.Log('Public','EP_PaymentTermMapper','getAllPaymentTerms');
        List<EP_Payment_Term__c> paymentTermList = new List<EP_Payment_Term__c>();
        for( List<EP_Payment_Term__c> paymentTerm:[Select Id,Name,
            EP_Payment_Term_Code__c 
            from EP_Payment_Term__c]){
         paymentTermList.addAll(paymentTerm);
     }
     return  paymentTermList;
 }
 /* L4 - 45352 -- unique field changes start*/
  /** This method returns EP_PaymentTermMapper Record bases on payment term code and company code.
    *  @name            getPaymentTermsfromCompanyAndPaymentCode 
    *  @param           String
    *  @return          EP_Payment_Term__c
    *  @throws          NA
    */
    public static EP_Payment_Term__c getPaymentTermsfromCompanyAndPaymentCode(String paymentcode,String CompanyCode) {
        EP_GeneralUtility.Log('Public','EP_PaymentTermMapper','getPaymentTermsfromCompanyAndPaymentCode');
        
        EP_Payment_Term__c pymentTerm = [SELECT Id,EP_Company__r.EP_Company_Code__c FROM EP_Payment_Term__c WHERE EP_Payment_Term_Code__c =: paymentcode and EP_Company__r.EP_Company_Code__c = : CompanyCode limit 1];
        return pymentTerm;
     }
     
 
   /* L4 - 45352 -- unique field changes end*/
   
	/*Defect 71448*/
	public static EP_Payment_Term__c getPaymentTermById(string id){
		EP_GeneralUtility.Log('Public','EP_PaymentTermMapper','getPaymentTermById');
        EP_Payment_Term__c pymentTerm = [SELECT Id,EP_Company__r.EP_Company_Code__c,Name,EP_Payment_Term_Code__c FROM EP_Payment_Term__c WHERE Id =: id limit 1];
        return pymentTerm;  
	}
	/*Defect 71448*/  

	/**BugFix-71967**/
	/**
	* @author 			Accenture
	* @name				getPaymentTermMapForCompany
	* @date 			01/05/2018
	* @description 		To get Map of payment method for a company
	* @param 			id
	* @return 			Map<Id,string>
	*/
	public static Map<Id,string> getPaymentTermMapForCompany (Id companyId){
		EP_GeneralUtility.Log('Public','EP_PaymentTermMapper','getPaymentTermMapForCompany');
		Map<Id,string> paymentTermMap = new Map<Id,string>();
		for(EP_Payment_Term__c paytrm : [SELECT Id, EP_Payment_Term_Code__c,EP_Company__c FROM EP_Payment_Term__c WHERE EP_Company__c =: companyId]){
			paymentTermMap.put(paytrm.id,paytrm.EP_Payment_Term_Code__c);
		}
		return paymentTermMap;
	}
	/**BugFix-71967**/
        
}