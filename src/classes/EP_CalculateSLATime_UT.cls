@isTest
public class EP_CalculateSLATime_UT{
    static testMethod void calculateMilestoneTriggerTime_test() {       

        MilestoneType[] mtLst = [SELECT Id, Name FROM MilestoneType LIMIT 1];     
        if(mtLst.size() == 0) { return; }
        MilestoneType mt = mtLst[0];
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];

        Company__c newcompany = EP_TestDataUtility.createCompany('comp_sample');
        newcompany.EP_Business_Hours__c =stdBusinessHours.id;
        insert newcompany ;
        system.debug('newcompany-->'+newcompany.id);
        
        EP_Company_Case_configuration__c testobj = new EP_Company_Case_configuration__c();
        testobj.EP_Company__c = newcompany.id ;
        testobj.EP_Category__c = 'Credit';
        testobj.EP_Sub_Category__c = 'Collection';
        testobj.EP_Queue__c = 'EP_Credit';
        testobj.EP_SLA_hours__c = 2 ;
        Database.insert(testobj);
        
        system.debug('testobj-->'+testobj.id);
        Account objAccount =EP_TestDataUtility.createSellToAccount(null,null);
        objAccount.EP_Puma_Company__c =newcompany.id ;
        insert objAccount ;
         system.debug('objAccount -->'+objAccount.id);
         
        Case c = new Case(priority = 'High');
        c.accountid = objAccount.id;
        c.type = 'Credit';
        c.EP_FE_Sub_Type__c= 'Collection';
        insert c;
         system.debug('c-->'+c.id);
        EP_CalculateSLATime calculator = new EP_CalculateSLATime();
        
        Test.startTest();
        Integer actualTriggerTime = calculator.calculateMilestoneTriggerTime(c.Id, mt.Id);
        Test.stopTest();
        System.assertEquals(actualTriggerTime, 120);
        
        }
        
        static testMethod void timeCalculation_test() {       
            BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];

            Company__c newcompany = EP_TestDataUtility.createCompany('comp_sample');
            newcompany.EP_Business_Hours__c =stdBusinessHours.id;
            insert newcompany ;
            
            EP_Company_Case_configuration__c testobj = new EP_Company_Case_configuration__c();
            testobj.EP_Company__c = newcompany.id ;
            testobj.EP_Category__c = 'Credit';
            testobj.EP_Sub_Category__c = 'Collection';
            testobj.EP_Queue__c = 'EP_Credit';
            testobj.EP_SLA_hours__c = 2 ;
            Database.insert(testobj);
            EP_Company_Case_configuration__c objComCaseConf =EP_CompanyCaseConfigurationMapper.FetchRecord(newcompany.id,'Credit','Collection');
            EP_CalculateSLATime calculator = new EP_CalculateSLATime();
            Test.startTest();
            Integer actualTriggerTime = calculator.timeCalculation(objComCaseConf );
            Test.stopTest();
            System.assertEquals(actualTriggerTime, 120);
        }
        
        static testMethod void CalculateTimeInaDay_test() {       
            BusinessHours stdBusinessHours = [select id,MondayStartTime, MondayEndTime from BusinessHours where IsDefault = true];
            EP_CalculateSLATime calculator = new EP_CalculateSLATime();
            Test.startTest();
            Integer actualTriggerTime = calculator.CalculateTimeInaDay(stdBusinessHours);
            Test.stopTest();
            System.assertNOTEquals(stdBusinessHours , null);
        }
}