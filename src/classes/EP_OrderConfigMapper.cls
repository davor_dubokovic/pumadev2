/** 
   @Author          Shakti Mehtani
   @name            EP_OrderConfigMapper
   @CreateDate      12/28/2016
   @Description     This class contains all SOQLs related to Order Configuration Object
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    *************************************************************************************************************
    */
    public with sharing class EP_OrderConfigMapper {
    /**
    *  Constructor. 
    *  @name            EP_OrderConfigMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */
    public EP_OrderConfigMapper() {
        
    } 

     /**  This method is used to get list of Order Configuration records by set of IDs and Country 
    *  @date            12/28/2016
    *  @name            getRecordsByIdsNCountry
    *  @param           Set<Id> setProductId, String strCountry
    *  @return          list<EP_Order_Configuration__c>
    *  @throws          NA
    */
    public list<EP_Order_Configuration__c> getRecordsByIdsCountry(List<Id> setProductId, String strCountry) {
        EP_GeneralUtility.Log('Public','EP_OrderConfigMapper','getRecordsByIdsCountry');

        list<EP_Order_Configuration__c> ordConfObjList = [SELECT EP_Max_Quantity__c, EP_Min_Quantity__c, EP_Product__c, 
        RecordType.DeveloperName, EP_Volume_UOM__c
        FROM EP_Order_Configuration__c
        WHERE EP_Country__c =: strCountry 
        AND EP_Product__c IN: setProductId
        ORDER BY EP_Min_Quantity__c ASC];      
        return ordConfObjList ; 
    }
}