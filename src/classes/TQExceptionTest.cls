@isTest
/**
*
**/
private class TQExceptionTest {
/**
* 
**/ 
    static testMethod void testSetterGetter() {
        Profile cscAgent = [Select id from Profile
                                    Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE
                                    limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Database.insert (cscUser);
        System.runAs(cscUser){
        Integer code = 404;

        TQException ex = new TQException(code);

        System.assertEquals(code, ex.getErrorCode());
        }
    }
}