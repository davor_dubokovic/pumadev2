/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToInvoiced>
*  @CreateDate <03/02/2017>
*  @Description <Handles status change from any status to Invoiced>
*  @Version <1.0>
*/

public class EP_OSTANYToInvoiced extends EP_OrderStateTransition{
    static string classname = 'EP_OSTANYToInvoiced';
    public EP_OSTANYToInvoiced(){
        finalState = EP_OrderConstant.OrderState_Invoiced;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToInvoiced','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToInvoiced','isRegisteredForEvent');        
        return super.isRegisteredForEvent();       
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTANYToInvoiced','isGuardCondition');            
        return true;               
    }
    
}