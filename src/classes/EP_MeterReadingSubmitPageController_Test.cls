///* 
//  @Author <Spiros Markantonatos>
//   @name <EP_MeterReadingSubmitPageController>
//   @CreateDate <06/11/2014>
//   @Description <This class is used by the users to select the site that they want to submit a Meter Readings>
//   @Version <1.0>
//*/
//@isTest
public class EP_MeterReadingSubmitPageController_Test{
//
//    private static List<Account> accList; 
//    private static EP_Tank_Dip__c  TankDipInstance; 
//    private static Account currentAccount; 
//    private static EP_Tank__c TankInstance; 
//    private static EP_Meter__c ExistingMeter;      
//    private Static Product2 currentProduct;
//    private Static EP_Meter_Reading__c oldreading;
//    private Static Datetime dtFilterDateTime;
//    private static user currentrunningUser;
//   
//    /**************************************************************************
//    *@Description : This method is used to create Data.                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init(){
//         currentrunningUser=EP_TestDataUtility.createTestRecordsForUser();
//         accList= EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
//         TankInstance= EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//         currentAccount =accList.get(0);
//         currentAccount.EP_Status__c =Label.EP_Active_Ship_To_Status_Label;
//         update currentAccount;
//         currentProduct=EP_TestDataUtility.createTestRecordsForProduct();
//         ExistingMeter=EP_TestDataUtility.createTestRecordsForMeter(String.valueof(accList.get(0).id),currentProduct.id);    
//         oldreading= EP_TestDataUtility.createTestRecordsForMeterReading(ExistingMeter.id);
//         oldreading.EP_Closing_Meter_Reading__c=10;
//         oldreading.EP_Reading_Date_Time__c=System.now().addDays(-2);
//         update oldreading;
//    }
//    
//    /***************************************************************************
//    *@Description : This method is used to test the submission of meter Reading*
//    *@Params      : none                                                       *
//    *@Return      : void                                                       *    
//    ****************************************************************************/        
//    private static testMethod void testMeterReadingSubmission(){ 
//     init();   
//     Test.startTest();  
//         system.runas(currentrunningUser){     
//             PageReference pageRef = Page.EP_MeterReadingSubmitPage;
//             Test.setCurrentPage(pageRef);//Applying page context here     
//             ApexPages.currentPage().getParameters().put('id', currentAccount.Id);
//             EP_MeterReadingSubmitPageController controller = new EP_MeterReadingSubmitPageController();
//             controller.userSelectedDate=system.today();
//             controller.dateEvent=new Event(ActivityDate=system.today());
//             controller.UserSelectedReadingTime = system.now().time();
//             controller.retrieveShipToDetails();
//             controller.populateAvailableRecords();
//             controller.validate();
//             controller.next();
//             controller.save();
//             list<EP_Meter_Reading__c> lstMeterReading = new list<EP_Meter_Reading__c>([select id from EP_Meter_Reading__c]);
//             system.assertEquals(lstMeterReading .size(),1);
//             controller.cancelSummary();
//             controller.cancel(); 
//             system.assertEquals(controller.showSummaryPage,false);
//
//        }
//      Test.stopTest();  
//        
//    }    
//     
}