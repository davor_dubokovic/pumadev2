@isTest
public class EP_GeneralUtility_UT
{
    //#60147 User Story Start
     @testSetup static void init() {
         List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_Order_State_Mapping__c> lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
        EP_INTEGRATION_CUSTOM_SETTING__c integration_Setting = new EP_INTEGRATION_CUSTOM_SETTING__c(Name = 'Request TimeOut',EP_Value__c = '120000');
        insert integration_Setting; 
    }
    //#60147 User Story End
    static testMethod void getETARangeOnOrder_Step1_test() {
        String estimatedDtTm = EP_Common_Constant.TEMPDATE;
        Integer stsrtTm = EP_Common_Constant.ONE;
        Integer endTm = EP_Common_Constant.TWO;
        String ordStatus = EP_Common_Constant.delivered;
        Test.startTest();
        String result = EP_GeneralUtility.getETARangeOnOrder(estimatedDtTm,stsrtTm,endTm,ordStatus);
        Test.stopTest();
        system.assertEquals(true,result != EP_Common_Constant.BLANK && result.contains(EP_Common_Constant.TEMPDATE.substring(0,4)));
    }
    
    static testMethod void getETARangeOnOrder_Step2_test() {
        String estimatedDtTm = EP_Common_Constant.TEMPDATE;
        Integer stsrtTm = EP_Common_Constant.ONE;
        Integer endTm = EP_Common_Constant.TWO;
        String ordStatus = EP_Common_Constant.ORDER_STATUS_SUBMITTED ;
        Test.startTest();
        String result = EP_GeneralUtility.getETARangeOnOrder(estimatedDtTm,stsrtTm,endTm,ordStatus);
        Test.stopTest();
        system.assertEquals(true,result != EP_Common_Constant.BLANK && result.contains(EP_Common_Constant.TEMPDATE.substring(0,4)));
    }
    
    static testMethod void getEmailAddresses_test() {
        String groupName = 'Test Group';
        EP_TestDataUtility.createGroup();
        Set<String> grpName = new set<String> {groupName};
        Test.startTest();
        LIST<String> result = EP_GeneralUtility.getEmailAddresses(grpName);
        Test.stopTest();
        system.assertEquals(true,result != NULL && !result.isEmpty());
    }
    
    static testMethod void sendEmail_test() {
        Integer emailCountBefore = Limits.getEmailInvocations();
        String plainBody = EP_Common_Constant.TEMPEMAILBODYSTR;
        LIST<String> lstemailAdrreses = new List<String> {EP_Common_Constant.EMAILRECIPIENT};
        EmailTemplate tempId;
        User userObject = [Select Id,Name from USer where Id =: UserInfo.getUserId() ];
        System.RunAs(userObject){
            tempId = EP_TestDataUtility.createAndGetEmailTemplate();
        }
        csord__Order__c objOrder = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        Id targetId = userObject.Id;
        Id templateId = tempId.Id;
        String strSubject = EP_Common_Constant.TEMPSTR;
        Test.startTest();
        EP_GeneralUtility.sendEmail(plainBody,lstemailAdrreses,targetId,templateId,strSubject);
        Integer emailCountAfter = Limits.getEmailInvocations();    
        Test.stopTest();
        System.assertNotEquals(emailCountBefore,emailCountAfter);
    }
    static testMethod void removeHyphen_test() {
        String strText = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
        Test.startTest();
        String result = EP_GeneralUtility.removeHyphen(strText);
        Test.stopTest();
        System.AssertEquals(true,!result.Contains(EP_Common_Constant.hyphen));
    }
    static testMethod void convertSelectedDateToDateInstance_test() {
        String strSelectedDate = EP_Common_Constant.TEMPDATEV2;
        Test.startTest();
        Date result = EP_GeneralUtility.convertSelectedDateToDateInstance(strSelectedDate);
        Test.stopTest();
        system.assertEquals(true,EP_Common_Constant.TEMPDATEV2.contains(string.valueOf(result.day())));
        system.assertEquals(true,EP_Common_Constant.TEMPDATEV2.contains(string.valueOf(result.Month())));
        system.assertEquals(true,EP_Common_Constant.TEMPDATEV2.contains(string.valueOf(result.Year())));
    }
    static testMethod void convertSelectedHourToTime_Step1_test() {
        String availableSlots = EP_Common_Constant.TEMPHOUR;
        Test.startTest();
        Time result = EP_GeneralUtility.convertSelectedHourToTime(availableSlots);
        Test.stopTest();
        System.AssertEquals(true,EP_Common_Constant.TEMPHOUR.equals(string.valueOf(result.Hour())));
    }
    
    static testMethod void convertSelectedHourToTime_Step2_test() {
        String availableSlots = '11:30:43';
        Test.startTest();
        Time result = EP_GeneralUtility.convertSelectedHourToTime(availableSlots);
        Test.stopTest();
        System.AssertNotEquals(null,result);
    }

    static testMethod void convertSelectedHourToTime_Blank_test() {
        String availableSlots = EP_Common_Constant.BLANK;
        Test.startTest();
        Time result = EP_GeneralUtility.convertSelectedHourToTime(availableSlots);
        Test.stopTest();
        System.AssertEquals(Time.newInstance(0,0,0,0),result);
    }
    
    static testMethod void getCurrencyFormat_test() {
        Test.startTest();
        String result = EP_GeneralUtility.getCurrencyFormat();
        Test.stopTest();
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_QUERY1));
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_HASH));
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_HASH_1));
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_HASH_2));
    }
    static testMethod void convertDateTimeToDecimal_test() {
        String strSelectedDateTime = EP_Common_Constant.TEMPDATEV3;
        Test.startTest();
        Decimal result = EP_GeneralUtility.convertDateTimeToDecimal(strSelectedDateTime);
        Test.stopTest();
        System.AssertEquals(true,EP_Common_Constant.TEMPDATE.equals(string.valueOf(result)));
    }
    static testMethod void Log_test() {
        EP_Debug__c debugObject = new EP_Debug__c(name='test debug',Enable__c=true,access_modifier__c='public::private');
        insert debugObject;
        String publicOrPrivate = EP_Common_Constant.PUBLICSTR;
        String ClassName = EP_Common_Constant.TESTCLASSNAMESTR;
        String MethodName = EP_Common_Constant.TESTMETHOSNAMESTR;
        Test.startTest();
        EP_GeneralUtility.Log(publicOrPrivate,ClassName,MethodName);
        Test.stopTest();
        System.AssertEquals(true,EP_GeneralUtility.counter > 0);
    }
    static testMethod void setLogSettings_test() {
        Test.startTest();
        EP_GeneralUtility.setLogSettings();
        Test.stopTest();
        System.AssertEquals(true,!EP_GeneralUtility.debugEnabled);
    }
    static testMethod void getNumericDisplayFormat_test() {
        Test.startTest();
        String result = EP_GeneralUtility.getNumericDisplayFormat();
        Test.stopTest();
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_QUERY1));
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_HASH));
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_HASH_1));
        system.assertEquals(true,result.contains(EP_Common_Constant.STR_HASH_2));
    }
    
    static testMethod void getCompositeKey_test() {
        String part1 = EP_Common_Constant.STEP_ONE;
        String part2 = EP_Common_Constant.STEP_TWO;
        String part3 = EP_Common_Constant.STEP_THREE;
        Test.startTest();
        String result = EP_GeneralUtility.getCompositeKey(part1,part2,part3);
        Test.stopTest();
        String compareStr = part1 + EP_COMMON_CONSTANT.HYPHEN +  part2 + EP_COMMON_CONSTANT.HYPHEN + part3;
        System.AssertEquals(true, result.equalsIgnoreCase(compareStr));
    }
    
    static testMethod void setDateFormat_test() {
        String dateString;
        Test.startTest();
        dateString = EP_GeneralUtility.setDateFormat(String.valueOf(System.today()));
        Test.stopTest();
        system.assert(dateString!=NULL);
    }
    
    static testMethod void getCompositeKeyWithTwoParam_test() {
        String part1 = EP_Common_Constant.STEP_ONE;
        String part2 = EP_Common_Constant.STEP_TWO;
        Test.startTest();
        String result = EP_GeneralUtility.getCompositeKey(part1,part2);
        Test.stopTest();
        String compareStr = part1 + EP_COMMON_CONSTANT.HYPHEN +  part2;
        System.AssertEquals(true,result.equalsIgnoreCase(compareStr));
    }
    
    static testMethod void getDMLErrorMessage_Test() {
        Database.SaveResult errorResult;
        Account acc = new Account();
        errorResult = Database.insert(acc,false);
        String result;  
        Test.startTest();
        result = EP_GeneralUtility.getDMLErrorMessage(errorResult);
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }
    
    
    static testMethod void getDMLErrorMessageUpdate_Test() {
        Database.UpsertResult errorResult;   
        Account acc = new Account();
        errorResult = Database.upsert(acc,false);
        String result;
        Test.startTest();
        result = EP_GeneralUtility.getDMLErrorMessage(errorResult);
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }
    
    
    static testMethod void returnLocalDateTime_Test() {
        DateTime result;
        Test.startTest();
        result = EP_GeneralUtility.returnLocalDateTime(System.now());
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }
    
    static testMethod void formatNumber_Test() {
        String str1 = 'test,test';
        String str2 = 'test test';
        String result; 
        Test.startTest();
        result = EP_GeneralUtility.formatNumber(str1);
        Test.stopTest();
        System.AssertNotEquals(str2,result);
    }
    
    /*static testMethod void formatDatePositive_Test() {
        String dateTimeStr =  String.valueOf(System.today());
        Test.startTest();
        Date result = EP_GeneralUtility.formatDate(String.valueOf(System.now()));
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }*/
    
    static testMethod void formatDateNegative_Test() {
        String emptyString = EP_Common_Constant.BLANK;
        Test.startTest();
        Date result = EP_GeneralUtility.formatDate(emptyString);
        Test.stopTest();
        System.AssertEquals(NULL,result);
    }
    
    static testMethod void formatNAVDate_Test() {
        Date result;
        Test.startTest();
        result = EP_GeneralUtility.formatNavDate(String.valueOf(System.today()));
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }
    
    static testMethod void formatNavDateNegative_Test() {
        String emptyString = EP_Common_Constant.BLANK;
        Date result; 
        Test.startTest();
        result = EP_GeneralUtility.formatNavDate(emptyString);
        Test.stopTest();
        System.AssertEquals(NULL,result);
    }
    
    static testMethod void createMessageGeneratorRecord_Test() {
        String result;
        Test.startTest();
        result = EP_GeneralUtility.createMessageGeneratorRecord();
        Test.stopTest();
        EP_Message_Id_Generator__c msgGenerator = [Select id, Name from EP_Message_Id_Generator__c Limit :EP_COMMON_CONSTANT.ONE];
        System.AssertEquals(msgGenerator.Name,result);
    }
    
    static testMethod void isDateTimeValid_Test() {
        Boolean result;
        Decimal currentTime = EP_GeneralUtility.convertDateTimeToDecimal(String.valueOf(System.now()));
        Test.startTest();
        result = EP_GeneralUtility.isDateTimeValid(String.valueOf(currentTime));
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void updateOrderStatus_Test() {
        List<csord__Order__c> orderList = new List<csord__Order__c>();
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        ord.Status__c = EP_Common_Constant.ORDER_STATUS_SUBMITTED;
        orderList.add(ord);
        update orderList;
        Test.startTest();
        EP_GeneralUtility.updateOrderStatus(orderList);
        Test.stopTest();
        //Method is delegating another methode, hence added dummy assert
        System.AssertEquals(true,true);
    }
    
    static testMethod void getNumberOfPackagedProducts_Test() {
        Integer packagedProducts;
        Account acc = EP_TestDataUtility.getSellTo();
        Id prcBookId = acc.EP_PriceBook__c;
        Test.startTest();
        packagedProducts = EP_GeneralUtility.getNumberOfPackagedProducts(prcBookId);
        Test.stopTest();
        System.Assert(packagedProducts > 0 );
    }
    //L4 - #45514 and #45515 code changes start
    static testmethod void stringToBoolenConversion_test1() {
        String yesString = EP_Common_Constant.YES;
        boolean check1;
        Test.startTest();
        check1 = EP_GeneralUtility.stringToBoolenConversion(yesString);
        Test.stopTest();
        System.AssertEquals(true,check1);
    }
    static testmethod void stringToBoolenConversion_test2() {
        String noString = EP_Common_Constant.NO;
        boolean check2;
        Test.startTest();
        check2 = EP_GeneralUtility.stringToBoolenConversion(noString);
        Test.stopTest();
        System.AssertEquals(false,check2);
    }
    static testmethod void stringToBoolenConversion_test3() {
        String trueString = EP_Common_Constant.STRING_TRUE;
        boolean check3;
        Test.startTest();
        check3 = EP_GeneralUtility.stringToBoolenConversion(trueString);
        Test.stopTest();
        System.AssertEquals(true,check3);
    }
    static testmethod void stringToBoolenConversion_test4() {
        String falseString = EP_Common_Constant.STRING_FALSE;
        boolean check4;
        Test.startTest();
        check4 = EP_GeneralUtility.stringToBoolenConversion(falseString);
        Test.stopTest();
        System.AssertEquals(false,check4);
    }
    
    //L4 - #45514 and #45515 code changes end
}