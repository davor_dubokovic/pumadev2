/*
  @Author <Ashok Arora>
  @name <EP_PayLoadHandler_Test>
  @CreateDate <20/10/2015>
  @Description <This is the test class for XML payload generator for Nav and Lomosoft>
  @Version <1.0>
 
*/
@isTest
private  with sharing class EP_PayLoadHandler_Test {
    /*
    ~comment start~* This methods unit tests the creation of Payload and callout the Customer Nav WebService
    *~comment end~
    private static testMethod void navBulkCustomersTest(){
        EP_TestDataUtility.stopAccountTrigger();
        EP_TestDataUtility.stopUserTrigger();  
        Company__c company = EP_TestDataUtility.createCompany(
                                                EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())));
        database.insert(company);
        Account transporter = EP_TestDataUtility.createTestVendorWithoutCompany(
                                                       'Transporter',
                                                            EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())),
                                                                company.Id);
        database.insert(transporter);

        System.runAs(EP_TestDataUtility.CSC_USER){
             String XMLString;
             EP_TestDataUtility.WrapperCustomerHierarchy customerHiearchy 
                                            = EP_TestDataUtility.createCustomerHierarchyForNAVWOTransporter(40,company.id, transporter.id);
            
             Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
             Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
             Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
             Test.startTest();
                
                //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
                XMLString = EP_PayLoadHandler.createNavBulkCustomerPayLoad(customerHiearchy.sCustomerId);
                //Callout Nav cusomer Integratiion webservice
               ~comment start~ EP_Trafigura_WS.BasicHttpBinding_ITwoWayAsync basicBinding  = new EP_Trafigura_WS.BasicHttpBinding_ITwoWayAsync();
                EP_SFDC_Customer_NAV_Intg_WebService.HeaderCommon_element headerElement = new EP_SFDC_Customer_NAV_Intg_WebService.HeaderCommon_element();
                EP_SFDC_Customer_NAV_Intg_WebService.Payload_element payLoadElement = new EP_SFDC_Customer_NAV_Intg_WebService.Payload_element();
                payLoadElement.Any_x = EncodingUtil.base64Encode(Blob.valueOf(XMLString));
                EP_SFDC_Customer_NAV_Intg_WebService.MSG_element msgElement =  basicBinding.Operation_SFDCToNav (headerElement,payLoadElement,EP_Common_Constant.BLANK);~comment end~
            
             Test.stopTest();
             System.assertNotEquals(null,XMLString);
             //System.assertEquals('Success_Mock_NAV',msgElement.StatusPayload);
        }
        
    }
    
    ~comment start~* This methods unit tests the creation of Payload and callout the Customer Nav WebService
    *~comment end~
    private static testMethod void createOrderXML_WinDMSTest(){
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
            EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
            //EP_AccountTriggerHandler.isRecordOnInsert = true;

        EP_TestDummyData.intializePricingHandlerData();
       
        Map<Id, Order> orderMap = new Map<Id, Order>([Select Id from order limit 10]);
         Test.startTest();
        System.runAs(EP_TestDataUtility.CSC_USER){
         String XMLString;
         Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
         Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
         Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
         Test.loadData(EP_Order_Fieldset_Sfdc_Windms_Intg__c.SobjectType,'TestData_EP_Order_Fieldset_Sfdc_Windms_Intg');
                    
         //XMLString = EP_PayLoadHandler.createOrderXML_WinDMS(orderMap.keySet()); 
                
         System.assertNotEquals(null,XMLString);
          EP_Order_Fieldset_Sfdc_Nav_Intg__c fieldset = new EP_Order_Fieldset_Sfdc_Nav_Intg__c ();
          fieldset.Name = 'orderFieldset';
          fieldset.Field_API_Name__c = 'Name';
          fieldset.Object_Name__c = 'Order';
          insert fieldset;
          //XMLString = EP_PayLoadHandler.createOrderXML(orderMap.keySet()); 
        //System.assertEquals('Success_Mock_NAV',msgElement.StatusPayload);
        }
        Test.stopTest();        
    }
    
    ~comment start~* This methods unit tests the creation of Payload and callout the Customer Nav WebService
    *~comment end~
    private static testMethod void createPayLoadTest(){
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
            EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
            //EP_AccountTriggerHandler.isRecordOnInsert = true;
        EP_TestDummyData.intializePricingHandlerData();
        id orderId= [Select Id from order limit 1].id;
       
        Map<Id, Order> orderMap = new Map<Id, Order>([Select Id from order limit 10]);
         Test.startTest();
        System.runAs(EP_TestDataUtility.CSC_USER){
             String XMLString;
             SObject sfObject;
             List<String>lFieldAPIName = new List<String>();
             lFieldAPIName.add('Name');
             Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
             Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
             Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
             Test.loadData(EP_Order_Fieldset_Sfdc_Windms_Intg__c.SobjectType,'TestData_EP_Order_Fieldset_Sfdc_Windms_Intg');
                        
             //XMLString = EP_PayLoadHandler.createOrderXML_WinDMS(orderMap.keySet());        
             System.assertNotEquals(null,XMLString);
             //String stringPay= EP_PayLoadHandler.createRecordPayload(orderId,'Order','orderFields');
             //stringPay = EP_PayLoadHandler.createPayLoad(sfObject,lFieldAPIName );
        }
        Test.stopTest();        
    }


    ~comment start~* This methods unit tests the creation of Ship To Address payload for Lomosoft
    *~comment end~
    private static testMethod void LomoSoftBulkShipToTest(){
        EP_TestDataUtility.stopAccountTrigger();
        EP_TestDataUtility.stopUserTrigger();  
        Company__c company = EP_TestDataUtility.createCompany(
                                            EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())));
        database.insert(company);
        Account transporter = EP_TestDataUtility.createTestVendorWithoutCompany(
                                                   'Transporter',
                                                        EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())),
                                                            company.Id);
        database.insert(transporter);

        System.runAs(EP_TestDataUtility.CSC_USER){
        String XMLString;
        EP_TestDataUtility.WrapperCustomerHierarchy customerHiearchy 
                        = EP_TestDataUtility.createCustomerHierarchyForNAVWOTransporter(10,company.id, transporter.id);    
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
         Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
         Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        Test.startTest();
            
            //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
            XMLString = EP_PayLoadHandler.createLomoSoftBulkShiptoPayLoad(customerHiearchy.sShipToId);
             set<id>accountid = new set<id>();
        accountid.add(transporter.id);
        EP_PayLoadHandler.createPricingEnginePayLoad(accountid,accountid);
            //Callout to cutomer import Lomosoft
           ~comment start~ EP_Trafigura_Lomo_WS.BasicHttpBinding_ITwoWayAsync basicBinding = new  EP_Trafigura_Lomo_WS.BasicHttpBinding_ITwoWayAsync ();
            EP_SFDC_Cstmr_Lomo_Intg_WebService.Payload_element payload = new EP_SFDC_Cstmr_Lomo_Intg_WebService.Payload_element(); 
            payload.Any_x = EncodingUtil.base64Encode(blob.valueOf(XMLString));
           EP_SFDC_Cstmr_Lomo_Intg_WebService.HeaderCommon_element  HeaderCommon = new EP_SFDC_Cstmr_Lomo_Intg_WebService.HeaderCommon_element();
            EP_SFDC_Cstmr_Lomo_Intg_WebService.MSG_element msgElement = basicBinding.Operation_CustomerImport(HeaderCommon
                                                                                                                 , payload
                                                                                                                 , '' );~comment end~
            
        Test.stopTest();
        System.assertnotEquals(null,XMLString);
        //System.assertEquals('Success_Mock_Lomo',msgElement.StatusPayload);
        }        
    }
    
    ~comment start~* This methods unit tests the creation of a single  Ship To Address payload for Nav
    *~comment end~
    private static testMethod void navShipToPayloadTest(){
        
        String NAV_SHIP_TO_FIELDS = 'Nav_ShipToFields';
        EP_TestDataUtility.stopAccountTrigger();
        EP_TestDataUtility.stopUserTrigger();  
        Company__c company = EP_TestDataUtility.createCompany(
                                            EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())));
        database.insert(company);
        Account transporter = EP_TestDataUtility.createTestVendorWithoutCompany(
                                                   'Transporter',
                                                        EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())),
                                                            company.Id);
        database.insert(transporter);
     

        System.runAs(EP_TestDataUtility.CSC_USER){
        String XMLString;
        EP_TestDataUtility.WrapperCustomerHierarchy customerHiearchy 
                = EP_TestDataUtility.createCustomerHierarchyForNAVWOTransporter(1,company.id, transporter.id);
        Test.startTest();
            //EP_PayloadHelper.WrapperFields shipToWrapperFields = EP_PayloadHandler.createFieldsWrapper('Account',NAV_SHIP_TO_FIELDS );
            //String shipToQuery = 'Select ' + shipTOWrapperFields.fieldString + ' from Account where Id =\''+customerHiearchy.lShipToAccounts[0].id+'\'    ';
           
            Account shipto  = DataBase.query(shipToQuery );
            //XMLString = EP_PayLoadHandler.createNavShipToPayload(shipTo,shipToWrapperFields .lFieldAPINames);
             set<id>accountid = new set<id>();
        accountid.add(transporter.id);
        EP_PayLoadHandler.createPricingEnginePayLoad(accountid,accountid);
        EP_PayLoadHandler.createNAVBulkShipToEditPayLoad(accountid);
        Test.stopTest();
        System.assertNotEquals(null,XMLString);
        }
    }
    
    ~comment start~* This methods unit tests the creation of a single bankAccount payload for Nav
    *~comment end~
    private static testMethod void navBankAccountPayloadTest(){
        EP_TestDataUtility.stopAccountTrigger();
        EP_TestDataUtility.stopUserTrigger();  
        String NAV_BANK_FIELDS = 'Nav_BankAccountFields' ;
        Company__c company = EP_TestDataUtility.createCompany(
                                            EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())));
        database.insert(company);
        Account transporter = EP_TestDataUtility.createTestVendorWithoutCompany(
                                                   'Transporter',
                                                        EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())),
                                                            company.Id);
        database.insert(transporter);
     

        System.runAs(EP_TestDataUtility.CSC_USER){
        String XMLString;
        EP_TestDataUtility.WrapperCustomerHierarchy customerHiearchy 
            = EP_TestDataUtility.createCustomerHierarchyForNAVWOTransporter(1,company.id, transporter.id);
        Test.startTest();
            EP_PayloadHelper.WrapperFields bankAccountWrapperFields = EP_PayloadHandler.createFieldsWrapper(EP_Common_Constant.BANK_ACCOUNT_OBJ
                                                                            ,NAV_BANK_FIELDS);

            String bankAccountQuery = 'Select ' + bankAccountWrapperFields.fieldString + ' from EP_Bank_Account__c where id =\''+customerHiearchy.lBankAccounts[0].id+'\''; 
            
            EP_Bank_Account__c bankAccount = DataBase.query(bankAccountQuery );
            XMLString = EP_PayLoadHandler.createNavBankAccountPayload(bankAccount ,bankAccountWrapperFields.lFieldAPINames);
        set<id>accountid = new set<id>();
        accountid.add(transporter.id);
        EP_PayLoadHandler.createPricingEnginePayLoad(accountid,accountid);
        ~comment start~
        map<id,order> maporderid = new map<id,order>();
        Order newOrder = EP_FE_TestDataUtility.createNonVMIOrder(); 
        maporderid.put(newOrder.id,newOrder );
        map<id,string> mapTemp = new map<id,string>();
        mapTemp.put(newOrder.id,'TEST');
        LIST<string> testlst = new list<string>();
        testlst.add('TEST1');
        EP_PayLoadHandler.createBulkCreditCheckOrders(maporderid,mapTemp,testlst);
        ~comment end~
        Test.stopTest();
        System.assertNotEquals(null,XMLString);
        }
    }
     ~comment start~* This methods unit tests the creation of a single tank payload for Nav
    *~comment end~
    private static testMethod void LomoSoftTankPayloadTest(){
        EP_TestDataUtility.stopAccountTrigger();
        EP_TestDataUtility.stopUserTrigger();  
        String LOMO_TANK_FIELDS = 'LM_TankFields';
        Company__c company = EP_TestDataUtility.createCompany(
                                            EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())));
        database.insert(company);
        Account transporter = EP_TestDataUtility.createTestVendorWithoutCompany(
                                                   'Transporter',
                                                        EP_Common_Constant.EPUMA +(String.valueOf(DateTime.now().millisecond())),
                                                            company.Id);
        database.insert(transporter);
     

        System.runAs(EP_TestDataUtility.CSC_USER){
        String XMLString;
        EP_TestDataUtility.WrapperCustomerHierarchy customerHiearchy 
                = EP_TestDataUtility.createCustomerHierarchyForNAVWOTransporter(1,company.id, transporter.id);
        Test.startTest();
            EP_PayloadHelper.WrapperFields tankWrapperFields = EP_PayloadHandler.createFieldsWrapper('EP_Tank__c',LOMO_TANK_FIELDS );
            String tankQuery = 'Select ' + tankWrapperFields.fieldString + ' from EP_Tank__c where Id =\''+customerHiearchy.lTanks[0].id+'\'';
            
            EP_Tank__c tank = DataBase.query(tankQuery );
            XMLString = EP_PayLoadHandler.createLomoSoftTankPayload(tank,tankWrapperFields .lFieldAPINames);
            
        set<id>accountid = new set<id>();
        accountid.add(transporter.id); 
        EP_PayLoadHandler.createPricingEnginePayLoad(accountid,accountid);
        
        Test.stopTest();
        System.assertNotEquals(null,XMLString);
        }
    }*/
}