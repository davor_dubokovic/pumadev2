///* 
//  @Author <Abhay Aror>
//   @name <EP_SelectTankDipDatePageExtensionClass>
//   @CreateDate <10/16/2015>
//   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
//   @Version <1.0>
// 
//*/
//@isTest
public class EP_SelectTankDipDatePageExtClass_Test{
//    private static user currentrunningUser;
//    private static List<Account> accList; 
//    private static List<EP_Tank_Dip__c> tankDipList; 
//    private static EP_Tank__c TankInstance; 
//    /**************************************************************************
//    *@Description : This method is used to create Data.                                  *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/    
//    private static void init(){
//         currentrunningUser=EP_TestDataUtility.createTestRecordsForUser();      
//         accList= EP_TestDataUtility.createTestAccount(new List<Account>{new Account()});
//         TankInstance= EP_TestDataUtility.createTestEP_Tank(accList.get(0));
//         tankDipList= EP_TestDataUtility.createTestEP_Tank_Dip (new List<EP_Tank_Dip__c>{EP_TestDataUtility.createTestRecordsForTankDip(TankInstance.id)}); 
//         for(EP_Tank_Dip__c dip:tankDipList){
//             dip.EP_Reading_Date_Time__c=system.now().addDays(-1);
//             dip.EP_Is_Placeholder_Tank_Dip__c=true;
//         }   
//         update  tankDipList;   
//    }
//    /**************************************************************************
//    *@Description : This method is used to test date being passed to page     *
//    *@Params      : none                                                      *
//    *@Return      : void                                                      *    
//    **************************************************************************/        
//    private static testMethod void testDateSelectionCase(){ 
//    init();   
//    Test.startTest();
//         system.runas(currentrunningUser){     
//             Account acc =accList.get(0);
//             acc.EP_Ship_To_UTC_Timezone__c='10:00';
//             acc.EP_Status__c ='Active';
//             update acc;
//             PageReference pageRef = Page.EP_SelectTankDipDatePage;
//             Test.setCurrentPage(pageRef);//Applying page context here  
//             ApexPages.currentPage().getParameters().put('id', acc.Id);
//             ApexPages.currentPage().getParameters().put('ret', 'account');     
//             ApexPages.currentPage().getParameters().put('date', '20151030');
//        
//             EP_SelectTankDipDatePageExtensionClass  controller = new EP_SelectTankDipDatePageExtensionClass ();
//             boolean blnRedirectToAccountPage = controller.blnRedirectToAccountPage;
//             controller.retrieveMissingSiteDates();
//             system.assertEquals(controller.tankDipDates.size(),2);
//             controller.dSelectedDate = system.now().Date();
//             controller.cancel();
//             controller.back();
//        }
//    Test.stopTest();  
//             
//   
//    }    
//    
//        
}