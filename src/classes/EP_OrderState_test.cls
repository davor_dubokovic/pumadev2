@isTest(seeAllData=false)
public class EP_OrderState_test {
    
    public static EP_OrderState orderState;
    //For state machine to give proper status
    //Test class Fix Start  
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    //Test class Fix End  
    // Do transitions testmethod 
    static testmethod void testDoTransition(){
        orderState = new EP_OrderState();
        EP_OrderDomainObject orderDomain = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        //Test class Fix Start
        orderDomain.setStatus('Planned');       
        EP_OrderEvent orderEvent = new EP_OrderEvent('Planned To Loaded');
        //Test class Fix End
        orderState.setOrderContext(orderDomain, orderEvent);
        orderState.doOnExit();
        orderState.doOnEntry();
        boolean result = orderState.doTransition();
        system.assert(result, true);
    }
    
    // test setOrderContext method
    static testmethod void testsetOrderContext(){
        orderState = new EP_OrderState();
        //Test class Fix Start
        //EP_OrderDomainObject orderDomain = new EP_OrderDomainObject('801p00000009jlU'); 
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        upsert orderObj;
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(orderObj.id);
        //Test class Fix End    
        EP_OrderEvent orderEvent = new EP_OrderEvent('Planned To Loaded');
        orderState.setOrderContext(orderDomain, orderEvent);
        system.assert((orderState.orderEvent != null && orderState.order !=null), true);  
    }
    
    //test ConvertCustomSettingsToObject method
    static testmethod void testConvertCustomSettingsToObject(){
        orderState = new EP_OrderState();
        //Test class Fix Start
        //EP_OrderDomainObject orderDomain = new EP_OrderDomainObject('801p00000009jlU');
        csord__Order__c orderObj = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        upsert orderObj;
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(orderObj.id);
        //Test class Fix End
        orderDomain.setWinDMSStatus('Loaded');
        orderDomain.setStatus('Planned');       
        EP_OrderEvent orderEvent = new EP_OrderEvent('Planned To Loaded');
        orderState.setOrderContext(orderDomain, orderEvent);
        List<EP_OrderStateTransition> listTransition = orderState.ConvertCustomSettingsToObject('Planned'); 
        system.assert((listTransition != null), true);
    }
   
}