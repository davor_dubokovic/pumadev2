/* 
   @Author <Ashok Arora>
   @name <EP_PayLoadHandler>
   @CreateDate <16/10/2015>
   @Description <This class will generate Payload for SObjects> 
   @Version <1.0> 
   */  
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    1/13/2017           Sandeep Kumar           Methods simplified and moved to other payload related classes.  
    1/31/2017           Sandeep Kumar           Documentation done for methods.
    *************************************************************************************************************
    */
    public with sharing class EP_PayLoadHandler {

        public static map<String,set<Id>>mObjectRecordId = new map<String,set<Id>>();
        public static map<String,String>mChildParent = new map<String,String>();
    public static Boolean isDeleteFor3rdPartyOrder = false; //defect #29683
    
    private static EP_CustomerPayLoad customerPayLoad = new EP_CustomerPayLoad();
    private static EP_ShipToPayload shipToPayLoad = new EP_ShipToPayload();
    private static EP_SObjectPayLoad sObjPayLoad = new EP_SObjectPayLoad();
    private static EP_PayloadHelper payLoadHelper = new EP_PayloadHelper();
    //private static EP_OrderPayLoad orderPayLoad = new EP_OrderPayLoad();
    private static EP_PayLoadService payLoadService = new EP_PayLoadService();
    public static string EXTERNAL_SYSTEM = null;
    
    /**
    *  This method creates Payload for bulk Customer record for NAVISION Customer Import
    *  @name    createNavBulkCustomerPayLoad
    *  @param   set<Id> sCustomerId
    *  @return  String
    *  @throws  NA
    */
    public static String createNavBulkCustomerPayLoad(set<Id> sCustomerId){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createNavBulkCustomerPayLoad');
        return customerPayLoad.createNavBulkCustomerPayLoad(sCustomerId);
    }
    
    /**
    *  @name    createPricingEnginePayLoad
    *  @param   set<Id>sCustomerId, set<Id>sShipTo
    *  @return  String
    *  @throws  NA
    */
    public static String createPricingEnginePayLoad(set<Id>sCustomerId,set<Id>sShipTo){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createPricingEnginePayLoad');
        return customerPayLoad.createPricingEnginePayLoad(sCustomerId, sShipTo);
    }
    
    /**
    *  This method creates paylEPoad for customers edit Nav
    *  @name    CreateNAVBulkCstmrEditPayLoad
    *  @param   set<Id> sCustomer
    *  @return  String
    *  @throws  NA
    */
    public static String CreateNAVBulkCstmrEditPayLoad(set<Id>sCustomer){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','CreateNAVBulkCstmrEditPayLoad');
        return createNAVBulkCstmrEditPayLoad(sCustomer);
    }

    /**
    *  Part of 17D thread Ship To Edit SFDC-NAV Bulk Payload
       This method creates ship to addresses payload for bulk ship to address edit NAV
    *  @name    createNAVBulkShipToEditPayLoad
    *  @param   set<Id> mShipToIds
    *  @return  String
    *  @throws  NA
    */
    public static String createNAVBulkShipToEditPayLoad(set<Id>mShipToIds){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createNAVBulkShipToEditPayLoad');
        return shipToPayLoad.createNAVBulkShipToEditPayLoad(mShipToIds);
    }
    
     /**
    *  This method creates Payload for Ship to addresses record for LOMOSOFT import
    *  @name    createLomoSoftBulkShiptoPayLoad
    *  @param   set<Id> sShipToIds
    *  @return  string
    *  @throws  NA
    */
    public static string createLomoSoftBulkShiptoPayLoad(set<Id> sShipToIds){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createLomoSoftBulkShiptoPayLoad');
        Map<Id, String> mShipToStatus = new Map<Id, String>();
        for(Id sShipToId : sShipToIds) {
            mShipToStatus.put(sShipToId, EP_Common_Constant.BLANK);
        }
        return shipToPayLoad.createLomoSoftBulkShiptoPayLoad(mShipToStatus, null, false);        
    }  
    /**DEFECT - 30074,INTEGRATIONRECORDCREATION END**/
    
    
    
    /**
    *  This method creates ship to addresses payload for bulk ship to address edit.
       Following changes have been done in this method for fixing  defect# 27961 - 
        1. Change in query to sync tanks and supply location for a new ship to on Active Customer
        2. If Active ship to is updated  then only ship to is synced.   
    *  @name    createLomoSoftBulkShipToEditPayLoad
    *  @param   map<Id,String> mShipToStatus, String fieldSetName
    *  @return  String
    *  @throws  NA
    */
    public static String createLomoSoftBulkShipToEditPayLoad(map<Id,String> mShipToStatus
        ,String fieldSetName){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createLomoSoftBulkShipToEditPayLoad');
        return shipToPayLoad.createLomoSoftBulkShipToPayLoad(mShipToStatus, fieldSetName, true);
    }
    
     /**
    *  METHOD FOR LOMOSOFT TANK EDIT PAYLOAD
    *  @name    createLomosoftBulkTankEdit
    *  @param   map<Id,Id> mUpdateTanksShipTo
    *  @return  String
    *  @throws  NA
    */
    public static String createLomosoftBulkTankEdit(map<Id,Id>mUpdateTanksShipTo){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createLomosoftBulkTankEdit');
        return shipToPayLoad.createLomosoftBulkTankEdit(mUpdateTanksShipTo);
        

    }
    
     /**
    *  This method creates Payload for a given STOCK LOCATION record for LOMOSOFT
    *  @name    createLomoSoftStockLocationPayload
    *  @param   SObject sfObject, List<String>lFieldAPINames
    *  @return  String
    *  @throws  NA
    */
    public static String createLomoSoftStockLocationPayload(SObject sfObject,List<String>lFieldAPINames){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createLomoSoftStockLocationPayload');
        return payloadService.createPayLoad(sfObject, lFieldAPINames, EP_Common_Constant.SUPPLY_LOCATION, null);
    }
    
    /**
    *  This method creates Payload for bulk  Bank Accounts records for NAVISION
    *  @name    createFieldsWrapper
    *  @param   String ObjectAPIName, String fieldSetName
    *  @return  EP_PayloadHelper.WrapperFields
    *  @throws  NA
    */
    public static EP_PayloadHelper.WrapperFields createFieldsWrapper(String ObjectAPIName,String fieldSetName){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createFieldsWrapper');
        return payLoadHelper.createFieldsWrapper(ObjectAPIName, fieldSetName);    
    }
    
    /**
    *  Query custom metadata for creating tags for request. 
    *  @name    createObjectFieldNodeMap
    *  @param   String externalSystemName
    *  @return  void
    *  @throws  NA
    */
    public static void createObjectFieldNodeMap(String externalSystemName){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createObjectFieldNodeMap');
        payLoadHelper.createObjectFieldNodeMap(externalSystemName);
    }
    
    /**
    *  This method craetes groups object and its set of ids
    *  @name    createObjectRecordMap
    *  @param   String objectName, Id recordId
    *  @return  void
    *  @throws  NA
    */
    public static void createObjectRecordMap(String objectName, Id recordId){                                        
        payLoadHelper.populateObjectRecordMap(objectName, recordId);
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createObjectRecordMap');
    }
    
    /**
    *  Returns xml payload structure of customer for Sending request to IPASS  
    *  @name    createCustomerRequest
    *  @param   String msgId, String interfaceName, String srcCompany, String payload, String objectName
    *  @return  String
    *  @throws  NA
    */
    public static String createCustomerRequest(String msgId
       ,String interfaceName
       ,String srcCompany
       ,String payload
       ,String objectName
       ){
        EP_GeneralUtility.Log('Public','EP_PayLoadHandler','createCustomerRequest');
      return customerPayLoad.createCustomerRequest(msgId, interfaceName, srcCompany, payload, objectName);            
  }
    
     /**
      *  CREATE PARENT CHILD MAP
      *  @name    createChildParentMap
      *  @param   String recId, String parentId
      *  @return  void
      *  @throws  NA
      */
      public static void createChildParentMap(String recId,String parentId){
        payLoadHelper.populateChildParentMap(recId, parentId);
    }
}