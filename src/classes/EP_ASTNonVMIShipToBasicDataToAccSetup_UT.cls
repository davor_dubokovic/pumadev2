@isTest
public class EP_ASTNonVMIShipToBasicDataToAccSetup_UT
{
    static final string EVENT_NAME = '02-BasicDataSetupTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '01-ProspectTo05-Active';
    
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        List<EP_Action__c> actions = [SELECT Id,EP_Status__c,EP_Account__c FROM  EP_Action__c WHERE EP_Account__c =: obj.localAccount.Id OR EP_Tank__r.EP_Ship_To__c =: obj.localAccount.Id];
        delete actions;
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    // No longer required as guardian is always returning true
    /*static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObject();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }*/
    
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        List<EP_Action__c> actions = [SELECT Id,EP_Status__c,EP_Account__c FROM  EP_Action__c WHERE EP_Account__c =: obj.localAccount.Id OR EP_Tank__r.EP_Ship_To__c =: obj.localAccount.Id];
        delete actions;
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObject();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void doOnExit_test(){
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.doOnExit();
        // No Processing Logic, hence adding a dummy assert.
        system.Assert(true); 
    }
    
    // #routeSprintWork starts
    // No longer required as guardian is always returning true
    /*static testMethod void isGuardCondition_negative_route_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObject();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTNonVMIShipToBasicDataToAccSetup ast = new EP_ASTNonVMIShipToBasicDataToAccSetup();
        ast.setAccountContext(obj,ae);
        
        Account account = obj.localAccount;
        Map<Id,EP_Route__c> routeRec = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c route = routeRec.values()[0];
        route.EP_Company__c = account.EP_Puma_Company__c;
        update route;
        account.EP_Default_Route__c = route.id;
        delete [Select Id from EP_Action__c];
    
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }*/
    // #routeSprintWork ends
}