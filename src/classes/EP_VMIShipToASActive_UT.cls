@isTest
public class EP_VMIShipToASActive_UT
{

    static final string EVENT_NAME = '05-ActiveTo05-Active';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
        
    static testMethod void setAccountDomainObject_test() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        Test.startTest();   
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    
    static testMethod void doOnEntry_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        Account newAccount = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario().getAccount();
        Account oldAccount = newAccount.clone();
        newAccount.EP_Integration_Status__c = EP_Common_Constant.SYNC_STATUS;
        //Case 1 : If Status Not changing
        EP_AccountDomainObject accDomain = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(accDomain,oe);
        Test.startTest();
        localObj.doOnEntry();
        //Case 2.
        oldAccount.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        EP_AccountDomainObject accDomain2 = new EP_AccountDomainObject(newAccount, oldAccount);
        localObj.setAccountContext(accDomain,oe);
        localObj.doOnEntry();
        Test.stopTest();
        //assert not needed
        system.assert(true);
    }
    static testMethod void doOnExit_test() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        //assert not needed
        system.assert(true);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASActiveDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    
    /* isInboundTransitionPossible does not have negative scenerio because it does not have implementation. It always returns true */
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
 
 // Changes made for CUSTOMER MODIFICATION L4 Start   
    static testMethod void doOnEntryNegative1_test() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        Account newAccount = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario().getAccount();
        EP_CS_OutboundMessageSetting__c customSettingData = new EP_CS_OutboundMessageSetting__c();
        customSettingData.Name = EP_AccountConstant.SFDC_TO_NAV_SHIPTO_PRICING_ENGINE;
        customSettingData.Target_Integration_System__c = 'NAV';
        insert customSettingData;
        EP_AccountDomainObject accDomain = new EP_AccountDomainObject(newAccount.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(accDomain,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assert(!newAccount.EP_Synced_PE__c);
    }

    static testMethod void doOnEntryNegative2_test() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        Account newAccount = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario().getAccount();
        newAccount.EP_Synced_PE__c = true;
        update newAccount;
        EP_CS_OutboundMessageSetting__c customSettingData = new EP_CS_OutboundMessageSetting__c();
        customSettingData.Name = EP_AccountConstant.SFDC_TO_NAV_SHIPTO_EDIT;
        customSettingData.Target_Integration_System__c = 'NAV';
        insert customSettingData;
        EP_AccountDomainObject accDomain = new EP_AccountDomainObject(newAccount.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(accDomain,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assert(!newAccount.EP_Synced_NAV__c);
    }

    static testMethod void doOnEntryNegative3_test() {
        EP_VMIShipToASActive localObj = new EP_VMIShipToASActive();
        Account newAccount = EP_TestDataUtility.getVMIShipToASActiveDomainObjectPositiveScenario().getAccount();
        newAccount.EP_Synced_PE__c = true;
        newAccount.EP_Synced_NAV__c = true;
        update newAccount;
        EP_CS_OutboundMessageSetting__c customSettingData = new EP_CS_OutboundMessageSetting__c();
        customSettingData.Name = EP_AccountConstant.SFDC_TO_WINDMS_SHIPTO_EDIT;
        customSettingData.Target_Integration_System__c = 'WINDMS';
        insert customSettingData;
        EP_AccountDomainObject accDomain = new EP_AccountDomainObject(newAccount.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(accDomain,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        system.assert(!newAccount.EP_Synced_WinDMS__C);
    }
// Changes made for CUSTOMER MODIFICATION L4 End
}