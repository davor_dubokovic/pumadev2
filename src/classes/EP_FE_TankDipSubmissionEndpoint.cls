/* 
*   @Author <Nicola Tassini>
*   @name <EP_FE_TankDipSubmissionEndpoint>
*   @CreateDate <24/05/2016>
*   @Description <Submit a tank dip>  
*   @Version <1.0>
*/
@RestResource(urlMapping = '/FE/V1/tank/submitdip/*')
global with sharing class EP_FE_TankDipSubmissionEndpoint {

    static boolean checkElse = true;

    /*
     *  Default Constructor // Commenting the below constructor to remove the isssue from Nova Suite.
     */
    //public EP_FE_TankDipSubmissionEndpoint() {}

    /*
     * API Call
     */
    @HttpPost
    global static EP_FE_TankDipSubmissionResponse submitTankDip(EP_FE_TankDipSubmissionRequest request) {
        EP_FE_TankDipSubmissionResponse response = new EP_FE_TankDipSubmissionResponse();
        System.debug('>>>'+request);
        if(validateRequestParameters(request, response)) {

            // Collect all the tank Ids
            Set<Id> tankIds = new Set<Id>();
            for(EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem tankDip : request.tankDips) {
                tankIds.add(tankDip.tankId);
            }
            Map<Id, EP_Tank__c> tanks = new Map<Id, EP_Tank__c>([SELECT Id FROM EP_Tank__c WHERE Id IN :tankIds limit 10000]);
            Set<Id> visibleTanks = tanks.keySet();

            // Get the record type
            Id tankDipRecordTypeId = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.ACTUAL).getRecordTypeId();

            // Loop through the request and push the items
            List<EP_Tank_Dip__c> tankDips = new List<EP_Tank_Dip__c>();
            for(EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem tankDip : request.tankDips) {
                if(visibleTanks.contains(tankDip.tankId)) {
                    tankDips.add(new EP_Tank_Dip__c(
                        EP_Tank__c = tankDip.tankId,
                        EP_Ambient_Quantity__c = tankDip.dipLevel,
                        EP_Reading_Date_Time__c = tankDip.measurementTime,
                        EP_Unit_Of_Measure__c = tankDip.unitOfMeasurement,
                        RecordTypeId = tankDipRecordTypeId
                    ));
                }
            }

            System.debug('>>>Dip'+tankDips);
            // Open a transaction
            Savepoint sp = Database.setSavepoint();
            Boolean rollBackB = false;

            try {
                // Some fields have been updated at the sell-to level and the user doesn't necessarely have access to it
                // therefore, execute the insert without sharing
                Database.SaveResult[] srList = EP_FE_DataService.submitTankDips(tankDips);

                Integer i = 0;
                for(Database.SaveResult sr : srList) {
                    if(!sr.isSuccess()) {
                        rollBackB = true;
                        response.status = EP_FE_TankDipSubmissionResponse.ERROR_TANK_DIPS_NOT_SAVED;

                        List<EP_FE_TankDipSubmissionResponse.EP_FE_Field_Error> errors = response.tankDipErrors.get(request.tankDips[i].tankId);
                        if(errors == null) {
                            errors = new List<EP_FE_TankDipSubmissionResponse.EP_FE_Field_Error>();
                        }
                        for(Database.Error err : sr.getErrors()) {
                            errors.add(new EP_FE_TankDipSubmissionResponse.EP_FE_Field_Error(err.getMessage(), err.getFields(), request.tankDips[i].measurementTime));
                        }

                        response.tankDipErrors.put(request.tankDips[i].tankId, errors);
                    }
                    i++;
                }
                EP_FE_Utils.raiseExceptionIfTestIsRunning();
            } catch(Exception e) {
                EP_FE_Utils.logError(EP_FE_TankDipSubmissionResponse.CLASSNAME, EP_FE_TankDipSubmissionResponse.METHOD, 
                    EP_FE_TankDipSubmissionResponse.AREA4, EP_FE_TankDipSubmissionResponse.DESCRIPTION4,
                    EP_FE_TankDipSubmissionResponse.TRANSACTION_ID, e, EP_FE_TankDipSubmissionResponse.ERROR_MISSING_ERROR_SAVING_TANK_DIPS, response);

                response.errors.add(e.getMessage());
                rollBackB = true;
            }

            // Rollback if needed
            if(rollBackB) {
                Database.rollback(sp);
            }

        }

        return response;
    }

    /*
    *  This method validates the request parameters for pricing API
    *  @param EP_FE_TankDipSubmissionRequest request
    *  @param EP_FE_TankDipSubmissionResponse response
    *  @return true, if all the parameters are validated, else returns false
    */
    private static Boolean validateRequestParameters(EP_FE_TankDipSubmissionRequest request, EP_FE_TankDipSubmissionResponse response) {
        if(request == null) {  
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_TankDipSubmissionResponse.CLASSNAME, EP_FE_TankDipSubmissionResponse.METHOD,
                        EP_FE_TankDipSubmissionResponse.SEVERITY, EP_FE_TankDipSubmissionResponse.TRANSACTION_ID, EP_FE_TankDipSubmissionResponse.DESCRIPTION1), 
                        response, EP_FE_TankDipSubmissionResponse.ERROR_MISSING_REQUEST);
                
            return false;
        } 
        else if (request.tankDips == null) {
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_TankDipSubmissionResponse.CLASSNAME, EP_FE_TankDipSubmissionResponse.METHOD,
                        EP_FE_TankDipSubmissionResponse.SEVERITY, EP_FE_TankDipSubmissionResponse.TRANSACTION_ID, EP_FE_TankDipSubmissionResponse.DESCRIPTION2), 
                        response, EP_FE_TankDipSubmissionResponse.ERROR_MISSING_TANKDIPS_REQUEST);

            return false;
        }
        else if (request.tankDips != null) {

            for(EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem tankDip : request.tankDips) {
                if(tankDip.tankId == null || tankDip.dipLevel == null || tankDip.measurementTime == null || tankDip.dipLevel <= 0 || tankDip.unitOfMeasurement == null) {
                    EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_TankDipSubmissionResponse.CLASSNAME, EP_FE_TankDipSubmissionResponse.METHOD,
                        EP_FE_TankDipSubmissionResponse.SEVERITY, EP_FE_TankDipSubmissionResponse.TRANSACTION_ID, EP_FE_TankDipSubmissionResponse.DESCRIPTION3), 
                        response, EP_FE_TankDipSubmissionResponse.ERROR_MISSING_INVALID_TANK_DIPS);
                
                    return false;
                }
            }
        }
        else{
            checkElse = false;
        }

        return true;        
    }

}