/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_StateMachine_Test>
*  @CreateDate <05/02/2017>
*  @Description <Test class to validation the order state transitons>
*  @Version <1.0>
*/

@isTest(seealldata=true)
public class EP_StateMachine_Test{   
   
    public static EP_OrderDomainObject odo;
    public static EP_OrderEvent oe;
    /*
   
   // Positive scenario 
   //For User submit, Draft to Credit issue
    static testMethod void testDraftToDraftWithCreditIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserSubmit');
        //odo.setHasOverdue(true);
        //odo.setIsPrePay(false);
        //odo.setIsPaymentApplied(false);
        //odo.setHasCreditIssues(true);
        odo.setStatus(EP_OrderConstant.OrderState_Draft);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Draft,odo.getStatus());
    }
    
    // Negative scenario
    // User submit - Draft to submitted 
    static testMethod void testDraftToSubmittedWithCreditIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserSubmit');
        //odo.setHasOverdue(false);
        //odo.setIsPrePay(false);
        //odo.setIsPaymentApplied(false);
        //odo.setHasCreditIssues(true);
        odo.setStatus(EP_OrderConstant.OrderState_Draft);
        
        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
		Test.stopTest();                    
            system.assertNotEquals(EP_OrderConstant.OrderState_Awaiting_Credit_Review,odo.getStatus());
    }
    
   // Negative scenario 
   //For User submit, Draft to Payment issue
    static testMethod void testDraftToPaymentWithPaymentIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('DraftToPayment');
        //odo.setHasOverdue(false);
        //odo.setIsPrePay(true);
        //odo.setIsPaymentApplied(false);
        odo.setStatus(EP_OrderConstant.OrderState_Draft);

        Test.startTest();
        	try{
	            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
	            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
	            boolean b = os.doTransition();
        	}catch(Exception e){
        		system.debug(e);
        	}
		Test.stopTest();                    
            system.assertNotEquals(EP_OrderConstant.OrderState_Awaiting_Payment,odo.getStatus());
    }
    
    // User submit - Draft to Inventory
    static testMethod void testDraftToInventoryWithInventoryIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserSubmit');
        //odo.setHasOverdue(false);
		//odo.setHasCreditIssues(false);
        //odo.setIsPrePay(false);
        //odo.setIsPaymentApplied(false);
        //odo.setIsLowInventory(true);
        odo.setStatus(EP_OrderConstant.OrderState_Draft);
        
        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Awaiting_Inventory_Review,odo.getStatus());
    } 
    
    // User submit - Draft to Cancelled
    static testMethod void testDraftToCancelled(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserCancel');
        //odo.setIsPrePay(false);
        //odo.setIsPaymentApplied(false);
        //odo.setIsLowInventory(false);
        odo.setStatus(EP_OrderConstant.OrderState_Draft);
        
        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
		Test.stopTest();                    
            system.assertEquals(EP_OrderConstant.OrderState_Cancelled,odo.getStatus());
        
    } 
    
    
    //Positive scenatio - Draft to Credit Issue
    // User submit - Draft to Cancelled
    static testMethod void testDraftToCreditIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserExceptionAcknowledged');
        //odo.setIsPrePay(false);
        //odo.setIsPaymentApplied(false);
        //odo.setIsLowInventory(false);
        //odo.setHasCreditIssues(true);
        //odo.setHasOverdue(false);
        odo.setStatus(EP_OrderConstant.OrderState_Draft);
        
        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
		Test.stopTest();                    
            system.assertEquals(EP_OrderConstant.OrderState_Awaiting_Credit_Review,odo.getStatus());
    } 
    
    // Positive scenario 
   //For UserExceptionApproved, CrediReview to Inventory review issue
    static testMethod void testCreditReviewExceptionApprovedWithInventoryIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserExceptionApproved');
        odo.setIsPrePay(false);
        odo.setIsPaymentApplied(false);
		odo.setIsLowInventory(true);
		odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setStatus(EP_OrderConstant.OrderState_Awaiting_Credit_Review);
		
        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
		Test.stopTest();                    
            system.assertEquals(EP_OrderConstant.OrderState_Awaiting_Inventory_Review,odo.getStatus());
    }
    
     // Positive scenario 
   //For UserExceptionApproved , CrediReview to Inventory review issue
    static testMethod void testCreditReviewExceptionApprovedWithNoInventoryIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserExceptionApproved');
        odo.setIsPrePay(false);
        odo.setIsPaymentApplied(false);
        odo.setIsLowInventory(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
        odo.setStatus(EP_OrderConstant.OrderState_Awaiting_Credit_Review);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
		Test.stopTest();                    
            system.assertEquals(EP_OrderConstant.OrderState_Submitted,odo.getStatus());
    }
    
      // Positive scenario 
   //For User submit, CrediReview to Inventory review issue
    static testMethod void testCreditReviewExceptionRejected(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserExceptionRejected');
        odo.setIsPrePay(false);
        odo.setIsPaymentApplied(false);
        odo.setIsLowInventory(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(true);
        odo.setStatus(EP_OrderConstant.OrderState_Awaiting_Credit_Review);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
		Test.stopTest();                    
            system.assertEquals(EP_OrderConstant.OrderState_Cancelled,odo.getStatus());
    }
    
       // Positive scenario 
   //For User submit, CrediReview to Inventory review issue
    static testMethod void testCreditReviewWithPaymentIssue(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserExceptionApproved');
        odo.setIsPrePay(true);
        odo.setIsPaymentApplied(false);
        odo.setIsLowInventory(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
        odo.setStatus(EP_OrderConstant.OrderState_Awaiting_Credit_Review);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Submitted,odo.getStatus());
        
    }
    
    //For User Cancel, On CrediReview 
    static testMethod void testOnCreditReviewUserCancel(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserCancel');
        odo.setIsPrePay(false);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setWinDMSStatus(EP_Common_Constant.STATUS_CANCELLED);
        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Cancelled,odo.getStatus());
    }
    
    
    
    //For User Cancel, On InventoryReview 
    static testMethod void testOnInventoryReviewUserCancel(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('UserCancel');
        odo.setIsPrePay(false);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Cancelled,odo.getStatus());
        
    }
    
    //Postive Scenario
    //For In bound integeration, status change from submitted to awaiting payment 
    static testMethod void testOnSubmittedtoawaitingPayment(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('Submitted To Awaiting Payment');
        odo.setIsPrePay(true);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setStatus(EP_OrderConstant.OrderState_Submitted);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Awaiting_Payment,odo.getStatus());
        
    }
    
    //Postive Scenario
    //For In bound integeration, status change from submitted to awaiting payment 
    static testMethod void testOnawaitingPaymenttoAccepted(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('Awaiting Payment To Accepted');
        odo.setIsPrePay(true);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setStatus(EP_OrderConstant.OrderState_Awaiting_Payment);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Accepted,odo.getStatus());
        
    }
    
    //Postive Scenario
    //For In bound integeration, status change from Accepted to Planning 
    static testMethod void testOnAcceptedToPlanning(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('Accepted To Planning');
        odo.setIsPrePay(true);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setStatus(EP_OrderConstant.OrderState_Accepted);
		odo.setWinDMSStatus(EP_Common_Constant.PLANNING_STATUS);
        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Planning,odo.getStatus());
        
    }
    
    //Postive Scenario
    //For In bound integeration, status change from Planning to Accepted 
    static testMethod void testOnPlanningToPlanned(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('Planning To Planned');
        odo.setIsPrePay(true);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setStatus(EP_OrderConstant.OrderState_Planning);
		odo.setWinDMSStatus(EP_OrderConstant.LOCKED_STATUS);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Planned,odo.getStatus());
        
    }
    
    //Postive Scenario
    //For In bound integeration, status change from Planned to Loaded 
    static testMethod void testOnPlannedToLoaded(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('Planned To Loaded');
        odo.setIsPrePay(true);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setStatus(EP_OrderConstant.OrderState_Planned);
		odo.setWinDMSStatus(EP_Common_Constant.loaded);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Loaded,odo.getStatus());
        
    }
    
    //Postive Scenario
    //For In bound integeration, status change from Loaded to Delivered 
    static testMethod void testOnLoadedToDelivered(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('Loaded To Delivered');
        odo.setIsPrePay(true);
        odo.setIsPaymentApplied(false);
        odo.setHasOverdue(false);
		odo.setHasCreditIssues(false);
		odo.setStatus(EP_OrderConstant.OrderState_Loaded);
		odo.setWinDMSStatus(EP_Common_Constant.delivered);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Delivered,odo.getStatus());
        
    }
    
    //Postive Scenario
    //For In bound integeration, status change from Delivered to Invoice 
    static testMethod void testOnDeliveredToInvoiced(){
        odo = new EP_OrderDomainObject('801p00000009jlU');
        oe = new EP_OrderEvent('Delivered To Invoiced');
		odo.setStatus(EP_OrderConstant.OrderState_Delivered);

        Test.startTest();
            EP_OrderTypeStateMachineFactory otsmf = new EP_OrderTypeStateMachineFactory();
            EP_OrderState os = otsmf.getOrderStateMachine(odo).getOrderState(oe);
            boolean b = os.doTransition();
        Test.stopTest();        
            system.assertEquals(EP_OrderConstant.OrderState_Invoiced,odo.getStatus());
        
    }
    
 /*    public static void datasetup(){

      //  odo = new EP_OrderDomainObject('801p00000009jlU');
        
        
        //NonVMI Non Consignment Order
        Order orderDraft = new Order();
        
        //Mandatory fields
        orderDraft.Account = acc;
        orderDraft.status=EP_OrderConstant.OrderState_Draft;
        orderDraft.EffectiveDate = Date.today();
        orderDraft.RecordTypeId = orderNonVMIRecordTypeId;
        orderDraft.EP_ShipTo__c= acc.id;
        orderDraft.EP_Order_Epoch__c = 'current'; 
        orderDraft.EP_ShipTo__r.EP_Ship_To_Type__c = 'Non-Consignment';
        insert orderDraft;
        system.debug(odo.getStatus());
        
    } */
}