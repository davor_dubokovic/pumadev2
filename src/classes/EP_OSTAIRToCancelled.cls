/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTANYToSubmitted>
*  @CreateDate <03/02/2017>
*  @Description <handles order status change from any status to submitted>
*  @Version <1.0>
*/

public class EP_OSTAIRToCancelled extends EP_OrderStateTransition{
    static string classname = 'EP_OSTAIRToCancelled';
    public EP_OSTAIRToCancelled(){
        finalState = EP_OrderConstant.CANCELLED_STATUS;
        //system.debug('In Constructor: EP_OSTANYToSubmitted');
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTAIRToCancelled','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTAIRToCancelled','isRegisteredForEvent');
        //system.debug('In isRegisteredForEvent of EP_OSTANYToSubmitted');
        
        return super.isRegisteredForEvent();      
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTAIRToCancelled','isGuardCondition');
        if( this.order.getOrder().EP_Reason_For_Change__c.equalsIgnoreCase(EP_OrderConstant.INVENTORYREJECTED)){
            return true;
        }
        return false;               
    }
    
}