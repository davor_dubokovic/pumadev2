@isTest
public class EP_ASTSellToBlockToDeactivate_UT
{
    private static testmethod void isGuardCondition_test()
    {    EP_ASTSellToBlockToDeactivate localobj = new EP_ASTSellToBlockToDeactivate();
         Test.startTest();
         boolean result = localobj.isGuardCondition();
         Test.stopTest();
         System.AssertEquals(true,result);
    }
}