/**
    @Author <Brajesh Tiwary>
    @name <EP_TankChangeRequestTest>
    Unit Testing for CR form - Tank and Account
    ATS for Tank Modification using CR.
 */
@isTest
private class EP_TankChangeRequestTest {
    private static Account sellToAccount;
    private static Account shipTo;
    private static EP_Tank__c tank;
    private static EP_Freight_Matrix__c freightMatrix;
    private static EP_Country__c country;
    private static EP_Region__c region;
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    private static string TANKOBJ = 'EP_Tank__c';
    private static EP_Tank__C tankObj1;
    private static Product2 product;
    private static Contact contactRec;
    private static string ID = 'id';
    private static string TYPE = 'type';
    private static string RETURN_TYPE = 'returl';
    private static string OPERATION = 'op';
    private static string UPDATE_OPERATION = 'UPDATE';
    private static string NEW_OPERATION = 'NEW';
    private static string RECORD_TYPE = 'rtype'; 
    private static string RECORD_TYPE_NAME = 'recTypeName';
    
    /* Method is creating test data 
     **/
    private static void loadData(){
        country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country,false);
        
        region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        Database.insert(region,false);
        
        sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        Database.insert(sellToAccount, false);
        contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
        shipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null);
        shipTo.EP_Country__c = country.id;
        Database.insert(shipTo, false);
    }
    /*
        Test Tank CR form Fields Update and CR,CRL,Action created against them
    */
    static testMethod void tankModificationUnitTest() {
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){
        loadData(); 
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
        database.insert(tank);
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(shipTo, false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.BASIC_DATA_SETUP;
        database.update(tank,false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        database.update(tank,false);
        
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(shipTo,false);
        
        
        
        test.startTest();
        //PageReference pageRef = Page.EP_RedirectTankEdit;
        //Test.setCurrentPage(pageRef);
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ID, tank.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'EP_Tank__c');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+tank.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Retail Site Tank');
        EP_CustomerEditRedirectController controller1 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        integer i=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
        fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), i);
        i++;
    }
        test.stopTest();
        //Check Fields on CR form
        system.assert(fieldByIndexMap.containskey('EP_Capacity__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Deadstock__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Safe_Fill_Level__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Status__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Alias__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Reason_Blocked__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Dip_Entry_Mode__c'.toLowerCase()));
        //Change Capacity Value
        controller.allFieldValues[0].ischanged = true;
        controller.allFieldValues[0].crlobj.EP_New_Value__c = '132456';
        controller.submit();
        //Query All CR and CRL created Against Tank Fields update
        EP_ChangeRequest__c cr = [Select id,Name,EP_Request_Status__c,EP_Tank__c,(select id,EP_Change_Request__c,EP_New_Value__c,EP_Action__c,EP_Original_Value__c,EP_Request_Status__c from Change_Request_Lines__r) from EP_ChangeRequest__c WHERE EP_Tank__c =:tank.id];
        //Check The Crl is automatically Approved
        system.assertEquals('Approved',cr.Change_Request_Lines__r[0].EP_Request_Status__c);
        //TRy to change status--User is not allowed to do that
        cr.Change_Request_Lines__r[0].EP_Request_Status__c = 'Rejected';
        Database.SaveResult sv = database.update(cr.Change_Request_Lines__r[0],false);
        system.assert(!sv.isSuccess());
        //Check No action is required for tank fields
        system.assertEquals(Null,cr.Change_Request_Lines__r[0].EP_Action__c);
        //Check for CR status--Status should be completed automatically
        system.assertEquals('Completed',cr.EP_Request_Status__c);
        //Check the old and new value is different - Account Submit 
        system.assertNotEquals(cr.Change_Request_Lines__r[0].EP_New_Value__c,cr.Change_Request_Lines__r[0].EP_Original_Value__c);
        controller1.redirectTank();
        system.assert(controller.request.id != null);
        EP_Tank__c tank1 = [SELECT id,name,EP_Capacity__c FROM EP_Tank__c WHERE id =:tank.id];
        //Check new values are successfully updated
        system.AssertEquals(tank1.EP_Capacity__c,double.valueOf(controller.allFieldValues[0].crlobj.EP_New_Value__c));
    }   
}
    /*
        Check Reason Bloked validation on tank CR form
    */
    static testMethod void tankModificationReasonBlockedBlankTest() {
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){
        loadData(); 
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
        database.insert(tank);
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(shipTo, false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.BASIC_DATA_SETUP;
        database.update(tank,false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        database.update(tank,false);
        
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(shipTo,false);
        
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        test.startTest();
        //Set the current Page
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ID, tank.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'EP_Tank__c');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+tank.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Retail Site Tank');
        EP_CustomerEditRedirectController controller1 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        integer i=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
        fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), i);
        i++;
    }
        //Make Reason Blocked blank when status is stopped
        controller.allFieldValues[3].ischanged = true;
        controller.allFieldValues[3].crlobj.EP_New_Value__c = 'Stopped';
        controller.submit();
        
        test.stopTest();
        system.assert(fieldByIndexMap.containskey('EP_Capacity__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Deadstock__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Safe_Fill_Level__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Status__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Alias__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Reason_Blocked__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Dip_Entry_Mode__c'.toLowerCase()));
        
        //Error message when reason blocked is blank
        system.assert(apexPages.hasmessages());
        for(apexPages.Message msg : apexPages.getMessages()){
            system.assert(msg.getDetail().ContainsIgnoreCase('Please provide a ‘Reason for Blocking’ this tank.'));
        
        }
    }   
}
    
    /*
        Deastock cannot excced safe fill level validation on tank through CR form 
    */
    static testMethod void tankModificationDeadStockBlankTest() {
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){
        loadData(); 
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
        database.insert(tank);
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(shipTo, false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.BASIC_DATA_SETUP;
        database.update(tank,false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        database.update(tank,false);
        
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(shipTo,false);
        
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        test.startTest();
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ID, tank.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'EP_Tank__c');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+tank.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Retail Site Tank');
        EP_CustomerEditRedirectController controller1 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        integer i=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
        fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), i);
        i++;
    }
        //Change DeadStock and Safe fill level values   
        controller.newObjInstance.put(controller.allFieldValues[2].crlobj.EP_Source_Field_API_Name__c,double.valueOf(100));
        controller.newObjInstance.put(controller.allFieldValues[1].crlobj.EP_Source_Field_API_Name__c,double.valueOf(80));
        controller.allFieldValues[1].ischanged = true;
        controller.allFieldValues[2].ischanged = true;
        controller.allFieldValues[2].crlobj.EP_New_Value__c = '100';
        controller.allFieldValues[1].crlobj.EP_New_Value__c = '80';
        controller.submit();
        test.stopTest();
        
        system.assert(fieldByIndexMap.containskey('EP_Capacity__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Deadstock__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Safe_Fill_Level__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Status__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Alias__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Reason_Blocked__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Dip_Entry_Mode__c'.toLowerCase()));
        
        //Error message for deadstock and safe fill level
        system.assert(apexPages.hasmessages());
        for(apexPages.Message msg : apexPages.getMessages()){
            system.assert(msg.getDetail().ContainsIgnoreCase('The tank deadstock cannot exceed the safe fill level'));
        
        }
    }   
}
    /*
        Check page message for decommisioned tank
        Test Case 20769:Customer_HLBR-E2E 002.77_To validate that CSC is able to access the change request form if the account is active or inactive.
    */
    static testMethod void tankModificationDecomisionedTest() {
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){
        loadData(); 
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
        database.insert(tank);
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(shipTo, false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.BASIC_DATA_SETUP;
        database.update(tank,false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        database.update(tank,false);
        
        tank.EP_Tank_Status__c  = EP_Common_Constant.TANK_DECOMISSIONED_STATUS;
        database.update(tank,false);
        
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(shipTo,false);
        
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
        //Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        test.startTest();
        PageReference pageRef = Page.EP_RedirectTankEdit;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(ID, tank.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'EP_Tank__c');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+tank.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Retail Site Tank');
        EP_CustomerEditRedirectController controller1 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        controller1.redirectTank();
    }
        
        test.stopTest();
        //Page error message for decommissioned tank
        system.assert(apexPages.hasmessages());
        for(apexPages.Message msg : apexPages.getMessages()){
            system.debug('12345678'+msg.getDetail());
            system.assert(msg.getDetail().ContainsIgnoreCase('Cannot modify a ‘Decommissioned’ tank.'));
    }   
}
    /*
        Redirect tank on standard page when status is new
    */
    static testMethod void tankModificationStandardPageTest() {
        loadData(); 
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
        database.insert(tank);
        
         test.startTest();
            PageReference pageRef = Page.EP_RedirectTankEdit;
            Test.setCurrentPage(pageRef);

            ApexPages.currentPage().getParameters().put(ID, tank.Id);
            ApexPages.currentPage().getParameters().put(TYPE, 'EP_Tank__c');
            ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+tank.Id);
            ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
            ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'EP_Retail_Site_Tank');
            EP_CustomerEditRedirectController controller = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
         test.stopTest();
            controller.redirectTank();
    }
    /*
        sELL TO ACCOUNT variables fields,fields update,CR,CRL and Action completion
    */
    static testMethod void accountRedirectTestSellToFieldUpdate(){
        
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        system.runAs(cscUser){
        loadData();
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
        test.startTest();
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(sellToAccount,false);
        
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(shipTo,false);
        
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        
        //Set Sell To and VMI ship To page Variables 
        ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'Account');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Sell To');
        EP_CustomerEditRedirectController controller = new EP_CustomerEditRedirectController(new ApexPages.StandardController(sellToAccount));
        EP_ChangeRequestController controller1 = new EP_ChangeRequestController();
        controller.redirectAccount();
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        integer count=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller1.allFieldValues){
            fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count);
            count++;
        }
        controller1.newObjInstance.put(controller1.allFieldValues[0].crlobj.EP_Source_Field_API_Name__c,'Ambient');
        
         
        test.stopTest();
        //check for sell to fields
        system.assert(fieldByIndexMap.containskey('EP_Billing_Basis__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Eligible_for_Rebate__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('Name'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('Website'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Email__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('Fax'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Mobile_Phone__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('Phone'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Language__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Delivery_Type__c'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('EP_Customer_Category__c'.toLowerCase()));
        controller1.allFieldValues[0].ischanged = true;
        
        controller1.allFieldValues[0].crlobj.EP_New_Value__c = 'Ambient';
        controller1.submit();
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);    
        EP_ChangeRequest__c cr = [Select id,Name,EP_Request_Status__c,EP_Account__c,(select id,EP_Change_Request__c,EP_New_Value__c,EP_Action__c,EP_Original_Value__c,EP_Request_Status__c from Change_Request_Lines__r) from EP_ChangeRequest__c WHERE EP_Account__c =:sellToAccount.id];
        EP_Action__c action = [Select Id, OwnerId,Owner.Name,EP_Status__c,recordType.developerName from EP_Action__c where EP_Request_Id__c =: cr.Id];
        
        system.runAs(adminUser){
        action.OwnerId = cscUser.Id;
        }
        //check current user assigned
        action.OwnerId = userinfo.getuserId();
        
        Database.SaveResult sv = database.update(action,false);
        
        //for(EP_Action__c lActions : action){
        //Approve Billing Basis CRL
        cr.Change_Request_Lines__r[0].EP_Request_Status__c = 'Approved';
        Database.update(cr.Change_Request_Lines__r[0],false);
        //check both are approved 
        system.assertEquals('Approved',cr.Change_Request_Lines__r[0].EP_Request_Status__c);
        //Cannot Change status once approved
        cr.Change_Request_Lines__r[0].EP_Request_Status__c = 'Rejected';
        sv = database.update(cr.Change_Request_Lines__r[0],false);
        system.assert(!sv.isSuccess());
        //COmplete the related action
        action.EP_Status__c = '03-Completed';
        database.update(action,false);
        system.assertEquals('03-Completed',action.EP_Status__c);
        
        EP_ChangeRequest__c  changeReq = [Select id,EP_Request_Status__c from EP_ChangeRequest__c WHERE id =: cr.id];
        //Complete change request
        changeReq.EP_Request_Status__c = 'Completed';
        Database.update(changeReq,false);
        EP_ActionTriggerHandler.isExecuteAfterUpdate = false;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;
        //system.assertEquals(Null,cr.Change_Request_Lines__r[0].EP_Action__c);
        system.assertEquals('Completed',changeReq.EP_Request_Status__c);
        //check whether new vlue and old value are different
        system.assertNotEquals(cr.Change_Request_Lines__r[0].EP_New_Value__c,cr.Change_Request_Lines__r[0].EP_Original_Value__c);
        controller.redirectAccount();
        }
    }  
    
    
    /*
        Ship TO ACCOUNT variables fields,fields update,CR,CRL and Action completion
    
    static testMethod void accountRedirectTestShipToFieldUpdate(){
        
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        system.runAs(cscUser){
        loadData();
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        tank = EP_TestDataUtility.createTestEP_Tank(shipTo.id,prod.id);
        test.startTest();
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(sellToAccount,false);
        
        shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(shipTo,false);
        
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        
         Company__c compObj = new Company__c();
            compObj = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
            insert compObj;
            List<Account> accList = new List<Account>();
            Account accObj = EP_TestDataUtility.createHoldingCompany('comapnyIam',EP_Common_Util.fetchRecordTypeId('Account','Holding Company'));
            
            //Adding company code
            accObj.EP_Puma_Company__c = compObj.id;
            Database.insert(accObj);
            accObj.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            DataBase.update(accObj);
            
           
            accList.add(accObj);
            //Test Case 7430 RW_009     
            accObj.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
            DataBase.saveResult updateAcc  = database.update(accObj,false) ;
            //system.debug('==updateAcc===='+updateAcc);
            DataBase.saveResult[] lCustomerSaveResult = new list<DataBase.SaveResult>();
            //Adding bank account to account
            EP_Bank_Account__c bankAccObj = new EP_Bank_Account__c();
            bankAccObj = EP_TestDataUtility.createBankAccount(accObj.id);
            bankAccObj.EP_Bank_Account_Status__c = 'Active';
            // Bank Account attached to Holding Company account & below status is also marked as Active
            Database.insert(bankAccObj);
        
        //Set Sell To and VMI ship To page Variables 
        ApexPages.currentPage().getParameters().put(ID, shipTo.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'Account');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+shipTo.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'VMI Ship To');
       // EP_Bank_Account__c testBank = EP_TestDataUtility.createBankAccount(shipTo.id);
        EP_CustomerEditRedirectController controller1 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(shipTo));
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        controller1.redirectAccount();
      //   controller1.redirectBankAccount();
        Map<string, integer> fieldByIndexMap = new Map<string, integer>();
        integer count=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
            fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count);
            count++;
        }
        controller.newObjInstance.put(controller.allFieldValues[0].crlobj.EP_Source_Field_API_Name__c,'Boston');
                  PageReference pageRef2 = Page.EP_RedirectBankAccountEdit;
        Test.setCurrentPage(pageRef2);
        
                
                
                ApexPages.currentPage().getParameters().put(ID, bankAccObj.Id);
            system.debug('********'+bankAccObj.id);
        //EP_Bank_Account__c testBank = EP_TestDataUtility.createBankAccount(shipTo.id);
        EP_CustomerEditRedirectController controller2 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(bankAccObj));
      //  EP_ChangeRequestController controller3 = new EP_ChangeRequestController();
       // controller2.redirectAccount();
       controller2.redirectBankAccount();
         
        test.stopTest();
        //check for sell to fields
        system.assert(fieldByIndexMap.containskey('Name'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Number_of_Hoses__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Pumps__c'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('ShippingCity'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('EP_Country__c'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('ShippingStreet'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('ShippingState'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('ShippingPostalCode'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('EP_Ship_To_Type__c'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('EP_Ship_To_Opening_Days__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Ship_To_UTC_Timezone__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Tank_Dips_Schedule_Time__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('Phone'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('Fax'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Mobile_Phone__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Email__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('Website'.toLowerCase()));
      //  system.assert(fieldByIndexMap.containskey('EP_Transportation_Management__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Status__c'.toLowerCase()));
        //system.assert(fieldByIndexMap.containskey('EP_Is_Blocked_Required__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Reason_Blocked__c'.toLowerCase()));
        system.assert(fieldByIndexMap.containskey('EP_Eligible_for_Rebate__c'.toLowerCase()));
        //Change Ship To Name
        controller.allFieldValues[0].ischanged = true;
        controller.allFieldValues[0].crlobj.EP_New_Value__c = 'Boston';
        controller.submit();
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);    
        EP_ChangeRequest__c cr = [Select id,Name,EP_Request_Status__c,EP_Account__c,(select id,EP_Change_Request__c,EP_New_Value__c,EP_Action__c,EP_Original_Value__c,EP_Request_Status__c from Change_Request_Lines__r) from EP_ChangeRequest__c WHERE EP_Account__c =:shipTo.id];
        system.assertEquals('Approved',cr.Change_Request_Lines__r[0].EP_Request_Status__c);
        system.assertEquals(Null,cr.Change_Request_Lines__r[0].EP_Action__c);
        system.assertEquals('Completed',cr.EP_Request_Status__c);
        system.assertNotEquals(cr.Change_Request_Lines__r[0].EP_New_Value__c,cr.Change_Request_Lines__r[0].EP_Original_Value__c);
        controller1.redirectAccount();
        system.assert(controller.request.id != null);
        Account acc = [SELECT id,Name FROM Account WHERE id =:shipTo.id];
        system.AssertEquals(acc.Name,controller.allFieldValues[0].crlobj.EP_New_Value__c);
        }
    }
    /*
        Account Standard page redirect when status is not active or Inactive
        
        Test Case 20768:Customer_HLBR-E2E 002.74_To validate that clicking on Edit button before the account is active should not open cr form
    */
    static testMethod void standardPageRedirectTest(){
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        system.runAs(cscUser){
        loadData();
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        
        //Set Sell To and VMI ship To page Variables 
        ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'Account');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Sell To');
        EP_CustomerEditRedirectController controller1 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(shipTo));
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        controller1.redirectAccount();
        controller.submit();
        
        
        ApexPages.currentPage().getParameters().put(ID, shipTo.Id);
        ApexPages.currentPage().getParameters().put(TYPE, 'Account');
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+shipTo.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'VMI Ship To');
        controller1 = new EP_CustomerEditRedirectController(new ApexPages.StandardController(shipTo));
        controller = new EP_ChangeRequestController();
        Pagereference pg = controller1.redirectAccount();
        controller.submit();
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_REJECTED;
        Database.update(sellToAccount,false);
        
        controller1.redirectAccount();
        
        system.assertNotEquals(page.EP_ChangeRequest, pg);
        controller.submit();
        
        }
    }
    
    
}