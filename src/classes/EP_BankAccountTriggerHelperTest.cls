/**
 * @author <Sandeep Kumar>
 * @name <EP_BankAccountTriggerHelperTest>
 * @createDate <06/11/2016>
 * @description
 * @version <1.0>
 */
@
isTest
public class EP_BankAccountTriggerHelperTest {

    /*********************************************************************************************
     *@Description : This method valids ValidateBillTo method of EP_BankAccountTriggerHelper class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void testValidateBillTo() {
        Account billAccount = new Account(Name = 'testAccount', BillingCity = 'test City', recordTypeId = EP_TestDataUtility.BILLTO_RT);
        insert billAccount;
        Test.startTest();
        system.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            billAccount.EP_Status__c = '02-Basic Data Setup';
            billAccount.Phone = '8824417537';
            billAccount.EP_Email__c = 'test@gmail.com';
            billAccount.EP_Billing_Basis__c = 'Standard Loaded';
            billAccount.EP_Is_Valid_Address__c = true;
            update billAccount;

            billAccount.EP_Status__c = '05-Active';
            update billAccount;

            Account acc = new Account(Name = 'testAccount', BillingCity = 'test City', EP_Bill_To_Account__c = billAccount.Id);
            insert acc;

            List < EP_Bank_Account__c > bankAccountList = new List < EP_Bank_Account__c > ();
            bankAccountList.add(new EP_Bank_Account__c(EP_Account__c = acc.Id));
            try {
                insert bankAccountList;
            } catch (Exception ex) {
                system.assertEquals(ex.getMessage().contains(System.Label.EP_BillTo_SellTo_Err_Msg), true);
            }
        }
        Test.stopTest();
    }

    /*********************************************************************************************
     *@Description : This method valids AllowModificationOnBankAccount method of EP_BankAccountTriggerHelper class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void testAllowModificationOnBankAccount() {
        Account acc = new Account(Name = 'testAccount', BillingCity = 'test City');
        insert acc;
        Test.startTest();
        system.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            List < EP_Bank_Account__c > newBankAccList = new List < EP_Bank_Account__c > ();
            newBankAccList.add(new EP_Bank_Account__c(EP_Account__c = acc.Id, EP_Integration_Status__c = EP_Common_Constant.SYNC_STATUS,
                EP_PostCode__c = 'testCode2', EP_Bank_Account_Status__c = EP_Common_Constant.BANK_STATUS_ACTIVE));
            insert newBankAccList;

            List < EP_Bank_Account__c > oldBankAccList = new List < EP_Bank_Account__c > ();
            oldBankAccList.add(new EP_Bank_Account__c(id = newBankAccList[0].Id, EP_Account__c = acc.Id,
                EP_Integration_Status__c = EP_Common_Constant.SENT_STATUS,
                EP_PostCode__c = 'testCode1'));
            Map < Id, EP_Bank_Account__c > oldMap = new Map < Id, EP_Bank_Account__c > (oldBankAccList);

            /*EP_BankAccountTriggerHelper.allowModificationOnBankAccount(newBankAccList,
                oldMap);*/


            //Exception handling case
            newBankAccList = new List < EP_Bank_Account__c > ();
            newBankAccList.add(new EP_Bank_Account__c(EP_Account__c = acc.Id, EP_Integration_Status__c = EP_Common_Constant.SYNC_STATUS,
                EP_PostCode__c = 'testCode2'));
            insert newBankAccList;

            oldBankAccList = new List < EP_Bank_Account__c > ();
            oldBankAccList.add(new EP_Bank_Account__c(EP_Account__c = acc.Id,
                EP_Integration_Status__c = EP_Common_Constant.SENT_STATUS,
                EP_PostCode__c = 'testCode1'));
            insert oldBankAccList;
            oldMap = new Map < Id, EP_Bank_Account__c > (oldBankAccList);

            /*EP_BankAccountTriggerHelper.allowModificationOnBankAccount(newBankAccList,
                oldMap);*/
        }
        Test.stopTest();
    }

    /*********************************************************************************************
     *@Description : This method valids doActiveSyncBankAccount method of EP_BankAccountTriggerHelper class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void testDoActiveSyncBankAcc() {
        Test.startTest();
        system.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            Account acc = new Account(Name = 'testAccount', BillingCity = 'test City');
            insert acc;

            List < EP_Bank_Account__c > bankAccountList = new List < EP_Bank_Account__c > ();
            //will behave as old record
            bankAccountList.add(new EP_Bank_Account__c(EP_Account__c = acc.Id, EP_Integration_Status__c = EP_Common_Constant.SENT_STATUS));

            //will behave as new record
            bankAccountList.add(new EP_Bank_Account__c(EP_Account__c = acc.Id, EP_Integration_Status__c = EP_Common_Constant.SYNC_STATUS));
            insert bankAccountList;
            EP_BankAccountTriggerHelper.doActiveSyncBankAccount(new Map < Id, EP_Bank_Account__c > (new List < EP_Bank_Account__c > {
                    bankAccountList[0]
                }),
                new Map < Id, EP_Bank_Account__c > (new List < EP_Bank_Account__c > {
                    bankAccountList[1]
                }));
        }
        Test.stopTest();
    }

    /*********************************************************************************************
     *@Description : This method valids validateIBAN method of EP_BankAccountTriggerHelper class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void testValidateIBAN() {
        Account acc = new Account(Name = 'testAccount', BillingCity = 'test City');
        insert acc;
        Test.startTest();
        system.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            List < EP_Bank_Account__c > bankAccountList = new List < EP_Bank_Account__c > ();
            bankAccountList.add(new EP_Bank_Account__c(EP_Account__c = acc.Id, EP_IBAN__c = 'testIBAN'));
            try {
                insert bankAccountList;
            } catch (Exception ex) {
                system.assertEquals(ex.getMessage().contains(System.Label.EP_Invalid_IBAN_Err), true);
            }
        }
        Test.stopTest();
    }

}