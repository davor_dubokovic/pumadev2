@isTest
public class EP_OSTVMINonConsignPlannedToCancelled_UT{
    
    public static final String PlannedTocancelled = 'Planned to cancelled';
    
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
    static testMethod void isTransitionPossible_positive_test() {
        EP_OrderDomainObject obj = new EP_OrderDomainObject(EP_TestDataUtility.getSalesOrderWithInvoiceOverdue());
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        EP_OSTVMINonConsignPlannedToCancelled ost = new EP_OSTVMINonConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    // Super class always returns true hence no negative scenario
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        EP_OSTVMINonConsignPlannedToCancelled ost = new EP_OSTVMINonConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    // Super class always returns true hence no negative scenario

    static testMethod void isGuardCondition_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderNegativeTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(PlannedTocancelled);
        EP_OSTVMINonConsignPlannedToCancelled ost = new EP_OSTVMINonConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        system.debug('obj'+obj.getorder().Credit_Check_Reason_Code_XML__c);
        System.AssertEquals(true,result);
    }
    static testMethod void isGuardCondition_negative_test() {
        EP_OrderDomainObject obj = getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
        EP_OSTVMINonConsignPlannedToCancelled ost = new EP_OSTVMINonConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        system.debug('obj'+obj.getorder().Credit_Check_Reason_Code_XML__c);
        System.AssertEquals(false,result);
    }
    public static EP_OrderDomainObject getOrderPositiveTestScenario() {
        EP_OrderDataWrapper odw = new EP_OrderDataWrapper();
        odw.orderType = EP_Common_Constant.SALES_ORDER;    
        odw.vmiType = EP_Common_Constant.VMIORDER;       
        odw.deliveryType = EP_Common_Constant.DELIVERY ;
        odw.productSoldAsType = EP_Common_Constant.BULK_ORDER_PRODUCT_CAT;
        odw.epochType = EP_Common_Constant.EPOC_CURRENT;  
        odw.consignmentType = EP_Common_Constant.NON_CONSIGNMENT;        
        odw.scenarioType = EP_Common_Constant.POSITIVE;
        odw.reasonForChange = EP_OrderConstant.INVENTORYAPPROVED;
        odw.status =  EP_Common_Constant.ORDER_AWAITING_CRED_REVIEW_STATUS ;      
        csord__Order__c orderObj = EP_OrderTestDataUtility_Temp.getOrder(odw);
        EP_OrderDomainObject odo = new EP_OrderDomainObject(orderObj);
        System.assert(true);
        return odo; 
    }
    
}