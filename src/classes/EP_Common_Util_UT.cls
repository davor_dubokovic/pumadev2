@isTest
public class EP_Common_Util_UT
{
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Integration_Status_Update__c>  integStatusUpdateList = Test.loadData(EP_Integration_Status_Update__c.sObjectType,'EP_Integration_StatusUpdateTestData');
    }
    static testMethod void getRecodTypeDevNameFromID_test() {
        String recTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ACTION_OBJ,EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV);
        Test.startTest();
        String result = EP_Common_Util.getRecodTypeDevNameFromID(recTypeID);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV,result);
    }
    static testMethod void checkPortalUser_PositiveScenariotest() {
        Id userId;
        Contact shiptocontact;
        User userWithRole = new User();
        if(UserInfo.getUserRoleId() == null) {
            UserRole usrrole = [SELECT ID FROM UserRole WHERE PortalType = 'None' LIMIT 1];
            userWithRole = EP_TestDataUtility.createUser(UserInfo.getProfileId());
            userWithRole.userroleid = usrrole.id;
        }else {
            userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
        }      
        System.runAs(userWithRole) {
            EP_AccountDomainObject shipToObj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectForPortal();
            Account shipToAccount = shipToObj.localaccount;
            
            shiptocontact = new Contact(LastName=EP_Common_Constant.COMPANY,AccountId= shipToAccount.Id);
            Database.insert(shiptocontact);
        }      
        /* Get any profile for the given type.*/
        Profile portalProfile = [SELECT ID FROM Profile WHERE Name =  'EP_Portal_Super_User' LIMIT 1];         
        String testemail = EP_Common_Constant.EMAILRECIPIENT;
        User portaluser = EP_TestDataUtility.createPortalUser(portalProfile.Id, shiptocontact.Id); 
        Database.insert(portaluser);    
        Test.startTest();
        Boolean result = EP_Common_Util.checkPortalUser(portaluser.Id);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void checkPortalUser_NegativeScenariotest() {
        Id userId = UserInfo.getUserId();
        Test.startTest();
        Boolean result = EP_Common_Util.checkPortalUser(userId);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getRecordTypeIdForGivenSObjectAndName_test() {
        String sobjectType;
        String recTypeDevName;
        Test.startTest();
        Id result = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ACTION_OBJ,EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV);
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }
    static testMethod void getRecordTypeForGivenSObjectAndName_test() {
        String sobjectType;
        String recTypeDevName;
        Test.startTest();
        Id result = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ACTION_OBJ,EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV);
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }
    static testMethod void fetchRecordTypeId_test() {
        String ObjectAPIName;
        String RecordTypeName;
        Test.startTest();
        Id result = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        Test.stopTest();
        System.AssertNotEquals(NULL,result);
    }
    static testMethod void checkIsRecordsDefined_PositiveScenariotest() {
        String ObjectAPIName = EP_Common_Constant.ORDER;
        Test.startTest();
        Boolean result = EP_Common_Util.checkIsRecordsDefined(ObjectAPIName);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void checkIsRecordsDefined_NegativeScenariotest() {
        String ObjectAPIName =EP_Common_Constant.SALES_ORD_STR;
        Test.startTest();
        Boolean result = EP_Common_Util.checkIsRecordsDefined(ObjectAPIName);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void fetchRecordTypeName_test() {
        String ObjectAPIName = EP_Common_Constant.ACTION_OBJ;
        Id recordTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(ObjectAPIName,EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV);
        Test.startTest();
        String result = EP_Common_Util.fetchRecordTypeName(ObjectAPIName,recordTypeId);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.ACT_CSC_REVIEW_RT,result);
    }
    static testMethod void getAssignmentRules_test() {
        String sobjectType = EP_Common_Constant.LEAD_OBJ;
        Test.startTest();
        LIST<AssignmentRule> result = EP_Common_Util.getAssignmentRules(sobjectType);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getFieldsLabels_test() {
        String sobjectType = EP_Common_Constant.ACCOUNT_OBJ;
        Test.startTest();
        Map<String,String> result = EP_Common_Util.getFieldsLabels(sobjectType);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void createIntegrationStatusByObjAPIName_test() {
        Test.startTest();
        Map<String,String> result = EP_Common_Util.createIntegrationStatusByObjAPIName();
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getQueryLimit_test() {
        Test.startTest();
        Integer result = EP_Common_Util.getQueryLimit();
        Test.stopTest();
        System.Assert(result > 0);
    }
    static testMethod void getUserInfo_test() {
        Id UserId;
        Test.startTest();
        User result = EP_Common_Util.getUserInfo(UserInfo.getUserId());
        Test.stopTest();
        System.AssertEquals(UserInfo.getName(),result.name);
    }
    static testMethod void createDataSet_test() {
        String Name = 'TestName';
        String seqId = 'TestSeqid';
        String errorCode = '404';
        String errorDescription = 'TestErrorDescription';
        Test.startTest();
        EP_AcknowledmentGenerator.Cls_dataset result = EP_Common_Util.createDataSet(Name,seqId,errorCode,errorDescription);
        Test.stopTest();
        System.AssertNotEquals(null,result);
    }
    static testMethod void createOption_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBlockedDomainObject();
        String objId = obj.localAccount.Id;
        String value = obj.localAccount.Name;
        Test.startTest();
        SelectOption result = EP_Common_Util.createOption(objId,value);
        Test.stopTest();
        System.Assert(result != NULL);
    }
    static testMethod void createApexMessage_test() {
        String summary = Label.EP_No_SupplyLocation_At_SellTo;
        Test.startTest();
        ApexPages.Message result = EP_Common_Util.createApexMessage(ApexPages.Severity.Info,summary);
        Test.stopTest();
        System.AssertEquals(ApexPages.Severity.Info,result.getSeverity());
    }
    static testMethod void getFieldSetMember_test() {
        String objectName = EP_Common_Constant.TANK_OBJ;
        String fieldSetName = EP_PayLoadConstants.LOMO_TANK_FIELDS;
        Test.startTest();
        LIST<Schema.FieldSetMember> result = EP_Common_Util.getFieldSetMember(objectName,fieldSetName);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
}