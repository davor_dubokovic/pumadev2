public with sharing class EP_OrderTestDataUtility_Temp {
   
   private static csord__Order__c baseOrderObj;
   private static Account sellToAccount;
   private static Account shipToAccount;
   private static List<Product2> productList;
   private static List<PriceBookEntry> priceBookEntryList;
   private static List<EP_Stock_Holding_Location__c> stockLocationsSellToList;
   private static List<EP_Stock_Holding_Location__c> stockLocationsShipToList;
   private static EP_OrderDataWrapper wrapperObj;
   private static List<Account> suppliersList;
       
       
   public static csord__Order__c getOrder(EP_OrderDataWrapper wrapperObject ){
       wrapperObj = wrapperObject;
       
       baseOrderObj = createBaseOrder();
         
       if(wrapperObj.epochType <> Null){
           setOrderEpoch(wrapperObj.epochType); 
       }    
       if(wrapperObj.deliveryType <> Null){
           setOrderDeliveryType();   
       }
       if(wrapperObj.productSoldAsType <> Null){
           setOrderProductType(wrapperObj.productSoldAsType);   
       }
       setPaymentTermAndMethod();
       insert baseOrderObj;
       if(wrapperObj.orderType <> Null){
           if( wrapperObj.orderType.equalsIgnoreCase(EP_Common_Constant.CONSUMPTION_ORDER) ){
               setOrderCategory(wrapperObj.orderType);
               setConsumptionOrderDetails();    
           }else if( wrapperObj.orderType.equalsIgnoreCase(EP_Common_Constant.ORD_CAT_TRANSFER_ORD) ){     
               setTransferOrderDetails();    
           }else{
               setOrderCategory(wrapperObj.orderType); 
               setSalesOrderOrderDetails();      
           }  
       }
       update baseOrderObj;
       system.debug('baseOrderObj>>>>>>>>>>'+baseOrderObj);
       If(wrapperObj.isInvoiceRequired){
            addInvoiceRecord(baseOrderObj);
        }
       
       EP_OrderMapper orderMapper = new EP_OrderMapper();
       csord__Order__c localOrder = orderMapper.getCsRecordById(baseOrderObj.Id);
       system.debug(localOrder); 
       return localOrder;                  
   }   
   private static void addInvoiceRecord(csord__Order__c orderObj){
        
        EP_Invoice__c invoice= new EP_Invoice__c();
        invoice.Name='121';
        invoice.EP_Bill_To__c=orderObj.AccountId__c;
        //invoice.EP_Order__c = orderObj.Id;
        invoice.EP_Invoice_Due_Date__c = Date.today().addDays(-10);
        invoice.EP_Invoice_Issue_Date__c = Date.today().addDays(-20);
        invoice.EP_Remaining_Balance__c   = 1000;
        //database.Insert(invoice);
        
        EP_Invoice_Line_Item__c invoiceItem = new EP_Invoice_Line_Item__c();
        invoiceItem.EP_Amount__c = 1000;
        invoiceItem.EP_Invoice__c = invoice.Id;
        invoiceItem.EP_Invoice_Line_Code__c = '234';
        //invoiceItem.EP_Order__c = orderObj.Id;
        //database.insert(invoiceItem);
   
   }

    private static csord__Order__c createBaseOrder(){
        csord__Order__c orderObj = new csord__Order__c();
        orderObj.Status__c = wrapperObj.status;
        orderObj.EP_WinDMS_Status__c = wrapperObj.status;
        orderObj.EP_Order_Credit_Status__c = EP_Common_Constant.Credit_Okay;
        List<EP_Country_Region_Group_Mapping__mdt> countryRegion = [SELECT EP_Group_Name__c, EP_Region__c FROM EP_Country_Region_Group_Mapping__mdt];
        if(!countryRegion.isEmpty())
            orderObj.EP_Region__c = countryRegion[0].EP_Region__c;
        EP_AccountDataWrapper accountWrapperObj = new EP_AccountDataWrapper();
        accountWrapperObj.accountType = EP_Common_Constant.SELL_TO_TAG;
        accountWrapperObj.billToRequired = wrapperObj.isBillToREquired;
        accountWrapperObj.scenarioType = wrapperObj.scenarioType;
        accountWrapperObj.status = EP_AccountConstant.ACTIVE;
        accountWrapperObj.deliveryType = wrapperObj.deliveryType;
        accountWrapperObj.isPaymentTermRequired = true;
        accountWrapperObj.addSupplyOption = true;
        accountWrapperObj.isPaymentMethodRequired = true;
        accountWrapperObj.restrictActionGeneration = true;
        sellToAccount = EP_AccountTestDataUtility_Temp.getAccount(accountWrapperObj);
        System.debug('********* sellToAccount.EP_Puma_Company_Code__c: ' + sellToAccount.EP_Puma_Company_Code__c);
        orderObj.AccountId__c = sellToAccount.Id;
        orderObj.csord__Account__c = sellToAccount.Id;
        orderObj.PriceBook2Id__c = sellToAccount.EP_Pricebook__c;
        orderObj.EffectiveDate__c = Date.today();
        orderObj.CurrencyISOCode = sellToAccount.CurrencyISOCode;
        orderObj.EP_WinDMS_Status__c = EP_Common_Constant.ORDER_PLANNING_STATUS;
        orderObj.EP_Use_Managed_Transport_Services__c = EP_Common_Constant.STRING_TRUE;
        orderObj.EP_Reason_For_Change__c = wrapperObj.reasonForChange;
        orderObj.Type__c = wrapperObj.consignmentType;
        Account billtoaccount;
        if( wrapperObj.isBillToREquired ){
            billtoaccount = [Select EP_AvailableFunds__c from Account where Id=:sellToAccount.EP_Bill_To_Account__c ];
        }else{
            billtoaccount = sellToAccount;
        }
        orderObj.EP_Bill_To__r = billtoaccount;
        orderObj.EP_Sell_To__r = sellToAccount;
        orderObj.EP_Bill_To__c = billtoaccount.Id;
        orderObj.EP_Sell_To__c = sellToAccount.Id;
        orderObj.EP_Action_for_Non_Delivery__c = EP_Common_Constant.TO_BE_DECIDED;
        system.debug('####'+orderObj.EP_Sell_To__r+'@@@@'+orderObj.EP_Bill_To__r);
        orderObj.csord__Identification__c = createProductBasket().Id;
        return orderObj;
    }

    private static cscfga__Product_Basket__c createProductBasket(){
    
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        
        insert basketInsert;
        return basketInsert;
    }

    private static Order createNegativeBaseOrder(Boolean isBillToREquired){
        Order orderObj = new Order();
        orderObj.status = EP_Common_Constant.ORDER_DRAFT_STATUS;
        EP_AccountDataWrapper accountWrapperObj = new EP_AccountDataWrapper();
        accountWrapperObj.accountType = EP_Common_Constant.SELL_TO_TAG;
        accountWrapperObj.BillToREquired = isBillToREquired;
        accountWrapperObj.scenarioType = EP_Common_Constant.NEGATIVE;
        accountWrapperObj.restrictActionGeneration = true;
        sellToAccount = EP_AccountTestDataUtility_Temp.getAccount(accountWrapperObj);
        orderObj.AccountId = sellToAccount.Id;
        orderObj.EP_Stock_Holding_Location__c = fetchStockLocationsOfSellTo()[0].Id;
        orderObj.PriceBook2Id = sellToAccount.EP_Pricebook__c;
        orderObj.effectivedate = Date.today();
        //orderObj.
        
        return orderObj;
    }
    
   private static void setOrderCategory(String orderCategory){
        baseOrderObj.EP_Order_Category__c = orderCategory;
    }
    
    private static void setOrderEpoch(String orderEpoch){
        baseOrderObj.EP_Order_Epoch__c = orderEpoch;
        if(orderEpoch.equalsIgnoreCase(EP_Common_Constant.EPOC_RETROSPECTIVE)){
            setRetrospectiveDateFields();
        }else{
            setCurrentDateFields();
        }
        
    }
    
    private static void setRetrospectiveDateFields(){
        if(wrapperObj.scenariotype.equalsIgnoreCase(EP_Common_Constant.POSITIVE)){
            baseOrderObj.EP_Order_Date__c = Date.today()-20;
            baseOrderObj.EP_Loading_Date__c = Date.today()-10;
            baseOrderObj.EP_Expected_Delivery_Date__c = Date.today()-2;
            baseOrderObj.EP_Credit_Status__c = EP_Common_Constant.OK;
        }else{
            baseOrderObj.EP_Order_Date__c = Date.today()+10;
            baseOrderObj.EP_Loading_Date__c = Date.today()+20;
            baseOrderObj.EP_Expected_Delivery_Date__c = Date.today()+15;
            baseOrderObj.EP_Credit_Status__c = EP_Common_Constant.CREDIT_FAIL;
        }
    }
    
    private static void setCurrentDateFields(){
        baseOrderObj.EP_Requested_Delivery_Date__c = Date.today()+20;
    }
    
    private static void setOrderDeliveryType(){
        baseOrderObj.EP_Delivery_Type__c = wrapperObj.deliveryType;
    }
    
    private static void setOrderProductType(String orderProductType){
        baseOrderObj.EP_Order_Product_Category__c = orderProductType;
    }
    
    private static void setShipTo(){
        EP_AccountDataWrapper accountWrapperObj = new EP_AccountDataWrapper();
        accountWrapperObj.accountType = EP_Common_Constant.SHIP_TO_TAG;
        accountWrapperObj.vendorType = wrapperObj.vmiType;
        accountWrapperObj.consignmentType = wrapperObj.consignmentType;
        accountWrapperObj.scenarioType = wrapperObj.scenarioType;
        accountWrapperObj.ParentId = sellToAccount.Id;
        accountWrapperObj.addSupplyOption = true;
        accountWrapperObj.isTankRequired = true;
        accountWrapperObj.status = EP_AccountConstant.ACTIVE;
        accountWrapperObj.restrictActionGeneration = true;
        accountWrapperObj.companyId =sellToAccount.EP_Puma_Company__c; 
        shipToAccount = EP_AccountTestDataUtility_Temp.getAccount(accountWrapperObj);
        baseOrderObj.EP_ShipTo__c = shipToAccount.Id;
        baseOrderObj.EP_ShipTo__r = shipToAccount;
    }
    
    
    private static void createConsignmentOrderProduct(){
        fetchSuppliers();
        csord__Order_Line_Item__c orderItemObj = new csord__Order_Line_Item__c();
        orderItemObj.OrderId__c = baseOrderObj.Id;
        orderItemObj.csord__Order__c = baseOrderObj.Id;
        orderItemObj.csord__Identification__c = baseOrderObj.csord__Identification__c;
        List<PriceBookEntry> pricebookEntries = fetchPriceBookEntries();
        //orderItemObj.PriceBookEntryId = pricebookEntries[0].Id;
        orderItemObj.EP_Product__c=pricebookEntries[0].Product2Id;
        orderItemObj.UnitPrice__c = 0;
        orderItemObj.Quantity__c = 1;
        orderItemObj.EP_Is_Standard__c = true;
        orderItemObj.EP_3rd_Party_Stock_Supplier__c = suppliersList[0].Id;
        insert orderItemObj;
    }
    private static void createNonConsignmentOrderProduct(){
        fetchSuppliers();
        csord__Order_Line_Item__c orderItemObj = new csord__Order_Line_Item__c();
        orderItemObj.OrderId__c = baseOrderObj.Id;
        orderItemObj.csord__Order__c = baseOrderObj.Id;
        orderItemObj.csord__Identification__c = baseOrderObj.csord__Identification__c;
        List<PriceBookEntry> pricebookEntries = fetchPriceBookEntries();
        //orderItemObj.PriceBookEntryId = pricebookEntries[0].Id;
        orderItemObj.EP_Product__c=pricebookEntries[0].Product2Id;
        orderItemObj.UnitPrice__c = 10;
        orderItemObj.Quantity__c = 1;
        orderItemObj.EP_Is_Standard__c = true;
        orderItemObj.EP_3rd_Party_Stock_Supplier__c = suppliersList[0].Id;
        System.debug('----- baseOrderObj.csord__Identification__c' + baseOrderObj.csord__Identification__c);
        System.debug('-----' + orderItemObj);
        insert orderItemObj;
    }
    
    private static void fetchProducts(){
        productList = [ Select Id,Name from Product2 order by Name asc];
    }
    
    private static List<PriceBookEntry> fetchPriceBookEntries(){
        
        priceBookEntryList = [ Select Id,Product2Id from PriceBookEntry where PriceBook2Id =: sellToAccount.EP_Pricebook__c AND  CurrencyISOCode =: sellToAccount.CurrencyISOCode ];
        system.debug('====='+sellToAccount+'===='+priceBookEntryList+'&&&&&'+sellToAccount.EP_Pricebook__c+sellToAccount.CurrencyISOCode );
        return priceBookEntryList;
    }
    
    private static void fetchSellTo(){
        sellToAccount = [ Select id from Account where id=: shipToAccount.ParentId];
    }
    
    private static List<EP_Stock_Holding_Location__c> fetchStockLocationsOfSellTo(){
        stockLocationsSellToList = [ Select Id,Stock_Holding_Location__c,Stock_Holding_Location__r.Id,EP_Transporter__c from EP_Stock_Holding_Location__c where EP_Sell_To__c =: sellToAccount.Id ];
        return stockLocationsSellToList;
    }
    
    private static List<EP_Stock_Holding_Location__c> fetchStockLocationsOfShipTo(){
        stockLocationsShipToList = [ Select Id, Stock_Holding_Location__c, Stock_Holding_Location__r.Id,EP_Transporter__c from EP_Stock_Holding_Location__c where EP_Ship_To__c =: shipToAccount.Id ];
         return stockLocationsShipToList;
    }
    
    private static void setShipToSupplyLocation(){
        fetchStockLocationsOfShipTo();
        system.debug('****'+stockLocationsShipToList);
        baseOrderObj.Stock_Holding_Location__c = stockLocationsShipToList[0].Id;
        baseOrderObj.EP_Stock_Holding_Location__c = stockLocationsShipToList[0].Stock_Holding_Location__c;
    }
    
    private static void setSellToSupplyLocation(){
        fetchStockLocationsOfSellTo();
        baseOrderObj.Stock_Holding_Location__c = stockLocationsSellToList[0].Id;
        baseOrderObj.EP_Stock_Holding_Location__c = stockLocationsShipToList[0].Stock_Holding_Location__c;
    }
    
    private static void setSupplyLocationPricing(){
        baseOrderObj.EP_Supply_Location_Pricing__c = baseOrderObj.EP_Stock_Holding_Location__c;
    }
   
    
    private static void setTransporter(){
        baseOrderObj.EP_Transporter__c = stockLocationsShipToList[0].EP_Transporter__c;
        baseOrderObj.EP_Transporter_Pricing__c = stockLocationsShipToList[0].EP_Transporter__c;
    }
    
    
    private static void setConsumptionOrderDetails(){
        setShipTo();
        setShipToSupplyLocation();
        setSupplyLocationPricing();
        createNonConsignmentOrderProduct();
    }
    
    private static void setDummySellTo(){
        EP_AccountDataWrapper accountWrapperObj = new EP_AccountDataWrapper();
        accountWrapperObj.accountType = EP_AccountConstant.DUMMY_SELL_TO;
        //sellToAccount = EP_AccountTestDataUtility_Temp.getAccount(accountWrapperObj);
        baseOrderObj.AccountId__c = sellToAccount.Id; 
    }
    
    private static void setStorageShipTo(){
        EP_AccountDataWrapper accountWrapperObj = new EP_AccountDataWrapper();
        accountWrapperObj.accountType = EP_AccountConstant.STORAGE_SHIP_TO;
        accountWrapperObj.vendorType = EP_Common_Constant.NONVMI;
        accountWrapperObj.consignmentType = EP_Common_Constant.CONSIGNMENT;
        accountWrapperObj.isTankRequired = true;
        accountWrapperObj.isPaymentTermRequired = true;
        accountWrapperObj.addSupplyOption = true;
        accountWrapperObj.scenarioType = wrapperObj.scenarioType;
        accountWrapperObj.status = EP_AccountConstant.ACTIVE;
        accountWrapperObj.restrictActionGeneration = true;
        shipToAccount = EP_AccountTestDataUtility_Temp.getAccount(accountWrapperObj);
        baseOrderObj.EP_ShipTo__r = shipToAccount;
        baseOrderObj.EP_ShipTo__c = shipToAccount.Id;
    }
    
    private static void setTransferOrderDetails(){
        //setDummySellTo();
       setStorageShipTo();
       sellToAccount = new EP_AccountMapper().getAccountRecord(shipToAccount.ParentId);
       
       baseOrderObj.AccountId__c = sellToAccount.Id;
       baseOrderObj.PriceBook2Id__c = sellToAccount.EP_Pricebook__c;
       baseOrderObj.CurrencyISOCode = sellToAccount.CurrencyISOCode;
       //baseOrderObj.EP_Order_Type__c = EP_Common_Constant.ORD_CAT_TRANSFER_ORD;
       setShipToSupplyLocation();
       system.debug('!!!!'+baseOrderObj);
       update baseOrderObj;
        createConsignmentOrderProduct();
        setTransporter();
        setRouteAndRun();
    }
    
    private static void setSalesOrderOrderDetails(){
        if(baseOrderObj.EP_Delivery_Type__c.equalsIgnoreCase(EP_Common_Constant.EX_RACK)){
            setShipTo(); 
            setSupplyLocationPricing();    
        }else{
            setShipTo();
            setShipToSupplyLocation();    
            if(wrapperObj.consignmentType.equalsIgnoreCase(EP_Common_Constant.CONSIGNMENT)){
 
                createConsignmentOrderProduct();
                
            }else{
                createNonConsignmentOrderProduct();
                setShipToSupplyLocation();
                setSupplyLocationPricing();
            }
            if(!wrapperObj.vmiType.equalsIgnoreCase(EP_Common_Constant.VMI)){
                setTransporter();
                setRouteAndRun();
            }
           
        }
    }
    
    private static void setRouteAndRun(){
        EP_TestDataUtility.createTestRouteAllocation(baseOrderObj.EP_Stock_Holding_Location__c,shipToAccount.Id);
    }
    
    private static void createOrderConfigRecord(){
        fetchProducts();
        EP_Order_Configuration__c orderConfiguration = EP_TestDataUtility.createOrderConfigRecord('Test Config','Country Range',10,100,'LT',sellToAccount.EP_Country__c,productList[0].Id );    
        insert orderConfiguration;
    }
    
    private static void fetchSuppliers(){
        
        system.debug('----'+baseOrderObj.Stock_Holding_Location__c+baseOrderObj.EP_Stock_Holding_Location__c);
        List<EP_Inventory__c> invemtoryList = [SELECT id,EP_Storage_Location__c,EP_Supplier__r.id,EP_Supplier__r.EP_Vendor_Type__c,EP_Supplier__r.Name,EP_Supplier__r.RecordType.DeveloperName 
                                            FROM EP_Inventory__c];
       system.debug('----'+invemtoryList);
        suppliersList = new EP_AccountMapper().getSuppliersOfSupplyLocation(baseOrderObj.Stock_Holding_Location__c);
    }
    
    private static void setPaymentTermAndMethod(){
        EP_Payment_Method__c paymentMethodObj = [Select Id from EP_Payment_Method__c where Id=:sellToAccount.EP_Payment_Method__c ];
        EP_Payment_Term__c paymentTermObj = [Select Id from EP_Payment_Term__c where Id=:sellToAccount.EP_Payment_Term_Lookup__c ];
        baseOrderObj.EP_Payment_Method__c = paymentMethodObj.Id;
        baseOrderObj.EP_Payment_Term_Value__c = paymentTermObj.Id;
    }
    
}