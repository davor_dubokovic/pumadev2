@isTest
/* 
  @Author <Vikas Malik>
  @name <EP_Case_Test>
  @CreateDate <05/10/2015>
  @Description <This is the test class for Case >
  @Version <1.0>
*/
private class EP_Case_Test {
    
    private static Case testCase;
    private static Lead testLead;
    private static List<AssignmentRule> assignmentRulesCase = new List<AssignmentRule>() ;
    private static List<AssignmentRule> assignmentRulesLead =new List<AssignmentRule>();
    private static final String FIRSTNAME = 'Test First';    
    private static final String LASTNAME = 'Last Name';
    private static final String EMAIL = 'sample@test.com';
    private static final String PORTAL_CASE_ORIGIN = 'Portal';
    private static Id recordTypeCase;
    
    /*
        Method to create the test data for case
    */
    private static void createTestData(){
    	
        Profile cscAgent = [Select id from Profile
                    Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser){
	        testCase = new Case();
	        assignmentRulesCase = EP_Common_Util.getAssignmentRules('testCase');
	        testCase.Origin = EP_Common_Constant.CASE_ORIGIN_EMAIL;
	        testCase.Status = EP_Common_Constant.CASE_STATUS_NEW_APPLICATION;
	        testCase.Priority = EP_Common_Constant.CASE_PRIORITY_MEDIUM;
	        testCase.Type = 'Portal Support';
	        //testCase.EP_FE_Sub_Type__c = 'Login Issue';                
	        RecordType recType = EP_Common_Util.getRecordTypeForGivenSObjectAndName
	                            ('Case',EP_Common_Constant.NEW_PROSPECTIVE_CUSTOMER_REQUEST_REC_TYPE);
	        if (testCase != NULL) {
	            testCase.RecordTypeId = recType.Id;
	        }
	        Database.insert(testCase);
	        recordTypeCase = EP_Common_Util.fetchRecordTypeName('case',null);
	        testLead = new Lead();
	        assignmentRulesLead = EP_Common_Util.getAssignmentRules('testLead ');
	        testLead.FirstName = FIRSTNAME;
	        testLead.LastName = LASTNAME;
	        testLead.Company = EMAIL;
	        testLead.EP_Bank_Name__c = EP_Common_Constant.LEAD_BANK_NAME;
	        testLead.EP_IBAN__c = EP_Common_Constant.LEAD_IBAN;
	        testLead.Case__c = testCase.Id;
	        testLead.LeadSource = EP_Common_Constant.LEAD_ORIGIN_EMAIL;
	        Database.insert(testLead);
	    }
    }
    
    /*
        This method will test the portal case auto-assignment rule
    */
    static testMethod void testPortalCaseAutoAssignmentRule() {
    	testCase = new Case();
	    testCase.Origin = PORTAL_CASE_ORIGIN;
	    testCase.Priority = EP_Common_Constant.CASE_PRIORITY_MEDIUM;
	    testCase.Type = 'New Order Inquiry';          
	    RecordType recType = EP_Common_Util.getRecordTypeForGivenSObjectAndName('Case', 'EP_Customer_Support_Case');
	    testCase.RecordTypeId = recType.Id;
	    Database.insert(testCase);
    	
    	// The auto-assignment rule must kick-in and allocate the new portal case to the appropriate queue
    	testCase = [SELECT ID, OwnerId FROM Case WHERE ID = :testCase.Id];
    	System.assertNotEquals(UserInfo.getUserId(), testCase.OwnerId);
    	
    }
    
    /*
        This method will test the calculation of the Case Target Resolution Date/Time
    */
    static testMethod void testCaseTargetResolutionDateTimeCalculation() {
    	
    	// Setup test data
    	List<BusinessHours> bhs = [SELECT ID FROM BusinessHours LIMIT 1];
    	// At least 1 Bussiness Hours set must be imported into the system
    	System.assertEquals(1, bhs.size());
    	
    	// Create SLA Map value
    	List<EP_Case_SLA_Map__c> lTestSLAMapRecords = new List<EP_Case_SLA_Map__c>();
    	
    	lTestSLAMapRecords.add(new EP_Case_SLA_Map__c(Name = 'Test1', EP_Business_Hours_ID__c = bhs[0].Id,
    																	EP_Case_Origin__c = EP_Common_Constant.CASE_ORIGIN_EMAIL, 
    																		EP_Case_Record_Type_Developer_Name__c = 'EP_General_Public_Support_Case',
    																			EP_Country_Code__c = 'AU',
    																				EP_Priority__c = 'High',
    																					EP_Target_Resolution_Time_Hours__c = 16,
    																						EP_Type__c = 'New Order Inquiry'));
    																						
    	lTestSLAMapRecords.add(new EP_Case_SLA_Map__c(Name = 'Test2', EP_Business_Hours_ID__c = bhs[0].Id,
    																	EP_Case_Origin__c = EP_Common_Constant.CASE_ORIGIN_EMAIL, 
    																		EP_Case_Record_Type_Developer_Name__c = 'EP_General_Public_Support_Case',
    																			EP_Country_Code__c = 'AU',
    																				EP_Priority__c = EP_Common_Constant.CASE_PRIORITY_MEDIUM,
    																					EP_Target_Resolution_Time_Hours__c = 8,
    																						EP_Type__c = 'Invoice Inquiry'));
    	Database.Insert(lTestSLAMapRecords);
    	
    	// Create RunAs user
    	User runAsAdminUser = EP_TestDataUtility.createTestRecordsForUser();
    	runAsAdminUser.EP_Default_Country_Code__c = 'AU';
    	Database.Update(runAsAdminUser);
    	
    	System.runAs(runAsAdminUser) {
	    	// Create Test Case
			testCase = new Case();
		    testCase.Origin = EP_Common_Constant.CASE_ORIGIN_EMAIL;
		    testCase.Priority = EP_Common_Constant.CASE_PRIORITY_MEDIUM;
		    testCase.Type = 'New Order Inquiry';          
		    RecordType recType = EP_Common_Util.getRecordTypeForGivenSObjectAndName('Case', 'EP_General_Public_Support_Case');
		    testCase.RecordTypeId = recType.Id;
		    Database.insert(testCase);
	    	
	    	// Check that the Case SLA has been calculated and that the Case Priority has been set to 'High'
	    	testCase = [SELECT ID, EP_Target_Resolution_Hours__c, EP_Case_Country_Code__c, Priority FROM Case WHERE ID = :testCase.Id];
	    	
	    	System.assertEquals('High', testCase.Priority);
	    	System.assertEquals(16, testCase.EP_Target_Resolution_Hours__c);
	    	System.assertEquals('AU', testCase.EP_Case_Country_Code__c);
	    	
	    	// Update the Case Type and check that the system has updated the Case SLA
	    	testCase.Type = 'Invoice Inquiry';
	    	Database.update(testCase);
	    	
	    	testCase = [SELECT ID, EP_Target_Resolution_Hours__c, EP_Case_Country_Code__c, Priority FROM Case WHERE ID = :testCase.Id];
	    	
	    	System.assertEquals(EP_Common_Constant.CASE_PRIORITY_MEDIUM, testCase.Priority);
	    	System.assertEquals(8, testCase.EP_Target_Resolution_Hours__c);
	    	System.assertEquals('AU', testCase.EP_Case_Country_Code__c);
	    	
    	}
    }
    
    /*
        This method will check for Case status change
    */
    static testMethod void testCaseStatusChange() {
        createTestData();
        Profile cscAgent = [Select id from Profile
                    Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser)
        {
        testCase.Status = EP_Common_Constant.CASE_STATUS_RDY_KYC_REVIEW;
            try{
                Test.startTest();
                Database.update(testCase);
                Test.stopTest();
            }catch (Exception e){
                System.assert( e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'),e.getMessage());
            }
        }
    }
    
    /*
        This method will test case status change to Under KYC review with CSC user
        User Story- US_001
        Test Script- 12
    */
     static testMethod void testChangeCaseStatus() {
     createTestData();
     Profile cscAgent = [SELECT ID FROM Profile
                    Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        System.runAs(cscUser)
        {
            testCase.Status = EP_Common_Constant.CASE_STATUS_UNDER_KYC_REVIEW;
            try{
                Test.startTest();
            Database.update(testCase);
            Test.stopTest();
            }catch (Exception e){
                System.assert( e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'),e.getMessage());
            }
        }
        System.assertEquals(EP_Common_Constant.CASE_STATUS_UNDER_KYC_REVIEW,testCase.Status);
        }
     }