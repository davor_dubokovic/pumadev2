/*
*  @Author <Accenture>
*  @Name <EP_CompanyCaseConfigurationMapper_UT>
*  @CreateDate <08/01/2018>
*  @Description <Test class for EP_CompanyCaseConfigurationMapper class>
*  @Version <1.0>
*/

@isTest
public class EP_CompanyCaseConfigurationMapper_UT{    

   static testMethod void FetchRecord_test(){
     
        EP_Company_Case_configuration__c testobj = new EP_Company_Case_configuration__c();
        
        Company__c newcompany = EP_TestDataUtility.createCompany('comp_sample');
        insert newcompany ;
        testobj.EP_Company__c = newcompany.id ;
        testobj.EP_Category__c = 'CSC';
        testobj.EP_Sub_Category__c = 'Missing Invoices';
        testobj.EP_Queue__c = 'CSC';
        testobj.EP_SLA_hours__c = 2 ;
        Database.insert(testobj);
               
        Test.startTest();
        EP_Company_Case_configuration__c result = EP_CompanyCaseConfigurationMapper.FetchRecord(newcompany.id,'CSC' , 'Missing Invoices');
        Test.stopTest();
        system.debug('result -->'+result );
        System.assert(result<>Null);
        
    }
}