/* 
  @Author <Nicola Tassini>
   @name <EP_FE_OverallStatusResponse>
   @CreateDate <20/04/2016>
   @Description <  >  
   @Version <1.0>
*/
global with sharing class EP_FE_OverallStatusResponse extends EP_FE_Response {
    public Integer activeOrders {get; set;} // right now
    public Integer invoicedOrders {get; set;} // for last 30 days
    public List<EP_FE_AmountOfFuel> fuelBought {get; set;} // for last 30 days
    public List<EP_FE_AmountOfFuel> lastOrder {get; set;}
    
    public EP_FE_AmountOfPayment activeOrdersAmount {get; set;}
    public EP_FE_AmountOfPayment totalInvoicedOrderValue {get; set;} // for last 30 days
    public EP_FE_AmountOfPayment lastPayment {get; set;}
    public Date lastPaymentTime {get; set;}
   /*********************************************************************************************
    *@Description : This is the Constructor.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
    public EP_FE_OverallStatusResponse (){
        activeOrders = 0;
        invoicedOrders = 0;
        fuelBought = new List<EP_FE_AmountOfFuel>();
        lastOrder = new List<EP_FE_AmountOfFuel>();
    }
   /*********************************************************************************************
    *@Description : This is the Wrapper Class.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
    public with sharing class EP_FE_AmountOfFuel {
        public Double amount {get; set;}
        public String unitOfMeasure {get; set;}
    	
		/*********************************************************************************************
		*@Description : This method is used to get/ set the fuel amount                  
		*@Params      : amount, unit of measure                    
		*@Return      : Void                                                                             
		*********************************************************************************************/
        public EP_FE_AmountOfFuel(Double amount, String unitOfMeasure) {
            this.amount = amount;
            this.unitOfMeasure = unitOfMeasure;
        }
    }   
   /*********************************************************************************************
    *@Description : This is the Wrapper Class.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
    public with sharing class EP_FE_AmountOfPayment {
        public Double amount {get; set;}
        public String currencyOfPayment {get; set;}
   /*********************************************************************************************
    *@Description : This is the Constructor.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
        public EP_FE_AmountOfPayment(Double amount, String currencyOfPayment) {
            this.amount = amount;
            this.currencyOfPayment = currencyOfPayment;
        }
    }   
}