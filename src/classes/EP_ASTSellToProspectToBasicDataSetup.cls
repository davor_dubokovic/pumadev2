/*
 *  @Author <Accenture>
 *  @Name <EP_ASTSellToProspectToBasicDataSetup>
 *  @CreateDate <08/02/2017>
 *  Novasuite Fixes -- comments added
 *  @Description <Handles Account status change from Prospect to Basic Data setup>
 *  @Version <1.0>
 */
 public with sharing class EP_ASTSellToProspectToBasicDataSetup extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTSellToProspectToBasicDataSetup
    *  @return boolean
    */
    public EP_ASTSellToProspectToBasicDataSetup() {
        finalState = EP_AccountConstant.BASICDATASETUP;
    }
    /*
    *  @Author <Accenture>
    *  @Name isTransitionPossible
    *  @return boolean
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToProspectToBasicDataSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /*
    *  @Author <Accenture>
    *  @Name constructor of class EP_ASTVMIShipToAccountSetupToActive
    *  @return boolean
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToProspectToBasicDataSetup','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
	//L4_45352_Start
    /*
    *  @Author <Accenture>
    *  @Name isGuardCondition
    *  @return boolean
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToProspectToBasicDataSetup','isGuardCondition');        
        EP_AccountService service = new EP_AccountService(this.account);
        if(!service.isSupplyOptionAttached()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_One_Stock_Holding_Loc_Reqd);
            return  false;
        }
        if(service.isBankAccountRequired()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.Label.EP_NOActiveBankAcc);
            return false;   
        }  
        if(service.isRecommendedCreditLimitRequired()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Recommended_Credit_Limit_Message);
            return  false;
        }      
        if(EP_AccountConstant.DELIVERY.equalsIgnoreCase(this.account.localAccount.EP_Delivery_Type__c)){
            if(!service.isSetUpShipToAssociated()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(System.Label.EP_No_Set_Up_Ship_To);
                return false;
            }    
        }         
        if(EP_AccountConstant.EXRACK.equalsIgnoreCase(this.account.localAccount.EP_Delivery_Type__c)){
            if(!service.isPickupEnabledSupplyOptionAttached()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(System.Label.EP_PickupEnabledSupplyOption);
                return false;
            }    
        }
        return true;
    }
	//L4_45352_End    
}