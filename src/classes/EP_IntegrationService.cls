/* 
   @Author 			Accenture
   @name 			EP_IntegrationService
   @CreateDate 		03/10/2017
   @Description		Integration Service Class for inbound interfaces
   @Version 		1.0
*/

public with sharing class EP_IntegrationService {
	//Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
    public static string processResponse = '';
    
    /**
	* @author 			Accenture
	* @name				handleRequest
	* @date 			03/13/2017
	* @description 		This method is used to Process the inbound Request(POST) based on message Type
	* @param 			string,string 
	* @return 			string
	*/
    public string handleRequest(string messageType,string requestBody){
        system.debug('**JSON REQUEST BODY**' + requestBody);
        //1: Get Header Details from the JSON String
        Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(requestBody);
        
        //2: Check for Idempotency - Check the message Id in integration Record if already exists then return old Response
        if(EP_IntegrationService.isIdempotent(mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID)) && !EP_Common_Constant.COMMON_ACKNOWLEDGEMENT.equalsIgnoreCase(messageType)) {
        	return EP_IntegrationService.processResponse;
        }
        
        //3: Log Request - Log the JSON String and other details from Header into the integration record for inbound Request        
        EP_IntegrationRecord__c integrationRecord = EP_IntegrationService.logInboundRequest(messageType,mapOfHeaderCommonDetails,requestBody);
        
        //4: Process Request - Get The inbound Request Handler and pass the JSON message to do the business proccessing
        EP_InboundFactory inboundFactory = new EP_InboundFactory();
        EP_InboundHandler inboundHandler = inboundFactory.getInboundHandler(messageType); 
        processResponse = inboundHandler.processRequest(requestBody);
        
        //5: Log the Proccessing Response
        EP_IntegrationService.logProcessResponse(integrationRecord, processResponse);
        
        system.debug('**JSON RESPONSE**' + processResponse);
        return processResponse;          
    }
    
    /**
	* @author 			Accenture
	* @name				isIdempotent
	* @date 			08/29/2017
	* @description 		This method is used to check if the given inbound request is already processed based on message Id
	* @param 			string 
	* @return 			boolean
	*/
    public static boolean isIdempotent(string messageId) {
    	EP_IntegrationRecordMapper intMapper = new EP_IntegrationRecordMapper();
    	list<EP_IntegrationRecord__c> recordList = intMapper.getRecordsByMessageIdOrSeqId(messageId, new set<string>());
    	if(!recordList.isEmpty()) {
    		EP_IntegrationRecord__c intRecord = recordList.get(0);
    		processResponse = intRecord.EP_Process_Result__c;
    		intRecord.EP_Attempt__c = intRecord.EP_Attempt__c==null ? 1 : intRecord.EP_Attempt__c + 1;
    		database.update(intRecord, false);
    		return true;
		} 
    	return  false;
    }
    /**
	* @author 			Accenture
	* @name				logInboundRequest
	* @date 			08/29/2017
	* @description 		Method to log the Inbound Request details into the integration record Object
	* @param 			string,Map<String, String>,string
	* @return 			SObject
	*/
    public static EP_IntegrationRecord__c logInboundRequest(string messageType, Map<String, String> mapOfHeaderCommonDetails, string requestBody) {
    	EP_IntegrationRecord__c integrationRecord = null;
    	
    	//Do not create a seperate entry for Acknowlegment Inbound Request since this request will be log along with its outbound Message Request Record 
    	if(EP_Common_Constant.COMMON_ACKNOWLEDGEMENT.equalsIgnoreCase(messageType) || isInboundMessageLoggerDisable()) {
    		return integrationRecord;
    	}
    	
    	try {
    		integrationRecord = new EP_IntegrationRecord__c();
    		Id devRecordTypeId = Schema.SObjectType.EP_IntegrationRecord__c.getRecordTypeInfosByName().get(EP_Common_Constant.INBOUND_MESSAGE_RT).getRecordTypeId();
    		integrationRecord.RecordTypeId  = devRecordTypeId;
    		integrationRecord.EP_Message_ID__c = mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID);
	    	integrationRecord.EP_Message_Type__c = messageType;
	    	integrationRecord.EP_XML_Message__c = requestBody; 
	    	integrationRecord.EP_DT_RECEIVED__c = system.now();
	    	integrationRecord.EP_Target__c = EP_Common_Constant.TARGET_SF;
	    	integrationRecord.EP_Company__c = mapOfHeaderCommonDetails.get(EP_Common_Constant.ACK_HEADER_SOURCECOMPANY);
	    	string interfaceType = mapOfHeaderCommonDetails.get(EP_Common_Constant.INTERFACE_TYPE);
	    	integrationRecord.EP_Source__c = string.isNotBlank(interfaceType) ? interfaceType : EP_Common_Constant.NA;
    		integrationRecord.EP_Attempt__c = 1;
    	} catch(exception exp) {
    		EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'logInboundRequest', EP_IntegrationService.class.getName(), ApexPages.Severity.ERROR);
    	}
    	return integrationRecord;
    }
    
    /**
	* @author 			Accenture
	* @name				isInboundMessageLoggerDisable
	* @date 			08/31/2017
	* @description 		Method to check Inbound Message logging setting in Communication custom setting
	* @param 			string 
	* @return 			Map<String, String>
	*/
	@TestVisible
    private static boolean isInboundMessageLoggerDisable() {
    	EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_INBOUND_LOG);
    	if(communicationSettings != null && communicationSettings.Disable__c) {
    		return true;
    	}
    	return false;
    }
    /**
	* @author 			Accenture
	* @name				getRequestHeaderDetails
	* @date 			08/29/2017
	* @description 		Method to get Header Details from the inbound Request JSON
	* @param 			string 
	* @return 			Map<String, String>
	*/
    public static Map<String, String> getRequestHeaderDetails(string requestBody) {
    	Map<String, String> mapOfHeaderCommonDetails = new Map<String, String>();
        System.debug('****:-'+requestBody);
    	try {
    		Map<String, Object> mapOfRequestJson = (Map<String, Object>)JSON.deserializeUntyped(requestBody);
	        Map<String, Object> mapOfMsgTagDetails = (Map<String, Object>)mapOfRequestJson.get(EP_Common_Constant.MSG);
	        mapOfHeaderCommonDetails = getHeaderDetails(mapOfMsgTagDetails);
    	} catch(exception exp) {
    		EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'getHeaderDetails', EP_IntegrationService.class.getName(), ApexPages.Severity.ERROR);
    	}
    	
    	return mapOfHeaderCommonDetails;
    }
    
    /**
	* @author 			Accenture
	* @name				getHeaderDetails
	* @date 			08/29/2017
	* @description 		Method to get Header Details from the Request/Response JSON
	* @param 			Map<String, Object> 
	* @return 			Map<String, String>
	*/
	@TestVisible
    private static Map<String, String> getHeaderDetails(Map<String, Object> mapOfHeaderTagDetails) {
    	Map<String, String> mapOfHeaderCommonDetails = new Map<String, String>();
        Map<String, Object> mapOfHeaderCommonTagDetails = (Map<String, Object>) mapOfHeaderTagDetails.get(EP_Common_Constant.HEADER_COMMON);
       	for(String key : mapOfHeaderCommonTagDetails.keyset()) {
            mapOfHeaderCommonDetails.put(key, String.valueof(mapOfHeaderCommonTagDetails.get(key)));
        }
    	return mapOfHeaderCommonDetails;
    }
    
    /**
	* @author 			Accenture
	* @name				logProcessResponse
	* @date 			08/29/2017
	* @description 		Method to log the business processing Response into the integration record Object
	* @param 			SObject, string
	* @return 			NA
	*/
    public static void logProcessResponse(EP_IntegrationRecord__c integrationRecord, string response) {
    	if(integrationRecord == null) {
    		return;
    	}
	    try {
	    	EP_AcknowledgementStub.MSG acknowledgement = ( EP_AcknowledgementStub.MSG) System.JSON.deserialize(response,  EP_AcknowledgementStub.MSG.class);
	    	EP_MessageHeader headerCommon =  acknowledgement.HeaderCommon;
	    	integrationRecord.EP_Process_Result__c =  response;
    		integrationRecord.EP_Process_Status__c  = headerCommon.ProcessStatus;
    		integrationRecord.EP_Status__c = integrationRecord.EP_Process_Status__c;
    		integrationRecord.EP_Error_Description__c = headerCommon.ErrorDescription;
    		integrationRecord.EP_DT_SENT__c = system.now();
    		database.insert(integrationRecord);
    	} catch(exception exp) {
    		EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'logProcessResponse', EP_IntegrationService.class.getName(), ApexPages.Severity.ERROR);
    	}
    }
    
    /**
	* @author 			Accenture
	* @name				handleRequest
	* @date 			03/13/2017
	* @description 		This method is used to Process the inbound Request(GET) based on message Type
	* @param 			string 
	* @return 			string
	*/
    public string handleRequest(string messageType){
        string response='';
        EP_InboundFactory inboundFactory = new EP_InboundFactory();
        EP_InboundHandler inboundHandler = inboundFactory.getInboundHandler(messageType); 
        response = inboundHandler.processRequest();
        system.debug('**JSON RESPONSE**' + response);
        return response;   
    }
}