@isTest
public class EP_NonVMIShipToASActive_UT
{   

    static final string EVENT_NAME = '05-ActiveTo05-Active';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';    
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void setAccountDomainObject_test() {
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
// Changes made for CUSTOMER MODIFICATION L4 Start
    static testMethod void doOnEntry_test() {
        //Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        //Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASActiveDomainObject().getAccount();
        Account oldAccount = newAccount.clone();
        
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);         
        //oldAccount.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        EP_AccountDomainObject accDomain2 = new EP_AccountDomainObject(newAccount, oldAccount);
        localObj.setAccountContext(accDomain2 ,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        Account acc = [Select id,EP_Status__c from Account where recordtype.name = 'Non-VMI Ship To' limit 1];
        system.debug('-------******EP_Common_Constant.SYNC_STATUS'+ acc.EP_Status__c);
        System.AssertEquals(EP_Common_Constant.STATUS_ACTIVE ,newAccount.EP_Status__c);
                       
    }

    static testMethod void doOnEntryNegative1_test() {
        //Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        //Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASActiveDomainObject().getAccount();
        newAccount.EP_Synced_NAV__c = true;
        update newAccount;
        Account oldAccount = newAccount.clone();
        
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);         
        //oldAccount.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        EP_AccountDomainObject accDomain2 = new EP_AccountDomainObject(newAccount, oldAccount);
        localObj.setAccountContext(accDomain2 ,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        Account acc = [Select id,EP_Status__c from Account where recordtype.name = 'Non-VMI Ship To' limit 1];
        system.debug('-------******EP_Common_Constant.SYNC_STATUS'+ acc.EP_Status__c);
        System.AssertEquals(EP_Common_Constant.STATUS_ACTIVE ,newAccount.EP_Status__c);
                       
    }

    static testMethod void doOnEntryNegative2_test() {
        //Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        //Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        Account newAccount = EP_TestDataUtility.getNonVMIShipToASActiveDomainObject().getAccount();
        //newAccount.EP_Synced_PE__c = true;
        newAccount.EP_Synced_WinDMS__C = true;
        update newAccount;
        Account oldAccount = newAccount.clone();
        
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);         
        //oldAccount.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        EP_AccountDomainObject accDomain2 = new EP_AccountDomainObject(newAccount, oldAccount);
        localObj.setAccountContext(accDomain2 ,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        Account acc = [Select id,EP_Status__c from Account where recordtype.name = 'Non-VMI Ship To' limit 1];
        system.debug('-------******EP_Common_Constant.SYNC_STATUS'+ acc.EP_Status__c);
        System.AssertEquals(EP_Common_Constant.STATUS_ACTIVE ,newAccount.EP_Status__c);
                       
    }
  // Changes made for CUSTOMER MODIFICATION L4 End
        
    static testMethod void doOnExit_test() {
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest(); 
        System.Assert(true);
        // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe); 
        string message;   
        Test.startTest();    
        try{
            Boolean result = localObj.doTransition();            
        }
        catch(Exception exp){
            message = exp.getMessage();
        } 
        Test.stopTest();
        System.assert(string.isNotBlank(message));       
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_NonVMIShipToASActive localObj = new EP_NonVMIShipToASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    /** 
        This isInboundTransitionPossible() method is returning only true for all cases.
        So only positive scenario applicable. 
    **/
}