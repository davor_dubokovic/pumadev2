/*
   @Author          CloudSense
   @Name            ProductLineInventoryCheck
   @CreateDate      28/02/2018
   @Description     
   @Version         1
 
*/

global class ProductLineInventoryCheck implements CSPOFA.ExecutionHandler {
    public List<sObject> process(List<SObject> data) {
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        List<CSPOFA__Orchestration_Step__c> extendedList = [SELECT Id, CSPOFA__Orchestration_Process__r.Order__c,
                                                        CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,
                                                        CSPOFA__Status__c, CSPOFA__Completed_Date__c,
                                                        CSPOFA__Message__c
                                                        FROM 
                                                        CSPOFA__Orchestration_Step__c 
                                                        WHERE 
                                                        Id IN :stepList];

        for(CSPOFA__Orchestration_Step__c step : extendedList) {
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
        }

        System.debug('EXTENDED LIST: '+extendedList);

        List<csord__Order__c> orderList = [SELECT id,EP_SeqId__c,EP_AnyProductLineInventory__c from csord__Order__c where id in :orderIdList];
        List<csord__Order_Line_Item__c> lineItemList = [SELECT id,EP_Product__c,EP_Account_Id__c from csord__Order_Line_Item__c where csord__Order__c in :orderIdList];
        List<EP_Inventory__c> accountInvList = [SELECT  EP_Inventory_Availability__c from EP_Inventory__c where EP_Storage_Location__c= :lineItemList[0].EP_Account_Id__c];

        System.debug('ORDER: '+orderList);
            System.debug('LINE ITEM: '+lineItemList);
                System.debug('INV: '+accountInvList);
        for(CSPOFA__Orchestration_Step__c step : extendedList) {

            try {
                for(csord__Order__c currentOrder : orderList) {             
                    for(csord__Order_Line_Item__c currentOrderLineItem:lineItemList) {
                            for (EP_Inventory__c inventory : accountInvList) {
                                if(inventory.EP_Product__c==currentOrderLineItem.EP_Product__c && currentOrderLineItem.csord__Order__c==currentOrder.Id
                                && inventory.EP_Inventory_Availability__c!='Good') {
                                    currentOrder.EP_AnyProductLineInventory__c = inventory.EP_Inventory_Availability__c;
                                    currentOrder.EP_Error_Product_Codes__c = currentOrder.EP_Error_Product_Codes__c + currentOrderLineItem.EP_Product_Code__c +',';
                                }
                            }       
                        }           
                        
                    if(currentOrder.EP_AnyProductLineInventory__c==null) {
                            currentOrder.EP_AnyProductLineInventory__c='Good';
                    }
                    currentOrder.EP_Error_Product_Codes__c = currentOrder.EP_Error_Product_Codes__c.removeEnd(',');
                }
        
            } catch(Exception e){

                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'ProductLineInventoryCheck', apexPages.severity.ERROR);
                System.debug('ERROR: '+e.getMessage());
                step.CSPOFA__Status__c = 'Error';
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = e.getMessage();
                result.add(step);
            }
            System.debug('sTEP: ');
            step.CSPOFA__Status__c = 'Complete';
            step.CSPOFA__Completed_Date__c = Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
            
        }

        update orderList;

        return result;
    }
}