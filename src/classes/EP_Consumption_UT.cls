@isTest
public class EP_Consumption_UT
{
    static testMethod void updateTransportService_test() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c newOrder =  EP_TestDataUtility.getConsumptionOrderDomainObject().getOrder();
        csord__Order__c objOrder = [Select EP_Transporter__c ,EP_Use_Managed_Transport_Services__c,Stock_Holding_Location__c  from csord__Order__c where ID=:newOrder.Id];
        Test.startTest();
        localObj.updateTransportService(objOrder);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.NA ,objOrder.EP_Use_Managed_Transport_Services__c);
    }
    static testMethod void getInventoryDetails_test() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c newOrderRecord = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, PricebookEntryId__c, OrderId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:newOrderRecord.Id];
        Test.startTest();
        Map<id,String> result = localObj.getInventoryDetails(oiList,newOrderRecord);
        Test.stopTest();
        System.AssertEquals(null,result);
    }
    static testMethod void findPriceBookEntries_test() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c objOrder = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        Account accountObj = [SELECT Id,EP_PriceBook__c FROM Account WHERE Id =: objOrder.AccountId__c];
        Id pricebookId = accountObj.EP_PriceBook__c;
        Test.startTest();
        LIST<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,objOrder);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void showCustomerPO_PositiveScenariotest() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c orderObject = [Select csord__Account__r.EP_Is_Customer_Reference_Visible__c from csord__Order__c where Id=:EP_TestDataUtility.getConsumptionOrderPositiveScenario().Id];
        Test.startTest();
        Boolean result = localObj.showCustomerPO(orderObject);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void showCustomerPO_NegativeScenariotest() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c orderObject = [Select csord__Account__r.EP_Is_Customer_Reference_Visible__c from csord__Order__c where Id=:EP_TestDataUtility.getConsumptionOrderNegativeScenario().Id];
        Test.startTest();
        Boolean result = localObj.showCustomerPO(orderObject);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getAllRoutesOfShipToAndLocation_test() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c orderObject = [Select EP_ShipTo__c,EP_Stock_Holding_Location__c from csord__Order__c where Id=:EP_TestDataUtility.getConsumptionOrderPositiveScenario().Id];
        String shipId = orderObject.EP_ShipTo__c;
        String shlId = orderObject.EP_Stock_Holding_Location__c;
        Test.startTest();
        LIST<EP_Route__c> result = localObj.getAllRoutesOfShipToAndLocation(shipId,shlId);
        Test.stopTest();
        System.Assert(result.size()== 0);
    }
    static testMethod void getShipTos_test() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c orderObject = [Select AccountId__c from csord__Order__c where Id=:EP_TestDataUtility.getConsumptionOrderPositiveScenario().Id];
        Id accountId = orderObject.accountId__c;
        Test.startTest();
        LIST<Account> result = localObj.getShipTos(accountId);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getOperationalTanks_test() {
        EP_Consumption localObj = new EP_Consumption();
        csord__Order__c orderObject = [Select EP_ShipTo__c from csord__Order__c where Id=:EP_TestDataUtility.getConsumptionOrderPositiveScenario().Id];
        String strSelectedShipToId = orderObject.EP_ShipTo__c;
        Test.startTest();
        Map<ID, EP_Tank__c> result = localObj.getOperationalTanks(strSelectedShipToId);
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    
}