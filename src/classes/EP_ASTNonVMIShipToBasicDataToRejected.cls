/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToBasicDataToRejected>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 02-Basic Data Setup to 08-Rejected>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToBasicDataToRejected extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToBasicDataToRejected () {
        finalState = EP_AccountConstant.REJECTED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToRejected','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToRejected','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToRejected','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToRejected','doOnExit');

    }
}