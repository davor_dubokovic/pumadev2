/**
   @Author          CR Team
   @name            EP_UserMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to User Object and also the database operational methods
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON

    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_UserMapper {
    
    /**  Constructor.
    *  @name             EP_UserMapper
    *  @param            NA
    *  @return           NA
    *  @throws           NA
    */  
    public EP_UserMapper() {
        
    } 
    

    /**  This method is used to get User Records by Id
    *  @name             getRecordsByIds
    *  @param            set<id>
    *  @return           list<User>
    *  @throws           NA 
    */
    public list<User> getRecordsByIds(set<id> idSet) {
        list<User> userList = new list<User>();
        for(list<User> userRecordList : [ SELECT 
                                             id
                                            , alias
                                            , contactId
                                        FROM User
                                        WHERE Id IN :idSet
                                ]){
            userList.addAll(userRecordList);
                                
        } 
        return userList;
    }
    
    public List<UserRole> getPortalAccountUserRole(Set<Id> setAccountIDs){
    	List<UserRole> accountUserRolesList = new List<Userrole>();
    	accountUserRolesList = [SELECT Id, PortalAccountId FROM UserRole  WHERE PortalAccountId IN :setAccountIDs ];
    	return accountUserRolesList;
    }
    /***L4-45352 start****/
        /****************************************************************
* @author       Accenture                                       *
* @name         UserProfileId                                   *
* @description  fetch user profile id 
                                                               *
* @param                                                      *
* @return       userprofileid                                              *
****************************************************************/ 
    
    Public string getUserProfileName(id userId){
        EP_GeneralUtility.Log('Public','EP_UserMapper','getUserProfileId');
        return [select name from profile where id=:[select profileid from User where id=:userId][0].profileid][0].name;
        
    }  
    /***L4-45352 End****/
}