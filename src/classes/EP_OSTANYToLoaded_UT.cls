@isTest
public class EP_OSTANYToLoaded_UT
{
    static final String event='Planned To Loaded';
    static final String invalidEvent='Loaded To Planned';

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    

static testMethod void isTransitionPossible_positive_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(event);
    EP_OSTANYToLoaded ost = new EP_OSTANYToLoaded();
    csord__Order__c ord = obj.getOrder();
    ord.EP_WinDMS_Status__c = EP_Common_Constant.loaded;
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isTransitionPossible_negative_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderNegativeTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(invalidEvent);
    EP_OSTANYToLoaded ost = new EP_OSTANYToLoaded();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(false,result);
}


static testMethod void isRegisteredForEvent_positive_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(event);
    EP_OSTANYToLoaded ost = new EP_OSTANYToLoaded();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isRegisteredForEvent();
    Test.stopTest();
    System.AssertEquals(true,result);
}

static testMethod void isGuardCondition_positive_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(event);
    EP_OSTANYToLoaded ost = new EP_OSTANYToLoaded();
    csord__Order__c ord = obj.getOrder();
    ord.EP_WinDMS_Status__c = EP_Common_Constant.loaded;
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isGuardCondition_negative_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderNegativeTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(invalidEvent);
    EP_OSTANYToLoaded ost = new EP_OSTANYToLoaded();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(false,result);
}


}