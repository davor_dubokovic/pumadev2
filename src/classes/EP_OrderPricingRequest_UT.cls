@isTest
public class EP_OrderPricingRequest_UT
{
public static final string PRICING_REQUEST_SENT='PRICING REQUEST SENT';
private static final string SERVICE_SLASH_APEXREST  = '/services/apexrest/v1/PricingResponse/';

@testSetup static void init() {
    Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
    Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
    Test.loadData(EP_Pricing_Engine_CS__c.SobjectType,'EP_Pricing_Engine_CS');
    Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_Integration_StatusUpdateTestData');
}

static testMethod void price_req_generator_test() {
	EP_IntegrationUtil.updateVariable = true;
	csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
	Set<ID> ordId = new Set<Id>{orderObj.id};
	String seqId = orderObj.id;
	Test.startTest();
	EP_OrderPricingRequest.price_req_generator(ordId,seqId);
	Test.stopTest();
	orderObj = [Select Id, EP_Pricing_Status__c from csord__Order__c where id = :orderObj.id];
	System.AssertEquals(orderObj.EP_Pricing_Status__c, PRICING_REQUEST_SENT);
}

static testMethod void price_req_generator2_test() {
	EP_IntegrationUtil.updateVariable = true;
	csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
	String seqId = orderObj.id;
	Test.startTest();
	EP_OrderPricingRequest.price_req_generator(orderObj.id);
	Test.stopTest();
	orderObj = [Select Id, EP_Pricing_Status__c from csord__Order__c where id = :orderObj.id];
	System.AssertEquals(orderObj.EP_Pricing_Status__c, PRICING_REQUEST_SENT);
}

static testMethod void pricing_process_test() {
	EP_IntegrationUtil.updateVariable = true;
	csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
	EP_Message_Id_Generator__c msgGenId = new EP_Message_Id_Generator__c();
    insert msgGenId;
	Set<Id> orderIds = new Set<Id>{orderObj.Id};
	String seq = orderObj.id;
	Id msg = msgGenId.id;
	
	Test.startTest();
	EP_OrderPricingRequest.pricing_process(orderIds,seq,msg,EP_Common_Constant.PER);
	Test.stopTest();
	orderObj = [Select Id, EP_Pricing_Status__c from csord__Order__c where id = :orderObj.id];
	System.AssertEquals(orderObj.EP_Pricing_Status__c, PRICING_REQUEST_SENT);
}

static testMethod void updatePricingRecords_test() {
	EP_IntegrationUtil.updateVariable = true;
	csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
	String s = orderObj.id;
	
	Test.startTest();
	EP_OrderPricingRequest.updatePricingRecords(s);
	Test.stopTest();

	//pricing engine record must be created
	List<EP_Pricing_Engine__c> prEngines = [Select Id from EP_Pricing_Engine__c];
	System.AssertEquals(prEngines.size(), 1);
	//order status must be sent
	orderObj = [Select Id, EP_Pricing_Status__c from csord__Order__c where id = :orderObj.id];
	System.AssertEquals(orderObj.EP_Pricing_Status__c, PRICING_REQUEST_SENT);
}

static testMethod void pricingRequestPayload_test() {
	csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
	Set<ID> orderIds = new Set<Id>{orderObj.id};
	
	String sequence = orderObj.id;
	EP_OrderPricingRequest.updatePricingRecords(sequence);
	Test.startTest();
	String result = EP_OrderPricingRequest.pricingRequestPayload(orderIds, sequence);
	Test.stopTest();
	
	system.assert(!String.isBlank(result));
}
//Delegates to other methods. Adding dummy assert
//Need to be reviewed by Order team
static testMethod void PRICINGSync_test() {
	Test.startTest();
	String jsonBody ='{  "name": "1600 Amphitheatre Parkway, Mountain View, CA",  "Status": {    "code": 200,    "request": "geocode"  },  "Placemark": [ {    "id": "p1",    "address": "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA",    "AddressDetails": {   "Accuracy" : 8,   "Country" : {      "AdministrativeArea" : {         "AdministrativeAreaName" : "CA",         "SubAdministrativeArea" : {            "Locality" : {               "LocalityName" : "Mountain View",               "PostalCode" : {                  "PostalCodeNumber" : "94043"               },               "Thoroughfare" : {                  "ThoroughfareName" : "1600 Amphitheatre Pkwy"               }            },         '+
            '   "SubAdministrativeAreaName" : "Santa Clara"         }      },      "CountryName" : "USA",      "CountryNameCode" : "US"   }},    "ExtendedData": {      "LatLonBox": {        "north": 37.4251466,        "south": 37.4188514,        "east": -122.0811574,        "west": -122.0874526      }    },    "Point": {      "coordinates": [ -122.0843700, 37.4217590, 0 ]    }  } ]}   ';

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse(); 
    req.requestURI = SERVICE_SLASH_APEXREST;  //Request URL
    req.httpMethod = 'POST';//HTTP Request Type
     
    req.requestBody = Blob.valueof(jsonBody);
    RestContext.request = req;
    RestContext.response= res; 
    EP_OrderPricingRequest.PRICINGSync();
	Test.stopTest();
	System.assertEquals(true,true);
}
}