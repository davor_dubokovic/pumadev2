/********* Test class for coverage of Order Pricing Trigger and Doc Util*****/

@isTest

public class EP_DocUtil_Test{
    
   private static Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();


   private static Id pricebookId = Test.getStandardPricebookId();

    static testMethod void testDocUtil() {
        string date1 = string.valueof(system.today());
        EP_DocumentUtil.generateCustomerInvoiceItemKey('test1','test2',date1,'test');
        
        EP_DocumentUtil.generateCASItemKey('test1','test2','test3',date1,date1);
        EP_DocumentUtil.generateCASKey ('test',date1,date1);
        set<string> str= new  set<string>();
        str.add('test');
        
        EP_DocumentUtil.generateStatementKey(str);
        EP_IntegrationUtil.updateVariable = true;
        
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        insert sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
            
        Order objOrder = EP_TestDataUtility.createOrder(sellToAccount.Id, orderRecTypeId, pricebookId);
        objOrder.Status = 'Draft';
        insert objOrder;
        EP_OrderPricingRequest.updatePricingRecords(string.valueof(objOrder.id));
        set<id> orderIds = new set<id>{objOrder.id};
        EP_OrderPricingRequest.price_req_generator(orderIds ,'1234');
        
        EP_OrderPricingRequest.price_req_generator(objOrder.id);
        EP_OrderPricingRequest.PRICINGSync();
        set<id> orderIds1 = new set<id>{sellToAccount.id};
        
        list<order> ordlst = new list<order>{objOrder};
        //EP_OrderTriggerHelper.updatePaymentTermAndMethod(ordlst );
        //EP_OrderTriggerHelper.setOrderStatusUpdateBeforeInsert(ordlst ,ordlst );
        string dt= string.valueof(system.now());
        
        //EP_OrderTriggerHelper.setOnRunForVmi(ordlst );
    }
    //L4 - #45419, #45420, #45421 - Start
	private static testMethod void deleteAttachmentsByName_UnitTest() {
		 System.runAs(EP_TestDataUtility.createRunAsUser()) {
	        Account accObj = EP_TestDataUtility.getSellTo();
	  		Attachment attach = EP_AccountNotificationTestDataUtility.createAttachment(accObj.id);
	  		
            test.startTest();
            	EP_DocumentUtil.deleteAttachmentsByName(new list<Attachment>{attach});
            test.stopTest();
            
            Integer attchCount = [SELECT count() FROM Attachment];
            System.assertEquals(0, attchCount);
    	}
    }
    //L4 - #45419, #45420, #45421 - END
}