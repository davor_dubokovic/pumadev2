@isTest
private class EP_GenerateRequestXML_UT {
   
    @testSetup
   public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
   }
    static testmethod void encodeXML_test(){
        dom.Document domDoc = new dom.Document();
        EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
        testobj.isEncryptionEnabled =true;
        Test.startTest();
        string result =testobj.encodeXML(domDoc );
        Test.stopTest();
        system.assert(result<>null, true);
    }
    
    static testmethod void getValueforNodeBoolean_Positivetest()
    {   
        EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
        
        Boolean msg = null;
        Test.startTest();
        String result = testobj.getValueforNode(msg);
        Test.stopTest();
        system.assertEquals(result,'false');
    }
    
    static testmethod void getValueforNodeBoolean_Negativetest()
    {   
        EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
        
        Boolean msg = true;
        Test.startTest();
        String result = testobj.getValueforNode(msg);
        Test.stopTest();
        system.assertEquals(result,'true');
    }
    
    Static testmethod void getValueforNodeString_Positivetest()
    {   EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
    
        String msg = EP_Common_Constant.POSITIVE;
        Test.startTest();
        String result = testobj.getValueforNode(msg);
        Test.stopTest();
        system.assertEquals(result,EP_Common_Constant.POSITIVE);
    }
    
    Static testmethod void getValueforNodeString_Negativetest()
    {   EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
    
        String msg = null;
        Test.startTest();
        String result = testobj.getValueforNode(msg);
        Test.stopTest();
        system.assertEquals(result,EP_Common_Constant.BLANK);
    }

    static testMethod void getValueforNodeDecimal_PositiveTest() 
    {   
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();

       Decimal msg = null;
       Test.startTest();
       String result = testobj.getValueforNode(msg);  
       Test.stopTest();
       System.assertEquals(result, EP_Common_Constant.BLANK);
   }

    static testMethod void getValueforNodeDecimal_NegativeTest() 
    {
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       Decimal msg = 8.5;
       Test.startTest();
       String result = testobj.getValueforNode(msg);  
       Test.stopTest();
       System.assertEquals(result, '8.5');
   }  



    static testMethod void formatDateAsStringDate_Test() { 
       Date inputDate = system.today();
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       Test.startTest();
       String result = testobj.formatDateAsString(inputDate);  
       Test.stopTest();
      System.assertNotEquals(result, EP_Common_Constant.BLANK);
   }

   static testMethod void formatDateAsStringDateTime_Test() {
       DateTime inputDate = system.now();
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       Test.startTest();
       String result = testobj.formatDateAsString(inputDate);  
       Test.stopTest();
       System.assertNotEquals(result, EP_Common_Constant.BLANK);
   }  
   
   static testMethod void transformBooleanValue_PositiveTest() {
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       Test.startTest();
       String result = testobj.transformBooleanValue(true);  
       Test.stopTest();
       System.assertEquals(result, EP_Common_Constant.STRING_YES_LS);
   } 
   
   static testMethod void transformBooleanValue_NegativeTest() {
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       Test.startTest();
       String result = testobj.transformBooleanValue(false);  
       Test.stopTest();
       System.assertEquals(result, EP_Common_Constant.STRING_NO_LS);
   } 
   
   static testMethod void createHeaderNode_Test() {
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       testobj.messageId ='test';
       testobj.messageType =EP_AccountConstant.SFDC_TO_NAV_CUSTOMER_EDIT;
       testobj.companyCode ='test';	   
       Test.startTest();
       testobj.init();
       testobj.createHeaderNode();  
       Test.stopTest();
       System.assertEquals(testobj.MSGNode<> null, true);
   } 
   static testMethod void createXML_Test() {
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       testobj.messageId ='test';
       testobj.messageType =EP_AccountConstant.SFDC_TO_NAV_CUSTOMER_EDIT;
       testobj.companyCode ='test';	   
       Test.startTest();
       testobj.createXML();  
       Test.stopTest();
       System.assertEquals(testobj.MSGNode<> null, true);
   } 
   
   static testMethod void init_Test() { 
       EP_GenerateRequestXML testobj = new EP_GenerateRequestXML();
       Test.startTest();
       testobj.init();  
       Test.stopTest();
       System.assertEquals(testobj.MSGNode<> null, true);
   } 

}