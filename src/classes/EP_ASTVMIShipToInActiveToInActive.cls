/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToInActiveToInActive>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 07-Inactive to 07-Inactive>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToInActiveToInActive extends EP_AccountStateTransition {

    public EP_ASTVMIShipToInActiveToInActive() {
        finalState = EP_AccountConstant.INACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToInActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToInActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToInActiveToInActive','doOnExit');

    }
}