/**
   @Author          Sandeep Kumar
   @name            EP_BankAccountPayLoad
   @CreateDate      01/13/2017
   @Description     This class contains payload methods for bank account object
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_BankAccountPayLoad {
        private EP_PayLoadHelper payLoadHelper = null;
        private EP_PayLoadService payLoadService = null;

        public EP_BankAccountPayLoad() {
            payLoadHelper = new EP_PayLoadHelper();
            payLoadService = new EP_PayLoadService();
        }

    /**
    *  This method creates Payload for bulk  Bank Accounts records for NAVISION
    *  @name    createNavBulkBankAccountPayLoad
    *  @param   bankAccountObject, lFieldAPINames
    *  @return  void
    *  @throws  Generic Exception
    */
    public string createNavBulkBankAccountPayLoad(List<EP_Bank_Account__c> bankAccountObjects, List<String>lFieldAPINames){
        EP_GeneralUtility.Log('Public','EP_BankAccountPayLoad','createNavBulkBankAccountPayLoad');
        String BankAccountsXML = EP_Common_Constant.BLANK;
        try{
         //CREATE BULK BANK ACCOUNTS PAYLOAD FOR CUSTOMER NAV
         for(EP_Bank_Account__c bankAccountObject : bankAccountObjects){
            BankAccountsXML = payLoadService.createPayload(bankAccountObject,
                lFieldAPINames,
                EP_Common_Constant.BANK_ACCOUNT, null);
            payLoadHelper.populateChildParentMap((String)bankAccountObject.ID,(String)bankAccountObject.EP_Account__c);
            
        }
        BankAccountsXML = payLoadService.createPayload(BankAccountsXML, EP_Common_Constant.BANK_ACCOUNTS, NULL);                   
        
    }
    catch(Exception handledException){
     throw handledException;
 }
 return BankAccountsXML;
}   

}