@isTest
public class EP_StorageShipTo_UT{
    
    
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
    }
    
    static testMethod void getSupplyLocationList_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        Id accountId = accountObj.Id;
        Test.startTest();
        LIST<EP_Stock_Holding_Location__c> result = localObj.getSupplyLocationList(accountId);
        Test.stopTest();
        system.assert(result.size()>0);
    }
    
    static testMethod void isParentActive_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        Account parentAccount = new EP_AccountDomainObject(accountObj.ParentId).getAccount();
        parentAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update parentAccount;
        Test.startTest();
        boolean result = localObj.isParentActive(accountObj);
        Test.stopTest();        
        system.assert(result);
    }
    
    static testMethod void isParentActive_test_negative() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectNegativeScenario().getAccount();
        Test.startTest();
        boolean result = localObj.isParentActive(accountObj);
        Test.stopTest();
        system.assertEquals(false,result);
    }   
    
    static testMethod void getTankRecordIds_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        List<account> storageShipToList = accountMapper.getShipToRecordsWithSupplyOptionAndTanks(new Set<Id>{accountObj.Id});
        Test.startTest();
        list<id> result = localObj.getTankRecordIds(storageShipToList);
        Test.stopTest();
        system.assert(result.size()>0);
    }   
    
    static testMethod void getStockHoldingLocationIds_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        EP_AccountMapper accountMapper = new EP_AccountMapper();
        List<account> storageShipToList = accountMapper.getShipToRecordsWithSupplyOptionAndTanks(new Set<Id>{accountObj.Id});
        Test.startTest();
        list<id> result = localObj.getStockHoldingLocationIds(storageShipToList);
        Test.stopTest();
        system.assert(result.size()>0);
    }
    
    static testMethod void setNavWinDmsFlag_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        Test.startTest();
        localObj.setNavWinDmsFlag(accountObj);
        Test.stopTest();
        Account objAccount = [select id,EP_SENT_TO_NAV_WINDMS__c from account where id =: accountObj.Id];
        system.assertEquals(true,objAccount.EP_SENT_TO_NAV_WINDMS__c);
    }
    
    static testMethod void isPrimarySupplyOptionAttached_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        Test.startTest();
        boolean result = localObj.isPrimarySupplyOptionAttached(accountObj);
        Test.stopTest();
        system.assertEquals(true,result);
    }
    
    static testMethod void sendShipToSyncRequestToLOMO_test(){
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        Test.startTest();
        localObj.sendShipToSyncRequestToLOMO(accountObj);
        Test.stopTest();
        List<EP_IntegrationRecord__c> result = [Select id, Name,EP_Object_ID__c, EP_Target__c from EP_IntegrationRecord__c where EP_Object_ID__c =: accountObj.Id ];
        system.assert(result.size()>0);
    }
    
    static testMethod void sendCustomerCreateRequest_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        Account parentAccount = new EP_AccountDomainObject(accountObj.ParentId).getAccount();
        parentAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update parentAccount;
        Test.startTest();
        localObj.sendCustomerCreateRequest(accountObj);
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods, hence adding a dummy assert.
        system.Assert(true);
    }
    
    static testMethod void doAfterInsertHandle_test() {
        EP_StorageShipTo localObj = new EP_StorageShipTo();
        Account accountObj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario().getAccount();
        Test.startTest();
        localObj.doAfterInsertHandle(accountObj);
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods,hence adding a dummy assert.
        system.Assert(true); 
    }
}