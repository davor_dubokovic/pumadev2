@isTest
public class EP_VMIStrategy_UT
{
    static testMethod void doUpdatePaymentTermAndMethod_test() {
        EP_VMIStrategy localObj = new EP_VMIStrategy();
        csord__Order__c orderObj = EP_TestDataUtility.getTransferOrder();
        Test.startTest();
        localObj.doUpdatePaymentTermAndMethod(orderObj);
        Test.stopTest();
        Account orderAccountObj= new EP_AccountMapper().getAccountRecord(orderObj.AccountId__c);
        System.AssertEquals(orderObj.EP_Email__c ,orderAccountObj.EP_Email__c);
    }
    
    static testMethod void setPaymentFields_test() {
        Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        EP_VMIStrategy localObj = new EP_VMIStrategy();
        csord__Order__c orderObj = EP_TestDataUtility.getTransferOrder();
        Account orderAccountObj = new EP_AccountMapper().getAccountRecord(orderObj.AccountId__c);
        Map<String, EP_Customer_Support_Settings__c> localSupportNumbers = EP_Customer_Support_Settings__c.getAll();
        Test.startTest();
        localObj.setPaymentFields(orderObj,orderAccountObj,localSupportNumbers);
        Test.stopTest();
        System.AssertEquals(orderAccountObj.BillingCountry, orderObj.BillingCountry__c);
        System.AssertEquals(orderAccountObj.BillingCity, orderObj.BillingCity__c);
        System.AssertEquals(orderAccountObj.EP_Payment_Term_Lookup__c, orderObj.EP_Payment_Term_Value__c);
        System.AssertEquals(orderAccountObj.EP_Price_Consolidation_Basis__c, orderObj.EP_Price_Consolidation_Basis__c);
    }
    
    static testMethod void doUpdateDeliveryType_test() {
        EP_VMIStrategy localObj = new EP_VMIStrategy();
        csord__Order__c orderObj = EP_TestDataUtility.getTransferOrder();
        Account orderAccountObj= new EP_AccountMapper().getAccountRecord(orderObj.AccountId__c);
        Account vendor = EP_TestDataUtility.getVendor();
        orderObj.EP_Transporter__c = vendor.Id;
        Test.startTest();
        
        localObj.doUpdateDeliveryType(orderObj);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.PIPELINE, orderObj.EP_Delivery_Type__c);
    }
    
    static testMethod void doUpdateVMISuggestion_test() {
        EP_VMIStrategy localObj = new EP_VMIStrategy(); 
        csord__Order__c orderObj = EP_TestDataUtility.getTransferOrder();
        Account orderAccountObj= new EP_AccountMapper().getAccountRecord(orderObj.EP_ShipTo__c);
        Test.startTest();
        orderAccountObj.EP_VMI_Suggestion__c = true;
        update orderAccountObj;
        localObj.doUpdateVMISuggestion(orderObj);
        Test.stopTest();
        System.AssertEquals(true , orderObj.EP_Is_VMI_Suggestion_Order__c);
    }
    
    static testMethod void findRecordType_test() {
        EP_VMIStrategy localObj = new EP_VMIStrategy();
        Test.startTest();
        Id recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();
        Id result = localObj.findRecordType();
        Test.stopTest();
        System.AssertEquals(recordTypeId,result);
    }
}