/* 
    @Author <Jyotsna Tiwari>
    @name <EP_FE_Customer_SupportInfo>
    @CreateDate <18/07/2016>
    @Description <This is API class use to return ChangeRequestId>  
    @Version <1.0>
*/

@RestResource(urlMapping='/FE/V1/changeRequest/submit/*') 
/*********************************************************************************************
     @Author <>
     @name <EP_FE_ChangeRequestEndpoint >
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/
global with sharing class EP_FE_ChangeRequestEndpoint {
    
    @httpPost
/*********************************************************************************************
     @Author <>
     @name <submitChangeRequest>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/    
    global static EP_FE_ChangeRequestSubmissionResponse submitChangeRequest(EP_FE_ChangeRequestSubmissionRequest request){

        EP_FE_ChangeRequestSubmissionResponse response = new EP_FE_ChangeRequestSubmissionResponse(); 
        EP_ChangeRequest__c changeRequest = null;

        if (validateRequestParameters(request, response)) {
            // Check the accuracy of the request

            try{
                changeRequest = sanitize(request.changeRequest);

                Database.insert(changeRequest);
            } catch(Exception ex) {
                EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_ChangeRequestSubmissionResponse.CLASSNAME, EP_FE_ChangeRequestSubmissionResponse.METHOD,
                            EP_FE_ChangeRequestSubmissionResponse.SEVERITY, EP_FE_ChangeRequestSubmissionResponse.TRANSACTION_ID, EP_FE_ChangeRequestSubmissionResponse.DESCRIPTION1), 
                            response,EP_FE_ChangeRequestSubmissionResponse.ERROR_CHANGE_REQUEST_CREATION_FAILED);  
            }
            
            try{
                List<EP_ChangeRequestLine__c> changeLineItemList = new List<EP_ChangeRequestLine__c>();

                for(EP_ChangeRequestLine__c changeReqLineItem : request.changeRequestLineItems){
                    changeReqLineItem.EP_Change_Request__c = changeRequest.Id;
                    changeLineItemList.add(sanitize(changeReqLineItem));
                }

                Database.insert(changeLineItemList);
            } catch(Exception e){
                EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_ChangeRequestSubmissionResponse.CLASSNAME, EP_FE_ChangeRequestSubmissionResponse.METHOD,
                            EP_FE_ChangeRequestSubmissionResponse.SEVERITY, EP_FE_ChangeRequestSubmissionResponse.TRANSACTION_ID, EP_FE_ChangeRequestSubmissionResponse.DESCRIPTION2), 
                            response,EP_FE_ChangeRequestSubmissionResponse.ERROR_CHANGE_REQUEST_LINEITEM_CREATION_FAILED); 
            }
        

            List<EP_ChangeRequest__c> changeRequestList = new List<EP_ChangeRequest__c>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            changeRequestList = [Select Id,EP_Object_Type__c, EP_Record_Type__c, EP_Process_Step__c, EP_Requestor__c, 
		            					 EP_Requestor__r.Profile.Name , EP_Region__c ,EP_Delivery_Country__c ,   
		            					 (select Id, EP_Action__c, EP_Request_Status__c,  EP_Review_Step__c, EP_New_Value__c, 
		            					 	     EP_Change_Request__c, EP_Original_Value__c,  EP_Source_Field_API_Name__c  
		            					    from Change_Request_Lines__r Limit : nRows) 
		            			    From EP_ChangeRequest__c 
                    			   WHERE ID = :changeRequest.Id limit 1];

            //EP_ChangeRequestService.findNextReviewActionforCRGeneric(changeRequestList);

            EP_ChangeRequest__c changeReq = [SELECT Name FROM EP_ChangeRequest__c WHERE Id = :changeRequest.Id LIMIT 1];
            response.changeRequestId =  changeReq.Name;
        }
        return response;
    }

    /*********************************************************************************************
     @Author <>
     @name <validateRequestParameters>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/ 
    private static Boolean validateRequestParameters(EP_FE_ChangeRequestSubmissionRequest request, EP_FE_ChangeRequestSubmissionResponse response){

        if (request == null) {  
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_ChangeRequestSubmissionResponse.CLASSNAME, EP_FE_ChangeRequestSubmissionResponse.METHOD,
                            EP_FE_ChangeRequestSubmissionResponse.SEVERITY, EP_FE_ChangeRequestSubmissionResponse.TRANSACTION_ID, EP_FE_ChangeRequestSubmissionRequest.DESCRIPTION1), 
                            response,EP_FE_ChangeRequestSubmissionRequest.ERROR_REQUEST_CANNOT_BE_NULL);                    
            return false;
        } 
        else if (request.changeRequest == null) {
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_ChangeRequestSubmissionResponse.CLASSNAME, EP_FE_ChangeRequestSubmissionResponse.METHOD,
                            EP_FE_ChangeRequestSubmissionResponse.SEVERITY, EP_FE_ChangeRequestSubmissionResponse.TRANSACTION_ID, EP_FE_ChangeRequestSubmissionRequest.DESCRIPTION2), 
                            response,EP_FE_ChangeRequestSubmissionRequest.ERROR_CHANGE_REQUEST_CANNOT_BE_NULL);
            return false;
        }
        else if (request.changeRequestLineItems == null || request.changeRequestLineItems.size() <= 0) {
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_ChangeRequestSubmissionResponse.CLASSNAME, EP_FE_ChangeRequestSubmissionResponse.METHOD,
                            EP_FE_ChangeRequestSubmissionResponse.SEVERITY, EP_FE_ChangeRequestSubmissionResponse.TRANSACTION_ID, EP_FE_ChangeRequestSubmissionRequest.DESCRIPTION3), 
                            response,EP_FE_ChangeRequestSubmissionRequest.ERROR_CHANGE_REQUEST_LINEITEM_CANNOT_BE_NULL);
            return false;
        }
        else
        {
            return true;    
        }
    }

    /*********************************************************************************************
     @Author <>
     @name <sanitize the change request>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/ 
    private static EP_ChangeRequest__c sanitize(EP_ChangeRequest__c changeRequest) {
        if(changeRequest != null) {
            changeRequest.EP_Delivery_Country__c = EP_FE_Utils.getInputValue(changeRequest.EP_Delivery_Country__c);
            changeRequest.EP_Object_ID__c = EP_FE_Utils.getInputValue(changeRequest.EP_Object_ID__c);
            changeRequest.EP_Object_Type__c = EP_FE_Utils.getInputValue(changeRequest.EP_Object_Type__c);
            changeRequest.EP_Operation_Type__c = EP_FE_Utils.getInputValue(changeRequest.EP_Operation_Type__c);
            changeRequest.EP_Process_Step__c = EP_FE_Utils.getInputValue(changeRequest.EP_Process_Step__c);
            changeRequest.EP_Reason__c = EP_FE_Utils.getInputValue(changeRequest.EP_Reason__c);
            changeRequest.EP_Record_Type__c = EP_FE_Utils.getInputValue(changeRequest.EP_Record_Type__c);
            changeRequest.EP_Region__c = EP_FE_Utils.getInputValue(changeRequest.EP_Region__c);
            changeRequest.EP_Request_Status__c = EP_FE_Utils.getInputValue(changeRequest.EP_Request_Status__c);
        }

        return changeRequest;
    }

    /*********************************************************************************************
     @Author <>
     @name <sanitize the change request>
     @CreateDate <>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/ 
    private static EP_ChangeRequestLine__c sanitize(EP_ChangeRequestLine__c changeRequest) {
        if(changeRequest != null) {
            changeRequest.EP_City__c = EP_FE_Utils.getInputValue(changeRequest.EP_City__c);
            changeRequest.EP_Country__c = EP_FE_Utils.getInputValue(changeRequest.EP_Country__c);
            changeRequest.EP_Reference_Value__c = EP_FE_Utils.getInputValue(changeRequest.EP_Reference_Value__c);
            changeRequest.EP_New_Value__c = EP_FE_Utils.getInputValue(changeRequest.EP_New_Value__c);
            changeRequest.EP_Original_Reference_Value__c = EP_FE_Utils.getInputValue(changeRequest.EP_Original_Reference_Value__c);
            changeRequest.EP_Original_Value__c = EP_FE_Utils.getInputValue(changeRequest.EP_Original_Value__c);
            changeRequest.EP_PostalCode__c = EP_FE_Utils.getInputValue(changeRequest.EP_PostalCode__c);
            changeRequest.EP_Reason_For_Rejection__c = EP_FE_Utils.getInputValue(changeRequest.EP_Reason_For_Rejection__c);
            changeRequest.EP_Request_Status__c = EP_FE_Utils.getInputValue(changeRequest.EP_Request_Status__c);
            changeRequest.EP_Request_Type__c = EP_FE_Utils.getInputValue(changeRequest.EP_Request_Type__c);
            changeRequest.EP_Review_Step__c = EP_FE_Utils.getInputValue(changeRequest.EP_Review_Step__c);
            changeRequest.EP_Source_Field_API_Name__c = EP_FE_Utils.getInputValue(changeRequest.EP_Source_Field_API_Name__c);
            changeRequest.EP_Source_Field_Label__c = EP_FE_Utils.getInputValue(changeRequest.EP_Source_Field_Label__c);
            changeRequest.EP_State__c = EP_FE_Utils.getInputValue(changeRequest.EP_State__c);
            changeRequest.EP_Street__c = EP_FE_Utils.getInputValue(changeRequest.EP_Street__c);
        }

        return changeRequest;
    }

}