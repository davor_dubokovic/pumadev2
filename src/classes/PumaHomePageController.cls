public with sharing class PumaHomePageController {

    private static final String ATTRIBUTE_SHIPPING_ACCOUNT_ID = 'Delivery';
    private static final String ATTRIBUTE_QUANTITY = 'Quantity';
    private static final String ATTRIBUTE_LIST_PRICE = 'List Price';
    private static final String ATTRIBUTE_PRICE = 'Price';
    private static final String ATTRIBUTE_TAX = 'VAT';
    private static final String ATTRIBUTE_PAYMENT_TERM = 'Customer Payment Term';
    private static final String ATTRIBUTE_PRODUCT = 'Product';
    private static final String ATTRIBUTE_PAYMENT_TERM_OVERRIDE = 'ptOverride';
    private static final String ATTRIBUTE_SHOULD_OVERRIDE_PAYMENT_TERM = 'Override Payment Term';

    private static final String ORDER_TYPE_PUMA_ENERGY_ORDER = system.label.Puma_Energy_Order;
    private static final String ORDER_TYPE_PUMA_FUEL = system.label.Puma_Energy_Order_Line;

    private static final String ORDER_STATUS_FINISHED = 'Delivered';

    private static final Map<String, String> ORDER_STATUS_TO_STEP_NAME_MAPPING = new Map<String, String> {
        null => 'Ordered',
        'Order Submitted' => 'Ordered',
        'Accepted' => 'Ordered',
        'Planning' => 'Ordered',
        'Planned' => 'Scheduled',
        'Loaded' => 'Loaded',
        'Delivered' => 'Delivered',
        'Invoiced' => 'Invoiced'
    };

    private static final Map<String, Integer> ORDER_STEP_TO_STEP_NUMBER_MAPPING = new Map<String, Integer> {
        'Ordered' => 1,
        'Scheduled' => 2,
        'Loaded' => 3,
        'Delivered' => 4,
        'Invoiced' => 5
    };

    // Initialise
    public Account account { get; private set; }
    public Contact contact { get; private set; }
    public String userName { get; private set; }
    public List<PumaEnergyOrder> energyOrders { get; private set; }
    public Integer numberOfOrders {
        get {
            return this.energyOrders.size();
        }
    }

    // Constructor
    public PumaHomePageController(ApexPages.StandardController controller) {
        // Get the user context for display
        Id userId = UserInfo.getUserId();
        User user = [select id, firstName, lastName, contactid from User where id = :userId];

        this.userName = user.firstName + ' ' + user.lastName;

        // Get the account context for creating basket etc...
        this.contact = [select id, name, accountId from Contact where id = :user.contactId];
        this.account = [select id, name from Account where id = :contact.accountId];

        this.energyOrders = buildHierarchy(
            readAccountRelatedData(
                readAttributes(
                    buildOrderObjects(this.account.id)
                )
            )
        );
    }

    // for testing
    public PumaHomePageController(Id accountId) {
        this.energyOrders = buildHierarchy(
            readAccountRelatedData(
                readAttributes(
                    buildOrderObjects(accountId)
                )
            )
        );
    }

    // Launch Configurator
    public PageReference launchConfigurator() {
        PageReference orderPage = Page.PumaOrderPage;
        Map<String, String> orderPageParams = orderPage.getParameters();
       // orderPageParams.put('id', account.id);
       	orderPageParams.put('id','0010k000003PKy9');
        orderPage.setRedirect(true);
        return orderPage;
    }

    private List<PumaEnergyOrder> buildHierarchy(List<Order> orders) {
        Map<Id, PumaEnergyOrder> energyOrdersById = new Map<Id, PumaEnergyOrder>();
        List<PumaEnergyOrder> energyOrders = new List<PumaEnergyOrder>();
        List<FuelOrder> fuelOrders = new List<FuelOrder>();
        for (Order o : orders) {
            if (o instanceof PumaEnergyOrder) {
                energyOrdersById.put(o.orderId, (PumaEnergyOrder) o);
                energyOrders.add((PumaEnergyOrder) o);
            } else if (o instanceof FuelOrder) {
                fuelOrders.add((FuelOrder) o);
            }
            // else throw exception i guess... but this is demo
        }

        for (FuelOrder fo : fuelOrders) {
            energyOrdersById.get(fo.parentOrderId).addFuelOrder(fo);
        }

        return energyOrders;
    }

    private static List<Order> readAccountRelatedData(List<Order> orders) {
        Set<Id> accountIdsToFetch = new Set<Id>();
        for (Order o : orders) {
            accountIdsToFetch.add(o.accountId);
            if (o instanceof PumaEnergyOrder) {
                accountIdsToFetch.add(((PumaEnergyOrder) o).shippingAccountId);
            }
        }

        Map<Id, Account> accountsById = loadAccounts(accountIdsToFetch);
        for (Order o : orders) {
            if (o instanceof PumaEnergyOrder) {
                PumaEnergyOrder rootOrder = (PumaEnergyOrder) o;
                rootOrder.readShippingAddress(accountsById.get(rootOrder.shippingAccountId));
                rootOrder.readBillingAddress(accountsById.get(rootOrder.accountId));
            }
        }

        return orders;
    }

    private static List<Order> readAttributes(List<Order> orders) {
        Map<Id, Order> ordersByConfigId = new Map<Id, Order>();
        for (Order o : orders) {
            ordersByConfigId.put(o.configId, o);
        }

        Map<Id, Map<String, String>> attributesByConfigId = loadAttributeValues(ordersByConfigId.keySet());
        for (Id configId : ordersByConfigId.keySet()) {
            ordersByConfigId.get(configId).setAttributeValues(attributesByConfigId.get(configId));
        }

        return orders;
    }

    private static List<Order> buildOrderObjects(Id accountId) {
        List<csord__Order__c> orders = fetchOrdersFromDB(accountId);
        List<Order> retVal = new List<Order>();
        for (csord__Order__c o : orders) {
            retVal.add(buildOrder(o));
        }
        return retVal;
    }

    private static Order buildOrder(csord__Order__c orderSObj) {
        if (orderSObj.csord__product_type__c == ORDER_TYPE_PUMA_ENERGY_ORDER) {
            return new PumaEnergyOrder(orderSObj);
        } else if (orderSObj.csord__product_type__c == ORDER_TYPE_PUMA_FUEL) {
            return new FuelOrder(orderSObj);
        }
        return null;
    }

    private Account readAccount(Id accountId) {
        return (Account) SObjectUtils.loadSObject(accountId);
    }

    private static Map<Id, Account> loadAccounts(Set<Id> accountIds) {
        return new Map<Id, Account>((List<Account>) SObjectUtils.loadSObjects(accountIds));
    }

    private static Map<Id, Map<String, String>> loadAttributeValues(Set<Id> configurationIds) {
        Set<String> atrributeNamesToFetch = new Set<String> {
            ATTRIBUTE_SHIPPING_ACCOUNT_ID,
            ATTRIBUTE_QUANTITY,
            ATTRIBUTE_LIST_PRICE,
            ATTRIBUTE_PRICE,
            ATTRIBUTE_TAX,
            ATTRIBUTE_PRODUCT,
            ATTRIBUTE_PAYMENT_TERM,
            ATTRIBUTE_PAYMENT_TERM_OVERRIDE,
            ATTRIBUTE_SHOULD_OVERRIDE_PAYMENT_TERM
        };

        List<cscfga__Attribute__c> attributes = [
            select
                name,
                cscfga__value__c,
                cscfga__product_configuration__c
            from cscfga__Attribute__c
            where cscfga__product_configuration__c in :configurationIds
                and name in :atrributeNamesToFetch
        ];

        Map<Id, Map<String, String>> retVal = new Map<Id, Map<String, String>>();
        for (cscfga__Attribute__c attr : attributes) {
            Map<String, String> attributeValuesByName = retVal.get(attr.cscfga__product_configuration__c);
            if (attributeValuesByName == null) {
                attributeValuesByName = new Map<String, String>();
                retVal.put(attr.cscfga__product_configuration__c, attributeValuesByName);
            }
            attributeValuesByName.put(attr.name, attr.cscfga__value__c);
        }

        return retVal;
    }

    private static List<csord__Order__c> fetchOrdersFromDB(Id accountId) {
        return [
            select
                id,
                name,
                windms_status__c,
                csord__status2__c,
                csord__order_number__c,
                csord__order_type__c,
                csord__product_type__c,
                csord__primary_order__c,
                csord__end_date__c,
                csord__start_date__c,
                csord__account__c,
                csord__desired_cease_date__c,
                csordtelcoa__product_configuration__c,
                csordtelcoa__product_configuration__r.name,
                csordtelcoa__product_configuration__r.cscfga__total_contract_value__c
            from csord__Order__c
            where csord__account__c = :accountId
                and (csord__product_type__c = :ORDER_TYPE_PUMA_ENERGY_ORDER or csord__product_type__c = :ORDER_TYPE_PUMA_FUEL)
                and (windms_status__c != :ORDER_STATUS_FINISHED or csord__primary_order__r.windms_status__c != :ORDER_STATUS_FINISHED)
            order by csord__primary_order__c nulls first, createdDate desc
        ];
    }

    public class Address {
        public String street;
        public String postalCode;
        public String city;
        public String state;
        public String country;

        public override String toString() {
            return String.format(
                '{0}, {1} {2}, {3}, {4}',
                new String[] {
                    this.street,
                    this.postalCode,
                    this.city,
                    this.state,
                    this.country
                }
            );
        }
    }

    public abstract class Order {
        public String name { get; private set; }
        public String status { get; private set; }
        public String orderNumber { get; private set; }
        public String orderType { get; private set; }
        public String productType { get; private set; }
        public Id configId { get; private set; }
        public Id parentOrderId { get; private set; }
        public Id orderId { get; private set; }
        public Id accountId { get; private set; }

        public Date endDate { get; private set; }
        public Date startDate { get; private set; }
        public Date desiredCeaseDate { get; private set; }

        public Decimal totalContractValue { get; private set; }

        public Map<String, String> attributeValues { get; private set; }

        public String processStep {
            get {
                if (this.processStep == null) {
                    this.processStep = ORDER_STATUS_TO_STEP_NAME_MAPPING.get(this.status);
                }
                return this.processStep;
            }
            private set;
        }

        public Integer processStepNumber {
            get {
                if (this.processStepNumber == null) {
                    this.processStepNumber = ORDER_STEP_TO_STEP_NUMBER_MAPPING.get(this.processStep);
                }
                return this.processStepNumber;
            }
            private set;
        }

        public Order(csord__Order__c orderSObj) {
            this.status = orderSObj.csord__status2__c;
            this.orderNumber = orderSObj.csord__order_number__c;
            this.orderType = orderSObj.csord__order_type__c;
            this.configId = orderSObj.csordtelcoa__product_configuration__c;
            this.productType = orderSObj.csord__product_type__c;
            this.parentOrderId = orderSObj.csord__primary_order__c;
            this.endDate = orderSObj.csord__end_date__c;
            this.startDate = orderSObj.csord__start_date__c;
            this.desiredCeaseDate = orderSObj.csord__desired_cease_date__c;
            this.orderId = orderSObj.id;
            this.accountId = orderSObj.csord__account__c;
            this.name = orderSObj.name;

            this.totalContractValue = orderSObj.csordtelcoa__product_configuration__r.cscfga__total_contract_value__c;

            this.attributeValues = new Map<String, String>();
        }

        public virtual void setAttributeValues(Map<String, String> valuesByName) {
            if (valuesByName == null) {
                System.debug(LoggingLevel.ERROR, 'failed to load attributes for ' + this.configId);
                return;
            }
            this.attributeValues.putAll(valuesByName);
        }
    }

    public class PumaEnergyOrder extends Order {
        public List<FuelOrder> fuelOrders { get; private set; }

        public Boolean hasFuelOrder {
            get {
                return !this.fuelOrders.isEmpty();
            }
        }

        public Id shippingAccountId {
            get{
                if (this.shippingAccountId == null) {
                    String attributeValue = this.attributeValues.get(ATTRIBUTE_SHIPPING_ACCOUNT_ID);
                    if (!String.isBlank(attributeValue)) {
                        this.shippingAccountId = (Id) attributeValue;
                    }
                }
                return this.shippingAccountId;
            }
            private set;
        }

        public Decimal quantity {
            get {
                Decimal retVal = 0;
                for (FuelOrder fo : this.fuelOrders) {
                    retVal += fo.quantity;
                }
                return retVal;
            }
        }
        
        public String paymentTerm {
            get {
                if (this.paymentTerm == null) {
                    if (this.attributeValues.get(ATTRIBUTE_SHOULD_OVERRIDE_PAYMENT_TERM) == 'Yes') {
                        this.paymentTerm = this.attributeValues.get(ATTRIBUTE_PAYMENT_TERM_OVERRIDE);
                    } else {
                        this.paymentTerm = this.attributeValues.get(ATTRIBUTE_PAYMENT_TERM);
                    }
                }
                return this.paymentTerm;
            }
            private set;
        }

        public String shipTo { get; private set; }
        public String billTo { get; private set; }

        public Address shippingAddress { get; private set; }
        public String shippingAddressNice {
            get {
                if (this.shippingAddress == null) {
                    return '';
                }
                return String.valueOf(this.shippingAddress);
            }
        }
        public Address billingAddress { get; private set; }
        public String billingAddressNice {
            get {
                if (this.billingAddress == null) {
                    return '';
                }
                return String.valueOf(this.billingAddress);
            }
        }

        public PumaEnergyOrder(csord__Order__c orderSObj) {
            super(orderSObj);
            this.fuelOrders = new List<FuelOrder>();
        }

        public FuelOrder addFuelOrder(FuelOrder o) {
            this.fuelOrders.add(o);
            return o;
        }

        public Address readShippingAddress(Account fromAccount) {
            this.shippingAddress = new Address();
            this.shippingAddress.street = fromAccount.shippingStreet;
            this.shippingAddress.country = fromAccount.shippingCountry;
            this.shippingAddress.city = fromAccount.shippingCity;
            this.shippingAddress.state = fromAccount.shippingState;
            this.shippingAddress.postalCode = fromAccount.shippingPostalCode;

            this.shipTo = fromAccount.name;

            return this.shippingAddress;
        }

        public Address readBillingAddress(Account fromAccount) {
            this.billingAddress = new Address();
            this.billingAddress.street = fromAccount.billingStreet;
            this.billingAddress.country = fromAccount.billingCountry;
            this.billingAddress.city = fromAccount.billingCity;
            this.billingAddress.state = fromAccount.billingState;
            this.billingAddress.postalCode = fromAccount.billingPostalCode;

            this.billTo = fromAccount.name;

            return this.billingAddress;
        }
    }

    public class FuelOrder extends Order {
        public Decimal quantity {
            get {
                if (this.quantity == null) {
                    String attributeValue = this.attributeValues.get(ATTRIBUTE_QUANTITY);
                    try {
                        this.quantity = Decimal.valueOf(attributeValue);
                    } catch (Exception e) {
                        System.debug(LoggingLevel.error, 'Error while parsing quantity: ' + attributeValue);
                        this.quantity = 0;
                    }
                }
                return this.quantity;
            }
            private set;
        }

        public Decimal listPrice {
            get {
                if (this.listPrice == null) {
                    String attributeValue = this.attributeValues.get(ATTRIBUTE_LIST_PRICE);
                    try {
                        this.listPrice = Decimal.valueOf(attributeValue);
                    } catch (Exception e) {
                        System.debug(LoggingLevel.error, 'Error while parsing quantity: ' + attributeValue);
                        this.listPrice = 0;
                    }
                }
                return this.listPrice;
            }
            private set;
        }

        public Decimal price {
            get {
                if (this.price == null) {
                    String attributeValue = this.attributeValues.get(ATTRIBUTE_PRICE);
                    try {
                        this.price = Decimal.valueOf(attributeValue);
                    } catch (Exception e) {
                        System.debug(LoggingLevel.error, 'Error while parsing price: ' + attributeValue);
                        this.price = 0;
                    }
                }
                return this.price;
            }
            private set;
        }

        public String product {
            get {
                if (this.product == null) {
                    this.product = this.attributeValues.get(ATTRIBUTE_PRODUCT);
                }
                return this.product;
            }
            private set;
        }

        public FuelOrder(csord__Order__c orderSObj) {
            super(orderSObj);
        }
    }
}