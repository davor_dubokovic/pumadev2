public class EP_ASTVendorBlockedToBlocked extends EP_AccountStateTransition {
    
    public EP_ASTVendorBlockedToBlocked() {
        finalstate = EP_AccountConstant.BLOCKED;
    }
    
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorBlockedToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorBlockedToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorBlockedToBlocked','isGuardCondition');
        //No guard conditions for Blocked to Blocked
        return true;
    }

}