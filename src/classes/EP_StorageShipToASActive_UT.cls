@isTest
public class EP_StorageShipToASActive_UT
{
	static final string EVENT_NAME = '05-Activeto05-Active';
    /*  
    @description: method to intialise data
    */
    
	static testMethod void setAccountDomainObject_test() {
		EP_StorageShipToASActive localObj = new EP_StorageShipToASActive();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASActiveDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.setAccountDomainObject(obj);
		Test.stopTest();
		system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
	}
	static testMethod void doOnEntry_test() {
		EP_StorageShipToASActive localObj = new EP_StorageShipToASActive();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASActiveDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnEntry();
		Test.stopTest();
		//Method has no implementation, hence adding dummy asert
		system.assert(true);
	}
	static testMethod void doOnExit_test() {
		EP_StorageShipToASActive localObj = new EP_StorageShipToASActive();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASActiveDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnExit();
		Test.stopTest();
		//Method has no implementation, hence adding dummy asert
		system.assert(true);
	}
	static testMethod void doTransition_PositiveScenariotest() {
		List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
	    List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
		EP_StorageShipToASActive localObj = new EP_StorageShipToASActive();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASActiveDomainObjectPositiveScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.doTransition();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	static testMethod void doTransition_NegativeScenariotest() {
		EP_StorageShipToASActive localObj = new EP_StorageShipToASActive();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASActiveDomainObjectNegativeScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		string errormsg = null;
		try{
			Boolean result = localObj.doTransition();
		}catch (exception e){
			errormsg = e.getmessage();
		}
		Test.stopTest();
		System.AssertEquals(true,errormsg != null);
	}
	//No negative scenario as the supper class always returns false; 
	static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
		EP_StorageShipToASActive localObj = new EP_StorageShipToASActive();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASActiveDomainObjectNegativeScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.isInboundTransitionPossible();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
}