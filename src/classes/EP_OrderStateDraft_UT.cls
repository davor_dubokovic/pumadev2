@isTest
public class EP_OrderStateDraft_UT
{
    @testSetup static void init() {
            List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
            List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
            List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
            List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_OrderStateDraft.getTextValue();
        Test.stopTest(); 
        System.AssertEquals(EP_OrderConstant.OrderState_Draft, result );         
    }
    //Method has no implementation, hence adding dummy assert  
    static testMethod void doOnEntry_test() {
        EP_OrderStateDraft localObj = new EP_OrderStateDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();  
        System.assertEquals(true, true);      
    }
    //Method has no implementation, hence adding dummy assert  
    static testMethod void doOnExit_test() {
        EP_OrderStateDraft localObj = new EP_OrderStateDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();     
        System.assertEquals(true, true);   
    }
    /* isInboundTransitionPossible does not have negative scenerio because it does not have implementation. It always returns true */
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_OrderStateDraft localObj = new EP_OrderStateDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Draft);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void doTransition_PositiveScenariotest() {
        EP_OrderStateDraft localObj = new EP_OrderStateDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_OrderStateDraft localObj = new EP_OrderStateDraft();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateDraft localObj = new EP_OrderStateDraft();
        EP_OrderDomainObject currentOrder = EP_TestDataUtility.getSalesOrderDomainObject();
        Test.startTest();
        localObj.setOrderDomainObject(currentOrder);
        Test.stopTest();
        System.assertEquals(true, localObj.order == currentOrder);
    }
}