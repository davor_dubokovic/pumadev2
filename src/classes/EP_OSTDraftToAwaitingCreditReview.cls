/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTDraftToAwaitingCreditReview>
*  @CreateDate <03/02/2017>
*  @Description <handles order state change from draft to awaiting credit review>
*  @Version <1.0>
*/

public class EP_OSTDraftToAwaitingCreditReview extends EP_OrderStateTransition{
    
    public EP_OSTDraftToAwaitingCreditReview(){
        finalState = EP_OrderConstant.OrderState_Awaiting_Credit_Review;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTDraftToAwaitingCreditReview','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTDraftToAwaitingCreditReview','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTDraftToAwaitingCreditReview','isGuardCondition');
        return true;           
    }        
}