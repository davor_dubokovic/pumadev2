@isTest
public class EP_RunService_UT
{      
     
    static testMethod void doBeforeInsertHandle_UT(){
         List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
         EP_RunDomainObject runDomainObject = new EP_RunDomainObject (runRecords);
         EP_RunService localObj = new EP_RunService(runDomainObject);
         Test.startTest();
            localObj.doBeforeInsertHandle();
         Test.stopTest();
         // Assert is not required since this method delegates to another Method.
         system.assertEquals(true,true);                             
    }
    static testMethod void doBeforeDeleteHandle_UT(){
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();    
        List<EP_Run__c> oldRunRecord = runRecords.clone();     
        oldRunRecord[0].EP_Run_Start_Date__c = System.today()+2; 
        Order ord = [SELECT Id,Status,EP_Run__c FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Run__c = oldRunRecord[0].id;
        update ord;
        Map<Id,EP_Run__c> oldRunMap = new Map<Id,EP_Run__c>(oldRunRecord);             
        EP_RunDomainObject runDomainObject = new EP_RunDomainObject (runRecords , oldRunMap );        
        EP_RunService localObj = new EP_RunService(runDomainObject);
        Test.startTest();
            localObj.doBeforeDeleteHandle();
        Test.stopTest(); 
        Order order = [SELECT Id,Status,EP_Run__c FROM Order LIMIT : 1];
        system.assertEquals(true,order.EP_Run__c!= null);
                     
    }
    static testMethod void setRunInstanceNumber_UT(){
         List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
         EP_RunDomainObject runDomainObject = new EP_RunDomainObject (runRecords);
         EP_RunService localObj = new EP_RunService(runDomainObject);
         Test.startTest();
            localObj.setRunInstanceNumber();
         Test.stopTest();
         List<EP_Run__c> runRecord= [SELECT Id, EP_Route__c, EP_Run_Number__c, Name  FROM EP_Run__c ];
         system.assertEquals(true ,runRecord[0].EP_Run_Number__c != null);          
    }
    static testMethod void getRouteIds_UT(){
         List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
         EP_RunDomainObject runDomainObject = new EP_RunDomainObject (runRecords);
         EP_RunService localObj = new EP_RunService(runDomainObject);
         Test.startTest();
            localObj.setRunInstanceNumber();
         Test.stopTest();
         List<EP_Run__c> runRecord= [SELECT Id, EP_Route__c, EP_Run_Number__c, Name  FROM EP_Run__c ];
         system.assertEquals(true ,runRecord[0].EP_Route__c != null);          
    }
    /*static testMethod void validateRunDeletionWithOrders_UT(){
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();    
        List<EP_Run__c> oldRunRecord = runRecords.clone();     
        oldRunRecord[0].EP_Run_Start_Date__c = System.today()+2; 
        List<Order> orderRecord1 = [SELECT Id, EP_Run__c FROM order];         
        Map<Id,EP_Run__c> oldRunMap = new Map<Id,EP_Run__c>(oldRunRecord);  
        EP_RunDomainObject runDomainObject = new EP_RunDomainObject (runRecords , oldRunMap );        
        EP_RunService localObj = new EP_RunService(runDomainObject);
        Test.startTest();
          localObj.validateRunDeletionWithOrders(oldRunMap);
        Test.stopTest();
        List<Order> orderRecord = [SELECT Id, EP_Run__c FROM order];         
        system.assertEquals(true ,orderRecord[0].EP_Run__c != null);
    }*/
}