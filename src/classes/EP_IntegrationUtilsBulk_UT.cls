/**
  * @author       Accenture
  * @name         EP_IntegrationUtilsBulk
  * @date         11/10/2017
  * @description  Unit Test Class for EP_IntegrationUtilsBulk
  */
@isTest
public class EP_IntegrationUtilsBulk_UT {

	//static member variable to be used as test data
    private static Account acc;
    private static list<Account> lstAccount;
    private static Account sellToAccount;
    private static Account shipToTemp;
    private static String txId;
    private static String msgId;
    private static String company;
    private static DateTime dt_sent; 
    private static final String TESTREC = 'test';
    private static final String SENTERROR = 'ERROR-SENT';
    private static final String SYNCERROR = 'ERROR-SYNC';
    private static final String INITIAL_VAL = '';
    private static final EP_Country__c country;

    // Data created for Test
	static {
		List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
        country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
	    insert country;
	    lstAccount = new List<Account>();
	    sellToAccount = EP_TestDataUtility.getSellTo();
		shipToTemp = EP_TestDataUtility.getShipTo();
		for (integer i=0;i<=100;i++) {
	   		Account shipTo = shipToTemp.clone();
	    	shipTo.ParentId=sellToAccount.Id;
	    	shipTo.Name = TESTREC;
	    	shipTo.EP_Composite_Id__c = shipTo.EP_Composite_Id__c+i;
	    	lstAccount.add(shipTo);
	    }

	    insert lstAccount; 
	    lstAccount.add(sellToAccount);
	    txId = EP_IntegrationUtil.getTransactionID('Test','create');    
	    msgId = EP_IntegrationUtil.getMessageId('Test'); 
	    company = 'test company';   
	    dt_sent = DateTime.now();       
    }


    /**
    * @author       Accenture
    * @name         createIntegrationRecordOutbound_PositiveTest
    * @date         11/10/2017
    * @description  Test Class to Test "createIntegrationRecordOutbound" for Positive Scenario
    */  
	@isTest
	static void createIntegrationRecordOutbound_PositiveTest() {
		Test.startTest();
		Set<Id> setAccountIds = new Set<Id>();
		for (Account objAccount : lstAccount) {
			setAccountIds.add(objAccount.Id);
		}
		
		List<EP_IntegrationRecord__c> integrationRecords = EP_IntegrationUtilsBulk.createIntegrationRecordOutbound(setAccountIds,txId,EP_Common_Constant.ACCOUNTS,msgId,company,dt_sent,'SENT','NAV','');
		Test.stopTest();
		System.assertEquals(integrationRecords.size(),102);
	}

	 /**
    * @author       Accenture
    * @name         createIntegrationRecordOutbound_PositiveTest
    * @date         11/10/2017
    * @description  Test Class to Test "createIntegrationRecordOutbound" for Negative Scenarios
    */ 
	@isTest
	static void createIntegrationRecordOutbound_NegativeTest() {
		Test.startTest();
		Set<Id> setAccountIds = new Set<Id>();
		for (Account objAccount : lstAccount) {
			setAccountIds.add(objAccount.Id);
		}
		
		List<EP_IntegrationRecord__c> integrationRecordsErrorSent = EP_IntegrationUtilsBulk.createIntegrationRecordOutbound(setAccountIds,txId,EP_Common_Constant.ACCOUNTS,msgId,company,dt_sent,EP_Common_Constant.ERROR_SENT_STATUS,'NAV','');
		System.assertEquals(integrationRecordsErrorSent.size(),102);
		List<EP_IntegrationRecord__c> integrationRecordsErrorSync = EP_IntegrationUtilsBulk.createIntegrationRecordOutbound(setAccountIds,txId,EP_Common_Constant.ACCOUNTS,msgId,company,dt_sent,EP_Common_Constant.ERROR_SYNC_STATUS,'NAV','');
		System.assertEquals(integrationRecordsErrorSync.size(),102);
		List<EP_IntegrationRecord__c> integrationRecordsErrorRecieved = EP_IntegrationUtilsBulk.createIntegrationRecordOutbound(setAccountIds,txId,EP_Common_Constant.ACCOUNTS,msgId,company,dt_sent,EP_Common_Constant.ERROR_RECEIVED_STATUS,'NAV','');
		System.assertEquals(integrationRecordsErrorRecieved.size(),102);
		Test.stopTest();
	}

	 /**
    * @author       Accenture
    * @name         createIntegrationRecordOutbound_PositiveTest
    * @date         11/10/2017
    * @description  Test Class to Test "createIntegrationRecordOutbound" for Exception Handling Scenario
    */ 
	@isTest
	static void createIntegrationRecordOutbound_ExeptionHandling() {
		Test.startTest();
		Set<Id> setAccountIds = new Set<Id>();
		for (Account objAccount : lstAccount) {
			setAccountIds.add(objAccount.Id);
		}
		
		List<EP_IntegrationRecord__c> integrationRecordsErrorSent = EP_IntegrationUtilsBulk.createIntegrationRecordOutbound(setAccountIds,txId,'Accounts',msgId,company,dt_sent,null,'NAV','');
		System.assertEquals(integrationRecordsErrorSent.size(),0);
		Test.stopTest();
	}


	 /**
    * @author       Accenture
    * @name         getSObjectRecords_Positive
    * @date         11/10/2017
    * @description  Test Class to Test "getSObjectRecords" for Positive Scenario
    */ 
	@isTest
	static void getSObjectRecords_Positive() {
		Test.startTest();
		Map<String,EP_Integration_Status_Update__c> integrationCS = EP_Integration_Status_Update__c.getAll();
		Map<Id,SObject> resultMap;
		
		//for Account
		Set<Id> setAccountIds = new Set<Id>();
		for (Account objAccount : lstAccount) {
			setAccountIds.add(objAccount.Id);
		} 
		
		Schema.SObjectType objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.ACCOUNTS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.ACCOUNTS,objName,setAccountIds);
		System.assertEquals(resultMap.size(),102);
		
		// for stock locations
		EP_Stock_Holding_Location__c stockHolding = EP_TestDataUtility.createStockLocation(shipToTemp.Id,true);
		insert stockHolding;
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.STOCKLOCATIONS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.STOCKLOCATIONS,objName,new Set<Id>{stockHolding.Id});
		System.assertEquals(resultMap.size(),1);

	
		// for Tanks
		Product2 prod = [SELECT Id FROM Product2 LIMIT 1];
		System.debug('*prod****'+prod);
		Map<ID, EP_Tank__c> tanksMap = EP_TestDataUtility.createTestTankRecords(lstAccount, prod, 'New', 1);
		System.debug('*****'+tanksMap);
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.TANKS_INT).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.TANKS_INT,objName,tanksMap.keySet());
		System.assertEquals(resultMap.size(),101);

		
		// for Tanks Dips
		Map<ID, EP_Tank_Dip__c> tankDipsMap  = EP_TestDataUtility.createTestTankDipsRecords(tanksMap.values(),Datetime.now(),true);
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.DIPS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.DIPS,objName,tankDipsMap.keySet());
		System.assertEquals(resultMap.size(),101);

		// for Orders
		csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.ORDERS_INT).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.ORDERS_INT,objName,new Set<Id>{ord.Id});
		System.assertEquals(resultMap.size(),1);

		// for OrderItems
		OrderItem ordItem =  [SELECT Id FROM OrderItem LIMIT 1];
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.ORDERITEMS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.ORDERITEMS,objName,new Set<Id>{ordItem.Id});
		System.assertEquals(resultMap.size(),1);		
		
		// for Bank Accounts
		EP_Bank_Account__c bankAccounts = EP_TestDataUtility.createBankAccount(sellToAccount.Id);
		bankAccounts.EP_Country__c = country.Id;
		insert bankAccounts;
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.BANKACCOUNTS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.BANKACCOUNTS,objName,new Set<Id>{bankAccounts.Id});
		System.assertEquals(resultMap.size(),1);

		// for Credit Exceptions
		EP_Credit_Exception_Request__c creditException = EP_TestDataUtility.createCreditException();
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.CREDITEXCEPTIONS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.CREDITEXCEPTIONS,objName,new Set<Id>{creditException.Id});
		System.assertEquals(resultMap.size(),1);
		Test.stopTest();
		
	}

	/**
    * @author       Accenture
    * @name         getSObjectRecords_Negative
    * @date         12/10/2017
    * @description  Test Class to Test "getSObjectRecords" for Negative Scenario
    */ 
	@isTest
	static void getSObjectRecords_Negative() {
		Test.startTest();
		Map<String,EP_Integration_Status_Update__c> integrationCS = EP_Integration_Status_Update__c.getAll();
		Map<Id,SObject> resultMap;
		
		//for Account
		Schema.SObjectType objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.ACCOUNTS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.ACCOUNTS,objName,null);
		System.assertEquals(resultMap.size(),0);

		// for stock locations
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.STOCKLOCATIONS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.STOCKLOCATIONS,objName,null);
		System.assertEquals(resultMap.size(),0);

		// for Tanks
		Product2 prod = [SELECT Id FROM Product2 LIMIT 1];
		Map<ID, EP_Tank__c> tanksMap = EP_TestDataUtility.createTestTankRecords(lstAccount, prod, 'New', 1);
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.TANKS_INT).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.TANKS_INT,objName,null);
		System.assertEquals(resultMap.size(),0);


		// for Tanks Dips
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.DIPS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.DIPS,objName,null);
		System.assertEquals(resultMap.size(),0);

		// for OrderItems
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.ORDERITEMS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.ORDERITEMS,objName,null);
		System.assertEquals(resultMap.size(),0);

		// for Orders
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.ORDERS_INT).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.ORDERS_INT,objName,null);
		System.assertEquals(resultMap.size(),0);

		// for Bank Accounts
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.BANKACCOUNTS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.BANKACCOUNTS,objName,null);
		System.assertEquals(resultMap.size(),0);

		// for Credit Exceptions
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.CREDITEXCEPTIONS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.CREDITEXCEPTIONS,objName,null);
		System.assertEquals(resultMap.size(),0);

		// for Tanks
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.TANKS_INT).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.TANKS_INT,objName,null);
		System.assertEquals(resultMap.size(),0);
		Test.stopTest();
	}

	/**
    * @author       Accenture
    * @name         updateNameAndCountryOnIntegrationRec
    * @date         13/10/2017
    * @description  Test Class to Test "updateNameAndCountryOnIntegrationRec" for Positive Scenario
    */ 
	@isTest
	static void updateNameAndCountryOnIntegrationRec_Positive() {
		Test.startTest();
		
		Map<String,EP_Integration_Status_Update__c> integrationCS = EP_Integration_Status_Update__c.getAll();
		Map<Id,SObject> resultMap;
		
		//for Account
		EP_IntegrationRecord__c integrationRec = EP_TestDataUtility.createIntegrationRec(sellToAccount.Id,EP_Common_Constant.ACCOUNTS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{sellToAccount.Id => sellToAccount},sellToAccount.Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertNotEquals(integrationRec.EP_Country__c,null);

		// for stock locations
		EP_Stock_Holding_Location__c stockHolding = EP_TestDataUtility.createStockLocation(shipToTemp.Id,true);
		insert stockHolding;
		Schema.SObjectType objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.STOCKLOCATIONS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.STOCKLOCATIONS,objName,new Set<Id>{stockHolding.Id});
		integrationRec = EP_TestDataUtility.createIntegrationRec(resultMap.values()[0].Id,EP_Common_Constant.STOCKLOCATIONS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{resultMap.values()[0].Id => resultMap.values()[0]},resultMap.values()[0].Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertNotEquals(integrationRec.EP_Country__c,null);

		// for Tanks
		Product2 prod = [SELECT Id FROM Product2 LIMIT 1];
		Map<ID, EP_Tank__c> tanksMap = EP_TestDataUtility.createTestTankRecords(lstAccount, prod, 'New', 1);
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.TANKS_INT).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.TANKS_INT,objName,tanksMap.keySet());
		integrationRec = EP_TestDataUtility.createIntegrationRec(resultMap.values()[0].Id,EP_Common_Constant.TANKS_INT,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{resultMap.values()[0].Id => resultMap.values()[0]},resultMap.values()[0].Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertNotEquals(integrationRec.EP_Country__c,null);
		
		// for Tanks Dips
		Map<ID, EP_Tank_Dip__c> tankDipsMap  = EP_TestDataUtility.createTestTankDipsRecords(tanksMap.values(),Datetime.now(),true);
		objName = Schema.getGlobalDescribe().get(integrationCS.get(EP_Common_Constant.DIPS).EP_API_Name__c);
		resultMap = EP_IntegrationUtilsBulk.getSObjectRecords(EP_Common_Constant.DIPS,objName,tankDipsMap.keySet());
		integrationRec = EP_TestDataUtility.createIntegrationRec(resultMap.values()[0].Id,EP_Common_Constant.DIPS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{resultMap.values()[0].Id => resultMap.values()[0]},resultMap.values()[0].Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertNotEquals(integrationRec.EP_Country__c,null);

		// for Orders
		csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
		
		integrationRec = EP_TestDataUtility.createIntegrationRec(ord.Id,EP_Common_Constant.ORDERS_INT,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{ord.Id => ord},ord.Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertNotEquals(integrationRec.EP_Country__c,null);

		// for OrderItems
		OrderItem ordItem =  [SELECT Id,OrderItemNumber,Order.Account.EP_Country__c FROM OrderItem LIMIT 1];
		integrationRec = EP_TestDataUtility.createIntegrationRec(ordItem.Id,EP_Common_Constant.ORDERITEMS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{ordItem.Id => ordItem},ordItem.Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertNotEquals(integrationRec.EP_Country__c,null);
		
		// for Bank Accounts
		EP_Bank_Account__c bankAccounts = EP_TestDataUtility.createBankAccount(sellToAccount.Id);
		bankAccounts.EP_Country__c = country.Id;
		insert bankAccounts;
		bankAccounts = [SELECT Id,EP_Country__c,Name FROM EP_Bank_Account__c WHERE Id=:bankAccounts.Id];
		integrationRec = EP_TestDataUtility.createIntegrationRec(bankAccounts.Id,EP_Common_Constant.BANKACCOUNTS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{bankAccounts.Id => bankAccounts},bankAccounts.Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertNotEquals(integrationRec.EP_Country__c,null);

		// for Credit Exceptions
		EP_Credit_Exception_Request__c creditException = EP_TestDataUtility.createCreditException();
		creditException = [SELECT Id,Name FROM EP_Credit_Exception_Request__c WHERE Id=:creditException.Id];
		integrationRec = EP_TestDataUtility.createIntegrationRec(creditException.Id,EP_Common_Constant.CREDITEXCEPTIONS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>{creditException.Id => creditException},creditException.Id);
		System.assertNotEquals(integrationRec.EP_Object_Record_Name__c,null);

		Test.stopTest();
	} 


	/**
    * @author       Accenture
    * @name         updateNameAndCountryOnIntegrationRec_Negative
    * @date         13/10/2017
    * @description  Test Class to Test "updateNameAndCountryOnIntegrationRec" for Negative Scenario
    */ 
	@isTest
	static void updateNameAndCountryOnIntegrationRec_Negative() {
		Test.startTest();

		//for Account
		EP_IntegrationRecord__c integrationRec = EP_TestDataUtility.createIntegrationRec(sellToAccount.Id,EP_Common_Constant.ACCOUNTS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),null);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);

		// for stock locations
		EP_Stock_Holding_Location__c stockHolding = EP_TestDataUtility.createStockLocation(shipToTemp.Id,true);
		insert stockHolding;
		integrationRec = EP_TestDataUtility.createIntegrationRec(stockHolding.Id,EP_Common_Constant.STOCKLOCATIONS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),stockHolding.Id);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);

		// for Tanks
		Product2 prod = [SELECT Id FROM Product2 LIMIT 1];
		Map<ID, EP_Tank__c> tanksMap = EP_TestDataUtility.createTestTankRecords(lstAccount, prod, 'New', 1);
		EP_Tank__c tank = tanksMap.values()[0];
		integrationRec = EP_TestDataUtility.createIntegrationRec(tank.Id,EP_Common_Constant.TANKS_INT,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),tank.Id);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);
		
		// for Tanks Dips
		Map<ID, EP_Tank_Dip__c> tankDipsMap  = EP_TestDataUtility.createTestTankDipsRecords(tanksMap.values(),Datetime.now(),true);
		EP_Tank_Dip__c tankDips = tankDipsMap.values()[0];
		integrationRec = EP_TestDataUtility.createIntegrationRec(tankDips.Id,EP_Common_Constant.DIPS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),tankDips.Id);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);

		// for Orders
		csord__Order__c ord = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
		integrationRec = EP_TestDataUtility.createIntegrationRec(ord.Id,EP_Common_Constant.ORDERS_INT,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),ord.Id);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);

		// for OrderItems
		OrderItem ordItem =  [SELECT Id FROM OrderItem LIMIT 1];
		integrationRec = EP_TestDataUtility.createIntegrationRec(ordItem.Id,EP_Common_Constant.ORDERITEMS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),ordItem.Id);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);

		// for Bank Accounts
		EP_Bank_Account__c bankAccounts = EP_TestDataUtility.createBankAccount(sellToAccount.Id);
		insert bankAccounts;
		integrationRec = EP_TestDataUtility.createIntegrationRec(bankAccounts.Id,EP_Common_Constant.BANKACCOUNTS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),bankAccounts.Id);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);

		// for Credit Exceptions
		EP_Credit_Exception_Request__c creditException = EP_TestDataUtility.createCreditException();
		integrationRec = EP_TestDataUtility.createIntegrationRec(creditException.Id,EP_Common_Constant.CREDITEXCEPTIONS,Datetime.now(),company,'SENT');
		integrationRec = EP_IntegrationUtilsBulk.updateNameAndCountryOnIntegrationRec(integrationRec,new Map<Id,SObject>(),creditException.Id);
		System.assertEquals(integrationRec.EP_Object_Record_Name__c,null);
		System.assertEquals(integrationRec.EP_Country__c,null);

		Test.stopTest();
	} 

	/**
    * @author       Accenture
    * @name         reCreateSeqId_Positive
    * @date         13/10/2017
    * @description  Test Class to Test "reCreateSeqId" for Positive Scenario
    */ 
	@isTest
	static void reCreateSeqId_Positive() {
		String msgId = 'SFD-GBL-PER-12102017-17:32:19-015022';
		String existingSeqId = sellToAccount.Id;
		String result = EP_IntegrationUtilsBulk.reCreateSeqId(msgId,existingSeqId);
		System.assertEquals(sellToAccount.Id+'-12102017173219-015022',result);
	}

	/**
    * @author       Accenture
    * @name         reCreateSeqId_Negative
    * @date         13/10/2017
    * @description  Test Class to Test "reCreateSeqId" for Negative Scenario
    */ 
	@isTest
	static void reCreateSeqId_Negative() {
		String msgId = null;
		String existingSeqId = null;
		String result = EP_IntegrationUtilsBulk.reCreateSeqId(msgId,existingSeqId);
		System.assertEquals(null,result);
	}
}