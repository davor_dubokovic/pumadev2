/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToInActiveToActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 07-Inactive to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToInActiveToActive extends EP_AccountStateTransition {

    public EP_ASTStorageShipToInActiveToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToInActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToInActiveToActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToInActiveToActive',' isGuardCondition');        
        return true;
    }
}