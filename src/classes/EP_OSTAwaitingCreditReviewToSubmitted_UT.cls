@isTest
public class EP_OSTAwaitingCreditReviewToSubmitted_UT
{
    static testMethod void isTransitionPossible_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        EP_OSTAwaitingCreditReviewToSubmitted ost = new EP_OSTAwaitingCreditReviewToSubmitted();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
        //As the class implements only positive scenario, no negative case can be checked against the method. 
    }
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        EP_OSTAwaitingCreditReviewToSubmitted ost = new EP_OSTAwaitingCreditReviewToSubmitted();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
        //As the class implements only positive scenario, no negative case can be checked against the method.
    }
    static testMethod void isGuardCondition_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        EP_OSTAwaitingCreditReviewToSubmitted ost = new EP_OSTAwaitingCreditReviewToSubmitted();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
        //As the class implements only positive scenario, no negative case can be checked against the method.
    }
}