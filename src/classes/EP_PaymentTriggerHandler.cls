/* 
      @Author <Pooja Dhiman>
      @name <EP_PaymentTriggerHandler>
      @CreateDate <05/24/2016>
      @Description <This class handles the EP_CustomerPaymentTrigger>
      @Version <1.0>
 */
public with sharing class EP_PaymentTriggerHandler {
	
    /*
        This method handles after insert requests from Payment trigger
    */
    public static void doAfterInsert(List<EP_Customer_Payment__c> lNewPayment){
       EP_PaymentTriggerHelper.createCustomerAccountStatmentItem(lNewPayment);
    }
}