@isTest
public class EP_OSTVMIConsignPlannedToCancelled_UT
{
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
    static testMethod void isTransitionPossible_positive_test() {
        EP_OrderDomainObject obj = new EP_OrderDomainObject(EP_TestDataUtility.getSalesOrderWithInvoiceOverdue());
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStatePlannedDomainObjectNegativeScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }


    static testMethod void isRegisteredForEvent_positive_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStatePlannedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {

        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStatePlannedDomainObjectNegativeScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }


    static testMethod void isGuardCondition_positive_test() {
        EP_OrderDomainObject obj = new EP_OrderDomainObject(EP_TestDataUtility.getSalesOrderWithInvoiceOverdue());
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isGuardCondition_negative_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStatePlannedDomainObjectNegativeScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_isInventory_test() {
        csord__Order__c orderIns = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario();
        EP_OrderDomainObject obj =new EP_OrderDomainObject(orderIns.Id);
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        //System.AssertEquals(true,result);
    }
    static testMethod void isGuardCondition_hasCreditIssue_test() {
        //Order orderIns = EP_TestDataUtility.getOrderStatePlannedDomainObjectNegativeScenario();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateLoadedDomainObjectNegativeScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OSTVMIConsignPlannedToCancelled ost = new EP_OSTVMIConsignPlannedToCancelled();
        ost.setOrderContext(obj,oe);
        Test.startTest();
        boolean result = ost.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
}