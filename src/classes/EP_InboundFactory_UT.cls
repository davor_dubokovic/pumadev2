@isTest
public class EP_InboundFactory_UT
{
	public static final string MESSAGE_TYPE ='VMI_ORDER_CREATION'; 
	static testMethod void getInboundHandler_test() {
		List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
	    EP_InboundFactory localObj = new EP_InboundFactory();     
	    String messageType = MESSAGE_TYPE ;     
	    
	    Test.startTest();
	    	EP_InboundHandler result = localObj.getInboundHandler(messageType);
	    Test.stopTest();
	    
	    System.AssertNotEquals(null ,result );
	}
	//#60147 User Story Start
	static testMethod void getResponseHandler_test() {
		List<EP_CS_OutboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
	    EP_InboundFactory localObj = new EP_InboundFactory();     
	    String messageType = EP_Common_Constant.SEND_ORDER_PRICING_REQUEST ;     
	    
	    Test.startTest();
	    	EP_InboundHandler result = localObj.getResponseHandler(messageType);
	    Test.stopTest();
	    
	    System.AssertNotEquals(null ,result );
	}
	//#60147 User Story End
}