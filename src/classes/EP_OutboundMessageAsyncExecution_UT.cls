/*
   @Author          Accenture
   @Name            EP_OutboundMessageAsyncExecution_UT
   @CreateDate      02/07/2017
   @Description     Test Pricing Response in Synchronous way - #60147
   					Another way to do future callout. 
   					For VMI order Creation, A Pricing callout will be a future outbound call out and after successfully pricing sync, State machine will hit another future outbound callout for order sync with NAV and SFDC does not allow to do a future callout from futrue call out
   @Version         1.0
*/
@isTest
private class EP_OutboundMessageAsyncExecution_UT {
	@testSetup static void init() {
		List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
      	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
      	List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
      	List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }
    
	static testMethod void doEnqueueJobCallOut() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    Account shipToAccount =EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();  
	    Account accObject =[select id, Parent.Id,Parent.EP_Puma_Company_Code__c from Account where id=:shipToAccount.Id];
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = accObject.Parent.Id;
	    String companyName= accObject.Parent.EP_Puma_Company_Code__c;  
	    LIST<id> childIdList = new List<Id>(); 
	    childIdList.add(shipToAccount.Id);
	    EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);
	    string messageId = localObj.generateMessageId();
        list<EP_IntegrationRecord__c> intRecordList = localObj.generateIntegrationRecords(messageId,companyName, childIdList);
        System.assertEquals(intRecordList[0].EP_XML_Message__c,null);
        set <Id> intRecordIdSet = EP_OutboundMessageUtil.getSObjectIdSet(intRecordList);
	    Test.startTest();
	    EP_OutboundMessageAsyncExecution asyncJob = new EP_OutboundMessageAsyncExecution(recordId,messageId, messageType, intRecordIdSet, companyName);
        ID jobID = System.enqueueJob(asyncJob);
	    Test.stopTest();
	    EP_IntegrationRecordMapper integrationMapper = new EP_IntegrationRecordMapper();
	    intRecordList = integrationMapper.getParentsRecords(intRecordIdSet);
	    System.assertNotEquals(intRecordList[0].EP_XML_Message__c,null);
	}
}