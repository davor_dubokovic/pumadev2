@isTest
public class EP_AccountShareUtility_UT { 
    private static User testSellToPortalUser;
    private static User testShipToPortalUser;
    private static final String PORTAL_SUPER_USER_NAME = 'EP_Portal_Super_User';
    private static Account sellToAccount;
    private static Account shipToAccount;
    private static Contact sellToContact;
    private static Contact shiptocontact; 
    
    
	@testSetup static void init() {       
		List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
	}
    
	public static void createTestdata() {
		user userWithRole = new user();
	    /* Make sure the running user has a role otherwise an exception will be thrown. */      
        if(UserInfo.getUserRoleId() == null) {
        	UserRole usrrole = [SELECT ID FROM UserRole WHERE PortalType = 'None' LIMIT 1];
            userWithRole = EP_TestDataUtility.createUser(UserInfo.getProfileId());
            userWithRole.userroleid = usrrole.id;
        }else {
            userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
        }	    
	    System.runAs(userWithRole) {
	      EP_AccountDomainObject shipToObj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectForPortal();
	      shipToAccount = shipToObj.localaccount;
	
	      shiptocontact = new Contact(LastName='ShipToContact',AccountId= shipToAccount.Id);
	      Database.insert(shiptocontact);
	    }	    
	    /* Get any profile for the given type.*/
	    Profile portalProfile = [SELECT ID FROM Profile WHERE Name = :PORTAL_SUPER_USER_NAME LIMIT 1];         
	    String testemail = 'testportaluser@test.com';
	    User portaluser = EP_TestDataUtility.createPortalUser(portalProfile.Id, shiptocontact.Id); 
	    Database.insert(portaluser);      
	}
	
    
    private static User portalAccountOwner(){
        List<UserRole> lTestRoles = [SELECT ID FROM UserRole WHERE PortalType = 'None' LIMIT 1];
        List<Profile> portalUserProfiles = [SELECT ID FROM Profile WHERE Name = :PORTAL_SUPER_USER_NAME LIMIT 1];
        User usr = EP_TestDataUtility.createUser(portalUserProfiles[0].Id);
        usr.UserRoleId = lTestRoles[0].Id;
        insert usr;
        return usr;
    }
    
    static testmethod void getAccountsForProcessing_test() {
        Map<Id, Account> mapAccountsForProcessing = new Map<Id, Account>();  
        Test.startTest();
        	createTestdata();
        	mapAccountsForProcessing = EP_AccountShareUtility.getAccountsForProcessing(shipToAccount);        
        Test.stopTest();
        system.assertEquals(2,mapAccountsForProcessing.values().size());
    }
    
    static testmethod void getUserRole_test(){ 
        List<UserRole> usrRole =  new List<UserRole>();   
        Test.startTest();        
	    createTestdata();      
        usrRole = EP_AccountShareUtility.getUserRole(new set<Id> {shipToAccount.Id});          
        Test.stopTest();
        system.assertEquals(3,usrRole.size());
    }

    static testmethod void getGroupPortalUserRoleRecords_test() {
        Map<String, String> mapGroupPortalUserRoleRecords =  new Map<String, String>();     
        Test.startTest();
            createTestData();           
            List<UserRole> usrRole = EP_AccountShareUtility.getUserRole(new set<Id> {shipToAccount.Id});
            mapGroupPortalUserRoleRecords = EP_AccountShareUtility.getGroupPortalUserRoleRecords(usrRole);
        Test.stopTest();
        system.assert(mapGroupPortalUserRoleRecords.values().size()>0);
    }    
    
    static testmethod void processPortalRoleSharingRules_test() {    
        Test.startTest();
            createTestData();
            Map<Id, Account> mapAccountsForProcessing = EP_AccountShareUtility.getAccountsForProcessing(shipToAccount);
            List<UserRole> usrRole = EP_AccountShareUtility.getUserRole(mapAccountsForProcessing.keyset());
            Map<String, String> mapGroupPortalUserRoleRecords = EP_AccountShareUtility.getGroupPortalUserRoleRecords(usrRole);
            EP_AccountShareUtility.mapAccountsForProcessing = mapAccountsForProcessing;
            EP_AccountShareUtility.lAccountUserRoles = usrRole;
            EP_AccountShareUtility.mapGroupPortalUserRoleRecords = mapGroupPortalUserRoleRecords;
            EP_AccountShareUtility.processPortalRoleSharingRules();
        Test.stopTest();
    }
    
    static testmethod void setSharingAccess_test() {
        List<EP_AccountShareUtility.ShareAccessWrapper> sharingAccessWrapper = new List<EP_AccountShareUtility.ShareAccessWrapper>();
        Test.startTest();
            createTestData();
            Map<Id, Account> mapAccountsForProcessing = EP_AccountShareUtility.getAccountsForProcessing(shipToAccount);
            List<UserRole> usrRole = EP_AccountShareUtility.getUserRole(mapAccountsForProcessing.keyset());
            Map<String, String> mapGroupPortalUserRoleRecords = EP_AccountShareUtility.getGroupPortalUserRoleRecords(usrRole);          
            EP_AccountShareUtility.mapAccountsForProcessing = mapAccountsForProcessing;
            EP_AccountShareUtility.lAccountUserRoles = usrRole;
            EP_AccountShareUtility.mapGroupPortalUserRoleRecords = mapGroupPortalUserRoleRecords;
            sharingAccessWrapper = EP_AccountShareUtility.setSharingAccess(shipToAccount.Id);
        Test.stopTest();
        system.assert(!sharingAccessWrapper.isEmpty());
    }
    
    static testmethod void getAccountAccessType_Edit_test() {
    	List<group> grp = EP_TestDataUtility.getExistingGroups(1);
        string accountAccessLevel;
        Test.startTest();
            createTestData();
            EP_AccountShareUtility.ShareAccessWrapper sharingAccessWrapper = new EP_AccountShareUtility.ShareAccessWrapper();
            sharingAccessWrapper.AccountId = shipToAccount.Id;
            sharingAccessWrapper.PortalRoleGroupId = grp[0].Id;
            sharingAccessWrapper.accountEditAccess = true;
            accountAccessLevel = EP_AccountShareUtility.getAccountAccessType(sharingAccessWrapper);
        Test.stopTest();
        system.assertEquals('Edit',accountAccessLevel);
    }
    
    static testmethod void getAccountAccessType_Read_test() {
        string accountAccessLevel;
        List<group> grp = EP_TestDataUtility.getExistingGroups(1);
        Test.startTest();
            createTestData();
            EP_AccountShareUtility.ShareAccessWrapper sharingAccessWrapper = new EP_AccountShareUtility.ShareAccessWrapper();
            sharingAccessWrapper.AccountId = shipToAccount.Id;
            sharingAccessWrapper.PortalRoleGroupId = grp[0].Id;
            sharingAccessWrapper.accountEditAccess = false;
            accountAccessLevel = EP_AccountShareUtility.getAccountAccessType(sharingAccessWrapper);
        system.assertEquals('Read',accountAccessLevel);
    }
    
    static testmethod void getCaseAccessType_Edit_test() {
        string caseAccessLevel;
         List<group> grp = EP_TestDataUtility.getExistingGroups(1);
        Test.startTest();
            createTestData();
            EP_AccountShareUtility.ShareAccessWrapper sharingAccessWrapper = new EP_AccountShareUtility.ShareAccessWrapper();
            sharingAccessWrapper.AccountId = shipToAccount.Id;
            sharingAccessWrapper.PortalRoleGroupId = grp[0].Id;
            sharingAccessWrapper.accountEditAccess = false;
            sharingAccessWrapper.caseEditAccess = true;
            caseAccessLevel = EP_AccountShareUtility.getCaseAccessType(sharingAccessWrapper);
        Test.stopTest();
        system.assertEquals('Edit',caseAccessLevel);
    }
    
    static testmethod void getCaseAccessType_None_test() {
        string caseAccessLevel;
         List<group> grp = EP_TestDataUtility.getExistingGroups(1);
        Test.startTest();
            createTestData();
            EP_AccountShareUtility.ShareAccessWrapper sharingAccessWrapper = new EP_AccountShareUtility.ShareAccessWrapper();
            sharingAccessWrapper.AccountId = shipToAccount.Id;
            sharingAccessWrapper.PortalRoleGroupId = '';
            sharingAccessWrapper.accountEditAccess = false;
            sharingAccessWrapper.caseEditAccess = false;
            caseAccessLevel = EP_AccountShareUtility.getCaseAccessType(sharingAccessWrapper);        
        Test.stopTest();
        system.assertEquals('None',caseAccessLevel);
    }
    
    static testmethod void getAccountPortalRoleManualSharingRecord_test() {
		List<Group> testgroup = EP_TestDataUtility.getExistingGroups(1);
		createTestData();
        AccountShare accountShareObj = new AccountShare();
        Test.startTest();
            EP_AccountShareUtility.ShareAccessWrapper sharingAccessWrapper = new EP_AccountShareUtility.ShareAccessWrapper();
            sharingAccessWrapper.AccountId = shipToAccount.id;
            sharingAccessWrapper.PortalRoleGroupId = testgroup[0].id;
            sharingAccessWrapper.accountEditAccess = true;
            sharingAccessWrapper.caseEditAccess = true;
            accountShareObj = EP_AccountShareUtility.getAccountPortalRoleManualSharingRecord(sharingAccessWrapper);
        Test.stopTest();
        system.assertEquals(shipToAccount.Id,accountShareObj.AccountId);
        system.assertEquals('Edit',accountShareObj.AccountAccessLevel);
        system.assertEquals('Edit',accountShareObj.CaseAccessLevel);
    }    
    
    static testmethod void insertManualSharingRecords_test(){
        AccountShare accountShareObj = new AccountShare();
        user runasuser = [Select id from user where ProfileId =: userInfo.getProfileId() AND Id !=: userInfo.getUserId() AND isActive = true Limit 1];
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Test.startTest();
	        system.runAs(thisUser){ 
	        	createTestData();
	        	EP_AccountShareUtility.insertManualSharingRecords(shipToAccount.Id);
	        }
        Test.stopTest();
        // Check that there are sharing rules in the system for the ship-To
        List<AccountShare> shipToaccountShareRecords = [SELECT Id, UserOrGroupId, AccountId FROM AccountShare WHERE AccountId = :shipToAccount.Id AND RowCause = 'Manual'];
        system.assert(shipToaccountShareRecords.size()>0);
    }    
}