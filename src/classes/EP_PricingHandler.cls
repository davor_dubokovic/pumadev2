/*
Author: Logistics - EPUMA
Description: This class parses the pricing response
*/
public with sharing class EP_PricingHandler {
    public static map < String, List < cls_invoiceComponent >> lineIdInvoiceDetailMap = new map < String, List < cls_invoiceComponent >> ();
    public static map < String, List < cls_lineItemInfo >> lineMapRec = new map < String, List < cls_lineItemInfo >> ();
    public static map < String, List < cls_acctComponent >> lineIdAcctDetailMap = new map < String, List < cls_acctComponent >> ();
    public static List < cls_lineItemInfo > lineItemLst = new List < cls_lineItemInfo > ();
    private static final string CREATEDATASET = 'createDataSet';
    public static String orderRecId = EP_Common_Constant.BLANK;
    public static Boolean hasError = false;
    private static set < String > sSeqId = new set < String > ();
    public static map < id, String > errDes = new map < id, string > ();
    public static map < id, String > errCode = new map < id, String > ();
    public static String processStatus = EP_Common_Constant.SUCCESS;
    public static String accTagString = EP_Common_Constant.BLANK;
    public static OrderItem invOrderItem;
    public static Map < String, Id > peMap = new Map < String, Id > ();
    public static List < EP_AcknowledmentGenerator.cls_dataset > lDataSets = new list < EP_AcknowledmentGenerator.cls_dataset > ();
    public static String ordItemQuery = 'Select id, OrderId, Quantity, OrderItemNumber, PriceBookEntryId from OrderItem where';
    public static Boolean toUpdatePriceRecord = false;
    public static Map < String, OrderItem > orderItemMap = new Map < String, OrderItem > ();
    private static final string CLASSNAME = 'EP_PricingHandler';
    private static final string PRICING_ENGINE = 'PRICING_ENGINE';
    private static final string PRICING_EXCEPTION = 'Pricing Exception';
    private static final string PRICING_STATUS_FAILURE = 'FAILURE';
    private static final string PRICING_STATUS_PRICED = 'PRICED';
    private static final String strStatus = 'priceDetails'; //Temporary- Needs to add in Common Constant
    //Defect 43922 Start
    private static Id currentPriceBookId;
    private static String currentCurrency = EP_Common_Constant.BLANK;
    //Defect 43922 Start
    /*
    Description: Wrapper generated from JSON for response
    */
    public without sharing class PRICINGwrapper {
        public List < cls_priceDetails > priceResp;
        public EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon;
    }
    //PRICINGwrapper.priceResp.priceLineItems.orderId/lineItem
    class cls_priceDetails {
        public String seqId; //seqId
        public String priceRequestSource; //priceRequestSource
        public String companyCode; //companyCode
        public String priceType; //Fuel Price
        public String priceDate; //2001-01-01
        public String customerId; //customerId
        public String shipToId; //shipToId
        public String supplyLocationId; //supplyLocationId
        public String transporterId; //transporterId
        public cls_priceLineItems priceLineItems;
        public cls_error error;
    }
    class cls_priceLineItems {
        public String orderId; //orderId
        public List < cls_lineItem > lineItem;
    }
    class cls_lineItem {
        public cls_lineItemInfo lineItemInfo;
        public cls_invoiceDetails invoiceDetails;
        public cls_accountingDetails accountingDetails;
    }
    class cls_lineItemInfo {
        public String lineItemId; //lineItemId
        public String itemId; //itemId
        public String quantity; //quantity
        public string unitPrice;
    }
    class cls_invoiceDetails {
        public cls_invoiceComponents invoiceComponents;
    }
    class cls_invoiceComponents {
        public List < cls_invoiceComponent > invoiceComponent;
    }
    class cls_invoiceComponent {
        public String type; //Fuel
        public String name; //name
        public String amount; //0.0
        public String taxPercentage; //0.0
        public String taxAmount; //0.0
        public String totalAmount;//new tag added
    }
    class cls_accountingDetails {
        public cls_acctComponents acctComponents;
    }
    class cls_acctComponents {
        public List < cls_acctComponent > acctComponent;
    }
    class cls_acctComponent {
        public String componentCode; //glPostingGroup
        public String glAccountNr;
        public String baseAmount; //0.0
        public String taxRate; //0.0
        public String taxAmount; //0.0
        public String totalAmount; //0.0
        public String isVAT; //Yes
    }
    class cls_error {
        public String errorCode;
        public String errorDescription;
    }
    /*
        parser method
        */
        public static PRICINGwrapper parse(String jsonReq) {
            EP_GeneralUtility.Log('Public','EP_PricingHandler','parse');
            System.debug('[[[[[[[[[[[[[[[[' + jsonReq);
            PRICINGwrapper PRICINGwrapper = new PRICINGwrapper();
            PRICINGwrapper.priceResp = new list < cls_priceDetails > ();
            try {
                if (string.isNotBlank(jsonReq)) {
                    JSONParser parser = JSON.createParser(jsonReq);
                //PARSE JSON REQUEST
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONTOKEN.FIELD_NAME) {
                        system.debug('parser.getCurrentName()***' + parser.getCurrentName()); 
                        //GET MESSAGE ID TO BE USED AS CORRELATION ID IN ACK
                        if (EP_Common_Constant.HEADER_COMMON.equalsIgnoreCase(parser.getCurrentName())) {
                            parser.nextToken();
                            PRICINGwrapper.headerCommon = (EP_AcknowledmentGenerator.cls_HeaderCommon) parser.readValueAs(EP_AcknowledmentGenerator.cls_HeaderCommon.class);
                            parser.skipChildren();
                            System.debug('[[[[[[[[[[[[[[[[[[[[[[[' + PRICINGwrapper.headerCommon);
                            } else {
                                if (strStatus.equalsIgnoreCase(parser.getCurrentName())) {
                                    parser.nextToken();
                                //IF LIST OF CUSTOMERS
                                if (parser.getCurrentToken() == JSONTOKEN.START_ARRAY) {
                                    PRICINGwrapper.priceResp = (List < cls_priceDetails > ) parser.readValueAs(List < cls_priceDetails > .class);
                                }
                                //ELSE IF SINGLE CUSTOMER
                                else {
                                    if (parser.getCurrentToken() == JSONTOKEN.START_OBJECT) {
                                        PRICINGwrapper.priceResp.add((cls_priceDetails) parser.readValueAs(cls_priceDetails.class));
                                    }
                                }
                                parser.skipChildren();
                            }
                        }
                    }
                }
            }
            } catch (Exception handledException) {
                System.debug('====================' + handledException.getMessage());
            }
            return PRICINGwrapper;
        }
    /*
    Description: This method creates various maps from the JSON response
    */
    public static void UpdatePRICING(PRICINGwrapper PRICINGwrapper) {
        EP_GeneralUtility.Log('Public','EP_PricingHandler','UpdatePRICING');
        try
        {
          set < String > setOrderNo = new set < String > ();
          Map < string, cls_priceDetails > parseMsgMap = new Map < string, cls_priceDetails > ();
          Map < String, List < cls_lineItem >> lineItemMap = new Map < String, List < cls_lineItem >> ();
          String orderId;
          String errorDescr = EP_Common_Constant.BLANK;
          List < cls_invoiceComponent > invoiceCompList;
          system.debug(' ----- new MAP ------ ' + PRICINGwrapper.priceResp);
          for (cls_priceDetails prDet: PRICINGwrapper.priceResp) {
              parseMsgMap.put(prDet.seqId, prDet);
              sSeqId.add(prDet.seqId);
              orderRecId = prDet.seqId.subString(0,18);
               //Defect 29541 Start
               if(prDet.error.errorDescription <> null && prDet.error.errorDescription <> ''){
                  hasError = true;
              }
              errorDescr = prDet.error.errorDescription;
              //Defect 29541 End
          }
          for (cls_priceDetails WrrprRec: PRICINGwrapper.priceResp) {
              
              //System.debug('[[[[[[[[[[[[[[[[' + WrrprRec.priceLineItems.lineItem);
              orderId = WrrprRec.priceLineItems.orderId;
              if(!hasError){
                  lineItemMap.put(orderId, WrrprRec.priceLineItems.lineItem);
                  for (cls_lineItem pLiWrap: WrrprRec.priceLineItems.lineItem) {
                      //system.debug(''+)
                      if (lineMapRec.containsKey(orderId))
                      lineMapRec.get(orderId).add(pLiWrap.lineItemInfo);
                      else {
                          lineItemLst = new List < cls_lineItemInfo > ();
                          lineItemLst.add(pLiWrap.lineItemInfo);
                          lineMapRec.put(WrrprRec.priceLineItems.orderId, lineItemLst);
                      }
                      lineIdInvoiceDetailMap.put(pLiWrap.lineItemInfo.lineItemId, pLiWrap.invoiceDetails.invoiceComponents.invoiceComponent);
                      lineIdAcctDetailMap.put(pLiWrap.lineItemInfo.lineItemId, pLiWrap.accountingDetails.acctComponents.acctComponent);
                  }
              }
          }
          system.debug(' ----- new MAP CHECK ------ ' + lineMapRec);
          if(!hasError){
              orderLineUpsert();
              
          }
          else{
              updateErrorDetails(errorDescr);
          }
      }
      catch( Exception e )
      {     
        system.debug('Exception is ****' + e);
        processStatus = EP_Common_Constant.FAILURE;
        lDataSets.add(createDataSet(PRICING_ENGINE, EP_Common_Constant.BLANK, PRICING_EXCEPTION, e.getMessage()));
            //Defect 30391 Start - Updated
            //updateErrorDetails( e.getMessage() );
            //Defect 30391 End - Updated
        }
        sendAcknowledgement(PRICINGwrapper);
    }
    
    public static void updateErrorDetails(String err){
        EP_GeneralUtility.Log('Public','EP_PricingHandler','updateErrorDetails');
        processStatus = EP_Common_Constant.FAILURE;
        List<OrderItem> updateOrderItemList = new List<OrderItem>();
        List<OrderItem> oiList = new List<OrderItem>();
        oiList = [Select id,description from OrderItem where OrderId =: orderRecId];
        //oiList[0].EP_Pricing_Error__c = err.subString(1,40);
        try{//Defect 30391 Start End
            if(!oiList.isEmpty()){
                for(OrderItem oi : oiList){
                    if(err.length() > 240){
                        oi.EP_Pricing_Error__c = err.subString(0,240);
                        updateOrderItemList.add(oi);
                    }
                    else
                    oi.EP_Pricing_Error__c = err; 
                }
            }   
            if(!updateOrderItemList.isEmpty()){ 
                update updateOrderItemList; 
            }               
            Order o = new Order(id=orderRecId, EP_Pricing_Status__c = PRICING_STATUS_FAILURE);
        //if(updateError)
        o.EP_Error_Description__c =  err;
        update o;
        
        list<EP_Pricing_Engine__c> priceRec = [Select id, Name, EP_Pricing_In_Progress__c, EP_Sequence_Id__c from EP_Pricing_Engine__c where EP_Order__c =: orderRecId and EP_Pricing_In_Progress__c = true limit 1];
        if( !priceRec.isEmpty() )
        {
          priceRec[0].EP_Pricing_In_Progress__c = false;
          update priceRec;
      }
        //Defect 30391 Start
    }
    catch(Exception ex){
        throw ex;
        }//Defect 30391 End    
    }
    
    public static void orderLineUpsert() {
        EP_GeneralUtility.Log('Public','EP_PricingHandler','orderLineUpsert');
        
        Set < String > oItemIds = new Set < String > ();
        EP_AcknowledmentGenerator.cls_dataset dataSet;
        String test = EP_Common_Constant.BLANK;
        OrderItem oItemUpsert;
        String key = EP_Common_Constant.BLANK;
        List<OrderItem> childItemDelList = new List<OrderItem>();
        Map < String, String > accTagMap = new Map < String, String > ();
        Database.UpsertResult[] srList;
        Map < String, String > headerMap = new Map < String, String > ();
        List < OrderItem > oItemUpsertList = new List < OrderItem > ();
        List < OrderItem > oItemUpsertList1 = new List < OrderItem > ();
            //List < EP_PortalOrderPageController1.OrderWrapper > aOrderWrapper1 = new List < EP_PortalOrderPageController1.OrderWrapper > ();
            //EP_PortalOrderPageController1.OrderWrapper ordWrapper;
            Set < Id > priceBookIds = new Set < Id > ();
            Savepoint sp = Database.setSavepoint();
            try {
              Boolean isImportedOrder = false;
              for (OrderItem oItem: [Select id, EP_Is_Standard__c, EP_OrderItem_Number__c, Order.EP_Is_Imported_Order__c ,OrderId, Quantity, unitPrice, order.Pricebook2Id, OrderItemNumber, PriceBookEntryId, order.currencyisoCode, EP_Product_Code__c from OrderItem where Order.OrderNumber IN: lineMapRec.keySet()]) {
                orderItemMap.put(String.valueOf(oItem.EP_OrderItem_Number__c), oItem);
                priceBookIds.add(oItem.order.Pricebook2Id);
                    //Defect 43922 Start
                    currentPriceBookId = oItem.order.Pricebook2Id;
                    currentCurrency = oItem.order.CurrencyIsoCode;
                    //Defect 43922 End
                    if( oItem.Order.EP_Is_Imported_Order__c )
                    isImportedOrder = true;
                    if(!oItem.EP_Is_Standard__c )
                    childItemDelList.add( oItem);
                }
                // Defect #29782 end
                if(!childItemDelList.isEmpty()){
                    delete childItemDelList;
                }
                for (PriceBookEntry pE: [Select id, pricebook2.id, product2.name, currencyisoCode from pricebookentry where pricebook2.id IN: priceBookIds]) {
                    key = pE.product2.name + pE.currencyisoCode + pE.pricebook2.id;
                    peMap.put(key, pE.id);
                }
                System.debug('::::::::::::::' + orderItemMap);
                System.debug('<<<<<<<<<<<<<<' + lineMapRec);
                System.debug('------------------' + peMap);
                for (String ordId: lineMapRec.keySet()) {
                    for (cls_lineItemInfo liInfo: lineMapRec.get(ordId)) {                
                        oItemUpsert = new OrderItem();
                        test = liInfo.itemId;
                        oItemUpsert.id = orderItemMap.get(liInfo.lineItemId).id;
                        oItemUpsert.Quantity = Integer.valueOf(liInfo.quantity.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));
                        oItemUpsert.UnitPrice = 0;
                        oItemUpsert.EP_Pricing_Response_Unit_Price__c = Decimal.valueOf(liInfo.unitPrice.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));

                        if(!hasError){                                          
                            if (lineIdInvoiceDetailMap.containsKey(liInfo.lineItemId)) {
                                //Defect 43922 Start                              
                                checkForInvoiceComponents(lineIdInvoiceDetailMap.get(liInfo.lineItemId), oItemUpsert, liInfo.lineItemId);//new code

                                //Defect 43922 Start
                                oItemUpsertList.addAll(invoiceComponentLineItem(lineIdInvoiceDetailMap.get(liInfo.lineItemId), oItemUpsert, liInfo.lineItemId));
                            }
                            if (lineIdAcctDetailMap.containsKey(liInfo.lineItemId)) {
                                acctComponentTag(lineIdAcctDetailMap.get(liInfo.lineItemId));
                            }
                        }
                        oItemUpsert.EP_Accounting_Details__c = accTagString;
                        oItemUpsertList.add(oItemUpsert);
                        system.debug('--List--'+oItemUpsertList);
                        System.debug('ppppppppppppppppppppp' + accTagString);
                    }
                }
                
                if (!oItemUpsertList.isEmpty()) {
                    //upsert oItemUpsertList;
                    srList = database.upsert(oItemUpsertList, true);
                }
                
                for (Database.upsertResult uR: srList) {
                    if (uR.isSuccess()) {
                        toUpdatePriceRecord = true;
                        for (String seqId: sSeqId) {
                            lDataSets.add(createDataSet(PRICING_ENGINE, seqId, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK));
                        }
                        } else {
                            for (Database.Error err: uR.getErrors()) {
                                for (String seqId: sSeqId) {
                                    lDataSets.add(createDataSet(PRICING_ENGINE, seqId, String.valueOf(err.getStatusCode()), err.getMessage()));
                                }
                            }
                        }
                    }
                    
                //to update the pricing record after successfull parsing of response from Pricing Engine
                if (toUpdatePriceRecord) {
                    
                    for (EP_Pricing_Engine__c priceRec: [Select id, Name, EP_Pricing_In_Progress__c, EP_Sequence_Id__c from EP_Pricing_Engine__c where EP_Sequence_Id__c In: sSeqId and EP_Pricing_In_Progress__c = true LIMIT :EP_Common_Constant.ONE]) {
                        priceRec.EP_Pricing_In_Progress__c = false;
                        update priceRec;
                    }
                    
                    Order o = new Order(id=orderRecId, EP_Pricing_Status__c = PRICING_STATUS_PRICED,EP_Error_Description__c=''); // Defect:44999
                    if( isImportedOrder )
                    {
                        o.Status = EP_Common_Constant.SUBMITTED;
                    }
                    update o;
                    // Defect #29782 end
                }
                } catch (Exception e) {
                    database.rollback(sp);
                    processStatus = EP_Common_Constant.FAILURE;
                    for (String seqId: sSeqId) {
                        lDataSets.add(createDataSet(PRICING_ENGINE, seqId, PRICING_EXCEPTION, e.getMessage()));
                    }
                    updateErrorDetails( e.getMessage() );
                }
            }
    //Defect 43922 Start
    public static Map<String,Product2> newProductMap = new Map<String,Product2>();
    public static List<PriceBookEntry> priceBookEntryList = new List<PriceBookEntry>();
    public static String key_new = EP_Common_Constant.BLANK;
    public static Map<String,Product2> productNameIdMap = new Map<String,Product2>();     
    private static void checkForInvoiceComponents(List<cls_invoiceComponent> clsList, OrderItem oi, String s){
        EP_GeneralUtility.Log('Private','EP_PricingHandler','checkForInvoiceComponents');
        Set<String> invoiceComponentNameSet = new Set<String>(); 
        Integer nRows = EP_Common_Util.getQueryLimit();
        Set<String> totalInvoiceComponents = new Set<String>();
        List<Product2> productsForWhichPBEntryToBeCreated = new List<Product2>();
        System.debug('-----------'+lineIdInvoiceDetailMap+'-----------'+s);
        for(cls_invoiceComponent invoiceCompInPriceBook : lineIdInvoiceDetailMap.get(s)){
            if(invoiceCompInPriceBook.type <> null){
                invoiceComponentNameSet.add(invoiceCompInPriceBook.type);                
            }
        }
        System.debug('%%%%%%%%%%%%%%%%'+invoiceComponentNameSet);
        List<Product2> productList = new List<Product2>();
        List<PriceBookEntry> pBEntry = new List<PriceBookEntry>();
        productList = [Select id, name, EP_Is_System_Product__c from Product2 where EP_Is_System_Product__c = TRUE LIMIT: nRows];
        for(Product2 prod : productList){
            //creating the temporary key to check if the Price Book Entry exists for the current pricebook or not
            String tempKey = prod.name + currentCurrency + currentPriceBookId;
            //check if invoice component exists as product and its PB Entry also exists
            System.debug('888888888888888888'+invoiceComponentNameSet+'99999999999999999'+peMap.containsKey(tempKey)+'777777777777'+invoiceComponentNameSet.contains(prod.name));
            if(invoiceComponentNameSet.contains(prod.name) && peMap.containsKey(tempKey)){
                invoiceComponentNameSet.remove(prod.name);
                //productNameIdMap.put(prod.name,prod);
            }//check if product exists but PB Entry does not exists in the current pricebook
            else if(invoiceComponentNameSet.contains(prod.name) && !peMap.containsKey(tempKey)){
               productsForWhichPBEntryToBeCreated.add(prod);
                 //Defect 44404 Start
                 invoiceComponentNameSet.remove(prod.name);
                 //Defect 44404 End 
             }
        }   //check if the product does not exist then create new product with entries in pricebook 
        if(!invoiceComponentNameSet.isEmpty()){
            createNewProducts(invoiceComponentNameSet);
        }
        System.debug('%%%%%%%%%%%%%%%%'+invoiceComponentNameSet);
        System.debug('##############'+productNameIdMap);
        System.debug('$$$$$$$$$$$$$$$$$$$invoiceComponentNameSet$$$'+invoiceComponentNameSet);
        if(!productsForWhichPBEntryToBeCreated.isEmpty()){
            createNewPricBookEntries(productsForWhichPBEntryToBeCreated);
        }
    }   
    
    private static void createNewProducts(Set<String> productString){
        EP_GeneralUtility.Log('Private','EP_PricingHandler','createNewProducts');
        Product2 prod = new Product2();
        List<Product2> newProductList = new List<Product2>();
        for(String s : productString){
            prod = new Product2();
            prod.Name = s;
            prod.IsActive = True;
            prod.EP_Is_System_Product__c = True;
            newProductList.add(prod);
        }//insert the new products 
        if(!newProductList.isEmpty()){
            Database.insert(newProductList);
        }
        System.debug('---------------'+newProductList);
        //create new pricebookentry for the products inserted
        createNewPricBookEntries(newProductList);
    }
    private static void createNewPricBookEntries(List<Product2> productsList){
        EP_GeneralUtility.Log('Private','EP_PricingHandler','createNewPricBookEntries');
        PriceBookEntry pE = new PriceBookEntry();
        Integer nRows = EP_Common_Util.getQueryLimit();
        Set<Id> pbEntryId = new Set<Id>();
        String key2 = EP_Common_Constant.BLANK;
        for(Product2 product : productsList){
            pE = new PriceBookEntry();
            pE.PriceBook2Id = currentPriceBookId;
            pE.Product2Id = product.id; 
            pE.isActive = true;
            pE.UnitPrice = 0.0;
            pE.EP_Is_Sell_To_Assigned__c = True;
            pE.currencyIsoCode = currentCurrency;
            priceBookEntryList.add(pE);
        }
        if(!priceBookEntryList.isEmpty()){
            Database.insert(priceBookEntryList);
            for(PriceBookEntry pbEntry: priceBookEntryList){
                pbEntryId.add(pbEntry.id);
            }
        }
        System.debug('@@@@@@@@@@@@@@'+priceBookEntryList);
        for(PriceBookEntry pbEntry : [Select id, PriceBook2Id, Product2Id, Product2.Name, CurrencyIsoCode from PriceBookEntry where id In: pbEntryId Limit: nRows]){
            key2 = pbEntry.product2.name+ pbEntry.currencyisoCode + pbEntry.Pricebook2Id;
            peMap.put(key2,pbEntry.id);
        }
        System.debug('@@@@@@@@@@@@@@'+peMap);
    }
    //Defect 43922 Start
        /*
           Description: create invoice component line items
           */
           private static List < OrderItem > invoiceComponentLineItem(List < cls_invoiceComponent > invLineItems, OrderItem oi, String s) {
            EP_GeneralUtility.Log('Private','EP_PricingHandler','<');
            List < OrderItem > invItemList = new List < OrderItem > ();
            System.debug('*******************'+lineIdInvoiceDetailMap);
            for (cls_invoiceComponent comp: lineIdInvoiceDetailMap.get(s)) {
                invOrderItem = new OrderItem();
                invOrderItem.EP_Parent_Order_Line_Item__c = oi.id;
                invOrderItem.OriginalOrderItemId = oi.id;
                invOrderItem.quantity = oi.quantity;
                invOrderItem.UnitPrice = Decimal.valueOf(comp.totalAmount.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));//Decimal.valueOf(comp.amount);
                invOrderItem.orderid = orderItemMap.get(s).orderId;
                invOrderItem.EP_Tax_Amount__c = Decimal.valueOf(comp.taxAmount);
                invOrderItem.EP_Invoice_Name__c = comp.name;
                invOrderItem.EP_Tax_Percentage__c = Decimal.valueOf(comp.taxPercentage);
                invOrderItem.EP_Pricing_Total_Amount__c = Decimal.valueOf(comp.amount.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));//comp.totalAmount;
                String key1 = comp.type + orderItemMap.get(s).order.currencyisoCode + orderItemMap.get(s).order.Pricebook2Id;
                system.debug('Key1--'+key1);
                invOrderItem.priceBookentryid = peMap.get(key1);
                invItemList.add(invOrderItem);
            }
            System.debug('$$$$$$$$$$$$$$$$$' + invOrderItem);
            return invItemList;
        }
        /*
        Description: create acct component tag
        */
        //Defect # 28275 Start
        public static String acctComponentTag(List < cls_acctComponent > accTags) {
            EP_GeneralUtility.Log('Public','EP_PricingHandler','acctComponentTag');
            accTagString = EP_Common_Constant.BLANK;
            
            for (cls_acctComponent accComp: accTags) {
                system.debug('---'+accComp.glAccountNr+((accComp.glAccountNr <> null)?accComp.glAccountNr:'') );
                accTagString += EP_Common_Constant.XML_ACC_COMPONENT_BEGIN;
                accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_COMPONENTCODE  + EP_Common_Constant.ANG_RIGHT + ((accComp.componentCode<> null)?accComp.componentCode:EP_Common_Constant.BLANK) + EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_COMPONENTCODE + EP_Common_Constant.ANG_RIGHT;
                accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_GLACCOUNT_NR + EP_Common_Constant.ANG_RIGHT + ((accComp.glAccountNr <> null)?accComp.glAccountNr:EP_Common_Constant.BLANK)  + EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_GLACCOUNT_NR + EP_Common_Constant.ANG_RIGHT;
                accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_BASEAMT + EP_Common_Constant.ANG_RIGHT + ((accComp.baseAmount <> null)?accComp.baseAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_BASEAMT + EP_Common_Constant.ANG_RIGHT;
                accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_TAXRATE + EP_Common_Constant.ANG_RIGHT + ((accComp.taxRate <> null)?accComp.taxRate :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_TAXRATE + EP_Common_Constant.ANG_RIGHT;
                accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_TAXAMOUNT + EP_Common_Constant.ANG_RIGHT + ((accComp.taxAmount <> null)?accComp.taxAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_TAXAMOUNT + EP_Common_Constant.ANG_RIGHT;
                accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.TOTALAMOUNT_TAG + EP_Common_Constant.ANG_RIGHT + ((accComp.totalAmount <> null)?accComp.totalAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.TOTALAMOUNT_TAG + EP_Common_Constant.ANG_RIGHT;
                accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_IS_VAT + EP_Common_Constant.ANG_RIGHT + ((accComp.isVAT <> null)?accComp.isVAT :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_IS_VAT + EP_Common_Constant.ANG_RIGHT;
                accTagString += EP_Common_Constant.XML_ACC_COMPONENT_CLOSE;
            //}  
            //}        
        }
        System.debug('%%%%%%%%%%%%%' + accTagString);
        return accTagString;
    }
    //Defect # 28275 End
    /*
     * Description: This method is created to send acknowledgement
     */
     private static void sendAcknowledgement(PRICINGwrapper PRICINGwrapper) {
        EP_GeneralUtility.Log('Private','EP_PricingHandler','sendAcknowledgement');
        string response = EP_Common_Constant.Blank;
        map < String, String > headerCommon = new map < String, String > ();
        String messageId = EP_Common_Constant.BLANK;
        EP_AcknowledmentGenerator.cls_dataset n;
        EP_Message_Id_Generator__c msgIdGenerator = new EP_Message_Id_Generator__c();
        database.insert(msgIdGenerator);
        Integer nRows = EP_Common_Util.getQueryLimit();
        msgIdGenerator = [Select Name From EP_Message_Id_Generator__c Where Id =: msgIdGenerator.Id Limit :nRows];
        
        messageId = EP_IntegrationUtil.getMessageId(EP_Common_Constant.SFD, EP_Common_Constant.GBL, EP_PROCESS_NAME_CS__c.getValues(EP_Common_Constant.SKU_CRTN_FRM_NAV).EP_Process_Name__c, DateTime.now(), MsgIdgenerator.name);
        //CREATE HEADER MAP
        headerCommon.put(EP_Common_Constant.MSG_ID, messageId);
        headerCommon.put(EP_Common_Constant.PROCESS_STATUS, processStatus);
        headerCommon.put(EP_Common_Constant.INTERFACE_TYPE, PRICINGwrapper.headerCommon.InterfaceType);
        headerCommon.put(EP_Common_Constant.SOURCE_GROUP_COMPANY, PRICINGwrapper.headerCommon.SourceGroupCompany);
        headerCommon.put(EP_Common_Constant.DESTINATION_GROUP_COMPANY, PRICINGwrapper.headerCommon.DestinationGroupCompany);
        headerCommon.put(EP_Common_Constant.SOURCE_COMPANY, PRICINGwrapper.headerCommon.SourceCompany);
        headerCommon.put(EP_Common_Constant.DESTINATION_COMPANY, PRICINGwrapper.headerCommon.DestinationCompany);
        headerCommon.put(EP_Common_Constant.CORR_ID, PRICINGwrapper.headerCommon.msgId);
        headerCommon.put(EP_Common_Constant.DESTINATION_ADDRESS, PRICINGwrapper.headerCommon.DestinationAddress);
        headerCommon.put(EP_Common_Constant.SOURCE_RESP_ADD, PRICINGwrapper.headerCommon.SourceResponseAddress);
        headerCommon.put(EP_Common_Constant.SOURCE_UPDATE_STTS_ADD, PRICINGwrapper.headerCommon.SourceUpdateStatusAddress);
        headerCommon.put(EP_Common_Constant.DESTINATION_UPDATE_STATUS_ADDRESS, PRICINGwrapper.headerCommon.DestinationUpdateStatusAddress);
        headerCommon.put(EP_Common_Constant.MIDDLEWARE_URL_FOR_PUSH, PRICINGwrapper.headerCommon.MiddlewareUrlForPush);
        headerCommon.put(EP_Common_Constant.EMAIL_NOTIFICATION, PRICINGwrapper.headerCommon.EmailNotification);
        headerCommon.put(EP_Common_Constant.ACK_ERROR_CODE, PRICINGwrapper.headerCommon.ErrorCode);
        headerCommon.put(EP_Common_Constant.ERROR_DESCRIPTION, PRICINGwrapper.headerCommon.ErrorDescription);
        headerCommon.put(EP_Common_Constant.PROCESSING_ERROR_DESCRIPTION, PRICINGwrapper.headerCommon.ProcessingErrorDescription);
        headerCommon.put(EP_Common_Constant.CONT_ON_ERR_STTS, PRICINGwrapper.headerCommon.ContinueOnError);
        headerCommon.put(EP_Common_Constant.COMP_LGNG, PRICINGwrapper.headerCommon.ComprehensiveLogging);
        headerCommon.put(EP_Common_Constant.TRANSPORT_STATUS, PRICINGwrapper.headerCommon.TransportStatus);
        headerCommon.put(EP_Common_Constant.UPDT_SRC_ON_RCV, PRICINGwrapper.headerCommon.UpdateSourceOnReceive);
        headerCommon.put(EP_Common_Constant.UPDT_SRC_ON_DLVRY, PRICINGwrapper.headerCommon.UpdateSourceOnDelivery);
        headerCommon.put(EP_Common_Constant.UPDT_SRC_AFTER_PRCSNG, PRICINGwrapper.headerCommon.UpdateSourceAfterProcessing);
        headerCommon.put(EP_Common_Constant.UPDT_DEST_ON_DLVRY, PRICINGwrapper.headerCommon.UpdateDestinationOnDelivery);
        headerCommon.put(EP_Common_Constant.CALL_DEST_FOR_PRCSNG, PRICINGwrapper.headerCommon.CallDestinationForProcessing);
        headerCommon.put(EP_Common_Constant.OBJECT_TYPE, PRICINGwrapper.headerCommon.ObjectType);
        headerCommon.put(EP_Common_Constant.OBJECT_NAME, PRICINGwrapper.headerCommon.ObjectName);
        headerCommon.put(EP_Common_Constant.COMMUNICATION_TYPE, PRICINGwrapper.headerCommon.CommunicationType);
        headerCommon.put(EP_Common_Constant.OBJECT_NAME, PRICINGwrapper.headerCommon.ObjectName);
        //CREATE ACK WITH HEADER
        System.debug('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' + lDataSets);
        response = EP_AcknowledmentGenerator.createacknowledgement(headerCommon, lDataSets, EP_Common_Constant.BLANK);
        RestContext.response.responseBody = blob.valueOf(response);
        System.debug('%%%%%%%%%%%%%%%%%RESPONSE=' + response);
    }

    /**
           This method creates dataset for acknowledgement
           **/
           public static EP_AcknowledmentGenerator.cls_dataset createDataSet(String Name, String seqId, String errorCode, String errorDescription) {
            EP_GeneralUtility.Log('Public','EP_PricingHandler','createDataSet');
            EP_AcknowledmentGenerator.cls_dataset dataSet;
            try {
                dataSet = new EP_AcknowledmentGenerator.cls_dataset();
                dataSet.name = name;
                dataSet.seqId = seqId;
                dataSet.errorCode = errorCode;
                dataSet.errorDescription = errorDescription;
                } catch (Exception e) {
            // exception handling logic
            EP_LoggingService.logHandledException(e,
                EP_Common_Constant.EPUMA,
                CREATEDATASET, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return dataset;
    }

}