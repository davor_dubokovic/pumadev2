/* 
  @Author <Spiros Markantonatos>
   @name <EP_ManageProductListViewControllerTest>
   @CreateDate <28/09/2016>
   @Description <This is the test class for EP_ManageProductListViewController> 
   @Version <1.0>
*/
@isTest
public with sharing class EP_ManageProductListViewControllerTest {
    
    private static Company__c testCompany;
    private static Account testSellToAccount;
    private static Pricebook2 testExistingPricebook;
    private static Product2 testProduct1;
    private static Product2 testProduct2;
    private static Product2 testProduct3;
    private static User testCSCUser;
    
    /*
    * @ Description: This method will create the test account records and the relevant pricebooks and products
    */
    private static void createTestAccountRecords() {
        // Create test CSC user
        Profile testCSCProfile = [SELECT ID FROM Profile WHERE Name = :EP_Common_Constant.CSC_AGENT_PROFILE];
        testCSCUser = EP_TestDataUtility.createUser(testCSCProfile.Id);
        Database.insert(testCSCUser);
        
        // Create a test company
        testCompany = EP_TestDataUtility.createCompany('xyz' + String.valueOf(Datetime.now().millisecond()));
        Database.insert(testCompany);
        
        System.RunAs(testCSCUser) {
            // Create a Sell-To account
            testSellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
            testSellToAccount.EP_Alternative_Payment_Method__c = 'Application in Currency';
            testSellToAccount.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;

            testSellToAccount.CurrencyIsoCode = 'GBP';
            Database.insert(testSellToAccount);
            
            // Activate the Sell-To account
            testSellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(testSellToAccount); 
            testSellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            testSellToAccount.EP_Puma_Company__c = testCompany.Id;
            Database.update(testSellToAccount); 
            
            // Create test products
            testProduct1 = new product2(Name = 'Diesel', CurrencyIsoCode = 'GBP', isActive = TRUE, Family = 'Hydrocarbon Liquid', EP_Company_Lookup__c = testCompany.Id);
            testProduct2 = new product2(Name = 'Unleaded', CurrencyIsoCode = 'GBP', isActive = TRUE, Family = 'Hydrocarbon Liquid', EP_Company_Lookup__c = testCompany.Id);
            // The 3rd product is not related to the same company as the test account
            testProduct3 = new product2(Name = 'Max 98', CurrencyIsoCode = 'GBP', isActive = TRUE, Family = 'Hydrocarbon Liquid', EP_Company_Lookup__c = NULL);
            
            // Update the attributes of the 2nd product so that it matches the filters
            testProduct2.ProductCode = 'VAL1';
            testProduct2.Family = 'VAL1';
            testProduct2.EP_Product_Sold_As__c = 'Packaged';
            
            List<Product2> testProductsForUpdate = new List<Product2>();
            testProductsForUpdate.add(testProduct1);
            testProductsForUpdate.add(testProduct2);
            Database.insert(testProductsForUpdate);
            
            // Create an existing Pricebook
            testExistingPricebook = new Pricebook2(Name = 'Existing Price Book' + String.valueOf(Datetime.now().millisecond()), IsActive = TRUE);
            Database.insert(testExistingPricebook);
            
            // Add a test product to the existing pricebook
            PricebookEntry testExistingPBE = new PricebookEntry(
                                                Pricebook2Id = testExistingPricebook.Id, Product2Id = testProduct1.Id,
                                                    UnitPrice = 12000, IsActive = TRUE, EP_Is_Sell_To_Assigned__c = TRUE);
            Database.insert(testExistingPBE);     
            
        }
    }
    
    /*
    * @ Description: This method will test that the controler can load the correct account details
    */
    @isTest(SeeAllData=true)
    static void testControlerInitialisationPricebookSearchAndBasicUpdate() {
        List<Account> testAccounts = new List<Account>();
        List<PricebookEntry> lPricebookEntries;
        PageReference ref;
        
        createTestAccountRecords();
        
        // Ensure that the test Account has been created
        System.assertNotEquals(NULL, testSellToAccount.Id);
        
        Test.startTest();
            System.RunAs(testCSCUser) {
                
                PageReference pageRef = Page.EP_ManageCustomerProductListsPage;
                Test.setCurrentPageReference(pageRef);
                System.currentPageReference().getParameters().put('recs', String.valueOf(testSellToAccount.Id));
                
                testAccounts.add(testSellToAccount);
                
                EP_ManageProductListViewController con = new EP_ManageProductListViewController(new ApexPages.StandardSetController(testAccounts));
                
                // Ensure that the filter is defaulted to the correct options
                System.assertEquals(2, con.getavailableFilterOptions.size());
                System.assertEquals('true', con.getfilterMethod());
                System.assertEquals(TRUE, con.blnSearchByProduct);
                
                // Ensure that we can change the filter method to a different value
                con.setfilterMethod('false');
                System.assertEquals('false', con.getfilterMethod()); // Now the controler is searching based on Product list (product filters are tested in separate method)
                System.assertEquals(FALSE, con.blnSearchByProduct);
                
                // Ensure that the controler was able to load 1 account
                System.assertEquals(1, con.lSelectedAccounts.size());
                
                // Ensure that the system can return the default pricebook
                String strStandardPricebook2Id = Test.getStandardPricebookId();
                System.assertEquals(strStandardPricebook2Id, con.standardPricebookRecord.Id);
                    
                // Ensure that the controler will create a filter product record to be used for the UI filter section
                System.assertNotEquals(NULL, con.filterProduct);
                
                // Ensure that the controler will initialise the maps that are used to determine which accounts the user has updated
                System.assertNotEquals(NULL, con.mapAccountProductListEntry);
                System.assertNotEquals(NULL, con.mapAccountProductPBE);
                System.assertNotEquals(NULL, con.lAccountProductRecords);
                
                // Retrieve the product list information for the selected account
                ref = con.retrieveAccountProductListInformation();
                
                // Ensure that the method is returning NULL
                System.assertEquals(NULL, ref);
                
                // The refresh method is used to rerender the UI and should always return NULL
                System.assertEquals(NULL, con.refresh());
                
                // Use the search method to return a pricebook based on name
                List<Pricebook2> lPricebookRecords = EP_ManageProductListViewController.getProductLists(testExistingPricebook.Name);
                
                // Ensure that the results contain the existing pricebook
                Boolean blnExistingPricebookFound = FALSE;
                
                for (Pricebook2 pb : lPricebookRecords)
                {
                    if (pb.Id == testExistingPricebook.Id)
                    {
                        blnExistingPricebookFound = TRUE;
                        break;
                    }
                }
                
                System.assertEquals(TRUE, blnExistingPricebookFound);
                
                // Simulate product list selection by the user
                con.strSelectedProductListName = testExistingPricebook.Name;
                con.strSelectedProductListID = testExistingPricebook.Id;
                
                // Search for products by product list
                ref = con.searchProducts();
                
                // Ensure that the method is returning NULL
                System.assertEquals(NULL, ref);
                
                // The search method must return 1 product (the one that was added as part of the test data initialisation method)
                System.assertEquals(1, con.lSelectedProducts.size());
                
                con.deselectAllFilteredProducts();
                
                // The returned product must be deselected
                System.assertEquals(FALSE, con.lSelectedProducts[0].Eligible_for_Rebate__c);
                
                con.selectAllFilteredProducts();
                
                // The returned product must be selected
                System.assertEquals(TRUE, con.lSelectedProducts[0].Eligible_for_Rebate__c);
                System.assertEquals(testSellToAccount.EP_Puma_Company__c, con.lSelectedProducts[0].EP_Company_Lookup__c);
                System.assertEquals(con.lSelectedAccounts[0].EP_Puma_Company__c, con.lSelectedProducts[0].EP_Company_Lookup__c);
                
                // Add the selected product to the Account
                con.addSelectedProductsToList();
                
                // Update the account record
                con.updateAccountProductLists();
                
                // The account should have a new pricebook assigned to it
                testSellToAccount = [SELECT Id, EP_PriceBook__c FROM Account WHERE Id = :testSellToAccount.Id];
                System.assertNotEquals(NULL, testSellToAccount.EP_PriceBook__c);
                
                // The new account pricebook should have exactly 1 entry
                lPricebookEntries = [SELECT Id FROM PricebookEntry WHERE Pricebook2Id = :testSellToAccount.EP_PriceBook__c];
                System.assertEquals(1, lPricebookEntries.size());
                
                // Reverse change and save
                con.removeSelectedProductsToList();
                con.updateAccountProductLists();
                
                // The new account pricebook should have no entries
                lPricebookEntries = [SELECT Id, IsActive FROM PricebookEntry WHERE Pricebook2Id = :testSellToAccount.EP_PriceBook__c];
                System.assertEquals(FALSE, lPricebookEntries[0].IsActive);
                
                // Add the product again and then reverse the change
                con.addSelectedProductsToList();
                con.removeSelectedProductsToList();
                
                // Ensure that the system will "reset" the selected account variable if required
                con.lSelectedAccounts = NULL;
                System.assertNotEquals(NULL, con.lSelectedAccounts);
                
            }
        Test.stopTest();
    }
    
    /*
    * @ Description: This method will test that the controler can search products by product attributes
    */
    @isTest(SeeAllData=true) // See all data is required as the test class must find the default pricebook which cannot be created
    static void testSearchByProductAttributeAndBasicUpdate() {
        List<Account> testAccounts = new List<Account>();
        List<PricebookEntry> lPricebookEntries;
        PageReference ref;
        
        createTestAccountRecords();
        
        // Ensure that the test Account has been created
        System.assertNotEquals(NULL, testSellToAccount.Id);
        
        Test.startTest();
            System.RunAs(testCSCUser) {
                PageReference pageRef = Page.EP_ManageCustomerProductListsPage;
                Test.setCurrentPageReference(pageRef);
                System.currentPageReference().getParameters().put('recs', String.valueOf(testSellToAccount.Id));
                
                testAccounts.add(testSellToAccount);
                
                EP_ManageProductListViewController con = new EP_ManageProductListViewController(new ApexPages.StandardSetController(testAccounts));
                
                // Load the account details
                con.retrieveAccountProductListInformation();
                
                // Set the product filter criteria to match Product #2
                con.filterProduct.ProductCode = testProduct2.ProductCode;
                con.filterProduct.Family = testProduct2.Family;
                con.filterProduct.EP_Product_Sold_As__c = testProduct2.EP_Product_Sold_As__c;
                con.filterProduct.Name = testProduct2.Name;
                
                // Search for matching products
                con.searchProducts();
                
                // Ensure that the controler has returned a single match
                System.assertEquals(1, con.lSelectedProducts.size());
                
                // Add and remove product to test that the change is reversed
                con.addSelectedProductsToList();
                con.removeSelectedProductsToList();
                
                // Because the original product is reversed, the system will capture a change
                System.assertEquals(0, con.lAccountProductRecords[0].intUpdatedNumberOfProducts);
                
                // Then add the product again
                con.addSelectedProductsToList();
                System.assertEquals(1, con.lAccountProductRecords[0].intUpdatedNumberOfProducts);
                
            }
        Test.stopTest();
    }   
    
    /*
    * @ Description: This method will test the scenario where the account has already a pricebook assigned to it, but the pricebook is empty/does not contain
        the products that the user is trying to add
    */
    @isTest(SeeAllData=true) // See all data is required as the test class must find the default pricebook which cannot be created
    static void testUpdateAccountWithExistingPricebook() {
        List<Account> testAccounts = new List<Account>();
        List<PricebookEntry> lPricebookEntries;
        PageReference ref;
        
        createTestAccountRecords();
        
        // Ensure that the test Account has been created
        System.assertNotEquals(NULL, testSellToAccount.Id);
        
        // Create a new pricebook
        Pricebook2 testNewPricebook = new Pricebook2(Name = 'New Price Book' + String.valueOf(Datetime.now().millisecond()), IsActive = TRUE);
        Database.insert(testNewPricebook);
        
        // Create standard price for the Product #1
      /*  PricebookEntry testStandardPBE = new PricebookEntry(Product2Id = testProduct1.Id, 
                                Pricebook2Id = Test.getStandardPricebookId(), 
                                    CurrencyIsoCode = 'GBP', 
                                        IsActive = TRUE,
                                            UnitPrice = 1,
                                                EP_Is_Sell_To_Assigned__c = TRUE,
                                                    UseStandardPrice = FALSE);
        
        Database.insert(testStandardPBE);*/
        
        // Assign Product #1 to the existing pricebook
        PricebookEntry testExistingPBE = new PricebookEntry(
                                                Pricebook2Id = testNewPricebook.Id, Product2Id = testProduct1.Id,
                                                    UnitPrice = 12000, IsActive = TRUE, EP_Is_Sell_To_Assigned__c = TRUE);
          Database.insert(testExistingPBE);    
        
        // Assign the new pricebook to the test account
        testSellToAccount.EP_PriceBook__c = testNewPricebook.Id;
        Database.update(testSellToAccount);
        
        Test.startTest();
            System.RunAs(testCSCUser) {
                PageReference pageRef = Page.EP_ManageCustomerProductListsPage;
                Test.setCurrentPageReference(pageRef);
                System.currentPageReference().getParameters().put('recs', String.valueOf(testSellToAccount.Id));
                
                testAccounts.add(testSellToAccount);
                
                EP_ManageProductListViewController con = new EP_ManageProductListViewController(new ApexPages.StandardSetController(testAccounts));
                
                // Load the account details
                con.retrieveAccountProductListInformation();
                
                // Set the product filter criteria to match Product #2
                con.filterProduct.ProductCode = testProduct2.ProductCode;
                con.filterProduct.Family = testProduct2.Family;
                con.filterProduct.EP_Product_Sold_As__c = testProduct2.EP_Product_Sold_As__c;
                
                // Search for matching products
                con.searchProducts();
                
                // Ensure that the controler has returned a single match
                System.assertEquals(1, con.lSelectedProducts.size());
                
                // Add and remove product to test that the change is reversed
                con.addSelectedProductsToList();
                
                // Update the account record
                con.updateAccountProductLists();
                
                // Remove Product #1 and #2 from the pricebook
                con = new EP_ManageProductListViewController(new ApexPages.StandardSetController(testAccounts));
                
                // Load the account details
                con.retrieveAccountProductListInformation();
                
                con.lSelectedProducts = new List<Product2>();
                con.lSelectedProducts.add(testProduct1);
                con.lSelectedProducts.add(testProduct2);
                con.removeSelectedProductsToList();
                
                // Update the account record
                con.updateAccountProductLists();
                
                // Generate an error to test the unexpected error behaviour of the controler
                con.lAccountProductRecords[0].productListItem = new Pricebook2(Name = NULL);
                String strMapKey = testSellToAccount.Id + 
                					EP_COMMON_CONSTANT.UNDERSCORE_STRING + 
                						testProduct1.Id + 
                							EP_COMMON_CONSTANT.UNDERSCORE_STRING + 
												testSellToAccount.CurrencyIsoCode;
                
                PricebookEntry testErrorPBE = new PricebookEntry(Id = con.mapAccountProductPBE.get(strMapKey).Id, Pricebook2Id = testNewPricebook.Id, Product2Id = NULL);
                con.mapAccountProductPBE.put(strMapKey, testErrorPBE);
                
                con.updateAccountProductLists();
                
            }
        Test.stopTest();
        
    }
    
    /*
    * @ Description: This method will test that the controler will return an error if not able to find the standard pricebook
    */
    testMethod static void testStandardPricebookError() {
        
        List<Account> testAccounts = new List<Account>();
        
        Test.startTest();
            
            PageReference pageRef = Page.EP_ManageCustomerProductListsPage;
            Test.setCurrentPageReference(pageRef);
            
            EP_ManageProductListViewController con = new EP_ManageProductListViewController(new ApexPages.StandardSetController(testAccounts));
            
            System.assertEquals(NULL, con.standardPricebookRecord);
            
        Test.stopTest();
    }
}