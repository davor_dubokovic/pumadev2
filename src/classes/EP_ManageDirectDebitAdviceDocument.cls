/**
 *  @Author <Kamal Garg>
 *  @Name <EP_ManageDirectDebitAdviceDocument>
 *  @CreateDate <25/08/2016>
 *  @Description <This is the apex class used to generate PDF for Direct Debit Advice record>
 *  @Version <1.0>
 */
public with sharing class EP_ManageDirectDebitAdviceDocument {

    private static final string CLASS_NAME = 'EP_ManageDirectDebitAdviceDocument';
    private static final string GENERATE_AT_METHOD = 'generateAttachmentForDirectDebitAdvice';
    private static final string DIRECT_DEBIT_ADVICE_TEMPLATE = 'EP_Direct_Debit_advice_notification';//Change Under L4
    /**
     * @author <Kamal Garg>
     * @name <generateAttachmentForDirectDebitAdvice>
     * @date <25/08/2016>
     * @description <This method is used to generate document for Direct Debit Advice>
     * @version <1.0>
     * @param EP_ManageDocument.Documents
     */
    public static void generateAttachmentForDirectDebitAdvice(EP_ManageDocument.Documents documents) {
        Map<String, List<Attachment>> directDebitAdviceAttachListMap = new Map<String, List<Attachment>>();
        //set containg 'AccountNumber' field values of 'Account' standard object received from Input JSON
        Set<String> acctNumSet = new Set<String>();
        try
        {	
            List<Attachment> directDebitAdviceAttachment = null;
            for(EP_ManageDocument.Document doc : documents.document) {
                EP_ManageDirectDebitAdviceDocument.DirectDebitAdviceWrapper directDebitAdviceWrap = EP_DocumentUtil.fillDirectDebitAdviceWrapper(doc.documentMetaData.metaDatas);
                String compositKey = directDebitAdviceWrap.issuedFromId + EP_Common_Constant.HYPHEN + directDebitAdviceWrap.billTo;
                acctNumSet.add(compositKey);
                
                Attachment att = EP_DocumentUtil.generateAttachment(doc);
                if(!(directDebitAdviceAttachListMap.containsKey(compositKey.toUpperCase()))) {
                    directDebitAdviceAttachment = new List<Attachment>();
                    directDebitAdviceAttachListMap.put(compositKey.toUpperCase(), directDebitAdviceAttachment);
                }
                directDebitAdviceAttachListMap.get(compositKey.toUpperCase()).add(att);
            }
            
            /** L4- chanage Start **/
            Map<String, Account> mapOfCompositeIdAccount = EP_DocumentUtil.getCompositIdAccountObjMap(acctNumSet, 
                        new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
            Map<Id, String> MapOfSFTdCompositId = new Map<Id, String>();
            /** L4- chanage End **/
            List<Attachment> attachments = new List<Attachment>();
            system.debug('---directDebitAdviceAttachListMap--'+directDebitAdviceAttachListMap);
            system.debug('---mapOfCompositeIdAccount--'+mapOfCompositeIdAccount);
            for(String key : directDebitAdviceAttachListMap.keySet()) {
                List<Attachment> attachList = directDebitAdviceAttachListMap.get(key);
                for(Attachment att : attachList) {
                    /** L4- chanage Start **/
                    att.ParentId = mapOfCompositeIdAccount.get(key).Id;
                    mapOfSFTdCompositId.put(att.ParentId, key);
                    /** L4- chanage End **/
                    attachments.add(att);
                }
            }
            
            EP_DocumentUtil.deleteAttachments(attachments);
            
            Database.SaveResult[] results = Database.insert(attachments, false);
            /** L4- chanage Start **/
            List<EmailTemplate> templates = new List<EmailTemplate>([SELECT Id, DeveloperName, Subject, Body, TemplateType 
                                                                     FROM EmailTemplate 
                                                                    where DeveloperName=:DIRECT_DEBIT_ADVICE_TEMPLATE]);
            /** L4- chanage End **/
            List<Messaging.SingleEmailMessage> mailToSend = new List<Messaging.SingleEmailMessage>();
            for (Integer i = 0; i < results.size(); i++) {
                
                if (results[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted attachment. Attachment ID: ' + results[i].getId());
                    EP_ManageDocument.successRecIdMap.put(attachments[i].Name, results[i].getId());
                    /** L4- chanage Start **/
                    Account accountObj = mapOfCompositeIdAccount.get(mapOfSFTdCompositId.get(attachments[i].ParentId));
                    If(!accountObj.Contacts.isEmpty() && !templates.isEmpty()){
                        mailToSend.add(createEmail(attachments[i], templates[0].Id, accountObj));
                    }
                    /** L4- chanage End **/
                } else {
                    // Operation failed, so get all errors
                    for(Database.Error err : results[i].getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Attachment fields that affected this error: ' + err.getFields());
                        EP_ManageDocument.errDesMap.put(attachments[i].Name, err.getMessage());
                        EP_ManageDocument.errCodeMap.put(attachments[i].Name, String.valueOf(err.getStatusCode()));
                    }
                }
            }
            /** L4- chanage Start **/
            system.debug('---mailToSend---'+mailToSend);
            if(!mailToSend.isEmpty()){
                Messaging.sendEmail(mailToSend);
            }
            /** L4- chanage End **/
        }
        catch(Exception e){
             EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, GENERATE_AT_METHOD , CLASS_NAME,apexPages.severity.ERROR);
             throw e;
        }
    }
    /** L4- chanage Start **/
    /**
    * @author       Accenture
    * @name         createEmail
    * @date         05/06/2017
    * @description  This method creates the email instance from Template ID , TargetObject as Contact and also adding attachment in mail
    * @param        Attachment, templateId, accountObj
    * @return       Messaging.SingleEmailMessag
    */
    private static Messaging.SingleEmailMessage createEmail(Attachment attach, Id templateId, Account accountObj){
        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
	    mail.setTargetObjectId(accountObj.Contacts[0].Id);
	    mail.setTemplateId(templateId);
	    mail.setWhatId(accountObj.Id);
	    mail.setSaveAsActivity(false);
	    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
	    efa.setFileName(attach.Name);
	    efa.setBody(attach.body);
	    mail.setFileAttachments(new Messaging.EmailFileattachment[]{efa});
	    return mail;
    }
    /** L4- chanage End **/
    /**
     *  @Author <Kamal Garg>
     *  @Name <DirectDebitAdviceWrapper>
     *  @CreateDate <25/08/2016>
     *  @Description <This is the wrapper class containing 'Direct Debit Advice' record information>
     *  @Version <1.0>
     */
    public with sharing class DirectDebitAdviceWrapper {
        public String billTo;
        public String seqId;
        public String docDate;
        public String paymentProposalNo;
        public String issuedFromId;
    }
    
}