/* 
   @Author 			Accenture
   @name 			EP_CustomerOtherAdjustmentHelper_UT
   @CreateDate 		02/08/2017
   @Description		Test class for helper class to handle the inbound customer adjustment interface
   @Version 		1.0
*/
@isTest
public class EP_CustomerOtherAdjustmentHelper_UT {
    @testSetup static void init() {
      List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    
    /**
	* @author 			Accenture
	* @name				setCustomerAdjustmentAttributes_test
	* @date 			02/08/2017
	* @description 		Test method used to set the customer adjustment attributes for inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
    static testMethod void setCustomerAdjustmentAttributes_test() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setCustomerAdjustmentAttributes(documents);
		Test.stopTest();
		System.assertEquals(true, document.SFCustomerOtherAdjustment.Name == document.identifier.docId);
	}
	
	/**
	* @author 			Accenture
	* @name				getCustomerAdjustmentSet_test
	* @date 			02/08/2017
	* @description 		Test method used to get the customer adjustment unique Ids from inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void getCustomerAdjustmentSet_test() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        Test.startTest();
		set<string> result = EP_CustomerOtherAdjustmentHelper.getCustomerAdjustmentSet(documents);
		Test.stopTest();
		System.assertEquals(true, result.size()>0);
	}
	
	/**
	* @author 			Accenture
	* @name				getCustomerIdSet_test
	* @date 			02/08/2017
	* @description 		Test method used to get the customer unique Ids from inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void getCustomerIdSet_test() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        Test.startTest();
		set<string> result = EP_CustomerOtherAdjustmentHelper.getCustomerIdSet(documents);
		Test.stopTest();
		System.assertEquals(true, result.size()>0);
	}
	
	/**
	* @author 			Accenture
	* @name				getPaymentTermSet_test
	* @date 			02/08/2017
	* @description 		Test method used to get the payment term unique Ids from inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void getPaymentTermSet_test() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        Test.startTest();
		set<string> result = EP_CustomerOtherAdjustmentHelper.getPaymentTermSet(documents);
		Test.stopTest();
		System.assertEquals(true, result.size()>0);
	}
	
	/**
	* @author 			Accenture
	* @name				setCustomerAdjustment_test1
	* @date 			02/08/2017
	* @description 		Test method used to set the customer adjustment record id in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setCustomerAdjustment_test1() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        
        Map<String,EP_Customer_Other_Adjustment__c> mapCustomerAdjustements = new Map<String,EP_Customer_Other_Adjustment__c>();
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setCustomerAdjustment(document,mapCustomerAdjustements);
		Test.stopTest();
		System.assertEquals(true, document.SFCustomerOtherAdjustment.Id==null);
	}
	
	/**
	* @author 			Accenture
	* @name				setCustomerAdjustment_test2
	* @date 			02/08/2017
	* @description 		Test method used to set the customer adjustment record id in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setCustomerAdjustment_test2() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
		EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
		
		EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(sellTo.Id,paymentTerm.Id,sellTo.EP_Puma_Company_Code__c);
		custAdjust.Name=document.identifier.docId;
		//L4 #45304 Changes Start
		custAdjust.EP_Entry_Number__c = document.identifier.entryNr;
		//L4 #45304 Changes End
		database.insert(custAdjust);
		
		EP_CustomerOtherAdjustmentMapper custAdjustMapper = new EP_CustomerOtherAdjustmentMapper();
		Set<String> setOfCustomerAdjustments = EP_CustomerOtherAdjustmentHelper.getCustomerAdjustmentSet(documents);
        Map<String,EP_Customer_Other_Adjustment__c> mapCustomerAdjustements = custAdjustMapper.getCustomerOtherAdjustmentMapByUniqueId(setOfCustomerAdjustments);
        
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setCustomerAdjustment(document,mapCustomerAdjustements);
		Test.stopTest();
		System.assertEquals(true, document.SFCustomerOtherAdjustment.Id == custAdjust.Id);
	}
	
	/**
	* @author 			Accenture
	* @name				setBillTo_test1
	* @date 			02/08/2017
	* @description 		Test method used to set the bill to record id in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setBillTo_test1() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		EP_AccountMapper accountMapper = new EP_AccountMapper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
		EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
		document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
		
		set<string> setOfCustomerIDs = EP_CustomerOtherAdjustmentHelper.getCustomerIdSet(documents);
		Map<String,Account> mapCompositeIDCustomers = accountMapper.getAccountsMapByCompositeKey(setOfCustomerIDs);
		
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setBillTo(document,mapCompositeIDCustomers);
		Test.stopTest();
		System.assertEquals(true, document.SFCustomerOtherAdjustment.EP_Bill_To__c==sellTo.Id);
	}
	
	/**
	* @author 			Accenture
	* @name				setBillTo_test2
	* @date 			02/08/2017
	* @description 		Test method used to set the bill to record id in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setBillTo_test2() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		EP_AccountMapper accountMapper = new EP_AccountMapper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
		
		set<string> setOfCustomerIDs = EP_CustomerOtherAdjustmentHelper.getCustomerIdSet(documents);
		Map<String,Account> mapCompositeIDCustomers = accountMapper.getAccountsMapByCompositeKey(setOfCustomerIDs);
		
		EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = '12312321';
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
		
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setBillTo(document,mapCompositeIDCustomers);
		Test.stopTest();
		System.assertEquals(true, document.errorDescription == EP_COMMON_CONSTANT.CUSTOMER.capitalize() +EP_Common_CONSTANT.SPACE +  document.identifier.billTo + EP_Common_CONSTANT.SPACE + EP_COMMON_CONSTANT.NOT_FOUND);
	}
	
	/**
	* @author 			Accenture
	* @name				setCustomerAdjustmentFields_test
	* @date 			02/08/2017
	* @description 		Test method used to set the customer adjustment fields in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setCustomerAdjustmentFields_test() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setCustomerAdjustmentFields(document);
		Test.stopTest();
		System.assertEquals(true, document.SFCustomerOtherAdjustment.Name == document.identifier.docId);
	}
	
	/**
	* @author 			Accenture
	* @name				setPaymentTerm_test1
	* @date 			02/08/2017
	* @description 		Test method used to set the payment term record id in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setPaymentTerm_test1() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		EP_PaymentTermMapper paymentTermMapper = new EP_PaymentTermMapper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id,EP_Payment_Term_Code__c from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
		EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.paymentTerm = string.valueOf(paymentTerm.EP_Payment_Term_Code__c);
		document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
		
		set<string> setOfPaymentTerms = EP_CustomerOtherAdjustmentHelper.getPaymentTermSet(documents);
		Map<String,Id> mapPaymentTermPaymentIDs = paymentTermMapper.getPaymentTermsMapByCodes(setOfPaymentTerms);
		
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setPaymentTerm(document,mapPaymentTermPaymentIDs);
		Test.stopTest();
		System.assertEquals(true, document.SFCustomerOtherAdjustment.EP_Payment_Term__c==paymentTerm.Id);
	}
	
	/**
	* @author 			Accenture
	* @name				setPaymentTerm_test2
	* @date 			02/08/2017
	* @description 		Test method used to set the payment term record id in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setPaymentTerm_test2() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		EP_PaymentTermMapper paymentTermMapper = new EP_PaymentTermMapper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id,EP_Payment_Term_Code__c from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
		
		set<string> setOfPaymentTerms = EP_CustomerOtherAdjustmentHelper.getPaymentTermSet(documents);
		Map<String,Id> mapPaymentTermPaymentIDs = paymentTermMapper.getPaymentTermsMapByCodes(setOfPaymentTerms);
		
		EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.paymentTerm = 'PQ';
        document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
		
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setPaymentTerm(document,mapPaymentTermPaymentIDs);
		Test.stopTest();
		System.assertEquals(true, document.errorDescription == EP_Common_CONSTANT.PYMNT_TRM + EP_Common_CONSTANT.SPACE +  document.paymentTerm + EP_Common_CONSTANT.SPACE + EP_COMMON_CONSTANT.NOT_FOUND);
	}
	
	/**
	* @author 			Accenture
	* @name				setPaymentTerm_test3
	* @date 			02/08/2017
	* @description 		Test method used to set the payment term record id in inbound customer adjustment request 
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setPaymentTerm_test3() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		EP_PaymentTermMapper paymentTermMapper = new EP_PaymentTermMapper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id,EP_Payment_Term_Code__c from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
		
		set<string> setOfPaymentTerms = EP_CustomerOtherAdjustmentHelper.getPaymentTermSet(documents);
		Map<String,Id> mapPaymentTermPaymentIDs = paymentTermMapper.getPaymentTermsMapByCodes(setOfPaymentTerms);
		
		EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.paymentTerm = '';
        document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
		
        Test.startTest();
		EP_CustomerOtherAdjustmentHelper.setPaymentTerm(document,mapPaymentTermPaymentIDs);
		Test.stopTest();
		System.assertEquals(true, document.SFCustomerOtherAdjustment.EP_Payment_Term__c==null);
	}
	
	//L4 #45304 Changes Start
	/**
	* @author 			Accenture
	* @name				setCustomerAdjustmentFields_test2
	* @date 			11/13/2017
	* @description 		Test method used to set the customer adjustment fields in inbound customer adjustment request when isReversal = 'No'
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void setCustomerAdjustmentFields_test2() {
		EP_CustomerOtherAdjustmentHelper localObj = new EP_CustomerOtherAdjustmentHelper();
		
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		
		EP_CustomerOtherAdjustmentStub stub = EP_TestDataUtility.createCustomerAdjustmentStub();
		List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
        EP_CustomerOtherAdjustmentStub.document document = documents[0];
        document.identifier.billTo = string.valueOf(sellTo.AccountNumber);
        document.identifier.clientId = string.valueOf(sellTo.EP_Puma_Company_Code__c);
        document.SFCustomerOtherAdjustment = new EP_Customer_Other_Adjustment__c();
        document.isReversal = EP_COMMON_CONSTANT.NO;
        
        Test.startTest();
			EP_CustomerOtherAdjustmentHelper.setCustomerAdjustmentFields(document);
		Test.stopTest();
		
		System.assertEquals( false, document.SFCustomerOtherAdjustment.EP_Is_Reversal__c);
	}
	//L4 #45304 Changes End
}