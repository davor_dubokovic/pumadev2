/*
    @Author          Accenture
    @Name            EP_OrderPricingRequestXML
    @CreateDate      04/18/2017
    @Description     This class  is used to generate outbound XML's of order Pricing request With NAV
    @Version         1.0
    @Reference       NA
*/
public class EP_OrderPricingRequestXML extends EP_GenerateOrderRequestXML{
    //Changes for #60147
    public dom.XmlNode PayloadNode;
   /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override string createXML(){
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createXML');

        string pricingPayloadXML = super.createXML();
        return pricingPayloadXML.unescapeXml();
    } 
    
    /**
    * @author           Accenture
    * @name             init
    * @date             04/18/2017
    * @description      This method is used to fetch data from Database which will be require to wrapped into XML
    * @param            NA
    * @return           NA
    */
    public override virtual void init(){
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','init');
        PayloadNode = doc.createRootElement(EP_OrderConstant.Payload, null, null);
        //MSGNode = doc.createRootElement(EP_OrderConstant.MSG, null, null);
        
        // Quering the Order object for the passed record id
     //   EP_OrderMapper ordermapper = new EP_OrderMapper();

       // orderobj = ordermapper.getRecordById(recordid);  
        
        // Initiating orderlines wrapper data
       // setOrderLineItems();
        
        // set order items
       // setOrderItems();
        
        // Get relevant custom setting
        isEncryptionEnabled = false;
        EP_CS_OutboundMessageSetting__c outboundMsgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
        if(outboundMsgSetting != null){
            isEncryptionEnabled = outboundMsgSetting.Payload_Encoded__c;
        }
    }
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */ 
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createPayload');
       
        DateTime dt = DateTime.now();
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode pricingRequestNode = tempDoc.createRootElement(EP_OrderConstant.pricingRequest,null, null);   
        Dom.XMLNode requestHeaderNode = pricingRequestNode.addChildElement(EP_OrderConstant.requestHeader,null,null);
        CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();

        String onRun =order.run;
        String onOffRun;
        system.debug('onRun is :' +onRun);
        if(onRun!= null && onRun== 'On'){
            onOffRun= 'True';
        }else if(onRun== 'Off'){
            onOffRun= 'False';
        }else{
            onOffRun= '';
        }
        

        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, csOrderSetting.Pricing_Message_Id__c); // strict format
        requestHeaderNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(csOrderSetting.Pricing_Sequence_Id__c); //Value for seqId
        
        try {
            requestHeaderNode.addChildElement(EP_OrderConstant.companyCode,null,null).addTextNode(order.lineItems[0].companyCode); //Value for companyCode
            requestHeaderNode.addChildElement(EP_OrderConstant.priceType,null,null).addTextNode('Fuel Price'); //Value for priceType // strict value
            requestHeaderNode.addChildElement(EP_OrderConstant.priceRequestSource,null,null).addTextNode('');
            requestHeaderNode.addChildElement(EP_OrderConstant.currencyCode,null,null).addTextNode(order.currencyCode);
            requestHeaderNode.addChildElement(EP_OrderConstant.deliveryType,null,null).addTextNode(order.deliveryType); //Value for deliveryType
            requestHeaderNode.addChildElement(EP_OrderConstant.customerId,null,null).addTextNode(order.accountNumber); //Value for customerId
            requestHeaderNode.addChildElement(EP_OrderConstant.shipToId,null,null).addTextNode(order.shipTo); //Value for shipToId
            requestHeaderNode.addChildElement(EP_OrderConstant.supplyLocationId,null,null).addTextNode(order.supplyLocation); //Value for supplyLocationId //41113
            requestHeaderNode.addChildElement(EP_OrderConstant.transporterId,null,null).addTextNode(order.transporterId); //Value for transporterId
            requestHeaderNode.addChildElement(EP_OrderConstant.supplierId,null,null).addTextNode(order.supplierId);
            
            requestHeaderNode.addChildElement(EP_OrderConstant.priceDate,null,null).addTextNode(order.requestDate); //Value for priceDate // must be present
            requestHeaderNode.addChildElement(EP_OrderConstant.onRun,null,null).addTextNode(onOffRun); //Value for onRun
            requestHeaderNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode(''); //Value for order id
            requestHeaderNode.addChildElement(EP_OrderConstant.applyOrderQuantity,null,null).addTextNode(''); //Value for totalOrderQuantity
            requestHeaderNode.addChildElement(EP_OrderConstant.totalOrderQuantity,null,null).addTextNode(order.quantity); //Value for totalOrderQuantity
            requestHeaderNode.addChildElement(EP_OrderConstant.priceConsolidationBasis,null,null).addTextNode('Summarised price'); /// strict value
            requestHeaderNode.addChildElement(EP_OrderConstant.versionNr,null,null).addTextNode(dt.format('yyyyMMdd\'T\'hhmmss') + '.' + dt.millisecondGmt());


            Dom.XMLNode requestLinesNode = pricingRequestNode.addChildElement(EP_OrderConstant.requestLines,null,null);
        
            for(EP_OrderDataStub.LineItem orderLine: order.lineItems){
                Dom.XMLNode lineNode = requestLinesNode.addChildElement(EP_OrderConstant.line,null,null);
               lineNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(csOrderSetting.Pricing_Sequence_Id__c);
                lineNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode('');
                lineNode.addChildElement(EP_OrderConstant.lineItemId,null,null).addTextNode(orderLine.lineItemId); // '0012300564'
                lineNode.addChildElement(EP_OrderConstant.itemId,null,null).addTextNode(orderLine.productCode); //Value for itemId  // strict format and value 2000005
                lineNode.addChildElement(EP_OrderConstant.quantity,null,null).addTextNode(orderLine.quantity); //Value for quantity
                lineNode.addChildElement(EP_OrderConstant.deliveryType,null,null).addTextNode(order.deliveryType);
                lineNode.addChildElement(EP_OrderConstant.shipToId,null,null).addTextNode(order.shipTo);
                lineNode.addChildElement(EP_OrderConstant.supplyLocationId,null,null).addTextNode(order.supplyLocation);
                lineNode.addChildElement(EP_OrderConstant.onRun,null,null).addTextNode(onOffRun);
                lineNode.addChildElement(EP_OrderConstant.priceDate,null,null).addTextNode(order.requestDate);
            }

            //end of request lines node 
            Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_OrderConstant.any0,null,null);
            // Encoding payload by calling encode XML method in superclass
            AnyNode.addTextNode(encodeXML(tempDoc));
        } catch (Exception e) {
            System.Debug('Failed to construct Order Pricing Request XML. e = ' + e.getMessage());
        }
    }
    
    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create Status XML tags
    * @param            NA
    * @return           NA
    */ 
    public override void createStatusPayLoad(){
        //#60147 User Story Start
        //Since for pricing Message Node and Status Node is not part of the XML Message hance override this method with empty implemetation 
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createStatusPayLoad');
        //MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null);
        //#60147 User Story End
    }
    
    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create Status XML tags
    * @param            NA
    * @return           NA
    */ 
    public override void createHeaderNode(){
        //#60147 User Story Start
        //Since for pricing Message Node and Status Node is not part of the XML Message hance override this method with empty implemetation 
        EP_GeneralUtility.Log('Public','EP_OrderPricingRequestXML','createHeaderNode');
        //MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null);
        //#60147 User Story End
    }
	
}