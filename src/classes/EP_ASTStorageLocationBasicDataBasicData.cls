/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationBasicDataBasicData>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 02-Basic Data Setup to 02-BasicDataSetup>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationBasicDataBasicData extends EP_AccountStateTransition {

    public EP_ASTStorageLocationBasicDataBasicData () {
        finalState = EP_AccountConstant.BASICDATASETUP;
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBasicDataBasicData',' isGuardCondition');        
        return true;
    }
}