/* 
   @Author          Accenture
   @name            EP_CustomerOtherAdjustmentHandler
   @CreateDate      02/08/2017
   @Description     Handler class to handle the inbound customer adjustment interface
   @Version         1.0
*/
public class EP_CustomerOtherAdjustmentHandler extends EP_InboundHandler {
    
    /**
    * @author           Accenture
    * @name             processRequest
    * @date             02/08/2017
    * @description      Method used to process the inbound customer adjustment request and sends back the response
    * @param            string
    * @return           string
    */
    public override string processRequest(string jsonRequest){ 
        EP_GeneralUtility.Log('Public','EP_CustomerOtherAdjustmentHandler','processRequest');
        string jsonResponse ='';
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        List<EP_AcknowledgementStub.dataset> ackCreateDatasets = new List<EP_AcknowledgementStub.dataset>();
        List<EP_AcknowledgementStub.dataset> ackUpdateDatasets = new List<EP_AcknowledgementStub.dataset>();
        List<EP_CustomerOtherAdjustmentStub.document> listOfDocumentsToBeCreated = new List<EP_CustomerOtherAdjustmentStub.document>();
        List<EP_CustomerOtherAdjustmentStub.document> listOfDocumentsToBeUpdated = new List<EP_CustomerOtherAdjustmentStub.document>();
        EP_MessageHeader HeaderCommon = new EP_MessageHeader();
         try {
            EP_CustomerOtherAdjustmentStub stub = parse(jsonRequest);
            HeaderCommon = stub.MSG.HeaderCommon;
            List<EP_CustomerOtherAdjustmentStub.document> documents = stub.MSG.payload.any0.documents.document;
            
            for(EP_CustomerOtherAdjustmentStub.document doc : documents) {
	            if(EP_Common_Constant.CREATE.equalsIgnoreCase(doc.docState)){            
		            listOfDocumentsToBeCreated.add(doc);
	            } else if(EP_Common_Constant.APPLICATION.equalsIgnoreCase(doc.docState)){           
	                listOfDocumentsToBeUpdated.add(doc);
	            }
            }
	        ackCreateDatasets = processDocuments(listOfDocumentsToBeCreated);
            ackUpdateDatasets = processDocuments(listOfDocumentsToBeUpdated); 
            ackDatasets.addAll(ackCreateDatasets);
            ackDatasets.addAll(ackUpdateDatasets);
            jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_ADJUSTMENTS,false,EP_Common_Constant.BLANK,HeaderCommon,ackDatasets);
        } catch(Exception ex){
            jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_ADJUSTMENTS,true,ex.getMessage(),HeaderCommon,ackDatasets);
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_ADJUSTMENTS,EP_CustomerOtherAdjustmentHandler.class.getName(), ApexPages.Severity.ERROR);
        }
        return jsonResponse;
    }
    
    /**
    * @author           Accenture
    * @name             processDocuments
    * @date             02/08/2017
    * @description      Method to process documents
    * @param            List<EP_CustomerOtherAdjustmentStub.document>
    * @return           List<EP_AcknowledgementStub.dataset>
    */
    @TestVisible
    private static List<EP_AcknowledgementStub.dataset> processDocuments(List<EP_CustomerOtherAdjustmentStub.document> documents){
    	List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
    	EP_CustomerOtherAdjustmentHelper.setCustomerAdjustmentAttributes(documents); 
        ackDatasets = createAcknowledgementDatasets(documents);
        return ackDatasets;
    } 
    
    /**
    * @author           Accenture
    * @name             parse
    * @date             02/08/2017
    * @description      Method to parse i.e. deserialize the json string into EP_CustomerOtherAdjustmentStub stub class
    * @param            String
    * @return           EP_CustomerOtherAdjustmentStub
    */
    @TestVisible
    private static EP_CustomerOtherAdjustmentStub parse(String json){
        EP_GeneralUtility.Log('private','EP_CustomerOtherAdjustmentHandler','parse');
        return (EP_CustomerOtherAdjustmentStub) System.JSON.deserialize(json, EP_CustomerOtherAdjustmentStub.class);
    }
    
    /**
    * @author           Accenture
    * @name             createAcknowledgementDatasets
    * @date             05/05/2017
    * @description      Method used to create the acknowledgement datasets if it has any error's in it, else updates the customer adjustments and creates acknowledgement datasets for the same 
    * @param            List<EP_CustomerOtherAdjustmentStub.document>
    * @return           List<EP_AcknowledgementStub.dataset>
    */
    @TestVisible
    private static List<EP_AcknowledgementStub.dataset> createAcknowledgementDatasets(List<EP_CustomerOtherAdjustmentStub.document> documents){
        EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHandler','createAcknowledgementDatasets');
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
        List<EP_Customer_Other_Adjustment__c> customerAdjustmentsToUpdate = new List<EP_Customer_Other_Adjustment__c>();
        Map<string, EP_Customer_Other_Adjustment__c> mapCustomerAdjustments = new Map<string, EP_Customer_Other_Adjustment__c>(); 
        for(EP_CustomerOtherAdjustmentStub.document document : documents){
            if(string.isNotBlank(document.errorDescription)){
                ackDataset = EP_AcknowledgementUtil.createDataSet(EP_COMMON_CONSTANT.DOCUMENT_TAG,document.seqId,document.errorDescription,document.errorDescription);
                ackDatasets.add(ackDataset);
            } else {
                if(!mapCustomerAdjustments.containsKey(EP_GeneralUtility.getCompositeKey(document.identifier.clientId,document.identifier.billTo,document.identifier.docId).toUpperCase())) {
                	customerAdjustmentsToUpdate.add(document.SFCustomerOtherAdjustment);
                	mapCustomerAdjustments.put(EP_GeneralUtility.getCompositeKey(document.identifier.clientId,document.identifier.billTo,document.identifier.docId).toUpperCase(), document.SFCustomerOtherAdjustment);
            	}
            }
        }
        
        doDML(customerAdjustmentsToUpdate,ackDatasets);
        return ackDatasets;
    }
    
    /**
    * @author           Accenture
    * @name             doDML
    * @date             03/13/2017
    * @description      Will perform the DML operations and sends back the appropriate acknowledgement datasets
    * @param            List<Account>,List<EP_AcknowledgementStub.dataset>
    * @return           List<EP_AcknowledgementStub.dataset>
    */
    public static boolean doDML(List<EP_Customer_Other_Adjustment__c> customerAdjustmentsToUpdate, List<EP_AcknowledgementStub.dataset> ackDatasets){
        EP_GeneralUtility.Log('public','EP_CustomerOtherAdjustmentHandler','doDML');
        boolean recordFailed = false;
        EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
        string errorDescription='';
        if(!customerAdjustmentsToUpdate.isEmpty()){
            List<Database.UpsertResult> results =  DataBase.upsert(customerAdjustmentsToUpdate,false);
            for(Integer count = 0; count < results.size(); count++){
                if(!results[count].isSuccess()){
                    recordFailed= true;
                    errorDescription = EP_GeneralUtility.getDMLErrorMessage(results[count]);
                }
                ackDataset = EP_AcknowledgementUtil.createDataset(EP_COMMON_CONSTANT.DOCUMENT_TAG,customerAdjustmentsToUpdate[count].EP_Source_Seq_Id__c,errorDescription,errorDescription);
                ackDatasets.add(ackDataset);
            }
        }
        return recordFailed;
    }
}