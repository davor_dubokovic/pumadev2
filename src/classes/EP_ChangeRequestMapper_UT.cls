@isTest
public class EP_ChangeRequestMapper_UT
{
    static final string EVENT_NAME = '02-BasicDataSetupTo02-BasicDataSetup';
    static final string INVALID_EVENT_NAME = '08-ProspectTo02-Basic Data Setup';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void queryChangeRequestRecordsWithChild_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        EP_ChangeRequest__c changeRequestObj = new EP_ChangeRequest__c();
        changeRequestObj.EP_Object_Type__c = EP_Common_Constant.ACCOUNT_OBJ;
        changeRequestObj.EP_Record_Type__c = EP_Common_Constant.SELL_TO;
        changeRequestObj.EP_Account__c = obj.localAccount.id;
        changeRequestObj.EP_Request_Date__c = System.today();  
        insert changeRequestObj;
        EP_ChangeRequest__c cr = [SELECT Id FROM EP_ChangeRequest__c LIMIT : EP_Common_Constant.ONE];
        EP_ChangeRequestMapper changeReqMapper = new EP_ChangeRequestMapper();        
        Test.startTest();
        List<EP_ChangeRequest__c> crList = changeReqMapper.queryChangeRequestRecordsWithChild(cr.Id);
        Test.stopTest();
        System.Assert(crList.size()>0);
    }
}