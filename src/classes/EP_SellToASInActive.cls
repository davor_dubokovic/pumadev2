public class EP_SellToASInActive extends EP_AccountState{
  public EP_SellToASInActive() {
    
  }

  public override void setAccountDomainObject(EP_AccountDomainObject currentAccount)
  {
    super.setAccountDomainObject(currentAccount);
  }

  public override void doOnEntry(){
    EP_GeneralUtility.Log('Public','EP_SellToASInActive','doOnEntry');
    if(!this.account.isStatuschanged()){
        EP_AccountService accService = new EP_AccountService(this.account);
        accService.doActionSendUpdateRequestToNavAndLomo();
    }
  }  

  public override void doOnExit(){
    EP_GeneralUtility.Log('Public','EP_SellToASInActive','doOnExit');
    
  }

  public override boolean doTransition(){
    EP_GeneralUtility.Log('Public','EP_SellToASInActive','doTransition');
    return super.doTransition();
  }

  public override boolean isInboundTransitionPossible(){
    EP_GeneralUtility.Log('Public','EP_SellToASInActive','isInboundTransitionPossible');
    return super.isInboundTransitionPossible();
  }
}