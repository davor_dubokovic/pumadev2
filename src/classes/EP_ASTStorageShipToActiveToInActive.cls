/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToActiveToInActive>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 05-Active to 07-Inactive>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToActiveToInActive extends EP_AccountStateTransition {

    public EP_ASTStorageShipToActiveToInActive () {
        finalState = EP_AccountConstant.INACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToInActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToInActive', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToInActive',' isGuardCondition');        
        return true;
    }
}