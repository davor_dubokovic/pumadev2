/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationBasicDataToSetup>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 02-Basic Data Setup to 04-AccountSet-up>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationBasicDataToSetup extends EP_AccountStateTransition {

    public EP_ASTStorageLocationBasicDataToSetup () {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBasicDataToSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBasicDataToSetup', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationBasicDataToSetup',' isGuardCondition');        
        return true;
    }

}