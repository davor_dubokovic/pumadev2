/**
   @Author          CR Team
   @name            EP_TankDipMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to EP_TankDip__c Object and database operation methods
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_TankDipMapper {    
    
    /* *  Constructor.
    *  @name             EP_TankDipMapper
    *  @param            NA
    *  @return           NA
    *  @throws           NA
    */
    public EP_TankDipMapper() {
    
    }
    

    /**  This method returns Placeholder Tank Dip Records
    *  @name             getPlaceHolderTankDipRecords
    *  @param            set<id>, Id
    *  @return           list<EP_Tank_Dip__c>
    *  @throws           NA
    */
    public list<EP_Tank_Dip__c> getPlaceHolderTankDipRecords(set<id> idSet, Id recordTypeId) {
        list<EP_Tank_Dip__c> tankDipList =new list<EP_Tank_Dip__c> ();
        for(list<EP_Tank_Dip__c> tankDipObjList :[SELECT EP_Ambient_Quantity__c,
                                                RecordType.id,
                                                CreatedDate,
                                                EP_Tank__c 
                                                FROM EP_Tank_Dip__c
                                                WHERE EP_Tank__c IN :idSet 
                                                AND RecordTypeId = :recordTypeId 
                                                AND EP_Tank_Dip_Entered_Today__c = true 
                                                ]){
                tankDipList.addAll(tankDipObjList);
        }                                           
        return tankDipList;
    }
    
    public List<EP_Tank_Dip__c> getTankDipsByIds(set<string> setTankIds){
    	List<EP_Tank_Dip__c> tankDipRecords = new List<EP_Tank_Dip__c>();
    	tankDipRecords = [select id,EP_Tank_Dip_Exported__c,EP_Integration_Status__c from EP_Tank_Dip__c where id in : setTankIds ];
    	return tankDipRecords;
    }
    
    public List<EP_Tank_Dip__c> getTankDipsToExport(){
    	List<EP_Tank_Dip__c> tankDipRecords = new List<EP_Tank_Dip__c>();
    	String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.Actual).getRecordTypeId();
    	for(List<EP_Tank_Dip__c> tankDips :  [SELECT ID,EP_Tank__r.Name,EP_Tank__r.EP_Ship_To__r.EP_Account_Company_Name__c,EP_Tank_Nr__c,
				                                RecordType.Id, EP_Tank__r.EP_Ship_To__r.Parent.AccountNumber,
				                                EP_Tank__r.EP_Ship_To__r.AccountNumber,EP_Tank__r.EP_Product__r.ProductCode,
				                                EP_Unit_Of_Measure__c, EP_Tank__r.EP_Tank_Code__c, EP_Ambient_Quantity__c,
				                                EP_Reading_Date_Time__c, LastModifiedDate, LastModifiedBy.Alias,
				                                EP_Tank__r.EP_Ship_To__c, EP_Tank__r.EP_Ship_To__r.EP_Ship_To_UTC_Timezone__c,
				                                EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c,EP_Tank__r.EP_Ship_To__r.EP_Puma_Company_Code__c
				                                FROM EP_Tank_Dip__c
				                                WHERE EP_Tank_Dip_Exported__c = FALSE 
				                                AND RecordTypeId =: recType LIMIT 1]){
			tankDipRecords.addAll(tankDips);		                                	
        }
        return tankDipRecords;
    }
}