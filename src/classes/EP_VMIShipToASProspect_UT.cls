@isTest
public class EP_VMIShipToASProspect_UT
{
    static final string EVENT_NAME = '01-ProspectTo01-Prospect';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void setAccountDomainObject_test() {
        EP_VMIShipToASProspect localObj = new EP_VMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
        
    }

    static testMethod void doTransition_PositiveScenariotest() {
        EP_VMIShipToASProspect localObj = new EP_VMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

    static testMethod void doTransition_NegativeScenariotest() {
        EP_VMIShipToASProspect localObj = new EP_VMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        string message;
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
        }Catch(Exception e){
            message = e.getMessage();
        }
        Test.stopTest();
        System.assert(string.isNotBlank(message));
    }

    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_VMIShipToASProspect localObj = new EP_VMIShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    /** 
	    This isInboundTransitionPossible() method is returning only true for all cases.
	    So only positive scenario applicable. 
	**/

}