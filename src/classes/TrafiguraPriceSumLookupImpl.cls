global with sharing class TrafiguraPriceSumLookupImpl extends cscfga.ALookupSearch {

  private static final String BASKET_ID_FIELD = 'Product Basket ID';
  private static final String REFRESH_INDICATIOR_FIELD = 'Refresh Order Value Indicator';

  global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
    String basketId = searchFields.get(BASKET_ID_FIELD);

    List<AggregateResult> result = [
      select sum(cscfga__total_price__c) total
      from cscfga__Product_Configuration__c
      where cscfga__product_basket__c = :basketId
        and cscfga__root_configuration__c = null
        and cscfga__product_definition__r.name = :system.label.Puma_Energy_Order_Line
    ];

    return new List<cscfga__Product_Configuration__c> {
      new cscfga__Product_Configuration__c(
        fuel_order_value__c = (Decimal) result[0].get('total')
      )
    };
  }

  global override String getRequiredAttributes() {
    return '["' + BASKET_ID_FIELD + '","' + REFRESH_INDICATIOR_FIELD + '"]';
  }
}