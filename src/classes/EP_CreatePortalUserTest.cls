@isTest
private class EP_CreatePortalUserTest {
    private static Account sellToAccount;
    private static Contact contactRec;
    private static Id strRecordTypeId;
    private static Account nonVmiShipToAccount;
    private static Product2 product;
    private static string TANKOBJ = 'EP_Tank__c';
    private static EP_Tank__c tankObj1;
    private static EP_Bank_Account__c bankAccObj;
    private static EP_ExceptionEmailTo__c exEm;
    private static EP_Country__c country;
    private static EP_region__c region;
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String REGION_NAME = 'APAC';
    private static String COUNTRY_REGION = 'North_Australia';
    private static string ID = 'id';
    private static string TYPE = 'type';
    private static string RETURN_TYPE = 'returl';
    private static string OPERATION = 'op';
    private static string UPDATE_OPERATION = 'UPDATE';
    private static string NEW_OPERATION = 'NEW';
    private static string RECORD_TYPE = 'rtype';
    private static String ACCOUNTSTR = 'Account';
    private static String SHIPTONONVMIRECORDTYPE = 'EP_Non_VMI_Ship_To';
    private static String SELLTORECORDTYPE = 'EP_Sell_To';
    private static String BANKACCOUNTOBJ = 'EP_Bank_Account__c';
    private static String TANK_RECORD_TYPE = 'EP_Retail_Site_Tank';
    private static string RECORD_TYPE_NAME = 'recTypeName';

    static void loadTestData(){
        
        country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, REGION_NAME);
        Database.insert(country,false);
    
        region = EP_TestDataUtility.createCountryRegion( COUNTRY_REGION, country.Id);  
        Database.insert(region,false);
        
        //account = EP_TestDataUtility.createSellToAccount(null,null);
        sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        insert sellToAccount;
        //Insert Bank Account
        bankAccObj =  EP_TestDataUtility.createBankAccount(sellToAccount.id);
        Database.insert(bankAccObj,false);
        
        sellToAccount = [Select ownerId,id from Account];
        EP_Freight_Matrix__c FreightMat = EP_TestDataUtility.createFreightMatrix();
        Database.insert(FreightMat,false);
        
        Account billAcc = EP_TestDataUtility.createBillToAccount();
        billAcc.EP_Freight_Matrix__c = FreightMat.id;
        Database.insert(billAcc,false);
        
        
        
        contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
        contactRec = [select id,ownerid from contact ];
        
         exEm = new EP_ExceptionEmailTo__c(SetupOwnerId=UserInfo.getOrganizationId(), Email__c = 'test@xyz.com');
        Database.insert(exEm,false);
    
        
        
        strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        nonVmiShipToAccount.EP_Delivery_Type__c = 'Ex-Rack';
        nonVmiShipToAccount.EP_Pumps__c= 10;
        //database.insert(nonVmiShipToAccount,false);
        Database.insert(nonVmiShipToAccount,false);
        
        //create Product
        product = EP_TestDataUtility.createTestRecordsForProduct();
        // insert tanks in order to mark vmi account as Active
        tankObj1 = EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,product.id);
        tankObj1.EP_Tank_Code__c='2';
        database.insert(tankObj1,false);
        
        // Make non Vmi account active
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        Database.update(nonVmiShipToAccount,false);
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(nonVmiShipToAccount,false);
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        sellToAccount.EP_Bill_To_Account__c = billAcc.id;   
        Database.update(sellToAccount, false); 
        
        
    }
    
    /* 
            ship to new operation fields check for PORTAL user
      */
      static testMethod void myUnitTestShipToNewPortal() { 
          
         Profile Admin = [Select id from Profile
                                    Where Name =: 'System Administrator'
                                    limit :EP_Common_Constant.ONE];
        User adminUser;
        User adminUser1 = EP_TestDataUtility.createUser(Admin.id);
        
        User portalUser;
        System.runAs(adminUser1){
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            adminUser = EP_TestDataUtility.createUser(Admin.id);
            adminUser.UserRoleId = r.id;
            insert adminUser;
        }
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
        EP_BankAccountTriggerHandler.isExecuteAfterUpdate = false;
        EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
        System.runAs(adminUser){
            loadTestData(); 
        }
      
        System.runAs(adminUser){
            Profile portalUserProfile = [Select id from Profile
                              Where Name =: EP_Common_Constant.RETAIL_VMI_SITE_PROFILE 
                              limit :EP_Common_Constant.ONE];
            
           portalUser = EP_TestDataUtility.createPortalUser(portalUserProfile.id,contactRec.id);
           insert portalUser;   
        }
          
          
          EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('India','IN', 'Asia');
          EP_Country__c contry2 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'aus');
          List<EP_Country__c> countryToInsert = new List<EP_Country__c>{contry1,contry2} ;
          Database.insert(countryToInsert);
        /*
          System.runAs(portalUser){
            test.startTest();
             //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteAfterUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
            ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
            ApexPages.currentPage().getParameters().put(TYPE, 'Account');
            ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
            ApexPages.currentPage().getParameters().put(OPERATION, NEW_OPERATION);
            ApexPages.currentPage().getParameters().put(RECORD_TYPE, SHIPTONONVMIRECORDTYPE);
            
            EP_ChangeRequestController controller1 = new EP_ChangeRequestController();
            Map<string, integer> fieldByIndexMap1 = new Map<string, integer>();
            integer count1=0;
            for(EP_ChangeRequestController.cRWrapperFields fls : controller1.allFieldValues){
                fieldByIndexMap1.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count1);
                count1++;
          }
          test.stopTest();
          system.assertEquals(false,fieldByIndexMap1.containskey('EP_Delivery_Type__c'.toLowerCase()));
          system.assert(fieldByIndexMap1.containskey('Name'.toLowerCase())); 
          system.assert(fieldByIndexMap1.containskey('EP_Number_of_Hoses__c'.toLowerCase()));
          system.assert(fieldByIndexMap1.containskey('EP_Pumps__c'.toLowerCase()));
          system.assert(fieldByIndexMap1.containskey('ShippingCity'.toLowerCase()));
          system.assert(fieldByIndexMap1.containskey('ShippingStreet'.toLowerCase()));
          system.assert(fieldByIndexMap1.containskey('ShippingState'.toLowerCase()));
          system.assert(fieldByIndexMap1.containskey('ShippingPostalCode'.toLowerCase()));
          system.assert(fieldByIndexMap1.containskey('ShippingCountry'.toLowerCase()));
        
          controller1.allFieldValues[fieldByIndexMap1.get('Name'.toLowerCase())].crlobj.EP_New_Value__c = 'Test';
          controller1.allFieldValues[fieldByIndexMap1.get('Name'.toLowerCase())].ischanged = true;
          
          controller1.allFieldValues[fieldByIndexMap1.get('EP_Number_of_Hoses__c'.toLowerCase())].crlobj.EP_New_Value__c = '80';
          controller1.allFieldValues[fieldByIndexMap1.get('EP_Number_of_Hoses__c'.toLowerCase())].ischanged = true;
          controller1.allFieldValues[fieldByIndexMap1.get('EP_Pumps__c'.toLowerCase())].crlobj.EP_New_Value__c = '1000';
          controller1.allFieldValues[fieldByIndexMap1.get('EP_Pumps__c'.toLowerCase())].ischanged = true;
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingCity'.toLowerCase())].crlobj.EP_New_Value__c = 'City';
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingCity'.toLowerCase())].ischanged = true;
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingStreet'.toLowerCase())].crlobj.EP_New_Value__c = 'Street';
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingStreet'.toLowerCase())].ischanged = true;
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingState'.toLowerCase())].crlobj.EP_New_Value__c = 'State';
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingState'.toLowerCase())].ischanged = true;
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingPostalCode'.toLowerCase())].crlobj.EP_New_Value__c = 'Code';
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingPostalCode'.toLowerCase())].ischanged = true;
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingCountry'.toLowerCase())].crlobj.EP_New_Value__c = 'Country';
          controller1.allFieldValues[fieldByIndexMap1.get('ShippingCountry'.toLowerCase())].ischanged = true;
          controller1.submit();
          system.assert(controller1.request.id != null);
            
        }*/
    } 
    
    
    static testMethod void myUnitTestShipToUpdatePortal() {
        Profile Admin = [Select id from Profile
                                    Where Name =: 'System Administrator'
                                    limit :EP_Common_Constant.ONE];
        User adminUser;
        User adminUser1 = EP_TestDataUtility.createUser(Admin.id);
        
        User portalUser;
        System.runAs(adminUser1){
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            adminUser = EP_TestDataUtility.createUser(Admin.id);
            adminUser.UserRoleId = r.id;
            insert adminUser;
        }
        System.runAs(adminUser){
            
            loadTestData();
            
            
        }
        System.runAs(adminUser){
            Profile portalUserProfile = [Select id from Profile
                              Where Name =: EP_Common_Constant.RETAIL_VMI_SITE_PROFILE 
                              limit :EP_Common_Constant.ONE];
            
           portalUser = EP_TestDataUtility.createPortalUser(portalUserProfile.id,contactRec.id);
           insert portalUser;
         
            
        }
        
        System.runAs(portalUser){
            test.startTest();
             //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteAfterUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
            ApexPages.currentPage().getParameters().put(ID, nonVmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put(TYPE, 'Account');
            ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+nonVmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
            ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Non-VMI Ship To');
            
            EP_ChangeRequestController controller = new EP_ChangeRequestController();
            Map<string, integer> fieldByIndexMap = new Map<string, integer>();
            integer count=0;
            for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
                fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count);
                count++;
        }
        test.stopTest();
            system.assertEquals(false,fieldByIndexMap.containskey('EP_Delivery_Type__c'.toLowerCase())); 
            system.assert(fieldByIndexMap.containskey('EP_Number_of_Hoses__c'.toLowerCase()));
            system.assert(fieldByIndexMap.containskey('EP_Pumps__c'.toLowerCase()));
           /* system.assert(fieldByIndexMap.containskey('ShippingCity'.toLowerCase()));
            system.assert(fieldByIndexMap.containskey('ShippingStreet'.toLowerCase()));
            system.assert(fieldByIndexMap.containskey('ShippingState'.toLowerCase()));
            system.assert(fieldByIndexMap.containskey('ShippingPostalCode'.toLowerCase())); */
            controller.allFieldValues[0].ischanged = true;
            controller.allFieldValues[0].crlobj.EP_New_Value__c = '123';
            controller.submit();
            //system.assert(controller.request.id != null);
            }
    }
    
    /* 
            Bank account new operation fields check for PORTAL user
      */
    static testMethod void testBankAccountNEWPortal() { 
        
         Profile Admin = [Select id from Profile
                                    Where Name =: 'System Administrator'
                                    limit :EP_Common_Constant.ONE];
        User adminUser;
        User adminUser1 = EP_TestDataUtility.createUser(Admin.id);
        
        User portalUser;
        System.runAs(adminUser1){
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            adminUser = EP_TestDataUtility.createUser(Admin.id);
            adminUser.UserRoleId = r.id;
            insert adminUser;
        }
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
        EP_BankAccountTriggerHandler.isExecuteAfterUpdate = false;
        EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
        System.runAs(adminUser){
            
            loadTestData();
            
            
        }
        System.runAs(adminUser){
            Profile portalUserProfile = [Select id from Profile
                              Where Name =: EP_Common_Constant.RETAIL_VMI_SITE_PROFILE 
                              limit :EP_Common_Constant.ONE];
            
           portalUser = EP_TestDataUtility.createPortalUser(portalUserProfile.id,contactRec.id);
           insert portalUser;
         
            
        }
         /*  
        System.runAs(portalUser){
             test.startTest();
             //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteAfterUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
            EP_Bank_Account__c bankAccObj1 =  EP_TestDataUtility.createBankAccount(sellToAccount.id);
            Database.insert(bankAccObj1,false);
                 //Bank Account for New operation portal user
            ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
            ApexPages.currentPage().getParameters().put(TYPE, BANKACCOUNTOBJ);
            ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
            ApexPages.currentPage().getParameters().put(OPERATION, NEW_OPERATION);
            EP_ChangeRequestController controller1 = new EP_ChangeRequestController();
            Map<string, integer> fieldByIndexMap1 = new Map<string, integer>();
            integer i1=0;
            for(EP_ChangeRequestController.cRWrapperFields fls : controller1.allFieldValues){
                fieldByIndexMap1.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), i1);
                i1++;
            }
            test.stopTest();
            system.assert(fieldByIndexMap1.containskey('EP_Bank_Account_No__c'.toLowerCase()));
            system.assert(fieldByIndexMap1.containskey('EP_Bank_Branch_No__c'.toLowerCase()));
            system.assert(fieldByIndexMap1.containskey('EP_Bank_Name__c'.toLowerCase()));
            system.assert(fieldByIndexMap1.containskey('EP_IBAN__c'.toLowerCase()));
            system.assert(fieldByIndexMap1.containskey('EP_Swift_Code__c'.toLowerCase()));
            controller1.allFieldValues[0].ischanged = true;
            controller1.allFieldValues[0].crlobj.EP_New_Value__c = '132456';
            controller1.submit();
            system.assert(controller1.request.id != null);
           }*/
        }
        
        /* 
            Bank account update operation fields check for PORTAL user
      */
    static testMethod void testBankAccountUpdatePortal() {
        
        
        Profile Admin = [Select id from Profile
                                    Where Name =: 'System Administrator'
                                    limit :EP_Common_Constant.ONE];
        User adminUser;
        User adminUser1 = EP_TestDataUtility.createUser(Admin.id);
        
        User portalUser;
        System.runAs(adminUser1){
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            adminUser = EP_TestDataUtility.createUser(Admin.id);
            adminUser.UserRoleId = r.id;
            insert adminUser;
        }
        
        System.runAs(adminUser){
            
            loadTestData();
            
            
        }
        System.runAs(adminUser){
            Profile portalUserProfile = [Select id from Profile
                              Where Name =: EP_Common_Constant.RETAIL_VMI_SITE_PROFILE 
                              limit :EP_Common_Constant.ONE];
            
           portalUser = EP_TestDataUtility.createPortalUser(portalUserProfile.id,contactRec.id);
           insert portalUser;
         
            
        }
       
        
        /*
       System.runAs(portalUser){
        test.starttest();   
        EP_Bank_Account__c bankAccObj1 =  EP_TestDataUtility.createBankAccount(sellToAccount.id);
        Database.insert(bankAccObj1,false); 
         //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
        EP_BankAccountTriggerHandler.isExecuteAfterUpdate = true;
        EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
        // Bank Account Update operation For Portal User
        ApexPages.currentPage().getParameters().put(ID, bankAccObj1.Id);
        ApexPages.currentPage().getParameters().put(TYPE, BANKACCOUNTOBJ);
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+bankAccObj1.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        
        EP_ChangeRequestController controller2 = new EP_ChangeRequestController();
        Map<string, integer> fieldByIndexMap2 = new Map<string, integer>();
        integer count=0;
        for(EP_ChangeRequestController.cRWrapperFields fls : controller2.allFieldValues){
            fieldByIndexMap2.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count);
            count++;
        }
        test.stopTest();
        system.assert(fieldByIndexMap2.containskey('EP_Bank_Account_No__c'.toLowerCase()));
        system.assert(fieldByIndexMap2.containskey('EP_Bank_Branch_No__c'.toLowerCase()));
        system.assert(fieldByIndexMap2.containskey('EP_Bank_Name__c'.toLowerCase()));
        system.assert(fieldByIndexMap2.containskey('EP_IBAN__c'.toLowerCase()));
        system.assert(fieldByIndexMap2.containskey('EP_Swift_Code__c'.toLowerCase()));
        system.assert(fieldByIndexMap2.containskey('EP_Bank_Account_Status__c'.toLowerCase()));
        controller2.allFieldValues[0].ischanged = true;
        controller2.allFieldValues[0].crlobj.EP_New_Value__c = '132456';
        controller2.submit();
        system.assert(controller2.request.id != null);
        }   */
    }
    
    
    /* 
            Tank new operation fields check for PORTAL user
      */
        static testMethod void testTankPortalNEWOperation() { 
          Profile Admin = [Select id from Profile
                                    Where Name =: 'System Administrator'
                                    limit :EP_Common_Constant.ONE];
        User adminUser;
        User adminUser1 = EP_TestDataUtility.createUser(Admin.id);
        
        User portalUser;
        System.runAs(adminUser1){
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            adminUser = EP_TestDataUtility.createUser(Admin.id);
            adminUser.UserRoleId = r.id;
            insert adminUser;
        }
        
        System.runAs(adminUser){
            
            loadTestData();
            
            
        }
        System.runAs(adminUser){
            Profile portalUserProfile = [Select id from Profile
                              Where Name =: EP_Common_Constant.RETAIL_VMI_SITE_PROFILE 
                              limit :EP_Common_Constant.ONE];
            
           portalUser = EP_TestDataUtility.createPortalUser(portalUserProfile.id,contactRec.id);
           insert portalUser;
           AccountShare  ashare = new AccountShare();
           ashare.UserOrGroupId = portalUser.id;
           ashare.AccountAccessLevel  = 'edit';
           ashare.OpportunityAccessLevel = 'read';
           ashare.ContactAccessLevel = 'Read';
           ashare.caseAccessLevel = 'Read';
           ashare.AccountId = nonVmiShipToAccount.id;
           database.insert(ashare);
         
            
        }
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
        EP_BankAccountTriggerHandler.isExecuteAfterUpdate = false;
        EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
        /*
            System.runAs(portalUser){
            TEST.startTest();   
            ApexPages.currentPage().getParameters().put(ID, nonVmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put(TYPE, TANKOBJ);
            ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+nonVmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put(OPERATION, NEW_OPERATION);
            ApexPages.currentPage().getParameters().put(RECORD_TYPE, TANK_RECORD_TYPE);
            
            
            EP_ChangeRequestController controller3 = new EP_ChangeRequestController();
            Map<string, integer> fieldByIndexMap3 = new Map<string, integer>();
            integer count2=0;
            for(EP_ChangeRequestController.cRWrapperFields fls : controller3.allFieldValues){
                fieldByIndexMap3.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count2);
                count2++;
            }
            test.stopTest();
            system.assert(fieldByIndexMap3.containskey('EP_Capacity__c'.toLowerCase()));
            system.assert(fieldByIndexMap3.containskey('EP_Deadstock__c'.toLowerCase()));
            system.assert(fieldByIndexMap3.containskey('EP_Safe_Fill_Level__c'.toLowerCase()));
            system.assert(fieldByIndexMap3.containskey('EP_Tank_Status__c'.toLowerCase()));
            //system.assert(fieldByIndexMap3.containskey('EP_Unit_Of_Measure__c'.toLowerCase()));
            
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Safe_Fill_Level__c'.toLowerCase())].crlobj.EP_New_Value__c = '800';
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Safe_Fill_Level__c'.toLowerCase())].ischanged = true;
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Capacity__c'.toLowerCase())].crlobj.EP_New_Value__c = '1000';
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Capacity__c'.toLowerCase())].ischanged = true;
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Deadstock__c'.toLowerCase())].crlobj.EP_New_Value__c = '100';
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Deadstock__c'.toLowerCase())].ischanged = true;
            //controller3.allFieldValues[fieldByIndexMap3.get('EP_Unit_Of_Measure__c'.toLowerCase())].crlobj.EP_New_Value__c = 'LT';
            //controller3.allFieldValues[fieldByIndexMap3.get('EP_Unit_Of_Measure__c'.toLowerCase())].ischanged = true;
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Tank_Status__c'.toLowerCase())].crlobj.EP_New_Value__c = 'New';
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Tank_Status__c'.toLowerCase())].ischanged = true;
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Product__c'.toLowerCase())].crlobj.EP_New_Value__c = product.id;
            controller3.allFieldValues[fieldByIndexMap3.get('EP_Product__c'.toLowerCase())].ischanged = true;
            controller3.submit();
            system.assert(controller3.request.id != null);
            }*/
        }
        
          /* 
            ship to update operation fields check for PORTAL user
      */
        static testMethod void testTankPortal() { 
            
         Profile Admin = [Select id from Profile
                                Where Name =: 'System Administrator'
                                limit :EP_Common_Constant.ONE];
        User adminUser;
        User adminUser1 = EP_TestDataUtility.createUser(Admin.id);
        
        User portalUser;
        System.runAs(adminUser1){
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            adminUser = EP_TestDataUtility.createUser(Admin.id);
            adminUser.UserRoleId = r.id;
            insert adminUser;
        }
        
        System.runAs(adminUser){
            
            loadTestData();
            
            
        }
        System.runAs(adminUser){
            Profile portalUserProfile = [Select id from Profile
                              Where Name =: EP_Common_Constant.RETAIL_VMI_SITE_PROFILE 
                              limit :EP_Common_Constant.ONE];
            
           portalUser = EP_TestDataUtility.createPortalUser(portalUserProfile.id,contactRec.id);
           insert portalUser;
            AccountShare  ashare = new AccountShare();
            ashare.UserOrGroupId = portalUser.id;
            ashare.AccountAccessLevel  = 'edit';
            ashare.OpportunityAccessLevel = 'read';
            ashare.ContactAccessLevel = 'Read';
            ashare.caseAccessLevel = 'Read';
            ashare.AccountId = nonVmiShipToAccount.id;
            Database.insert(ashare,false);
            
        }
        
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = false;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteAfterUpdate = false;
            EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = false;
           
            System.runAs(portalUser){
            Test.startTest();
            ApexPages.currentPage().getParameters().put(ID, tankObj1.Id);
            ApexPages.currentPage().getParameters().put(TYPE, TANKOBJ);
            ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+tankObj1.Id);
            ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
            ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Retail Site Tank');
            EP_ChangeRequestController controller2 = new EP_ChangeRequestController();
            Map<string, integer> fieldByIndexMap2 = new Map<string, integer>();
            integer count1=0;
            for(EP_ChangeRequestController.cRWrapperFields fls : controller2.allFieldValues){
                fieldByIndexMap2.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count1);
                count1++;
            }
            test.stopTest();
            system.assert(fieldByIndexMap2.containskey('EP_Tank_Status__c'.toLowerCase()));
            controller2.allFieldValues[0].ischanged = true;
            controller2.allFieldValues[0].crlobj.EP_New_Value__c = 'New';
            controller2.submit();
            //system.assert(controller2.request.id != null);
            }
            
            
     }
     
      /* 
            sell to update operation fields check for PORTAL user
      */
      static testMethod void testSellToPortal() { 
            
         Profile Admin = [Select id from Profile
                            Where Name =: 'System Administrator'
                            limit :EP_Common_Constant.ONE];
        User adminUser;
        User adminUser1 = EP_TestDataUtility.createUser(Admin.id);
        
        User portalUser;
        System.runAs(adminUser1){
            UserRole r = new UserRole(name = 'TEST ROLE');
            Database.insert(r);
            
            adminUser = EP_TestDataUtility.createUser(Admin.id);
            adminUser.UserRoleId = r.id;
            insert adminUser;
        }
        
        System.runAs(adminUser){
            loadTestData();
        }
        System.runAs(adminUser){
            Profile portalUserProfile = [Select id from Profile
                              Where Name =: EP_Common_Constant.RETAIL_VMI_SITE_PROFILE 
                              limit :EP_Common_Constant.ONE];
            
           portalUser = EP_TestDataUtility.createPortalUser(portalUserProfile.id,contactRec.id);
           insert portalUser;
            
            
        }
            
            
            
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
            EP_BankAccountTriggerHandler.isExecuteAfterUpdate = true;
            EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = true;
            System.runAs(portalUser){
                test.startTest();
                PageReference pageRef = Page.EP_ChangeRequest;
                Test.setCurrentPage(pageRef);
                //Update Operation For Sell To Account
                ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
                ApexPages.currentPage().getParameters().put(TYPE, ACCOUNTSTR);
                ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
                ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
                ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, 'Sell To');
                EP_ChangeRequestController controller1 = new EP_ChangeRequestController();
                Map<string, integer> fieldByIndexMap1 = new Map<string, integer>();
                integer i=0;
                for(EP_ChangeRequestController.cRWrapperFields fls : controller1.allFieldValues){
                    fieldByIndexMap1.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), i);
                    i++;
                }
                test.stopTest();
                system.assert(fieldByIndexMap1.containskey('Name'.toLowerCase()));
              /*system.assert(fieldByIndexMap1.containskey('BillingCity'.toLowerCase()));
                system.assert(fieldByIndexMap1.containskey('BillingStreet'.toLowerCase()));
                system.assert(fieldByIndexMap1.containskey('BillingState'.toLowerCase()));
                system.assert(fieldByIndexMap1.containskey('BillingPostalCode'.toLowerCase())); */
                system.assert(fieldByIndexMap1.containskey('EP_Email__c'.toLowerCase()));
                system.assert(fieldByIndexMap1.containskey('Fax'.toLowerCase()));
                system.assert(fieldByIndexMap1.containskey('EP_Mobile_Phone__c'.toLowerCase()));
                system.assert(fieldByIndexMap1.containskey('Phone'.toLowerCase()));
                system.assert(fieldByIndexMap1.containskey('EP_Language__c'.toLowerCase()));
                controller1.allFieldValues[0].ischanged = true;
                controller1.allFieldValues[0].crlobj.EP_New_Value__c = '132456';
                controller1.submit();
               // system.assert(controller1.request.id != null);
              }
        }
    
}