/**
 * @author <Kamal Garg>
 * @name <EP_TankDipCreateUpdate1Test>
 * @createDate <16/12/2015>
 * @description <Test class for testing Tank Dip staus based on Account & Tank status> 
 * @version <1.0>
 */
@isTest
private class EP_TankDipCreateUpdate1Test {

    /**
     * @author Kamal Garg
     * @date 16/12/2015
     * @description Test Method to test that Dip entry is mandate for a CSC user for blocked ship to and unblocked tank via Submit Tank Dips Button
     */
    private static void updateAccountStatus(){  
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;      
    } 
    static testMethod void testDipEntryWithBlockedShipToUnblockedTankViaSubmitTankDipsButton() {
        Profile cscAgentProfile = [SELECT Id FROM Profile WHERE Name='Puma CSC Agent_R1' Limit 1];
        User cscAgentUser = EP_TestDataUtility.createUser(cscAgentProfile.id);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        System.runAs(cscAgentUser){
            // create test record for Account with RecordType='VMI Ship To'
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
            Account vmiShipToAccount = EP_TestDataUtility.createShipToAccount(null, strRecordTypeId);
            insert vmiShipToAccount;
            // create test records for EP_Tank__c with EP_Tank_Status__c='Operational'
            //Map<ID, EP_Tank__c> tanksMap = EP_TestDataUtility.createTestTankRecords(new List<Account>{vmiShipToAccount}, prod, 'Operational', 2);
            // verify that Tank status should be Operational
            EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
            insert tankObj1;
            updateAccountStatus();
            vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            update vmiShipToAccount;
            
            //Updating the tank record to be 'Basic Data Setup'
            tankObj1.EP_Tank_Status__c= EP_Common_Constant.BASIC_DATA_SETUP;
            update tankObj1;
            
            //Updating the tank record to be 'Operational'
            tankObj1.EP_Tank_Status__c = 'Operational';
            update tankObj1;
            
            system.assertEquals(tankObj1.EP_Tank_Status__c, 'Operational');
            //system.assertEquals(tanksMap.values().get(1).EP_Tank_Status__c, 'Operational');
            // update test record for Account. Mark vmiShipToAccount as '04-Blocked'
            vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
            vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
            vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
            vmiShipToAccount.EP_Reason_Blocked__c = 'Maintenance';
            update vmiShipToAccount;
            // verify below points:
            //  1. Account Status should be Blocked
            //  2. Tank Dips Schedule Time should not be blank
            //  3. Tank Dip Entry Mode should be Portal dip entry
            vmiShipToAccount = [SELECT Id, EP_Tank_Dips_Schedule_Time__c, EP_Tank_Dip_Entry_Mode__c, EP_Status__c FROM Account WHERE Id=:vmiShipToAccount.Id LIMIT 1];
            system.assertEquals(vmiShipToAccount.EP_Status__c, EP_Common_Constant.STATUS_BLOCKED);
            system.assert(String.isNotBlank(vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c));
            system.assertEquals(vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c, 'Portal Dip Entry');
            // getting 'EP_SelectTankDipDatePage_R1' PageReference 
            PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);
            // setting parameters to be passed with the page rendering
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of EP_TankDipSubmitPageControllerClass_R1 apex class
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
            // Verify the checkbox is checked for the records listed. Checkbox should be checked if dip entry is mandate
            system.assertEquals(tankDippageObj.displayedTankDipLines.size(), 1);
            system.assertEquals(tankDippageObj.displayedTankDipLines.get(0).blnAddTankDip, true);
            //system.assertEquals(tankDippageObj.displayedTankDipLines.get(1).blnAddTankDip, true);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 16/12/2015
     * @description Test Method to test that Dip entry is mandate for a CSC user for blocked ship to and unblocked tank via Submit Tank dips link
     */
    static testMethod void testDipEntryWithBlockedShipToUnblockedTankViaSubmitTankDipsLink() {
        Profile cscAgentProfile = [SELECT Id FROM Profile WHERE Name='Puma CSC Agent_R1' Limit 1];
        User cscAgentUser = EP_TestDataUtility.createUser(cscAgentProfile.id);
        Product2 prod = EP_TestDataUtility.createTestRecordsForProduct();
        System.runAs(cscAgentUser){
            // create test record for Account with RecordType='VMI Ship To'
            Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
            Account vmiShipToAccount = EP_TestDataUtility.createShipToAccount(null, strRecordTypeId);
            insert vmiShipToAccount;
            // create test record for EP_Tank__c with EP_Tank_Status__c='Operational'
            //Map<ID, EP_Tank__c> tanksMap = EP_TestDataUtility.createTestTankRecords(new List<Account>{vmiShipToAccount}, prod, 'Operational', 1);
            // verify that Tank status should be Operational
            EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
            insert tankObj1;
            updateAccountStatus();
            vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            update vmiShipToAccount;
            
            //Updating the tank record to be 'Basic Data Setup'
            tankObj1.EP_Tank_Status__c= EP_Common_Constant.BASIC_DATA_SETUP;
            update tankObj1;
            
            //Updating the tank record to be 'Operational'
            tankObj1.EP_Tank_Status__c = 'Operational';
            update tankObj1;
            
            system.assertEquals(tankObj1.EP_Tank_Status__c, 'Operational');            
            // update test record for Account. Mark vmiShipToAccount as '04-Blocked'
            vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
            vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c = '01:00';
            vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c = 'Portal Dip Entry';
            vmiShipToAccount.EP_Reason_Blocked__c = 'Maintenance';
            update vmiShipToAccount;
            // verify below points:
            //  1. Account Status should be Blocked
            //  2. Tank Dips Schedule Time should not be blank
            //  3. Tank Dip Entry Mode should be Portal dip entry
            vmiShipToAccount = [SELECT Id, EP_Tank_Dips_Schedule_Time__c, EP_Tank_Dip_Entry_Mode__c, EP_Status__c FROM Account WHERE Id=:vmiShipToAccount.Id LIMIT 1];
            system.assertEquals(vmiShipToAccount.EP_Status__c, EP_Common_Constant.STATUS_BLOCKED);
            system.assert(String.isNotBlank(vmiShipToAccount.EP_Tank_Dips_Schedule_Time__c));
            system.assertEquals(vmiShipToAccount.EP_Tank_Dip_Entry_Mode__c, 'Portal Dip Entry');
            // create test records for EP_Tank_Dip__c of RecordType='PlaceHolder' with EP_Reading_Date_Time__c='system.now()-5'
            EP_TestDataUtility.createTestTankDipsRecords(new list<EP_Tank__c>{tankObj1}, system.now()-5, true);
            // getting 'EP_SelectTankDipSitePage_R1' PageReference
            PageReference pageRef = Page.EP_SelectTankDipSitePage_R1;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            // creating instance of 'EP_SelectTankDipDatePageExtnClass_R1' apex class
            EP_SelectTankDipDatePageExtnClass_R1 selectTankDipDatePageExtnClass = new EP_SelectTankDipDatePageExtnClass_R1();
            // calling 'retrieveMissingSiteDates' api
            selectTankDipDatePageExtnClass.retrieveMissingSiteDates();
            selectTankDipDatePageExtnClass.tankDipDates.get(0).dateSelected = true;
            // getting 'EP_TankDipSubmitPage_R1' PageReference
            pageRef = Page.EP_TankDipSubmitPage_R1;
            Test.setCurrentPage(pageRef);
            // setting parameters to be passed with the page rendering
            ApexPages.currentPage().getParameters().put('id', vmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put('date', selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText);
            // creating instance of 'EP_TankDipSubmitPageControllerClass_R1' apex class
            EP_TankDipSubmitPageControllerClass_R1 tankDippageObj = new EP_TankDipSubmitPageControllerClass_R1();
            Integer intYear = Integer.valueOf(selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText.substring(0, 4));
            Integer intMonth = Integer.valueOf(selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText.substring(4, 6));
            Integer intDay = Integer.valueOf(selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText.substring(6, 8));
            Integer intHours = Integer.valueOf(selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText.substring(8, 10));
            Integer intMinutes = Integer.valueOf(selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText.substring(10, 12));
            Integer intSeconds = Integer.valueOf(selectTankDipDatePageExtnClass.tankDipDates.get(0).dateText.substring(12, 14));
            DateTime dtSelectedDateTime = DateTime.newInstance(intYear, intMonth, intDay, intHours, intMinutes, intSeconds);
            system.assertEquals(tankDippageObj.dtSelectedDateTime, dtSelectedDateTime);
            //Verify the checkbox is checked for the record listed. Checkbox should be checked if dip entry is mandate
            system.assertEquals(tankDippageObj.displayedTankDipLines.size(), 1);
            //system.assertEquals(tankDippageObj.displayedTankDipLines.get(0).blnAddTankDip, true);
        }
    }
    
    Private static void createTank(list<Account> AccList, Product2 Prod)
    {
        List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
        for(Account acc : AccList){
        EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(acc.Id,prod.Id);
        //tankObj1.EP_Tank_Code__c = '9876';
        tanksToBeInsertedList.add(tankObj1);
        }
        insert tanksToBeInsertedList;
        
    }
}