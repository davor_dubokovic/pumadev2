@isTest
private class EP_TankDetailPageControllerTest {
    
    
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    private static  PricebookEntry pbEntryPetrol;
    private static Product2 productPetrolObj;
    static PricebookEntry pbEntryPetrol11;
    static List<PricebookEntry> pbeList1;
    
    
    private static Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
    private static set<String> profileName = new set<String>{EP_Common_Constant.SM_AGENT_PROFILE,EP_Common_Constant.ADMIN_PROFILE
                                                            ,EP_Common_Constant.TM_AGENT_PROFILE
                                                            ,EP_Common_Constant.CSC_AGENT_PROFILE, EP_Common_Constant.LOGISTICS_PROFILE
                                                            ,EP_Common_Constant.PRICING_PROFILE
                                                            ,EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE
                                                            ,EP_Common_Constant.CREDIT_PROFILE};
                                                    
    private static set<String> userRole = new  set<String>{'EP_SM_TM_Team_AU', 
                                                           'EP_Internal_Review_Team_AU'};   

    private static Map<String, Profile> profileMap = new Map<String, Profile>();
    private static Map<String, UserRole> userRoleMap = new Map<String, UserRole>();
    
    private static Map<String, User> userMap ;
    Id pricebookId; 
    
    @testSetUp
    static void dataSetup(){
        Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
        Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
        Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
       // Test.setMock(WebserviceMock.class,new EP_MockDispatcher());
        userMap  = new Map<String, User>();
        EP_Country__c country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country,false);
        Company__c comp = EP_TestDataUtility.createCompany('EPUMA');
        comp.EP_BSM_GM_Review_Required__C = true;
        database.insert(comp);
        profileMap = new Map<String, Profile>();
        for(Profile p : [Select id , Name from Profile
                            Where Name in:profileName ]){
            profileMap.put(p.Name,p);
        }
        List<database.SaveResult> accSaveResults  = new List<database.SaveResult>();
        List<account> accounts = new List<account>();
        User SMTMuser = EP_TestDataUtility.createUser(profileMap.get('Puma SM Agent_R1').id);
        User adminUser = EP_TestDataUtility.createUser(profileMap.get('System Administrator').id);
        User cscUser = EP_TestDataUtility.createUser(profileMap.get(EP_Common_Constant.CSC_AGENT_PROFILE).id); 
        User logisticsUser = EP_TestDataUtility.createUser(profileMap.get(EP_Common_Constant.LOGISTICS_PROFILE).id);
        User pricingUser = EP_TestDataUtility.createUser(profileMap.get(EP_Common_Constant.PRICING_PROFILE).id);
        userMap.put('Puma SM Agent_R1', SMTMuser );
        userMap.put('System Administrator', adminUser );
        userMap.put(EP_Common_Constant.CSC_AGENT_PROFILE, cscUser );
        userMap.put(EP_Common_Constant.LOGISTICS_PROFILE, logisticsUser );
        userMap.put(EP_Common_Constant.PRICING_PROFILE, pricingUser );
        database.insert(userMap.values());
        
        //Insert Custom PriceBook and Product
        Pricebook2 customPB = new Pricebook2(Name=EP_Common_Constant.STANDARD_PRICE_BOOK_NAME, isActive=true);
        customPB.EP_Company__c = comp.id;
        database.insert(customPB);
            
        productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = EP_Common_Constant.GBP,
                                                isActive = TRUE,family=EP_Common_Constant.Hydrocarbon_Liquid,
                                                Eligible_for_Rebate__c = true,EP_Blended__c = false,
                                                EP_Company_Lookup__c = comp.id);                           
        database.insert(productPetrolObj);
        
        Id pricebookId = Test.getStandardPricebookId();
        
        
        pbEntryPetrol11 = new PricebookEntry(
        Pricebook2Id = customPB.Id,EP_Is_Sell_To_Assigned__c = true, Product2Id = productPetrolObj.Id,
        UnitPrice = 12000, IsActive = true);
        //database.insert(pbEntryPetrol,false);
         
        pbeList1 = new List<PricebookEntry>();
        pbeList1.add(pbEntryPetrol11);
        product2 productPetrolObj1 = new product2(Name = 'petrol1', CurrencyIsoCode = EP_Common_Constant.GBP,
                                                isActive = true,family=EP_Common_Constant.Hydrocarbon_Liquid,
                                                Eligible_for_Rebate__c = true,EP_Blended__c = false,
                                                EP_Company_Lookup__c = comp.id,EP_Unit_of_Measure__c = 'HL',EP_Product_Sold_As__c=EP_Common_Constant.BULK_ORDER_PRODUCT_CAT,EP_Is_System_Product__c = FALSE);                           
        database.insert(productPetrolObj1);
        
        
        PricebookEntry pbEntry1= new PricebookEntry(
        Pricebook2Id = customPB.Id,EP_Is_Sell_To_Assigned__c = true, Product2Id = productPetrolObj1.Id,
        UnitPrice = 12000, IsActive = true);
        pbeList1.add(pbEntry1);
        
        product2 productPetrolObj2 = new product2(Name = 'petrol2', CurrencyIsoCode = EP_Common_Constant.GBP,
                                                isActive = true,family=EP_Common_Constant.Hydrocarbon_Liquid,
                                                Eligible_for_Rebate__c = true,EP_Blended__c = false,EP_Company_Lookup__c = comp.id
                                                ,EP_Unit_of_Measure__c = 'LT',EP_Product_Sold_As__c=EP_Common_Constant.BULK_ORDER_PRODUCT_CAT,EP_Is_System_Product__c = FALSE);                           
        database.insert(productPetrolObj2);
       
        
        //database.insert(pbeList);
        PricebookEntry pbEntry2= new PricebookEntry(
        Pricebook2Id = customPB.Id, EP_Is_Sell_To_Assigned__c = true, 
        Product2Id = productPetrolObj2.Id,
        UnitPrice = 12000, IsActive = true);
        //pbeList1.add(pbEntry2);
        database.insert(pbeList1);
        EP_Region__c region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        Database.insert(region,false);
        
        Account  sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        sellToAccount.EP_Puma_Company__c = comp.id;
        sellToAccount.ownerID = userMap.get('Puma SM Agent_R1').id;
        sellToAccount.EP_Requested_Payment_Terms__c='PrePayment';
        sellToAccount.EP_Requested_Packaged_Payment_Term__c='PrePayment';
        sellToAccount.EP_Alternative_Payment_Method__c  = 'Cash Payment';
        Database.insert(sellToAccount);
        
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true limit 1];
 
        Account storageLoc = EP_TestDataUtility.createStorageLocAccount(country.Id,bhrs.Id);
        Account TransporterRec = EP_TestDataUtility.createTestVendor('Transporter','testNAV','AUN1');
        Account SupplierRec = EP_TestDataUtility.createTestVendor('3rd Party Stock Supplier','testNAV','AUN');
        
        
        EP_Payment_Term__c paymentTerm =  EP_TestDataUtility.createPaymentTerm();
        insert paymentTerm;

        Contact  contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);  
        Contract contractRec = EP_TestDataUtility.createContract(storageLoc.Id,comp.Id,paymentTerm.Id,storageLoc);
        
        ID sellToSHLRTID = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName('EP_Stock_Holding_Location__c', 'Ex_Rack_Supply_Location');
        //ID shipToToSHLRTID= EP_Common_Util.getRecordTypeIdForGivenSObjectAndName('EP_Stock_Holding_Location__c', 'Delivery_Supply_Location');
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,sellToSHLRTID); //Add one SHL
        SHL1.EP_Supplier__c = SupplierRec.id;
        SHL1.EP_Purchase_Contract__c = contractRec.Id;
        SHL1.EP_Is_Pickup_Enabled__c= true;
        SHL1.EP_Supplier_Contract_Advice__c='None';
        SHL1.EP_Use_Managed_Transport_Services__c = 'N/A';
        SHL1.EP_Transporter__c = TransporterRec.Id ;
        SHL1.EP_Duty__c = 'Excise Paid';
        //system.debug('--SHL1.EP_Transporter__c--'+ SHL1.EP_Transporter__c);
        Database.saveResult s12 = Database.insert(SHL1,false);
       // system.assertEquals('', s12.getErrors()[0].getMessage());
        system.debug('--s12--'+ s12);
        
        system.assert(s12.isSuccess());
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status to basic data setup
        sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
        sellToAccount.EP_PriceBook__c = customPB.id;
       
        Database.saveResult sv = Database.update(sellToAccount);
        //Create Bank Acc
        system.debug(sv+'selllllllllllllllll'+sellToAccount.EP_PriceBook__c);
        EP_Bank_Account__c bankAcc  = EP_TestDataUtility.createBankAccount(sellToAccount.id);
        for(UserRole r : [Select id,developerName  from UserRole where developerName in: userRole]){
            userRoleMap.put(r.developerName,r);
        }
        
        //create ship to
        Account nonVMIshipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,shpToRecordTypeId);
        nonVMIshipTo.EP_Country__c = country.id;
        nonVMIshipTo.ownerID = userMap.get('Puma SM Agent_R1').id;
        nonVMIshipTo.EP_Transportation_Management__c = 'Puma Planned';
        nonVMIshipTo.EP_Eligible_for_Rebate__c = true;
        nonVMIshipTo.EP_Ship_To_Type__c = 'Consignment';
        Database.insert(nonVMIshipTo);
        //create SHL
        EP_Stock_holding_location__c SHL2 = EP_TestDataUtility.createShipToStockLocation(nonVMIshipTo.id,true,null,sellToSHLRTID); //Add one SHL
        Database.insert(SHL2,false);
        //create tank
        EP_Tank__c tank1 = EP_TestDataUtility.createTestEP_Tank(nonVMIshipTo.id, productPetrolObj.id);
        
        Account vmiShipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null);
        vmiShipTo.RecordTypeId =  EP_Common_Util.fetchRecordTypeId('Account','VMI Ship To');
        vmiShipTo.EP_Country__c = country.id;
        vmiShipTo.ownerID = userMap.get('Puma SM Agent_R1').id;
        vmiShipTo.EP_Transportation_Management__c = 'Puma Planned';
        vmiShipTo.EP_Eligible_for_Rebate__c = false;
        vmiShipTo.EP_Puma_company__c =  comp.id;
        vmiShipTo.EP_Ship_To_Type__c = 'Non-Consignment';
        Database.insert(vmiShipTo);
        //Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();  
        //create tank
        EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(vmiShipTo.id, productPetrolObj.id);
        database.insert(new List<EP_Tank__C>{tank1,tank});
        //Update ship to stats to basic data setup
        vmiShipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        nonVMIshipTo.EP_Status__c  =    EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        vmiShipTo.EP_Status__c  =   EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        
        sellToAccount.EP_Status__c  =   EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        accounts = new List<account>{sellToAccount};//,nonVMIshipTo, vmiShipTo
        database.update(accounts);
    }

    static testMethod void tankProductUomTest(){
        userMap = new Map<String, User>();
        String Str_Lkid = '_lkid';
        for(User u : [Select id, Name, Profile.Name from User Where Profile.Name in :profileName]){
          userMap.put(u.Profile.Name, u);
        }
        Map<String, Account> accountMap = New Map<String, Account>();
        for(Account acc : [Select id, Name, RecordType.Name,OwnerID, RecordType.DeveloperName, 
                                  EP_Status__c,EP_PriceBook__c,
                                  EP_Puma_Company__c, 
                                    
                                  (Select id,Name From Tank__r)from Account limit 10]){
            accountMap.put(acc.RecordType.DeveloperName, acc);  
        }
         EP_Tank__c tnk = new EP_Tank__c(); 
         //EP_Tank__c tnk1 = new EP_Tank__c(EP_Safe_Fill_Level__c = 50, EP_Deadstock__c = 10, EP_Ship_To__c);
         //insert tnk1;
        system.runAs(userMap.get(EP_Common_Constant.CSC_AGENT_PROFILE)){
            Test.startTest();
            PageReference pageRef = Page.EP_Tank_Product_Page;
            Test.setCurrentPage(pageRef);
            //Set Sell To apage Variables
            //system.assertEquals('--', string.valueOf(accountMap));
            //system.debug('===accountMap======='+ accountMap.get(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME)); 
            ApexPages.currentPage().getParameters().put('Id', accountMap.get(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME).Tank__r[0].id);
            EP_TankDetailPageController controller = new EP_TankDetailPageController(new ApexPages.StandardController(accountMap.get(EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME).Tank__r[0]));
            system.assert(controller.blnShowSaveButton);
            controller.fetchProductValues();
            //controller.updateUOM();
            
            ApexPages.currentPage().getParameters().put('Id', null);
            ApexPages.currentPage().getParameters().put(label.EP_Ship_To_Field_Id_for_Tank_Page+Str_Lkid, accountMap.get(EP_Common_COnstant.VMI_SHIP_TO_DEV_NAME).id);
            EP_TankDetailPageController controller1 = new EP_TankDetailPageController(new ApexPages.StandardController(tnk));
            controller1.blnShowSaveButton = null;
            controller1.fetchProductValues();
           // Product2 prod =[select id,name,EP_Unit_of_Measure__c from product2 where id =: controller1.tnk.EP_Product__c];
            //controller.updateUOM();
            system.assert(controller.ProductValues.isEmpty());
            
           // system.assert(string.isNotBlank(controller1.tnk.EP_UNIT_OF_MEASURE__C));
            
        }
    }

    static testMethod void returnPriceBookIds_test(){
        EP_Tank__c tnk = new EP_Tank__c(); 
        EP_TankDetailPageController controller = new EP_TankDetailPageController(new ApexPages.StandardController(tnk));
        Account acc = EP_TestDataUtility.getSellTo();
        EP_TestDataUtility.createCompany('123');
        system.debug('EP_Puma_Company__c = '+acc.EP_Puma_Company__c);
        Pricebook2 testCustomPricebook = new Pricebook2(Name='myPB123', isActive = TRUE, EP_Company__c = acc.EP_Puma_Company__c);
        Database.insert(testCustomPricebook);
        EP_Product_Option__c obj = EP_TestDataUtility.createProductOption(acc.id,testCustomPricebook.id);
        List<EP_Product_Option__c> listProduct = new List<EP_Product_Option__c>{obj};
        Test.startTest();
        set<id> idset = controller.returnPriceBookIds(listProduct);
        Test.stopTest();
        system.assert(idset!=null);
    }
    
    static testMethod void getPriceBookIds_test(){
        EP_Tank__c tnk = new EP_Tank__c(); 
        EP_TankDetailPageController controller = new EP_TankDetailPageController(new ApexPages.StandardController(tnk));
        Account acc = EP_TestDataUtility.getSellTo();
        EP_TestDataUtility.createCompany('123');
        system.debug('EP_Puma_Company__c = '+acc.EP_Puma_Company__c);
        Pricebook2 testCustomPricebook = new Pricebook2(Name='myPB123', isActive = TRUE, EP_Company__c = acc.EP_Puma_Company__c);
        Database.insert(testCustomPricebook);
        EP_Product_Option__c obj = EP_TestDataUtility.createProductOption(acc.id,testCustomPricebook.id);
        List<EP_Product_Option__c> listProduct = new List<EP_Product_Option__c>{obj};
        Test.startTest();
        set<id> idset =  controller.getPriceBookIds(acc.id);
        Test.stopTest();
        system.assert(idset!=null);
    }
}