/*   
     @Author <Sanchit Dua>
     @name <EP_IntegrationUtil_Test.cls>   
     @CreateDate <19/11/2015>   
     @Description <This class is used to cover the class EP_IntegrationUtil.>   
     @Version <1.1> 
     Revision updates:
     Added 3 methods for ok by jyotsna yadav
     Added 3 methods for error handling by <<Sanchit Dua>>
*/  
@isTest
private class EP_IntegrationUtil_Test {
    //static member variable to be used as test data
    private static Account acc;
    private static String txId;
    private static String msgId;
    private static String company;
    private static DateTime dt_sent; 
    private static final String TESTREC = 'test';
    private static final String SENTERROR = 'ERROR-SENT';
    private static final String SYNCERROR = 'ERROR-SYNC';
    private static final String INITIAL_VAL = '';
    
    private static final Integer LMT = Limits.getLimitQueries();
    
    /*
     * Description: creating the sample test data
     * @author: sanchit dua
    */
    private static void createSampleData(){
        EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
        insert country;
        acc = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
        BillingCountry='in', ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in',Phone = '123456');
        insert acc; 
        txId = EP_IntegrationUtil.getTransactionID(TESTREC, TESTREC);    
        msgId = EP_IntegrationUtil.getMessageId(TESTREC); 
        company = TESTREC;   
        dt_sent = DateTime.now();  
    }
     
        /**
       Method to create test data.
      **/ 
     private static void createTestData(){
         
             EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
             insert country;
             acc = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
             BillingCountry='in', ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
             ShippingCountry='in');
             insert acc; 
             txId = EP_IntegrationUtil.getTransactionID('Test','create');    
             msgId = EP_IntegrationUtil.getMessageId('Test'); 
             company = 'test company';   
             dt_sent = DateTime.now();    
             
     }
    
     /**
       Method to test the creation of integration records if multiple record are sent to external system
      **/     
     static testMethod void sendBulkOk_Test(){
         //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
             EP_TestDataUtility.createStatusList();
             createTestData();
             List<Id> objIdList = new List<Id>();
             EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
             insert country;
             Account acc1 = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
             BillingCountry='in',ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in');
            
             insert acc1; 
             objIdList.add(acc.Id); 
             objIdList.add(acc1.Id); 
             List<String> objTypeList = new List<String>{'Accounts','Accounts'};
             test.startTest();
             // List<Id> objIdList,List<String> objTypeList,String transacionId,String msgId,String company,DateTime dtSent
             EP_IntegrationUtil.sendBulkOk(objIdList,objTypeList,txId,msgId,company,dt_sent, TESTREC);
             test.stopTest();
             List<EP_IntegrationRecord__c> intRecList = [Select EP_Status__c from EP_IntegrationRecord__c where EP_Object_ID__c=:objIdList LIMIT: LMT];
             system.assertEquals(2,intRecList.size());
             system.assertEquals('SENT',intRecList[0].EP_Status__c);
             system.assertEquals('SENT',intRecList[1].EP_Status__c); 
         }        
     }
     
     /*
     @Description Method to test send bulk data
     */
     static testMethod void sendBulkOk2_Test(){
         //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
             EP_TestDataUtility.createStatusList();
             createTestData();
             List<Id> objIdList = new List<Id>();
             EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
             insert country;
             Account acc1 = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
             BillingCountry='in',ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in');
            
             insert acc1; 
             objIdList.add(acc.Id); 
             objIdList.add(acc1.Id); 
             List<String> objTypeList = new List<String>{'Accounts','Accounts'};
             test.startTest();
             // List<Id> objIdList,List<String> objTypeList,String transacionId,String msgId,String company,DateTime dtSent
//             EP_IntegrationUtil.sendBulkOk(objIdList,objTypeList,txId,msgId,company,dt_sent, TESTREC,new map<String,String>());
             test.stopTest();
         }        
     }
     
     /*
     @Description Method to test bulk Error
     */
     static testMethod void syncBulkError_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationRecord__c intRecord = new EP_IntegrationRecord__c();
            intRecord.EP_Object_ID__c = acc.Id;         
            intRecord.EP_Message_ID__c = msgId;         
            intRecord.EP_DT_SENT__c = dt_Sent;  
            intRecord.EP_Target__c = TESTREC;   
            intRecord.EP_Object_Type__c = 'Accounts';     
            intRecord.EP_Status__c = 'SENT';
            intRecord.EP_IsLatest__c = true;
            intRecord.EP_Source__c = TESTREC;
            insert intRecord;
            EP_IntegrationUtil.syncBulkError(msgId
                                ,True
                                ,new set<String>()
                                ,new map<String,String>{acc.Id=>acc.Id}
                                ,new map<String,String>{acc.Id=>acc.Id});   
         
        }
     }
     /*
     @Description sync Bulk data
     */
     static testMethod void syncBulkOk_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationRecord__c intRecord = new EP_IntegrationRecord__c();
            intRecord.EP_Object_ID__c = acc.Id;         
            intRecord.EP_Message_ID__c = msgId;         
            intRecord.EP_DT_SENT__c = dt_Sent;  
            intRecord.EP_Target__c = TESTREC;   
            intRecord.EP_Object_Type__c = 'Accounts';     
            intRecord.EP_Status__c = 'SENT';
            intRecord.EP_Source__c = TESTREC;
            intRecord.EP_IsLatest__c = true;
            insert intRecord;
            EP_IntegrationUtil.syncBulkOk(msgId
                                ,True
                                ,new set<String>()
                                ,new set<String>{acc.Id});   
         
        }
     } 
    
    /*
    Method to create Integration record Inbound
    */
    static testMethod void createIntegrationRecordInbound_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationUtil.createIntegrationRecordInbound(acc.Id, 'Accounts',msgId,TESTREC,'',datetime.now(), 'SENT','');  
            
        }
     } 
     /*
     @Description Sync Data Test 
     */
     static testMethod void syncOk_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationRecord__c intRecord = EP_IntegrationUtil.createIntegrationRecordInbound(acc.Id, 'Accounts',msgId,TESTREC,'',datetime.now(), 'SENT','');  
            insert intRecord;
            EP_IntegrationUtil.syncOk(acc.Id,msgId);
        }
     }
     /*
     @Description Resend Data Test 
     */
     static testMethod void reSendOk_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationRecord__c intRecord = EP_IntegrationUtil.createIntegrationRecordInbound(acc.Id, 'Accounts',msgId,TESTREC,'',datetime.now(), 'SENT','');  
            insert intRecord;
            EP_IntegrationUtil.reSendOk(msgId);
        }
     }
     /*
     @Description ResendError Data Test 
     */
     static testMethod void reSendError_Test(){
        String errorDescription = 'Functional Error';
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationRecord__c intRecord = EP_IntegrationUtil.createIntegrationRecordInbound(acc.Id, 'Accounts',msgId,TESTREC,'',datetime.now(), 'SENT','');  
            insert intRecord;
            EP_IntegrationUtil.reSendError(msgId,errorDescription);
        }
     }
     /*
     @Description Received Data Test
     */
     static testMethod void receivedOK_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationUtil.receivedOK(acc.Id, 'Accounts',msgId,datetime.now(),TESTREC,'');  
            //EP_IntegrationUtil.syncOk(acc.Id,msgId);
        }
     }
     /*
     @Description Recieved Error Test Data
     */
     static testMethod void receivedError_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            EP_IntegrationUtil.receivedError(acc.Id, 'Accounts',msgId,'',datetime.now(),TESTREC,'');  
            //EP_IntegrationUtil.syncOk(acc.Id,msgId);
        }
     }
    
    /**************************************************************
     * Test Method for EP_AccountTriggerHelper, accountStatusToActive Method
     * 
     * ************************************************************/
     static testMethod void accountStatusToActive_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            setupData();
            //Change the status on the account
            acc.EP_Status__c = EP_Common_Constant.STATUS_INACTIVE;
            acc.RecordTypeID = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO);
            acc.EP_Account_Synced_with_NAV__c = True;
            acc.EP_Account_Synced_With_winDms__c = True;
            //update acc;
            //EP_AccountTriggerHelper.accountStatusToActive(new Account[]{acc});
            //EP_AccountTriggerHelper.callSfdcToNav('TestPayload','TestCompany');
        }
    }
    
    static void setupData(){
        
        // Assign at least supply location 
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(acc.id,true,null,null); //Add one SHL          
        Database.insert(SHL1,false);
        //Complete the pending actions on account.
        completeActions(acc.id);
        //Complete the Pricing Review action
        completeActions(acc.id, 'EP_Pricing_Review');
        acc.RecordTypeID = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO);
        update acc;
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(acc.Id,prod.Id);
        tankObj.EP_Tank_Status__c = 'New';
        insert tankObj;
        acc.EP_Is_Valid_Address__c = true;
        acc.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        acc.EP_Email__c = 'Salesforceuser@salesforce.com';
        update acc;
    }
    
    static void completeActions(Id accountId){      
        list<EP_Action__c> lActions = new list<EP_Action__c>();
        lActions = [Select id,EP_Account__c,Owner.Name,EP_Status__c,recordType.developerName,EP_Tank__c from EP_Action__c Where EP_Account__c =:accountId];
        //ACTIONS CANNNOT BE MODIFIED WITHOUT ASSIGNING OWNER
        for(EP_Action__c actionPending : lActions){
            actionPending.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;               
        }
        DataBase.SaveResult[] lActionInQueueUpdtErrRes = DataBase.update(lActions,false);
    }
    
    static void completeActions(Id accountId, string recordTypeDevName){        
        list<EP_Action__c> lActions = new list<EP_Action__c>();
        lActions = [Select id,EP_Account__c,Owner.Name,EP_Status__c,recordType.developerName,EP_Tank__c from EP_Action__c Where EP_Account__c =:accountId AND recordType.developerName =: recordTypeDevName];
        //ACTIONS CANNNOT BE MODIFIED WITHOUT ASSIGNING OWNER
        for(EP_Action__c actionPending : lActions){
            actionPending.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;               
        }
        DataBase.SaveResult[] lActionInQueueUpdtErrRes = DataBase.update(lActions,false);
    }   
}