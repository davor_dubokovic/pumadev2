@isTest
public class EP_ProductMapper_UT
{
    static String PRODUCT_INFO = 'petrol';

    static testMethod void getRecordsProductCode_test() {
        EP_ProductMapper localObj = new EP_ProductMapper();
        EP_TestDataUtility.getSellTo();
        Set<string> productCodeSet = new Set<string>();
        productCodeSet.add(PRODUCT_INFO);   
        Test.startTest();
        LIST<Product2> result = localObj.getRecordsProductCode(productCodeSet);
        Test.stopTest();
        System.assertEquals(1,result.size());
    }
    static testMethod void getProductSoldAsInfo_test() {
        EP_ProductMapper localObj = new EP_ProductMapper();
        EP_TestDataUtility.getSellTo();
        Set<string> itemIdSet = new Set<string>();
        itemIdSet.add(PRODUCT_INFO);
        Test.startTest();
        Map<string,string> result = localObj.getProductSoldAsInfo(itemIdSet);
        Test.stopTest();
        System.assertEquals(1,result.size());
    }
    static testMethod void getRecordsByName_test() {
        EP_ProductMapper localObj = new EP_ProductMapper();
        EP_TestDataUtility.getSellTo();
        Set<string> productNameSet = new Set<string>();
        Product2 prod = EP_TestDataUtility.createProduct2(false);
        prod.EP_Is_System_Product__c = true;
        insert prod;
        productNameSet.add(prod.Name);       
        Test.startTest();
        LIST<Product2> result = localObj.getRecordsByName(productNameSet);
        Test.stopTest();
        System.assertEquals(1,result.size());
    }
}