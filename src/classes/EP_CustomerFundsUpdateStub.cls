/* 
   @Author          Accenture
   @name            EP_CustomerFundsUpdateStub
   @CreateDate      03/10/2017
   @Description     Stub class to update available funds of customer i.e. inbound interface to update customers funds
   @Novasuite fix Comments added
   @Version         1.0
*/
public class EP_CustomerFundsUpdateStub {
    public MSG msg;
    /* 
    @Author          Accenture
    @name            constructor of EP_CustomerFundsUpdateStub
    */
    public EP_CustomerFundsUpdateStub(){
        msg = new MSG();
    }
    /* 
    @Author          Accenture
    @name            Inner class MSG
    */
    public class MSG {
        public EP_MessageHeader HeaderCommon;
        public Payload Payload;
        public String StatusPayload;    //StatusPayload
         /* 
        @Author          Accenture
        @name            constructor of Inner class MSG
        */
        public MSG(){
            HeaderCommon =new EP_MessageHeader();
            Payload = new Payload();
        }
    }
    /* 
    @Author          Accenture
    @name            Inner class Payload
    */
    public class Payload {
        public Any0 any0;
        /* 
        @Author          Accenture
        @name            Constructor of Payload
        */
        public Payload(){
        	any0 = new Any0();
        }
    }
    /* 
    @Author          Accenture
    @name            Inner class Any0
    */
    public class Any0 {
        public AvailableFunds availableFunds;
        /* 
        @Author          Accenture
        @name            constructor of Any0
        */
        public Any0(){
        	availableFunds = new AvailableFunds();
        }
    }
    /* 
    @Author          Accenture
    @name            Inner class AvailableFunds 
    */
    public class AvailableFunds {
        public List<Customer> customer;
        /* 
        @Author          Accenture
        @name            constructor of AvailableFunds 
        */
        public AvailableFunds(){
        	customer = new List<Customer>();
        }
    }
    /* 
    @Author          Accenture
    @name            Inner class Customer
    */
    public class Customer {
        public Account SFAccount;
        public Account holdingAccount;//L4# 67333
        public string errorDescription;
        public String seqId;    
        /*45352 CAM 2.7 start*/  
        //public String availableAmt;//L4# 67333 
        public String overdueAmt;
        
        //public String availableAmtCustCurr;//L4# 67333        
        public String overdueAmtCustCurr;
        public String balanceCustCurr;
        public String balance;
        public String versionNr;
        /*45352 CAM 2.7 end */
        public String currencyId ;
        public Identifier identifier {get;set;}
        public HoldingCreditInfo creditHoldingInfo{get;set;}//L4# 67333    
        /* 
        @Author          Accenture
        @name            constructor of Customer
        */
        public Customer(){
            identifier = new Identifier();
            creditHoldingInfo = new HoldingCreditInfo();//L4# 67333    
        }

    }
    /* 
    @Author          Accenture
    @name            Inner class Identifier 
    */
    public class Identifier {
        public String billTo;
        public String clientId; 
    }
    
    //L4# 67333 Code Changes Start
    /* 
    @Author          Accenture
    @name            Inner class HoldingCreditInfo 
    */
    public class HoldingCreditInfo {
        public String creditHoldingId;
        public String availableAmt;
        public String availableAmtCustCurr;
        public String overDueBalance;
        public String overDueBalanceCustCurr;       
    }
    
    //L4# 67333 Code Changes End
}