@isTest
public class EP_OSTVmiNonConsignPlannedToPlanned_UT
{

static testMethod void isTransitionPossible_positive_test() {
	EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
	EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
	EP_OSTVmiNonConsignPlannedToPlanned ost = new EP_OSTVmiNonConsignPlannedToPlanned();
	ost.setOrderContext(obj,oe);
	Test.startTest();
	boolean result = ost.isTransitionPossible();
	Test.stopTest();
	System.AssertEquals(true,result);
}


static testMethod void isRegisteredForEvent_positive_test() {
	EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
	EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
	EP_OSTVmiNonConsignPlannedToPlanned ost = new EP_OSTVmiNonConsignPlannedToPlanned();
	ost.setOrderContext(obj,oe);
	Test.startTest();
	boolean result = ost.isRegisteredForEvent();
	Test.stopTest();
	System.AssertEquals(true,result);
}

static testMethod void isGuardCondition_positive_test() {
	EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
	EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
	EP_OSTVmiNonConsignPlannedToPlanned ost = new EP_OSTVmiNonConsignPlannedToPlanned();
	ost.setOrderContext(obj,oe);
	Test.startTest();
	boolean result = ost.isGuardCondition();
	Test.stopTest();
	System.AssertEquals(true,result);
}

static testMethod void isGuardCondition_creditIssuePositive_test() {
	EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
	obj.getorder().EP_Pricing_Status__c = EP_Common_constant.PRICING_STATUS_PRICED;
	obj.getorder().EP_Sync_with_NAV__c = false;
	EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
	EP_OSTVmiNonConsignPlannedToPlanned ost = new EP_OSTVmiNonConsignPlannedToPlanned();
	ost.setOrderContext(obj,oe);
	Test.startTest();
	boolean result = ost.isGuardCondition();
	Test.stopTest();
	System.AssertEquals(true,result);
}

static testMethod void isGuardCondition_creditIssueNegative_test() {
	EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
	List<csord__Order_Line_Item__c> ordItemLst = new List<csord__Order_Line_Item__c>();
	csord__Order__c orderIns = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario();
	orderIns.EP_Pricing_Status__c = EP_Common_constant.PRICING_STATUS_PRICED;
	orderIns.EP_Sync_with_NAV__c = false;
	for(csord__Order_Line_Item__c oItem: orderItemMapper.getCSOrderItemsByOrderNumber(orderIns.OrderNumber__c)){
            oItem.EP_Prior_Quantity__c = 10;
            oItem.Quantity__c =20;
            ordItemLst.add(oItem);
    }
    update orderIns;
    update ordItemLst;
    EP_OrderDomainObject obj =new EP_OrderDomainObject(orderIns.Id);
    System.debug('oItem--'+orderItemMapper.getCSOrderItemsByOrderNumber(obj.getorder().OrderNumber__c));
	EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
	EP_OSTVmiNonConsignPlannedToPlanned ost = new EP_OSTVmiNonConsignPlannedToPlanned();
	ost.setOrderContext(obj,oe);
	Test.startTest();
	boolean result = ost.isGuardCondition();
	Test.stopTest();
	System.AssertEquals(false,result);
}


static testMethod void isGuardCondition_negative_test() {
	EP_OrderDomainObject obj = EP_TestDataUtility.getOrderNegativeTestScenario();
	obj.getorder().EP_Pricing_Status__c = 'Dummy Status';
	EP_OrderEvent oe = new EP_OrderEvent('UserSubmit');
	EP_OSTVmiNonConsignPlannedToPlanned ost = new EP_OSTVmiNonConsignPlannedToPlanned();
	ost.setOrderContext(obj,oe);
	Test.startTest();
	boolean result = ost.isGuardCondition();
	Test.stopTest();
	System.AssertEquals(false,result);
}


}