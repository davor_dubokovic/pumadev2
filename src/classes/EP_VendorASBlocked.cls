/*
 *  @Author <Accenture>
 *  @Name <EP_VendorASBlocked>
 *  @CreateDate <>
 *  @Description <Vendor Account State for 06-Blocked Status>
 *  @Version <1.0>
 */
 public with sharing class EP_VendorASBlocked extends EP_AccountState{
     
    public EP_VendorASBlocked() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_VendorASBlocked','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_VendorASBlocked','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_VendorASBlocked','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_VendorASBlocked','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_VendorASBlocked','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}