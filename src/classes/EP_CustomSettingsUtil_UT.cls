@isTest
public class EP_CustomSettingsUtil_UT
{   
    @testSetup static void init() {
    List<EP_CS_OutboundMessageSetting__c> msgSettingList = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    List<EP_Integration_Status_Update__c> integrationStatusUpdateList = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    List<EP_OrderStatusNAVMapping__c> orderStatusNavMappingList = Test.loadData(EP_OrderStatusNAVMapping__c.sObjectType, 'EP_OrderStatusUpdateNAV');
    }
    public static final string MESSAGE_TYPE ='SEND_CREDIT_EXCEPTION_REQUEST'; 
    EP_CS_OutboundMessageSetting__c result;
    static testMethod void getOutboundMessageSetting_test() {
        String messageType = MESSAGE_TYPE ;
        Test.startTest();
        EP_CS_OutboundMessageSetting__c result = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        System.Assert(result != NULL);//check
        // **** TILL HERE ~@~ *****
    }
    static testMethod void createIntgStatusByObjAPIName_test() {
        Test.startTest();
        Map<String,String> result = EP_CustomSettingsUtil.createIntgStatusByObjAPIName();
        Test.stopTest();
        System.AssertEquals(true,result.size()>0);
        
    }
    static testMethod void getOrderStatusFromCustomSetting_test() {
        Test.startTest();
        Map< String, String > result = EP_CustomSettingsUtil.getOrderStatusFromCustomSetting();
        Test.stopTest();
        System.AssertEquals(true,result.size()>0);
        
    }
}