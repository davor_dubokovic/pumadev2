/*
 *  @Author <Accenture>
 *  @Name <EP_StorageLocationASAccountSetup >
 *  @CreateDate <6/3/2017>
 *  @Description < Account State for  04-Account Set-up Status>
 *  @Version <1.0>
 */
 public with sharing class EP_StorageLocationASAccountSetup extends EP_AccountState{
     
    public EP_StorageLocationASAccountSetup() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASAccountSetup','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASAccountSetup','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASAccountSetup','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASAccountSetup','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_StorageLocationASAccountSetup','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    } 
}