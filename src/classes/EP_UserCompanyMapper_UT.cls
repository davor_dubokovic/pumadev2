@isTest
private class EP_UserCompanyMapper_UT {
	
	static testMethod void getAssociatedUserListpositive_test(){
		set<User> userList = new set<User>();
		company__c company = EP_TestDataUtility.createCompany('1234');
		insert company;
		set<id> companySet = new set<id>();
		companySet.add(company.id);
		EP_User_Company__c userCompObj = EP_TestDataUtility.createUserCompany(UserInfo.getUserId(),company.id);
		EP_UserCompanyMapper localObj = new EP_UserCompanyMapper();
		Test.startTest();
		userList = localObj.getAssociatedUserList(companySet);
		Test.stopTest();
		system.assertEquals(true,!userList.isEmpty());
	}

	static testMethod void getAssociatedUserListnegative_test(){
		set<User> userList = new set<User>();
		company__c company = EP_TestDataUtility.createCompany('1234');
		insert company;
		set<id> companySet = new set<id>();
		EP_User_Company__c userCompObj = EP_TestDataUtility.createUserCompany(UserInfo.getUserId(),company.id);
		EP_User_CompanyDomainObject userCompDomain = new EP_User_CompanyDomainObject(userCompObj);
		EP_UserCompanyMapper localObj = new EP_UserCompanyMapper();
		Test.startTest();
		userList = localObj.getAssociatedUserList(companySet);
		Test.stopTest();
		system.assertEquals(false,!userList.isEmpty());
	}
}