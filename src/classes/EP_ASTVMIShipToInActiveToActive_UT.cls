@isTest
public class EP_ASTVMIShipToInActiveToActive_UT
{
    static final string EVENT_NAME = '07-InactiveTo05-Active';
    static final string INVALID_EVENT_NAME = '08-ProspectTo08-Rejected';
    
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    } 
    
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTVMIShipToInActiveToActive ast = new EP_ASTVMIShipToInActiveToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToInActiveToActive ast = new EP_ASTVMIShipToInActiveToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTVMIShipToInActiveToActive ast = new EP_ASTVMIShipToInActiveToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTVMIShipToInActiveToActive ast = new EP_ASTVMIShipToInActiveToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASInactiveDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTVMIShipToInActiveToActive ast = new EP_ASTVMIShipToInActiveToActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    
}