@isTest
public class EP_OrderType_UT
{

static testMethod void doSubmitActions_test() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    localObj.doSubmitActions();
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,localobj);
    // **** TILL HERE ~@~ *****
}
static testMethod void setOrderDomain_test() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
    EP_OrderDomainObject porderDomainObj = new EP_OrderDomainObject(orderObj);
    // **** TILL HERE ~@~ *****
    Test.startTest();
    localObj.setOrderDomain(porderDomainObj);   
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(NULL,porderDomainObj.orderEpochType);
    System.AssertNotEquals(NULL,porderDomainObj.delType);
    System.AssertNotEquals(NULL,porderDomainObj.psaType);
    System.AssertNotEquals(NULL,porderDomainObj.consignType);   
    // **** TILL HERE ~@~ *****
}
static testMethod void doUpdatePaymentTermAndMethod_test() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    localObj.doUpdatePaymentTermAndMethod();
    Test.stopTest();
    // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
    // **** IMPLEMENT ASSERT ~@~ *****
    //System.AssertEquals(true,<asset conditions>);
    // **** TILL HERE ~@~ *****
}
static testMethod void hasCreditIssue_PositiveScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    Boolean result = localObj.hasCreditIssue();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void hasCreditIssue_NegativeScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    Boolean result = localObj.hasCreditIssue();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void getShipTos_test() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Id accountId;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    LIST<Account> result = localObj.getShipTos(accountId);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void getOperationalTanks_test() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Id accountId = EP_TestDataUtility.getShipTo().Id;
    localObj.deliverytype =  new EP_DeliveryType();
    localObj.productSoldAsType = new EP_ProductSoldAs();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Map<Id, EP_Tank__c> result = localObj.getOperationalTanks(accountId);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void isValidLoadingDate_PositiveScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isValidLoadingDate(orderObj);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void isValidLoadingDate_NegativeScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isValidLoadingDate(orderObj);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void isValidExpectedDate_PositiveScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isValidExpectedDate(orderObj);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void isValidExpectedDate_NegativeScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isValidExpectedDate(orderObj);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void getAllRoutesOfShipToAndLocation_test() {
    EP_OrderType localObj = new EP_OrderType();
    csord__Order__c orderObject = EP_TestDataUtility.getCurrentOrder();
    String shipId = orderObject.EP_ShipTo__c;
    String shlId = orderObject.EP_Stock_Holding_Location__c;
    Test.startTest();
    LIST<EP_Route__c> result = localObj.getAllRoutesOfShipToAndLocation(shipId,shlId);
    Test.stopTest();
    System.AssertEquals(true,result.size() == 0);
}
static testMethod void showCustomerPO_PositiveScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    localObj.deliveryType = new EP_DeliveryType();
    Test.startTest();
    Boolean result = localObj.showCustomerPO();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void showCustomerPO_NegativeScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    localObj.deliveryType = new EP_DeliveryType();
    Test.startTest();
    Boolean result = localObj.showCustomerPO();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void findRecordType_test() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    Id result = localObj.findRecordType();
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void getApplicablePaymentTerms_test() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Account accountObj = EP_TestDataUtility.getSellToPositiveScenario();
    csord__Order__c objOrder = new csord__Order__c(EP_Order_Product_Category__c= EP_Common_Constant.BULK_ORDER_PRODUCT_CAT);
    //Account accountObject = objOrder.Account;
    localObj.orderDomainObj = new EP_OrderDomainObject(objOrder);
    Test.startTest();
    LIST<String> result = localObj.getApplicablePaymentTerms(accountObj);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void getApplicableDeliveryTypes_test() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    LIST<String> result = localObj.getApplicableDeliveryTypes();
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null,result);
    // **** TILL HERE ~@~ *****
}

static testMethod void findPriceBookEntries_test() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    //Id pricebookId;
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder();
    Id pricebookId = objOrder.csord__Account__r.EP_PriceBook__c;

    // **** TILL HERE ~@~ *****
    Test.startTest();
    LIST<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void isValidOrderDate_PositiveScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isValidOrderDate(orderObj);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void isValidOrderDate_NegativeScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isValidOrderDate(orderObj);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void setRequestedDateTime_test() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrder();
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    String selectedDate = String.valueOf(System.today());
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    String availableSlots;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    csord__Order__c result = localObj.setRequestedDateTime(orderObj,selectedDate,availableSlots);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(orderObj,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void calculatePrice_test() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    localObj.calculatePrice();
    Test.stopTest();
    // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
    // **** IMPLEMENT ASSERT ~@~ *****
    //System.AssertEquals(true,<asset conditions>);
    // **** TILL HERE ~@~ *****
}
static testMethod void getDeliveryTypes_test() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    LIST<String> result = localObj.getDeliveryTypes();
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void checkForNoTransporter_test() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    String transporterName = EP_Common_Constant.NO_TRANSPORTER;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    csord__Order__c result = localObj.checkForNoTransporter(transporterName);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(localObj.orderObj,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void setRetroOrderDetails_test() {
    EP_OrderType localObj = new EP_OrderType();
    localObj.orderObj =  EP_TestDataUtility.getCurrentOrder();
    localObj.epochType = new EP_OrderEpoch();
    Test.startTest();
    localObj.setRetroOrderDetails();
    Test.stopTest();
    // This method does not need assertion since its calling parent class method and that is just logging the method (EP_GeneralUtility.Log())
    // **** IMPLEMENT ASSERT ~@~ *****
    //System.AssertEquals(true,<asset conditions>);
    // **** TILL HERE ~@~ *****
}
static testMethod void isOrderEntryValid_PositiveScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Integer numOfTransportAccount = 5;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isOrderEntryValid_NegativeScenariotest() {
    EP_OrderType localObj = new EP_OrderType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Integer numOfTransportAccount = 5;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void setConsumptionOrderDetails_test() {
    EP_OrderType localObj = new EP_OrderType();
    Test.startTest();
    localObj.setConsumptionOrderDetails();
    Test.stopTest();
    // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
    // **** IMPLEMENT ASSERT ~@~ *****
    //System.AssertEquals(true,<asset conditions>);
    // **** TILL HERE ~@~ *****
}
}