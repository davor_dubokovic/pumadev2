/* 
   @Author Spiros Markantonatos
   @name <EP_RetrieveIntegrationRecordsV4Test>
   @CreateDate <17/01/2017>
   @Description <This is the test class of the EP_RetrieveIntegrationRecordsV4 class which is implementing the API returning log records to the ePuma monitoring system>
   @Version <1.0>
 
*/
@isTest
private class EP_RetrieveIntegrationRecordsV4Test {
    
    // Properties
    private static EP_IntegrationRecord__c testIntegrationRecord;
    
    // Methods
    
    /*
    This method is used to generate a test integration record
    */
    private static void generateTestIntegrationRecord() {
    	Test.startTest();
	    	EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy = 
	                                                                    EP_TestDataUtility.createCustomerHierarchyForNAV(1);
			Account testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
			            
			testIntegrationRecord = new EP_IntegrationRecord__c();
			testIntegrationRecord.EP_Object_Type__c = EP_Common_Constant.ACCOUNTS;
			testIntegrationRecord.EP_Status__c = EP_Common_Constant.SENT_STATUS;
			testIntegrationRecord.EP_Object_ID__c = testSellToAccount.Id; 
			testIntegrationRecord.EP_Message_ID__c = 'TEST';
			testIntegrationRecord.EP_Source__c = 'TEST';
			testIntegrationRecord.EP_Target__c = 'TEST';
			
			Database.insert(testIntegrationRecord);
		Test.stopTest();
    }
    
    /*
    This method is used to callout the API
    */
    private static String simulateRestCallout(List<String> lParamNames, List<String> lParamValues) {
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
		
		req.requestURI = '/services/apexrest/v4/IntegrationRecords';
		req.httpMethod = 'GET';
		
		for (Integer i = 0; i < lParamNames.size(); i++)
		{
			req.addParameter(lParamNames[i], lParamValues[i]);
		}
		
		RestContext.request = req;
		RestContext.response = res;
		
		EP_RetrieveIntegrationRecordsV4.retrieveIntegrationRecords();
		
		String strResponse = res.responseBody.toString();
		
		return strResponse;
    }
    
    /*
    This method will test that the class is able to successfully construct the select statement used by the interface
    */
    private static testMethod void testInterfaceSelectStatementGeneration() {
    	
    	// The API class should return the query select statement without an error (method returns BLANK when an error has occured)
    	String strSelectStatement = EP_RetrieveIntegrationRecordsV4.getObjectFullSelectStatement();
    	System.assertNotEquals(EP_Common_Constant.BLANK, strSelectStatement);
    	
    }
    
    /*
    This method will test the happy path where the system is meant to return a single integration record
    */
    private static testMethod void testSuccessfullInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dToday = System.Today();
		Date dYesterday = System.Today().addDays(-1);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Yesterday
		lParamValues.add(String.valueOf(dYesterday));
		// Param 2 = Today
		lParamValues.add(String.valueOf(dToday));
		// Param 3 = 100
		lParamValues.add('100');
		
		// Generate the test integration record
		generateTestIntegrationRecord();
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		// The response should contain the ID of the test integration record
		System.assertEquals(TRUE, (strResponse.indexOf(testIntegrationRecord.Id) > -1));
		
    }
    
    /*
    This method will not return any records as the query limit is set to zero
    */
    private static testMethod void testZeroLimitInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dToday = System.Today();
		Date dYesterday = System.Today().addDays(-1);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Yesterday
		lParamValues.add(String.valueOf(dYesterday));
		// Param 2 = Today
		lParamValues.add(String.valueOf(dToday));
		// Param 3 = 0
		lParamValues.add('0');
		
		// Generate the test integration record
		generateTestIntegrationRecord();
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		// The response should contain NULL records
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.NULL_VAR) > -1));
		
    }
    
    /*
    This method will attempt to return records created in the past. Since there are no records it should not return any results
    */
    private static testMethod void testPastRecordSearchInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dThreeDaysAgo = System.Today().addDays(-3);
		Date dTwoDaysAgo = System.Today().addDays(-2);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Three Days Ago
		lParamValues.add(String.valueOf(dThreeDaysAgo));
		// Param 2 = Two Days Ago
		lParamValues.add(String.valueOf(dTwoDaysAgo));
		// Param 3 = 10
		lParamValues.add('10');
		
		// Generate the test integration record
		generateTestIntegrationRecord();
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		// The response should contain NULL records
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.NULL_VAR) > -1));
		
    }
    
    /*
    This method will ensure that the interface returns the appropriate error when receiving an invalid param. This method will simulate an invalid date param
    */
    private static testMethod void testInvalidDateParamErrorInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Invalid Date
		lParamValues.add('AA-BB-CCCC');
		// Param 2 = Invalid Date
		lParamValues.add('AA-BB-CCCC');
		// Param 3 = 10
		lParamValues.add('10');
		
		// Generate the test integration record
		generateTestIntegrationRecord();
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		// The response should contain the invalid filter response
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.GENERIC_FILTER_RESPONSE_FALSE) > -1));
		
    }
    
    /*
    This method will ensure that the interface returns the appropriate error when an unexpected error occurs
    */
    private static testMethod void testUnexpectedErrorInterfaceCallout() {
		
		List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dToday = System.Today();
		Date dYesterday = System.Today().addDays(-1);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Yesterday
		lParamValues.add(String.valueOf(dYesterday));
		// Param 2 = Today
		lParamValues.add(String.valueOf(dToday));
		// Param 3 = 100
		lParamValues.add('100');
		
		// Generate the test integration record
		generateTestIntegrationRecord();
		
		// Generate an unexpected error
		EP_RetrieveIntegrationRecordsV4.strIntegrationObjectName = 'EP_IntegrationRecord123__c';
		
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		
		// The response should contain the invalid filter response
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.RESPONSE_FALSE) > -1));
		
    }
    
    /*
    This method will attempt to return records without providing the mandatory FROM DATE param. The interface should return a generic error
    */
    private static testMethod void testNoMandatoryParamsSearchInterfaceCallout() {
		
		// Param list is initialised, however no values are added
		List<String> lParamNames = new List<String>();
		List<String> lParamValues = new List<String>();
		
		// Generate the test integration record
		generateTestIntegrationRecord();
		
		// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
		
		// The response should contain the generic error
		System.assertEquals(TRUE, (strResponse.indexOf(EP_Common_Constant.GENERIC_FILTER_RESPONSE_FALSE) > -1));
		
    }
    
    /*
    This method will test that the monitor lib class will not return a limit that exceeds the max value
    */
    private static testMethod void testMaxQueryLimitMethod() {
    	
    	List<String> lParamNames = new List<String>();
		lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
		lParamNames.add(EP_Common_Constant.limitException);
		
		Date dToday = System.Today();
		Date dYesterday = System.Today().addDays(-1);
		
		List<String> lParamValues = new List<String>();
		// Param 1 = Yesterday
		lParamValues.add(String.valueOf(dYesterday));
		// Param 2 = Today
		lParamValues.add(String.valueOf(dToday));
		// Param 3 = 100
		lParamValues.add('200000');
    	
    	// Callout the REST API
		String strResponse = simulateRestCallout(lParamNames, lParamValues);
    	
    	// Set a very high limit value
    	Integer intLimit = EP_MonitorExportLibClassV3.returnQueryLimitFromRequest(EP_Common_Constant.limitException);
    	
    	// The method should return the max limit
    	System.assertEquals(5000, intLimit);
    
    }
    
    /*
    This method will test that the monitor lib class will handle a JSON generation unexpected error
    */
    private static testMethod void testResponseGeneratorMethodUnexpectedError() {
    	
		List<EP_IntegrationRecord__c> lIntegrationRecords = NULL;
		
		// Attempt to deserialise a NULL array
		String strResponse = EP_MonitorExportLibClassV3.returnPayloadBody(lIntegrationRecords, 'SUCCESS', 'FAILURE');
		
		// The method should return the pre-defined blank response
		System.assertEquals(EP_Common_Constant.BLANK, strResponse);
    }
}