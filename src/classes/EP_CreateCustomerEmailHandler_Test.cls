/* 
  @Author <Brajesh Tiwary>
  @name <EP_CreateCustomerEmailHandler_Test>
  @CreateDate <05/10/2015>
  @Description <This is the test class for Customer Email Handling>
  @Version <1.0>
 
*/
@isTest
private class EP_CreateCustomerEmailHandler_Test {
    /*
        This method will test for successful insertion of data
        User Story- US_001
        Test Script- 1,3
    */    
    static testMethod void testIncomingEmailSucess(){
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Messaging.InboundEmail email = EP_TestDataUtility.createInboundEmail(); 
        Messaging.InboundEnvelope env = EP_TestDataUtility.createInboundEnvelope();
        EP_CreateCustomerEmailHandler handler = new EP_CreateCustomerEmailHandler(); 
        Messaging.InboundEmailResult result; 
        System.runAs(adminUser)
        {
             result = handler.handleInboundEmail(email,env);  
        }
        try{
            result = handler.handleInboundEmail(email,env);
            
        }catch(Exception e){
            //system.debug('eeeee'+e);
            EP_LoggingService.logHandledException (e, 'ePuma', 'testIncomingEmailSucess', 'EP_CreateCustomerEmailHandler_Test', ApexPages.Severity.ERROR);
            System.assert( e.getMessage().contains('FIELD_CUSTOM_Field_EXCEPTION'),e.getMessage());
        }    
        
        
        System.assertEquals(true, result.success); // Checking the success of email recived 
         
        
        RecordType recType = EP_Common_Util.getRecordTypeForGivenSObjectAndName('Case'
                            ,EP_Common_Constant.NEW_PROSPECTIVE_CUSTOMER_REQUEST_REC_TYPE);
        List<Case> createdCases = [SELECT Id
                                        ,Origin
                                        ,Status
                                        ,Subject
                                        ,SuppliedEmail
                                        ,SuppliedName
                                        ,Priority
                                        ,Description
                                        ,RecordTypeId FROM Case limit :EP_Common_Constant.ONE];
        System.assertEquals(true, createdCases != null); // To check case is getting created with the email recieved
        System.assertEquals(true, createdCases.size() == 1);    
        System.assertEquals(EP_Common_Constant.CASE_ORIGIN_EMAIL, createdCases[0].Origin);
        System.assertEquals(EP_Common_Constant.CASE_STATUS_NEW_APPLICATION, createdCases[0].Status);
        System.assertEquals(EP_TestDataUtility.SUBJECT, createdCases[0].Subject);
        System.assertEquals(EP_TestDataUtility.FROMADDRESS, createdCases[0].SuppliedEmail);
        System.assertEquals(EP_TestDataUtility.FROMNAME, createdCases[0].SuppliedName);
        System.assertEquals(EP_Common_Constant.CASE_PRIORITY_MEDIUM, createdCases[0].Priority);
        System.assertEquals(EP_TestDataUtility.PLAINTEXTBODY, createdCases[0].Description);
        System.assertEquals(recType.Id, createdCases[0].RecordTypeId);        
        
        List<Lead> createdLeads = [SELECT Id,LastName,Email,Company,Case__c,LeadSource FROM Lead limit :EP_Common_Constant.ONE];
        System.assertEquals(true, createdLeads != null);
        System.assertEquals(true, createdLeads.size() == 1);
        System.assertEquals(EP_TestDataUtility.FROMNAME, createdLeads[0].LastName);
        System.assertEquals(EP_TestDataUtility.FROMADDRESS, createdLeads[0].Email);
        System.assertEquals(EP_TestDataUtility.FROMNAME, createdLeads[0].Company);       
        System.assertEquals(EP_Common_Constant.LEAD_ORIGIN_EMAIL, createdLeads[0].LeadSource);
        System.assertEquals(createdCases[0].Id, createdLeads[0].Case__c);
    }
    
    
    /*
        This method will test for incoming email failure
    */
    static testMethod void testIncomingEmailFailure(){
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Messaging.InboundEmail email = EP_TestDataUtility.createInboundEmail();
        Messaging.InboundEnvelope env = EP_TestDataUtility.createInboundEnvelope();
        
        email.Subject = '';
        email.fromAddress = '';
        email.fromName = '';
        email.plainTextBody = '';
        EP_CreateCustomerEmailHandler handler = new EP_CreateCustomerEmailHandler();
        
        Messaging.InboundEmailResult result; 
        Test.startTest();
        System.runAs(adminUser)
        {
             result = handler.handleInboundEmail(email,env);  
        }
        Test.stopTest();
        
        System.assertEquals(false, result.success);
        
        List<Case> createdCases = [SELECT Id FROM Case limit :EP_Common_Constant.ONE];
        System.assertEquals(true, createdCases != null);
        System.assertEquals(true, createdCases.size() == 0);
        
        List<Lead> createdLeads = [SELECT Id FROM Lead limit :EP_Common_Constant.ONE];
        System.assertEquals(true, createdLeads != null);
        System.assertEquals(true, createdLeads.size() == 0);
    }
    
    /*
        This method will count no. of emails recieved and no of cases created
        User Story- US_001
        Test Script- 2
    */
    static testMethod void testCountCasesCreated()
    {
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Messaging.InboundEmail email = EP_TestDataUtility.createInboundEmail();
        Messaging.InboundEnvelope env = EP_TestDataUtility.createInboundEnvelope();
        EP_CreateCustomerEmailHandler handler = new EP_CreateCustomerEmailHandler();
        
        Test.startTest();
        System.runAs(adminUser)
            {
            Messaging.InboundEmailResult result = handler.handleInboundEmail(email,env);
            Messaging.InboundEmailResult result1 = handler.handleInboundEmail(email,env);
            }
        Test.stopTest();        
        List<Case> listCases=[Select id from case limit :EP_Common_Constant.TWO];           
        System.assertEquals(2, listCases.size()); 
    }   
    
    /*
        This method will check exception scenarios on lead like required fields missing
        User Story- US_001
        Test Script- 
    */
    static testMethod void testExceptionScenariosOnLead()
    {
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Messaging.InboundEmail email = EP_TestDataUtility.createInboundEmail();
        Messaging.InboundEnvelope env = EP_TestDataUtility.createInboundEnvelope();
        EP_CreateCustomerEmailHandler handler = new EP_CreateCustomerEmailHandler();
        
        Test.startTest();
        System.runAs(adminUser)
        {
            Messaging.InboundEmailResult result = handler.handleInboundEmail(email,env);
        }
        Test.stopTest();    
        List<Case> newCases=[Select id from case limit :EP_Common_Constant.ONE];
        newCases[0].OwnerId = UserInfo.getUserId();
        update newCases;
        EP_LeadTriggerHandler.isExecuteBeforeUpdate = false;
        EP_LeadTriggerHandler.isExecuteAfterUpdate = false;
        Lead createdLeads = [Select Case__c from Lead limit :EP_Common_Constant.ONE];
        createdLeads.Status = EP_Common_Constant.NEW_PROSPECTIVE_CUSTOMER_REQUEST_REC_TYPE;
        createdLeads.Status = EP_Common_Constant.CASE_STATUS_RDY_KYC_REVIEW;
        createdLeads.EP_Company_Tax_Number__c = '';
        createdLeads.Fax = '';
        createdLeads.FirstName = 'Test';
        createdLeads.EP_Products_Interested_In__c = '';
        createdLeads.EP_Requested_Payment_Method__c = '';
        createdLeads.EP_Requested_Payment_Terms__c = '';
        createdLeads.Street = 'Street';
        createdLeads.City = 'city';
        createdLeads.state = 'state';
        createdLeads.postalcode = 'test code';           
        try{
            database.update(createdLeads);
        }
        catch(DMLException e) 
                {
        System.assert( e.getMessage().contains('FIELD_CUSTOM_TRIGGER_EXCEPTION'),e.getMessage());
                }
        //ApexPages.CurrentPage().getParameters().put('hasError','true');  
        //System.assertEquals();
    }
}