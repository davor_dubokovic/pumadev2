/**
 * @author <Sandeep Kumar>
 * @name <EP_SupplyLocTransprtrTriggerHelperTest >
 * @createDate <08/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_SupplyLocTransprtrTriggerHelperTest {
    
    private static EP_Supply_Location_Transporter__c testSupplyLocationTransporter;
    private static Account testTransporterAccount;
    private static Company__c testCompany;
    private static Account testShipToAccount;      
    private static final String TRANSPORTER_TYPE = 'Transporter';
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    
    /*********************************************************************************************
    *@Description : This method valids handleDefaultTransporterChange method of EP_SupplyLocTransprtrTriggerHelperTest class
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    private static testMethod void testHandleDefaultTransporterChange(){ 
          testCompany = EP_TestDataUtility.createCompany('AUN');
          testCompany.EP_Disallow_DD_for_PP_customers__c = TRUE;
          Database.insert(testCompany);
          testTransporterAccount = 
          EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE, 
                                      '123456NAV', 
                                        testCompany.Id);
          Database.insert(testTransporterAccount);
          EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy = 
                                      EP_TestDataUtility.createCustomerHierarchyForNAV(1);
          testShipToAccount = testAccountHierarchy.lShipToAccounts[0];
          EP_Country__c testCountry = 
                EP_TestDataUtility.createCountryRecord(
                                COUNTRY_NAME, 
                                  COUNTRY_CODE, 
                                    COUNTRY_REGION);
          Database.insert(testCountry);
          Id exRackshlRecordTypeId = 
                   EP_Common_Util.fetchRecordTypeId(
                          EP_Common_Constant.SUPPLY_LOCATION_OBJ,
                            EP_Common_Constant.EX_RACK_REC_TYPE_NAME);
          BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours 
                              WHERE IsActive = TRUE 
                                AND IsDefault = TRUE 
                                  LIMIT :EP_Common_Constant.ONE];
          Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);          
          Database.insert(testStorageLocation);
          
          EP_Stock_holding_location__c sellToSHL = EP_TestDataUtility.createSellToStockLocation(testShipToAccount.ParentId,true,testStorageLocation.id,exRackshlRecordTypeId); //Add one SHL
          Database.insert(sellToSHL);
          Id strSupplyLocationDeliveryRecordTypeId = 
                   EP_Common_Util.fetchRecordTypeId(
                          'EP_Stock_Holding_Location__c',
                            EP_Common_Constant.DLVRY_REC_TYPE_NAME);
                            
                        
          EP_Stock_Holding_Location__c testSupplyOption2 = 
                        EP_TestDataUtility.createShipToStockLocation(
                          testShipToAccount.Id,
                            TRUE,
                              testStorageLocation.Id,
                                strSupplyLocationDeliveryRecordTypeId);
          /*testSupplyOption2.EP_Trip_Duration__c = 1;
          testSupplyOption2.EP_Ship_To__c = testShipToAccount.Id;
          testSupplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
         
          testSupplyOption2.EP_Is_Pickup_Enabled__c = TRUE;
          testSupplyOption2.EP_Duty__c = 'Excise Free';
          testSupplyOption2.EP_Transporter__c = testTransporterAccount.Id;*/
          Database.insert(testSupplyOption2);
      
          testSupplyLocationTransporter = EP_TestDataUtility.createSupplyLocationTransporter(
                                        testTransporterAccount.Id,
                                          testSupplyOption2.Id,
                                            TRUE);
    }
        
}