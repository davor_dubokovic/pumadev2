/*Temporary class for downloading lines of code for Apex Class*/
public class LineOfCodeController {

    public List<ClassDataWrapper> classData{get;set;}
    public LineOfCodeController(){
        getLinesofCode();        
    }
    
    public void getLinesofCode(){
        classData = new List<ClassDataWrapper>();
        for(ApexClass a : [Select id, Name,Body,LengthWithoutComments from ApexClass where Name IN('EP_OrderSyncPayloadCtrlExtn','EP_CreditExceptionPayloadCtrlExtn','EP_OrderCreditCheckXMLCtrlExtn','EP_PricingRequestXMLCtrlExtn','EP_OrderConfirmationXMLCtrlExtn','EP_CustomerCreationPayloadCtrlExtn','EP_AccountSyncCtrlExtn','EP_CustomerPricingEnginePayloadCtrlExtn','EP_ShipToPricingEnginePayloadCtrlExtn','EP_ShipToPayloadCtrlExtn','EP_CustomerEditPayloadCtrlExtn','EP_TankCreationPayloadCtrlExtn','EP_TankSyncCtrlExtn','EP_OutboundMessageHeader','EP_XMLMessageGenerator','EP_OutboundMessageService','EP_OutboundMessageUtil','EP_OutboundIntegrationService','EP_IntegrationException')]){
            List<String> lines = a.Body.split('\n');
            Integer classLines = 0;
            classLines += lines.size();
            integer mcount = getMethodCount(a.Id);                                                                                                                    
            ClassDataWrapper cwrap = new ClassDataWrapper();
            cwrap.name = a.Name;
            cwrap.linesofcode = classLines; 
            cwrap.numOfmethods = mcount;
            cwrap.LengthWithoutComments = a.LengthWithoutComments;
            classData.add(cwrap);
        } 
    }


    
    public integer getMethodCount(Id classId){
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm()+ '/services/data/v37.0/tooling/sobjects/ApexClass/';
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint(baseUrl + classId);
        req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setHeader('Content-Type', 'application/json');
        Http h = new Http();
        HttpResponse res = h.send(req);
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Map<String, Object> SymbolTable = (Map<String, Object>)m.get('SymbolTable');
        List<Object> methods = (List<Object>)SymbolTable.get('methods');
        system.debug('**Number of methods**'+methods.size());
        return methods.size();
    }
    public class ClassDataWrapper{
        public  string name{ get; set; }
        public  integer linesofcode { get; set; }
        public integer numOfmethods {get;set;}
         public integer LengthWithoutComments {get;set;}
    } 
}