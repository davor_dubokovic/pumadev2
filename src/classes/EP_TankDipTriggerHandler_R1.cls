/*
 * Class Name: EP_TankDipTriggerHandler_R1
 * Requirement # This class handles request from TankDip
*/
public with sharing class EP_TankDipTriggerHandler_R1 {
    
    /*
        This method handles before insert requests from Tank Dip trigger
    */
    public static void doBeforeInsert(List<EP_Tank_Dip__c> lNewTankDips) {
        // Add the tank status on dips
        EP_TankDipTriggerHelper_R1.addTankStatus(lNewTankDips);
        // 1. Validate new tank dips against existing tank dips for the same tank. Users are not allowed to enter tank dips
        // when there is already a tank dip for the same day for a later time
        EP_TankDipTriggerHelper_R1.validateTankDipsAgainstsExistingDailyDips(lNewTankDips);
    }
    
    /*
        This method handles before update requests from Tank Dip trigger
    */
    public static void doBeforeUpdate(Map<Id, EP_Tank_Dip__c> mapOldTankDips, List<EP_Tank_Dip__c> lOldTankDips) {
        // 1. Validate new tank dips against existing tank dips for the same tank. Users are not allowed to enter tank dips
        // when there is already a tank dip for the same day for a later time
        EP_TankDipTriggerHelper_R1.validateTankDipsAgainstsExistingDailyDips(lOldTankDips);
        
        // Fix for Defect 38939 - Start
        // 2. Update the user offset field when reading time is changed
        EP_TankDipTriggerHelper_R1.updateUserUTCOffsetOnReadingTimeChange(lOldTankDips, mapOldTankDips);
    	// Fix for Defect 38939 - End
    }
   
    /*
        This method handles after insert requests from Tank Dip trigger
    */
    public static void doAfterInsert(List<EP_Tank_Dip__c> lNewTankDips) {
        // 1. Remove any placeholder tank dips from the system when an actual dip is entered
        EP_TankDipTriggerHelper_R1.removePlaceholderTankDipsFromSystem(lNewTankDips);
    }
    
    /*
        This method handles after update requests from Tank Dip trigger
    */
    public static void doAfterUpdate(Map<Id, EP_Tank_Dip__c> mapOldTankDips, List<EP_Tank_Dip__c> lOldTankDips) {
        // 1. Remove any placeholder tank dips from the system when an actual dip is update
        EP_TankDipTriggerHelper_R1.removePlaceholderTankDipsFromSystem(lOldTankDips);
        EP_TankDipTriggerHelper_R1.updateIsExported();
    }

}