public class EP_ASTSellToRejectedToRejected extends EP_AccountStateTransition{
    
    public EP_ASTSellToRejectedToRejected() {
        finalstate = EP_AccountConstant.REJECTED;
    }
    
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToRejectedToRejected','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToRejectedToRejected','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToRejectedToRejected','isGuardCondition');
        //No guard conditions for Rejected to Rejected
        return true;
    }

}