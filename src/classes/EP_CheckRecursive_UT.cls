@isTest
public class EP_CheckRecursive_UT
{   
    static testMethod void runOnce_positive_test() {
        Test.startTest();
        Boolean result = EP_CheckRecursive.runOnce();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void runOnce_negative_test() {
        Test.startTest();
        EP_CheckRecursive.runOnce();
        Boolean result = EP_CheckRecursive.runOnce();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void runBeforeOnce_positive_test() {
        Test.startTest();
        EP_CheckRecursive.runBeforeOnce();
        Boolean result = EP_CheckRecursive.runOnce();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void runBeforeOnce_negative_test() {
        EP_CheckRecursive.isBeforeRunning = false;
        Test.startTest();
        Boolean result = EP_CheckRecursive.runBeforeOnce();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
     static testMethod void runAfterOnce_positive_test() {
        Test.startTest();
        EP_CheckRecursive.runAfterOnce();
        Boolean result = EP_CheckRecursive.runOnce();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void runAfterOnce_negative_test() {
        EP_CheckRecursive.isAfterRunning = false;
        Test.startTest();
        Boolean result = EP_CheckRecursive.runAfterOnce();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
}