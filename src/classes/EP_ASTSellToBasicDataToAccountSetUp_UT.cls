@isTest
public class EP_ASTSellToBasicDataToAccountSetUp_UT
{
    static final string EVENT_NAME = '02-BasicDataSetupTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        delete [Select Id From EP_Action__c];
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        delete [Select Id From EP_Action__c];
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    //No longer required as guardian is always returning true
    /*static testMethod void isGuardCondition_Step2_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        delete [Select Id From EP_Action__c];
        obj.getAccount().EP_Delivery_Type__c = EP_AccountConstant.DELIVERY;
        update obj.getAccount();
        ast.account =  new EP_AccountDomainObject(obj.getAccount().id);
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isGuardCondition_Step3_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        Account accObj = obj.getAccount();
        accObj.EP_Requested_Payment_Method__c = EP_Common_Constant.DIRECT_DEBIT;
        update accObj;
        delete [SELECT id FROM EP_Bank_Account__c ];
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        delete [Select Id From EP_Action__c];
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }*/
    
	static testMethod void doOnExit_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASBasicDataSetupDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME );
        EP_ASTSellToBasicDataToAccountSetUp ast = new EP_ASTSellToBasicDataToAccountSetUp();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        	 ast.doOnExit();
        Test.stopTest();
        //Method delegates to other method hence adding a dummy assert
        System.Assert(true);
    }
}