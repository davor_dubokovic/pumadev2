/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTVMINonConsignPlannedToCancelled >
*  @CreateDate <03/02/2017>
*  @Description <Handles Vmi consignment order status change from Planned to cancelled status>
*  @Version <1.0>
*/

public class EP_OSTVMINonConsignPlannedToCancelled extends EP_OrderStateTransition{
  
    public EP_OSTVMINonConsignPlannedToCancelled(){
      finalState = EP_OrderConstant.OrderState_Cancelled;
    }
    
    public override boolean isTransitionPossible(){
      EP_GeneralUtility.Log('Public','EP_OSTVMINonConsignPlannedToCancelled','isTransitionPossible');
      return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
      EP_GeneralUtility.Log('Public','EP_OSTVMINonConsignPlannedToCancelled','isRegisteredForEvent');
      return super.isRegisteredForEvent(); 
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTVMINonConsignPlannedToCancelled','isGuardCondition');
        // Check if either its user cancel or exception rejected or cancel update from WINDMS
       EP_OrderService orderService = new EP_OrderService(order);

      if(this.order.isInventoryLow()){
        system.debug('Invetorry is low ***---' + this.order.isInventoryLow());
        this.order.setOrderItemWithInventoryAvailability();
        this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.CREDIT_BLOCK;
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = Label.EP_Low_Inventory_Error;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      }

      if(this.order.isOrderSellToInvoiceOverdue()){
        system.debug('Invoice overdue');
        this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.CREDIT_BLOCK;
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = EP_OrderConstant.FUNDS_NOT_AVAILABLE;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      }

      if(this.order.getSellToAccountDomain() != null && !this.order.getSellToAccountDomain().localAccount.EP_IsActive__c){
        system.debug('Sell to is not active ***---');
        this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.CREDIT_BLOCK;
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = EP_OrderConstant.SELL_TO_BLOCKED;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      } 

       if(this.order.getShipToAccountDomain() != null && !this.order.getShipToAccountDomain().localAccount.EP_IsActive__c){
        system.debug('Bill to is not active ***---');
        this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.CREDIT_BLOCK;
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = EP_OrderConstant.SHIP_TO_BLOCKED;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      }

      if(orderService.getOperationalTanks(this.order.getOrder().EP_ShipTo__c) == null){
        system.debug('is tank operational ***---');
        this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.CREDIT_BLOCK;
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = EP_OrderConstant.TANK_BLOCKED;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      }

      if(EP_Common_constant.PRICING_STATUS_FAILURE.equalsignorecase(order.getOrder().EP_Pricing_Status__c)){
        system.debug('Pricing failed ***---');
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = EP_OrderConstant.PRICING_REQUEST_FAILED;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      }

      if(EP_Common_constant.PRICING_STATUS_PRICED.equalsIgnoreCase(order.getOrder().EP_Pricing_Status__c) && orderService.hasCreditIssue()){
        system.debug('Order - account has credit issue');
        this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.CREDIT_BLOCK;
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = EP_OrderConstant.FUNDS_NOT_AVAILABLE;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      }

      if(EP_Common_constant.PRICING_STATUS_PRICED.equalsIgnoreCase(order.getOrder().EP_Pricing_Status__c) && !EP_Common_constant.OK.equalsignorecase(this.order.getOrder().EP_Credit_Status__c)){
        system.debug('Order - Nav says it has credit issue');
        this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.CREDIT_BLOCK;
        this.order.getOrder().Credit_Check_Reason_Code_XML__c = EP_OrderConstant.FUNDS_NOT_AVAILABLE;
        this.order.getOrder().EP_Sync_With_LS__c =false;
        return true;
      }

      return false;
    }
}