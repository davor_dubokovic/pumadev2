/* 
   @Author 			Accenture
   @name 			EP_AccountSyncCtrlExtn
   @CreateDate 		02/08/2017
   @Description		Controller extension used to generate different outbound XML's of Account
   @Version 		1.0
*/
public with sharing class EP_AccountSyncCtrlExtn{ 
    
    private string messageId;
    private string messageType;
    private string companyCode;
    public String encodedPayload {get;set;}
    public Account AccountObject {get;set;}
    public EP_OutboundMessageHeader headerObj {get;set;}
    private EP_CS_OutboundMessageSetting__c msgSetting;
    private string secretCode;
    
    /**
	* @author 			Accenture
	* @name				EP_AccountSyncCtrlExtn
	* @date 			02/08/2017
	* @description 		Constructor of EP_AccountSyncCtrlExtn. It will get the AccountObject by using the getRecord method of standard controller.This will setup the data in the header Object and will setup the data in payload.
	* @param 			ApexPages.StandardController
	* @return 			NA
	*/          
    public EP_AccountSyncCtrlExtn(ApexPages.StandardController stdController) {
        EP_GeneralUtility.Log('Public','EP_AccountSyncCtrlExtn','EP_AccountSyncCtrlExtn');
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        if (!Test.isRunningTest()) stdController.addFields(new List<String>{ EP_Common_Constant.COMPANY_CODE_FIELD}); 
        this.AccountObject = (Account) stdController.getRecord();
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        this.companyCode =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.SOURCE_COMPANY);
        this.msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(this.messageType);
        this.headerObj = new EP_OutboundMessageHeader();
        this.setHeader();
        this.setPayload();
    }
    
    /**
	* @author 			Accenture
	* @name				setHeader
	* @date 			02/08/2017
	* @description 		Method used to setup the values in Header in XML message
	* @param 			NA
	* @return 			NA
	*/        
    public void setHeader() {
        EP_GeneralUtility.Log('Public','EP_AccountSyncCtrlExtn','setHeader');
        this.headerObj = EP_OutboundMessageUtil.setMessageHeader(msgSetting, this.messageId, this.companyCode);
    }   
    
    /**
	* @author 			Accenture
	* @name				setPayload
	* @date 			02/08/2017
	* @description 		Method used to setup the values in payload in XML message
	* @param 			NA
	* @return 			NA
	*/         
    @TestVisible
    private void setPayload() {
        EP_GeneralUtility.Log('Private','EP_AccountSyncCtrlExtn','setPayload');
        this.encodedPayload = EP_OutboundMessageUtil.getPayloadXML(msgSetting,this.AccountObject.id, this.messageId,this.companyCode);
    }
    
    /**
	* @author 			Accenture
	* @name				checkPageAccess
	* @date 			02/08/2017
	* @description 		Method used to redirect user at Error Page if they are trying to access this page without passing secret Code.
	* @param 			NA
	* @return 			PageReference
	*/
    public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_AccountSyncCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}