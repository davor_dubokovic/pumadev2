/* 
  	@Author <Accenture>
	@name <EP_CreditExcpTriggerHelper_UT>
	@CreateDate <02-01-2017>
	@Description <This class handles request from EP_Credit_Exception_Request__c trigger> 
	@Version <1.0>
  */
@isTest
private class EP_CreditExcpTriggerHelper_UT {
	
	@testSetup static void init() {
        List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }

	@isTest
	static void sendSyncRequestToNAV_Postive() {
		
		EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
		EP_CreditExcpTriggerHelper.sendSyncRequestToNAV(new Map<Id,EP_Credit_Exception_Request__c>{creditExp.Id=>creditExp},null,'AAF');
		List<EP_IntegrationRecord__c> lstIntegrationRecs = [SELECT Id FROM EP_IntegrationRecord__c];
		System.assert(lstIntegrationRecs.size() > 0);
	}

	@isTest
	static void sendSyncRequestToNAV_Negative() {

		EP_CreditExcpTriggerHelper.sendSyncRequestToNAV(null,null,'AAF');
		List<EP_IntegrationRecord__c> lstIntegrationRecs = [SELECT Id FROM EP_IntegrationRecord__c];
		System.assert(lstIntegrationRecs.size() == 0);
	}
	
	@isTest
	static void isSyncRequired_Positive() {
		EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
		creditExp.EP_Integration_Status__c = null;
		Boolean conditionResult = EP_CreditExcpTriggerHelper.isSyncRequired(creditExp,null);
		System.assertEquals(true,Conditionresult);
	}

	@isTest
	static void isSyncRequired_Negative() {
		EP_Credit_Exception_Request__c creditExp = EP_TestDataUtility.createCreditExceptionRequest();
		creditExp.EP_Status__c = 'Approved';
		Boolean conditionResult = EP_CreditExcpTriggerHelper.isSyncRequired(creditExp,null);
		System.assertNotEquals(true,Conditionresult);
	}
	
}