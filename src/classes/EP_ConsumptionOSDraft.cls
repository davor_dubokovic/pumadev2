/*   
     @Author Aravindhan Ramalingam
     @name <RetroNonConsignmentOSDraft.cls>     
     @Description <Retro Non Consignment Order State for draft status>   
     @Version <1.1> 
     */

     public class EP_ConsumptionOSDraft extends EP_OrderState {

        public EP_ConsumptionOSDraft(){

        }
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            EP_GeneralUtility.Log('Public','EP_ConsumptionOSDraft','setOrderDomainObject');
            super.setOrderDomainObject(currentOrder);
        }
        public override boolean doTransition(){
            EP_GeneralUtility.Log('Public','EP_ConsumptionOSDraft','doTransition');
            return super.doTransition();
        }

        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_ConsumptionOSDraft','doOnEntry');
            csord__Order__c ord = order.getOrder();

        ord.EP_Actual_Delivery_Date_Time__c = EP_GeneralUtility.convertDateTimeToDecimal(String.valueOf(ord.EP_Order_Date__c));  // value is mapped to delivery date since delivery date takes the value from actual delivery date
        ord.EP_Requested_Delivery_Date__c = Date.newInstance(ord.EP_Order_Date__c.Year(),ord.EP_Order_Date__c.Month(),ord.EP_Order_Date__c.Day()); // defect fix 44398, #anubhutiWork update Request Delivery Date with actual delivery date in case of Order Epoch = “Retrospective”. This would enable ROs also to appear in On-run report if they are getting assigned to a run at the time of order creation.
        ord.EP_Requested_Pickup_Time__c = String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(order.availableSlots)).subString(0,8); //Defect fix 44398
        ord.EP_Loading_Date__c = ord.EP_Order_Date__c;
        ord.EP_Expected_Loading_Date__c = EP_PortalLibClass_R1.returnLocalDate(Date.valueOf(ord.EP_Order_Date__c));

        //set thr order back in the order domainboject
        order.setOrder(ord);
    }
    
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ConsumptionOSDraft','doOnExit');

    }
    
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ConsumptionOSDraft','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();
    }
    public static String getTextValue()
    {
        EP_GeneralUtility.Log('Public','EP_ConsumptionOSDraft','getTextValue');
        return EP_OrderConstant.OrderState_Draft;
    }
    
}