/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EP_ApprovalWorkflow_Test {
    
    private static Contact contactRec;
    private static Account sellToAccount;
    private static EP_Country__c country;
    private static EP_Region__c region;
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia'; 
    private static Id shpToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
    
    /* Method is creating test data 
     **/
    private static void loadData(){
        country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country,false);
        
        region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        Database.insert(region,false);
        
        sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        Database.insert(sellToAccount, false);
        contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
    }
    /*
     * Unit Test for ACtion Creation on Basic Data setup and No KYC Required scenario  
     **/
    static testMethod void unitTestFotNoKYCRequired() {
        Profile SMAgent = [Select id from Profile
                            Where Name = 'Puma SM Agent_R1' 
                            limit :EP_Common_Constant.ONE];
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
                  
        system.runAs(SMTMuser){
            loadData();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(sellToAccount);
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = sellToAccount.Id
            );
            Insert att;
        }
        
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        database.insert(cscUser);
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName = 'EP_CSC_Review' limit 1];
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = cscUser.Id
        );
        insert gm;
        
        system.runAs(cscUser)
        {           
            action.OwnerId = userinfo.getuserId();
            action.EP_Status__c = '03-Completed';
            database.update(action);
            
            
        } 
         system.runAs(SMTMuser){
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_KYC_REVIEW;
            Database.SaveResult svResult = Database.update(sellToAccount,false);
            /*Test case 17621: Customer_HLBR-E2E 002.13_To check that System should not allow user to update the status as “04-Account Setup”, 
             directly from “02-Basic Data Setup”, if “Indicative Total Purchase Value/Yr (USD)” > 250K USD
             */
            if( !svResult.isSuccess() )
            {
                for( Database.Error err : svResult.getErrors() )
                {
                    system.assertEquals(err.getMessage().containsignorecase('Indicative Total'),True );
                }
            }
            
          //  Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
           // Account Acc = [Select id, EP_Status__c from Account where Id =: sellToAccount.id limit: nRows];
            //system.assert(Acc.size() == 1); 
           // system.assertNotEquals(EP_Common_Constant.STATUS_KYC_REVIEW, Acc.EP_Status__c);
         }     
        
        
    }
    /*
     *Unit Test FOr checking KYC Review APPROVAL/REJECTION
     **/
    static testMethod void myUnitForKYCApproveANDReject() {
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);  
        Profile SMAgent = [Select id from Profile
                            Where Name = 'Puma SM Agent_R1' 
                            limit :EP_Common_Constant.ONE];
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
                  
        system.runAs(SMTMuser){
            loadData();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '> 1m USD';
            Database.update(sellToAccount);
            
            EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName = 'EP_CSC_Review' limit 1];
            group g = [Select Id from Group where Id =: action.OwnerId ];
            GroupMember gm = new GroupMember(
            GroupId = g.Id
            );
           System.runAs(adminUser){
                database.insert(cscUser);
                gm.UserOrGroupId = cscUser.Id;
                insert gm;
            }
            system.runAs(cscUser)
            {           
                action.OwnerId = userinfo.getuserId();
                database.update(action);
                action.EP_Status__c = '03-Completed';
                database.update(action);
            }
            
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_KYC_REVIEW;
            Database.update(sellToAccount);
            
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            List<EP_Action__c> actions = [Select id, EP_Record_Type_Name__c,RecordTypeId, EP_Status__c from EP_Action__c where EP_Account__c =: sellToAccount.id AND RecordType.DeveloperName = 'EP_KYC_Review' order by Name Asc limit: nRows];
            system.assert(actions.size() == 1);
            system.assertEquals('KYC Review', actions[0].EP_Record_Type_Name__c);
            
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = sellToAccount.Id
            );
            Insert att;
        }
        
        Profile kycReviewer = [Select id from Profile
                            Where Name =: EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User kycUser = EP_TestDataUtility.createUser(kycReviewer.id);
        database.insert(kycUser);
        
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName = 'EP_KYC_Review' limit 1];
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = kycUser.id
        );
        insert gm;
        
        system.runAs(kycUser)
        {   //Test case 17615: Customer_HLBR-E2E 002.17_To validate that if the task status is rejected reason for rejection is mandatory       
            action.OwnerId = kycUser.Id;
            Database.SaveResult sv = database.update(action, false);
            system.assert(sv.isSuccess());
            action.EP_Status__c = '03-Rejected'; 
            sv = database.update(action, false);
            system.assert(!sv.isSuccess());
            action.EP_Status__c = '03-Rejected';
            action.EP_Reason__c = 'TBD';
            sv = database.update(action, false);
            system.assert(sv.isSuccess());
            //Test case 17620: Customer_HLBR-E2E 002.17_To validate that user is not allowed to change the status of task, once it is approved or rejected
            action = [select id, EP_Status__c from EP_Action__c where id =: action.id limit 1];
            system.assertEquals('03-Rejected', action.EP_Status__c);
            action.EP_Status__c = '03-Approved'; 
            sv = database.update(action, false);
            system.assert(!sv.isSuccess());
        } 
        
        Account Acc = [Select id, EP_Status__c from Account where Id =: sellToAccount.id limit 1];
        system.assertEquals(EP_Common_Constant.STATUS_REJECTED, Acc.EP_Status__c);
    }
    /*
     *Unit Test FOr checking - KYC Review OwnerChange
     **/
    static testMethod void unitTestForKYCReiewOwnerChange() {
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Profile sysAdmin = [Select id from Profile
                                Where Name = 'System Administrator' 
                                limit :EP_Common_Constant.ONE];
        User adminUser = EP_TestDataUtility.createUser(sysAdmin.id);
        Profile SMAgent = [Select id from Profile
                            Where Name = 'Puma SM Agent_R1' 
                            limit :EP_Common_Constant.ONE];
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
                  
        system.runAs(SMTMuser){
            loadData();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '> 1m USD';
            Database.update(sellToAccount);
            EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName = 'EP_CSC_Review' limit 1];
            group g = [Select Id from Group where Id =: action.OwnerId ];
            GroupMember gm = new GroupMember(
            GroupId = g.Id
            );
           System.runAs(adminUser){
                database.insert(cscUser);
                gm.UserOrGroupId = cscUser.Id;
                insert gm;
            }
            system.runAs(cscUser)
            {           
                action.OwnerId = userinfo.getuserId();
                database.update(action);
                action.EP_Status__c = '03-Completed';
                database.update(action);
            }
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_KYC_REVIEW;
            Database.update(sellToAccount);
            
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            List<EP_Action__c> actions = [Select id, EP_Record_Type_Name__c,RecordTypeId, EP_Status__c from EP_Action__c where EP_Account__c =: sellToAccount.id AND RecordType.DeveloperName = 'EP_KYC_Review' order by Name Asc limit: nRows];
            system.assert(actions.size() == 1);
            system.assertEquals('KYC Review', actions[0].EP_Record_Type_Name__c);
            
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = sellToAccount.Id
            );
            Insert att;
        }
        
        Profile kycReviewer = [Select id from Profile
                            Where Name =: EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User kycUser = EP_TestDataUtility.createUser(kycReviewer.id);
        database.insert(kycUser);
        
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName = 'EP_KYC_Review' limit 1];
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = kycUser.id
        );
        insert gm;
        
        cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        database.insert(cscUser);
        
        system.runAs(kycUser)
        {           
            action.OwnerId = cscUser.Id;
            database.update(action);            
        } 
        list<EP_Action__Share> act = [SELECT AccessLevel,Id,ParentId,RowCause, UserOrGroupId FROM EP_Action__Share WHERE ParentId =: action.id AND UserOrGroupId =: kycUser.Id];
        system.assertEquals( act.size(),0); 
    }
    
    /*
     * Test case 17575: Customer_HLBR-E2E 002.16_To validate that action for KYC is created in the action related list of account
     * 17578: Customer_HLBR-E2E 002.38_To validate that SM\TM are able to attach the documents under "Notes and Attachment" related list
     * Test case 17579: Customer_HLBR-E2E 002.38_To validate that SM\TM should be able to change the status to Under KYC review.
     * Test case 17582: Customer_HLBR-E2E 002.38_To validate that SM\TM should not be able to change the status to KYC review if indicative purchase value is <=250K usd
     * 17608: Customer_HLBR-E2E 002.16_To validate that structure of the related list Action (Partially Done)
     * 17609: Customer_HLBR-E2E 002.16_To validate that task is created for KYC under KYC review (Partially Done)
     * 17610: Customer_HLBR-E2E 002.16_To validate that KYC is not able to work on the task until he assigns it to himself
     * Test case 17612: Customer_HLBR-E2E 002.17_To validate that KYC has only read access to account (Partially Done)
     */
    static testMethod void myUnitTest4() {
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        database.insert(contry1);
        EP_Region__c reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg); 
        EP_Freight_Matrix__c freightMatrix;
        List<EP_Stock_Holding_Location__c>lStockHlodingLocation = new list<EP_Stock_Holding_Location__c>();
        DataBase.SaveResult[] lSHLSaveResult = new list<DataBase.SaveResult>(); 
        String sellTo = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ
                                                            ,EP_Common_Constant.SELL_TO);
        Account parentAccWOKYC;
        Account parentAccWithKYC;
        Profile TM_PROFILE = [Select id from Profile
                                Where Name = :EP_Common_Constant.TM_AGENT_PROFILE
                                limit :EP_Common_Constant.ONE];
        User tmUser = EP_TestDataUtility.createUser(TM_PROFILE.id);
        database.insert(tmUser);
        Test.startTest();
        
        System.runAs(tmUser){ 
            //CREATE FRIEGHT MATRIX
            freightMatrix = EP_TestDataUtility.createFreightMatrix();
            database.insert(freightMatrix);
            //CREATE CUSTOMER
            parentAccWOKYC = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
            parentAccWOKYC.EP_Country__c = contry1.id;
            parentAccWOKYC.EP_Delivery_Pickup_Country__c = contry1.id;
            parentAccWOKYC.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            parentAccWOKYC.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;//Insert Sell To Delivery Type
            parentAccWOKYC.EP_Cntry_Region__c = reg.id;
            
            parentAccWithKYC = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
            parentAccWithKYC.EP_Country__c = contry1.id;
            parentAccWithKYC.EP_Delivery_Pickup_Country__c = contry1.id;
            // Creation of sell to with indicative purchase value for the account is > 250K USD
            //17608: Customer_HLBR-E2E 002.16_To validate that structure of the related list Action
            parentAccWithKYC.EP_Indicative_Total_Pur_Value_Per_Yr__c = '> 1m USD';
            parentAccWithKYC.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;//Insert Sell To Delivery Type
            parentAccWithKYC.EP_Cntry_Region__c = reg.id;
            database.insert(parentAccWOKYC);
            database.insert(parentAccWithKYC);
            EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(parentAccWOKYC.id,true,null,null); //Add one SHL
            EP_Stock_holding_location__c SHL2 = EP_TestDataUtility.createSellToStockLocation(parentAccWithKYC.id,true,null,null); //Add one SHL
            List<EP_Stock_holding_location__c> shlList = new List<EP_Stock_holding_location__c>{SHL1,SHL2};
            Database.insert(shlList, false);
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
            EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = true;
            parentAccWOKYC.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            Database.update(parentAccWOKYC);
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
            EP_BankAccountTriggerHandler.isExecuteBeforeUpdate = true;
            parentAccWithKYC.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            Database.update(parentAccWithKYC);
        }
        Test.stopTest();
        Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        List<EP_Action__c> actions = [Select id, OwnerID, EP_Record_Type_Name__c,RecordTypeId, EP_Status__c from EP_Action__c where 
                                        EP_Record_Type_Name__c = 'CSC Review' AND
                                        (EP_Account__c =: parentAccWOKYC.id 
                                        OR EP_Account__c =: parentAccWithKYC.id) 
                                        order by Name Asc limit: nRows];
        system.assert(actions.size() == 2);
        system.assertEquals('CSC Review', actions[0].EP_Record_Type_Name__c);
        system.assertEquals('CSC Review', actions[1].EP_Record_Type_Name__c);
        
        Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE
                            Limit :EP_Common_Constant.ONE];
        
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Database.insert(cscUser);
        System.runAs(tmUser){ 
            //Test case 17578: Customer_HLBR-E2E 002.38_To validate that SM\TM are able to attach the documents under "Notes and Attachment" related list
            Attachment attach1=new Attachment();    
            attach1.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach1.body=bodyBlob;
            attach1.parentId=parentAccWOKYC.id;
            
            Attachment attach2=new Attachment();    
            attach2.Name='Unit Test Attachment';
            attach2.body=bodyBlob;
            attach2.parentId=parentAccWithKYC.id;
            List<attachment> attachList = new List<attachment>{attach1,attach2};
            Database.insert(attachList);
            Group g = [Select Id from Group where Id =: actions[0].OwnerId ];
            GroupMember gm = new GroupMember(
                GroupId = g.Id,
                UserOrGroupId = cscuser.id
            );
            database.insert(gm);
            
        }
        
        
        system.runAs(cscUser){   
            EP_Action__c action1 = new EP_Action__c(id=actions[0].id);          
            action1.OwnerId = userinfo.getuserId();
            
            EP_Action__c action2 = new EP_Action__c(id=actions[1].id);          
            action2.OwnerId = userinfo.getuserId();
            List<EP_Action__c> actToUp = new List<EP_Action__c>{action1,action2};
            database.update(actToUp);
            List<EP_Action__c> actionStatusToChange = new List<EP_Action__c>();
            for(EP_Action__c ac : [Select id, OwnerID from EP_Action__c where EP_Record_Type_Name__c = 'CSC Review' limit 2]){
                system.assertEquals(cscUser.id, ac.OwnerID);
                ac.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
                actionStatusToChange.add(ac);
            }
            Database.update(actionStatusToChange);
        } 
       
        //Create KYC User
        Profile kycReviewer = [Select id from Profile
                            Where Name =: EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User kycUser = EP_TestDataUtility.createUser(kycReviewer.id);
        database.insert(kycUser);
        
        
        system.runAs(tmUser){
            //Test case 17579: Customer_HLBR-E2E 002.38_To validate that SM\TM should be able to change the status to Under KYC review.
            parentAccWithKYC.EP_Status__c = '03-KYC Review';
            parentAccWOKYC.EP_Status__c = '03-KYC Review';
            List<Account> accountToUpdateCheck = new List<Account>{parentAccWithKYC,parentAccWOKYC };
            List<database.saveResult> srList = Database.update(accountToUpdateCheck, false);
            // CHeck for KYC avalidation Rule
            System.assert(srList[0].isSuccess()); 
            System.assert(srList[1].isSuccess()); 
            
            // Now enable KYC condition on parentAccWOKYC
            parentAccWOKYC.EP_Indicative_Total_Pur_Value_Per_Yr__c = '> 1m USD';
            parentAccWOKYC.EP_Status__c = '03-KYC Review';
            //Test Automation-17575
            EP_Action__c kycAction = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: parentAccWithKYC.Id AND RecordType.DeveloperName = 'EP_KYC_Review' limit 1];
            
            database.update(parentAccWOKYC);
            Group kycg = [Select Id from Group where Id =: kycAction.OwnerId ];
            GroupMember kycgm = new GroupMember(
                GroupId = kycg.Id,
                UserOrGroupId = kycUser.id
            );
            database.insert(kycGM);
        }
        Id rejectedAccountID  ;
        system.runAs(kycUser){   
            List<EP_Action__c> kycActions = [Select Id, EP_Account__C, OwnerId from EP_Action__c where RecordType.DeveloperName = 'EP_KYC_Review'];
            system.assertNotEquals(kycReviewer.id, kycActions[0].OwnerId);
            List<EP_Action__c> kycActionsTOuP = NEW List<EP_Action__c>();
            
            //Trying to update ACtion without assigning it to KYC user
            for(EP_Action__c ac :kycActions ){
                ac.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
                kycActionsToUp.add(new EP_Action__c(id=ac.id));
            }
            //User should not be able to update
            List<database.saveResult> svList =  database.update(kycActions, false); 
            kycActions = new List<EP_Action__c>();
            for(database.saveResult s : svList){
                system.assert(!s.isSuccess());
            }
            
            for(EP_Action__c ac :kycActionsToUp){
                ac.OwnerID = kycUser.id;
            } 
            database.update(kycActionsToUp, false);
            
            //Test case 17611: Customer_HLBR-E2E 002.16_To validate that status of the task should be assigned as soon as he assigns it to himself
            kycActions = new List<EP_Action__c>([Select Id,EP_Account__c,EP_Status__c, OwnerId from EP_Action__c where RecordType.DeveloperName = 'EP_KYC_Review']);
            for(EP_Action__c ac :kycActions){
                system.assertEquals(kycUser.id, ac.ownerID);
                system.assertEquals('02-Assigned', ac.EP_Status__c);
            }
            //Test case 17613: Customer_HLBR-E2E 002.17_To validate that KYC can enter any additional details in comments section
            //Test case 17614: Customer_HLBR-E2E 002.17_To validate that KYC can change the status of the task to Approved or Rejected.
            
            kycActions[0].EP_Status__c = '03-Approved';
            kycActions[0].EP_Comments__c = 'test Comment';
          //  kycActions[1].EP_Status__c = '03-Rejected';
          //  kycActions[1].EP_Reason__c = 'Rejected';
          //  kycActions[1].EP_Comments__c = 'test Comment for rejected';
           // rejectedAccountID = kycActions[1].EP_Account__c;
            List<database.saveResult> svList1 =  database.update(kycActions,false);
            for(database.saveResult sv: database.update(kycActions,false)){
                system.assert(sv.isSuccess());
            }
        } 
        
        system.runAs(tmUser){
          //  account acc1 = [Select id, EP_Status__c from Account where id=: rejectedAccountID limit 1];
            //Test case 17617: Customer_HLBR-E2E 002.17_To validate that if KYC change the status to Rejected Account status should be changed to Rejected
           // system.assertEquals('08-Rejected',acc1.EP_Status__c);
        }
     } 
     
     /*
        CHeck for Initiated Pricing Review
        Update Pricebook On Sell To before review completion
     */
     static testMethod void pricingReviewInitiatedTest() {
        Id pricebookId = Test.getStandardPricebookId();
        Profile SMAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.SM_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        Profile CSCAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        
        UserRole SMTMRole = [Select id from UserRole where developerName = 'EP_SM_TM_Team_AU' limit 1];  
        UserRole internalReviewUserRole = [Select id from UserRole where developerName = 'EP_Internal_Review_Team_AU' limit 1];                 
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
        User CSCUser = EP_TestDataUtility.createUser(CSCAgent.id);
        CSCUser.UserRoleId =  internalReviewUserRole.id;
        insert CSCUser;
        smTMUser.UserRoleId =  SMTMRole.id;        
        system.runAs(SMTMuser){
            loadData();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(sellToAccount);
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = sellToAccount.Id
            );
            Insert att;
        }
        EP_Action__c action1 = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id and RecordType.developerName=:EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV limit 1];
        Group g1 = [Select Id from Group where Id =: action1.OwnerId ];
        GroupMember gm1 = new GroupMember(
            GroupId = g1.Id,
            UserOrGroupId = CSCUser.Id
        );
        insert gm1;
        System.RunAs(CSCUser){
        	action1.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(action1, false);
            EP_Action__c act1 = [Select Id, Name,EP_Status__c FROM EP_Action__c WHERE EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_CSC_REVIEW_RT_DEV limit 1]; //EP_Common_Constant.ACT_SELLTO limit 1]; removed as per test class fix
            system.assertEquals(EP_Common_Constant.ACT_ASSIGNED_STATUS,act1.EP_Status__c);
           
            action1.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS; 
            sv = database.update(action1, false);
            system.assert(sv.isSuccess());	
        }
        
        
        
        Profile logisticsAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.LOGISTICS_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User logisticsUser = EP_TestDataUtility.createUser(logisticsAgent.id);
        logisticsUser.UserRoleId =  internalReviewUserRole.id;
        database.insert(logisticsUser);
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =:EP_Common_Constant.ACT_SELLTO limit 1 ]; //EP_Common_Constant.ACT_SELLTO limit 1]; removed as per test class fix
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = logisticsUser.Id
        );
        insert gm;
        
        system.runAs(logisticsUser)
        {           
            action.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(action, false);
            EP_Action__c act1 = [Select Id, Name,EP_Status__c FROM EP_Action__c WHERE EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SELLTO limit 1]; //EP_Common_Constant.ACT_SELLTO limit 1]; removed as per test class fix
            system.assertEquals(EP_Common_Constant.ACT_ASSIGNED_STATUS,act1.EP_Status__c);
            sellToAccount.EP_PriceBook__c = pricebookId; //17750
            Database.update(sellToAccount);
            action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS; //17752
            sv = database.update(action, false);
            system.assert(sv.isSuccess());
            
        }
        
        
         
        system.runAs(SMTMuser){
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = false; 
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            Database.saveresult sv = database.update(sellToAccount,false);
            system.assert(!sv.isSuccess());
            
            List<EP_Action__c> actions = [Select Id, OwnerId from EP_Action__c  where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            system.assert(actions != Null);
            system.assert(!actions.isEmpty());
        }
        
        Profile pricingAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.PRICING_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User pricingUser = EP_TestDataUtility.createUser(pricingAgent.id);
        database.insert(pricingUser);
        EP_Action__c act;
        system.runAs(SMTMuser){
            act = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            g = [Select Id from Group where Id =: act.OwnerId ];
            gm = new GroupMember(
                GroupId = g.Id,
                UserOrGroupId = pricingUser.Id
            );
            insert gm;
        }
        system.runAs(pricingUser){
            
            act.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(act, false);
            act.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(act, false);
            system.assertEquals(pricingUser.id,[Select id, OwnerId from EP_Action__c where id=: act.id].ownerID);
            system.assert(sv.isSuccess());
        }
    }
    /*
        Cannot access action record until pricing queue member is assigned on the pricing review action
    */
    static testMethod void wrongUserWithProfile(){
        Profile SMAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.SM_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
        UserRole SMTMRole = [Select id from UserRole where developerName = 'EP_SM_TM_Team_AU' limit 1];  
        UserRole internalReviewUserRole = [Select id from UserRole where developerName = 'EP_Internal_Review_Team_AU' limit 1];                 
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
        smTMUser.UserRoleId =  SMTMRole.id;        
        system.runAs(SMTMuser){
            loadData();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(sellToAccount);
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = sellToAccount.Id
            );
            Insert att;
        }
        Profile logisticsAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.LOGISTICS_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User logisticsUser = EP_TestDataUtility.createUser(logisticsAgent.id);
        logisticsUser.UserRoleId =  internalReviewUserRole.id;
        database.insert(logisticsUser);
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SELLTO limit 1];
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = logisticsUser.Id
        );
        insert gm;
        
        system.runAs(logisticsUser)
        {           
            action.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(action, false);
            action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            
            sv = database.update(action, false);
            system.debug('==========='+sv);
            system.assert(sv.isSuccess());
            
        } 
        Profile pricingAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.PRICING_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User pricingUser = EP_TestDataUtility.createUser(pricingAgent.id);
        database.insert(pricingUser);
        EP_Action__c act;
        system.runAs(SMTMuser){
            act = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
            g = [Select Id from Group where Id =: act.OwnerId ];
            gm = new GroupMember(
                GroupId = g.Id,
                UserOrGroupId = logisticsUser.Id
            );
            insert gm;
        }
        system.runAs(pricingUser){
            
            act.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(act, false);
            act.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
            sv = database.update(act, false);
            system.debug('==========='+sv);
            system.assertNotEquals(pricingUser.id,[Select id, OwnerId from EP_Action__c where id=: act.id].ownerID);
            system.assert(!sv.isSuccess());
        }
    }
    
    /*
        CHeck for Initiated Pricing Review
        Update Pricebook On Sell To before review completion
     */
     static testMethod void changeStatusBackTest() {
        Id pricebookId = Test.getStandardPricebookId();
        Profile SMAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.SM_AGENT_PROFILE  
                            limit :EP_Common_Constant.ONE];
        UserRole SMTMRole = [Select id from UserRole where developerName = 'EP_SM_TM_Team_AU' limit 1];  
        UserRole internalReviewUserRole = [Select id from UserRole where developerName = 'EP_Internal_Review_Team_AU' limit 1];                 
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
        smTMUser.UserRoleId =  SMTMRole.id;        
        system.runAs(SMTMuser){
            loadData();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(sellToAccount);
            Attachment att = new Attachment(
                Name = 'Test',
                Body = Blob.valueof('Test'),
                ParentId = sellToAccount.Id
            );
            Insert att;
        }
        
        Profile logisticsAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.LOGISTICS_PROFILE 
                            limit :EP_Common_Constant.ONE];
        User logisticsUser = EP_TestDataUtility.createUser(logisticsAgent.id);
        logisticsUser.UserRoleId =  internalReviewUserRole.id;
        database.insert(logisticsUser);
        EP_Action__c action = [Select Id, OwnerId from EP_Action__c where EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SELLTO limit 1];
        Group g = [Select Id from Group where Id =: action.OwnerId ];
        GroupMember gm = new GroupMember(
            GroupId = g.Id,
            UserOrGroupId = logisticsUser.Id
        );
        insert gm;
        
        system.runAs(logisticsUser)
        {           
            action.OwnerId = userinfo.getuserId();
            Database.saveresult sv = database.update(action, false);
            EP_Action__c act1 = [Select Id, Name,EP_Status__c FROM EP_Action__c WHERE EP_Account__c =: sellToAccount.Id AND RecordType.DeveloperName =: EP_Common_Constant.ACT_SELLTO limit 1];
            system.assertEquals(EP_Common_Constant.ACT_ASSIGNED_STATUS,act1.EP_Status__c);
            sellToAccount.EP_PriceBook__c = pricebookId; //17750
            Database.update(sellToAccount);
            action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS; 
            sv = database.update(action, false);
            action.EP_Status__c = '01-New'; //17754
            sv = database.update(action, false);
            system.assert(!sv.isSuccess());
        }
         
        system.runAs(SMTMuser){
        List<EP_Action__c> actions = [Select Id, OwnerId from EP_Action__c  where  RecordType.DeveloperName =: EP_Common_Constant.ACT_PRICING limit 1];
        system.assert(actions != Null);
        system.assert(!actions.isEmpty());
        }
    }
}