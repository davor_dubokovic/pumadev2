/**
 *  @Author <Vinay Jaiswal>
 *  @Name <EP_CustomerCreditMemoHandler>
 *  @CreateDate <06/21/2016>
 *  @Description <This apex class used to perform upsert operations for Credit Memo records>
 *  @Version <1.0>
 */
 public with sharing class EP_CustomerCreditMemoHandler {

 	private static final string METHOD_SYNC_CUSTOMER_CREDIT_MEMO = 'syncCustomerCreditMemo';  
 	private static list<EP_Customer_Credit_Memo_Item__c> listOfCreditMemoItem = NULL;
 	private static list<EP_Customer_Account_Statement_Item__c> listOfCustomerStatementItem = NULL;
 	private static map<String, String> mapOfCreditMemoItemKeyWithRefLineId = NULL;
 	private static map<String, EP_Invoice__c> mapOfNameWithInvoiceObj = NULL;
 	private static EP_GenericFinance_Json.EP_AcknowlegmentWrapper acknowledgementWrapper = NULL;
 	private static map<String, Id> mapOfCompositeKeyWithAccountId = NULL;
 	private static map<String, Id> mapOfInvoiceNameWithInvoiceId = NULL;
 	private static map<String, String> mapOfItemSeqIdWithCreditMemoSeqId = NULL;
 	private static map<String, String> mapOfStatementItemKeyWithCreditMemoSeqId = NULL;
 	private static map<String, String> mapOfItemSeqIdWithProductCode = NULL;
 	private static map<string, Id> mapOfCreditMemoKeyWithIds = NULL;
 	private static set<Id> setOfInvoicesIds; 
 	private static set<String> setOfProductCode;     
 	private static boolean isItemExist;
 	private static boolean isUpdate; 	

      /**
     * @author <Vinay Jaiswal>
     * @date <06/21/2016>
     * @description <This method is used to perform upsert operations for Credit Memo records>
     * @param List<EP_GenericFinance_Json.Document>
     * @return EP_GenericFinance_Json.EP_AcknowlegmentWrapper
     */
     public static EP_GenericFinance_Json.EP_AcknowlegmentWrapper syncCustomerCreditMemo(List<EP_GenericFinance_Json.Document> listOfDocument){
     	EP_GeneralUtility.Log('Public','EP_CustomerCreditMemoHandler','syncCustomerCreditMemo');
     	List<EP_GenericFinance_Json.Document> listOfDocumentsToBeCreated = new List<EP_GenericFinance_Json.Document>();
     	List<EP_GenericFinance_Json.Document> listOfDocumentsToBeUpdated  = new List<EP_GenericFinance_Json.Document>();
     	listOfCreditMemoItem = new list<EP_Customer_Credit_Memo_Item__c>();
     	listOfCustomerStatementItem =  new list<EP_Customer_Account_Statement_Item__c>();
     	mapOfCreditMemoItemKeyWithRefLineId = new Map<String, String>();
     	mapOfNameWithInvoiceObj = new Map<String, EP_Invoice__c>();
     	mapOfCreditMemoKeyWithIds = new map<string, Id>(); 		
     	setOfProductCode = new set<String>();
     	mapOfInvoiceNameWithInvoiceId = new Map<String, Id>();
     	mapOfItemSeqIdWithProductCode = new map<String, String>();
     	mapOfCompositeKeyWithAccountId = new  Map<String, Id>();
     	mapOfItemSeqIdWithCreditMemoSeqId = new map<String, String>();
     	mapOfStatementItemKeyWithCreditMemoSeqId = new map<String, String>();
     	acknowledgementWrapper = new EP_GenericFinance_Json.EP_AcknowlegmentWrapper();
     	//L4 #45304 Changes Start
     	setOfInvoicesIds = new Set<Id>();
     	//L4 #45304 Changes End
     	try{
     		if(!listOfDocument.isEmpty()){
     			for(EP_GenericFinance_Json.Document documentObject : listOfDocument){ 
     				if(EP_Common_Constant.CREATE.equalsIgnoreCase(documentObject.docState)){            
     					listOfDocumentsToBeCreated.add(documentObject);
     					} else {
     						if(EP_Common_Constant.APPLICATION.equalsIgnoreCase(documentObject.docState)){           
     							listOfDocumentsToBeUpdated.add(documentObject);
     						}
     					}	
     				}
     				if(!listOfDocumentsToBeCreated.isEmpty()){
     					upsertCreditMemo(listOfDocumentsToBeCreated);
     				}
     				if(!listOfDocumentsToBeUpdated.isEmpty()){
     					upsertCreditMemo(listOfDocumentsToBeUpdated);
     				} 
     			} 
     			}catch(Exception ex){
     				Database.rollback(EP_GenericFinance_Json.sp);
     				EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, METHOD_SYNC_CUSTOMER_CREDIT_MEMO
     					, EP_CustomerCreditMemoHandler.class.getName(), EP_Common_constant.ERROR, UserInfo.getUserId()
     					, EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
     				Map<String, String> mapSeqIdErrorDescription = new Map<String, String>();
     				for(EP_GenericFinance_Json.Document documentObject : listOfDocument){
     					mapSeqIdErrorDescription.put(documentObject.seqId, ex.getMessage());
     				}   
     				acknowledgementWrapper = EP_GenericFinance_Json.fillSendAcknowledgementWrapper(ex, NULL, mapSeqIdErrorDescription); 
     			}      
     			return acknowledgementWrapper; 
     		} 

    /**
     * @author <Vinay Jaiswal>
     * @date <06/21/2016>
     * @description <To upsert Customer Credit Memo records>
	 * @Param List<EP_GenericFinance_Json.Document>
     * @return void
     */
     private static void upsertCreditMemo(List<EP_GenericFinance_Json.Document> listOfDocument){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','upsertCreditMemo');
     	list<EP_Customer_Credit_Memo__c> listOfCreditMemoObj = new list<EP_Customer_Credit_Memo__c>();
     	map<string,Id> mapOfProductCodeWithId = new map<string,Id>();
     	map<string,String> mapOfNavSeqIdWithErrorMsg = new map<string,String>();
     	map<string,string> mapOfSuccessCreditMemoId = new map<String, String>();
     	Integer queryRows = EP_Common_Util.getQueryLimit();        
     	if(!listOfDocument.isEmpty()){
     		getCreditMemoKeyAndAccountDetails(listOfDocument);
     	}          
     	CreditMemoWrapper creditMemoWrapperObject = new CreditMemoWrapper();  			
     	for(EP_GenericFinance_Json.Document documentObject : listOfDocument){
     		isUpdate = false; 
     		creditMemoWrapperObject = createCreditMemoData(documentObject, mapOfCreditMemoKeyWithIds);
     		listOfCreditMemoObj.add(creditMemoWrapperObject.creditMemoObject);             
     	}

     	map<String,EP_Customer_Credit_Memo__c> mapOfSuccessCreditMemoObj = new map<String,EP_Customer_Credit_Memo__c>();           
     	List<Database.UpsertResult> lstCreditMemoUpsertResult = Database.Upsert(listOfCreditMemoObj, EP_Customer_Credit_Memo__c.Fields.EP_Credit_Memo_Key__c, true);                 
     	for(Integer count = 0 ; count < lstCreditMemoUpsertResult.size(); count++){        
     		if(lstCreditMemoUpsertResult[count].isSuccess()){
     			mapOfSuccessCreditMemoObj.put(listOfCreditMemoObj[count].EP_NavSeqId__c,listOfCreditMemoObj[count]);
     			mapOfSuccessCreditMemoId.put(listOfCreditMemoObj[count].EP_NavSeqId__c,listOfCreditMemoObj[count].Id);
     			}else{
     				for(Database.Error err : lstCreditMemoUpsertResult[count].getErrors()){
     					mapOfNavSeqIdWithErrorMsg.put(listOfCreditMemoObj[count].EP_NavSeqId__c, err.getMessage());
     				}               
     			}
     		}		
     		acknowledgementWrapper = EP_GenericFinance_Json.fillSendAcknowledgementWrapper( NULL, 
     			mapOfSuccessCreditMemoId,
     			mapOfNavSeqIdWithErrorMsg);       
     		if(!mapOfSuccessCreditMemoObj.isEmpty() && (isUpdate == false)) {
     			updateReferenceInvoice();
     			for(EP_Customer_Account_Statement_Item__c oCASItemRecord : listOfCustomerStatementItem){
     				oCASItemRecord.EP_Customer_Credit_Memo__c = mapOfSuccessCreditMemoObj.get(mapOfStatementItemKeyWithCreditMemoSeqId.get(oCASItemRecord.EP_Customer_Account_Statement_Item_Key__c)).Id;
     			}
     			for(Product2 oProduct : [select ProductCode from Product2 where ProductCode IN :setOfProductCode limit : queryRows]){
     				mapOfProductCodeWithId.put(oProduct.ProductCode , oProduct.Id);
     			}
     						
     			for(EP_Customer_Credit_Memo_Item__c oCreditMemoItemObj : listOfCreditMemoItem) {
     				oCreditMemoItemObj.EP_Credit_Memo__c = mapOfSuccessCreditMemoObj.get(mapOfItemSeqIdWithCreditMemoSeqId.get(oCreditMemoItemObj.EP_NavSeqId__c)).Id;
     				oCreditMemoItemObj.EP_Product__c = mapOfProductCodeWithId.get(mapOfItemSeqIdWithProductCode.get(oCreditMemoItemObj.EP_NavSeqId__c));
     			}
     			if(!listOfCreditMemoItem.isEmpty()){
     				insertCreditMemoItems(listOfCreditMemoItem);
     			}
     			if(!listOfCustomerStatementItem.isEmpty()){
     				Database.insert(listOfCustomerStatementItem, false);
     			}		
     		}          
     	}

	/**
     * @author <Vinay Jaiswal>
     * @date <23/01/2016>
     * @description <To get details related to Credit Memo document and Account details>
     * @param List<EP_Customer_Credit_Memo_Item__c>
     * @return void
     */
     private static void updateReferenceInvoice(){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','updateReferenceInvoice');
     	Integer queryRows = EP_Common_Util.getQueryLimit();
     	Map<String, String> mapOfCreditMemoKeyWithId = new Map<String, String>();	
     	Map<String, String> mapOfRelatesInvoiceWithCreditMemoKey = new Map<String, String>();
     	for(EP_Customer_Credit_Memo__c oCreditMemo : [SELECT Id, EP_Credit_Memo_Type__c, EP_Credit_Memo_Key__c,EP_Relates_To_Invoice__c 
     		FROM EP_Customer_Credit_Memo__c limit :queryRows]) {
     		mapOfCreditMemoKeyWithId.put(oCreditMemo.EP_Credit_Memo_Key__c, oCreditMemo.Id);
     		mapOfRelatesInvoiceWithCreditMemoKey.put(oCreditMemo.EP_Relates_To_Invoice__c , oCreditMemo.EP_Credit_Memo_Key__c);
     	}
     	List<EP_Invoice__c> listOfInvoice = new List<EP_Invoice__c>();
     	for(EP_Invoice__c oInvoice : mapOfNameWithInvoiceObj.values()){
     		oInvoice.EP_Remaining_Balance__c = 0.0;
     		oInvoice.EP_Credit_Memo__c =  mapOfCreditMemoKeyWithId.get(mapOfRelatesInvoiceWithCreditMemoKey.get(oInvoice.Id)); 
     		if(!setOfInvoicesIds.isEmpty()){
     			Integer noOfRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
     			for(EP_Invoice__c oInvoiceObj : [Select Id from EP_Invoice__c where ID IN: setOfInvoicesIds LIMIT : noOfRows ]) {
     				oInvoiceObj.EP_Credit_Memo__c = mapOfCreditMemoKeyWithId.get(mapOfRelatesInvoiceWithCreditMemoKey.get(oInvoice.Id));
     				listOfInvoice.add(oInvoiceObj);
     			}
     		}              
     		listOfInvoice.add(oInvoice);
     	}
     	if(!listOfInvoice.isEmpty()){
     		Database.update(listOfInvoice, true);
     	}
     }

	/**
     * @author <Vinay Jaiswal>
     * @date <01/23/2016>
     * @description <To get details related to Credit Memo document and Account details>
     * @param List<EP_Customer_Credit_Memo_Item__c>
     * @return void
     */
     private static void insertCreditMemoItems(list<EP_Customer_Credit_Memo_Item__c> listOfCreditMemoItems){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','insertCreditMemoItems');
     	list<EP_Customer_Credit_Memo_Item__c> listOfReferenceCreditMemoItems;
     	map<String,EP_Customer_Credit_Memo_Item__c> mapOfSuccessCreditMemoItems;
     	Integer queryRows = EP_Common_Util.getQueryLimit();
     	if(!listOfCreditMemoItems.isEmpty()){
     		mapOfSuccessCreditMemoItems = new map<String,EP_Customer_Credit_Memo_Item__c>();
     		List<Database.SaveResult> listOfCreditMemoItemsSaveResult  = Database.Insert(listOfCreditMemoItems,true);
     		for(Integer count = 0 ; count < listOfCreditMemoItemsSaveResult.size(); count++){           
     			if(listOfCreditMemoItemsSaveResult[count].isSuccess()){
     				mapOfSuccessCreditMemoItems.put(listOfCreditMemoItems[count].EP_NavSeqId__c,listOfCreditMemoItems[count]);
     			}
     		}                   
     	}
     	if(!mapOfSuccessCreditMemoItems.isEmpty()){  
     		listOfReferenceCreditMemoItems = new list<EP_Customer_Credit_Memo_Item__c>();           
     		map<String, Id> mapOfItemLineValueWithItemId = new map<String, Id>();
     		for(EP_Customer_Credit_Memo_Item__c creditMemoItemObj : [Select Id , EP_Credit_Memo_Line_Id__c from EP_Customer_Credit_Memo_Item__c 
     			where EP_Credit_Memo_Line_Id__c IN: mapOfCreditMemoItemKeyWithRefLineId.values() LIMIT 1]){
     			mapOfItemLineValueWithItemId.put(creditMemoItemObj.EP_Credit_Memo_Line_Id__c , creditMemoItemObj.Id);        
     		}
     		for(EP_Customer_Credit_Memo_Item__c creditMemoItemObj : [Select Id, EP_Credit_Memo_Item_Key__c from EP_Customer_Credit_Memo_Item__c 
     			where EP_Credit_Memo_Item_Key__c IN: mapOfCreditMemoItemKeyWithRefLineId.Keyset() LIMIT :queryRows]){
     			listOfReferenceCreditMemoItems.add(creditMemoItemObj);         
     		}
     		for(EP_Customer_Credit_Memo_Item__c creditMemoItemObj : listOfReferenceCreditMemoItems){
     			creditMemoItemObj.EP_Parent_Line_Item__c =  mapOfItemLineValueWithItemId.get(mapOfCreditMemoItemKeyWithRefLineId.get(creditMemoItemObj.EP_Credit_Memo_Item_Key__c));         
     		}    
     	}          
     	if(!listOfReferenceCreditMemoItems.isEmpty()){
     		Database.update(listOfReferenceCreditMemoItems, true);
     	}
     }

	/**
     * @author <Vinay Jaiswal>
     * @date <01/23/2016>
     * @description <To get details related to Credit Memo document and Account details>
     * @param list<EP_GenericFinance_Json.Document>
     * @return void
     */ 
     private static void getCreditMemoKeyAndAccountDetails(list<EP_GenericFinance_Json.Document> listOfDocument){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','getCreditMemoKeyAndAccountDetails');
     	set<String> setOfAccountCompositeKey = new set<String>();
     	set<String> setOfInvoiceReferenceInfo = new set<String>();
     	set<string> setOfCreditMemoKey = new set<String>();
     	set<Id> setOfOrderIds;

     	for(EP_GenericFinance_Json.Document documentObject : listOfDocument) {
     		string compositkey = documentObject.identifier.clientId + EP_Common_Constant.HYPHEN +documentObject.identifier.billTo;
     		setOfAccountCompositeKey.add(compositkey);
     		setOfCreditMemoKey.add(getUniqueKey(documentObject));
     		for(EP_GenericFinance_Json.RefDoc refDoc : documentObject.refDocs.refDoc) {
     			if(String.isNotBlank(refDoc.identifier.refDocId)) {
     				setOfInvoiceReferenceInfo.add(refDoc.identifier.refDocId);
     			}
     		}
     	}			
     	getAccountNumberIdMap(setOfAccountCompositeKey, new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
     	getInvoiceNumberIdMap(setOfInvoiceReferenceInfo);
     	Integer numOfQueryRows = EP_Common_Util.getQueryLimit();
     	if(!mapOfInvoiceNameWithInvoiceId.isEmpty()) {
     		setOfOrderIds = new Set<Id>();
     		for(EP_Invoice_Line_Item__c invLineItemObj : [SELECT EP_Invoice__c, EP_Order__c FROM EP_Invoice_Line_Item__c 
     			WHERE EP_Invoice__c IN:mapOfInvoiceNameWithInvoiceId.values() LIMIT : numOfQueryRows]) {
     			setOfOrderIds.add(invLineItemObj.EP_Order__c);
     		}
     	}
     	if(setOfOrderIds != NULL && !setOfOrderIds.isEmpty()){
     		Id proFormaInvoiceRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.INVOICE_OBJECT,
     			EP_Common_Constant.PRO_FORMA_INVOICE_RECORD_TYPE_NAME);
     		setOfInvoicesIds = new Set<Id>(); 
     		for(EP_Invoice_Line_Item__c invLineItemObj : [SELECT EP_Invoice__c, EP_Order__c FROM EP_Invoice_Line_Item__c WHERE EP_Order__c IN: setOfOrderIds 
     			AND EP_Invoice__r.RecordTypeId =: proFormaInvoiceRecordTypeId 
     			AND EP_Invoice__r.EP_Status__c !=: EP_Common_Constant.CREDITED LIMIT : numOfQueryRows]) {
     			if(invLineItemObj.EP_Invoice__c <> NULL){
     				setOfInvoicesIds.add(invLineItemObj.EP_Invoice__c);                        
     			}               
     		}
     	}
     	if(!setOfCreditMemoKey.isEmpty()){      
     		for(EP_Customer_Credit_Memo__c obj :[Select Id, EP_Credit_Memo_Key__c from EP_Customer_Credit_Memo__c 
     			WHERE  EP_Credit_Memo_Key__c IN : setOfCreditMemoKey LIMIT : numOfQueryRows] ){
     			mapOfCreditMemoKeyWithIds.put(obj.EP_Credit_Memo_Key__c, obj.Id);
     		}             
     	}
     }

	/**
     * @author <Vinay Jaiswal>
     * @date <01/23/2016>
     * @description <To Create Credit Memo if not available>
     * @param EP_GenericFinance_Json.Document
     * @return CreditMemoWrapper
     */      
     private static CreditMemoWrapper createCreditMemoIfNotAvailable(EP_GenericFinance_Json.Document documentObject){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','createCreditMemoIfNotAvailable');
     	EP_Customer_Credit_Memo__c creditMemoReference;
     	EP_Customer_Credit_Memo__c creditMemoObj = new EP_Customer_Credit_Memo__c();
     	CreditMemoWrapper creditMemoWrapperObject = new CreditMemoWrapper();                                       
     	String compositBillToKey = (documentObject.identifier.clientId + EP_Common_Constant.HYPHEN + documentObject.identifier.billTo).ToUpperCase();  
     	creditMemoObj.EP_Credit_Memo_Number__c = documentObject.identifier.docId;
     	creditMemoObj.EP_NavSeqId__c = documentObject.seqId;          
     	  
     	/** TR Datetime 59846 Start **/                          
     	creditMemoObj.EP_Credit_Memo_Issue_Date__c =  EP_DateTimeUtility.convertStringToDate(documentObject.submissionDate);//Date.valueof(documentObject.submissionDate);                            
     	creditMemoObj.EP_Credit_Memo_Due_Date__c = EP_DateTimeUtility.convertStringToDate(documentObject.dueDate);//Date.valueOf(documentObject.dueDate);                         
     	/** TR Datetime 59846 End **/
     	creditMemoObj.EP_Credit_Memo_Remaining_Balance__c = decimal.valueOf(String.valueOf(documentObject.remainingAmount).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));                                   
     	creditMemoObj.EP_Credit_Memo_Total__c = decimal.valueOf(String.valueOf(documentObject.amount).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));                    
     	creditMemoObj.CurrencyIsoCode = documentObject.currencyId;               
     	creditMemoObj.EP_Credit_Memo_Key__c = getUniqueKey(documentObject);  
     	creditMemoObj.EP_Credit_Memo_Pull_Status__c = EP_Common_Constant.COMPLETED;
     	
     	//L4 #45304 Changes Start
     	creditMemoObj.EP_Is_Reversal__c =  documentObject.isReversal != null && documentObject.isReversal.equalsIgnoreCase(EP_COMMON_CONSTANT.YES) ? true : false ;
     	creditMemoObj.EP_Entry_Number__c = documentObject.identifier.entryNr;
     	//L4 #45304 Changes End
     	//L4 #45373 Changes Start
        creditMemoObj.EP_Reason_for_Credit_Memo__c = documentObject.description != null  ? documentObject.description : EP_Common_Constant.BLANK ;
        //L4 #45373 Changes End
     	
     	if(mapOfCompositeKeyWithAccountId.containsKey(compositBillToKey)){
     		creditMemoObj.EP_Bill_To__c = mapOfCompositeKeyWithAccountId.get(compositBillToKey);
     		/* L4 - 45352 -- unique field changes start*/
            creditMemoObj.EP_Payment_Term__c = getPaymentTermId(documentObject.paymentTerm,documentObject.identifier.clientId);
            /* L4 - 45352 -- unique field changes end*/
		}
     	creditMemoObj.EP_Relates_To_Invoice__c= mapOfInvoiceNameWithInvoiceId.get(documentObject.refDocs.refDoc.get(0).identifier.refDocId);
     	creditMemoReference = new EP_Customer_Credit_Memo__c(EP_NavSeqId__c=documentObject.seqId);
     	createCreditMemoItem(documentObject, creditMemoReference);
     	createCASItemRecord(documentObject, creditMemoReference);
     	if(isItemExist == true){
     		creditMemoObj.EP_Credit_Memo_Type__c = EP_Common_Constant.GLAndItems;
     	}
     	else{
     		creditMemoObj.EP_Credit_Memo_Type__c = EP_Common_Constant.GLOnly;
     	}
     	creditMemoWrapperObject.creditMemoObject = creditMemoObj;
     	return creditMemoWrapperObject;
     }

    /**
     * @author <Vinay Jaiswal>
     * @date <01/23/2016>
     * @description <To find the already existing Credit Memo>
     * @param EP_GenericFinance_Json.Document, map<string, Id>
     * @return EP_Customer_Credit_Memo__c
     */  
     private static EP_Customer_Credit_Memo__c findIfAlreadyExistCreditMemo(EP_GenericFinance_Json.Document documentObject, map<string, Id> mapExistCreditMemo) {
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','findIfAlreadyExistCreditMemo');
     	EP_Customer_Credit_Memo__c creditMemoObject;
     	if(mapExistCreditMemo.containsKey(getUniqueKey(documentObject))){  
     		isUpdate = true; 
     		creditMemoObject = new EP_Customer_Credit_Memo__c();
     		creditMemoObject.id = (Id)mapExistCreditMemo.get(getUniqueKey(documentObject));
     		creditMemoObject.EP_NavSeqId__c = documentObject.seqId;
     		//L4 #45373 Changes Start
	        creditMemoObject.EP_Reason_for_Credit_Memo__c = documentObject.description != null  ? documentObject.description : EP_Common_Constant.BLANK ;
	        //L4 #45373 Changes End
     		if(!String.isBlank(documentObject.remainingAmount)){           
     			creditMemoObject.EP_Credit_Memo_Remaining_Balance__c = Decimal.valueOf(String.valueOf(documentObject.remainingAmount).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));                
     		}
     		creditMemoObject.EP_Credit_Memo_Key__c = getUniqueKey(documentObject);			
     	}
     	return creditMemoObject;
     }	

	/**
     * @author <Vinay Jaiswal>
     * @date <01/23/2016>
     * @description <To Create Data for Credit Memo>
     * @param EP_GenericFinance_Json.Document,  map<string, Id>
     * @return CreditMemoWrapper
     */  
     private static CreditMemoWrapper createCreditMemoData(EP_GenericFinance_Json.Document documentObject, map<string, Id> mapExistCreditMemo){        
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','createCreditMemoData');
     	CreditMemoWrapper creditMemoWrapperObject  = new CreditMemoWrapper();
     	if(!mapExistCreditMemo.isEmpty()){
     		creditMemoWrapperObject.creditMemoObject = findIfAlreadyExistCreditMemo(documentObject, mapExistCreditMemo);            
     		} else{
     			creditMemoWrapperObject = createCreditMemoIfNotAvailable(documentObject);         
     		}   
     		return creditMemoWrapperObject;
     	}

    /**
     * @author <Vinay Jaiswal>
     * @date <06/21/2016>
     * @description <To create Credit Memo Items line>
	 * @param EP_GenericFinance_Json.Document, EP_Customer_Credit_Memo__c
     * @return void
     */
     private static void createCreditMemoItem(EP_GenericFinance_Json.Document documentObject, EP_Customer_Credit_Memo__c oCreditMemoReference){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','createCreditMemoItem');
     	isItemExist = false;
     	EP_Customer_Credit_Memo_Item__c oCreditMemoItem;
     	for(EP_GenericFinance_Json.Line oLine : documentObject.lines.line){
     		oCreditMemoItem = new EP_Customer_Credit_Memo_Item__c();
     		String itemId = oLine.itemId + EP_Common_Constant.STRING_HYPHEN + documentObject.identifier.clientId;
     		if(!string.isBlank(oLine.lineType) && oLine.lineType.equalsIgnoreCase(EP_Common_Constant.ITEM)){
     			if(!string.isBlank(oLine.itemId)){
     				mapOfItemSeqIdWithProductCode.put(oLine.seqId , oLine.itemId);
     				setOfProductCode.add(oLine.itemId);
     				isItemExist = true;
     			}
     		}
     		oCreditMemoItem.EP_NavSeqId__c = oLine.seqId;
     		oCreditMemoItem.EP_Description__c = oLine.desc_x;
     		oCreditMemoItem.CurrencyIsoCode = documentObject.currencyId; 
     		oCreditMemoItem.EP_Credit_Memo_Line_Type__c = oLine.lineType; 				
     		if(!string.isBlank(oLine.unitPrice)){
     			oCreditMemoItem.EP_Unit_Price__c = Decimal.valueOf(oLine.unitPrice.replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));   
     		}
     		if(!string.isBlank(oLine.identifier.lineId)){
     			oCreditMemoItem.EP_Credit_Memo_Line_Id__c = oLine.identifier.lineId;
     		}
     		if(!string.isBlank(oLine.pct)){
     			oCreditMemoItem.EP_Percentage__c = Decimal.valueOf(oLine.pct);  
     		}
     		if(!string.isBlank(oLine.qty)){
     			oCreditMemoItem.EP_Quantity__c = decimal.valueOf(String.valueOf(oLine.qty).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK))>0?decimal.valueOf(String.valueOf(oLine.qty).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)):null;                                                                                            
     		}
     		if(string.isNotEmpty(oLine.uom)){
     			oCreditMemoItem.EP_UoM__c = oLine.uom;
     		}                                
     		if(!string.isBlank(oLine.totalAmount)){
     			oCreditMemoItem.EP_Total_Amount__c = decimal.valueOf(String.valueOf(oLine.totalAmount).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));
     		}
     		if(!string.isBlank(oLine.vatAmount) || !string.isBlank(oLine.amount)){
     			oCreditMemoItem.EP_Amount__c = EP_Common_Constant.VAT.equals(oLine.lineType) ?decimal.valueOf(String.valueOf(oLine.vatAmount).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)) : decimal.valueOf(String.valueOf(oLine.amount).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK)); 
     		} 
     		//L4 #45304 Changes Start - Added entryNr in composit key formation
     		oCreditMemoItem.EP_Credit_Memo_Item_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{documentObject.identifier.billTo,
     																										documentObject.identifier.docId,
     																										documentObject.docDate ,
     																										oLine.identifier.lineId,
     																										documentObject.identifier.entryNr});                 
     		//L4 #45304 Changes Start
     		
     		//L4 #45373 Changes Start
	        oCreditMemoItem.EP_Ship_To_Number__c = oLine.shipToId != null  ? oLine.shipToId : EP_Common_Constant.BLANK ;
	        //L4 #45373 Changes End
     		if(!string.isBlank(oLine.refLineId) && String.isNotBlank(oCreditMemoItem.EP_NavSeqId__c)){
     			if(oLine.refLineId <> EP_Common_Constant.NULL_VAR ){
     				mapOfCreditMemoItemKeyWithRefLineId.put(oCreditMemoItem.EP_Credit_Memo_Item_Key__c , oLine.refLineId );
     			}                   
     		}                
     		mapOfItemSeqIdWithCreditMemoSeqId.put(oCreditMemoItem.EP_NavSeqId__c,oCreditMemoReference.EP_NavSeqId__c);
     		listOfCreditMemoItem.add(oCreditMemoItem);  
     	} 
     }

	 /**
     * @author <Vinay Jaiswal>
     * @date <06/21/2016>
     * @description <To create Customer Account Statement Item>
	 * @param EP_GenericFinance_Json.Document, EP_Customer_Credit_Memo__c
     * @return void
     */
     private static void createCASItemRecord(EP_GenericFinance_Json.Document docObj, EP_Customer_Credit_Memo__c oCreditMemoReference){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','createCASItemRecord');
     	String KeyCM = 'CM';
     	String billToCompositeId = (docObj.identifier.clientId + EP_Common_Constant.HYPHEN + docObj.identifier.billTo).toUpperCase();
     	EP_Customer_Account_Statement_Item__c oCASItemRecord = new EP_Customer_Account_Statement_Item__c(); 		
     	oCASItemRecord.EP_NAV_Statement_Item_ID__c = docObj.identifier.docId;
     	/** TR Datetime 59846 Start **/
     	oCASItemRecord.EP_Statement_Item_Issue_Date__c = EP_DateTimeUtility.convertStringToDate(docObj.docDate);//Date.valueOf(docObj.docDate);
     	/** TR Datetime 59846 End **/
     	oCASItemRecord.CurrencyIsoCode = docObj.currencyId;
     	oCASItemRecord.EP_Bill_To__c = mapOfCompositeKeyWithAccountId.get(billToCompositeId);  
     	if(!String.isBlank(docObj.amount)){
     		oCASItemRecord.EP_Credit__c = decimal.valueOf(String.valueOf(docObj.amount).replace(EP_Common_Constant.COMMA,EP_Common_Constant.BLANK));
     	}															
     	oCASItemRecord.EP_Customer_Account_Statement_Item_Key__c =  EP_DocumentUtil.generateCASItemKey(keyCM, docObj.identifier.billTo, docObj.identifier.docId, docObj.docDate, EP_Common_Constant.BLANK);
     	oCASItemRecord.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERACCOUNTSTATEMENTITEM_OBJECT, EP_Common_Constant.CREDITMEMO_RECORD_TYPE_NAME);    
     	mapOfStatementItemKeyWithCreditMemoSeqId.put(oCASItemRecord.EP_Customer_Account_Statement_Item_Key__c, oCreditMemoReference.EP_NavSeqId__c);      
     	listOfCustomerStatementItem.add(oCASItemRecord);                                                                               
     }

    /**
     * @author <Vinay Jaiswal>
     * @date <06/21/2016>
     * @description <This method is used to get Map containing 'AccountNumber' as key and 'Id' as value corresponding to a set of specific RecordType 'DeveloperName'>
     * @param Set<String>, Set<String>
     * @return void
     */
     private static void getAccountNumberIdMap(Set<String> acctNumSet, Set<String> recTypeDevNameSet) {
     	for(Account acct :[SELECT Id, AccountNumber, EP_Composite_Id__c FROM Account WHERE EP_Composite_Id__c IN:acctNumSet 
     		AND RecordType.DeveloperName IN:recTypeDevNameSet LIMIT 1]) {            
     		mapOfCompositeKeyWithAccountId.put(acct.EP_Composite_Id__c.toUpperCase(), acct.Id);

     	}
     }

	/**
     * @author <Vinay Jaiswal>
     * @date <01/18/2017>
     * @description <This method is used to get unique key details>
     * @param EP_GenericFinance_Json.Document
     * @return String
     */  
     private static String getUniqueKey(EP_GenericFinance_Json.Document documentObject){
     	EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','getUniqueKey');
     	//L4 #45304 Changes Start - Added entryNr in composit key formation
     	return EP_DocumentUtil.generateUniqueKey(new Set<String>{documentObject.identifier.billTo, documentObject.identifier.docId, documentObject.identifier.clientId, documentObject.identifier.entryNr});		
     	//L4 #45304 Changes End
     }

    /**
     * @author <Vinay Jaiswal>
     * @date <06/21/2016>
     * @description <This method is used to get Map containing 'Name' as key and 'Id' as value of 'EP_Invoice__c' custom object>
     * @param Set<String>
     * @return void
     */
     private static void getInvoiceNumberIdMap(Set<String> invNumSet) {
     	for(EP_Invoice__c inv : [SELECT Id, Name FROM EP_Invoice__c WHERE Name IN:invNumSet LIMIT 1]) {
     		mapOfInvoiceNameWithInvoiceId.put(inv.Name, inv.Id);
     		mapOfNameWithInvoiceObj.put(inv.Name, inv);
     	}
     }
    /* L4 - 45352 -- unique field changes start*/
    /**
     * @author <Vinay Jaiswal>
     * @date <06/21/2016>
     * @description <This method is used to get value of EP_Payment_Term__c object>
     * @param String
     * @return Id
     */
     private static Id getPaymentTermId(String sPaymentTerm,String CompanyCode){
        EP_GeneralUtility.Log('Private','EP_CustomerCreditMemoHandler','getPaymentTermId');
        return EP_PaymentTermMapper.getPaymentTermsfromCompanyAndPaymentCode(sPaymentTerm,CompanyCode).id;     
     }
    /* L4 - 45352 -- unique field changes end*/

	/**
     * @author <Vinay Jaiswal>
     * @name <CreditMemoWrapper> 
     * @CreateDate <01/23/2016>
     * @description <This class used to create new EP_Customer_Credit_Memo__c record> 
     * @version <1.0>
     */ 
     private class CreditMemoWrapper {
     	public EP_Customer_Credit_Memo__c creditMemoObject ;
        /**
         * Constructor
         */
         public CreditMemoWrapper(){
         	this.creditMemoObject = new EP_Customer_Credit_Memo__c();
         }
     }  
 }