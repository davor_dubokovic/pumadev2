/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToSetupToSetup>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 04-Account Set-up to 04-AccountSet-up>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToSetupToSetup extends EP_AccountStateTransition {

    public EP_ASTStorageShipToSetupToSetup () {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToSetupToSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToSetupToSetup', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToSetupToSetup',' isGuardCondition');        
        return true;
    }
}