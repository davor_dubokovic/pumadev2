/** 
 * @author <Ashok Arora>
 * @name <EP_LeadTriggerHandler>
 * @createDate <10/11/2015>
 * @description <This class handles requests from Lead Trigger> 
 * @version <1.0>
 */
public with sharing class EP_LeadTriggerHandler{
	
	public static Boolean isExecuteBeforeUpdate = false; 
	public static Boolean isExecuteAfterUpdate = false;
	
	private static final String DO_BEFORE_INSERT = 'doBeforeInsert';
	private static final String DO_BEFORE_UPDATE = 'doBeforeUpdate';
	private static final String DO_AFTER_UPDATE = 'doAfterUpdate';
	
	/**
	 * @author <Ashok Arora>
	 * @description <This method handles before update requests from Lead trigger>
	 * @name <doBeforeUpdate>
	 * @date <10/11/2015>
	 * @param List<Lead>, List<Lead>, Map<Id, Lead>, Map<Id, Lead>
	 * @return void
	 */
	public static void doBeforeUpdate(List<Lead> listOfOldLeads, List<Lead> listOfNewLeads, 
	Map<Id, Lead>mapOldLeadIdLeadObject, Map<Id, Lead>mapNewLeadIdLeadObject){
		isExecuteBeforeUpdate = true;//Set true to avoid re-execution
		try{
			//Update record type of lead to acepted once it is approved
			EP_LeadTriggerHelper.updateAcceptedRecordType(listOfNewLeads); 
			//To populate lead data from country object
			EP_LeadTriggerHelper.populateLeadData(listOfNewLeads);
			//Update validate address checkbox
			EP_LeadTriggerHelper.validateLeadAddress(mapOldLeadIdLeadObject,mapNewLeadIdLeadObject);
		}
		catch(Exception e){
			EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_UPDATE, EP_LeadTriggerHandler.Class.getName(), ApexPages.severity.ERROR);
		}
	}
	
	/**
	 * @author <Ashok Arora>
	 * @description <This method handles before insert requests from Lead trigger>
	 * @name <doBeforeInsert>
	 * @date <10/11/2015>
	 * @param List<Lead>
	 * @return void
	 */
	public static void doBeforeInsert(List<Lead> listOfLeads){
		//To populate lead data from country object on before insert operation
		try{
			EP_LeadTriggerHelper.populateLeadData(listOfLeads);
			EP_LeadTriggerHelper.createCase(listOfLeads);
		}
		catch(Exception e){
			EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_BEFORE_INSERT, EP_LeadTriggerHandler.Class.getName(), ApexPages.severity.ERROR);
		}
	}

	/**
	 * @author <Ashok Arora>
	 * @description <This method handles after update requests from Lead trigger>
	 * @name <doAfterUpdate>
	 * @date <10/11/2015>
	 * @param List<Lead>, List<Lead>, Map<Id, Lead>, Map<Id, Lead>
	 * @return void
	 */
	public static void doAfterUpdate(List<Lead> listOfOldLeads, List<Lead> listOfNewLeads, 
	Map<Id, Lead>mapOldLeadIdLeadObject ,Map<Id, Lead>mapNewLeadIdLeadObject){
		isExecuteAfterUpdate = true;//Set true to avoid re-execution
		try{
			//Map payment method from converted lead and default payment term on converted account
			EP_LeadTriggerHelper.mapPaymentMethodAndTerm(mapNewLeadIdLeadObject);
			//Update product interested in on case
			EP_LeadTriggerHelper.updateCaseProductInterestedIn(mapOldLeadIdLeadObject,mapNewLeadIdLeadObject);
		}
		catch(Exception e){
			EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, DO_AFTER_UPDATE, EP_LeadTriggerHandler.Class.getName(), ApexPages.severity.ERROR);
		}
	}
}