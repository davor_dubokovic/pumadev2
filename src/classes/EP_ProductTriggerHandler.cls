/* 
   @Author........<Amit Singh>
   @name..........<EP_ProductTriggerHandler>
   @CreateDate....<08/06/2016>
   @Description...<This class handles requests from ProductTrigger>  
   @Version.......<1.0>
*/
public with sharing class EP_ProductTriggerHandler {

    private static final string doAfterInsert = 'doAfterInsert';
    
    
    /**
     * @author <Amit Singh>
     * @date <08/06/2017>
     * @description <This method performs logic which need to pre execute 
    *               on after insert event of product record.>
     * @param List<Product2>
     * @return NONE
     */     
    public static void doAfterInsert(List<Product2> listOfProducts){
        try{
            //To create standard price of products
            EP_ProductTriggerHelper.createStandardPrice(listOfProducts);
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, doAfterInsert, EP_ProductTriggerHandler.class.getName() ,apexPages.severity.ERROR);
        }
    }
}