@isTest
/*
    Test class to implement change request controller functions.
*/
private class EP_ChangeRequestController_Test {
    private static Account nonVmiShipToAccount;
    private static Account sellToAccount;
    private static EP_Country__c country;
    private static EP_Region__c region;
    private static string TANKOBJ = 'EP_Tank__c';
    private static EP_Tank__C tankObj1;
    private static Product2 product;
    private static Contact contactRec;
    private static string ID = 'id';
    private static string TYPE = 'type';
    private static string RETURN_TYPE = 'returl';
    private static string OPERATION = 'op';
    private static string UPDATE_OPERATION = 'UPDATE';
    private static string NEW_OPERATION = 'NEW';
    private static string RECORD_TYPE = 'rtype';
    private static String ACCOUNTSTR = 'Account';
    private static String SHIPTONONVMIRECORDTYPE = 'EP_Non_VMI_Ship_To';
    private static String SELLTORECORDTYPE = 'EP_Sell_To';
    private static String BANKACCOUNTOBJ = 'EP_Bank_Account__c';
    private static String TANK_RECORD_TYPE = 'EP_Retail_Site_Tank';
    private static String COUNTRY_NAME = 'India';
    private static String COUNTRY_CODE = 'IND';
    private static String COUNTRY_REGION = 'Asia';
    private static String REGION_NAME = 'Asia';
    private static string RECORD_TYPE_NAME = 'recTypeName';
   /* 
        Create common Data for all methods 
    */
    private static void loadTestData() {
        
        EP_ExceptionEmailTo__c exEm = new EP_ExceptionEmailTo__c(SetupOwnerId=UserInfo.getOrganizationId(), Email__c = 'test@xyz.com');
        Database.insert(exEm,false);
        
        country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country,false);
        
        region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        Database.insert(region,false);
        
        //sellToAccount =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
        sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        Database.insert(sellToAccount, false);
        contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
           
        EP_Freight_Matrix__c FreightMat = EP_TestDataUtility.createFreightMatrix();
        Database.insert(FreightMat,false);
        
        Account billAcc = EP_TestDataUtility.createBillToAccount();
        billAcc.EP_Freight_Matrix__c = FreightMat.id;
        Database.insert(billAcc,false);
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE; 
        sellToAccount.EP_Bill_To_Account__c = billAcc.id;   
        Database.update(sellToAccount, false); 
        
        
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
        nonVmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        nonVmiShipToAccount.EP_Delivery_Type__c = 'Ex-Rack';
        nonVmiShipToAccount.EP_Pumps__c= 10;
        //database.insert(nonVmiShipToAccount,false);
        Database.insert(nonVmiShipToAccount,false);
        
        //create Product
        product = EP_TestDataUtility.createTestRecordsForProduct();
        // insert tanks in order to mark vmi account as Active
        tankObj1 = EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id,product.id);
        tankObj1.EP_Tank_Code__c='2';
        database.insert(tankObj1,false);
        
        // Make non Vmi account active
        nonVmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(nonVmiShipToAccount,false);
        //Insert Bank Account
        EP_Bank_Account__c bankAccObj =  EP_TestDataUtility.createBankAccount(sellToAccount.id);
        Database.insert(bankAccObj,false);  
    }
    
        /* 
            Ship to account fields check for CSC user
        */
      static testMethod void testFieldsInShipToFieldSet(){
            //CREATE CSC USER
            Profile cscAgent = [Select id from Profile
                            Where Name =: EP_Common_Constant.CSC_AGENT_PROFILE 
                            limit :EP_Common_Constant.ONE];
            User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
            System.runAs(cscUser){  
            loadTestData(); 
            test.startTest();
            ApexPages.currentPage().getParameters().put(ID, nonVmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put(TYPE, 'Account');
            ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+nonVmiShipToAccount.Id);
            ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
            ApexPages.currentPage().getParameters().put(RECORD_TYPE_NAME, EP_Common_Constant.NON_VMI_SHIP_TO);
            EP_ChangeRequestController controller = new EP_ChangeRequestController();
            Map<string, integer> fieldByIndexMap = new Map<string, integer>();
            integer count=0;
            for(EP_ChangeRequestController.cRWrapperFields fls : controller.allFieldValues){
                System.debug('======='+fls.crlobj.EP_Source_Field_API_Name__c);
                fieldByIndexMap.put(fls.crlobj.EP_Source_Field_API_Name__c.toLowerCase(), count);
                count++;
            }   
            test.stopTest();

            //system.assert(fieldByIndexMap.containskey('EP_Delivery_Type__c'.toLowerCase()));
            //system.assert(fieldByIndexMap.containskey('EP_Number_of_Hoses__c'.toLowerCase()));
            system.assert(fieldByIndexMap.containskey('EP_Pumps__c'.toLowerCase()));
           // system.assert(fieldByIndexMap.containskey('ShippingCity'.toLowerCase()));
           // system.assert(fieldByIndexMap.containskey('ShippingStreet'.toLowerCase()));
          //  system.assert(fieldByIndexMap.containskey('ShippingState'.toLowerCase()));
          //  system.assert(fieldByIndexMap.containskey('ShippingPostalCode'.toLowerCase())); Address validation change
            system.assert(fieldByIndexMap.containskey('EP_Ship_To_Type__c'.toLowerCase()));
            system.assert(fieldByIndexMap.containskey('EP_Status__c'.toLowerCase()));
           // system.assert(fieldByIndexMap.containskey('EP_Is_Blocked_Required__c'.toLowerCase()));
            //system.assert(fieldByIndexMap.containskey('EP_Reason_Blocked__c'.toLowerCase()));
            //system.assertEquals(false,fieldByIndexMap.containskey('EP_Region__c'.toLowerCase()));
            controller.allFieldValues[0].ischanged = true;
            controller.allFieldValues[0].crlobj.EP_New_Value__c = 'Ex-Rack';
            controller.submit();
            system.assert(controller.request.id != null);
          }
      }  
       /* 
            KYC user access check for change request page
      */
      static testMethod void testKYCAccess(){
        Profile kycReviewer = [Select id from Profile
                                    Where Name =:EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE  
                                    limit :EP_Common_Constant.ONE];
        User kycUser = EP_TestDataUtility.createUser(kycReviewer.id); 
        System.runAs(kycUser){
        loadTestData();
        test.startTest();
        PageReference pageRef = Page.EP_ChangeRequest;
        Test.setCurrentPage(pageRef);
        //Update Operation For Sell To Account
        ApexPages.currentPage().getParameters().put(ID, sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(TYPE, ACCOUNTSTR);
        ApexPages.currentPage().getParameters().put(RETURN_TYPE, '/'+sellToAccount.Id);
        ApexPages.currentPage().getParameters().put(OPERATION, UPDATE_OPERATION);
        EP_ChangeRequestController controller = new EP_ChangeRequestController();
        test.stopTest();
        system.assert(controller.allFieldValues.isEmpty());
            }
      }
      
      static testmethod void test_Ep_ChangeRequestService(){
      Set<String>  objects = new Set<String> ();
      Set<string> fieldSet = new Set<string> ();
      Set<String> recordTypeset1 = new Set<String>();
      //EP_ChangeRequestService.fetchObjectFieldActionMapping( objects,fieldSet,recordTypeset1);
 
                                                                                                                                                                                                                                                                         
      
      
      
      
      
      }

}