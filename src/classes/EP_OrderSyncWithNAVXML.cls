/*
    @Author          Accenture
    @Name            EP_OrderSyncWithNAVXML
    @CreateDate      04/18/2017
    @Description     This class  is used to generate outbound XML's of Order to Sync with NAV
    @Version         1.0
    @Reference       NA
*/
public class EP_OrderSyncWithNAVXML extends EP_GenerateOrderRequestXML{
    
    /**
    * @author           Accenture
    * @name             createXML
    * @date             04/18/2017
    * @description      This method is used to create XML for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */
    public override string createXML(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createXML');
        return super.createXML();
    }
    
    /**
    * @author           Accenture
    * @name             createPayload
    * @date             04/18/2017
    * @description      This method is used to create Payload for Order to Sync with NAV
    * @param            NA
    * @return           NA
    */      
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','createPayload');
        
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode OrderNode = tempDoc.createRootElement(EP_OrderConstant.Order,null, null);
        Datetime dt = Datetime.now();
        String versionNr = dt.format('yyyyMMdd\'T\'hhmmss') + '.' + dt.millisecondGmt();
        String onRun =OrderObj.EP_OnOff_Run__c;
        String onOffRun;
        system.debug('onRun is :' +onRun);
        if(onRun!= null && onRun== EP_OrderConstant.ON_STR){
            onOffRun= EP_OrderConstant.TRUE_STR;
        }else if(onRun== EP_OrderConstant.OFF_STR){
            onOffRun= EP_OrderConstant.FALSE_TAG;
        }else{
            onOffRun= EP_Common_Constant.BLANK;
        }
        // Defect -80324 Ex-Rack changes
        String locationId;
        if(OrderObj.EP_Delivery_Type__c == Label.Delivery_Type || OrderObj.EP_Delivery_Type__c == Label.Consumption_Type){
            locationId = getValueforNode(OrderObj.EP_Supply_Location_Number__c);
        }else{
            locationId = getValueforNode(OrderObj.EP_Pickup_Location_ID__c);
      
        }
        
        //Construct Seq ID
        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, OrderObj.Id);
        Dom.XMLNode seqIdNode = OrderNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(seqid)); //Value for seqId
        
        Dom.XMLNode IdentifierNode = OrderNode.addChildElement(EP_OrderConstant.identifier_windmsxml,null,null);
        Dom.XMLNode orderIdNode = IdentifierNode.addChildElement(EP_OrderConstant.orderId,null,null).addTextNode(getValueforNode(OrderObj.OrderNumber__c)); //Value for orderId
        Dom.XMLNode entrprsIdNode = IdentifierNode.addChildElement(EP_OrderConstant.entrprsId,null,null).addTextNode(getValueforNode(OrderObj.Enterprise_Id__c)); //Value for enterpriseId

        OrderNode.addChildElement(EP_OrderConstant.sellToId,null,null).addTextNode(getValueforNode(OrderObj.EP_Sell_To_Id__c)); //Value for sellToId
        OrderNode.addChildElement(EP_OrderConstant.shipToId,null,null).addTextNode(getShipToOfOrder()); //Value for shipToId
        OrderNode.addChildElement(EP_OrderConstant.reqDlvryDt,null,null).addTextNode(getValueforNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Requested_Delivery_Date__c)));
        OrderNode.addChildElement(EP_OrderConstant.expctdDlvryDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Expected_Delivery_Date__c)); // should be Null
        OrderNode.addChildElement(EP_OrderConstant.orderStartDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.Delivery_From_Date__c));// Delivery from date
        OrderNode.addChildElement(EP_OrderConstant.loadingDt,null,null).addTextNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Expected_Loading_Date__c));
        OrderNode.addChildElement(EP_OrderConstant.dlvryDt,null,null).addTextNode(getValueforNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Requested_Delivery_Date__c)));// null - EP_Delivery_Date__c
        OrderNode.addChildElement(EP_OrderConstant.deliveryType,null,null).addTextNode(getValueforNode(OrderObj.EP_Delivery_Type__c)); //Value for deliveryType
        OrderNode.addChildElement(EP_OrderConstant.prodcutSoldAs,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Product_Category__c));
        if(OrderObj.Cancellation_Check_Done__c == true){
            OrderNode.addChildElement(EP_OrderConstant.docStatus,null,null).addTextNode(Label.DocStatus_Cancel);
        } else {
            OrderNode.addChildElement(EP_OrderConstant.docStatus,null,null).addTextNode('');
        }
        OrderNode.addChildElement(EP_OrderConstant.orderType,null,null).addTextNode(getValueforNode(OrderObj.EP_Type2__c));
        
        OrderNode.addChildElement(EP_OrderConstant.totalOrderQty,null,null).addTextNode(getValueforNode(OrderObj.EP_Total_Order_Quantity__c));
        
        OrderNode.addChildElement(EP_OrderConstant.paymentMethod,null,null).addTextNode(getValueforNode(OrderObj.EP_Payment_Method_Code__c));
        String paymentMethod = OrderObj.EP_Payment_Method_Name__c;
        if(OrderObj.EP_Payment_Method_Name__c == Label.Check_on_Delivery || OrderObj.EP_Payment_Method_Name__c == Label.Cash_On_Delivery) {
            paymentMethod = 'COD';
        }
        OrderNode.addChildElement(EP_OrderConstant.paymentType,null,null).addTextNode(getValueforNode(paymentMethod));
        OrderNode.addChildElement(EP_OrderConstant.currencyId,null,null).addTextNode(getValueforNode(OrderObj.Currency_Code__c));
        OrderNode.addChildElement(EP_OrderConstant.pricingDate,null,null).addTextNode(getValueforNode(EP_DateTimeUtility.formatDateAsString(OrderObj.EP_Requested_Delivery_Date__c))); //Value for pricingDate
        OrderNode.addChildElement(EP_OrderConstant.priceValidityPeriod,null,null).addTextNode(getValueforNode(OrderObj.Description__c)); // Null
        OrderNode.addChildElement(EP_OrderConstant.pricingStockHldngLocId,null,null).addTextNode(getValueforNode(OrderObj.EP_Supply_Location_Number__c));
        OrderNode.addChildElement(EP_OrderConstant.logisticStockHldngLocId,null,null).addTextNode(locationId); //blank
        OrderNode.addChildElement(EP_OrderConstant.comment,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Comments__c)); 
        OrderNode.addChildElement(EP_OrderConstant.exceptionNr,null,null).addTextNode(this.getCreditExceptionId(OrderObj.ID)); 
        OrderNode.addChildElement(EP_OrderConstant.pricingTransporterCode,null,null).addTextNode(getValueforNode(OrderObj.EP_Transportation_Code__c)); 
        OrderNode.addChildElement(EP_OrderConstant.logisticTransporterCode,null,null).addTextNode(getValueforNode(OrderObj.EP_Transportation_Code__c)); 
        OrderNode.addChildElement(EP_OrderConstant.orderEpoch,null,null).addTextNode(getValueforNode(OrderObj.EP_Order_Epoch__c));
        OrderNode.addChildElement(EP_OrderConstant.orderOrigination,null,null).addTextNode(EP_OrderConstant.CS_TAG);
        OrderNode.addChildElement(EP_OrderConstant.orderRefNr,null,null).addTextNode(getValueforNode(OrderObj.OrderReferenceNumber__c));
        OrderNode.addChildElement(EP_OrderConstant.customerIntRefNr,null,null).addTextNode(getValueforNode(OrderObj.EP_Customer_Reference_Number__c));
        OrderNode.addChildElement(EP_OrderConstant.onRun,null,null).addTextNode(onOffRun);
        OrderNode.addChildElement(EP_OrderConstant.runId,null,null).addTextNode(getValueforNode(OrderObj.Run_Number__c));
        OrderNode.addChildElement(EP_OrderConstant.versionNr,null,null).addTextNode(getValueforNode(versionNr));
      
        Dom.XMLNode OrderLinesNode = OrderNode.addChildElement(EP_OrderConstant.OrderLines,null,null);
                system.debug('pep ' + orderLineItemsWrapper);
        //Repeat this for every Child node
        
       
        list<Attachment> lineItems =  [SELECT Id, Name,body, ParentId, Parent.Type FROM Attachment where ParentId =: OrderObj.csord__Identification__c];
        
     
        system.debug('pep ' + lineItems);
        for(orderLineItemWrapper orderLineWrp: orderLineItemsWrapper){
                system.debug('pep ' + orderLineWrp);
            Dom.XMLNode OrderLineNode = OrderLinesNode.addChildElement(EP_OrderConstant.OrderLine,null,null);
            
            OrderLineNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineSeqId));
            Dom.XMLNode IdentifierNode2 = OrderLineNode.addChildElement(EP_OrderConstant.identifier_windmsxml,null,null);
            IdentifierNode2.addChildElement(EP_OrderConstant.lineId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Order_Line_Number__c)); //Value for lineId
            
            OrderLineNode.addChildElement(EP_OrderConstant.itemId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Product_Code__c)); //Value for itemId
            OrderLineNode.addChildElement(EP_OrderConstant.qty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Quantity__c)); //Value for qty
            OrderLineNode.addChildElement(EP_OrderConstant.pricingStockHldngLocId,null,null).addTextNode(getValueforNode(OrderObj.EP_Supply_Location_Number__c)); //Value for pricingStockHldngLocId
            OrderLineNode.addChildElement(EP_OrderConstant.logisticStockHldngLocId,null,null).addTextNode(locationId); //Value for logisticStockHldngLocId
            OrderLineNode.addChildElement(EP_OrderConstant.uOm,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Quantity_UOM__c)); //Value for uOm
            OrderLineNode.addChildElement(EP_OrderConstant.unitPrice,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Pricing_Response_Unit_Price__c)); //Value for unitPrice
            OrderLineNode.addChildElement(EP_OrderConstant.loadedAmbientQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Ambient_Loaded_Quantity__c));
            OrderLineNode.addChildElement(EP_OrderConstant.loadedStandardQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Standard_Loaded_Quantity__c));
            OrderLineNode.addChildElement(EP_OrderConstant.deliveredAmbientQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Ambient_Delivered_Quantity__c));
            OrderLineNode.addChildElement(EP_OrderConstant.deliveredStandardQty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Standard_Delivered_Quantity__c));
            
            Dom.XMLNode bolsNode = OrderLineNode.addChildElement(EP_OrderConstant.bols,null,null);
            Dom.XMLNode bolNode = bolsNode.addChildElement(EP_OrderConstant.bol,null,null);
            bolNode.addChildElement(EP_OrderConstant.bolId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_BOL_Number__c));
            bolNode.addChildElement(EP_OrderConstant.qty,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.Quantity__c)); //Value for qty
            bolNode.addChildElement(EP_OrderConstant.contractId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Contract_Nav_Id__c)); //Value for qty
            bolNode.addChildElement(EP_OrderConstant.supplierId,null,null).addTextNode(getValueforNode(orderLineWrp.orderLineItem.EP_Supplier_Nav_Vendor_Id__c));
            // end of bol node
            
            Dom.XMLNode lineComponentsNode = OrderLineNode.addChildElement(EP_OrderConstant.lineComponents,null,null);
            Dom.XMLNode invoiceComponentsNode = lineComponentsNode.addChildElement(EP_OrderConstant.invoiceComponents,null,null);
            Dom.XMLNode acctComponentsNode = lineComponentsNode.addChildElement(EP_OrderConstant.acctComponents,null,null);
            //if(lineItems.get(0).pricingResponseJson__c != null){
            
            if(lineItems != null && lineItems.size() >0 && lineItems.get(0).body.toString() !=null ){
            
           System.debug('Attachement  Body:'+ lineItems[0].body.toString());
            EP_PricingResponseStub stub = (EP_PricingResponseStub) System.JSON.deserialize(lineItems.get(0).body.toString(), EP_PricingResponseStub.class);
            
           for (Integer i = 0;  i < stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent.size(); i++) {
                Dom.XMLNode invoiceComponentNode = invoiceComponentsNode.addChildElement(EP_OrderConstant.invoiceComponent,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].seqId));
                invoiceComponentNode.addChildElement(EP_OrderConstant.TYPE,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].type));
                invoiceComponentNode.addChildElement(EP_OrderConstant.name,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].name));
                invoiceComponentNode.addChildElement(EP_OrderConstant.amount,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].amount));
                invoiceComponentNode.addChildElement(EP_OrderConstant.amountLCY,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].amountLCY)); //Value for amount
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxPercentage,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].taxPercentage)); //Value for taxPercentage
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].taxAmount));
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].taxAmountLCY));
                invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].totalAmount));
                invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.invoiceDetails.invoiceComponent[i].totalAmountLCY));
            }
            
            //End of Invoice Components node
           
            for (Integer i = 0;  i < stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent.size(); i++) {
                Dom.XMLNode acctComponentNode = acctComponentsNode.addChildElement(EP_OrderConstant.acctComponent,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.seqId,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].seqId));
                acctComponentNode.addChildElement(EP_OrderConstant.componentCode,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].componentCode));
                acctComponentNode.addChildElement(EP_OrderConstant.glAccountNr,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].glAccountNr));
                acctComponentNode.addChildElement(EP_OrderConstant.baseAmount,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].baseAmount));
                acctComponentNode.addChildElement(EP_OrderConstant.baseAmountLCY,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].baseAmountLCY));
                acctComponentNode.addChildElement(EP_OrderConstant.taxRate,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].taxRate));
                acctComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].taxAmount));
                acctComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].taxAmountLCY));
                acctComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].totalAmount));
                acctComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].totalAmountLCY));
                acctComponentNode.addChildElement(EP_OrderConstant.isVAT,null,null).addTextNode(getValueforNode(stub.Payload.Any0.PricingResponse.PriceDetails.PriceLineItems.lineItem[0].lineItemInfo.acctComponents.acctComponent[i].isVAT));
            }
            // fix for defect -80505
            }else{ 
            
                Dom.XMLNode invoiceComponentNode = invoiceComponentsNode.addChildElement(EP_OrderConstant.invoiceComponent,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.seqId,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.TYPE,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.name,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.amount,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.amountLCY,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxPercentage,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null);
                invoiceComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null);
                
                Dom.XMLNode acctComponentNode = acctComponentsNode.addChildElement(EP_OrderConstant.acctComponent,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.seqId,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.componentCode,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.glAccountNr,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.baseAmount,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.baseAmountLCY,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.taxRate,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.taxAmount,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.taxAmountLCY,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.totalAmount,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.totalAmountLCY,null,null);
                acctComponentNode.addChildElement(EP_OrderConstant.isVAT,null,null);
            
            
            
            
            }

            
            //end of orderline node
        }
        //end of Order node
        OrderNode.addChildElement(EP_OrderConstant.clientId,null,null).addTextNode(getValueforNode(OrderObj.EP_Puma_Company_Code__c));        
        Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_OrderConstant.Payload,null,null);
          Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_OrderConstant.any0,null, null);
        // Encoding payload by calling encode XML method in superclass
        system.debug('pep xml' + tempDoc.toXmlString());
        AnyNode.addTextNode(encodeXML(tempDoc));
    }
    
 /**
    * @author           CloudSense
    * @name             getCreditExceptionId
    * @date             02/08/2018
    * @description      This method is used to get the credit exception id
    * @param            NA
    * @return           NA
    */  
    @TestVisible
    private String getCreditExceptionId(Id OrderId){
        EP_GeneralUtility.Log('Public','EP_OrderCreditCheckXML','getCreditExceptionId');
        //Defect Start #57275 -  NAV is Accepting only "PrePayment" text for PaymentType xml tag. Now set blank for other EP_Payment_Term__c values
        String CRNumber = EP_Common_Constant.BLANK;
        List<EP_Credit_Exception_Request__c> crList =[SELECT
                                                                ID,EP_CS_Order__c,EP_Status__c,Name
                                                      FROM
                                                              EP_Credit_Exception_Request__c
                                                      WHERE EP_CS_Order__c=:OrderId and EP_Status__c= :EP_Common_Constant.CRLITEM_STATUS_APPROVED
                                                      ];
        
        if(crList != null && crList.size() > 0){
           CRNumber = crList.get(0).Name;
        }
        return CRNumber;
        }
  
}