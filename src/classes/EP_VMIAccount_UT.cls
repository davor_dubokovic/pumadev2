@isTest
public class EP_VMIAccount_UT{

    static testMethod void updateVMIShipToUTCTimeZoneOffset_test() {
        EP_VMIAccount localObj = new EP_VMIAccount();
        EP_AccountMApper accountMapper = new EP_AccountMApper();
        Account account = EP_TestDataUtility.getVMIShipTo();
        Test.startTest();  
        localObj.updateVMIShipToUTCTimeZoneOffset(account);
        Test.stopTest();
        //System.assertEquals(3.0, accountMapper.getAccountRecord(account.id).EP_Ship_To_UTC_Offset__c);
        System.assertEquals(5.5,account.EP_Ship_To_UTC_Offset__c);
    }
    
    static testMethod void insertPlaceHolderTankDips_test() {
        EP_VMIAccount localObj = new EP_VMIAccount();
        List<Account> laccount = new List<Account>();
        Account shipTo = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        Test.startTest();
        localObj.insertPlaceHolderTankDips(shipTo);
        Test.stopTest();
        EP_TankMapper tankMapper = new EP_TankMapper();
        Set<Id> tankIds = new Set<Id>();
        for(EP_Tank__c tank : [Select Id from EP_Tank__c where EP_Ship_To__c = :shipTo.id] ){
            tankIds.add(tank.Id);
        }

        List<EP_Tank_Dip__c> td = [select id, EP_Tank__c, EP_LastModified_User_Offset__c, EP_Tank_Dip_Entered_Today__c,RecordType.Name From EP_Tank_Dip__c];
        EP_TankDipMapper tankDipMapper = new EP_TankDipMapper();
        List<EP_Tank_Dip__c> returnedList = tankDipMapper.getPlaceHolderTankDipRecords(tankIds,EP_TestDataUtility.RECORDTYPEID_PLACEHOLDER);     
        system.debug('----returnedList----'+returnedList);
        System.assertEquals(false,returnedList.isEmpty());
    }
    
    static testMethod void deletePlaceHolderTankDips_test() {
        EP_VMIAccount localObj = new EP_VMIAccount();
        Account acct = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        Test.startTest();
        EP_TankMapper mapper = new EP_TankMapper();
        List<EP_Tank__c> tanks  = mapper.getRecordsByShipToIds(new Set<Id>{acct.Id});
        MAp<Id, EP_Tank_Dip__c> tankDipMap = EP_TestDataUtility.createTestTankDipsRecords(tanks, system.now(), true);
        //acct.EP_Status__c = EP_Common_Constant.INACTIVE_SHIP_TO_STATUS;
        acct.EP_Ship_To_Opening_Days__c = null;
        Database.Update(acct);
        EP_AccountMapper accountMapper = new  EP_AccountMapper();
        Account shipTo = accountMapper.getAccountRecord(acct.Id);
        localObj.deletePlaceHolderTankDips(shipTo);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        //System.AssertEquals(true,<asset conditions>);
        // **** TILL HERE ~@~ *****
        set<Id> tankIds = new set<Id>();
        for(EP_Tank__c tank : [Select Id from EP_Tank__c where EP_Ship_To__c = :acct.id] ){
            tankIds.add(tank.Id);
        }

        List<EP_Tank_Dip__c> td = [select id, EP_Tank__c, EP_LastModified_User_Offset__c, EP_Tank_Dip_Entered_Today__c,RecordType.Name From EP_Tank_Dip__c];
       
        EP_TankDipMapper tankDipMapper = new EP_TankDipMapper();
        List<EP_Tank_Dip__c> returnedList = tankDipMapper.getPlaceHolderTankDipRecords(tankIds,EP_TestDataUtility.RECORDTYPEID_PLACEHOLDER);     
        System.assertEquals(true,returnedList.size()==0);

    }
}