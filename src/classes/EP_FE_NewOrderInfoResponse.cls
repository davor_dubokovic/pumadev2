/*********************************************************************************************
     @Author <>
     @name <EP_FE_NewOrderInfoResponse >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
global with sharing class EP_FE_NewOrderInfoResponse extends EP_FE_Response {

    // Error list
    global static final Integer ERROR_MISSING_SELLTO = -1;
    global static final Integer ERROR_PARSING_SELLTO = -2;
    global static final Integer ERROR_SELLTOID_NOT_FOUND = -3;
    global static final Integer ERROR_SELLTO_NOT_FOUND = -4;
    global static final Integer ERROR_MISSING_SHIPTO = -5;
    global static final Integer ERROR_MISSING_TERMINAL = -6;
    global static final Integer ERROR_ORDER_NOT_PRESENT = -7;
    global static final Integer ERROR_REORDER_SELLTOID_DO_NOT_MATCH = -8;
    global static final Integer ERROR_REORDER_SHIPTOIDS_DO_NOT_MATCH = -9;
    global static final Integer ERROR_REORDER_PRODUCT_NOT_AVAILABLE_IN_SHIPTOIDS = -10;
    global static final Integer ERROR_REORDER_TERMINALIDS_DO_NOT_MATCH = -11;
    global static final Integer ERROR_REORDER_PRODUCT_NOT_AVAILABLE_IN_TERMINALIDS = -12;
    global static final Integer ERROR_REORDER_DELIVERY_TYPE_DO_NOT_MATCH = -13;
    global static final Integer ERROR_PRODUCTS_CAN_NOT_BE_REORDERD = -14;
    global static final Integer ERROR_SELLTO_ORDER_NOT_FOUND = -15;

    global static final String CLASSNAME = 'EP_FE_NewOrderInfoEndpoint';
    global static final String METHOD1 = 'getOrderInfo';
    global static final String METHOD2 = 'populateDeliveryTypeResponse';
    global static final String METHOD3 = 'populateExRackTypeResponse';
    global static final String METHOD4 = 'validateReOrderParameters';
    global final static String SEVERITY = 'ERROR';
    global final static String TRANSACTION_ID = 'PE-188';
    global final static String DESCRIPTION1 = 'Missing SellTo';
    global final static String DESCRIPTION2 = 'SellToId not found';
    global final static String DESCRIPTION3 = 'SellTo not found';
    global final static String DESCRIPTION4 = 'Missing ShipTo';
    global final static String DESCRIPTION5 = 'Missing Terminal';
    global final static String DESCRIPTION6 = 'Order not present';
    global final static String DESCRIPTION7 = 'Reorder SellToIds do not match';
    global final static String DESCRIPTION8 = 'Reorder ShipTos do not match';
    global final static String DESCRIPTION9 = 'Reorder products not available in shipTos';
    global final static String DESCRIPTION10 = 'Reorder Terminals do not match';
    global final static String DESCRIPTION11 = 'Reorder products not available in terminals';
    global final static String DESCRIPTION12 = 'Reorder DeliveryType do not match';
    global final static String DESCRIPTION13 = 'Products can not be reordered';
    global final static String DESCRIPTION14 = 'SellTo or OrderId not found';

    // Delivery types
    global static final String DELIVERY_TYPE_DELIVERY = 'Delivery';
    global static final String DELIVERY_TYPE_EX_RACK = 'Ex-Rack';
    
    public String deliveryType; //Either Ex-Rack or Delivery //Taken from Sell To
    public String currencyCode; //Taken from Bill_To / Sell_to
    public Decimal maximalOrderAmount;
    
    public List<EP_FE_ProductInfo> productInfoList;
    public String previousCustomerReferenceNumber;
    public String previousComments; //Available in re-order only
    
    //FIELD VISIBILITY FIELDS
    public Boolean isTruckPlateVisible {get; set;}
    public Boolean isTruckPlateMandatory {get; set;}
    public Boolean isCustomerReferenceVisible {get; set;}
    public Boolean isCustomerReferenceMandatory {get; set;}
    public Boolean isCommentsFieldVisible {get; set;}
    
    //DATA AVAILABLE ONLY FOR DELIVERY TYPE: EX-RACK 
    public List<EP_FE_TerminalInfo> terminalInfoList; //Not set for delivery type
    public Id selectedTerminalId;        // Available in re-order only
    public String previousTruckPlate;   // Available in re-order only
    
    
    //DATA AVAILABLE ONLY FOR DELIVERY TYPE: DELIVERY
    public List<EP_FE_ShipToInfo> shipToList {get; set;} //Recognize list of shipTo accounts for making a new order
    public Id selectedShipToId {get; set;}    // Available in re-order only
    
    //DATA WILL BE AVAILABLE WHEN ORDER IS EDITING
    public Date selectedOrderDate {get; set;}
    public String selectedOrderTime {get; set;}
    public Boolean canSeePrices {get; set;}
    public Id orderId {get; set;}
    public EP_FE_OrderProductData priceData{get; set;}
    
    // True if user is editing a draft order, false otherwise.
    public Boolean isDraftOrder{get; set;}
    
    //Defined helper classes:
/*********************************************************************************************
     @Author <>
     @name <EP_FE_ProductInfo >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
    global with sharing class EP_FE_ProductInfo {
        public Id productId {get; set;} //Taken from Product
        public String productName {get; set;} //Taken from Product
        public Decimal minimalProductAmount {get; set;} //Taken from Order Configuration object (Record Type:)
        public Decimal maximalProductAmount {get; set;}//Taken from Order Configuration object (Record Type:)
        public List < EP_FE_OrderItem > previousOrders {get; set;}    // Available in re-order only
        public String unitOfMeasurement {get; set;}//Should be taken from Product object
/*********************************************************************************************
     @Author <>
     @name <EP_FE_ProductInfo >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/       
        public EP_FE_ProductInfo (Id productId, String productName, String unitOfMeasurement, Decimal minimalProductAmount, Decimal maximalProductAmount , 
            List < EP_FE_OrderItem > previousOrders ) {
            this.productId = productId;
            this.productName= productName;
            this.unitOfMeasurement= unitOfMeasurement;
            this.minimalProductAmount  = minimalProductAmount;
            this.maximalProductAmount= maximalProductAmount;
            this.previousOrders = previousOrders;
        }       
    }
    
/*********************************************************************************************
     @Author <>
     @name <EP_FE_ProductInfo >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
    global with sharing class EP_FE_OrderItem {
        public Decimal amount {get; set;}
        public Id tankId {get; set;}
        public DateTime lastModifiedDate {get; set;}
        
    /*********************************************************************************************
     @Author <>
     @name <EP_FE_OrderItem >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/        
        public EP_FE_OrderItem (Decimal amount, Id tankId, DateTime lastModifiedDate) {
            this.amount = amount;
            this.tankId = tankId;
            this.lastModifiedDate = lastModifiedDate;
        }
    }
    
    
/*********************************************************************************************
     @Author <>
     @name <EP_FE_NewOrderInfoResponse >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/    
    global with sharing class EP_FE_ShipToInfo {
        public Id shipToId;
        public String shipToName; //Name of the ship To Company
        public EP_FE_Address shipToAddress; //Name of the sell To Company
        public Boolean isAvailable; //Check if this shipTo is available and could be used
        public List<Id> availableProductIds; //List of available products for shipTo account    
        public Date requestPeriodStartDate {get; set;} //Value to recognize what is the start date of period
        public Date requestPeriodEndDate {get; set;}   //Value to recognize what is the end date of period
/*********************************************************************************************
     @Author <>
     @name <EP_FE_ShipToInfo>
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/       
        public EP_FE_ShipToInfo(Id shipToId, String shipToName, Address shipToAddress, Boolean isAvailable, Date requestPeriodStartDate, Date requestPeriodEndDate) {
            this.shipToId = shipToId;
            this.shipToName = shipToName;
            this.shipToAddress = new EP_FE_Address(shipToAddress);
            this.isAvailable = isAvailable;
            this.availableProductIds = new List<Id>();
            this.requestPeriodStartDate = requestPeriodStartDate;
            this.requestPeriodEndDate = requestPeriodEndDate;
        }       
    }
/*********************************************************************************************
     @Author <>
     @name <EP_FE_TerminalInfo >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/    
    global with sharing class EP_FE_TerminalInfo {
        public Id terminalId {get; set;}
        public String terminalName {get; set;}  
        public List<EP_FE_DateOpeningTime> availableDateTimeList {get; set;}    
        public List<Id> availableProductIds;     //List of producst which could be taken 
        public Boolean isSlottingEnabled;
                
        
/*********************************************************************************************
     @Author <>
     @name <EP_FE_TerminalInfo>
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/  
        public EP_FE_TerminalInfo() {
            availableDateTimeList = new List<EP_FE_DateOpeningTime>();
            availableProductIds = new List<Id>();
            isSlottingEnabled = true;
        }
/*********************************************************************************************
     @Author <>
     @name <EP_FE_TerminalInfo>
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/        
        public EP_FE_TerminalInfo(String storageLocationName, Id storageId) {
            this();
            terminalName = storageLocationName;
            terminalId = storageId;
        }
    }
/*********************************************************************************************
     @Author <>
     @name <EP_FE_TimePeriod >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/    
    global with sharing class EP_FE_TimePeriod {
        public Time startTime {get; set;} //Open time and close time for terminal for specific date
        public Time endTime {get; set;} //Open time and close time for terminal for specific date
        
/*********************************************************************************************
     @Author <>
     @name <EP_FE_TimePeriod >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/          
        public EP_FE_TimePeriod(Time dOpeningTime, Time tClosingTime){
            startTime = dOpeningTime;
            endTime = tClosingTime;         
        }
    }
 /*********************************************************************************************
     @Author <>
     @name <EP_FE_DateOpeningTime >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/   
    global with sharing class EP_FE_DateOpeningTime {
        public String workingDate {get; set;} //Available date for pick up
        public List<EP_FE_TimePeriod> timePeriods {get; set;}
/*********************************************************************************************
     @Author <>
     @name <EP_FE_NewOrderInfoResponse >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/       
        public EP_FE_DateOpeningTime(Date dDate) {
            workingDate = String.valueOf(dDate);
            timePeriods = new List<EP_FE_TimePeriod>();
        }
    }
/*********************************************************************************************
     @Author <>
     @name <EP_FE_Address >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
    global with sharing class EP_FE_Address {
        public String street {get; set;}
        public String city {get; set;}
        public String state {get; set;}
        public String country {get; set;}
        public String postalCode {get; set;}
/*********************************************************************************************
     @Author <>
     @name <EP_FE_Address>
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
        public EP_FE_Address(String street, String city, String state, String country, String postalCode) {
            this.street = street;
            this.city = city;
            this.state = state;
            this.country = country;
            this.postalCode = postalCode;
        }
/*********************************************************************************************
     @Author <>
     @name <EP_FE_Address>
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
        public EP_FE_Address(Address address) {
            this(address.Street, address.City, address.State, address.Country, address.PostalCode);
        }
    }
    
/*********************************************************************************************
     @Author <>
     @name <EP_FE_NewOrderInfoResponse >
     @CreateDate <>
     @Description <>  
     @Version <1.0>
    *********************************************************************************************/
    public EP_FE_NewOrderInfoResponse(){
        productInfoList = new List<EP_FE_ProductInfo>();
        selectedTerminalId = null;
        selectedShipToId = null; 
        previousTruckPlate = null;
        canSeePrices = true;
    }
}