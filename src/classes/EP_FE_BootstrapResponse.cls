/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_BootstrapResponse >
     @CreateDate <12/04/2016>
     @Description <This class to construct response for bootstrap Enpoint  >  
     @Version <1.0>
    *********************************************************************************************/
global with sharing class EP_FE_BootstrapResponse extends EP_FE_Response {

    global static final Integer ERROR_FETCHING_SELLTO = -1; //Error in fetching sell tos
    global static final Integer ERROR_FETCHING_BILLTO = -2; //Error in fetching the bill tos
    global static final Integer ERROR_FETCHING_SHIPTO = -3; //Error in fetching the ship tos
    global static final Integer ERROR_FETCHING_TANKS  = -4;// Error in fetching  Tanks 
    global static final Integer ERROR_FETCHING_TERMINALS = -5;// Error in fetching  Tanks 
    global static final Integer ERROR_FETCHING_DICTIONARY =-6;
    global static final Integer CONFIGURATION_NOT_SET =-7;
    global static final Integer NOSELLTOS_NOT_SET = -8;
    global static final Integer SELLTO_NOT_SET = -9;
    global static final Integer PERMISSIONS_NOT_SET = -10;
    global static final Integer ERROR_FETCHING_Routes =-11;
    
    //Error Logging Methode Parameters
    global static final String  CLASS_NAME = 'EP_FE_BootstrapEndpoint'; 
    global static final String  METHOD_GETSELLTORESPONSE = 'getSelltoResponse';
    global static final String  METHOD_GETDICTIONARY = 'getDictionary';
    global static final String  METHOD_GETCREDITINFO = 'getCreditInfo';
    global static final String  METHOD_ACCOUNTOVERALL = 'setAccountOverallStatus';
    global static final String  METHOD_OVERDUEINVOICES = 'getOverdueInvoices';
    //global static final String  METHOD_='getCreditInfo';
    global static final String  ERROR ='Error';
    global static final String  AREA = 'BootstrapAPI';
    global static final String  FUNCTION = 'SOQL Query to fetch the list';
    global static final String  FUNCTION_DICTIONARY = 'Getting Dictonay Item data';
    global static final String  ISSUE = 'Raised exception';
    global static final String  TRANSACTION_ID = 'PE-369';
    global static final String  DESCRIPTION ='The configuration has not been set properly';
    global static final String  CONVERT_TO_NUMBER  = 'Convert oneMonthAgo in a number:';
    global static final String  QUERY_FETCHING_INFO =  'SOQL Query to fetch the list for the Account Overall info';
    
    /*
     *  Default constructor
     */
    public EP_FE_BootstrapResponse() {
        sellToList = new List<EP_FE_SellTo>();
    }
    
   /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_ShipTo >
        @CreateDate <12/04/2016>
        @Description <This methode is to get stucture for sellto response  >  
        @Version <1.0>
    *********************************************************************************************/
    global with sharing class EP_FE_ShipTo {
        public Account record;
        public List<EP_Tank__c> tanks {get; set;} // List of tanks
        public List<Date> missingTankDipDates {get; set;}
        public List<Product2> availableProducts {get; set;}
        public List<EP_FE_RouteAllocation> routeAllocation {get;set;}
/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_ShipTo>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/
        public EP_FE_ShipTo(Account accountRecord) {
            record = accountRecord;
            tanks = new List<EP_Tank__c>();
            routeAllocation=new List<EP_FE_RouteAllocation>();
            missingTankDipDates = new List<Date>();
            availableProducts = new List<Product2>();
            system.debug('>>>> AVAILABLE PRODUCTS :' + availableProducts);
        
        }

    }
       /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_HighLevelBankAccount >
        @CreateDate <12/04/2016>
        @Description <This is inner class for masked high level bank accounts of sell to>  
        @Version <1.0>
    *********************************************************************************************/
    global with sharing class EP_FE_HighLevelBankAccount { 
        public String id {get; set;}
        public String name {get; set;}
        public String type {get; set;}
        public String maskedAccountNumber {get; set;} //Masked bank account number 
        public String status {get; set;}
    }
     /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_MyDataUpdateAlert >
        @CreateDate <12/04/2016>
        @Description <This is inner class for masked high level bank accounts of sell to>  
        @Version <1.0>
    *********************************************************************************************/  
    global with sharing class EP_FE_MyDataUpdateAlert {
        String changeRequestId {get; set;}
        String message {get; set;}
        Boolean success {get; set;}
    }
     /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_SellTo >
        @CreateDate <12/04/2016>
        @Description <This class get stucture for EP_FE_SellTo response>  
        @Version <1.0>
    *********************************************************************************************/    
    global with sharing class EP_FE_SellTo {
        public Account record;
        public List<EP_FE_ShipTo> shipToList {get; set;}
        public List<EP_FE_Terminal> terminalList {get; set;} //List of terminals which are available for this bill to account
        public Account billTo {get; set;} //All account data is taken from EP_Bill_To_Account__r
        public List<EP_FE_HighLevelBankAccount> highLevelBankAccounts {get; set;} // List of high level bank accounts without detail
        public Boolean canCreateDeliveryOrder {get; set;}
        public Boolean canCreateExRackOrder {get; set;}
        public Boolean isPrimary{get;set;}
        //List<EP_FE_MyDataUpdateAlert> myDataUpdateAlerts {get; set;} // List of my data update request alerts
  
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/      
        public EP_FE_SellTo(Account accountRecord) {
            record = accountRecord;
            billTo = accountRecord.EP_Bill_To_Account__r;
            accountRecord=null;
            shipToList = new List<EP_FE_ShipTo>();
            terminalList = new List<EP_FE_Terminal>();
            highLevelBankAccounts = new List<EP_FE_HighLevelBankAccount>();
            //myDataUpdateAlerts = new List<EP_FE_MyDataUpdateAlert>();
            canCreateDeliveryOrder = true;
            canCreateExRackOrder = true;
        }
    }
     /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_Terminal >
        @CreateDate <12/04/2016>
        @Description <This class get stucture for EP_FE_Terminal response>  
        @Version <1.0>
    *********************************************************************************************/     
    global with sharing class EP_FE_Terminal {
        public Id terminalId;
        public String terminalName;
        public Id storeageHoldingLocationId;
  
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/      
        public EP_FE_Terminal(String accountName,Id storageid, Id shlId) {
            terminalName= accountName;
            terminalId=storageid ;
            storeageHoldingLocationId = shlId;
        }
    }
     /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_Dashboard >
        @CreateDate <12/04/2016>
        @Description <This class get stucture for EP_FE_Dashboard response>  
        @Version <1.0>
    *********************************************************************************************/      
    global with sharing class EP_FE_Dashboard {
        public Integer alertOrderAmount {get; set;}
        public Integer activeOrderAmount {get; set;}
        public Integer pastOrderAmount {get; set;}
        public Integer blanketOrderAmount {get; set;}
        // List of past due invoices with details (pdf link & related orders)
        public EP_WrapperAccountStatement pastDueInvoicesStatement {get; set;}
        public Boolean pastDueInvoiceFlag {get; set;}
        public Double balanceOverdue {get; set;}
  
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/      
        public EP_FE_Dashboard() {
            alertOrderAmount = 0;
            activeOrderAmount = 0;
            pastOrderAmount = 0;
            blanketOrderAmount = 0;
        }
        
    }
 /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_Permission >
        @CreateDate <12/04/2016>
        @Description <This class get stucture for EP_FE_Permission response>  
        @Version <1.0>
    *********************************************************************************************/  
    global with sharing class EP_FE_Permission {
        public Boolean canViewDips {get; set;}
        public Boolean canSubmitDips {get; set;}
        public Boolean canViewOrders {get; set;}
        public Boolean canSubmitCancelOrders {get; set;}
        public Boolean canViewOrderPricesOnOrder {get; set;}
        public Boolean canViewInvoices {get; set;}
        public Boolean canViewRemainingCredit {get; set;}
        public Boolean canViewPaymentBalance {get; set;}
        public Boolean canMakePayment {get; set;}
        public Boolean canViewCases {get; set;}
        public Boolean canSubmitCases {get; set;}
        public Boolean canSubmitMasterDataChangeRequest {get; set;}
        public Boolean canViewBlanketOrdersBalance {get; set;}
        public Boolean canViewSellTo {get; set;}
        public Boolean canUploadProofOfPayment {get; set;}
    }
 /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_Credit_Information >
        @CreateDate <12/04/2016>
        @Description <This class get stucture for Credit nformation response>  
        @Version <1.0>
    *********************************************************************************************/  
    global with sharing class EP_FE_Credit_Information {
        public Id sellToId {get; set;}
        public Decimal totalCredit {get; set;}
        public Decimal availableCredit {get; set;}
        public String currencyCode {get; set;}

    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_Credit_Information(Id sellToId, Decimal availableCredit, Decimal totalCredit, String currencyCode) {
            this.sellToId = sellToId;
            this.availableCredit = availableCredit;
            this.totalCredit = totalCredit;
            this.currencyCode = currencyCode;
        }
     }   
 /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_Fieldset_Information >
        @CreateDate <12/04/2016>
        @Description <This class get stucture for Fieldset_Information response>  
        @Version <1.0>
    *********************************************************************************************/    
    global with sharing class EP_FE_Fieldset_Information {
        public String fieldsetValue;
        public String fieldsetName;
        public List<EP_FE_FieldsetItems> fields;
        public sObject defaultValues;

    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_Fieldset_Information(String name , String value , List<EP_FE_FieldsetItems> fields) {
            this.fieldsetValue = value;
            this.fieldsetName = name; 
            this.fields= fields;      
        }
    
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/    
        public EP_FE_Fieldset_Information(String name , String value , List<EP_FE_FieldsetItems> fields, sObject defaultValues) {
            this(name, value, fields);
            this.defaultValues = defaultValues;    
        }
    }
 /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_FieldsetItems >
        @CreateDate <12/04/2016>
        @Description <This class get stucture for EP_FE_FieldsetItems response>  
        @Version <1.0>
    *********************************************************************************************/   
    global with sharing class EP_FE_FieldsetItems { 
        public Integer position;
        public String fieldAPIName;
        public String fieldLabel;
        public String fieldType;
        public Boolean mandatory;
        public Integer fieldLength;
        public Integer fieldSize ;
        public String relatedObjectApiName;
        public String relatedNameFieldApiName;
        public List<String> options;

    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_FieldSetItems(String fieldAPIName, String fieldLabel, String fieldType, 
                Integer position, Boolean mandatory,Integer fieldLength,Integer fieldSize , String relatedObjectApiName,String relatedNameFieldApiName , List<String> options) {
            this.position = position ;
            this.fieldAPIName = fieldAPIName;
            this.fieldLabel = fieldLabel;
            this.fieldType = fieldType;
            this.mandatory  = mandatory;
            this.fieldLength = fieldLength;
            this.fieldSize = fieldSize ;
            this.relatedObjectApiName = relatedObjectApiName;
            this.relatedNameFieldApiName = relatedNameFieldApiName;
            this.options = options;
        }
    }
   
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
    global with sharing class EP_FE_User { 
        public String FirstName {get; set;}
        public String LastName {get; set;}
        public String UserName {get; set;}
        public String Id {get; set;} 
        public String Title {get; set;}
        public String SmallPhotoUrl {get; set;}    
        public String FullPhotoUrl {get; set;}
        public String Email {get; set;}
        public String LanguageLocaleKey {get; set;}
        public String LocaleSidKey {get; set;}
        public String ContactId {get; set;} //lookup contact value
        public String AccountId {get; set;}
        public String AccountCountry {get; set;}
        public String ContactPhone {get; set;} //to be fetched from the contact object
        public String ContactMobilePhone {get; set;} //to be fetched from the contact object
        public DateTime LatestAcceptedTCTime {get; set;}
        public Decimal LatestAcceptedTCVersion {get; set;}
        public Boolean ShouldConfirmTermsAndConditions {get; set;}
        public String TermsAndConditionsText {get; set;}
        public Decimal VersionOfTermsAndCondtionsToConfirm {get; set;}
        public Date ContactBirthDate {get; set;} //to be fetched from the contact object
        public String AccountNumber {get; set;}
        public Double Offset {get;set;}
        public boolean SendChangeRequestNotification{get;set;}  //Prayank
        //public Map<String,boolean> NotificationValue = new Map<String, boolean>{'Send_Change_Request_Notification__c' => False};
        //public Map<String,boolean> NotificationValue  = new Map<String,boolean>();
        
        
    }
    
    /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_FetchRecordTypeList>
        @CreateDate <16/05/2016>
        @Description <This methode is to return all the record type list>  
        @Version <1.0>
    *********************************************************************************************/
    global with sharing class EP_FE_FetchRecordTypeList {
        public List<EP_FE_RecordTypeData> Account;
        public List<EP_FE_RecordTypeData> CaseRT;

    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_FetchRecordTypeList(List<EP_FE_RecordTypeData> recordData) {
            this.Account = recordData; 
        }
        
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_FetchRecordTypeList(List<EP_FE_RecordTypeData> accountData, List<EP_FE_RecordTypeData> caseData) {
            this.Account = accountData;
            this.CaseRT = caseData; 
        }
    }

    /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_RecordTypeData>
        @CreateDate <16/05/2016>
        @Description <This methode is to return all the record type Data>  
        @Version <1.0>        
    *********************************************************************************************/
    global with sharing class EP_FE_RecordTypeData {
        public String recordTypeName;
        public Id recordTypeId; 

    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_RecordTypeData(String recordTypeName, Id recordTypeId) {
            this.recordTypeName = recordTypeName;
            this.recordTypeId = recordTypeId;
        }
    }

    /*
    *   Missing Dips
    */
    global with sharing class EP_FE_DipSites {
        public Id shipToId;
        public String shipToName;
        public Integer missingTankDipDays;
        public List<EP_FE_DipDate> tankDates;
    
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/    
        global EP_FE_DipSites() {
            missingTankDipDays = 0;
        }
    }
    /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_DipDate >
        @CreateDate <16/05/2016>
        @Description <>  
        @Version <1.0>        
    *********************************************************************************************/
    global with sharing class EP_FE_DipDate implements Comparable {
        public String dipDate;
        public Datetime convertedDate;
        public Boolean isMissingDips;
        public Map<String, EP_FE_TankDipInfo> tankDipInfoMap; //Map TankId, TankDipInfo

    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        global EP_FE_DipDate() {
            isMissingDips = false;
            this.tankDipInfoMap = new Map<String, EP_FE_TankDipInfo>();
        }
    /*********************************************************************************************
        @Author <Soumya Raj>
        @name <compareTo>
        @CreateDate <16/05/2016>
        @Description <>  
        @Version <1.0>        
    *********************************************************************************************/
        global Integer compareTo(Object compareTo) {
            EP_FE_DipDate compareToDipDate = (EP_FE_DipDate) compareTo;
            
            if(this.dipDate > compareToDipDate.dipDate){ 
                return -1;
            }else if(this.dipDate < compareToDipDate.dipDate) {
                return 1;
            }else { 
                return 0;
            }
        }
    }
   /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_TankDipInfo >
        @CreateDate <16/05/2016>
        @Description <Inner class to get  Tank Dip info >  
        @Version <1.0>        
    *********************************************************************************************/
    global with sharing class EP_FE_TankDipInfo {
        public Id tankId;
        public Boolean isMissingDip;
    }

    /*
    *   Overall status of an account
    */
    global with sharing class EP_FE_OverallStatus {
        public Integer activeOrders {get; set;}
        public Integer invoicedOrders {get; set;}
        public List<EP_FE_AmountOfFuel> fuelBought {get; set;}
        public List<EP_FE_AmountOfFuel> lastOrder {get; set;}
        public EP_FE_AmountOfPayment activeOrdersAmount {get; set;}
        public EP_FE_AmountOfPayment totalInvoicedOrderValue {get; set;}
        public EP_FE_AmountOfPayment lastPayment {get; set;}
        public Date lastPaymentTime {get; set;}
     /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_OverallStatus() {
            activeOrders = 0;
            invoicedOrders = 0;
            fuelBought = new List<EP_FE_AmountOfFuel>();
            lastOrder = new List<EP_FE_AmountOfFuel>();
        }
    }
    
       /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_AmountOfFuel >
        @CreateDate <16/05/2016>
        @Description <>  
        @Version <1.0>        
    *********************************************************************************************/
    global with sharing class EP_FE_AmountOfFuel {
        public Double amount {get; set;}
        public String unitOfMeasure {get; set;}
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/
        public EP_FE_AmountOfFuel(Double amount, String unitOfMeasure) {
            this.amount = amount;
            this.unitOfMeasure = unitOfMeasure;
        }
    }  
     /*********************************************************************************************
        @Author <Soumya Raj>
        @name <EP_FE_AmountOfPayment >
        @CreateDate <16/05/2016>
        @Description <>  
        @Version <1.0>        
    *********************************************************************************************/ 
    global with sharing class EP_FE_AmountOfPayment {
        public Double amount {get; set;}
        public String currencyOfPayment {get; set;}
    /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/     
        public EP_FE_AmountOfPayment() {
            this.amount = 0;
            this.currencyOfPayment = '';
        }
     /*********************************************************************************************
         Default constructor       
    *********************************************************************************************/  
        public EP_FE_AmountOfPayment(Double amount, String currencyOfPayment) {
            this.amount = amount;
            this.currencyOfPayment = currencyOfPayment;
        }
    }
    
    global with sharing class EP_FE_CreditHoldings {
         global String AccountID {get; set;}
        global String AccountName {get; set;}
        global String holdingOverdueBalanceHCY {get; set;}
        global String holdingOverdueBalanceLCY {get; set;}
        global String OverdueBalanceHCY {get; set;}
        global string balanceHCY {get; set;}
        global string avaialableFundsHCY {get; set;}
        global string avaialableFundsLCY {get; set;}
        global string creditLimit {get; set;}
        global string primaryContactDetails {get; set;}
        
    }
    
    global with sharing class EP_FE_RouteAllocation {
        global ID shipTo {get; set;}
        global string Name {get; set;}
        global boolean DefaultRoute {get; set;}
        global string DeliveryWindowEnd {get; set;}
        global string DeliveryWindowStart {get; set;}
        global EP_FE_Route Route {get; set;}
        
    }
    global with sharing class EP_FE_Route {
        global string Company {get; set;}
        global string RouteDescription {get; set;}
        global boolean IsRouteActive {get; set;}
        global string RouteCode {get; set;}
        global string RouteName {get; set;}
        global string RouteStatus {get; set;}
        global List<EP_FE_Run> Run {get; set;}
    }
    global with sharing class EP_FE_Run {
         global string Name {get; set;}
        global string RunName {get; set;}
        global string RunNumber {get; set;}
        global string RunStartDate {get; set;}
        global string RunEndDate {get; set;}
        global string RunNameSys {get; set;}
        global string InstanceNumber {get;set;}
    }
    
    //Response except standard fields 
    public List<EP_FE_SellTo> sellToList {get; set;}
    public EP_FE_Dashboard dashboard {get; set;}
    public Map<String, EP_FE_Permission> permissions {get; set;}
    public Map<String, Map<String, List<String>>> statusMap;
    public Map<String, String> dictionary;
    public List<EP_FE_Credit_Information> credit {get;set;}
    public List<EP_FE_Fieldset_Information> fieldset {get; set;}
    public Map<String, String> configuration {get;set;}
    public Map<String, String> googleApiKey {get;set;}
    public List<EP_FE_Configuration__mdt> faqTopics {get;set;}
    public EP_FE_FetchRecordTypeList recordtype {get; set;}
    public List<EP_FE_DipSites> dipSites {get; set;}
    public EP_FE_User user {get; set;}
    public Map<String, EP_FE_OverallStatus> accountOverallStatus;
    public Map<string,List<EP_FE_CreditHoldings>> creditHoldings {get; set;}
    
    
}