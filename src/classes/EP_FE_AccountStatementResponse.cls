/* 
   @Author <Nicola Tassini>
   @name <EP_FE_AccountStatementResponse>
   @CreateDate <30/06/2016>
   @Description <Response for the Account Statement API>  
   @Version <1.0>
*/
global with sharing class EP_FE_AccountStatementResponse extends EP_FE_Response {

	global static final Integer ERROR_MISSING_REQUEST = -1;
	global static final Integer GENERIC_EXCEPTION_UTILITY = -2;

    global static final String CLASSNAME = 'EP_FE_AccountStatementEndpoint';
    global static final String METHOD = 'listCustomAccountStatement';
    global final static String SEVERITY = 'PE-';
    global final static String TRANSACTION_ID = 'ERROR';
    global final static String DESCRIPTION1 = 'Invalid Request';
    global final static String DESCRIPTION2 = 'Incomplete Request';
    global final static String DESCRIPTION3 = 'Date range cant be greater than 2 months';
    global final static String DESCRIPTION4 = 'Invalid Bill To Id';
    global final static String DESCRIPTION5 = 'Exception in fetching account statement items';


    public EP_WrapperAccountStatement customerAccountStatement {get; set;}
	
	//public EP_FE_AccountStatementResponse() {}

}