/* 
   @Author <Accenture>
   @name <EP_WINDMSOrderNewHandler>
   @CreateDate <02/27/2017>
   @Description <These are the stub classes to map JSON string for orders creation Rest Service Request from WINDMS> 
   @Version <1.0>
*/
/* Main stub Class for JSON parsing for VMI order Creation from WINDMS  */
public class EP_WINDMSOrderNewHandler extends EP_InboundHandler {
    @TestVisible private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();
    @TestVisible private static boolean processingFailed = false;
    @TestVisible private static EP_WINDMSOrderNewHelper VMIOrderHelper =  new EP_WINDMSOrderNewHelper();
    @TestVisible private map<id,Order> orderMap = new map<id,Order>();
    /**
    * @Author       Accenture
    * @Name         doProcess
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    public override string processRequest(string jsonBody) {
        EP_GeneralUtility.Log('public','EP_WINDMSOrderNewHandler','doProcess');
        EP_MessageHeader headerCommon = new EP_MessageHeader();
        string failureReason;
        try {
            EP_WINDMSOrderNewStub stub = (EP_WINDMSOrderNewStub )  System.JSON.deserialize(jsonBody, EP_WINDMSOrderNewStub.class);
            headerCommon = stub.MSG.HeaderCommon;
            list<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList = stub.MSG.Payload.Any0.Orders.Order;
            
            VMIOrderHelper.setCompanyCode(stub.MSG.HeaderCommon.SourceCompany);
            VMIOrderHelper.setOrderAttributes(orderWrapperList);
            createOrders(orderWrapperList);
            
            VMIOrderHelper.setOrderItemAttributes(orderWrapperList);
            createOrdersItems(orderWrapperList);
            
        } catch (exception exp ) {
            failureReason = exp.getMessage();
            EP_LoggingService.logServiceException(exp, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'processRequest', 'EP_WINDMSOrderNewHandler',  EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            
            createResponse(null, exp.getTypeName(), exp.getMessage()); 
        }
        return EP_AcknowledgementHandler.createAcknowledgement('VMI_ORDER_CREATION', processingFailed, failureReason, headerCommon, ackResponseList);
    }
    
    /**
    * @Author       Accenture
    * @Name         createOrders
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createOrders(list<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','createOrders');
        list<csord__Order__c> orderList = new list<csord__Order__c>();
        for(EP_WINDMSOrderNewStub.orderWrapper orderWrapper : orderWrapperList) {
            if(string.isNotEmpty(orderWrapper.errorDescription)) {
                createResponse(orderWrapper.seqId,orderWrapper.errorCode, orderWrapper.errorDescription);
            } else {
                orderList.add(orderWrapper.sfOrder);
            }
        }
        doOrderUpsert(orderList);
    }
    
    /**
    * @Author       Accenture
    * @Name         doOrderUpsert
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void doOrderUpsert(list<csord__Order__c> orderList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','doOrderUpsert');
        if(orderList.isEmpty()){
            return;
        }
        list<Database.UpsertResult> ordersUpsertResult = DataBase.Upsert(orderList,false);
        
        for(integer counter = 0 ; counter < ordersUpsertResult.size(); counter++) {        
            if(ordersUpsertResult[counter].isSuccess()){
                createResponse(orderList[counter].EP_SeqId__c, '', '' ); 
            }
            else {
                processUpsertErrors(ordersUpsertResult[counter].getErrors(), orderList[counter].EP_SeqId__c);
            }
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         processUpsertErrors
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processUpsertErrors(list<Database.Error> errorList,string seqId) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','processUpsertErrors');
        for(Database.Error err : errorList) {
            createResponse(seqId,String.valueOf(err.getStatusCode()), err.getMessage() ); 
        }
    }
    /**
    * @Author       Accenture
    * @Name         createOrdersItems
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createOrdersItems(list<EP_WINDMSOrderNewStub.orderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','createOrdersItems');
        list<csord__Order_Line_Item__c> orderItemsToBeProcess  = new list<csord__Order_Line_Item__c>();
        for(EP_WINDMSOrderNewStub.orderWrapper orderWrapper : orderWrapperList) {
            for(EP_WINDMSOrderNewStub.OrderLine ordLine : orderWrapper.OrderLines.orderLine){
                if(string.isNotEmpty(ordLine.errorDescription)) {
                    system.debug('**processOrderItemsError is' + ordLine.orderLineItem.EP_SeqId__c);
                    processOrderItemsError(orderWrapper,ordLine.orderLineItem.EP_SeqId__c, ordLine.errorDescription);
                } else {
                    orderItemsToBeProcess.add(ordLine.orderLineItem);
                }
            }
        }
        doOrderItemUpsert(orderItemsToBeProcess);
    }
    
    /**
    * @Author       Accenture
    * @Name         processOrderItemsError
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void processOrderItemsError(EP_WINDMSOrderNewStub.orderWrapper orderWrapper, string seqId, string errorDescription) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','processOrderItemsError');
        list<csord__Order__c> orderToBeDelete = new list<csord__Order__c>();
        createResponse(seqId , null, errorDescription);
        if(!orderWrapper.isExistingOrder) {
            orderToBeDelete.add(orderWrapper.sfOrder);
        }
        deleteOrder(orderToBeDelete);
    }
    
    /**
    * @Author       Accenture
    * @Name         doOrderItemUpsert
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void doOrderItemUpsert(list<csord__Order_Line_Item__c> orderItemsToBeProcess) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','doOrderItemUpsert');
        set<id> orderIdSet = new set<id>();
        system.debug('orderItemsToBeProcess is' + orderItemsToBeProcess);
        if(orderItemsToBeProcess.isEmpty()){
            return;
        }
        list<Database.UpsertResult> ordersItemsUpsertResult = Database.upsert(orderItemsToBeProcess,false);
        for(integer counter = 0 ; counter < ordersItemsUpsertResult.size(); counter++){           
            if(!ordersItemsUpsertResult[counter].isSuccess()){
                processUpsertErrors(ordersItemsUpsertResult[counter].getErrors(), orderItemsToBeProcess[counter].EP_SeqId__c);
            }else{
                orderIdSet.add(orderItemsToBeProcess[counter].orderId__c);
                system.debug('***orderItemsToBeProcess' + orderItemsToBeProcess[counter]);
            }
        }
        system.debug('orderIdSet is' + orderIdSet);
        List<csord__Order__c> orderWithStatusUpdate = VMIOrderHelper.validateAndSetOrderStatus(orderIdSet);
        updateOrders(orderWithStatusUpdate);
        VMIOrderHelper.doPostStatusUpdateActions(orderIdSet);
    }
    
    /**
    * @Author       Accenture
    * @Name         deleteOrder
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void deleteOrder(list<csord__Order__c> ordersObj) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','deleteOrder');
        if(!ordersObj.isEmpty()) {
            DataBase.Delete(ordersObj,false);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         createResponse
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void createResponse(string seqId, string errorCode, string errorDescription) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','createResponse');
        if(string.isNotBlank(errorDescription)) processingFailed = true;
        ackResponseList.add(EP_AcknowledgementUtil.createDataSet(EP_Common_Constant.ORDER_STRING, seqId, errorCode, errorDescription));
    }
    
    /**
    * @Author       Accenture
    * @Name         updateOrders
    * @Date         03/25/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private static void updateOrders(list<csord__Order__c> orderList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHandler','updateOrders');
        try {
            if(!orderList.isEmpty()) {
                database.update(orderList);
            }
        }catch(exception ex){
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'UPDATE_ORDERS_STATUS', 'VMIORDER_CREATION_HANDLER', EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF,EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
        }
    }
}