public class EP_ASTVendorNewToBlocked extends EP_AccountStateTransition {
    public EP_ASTVendorNewToBlocked() {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorNewToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVendorNewToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
		EP_GeneralUtility.Log('Public','EP_ASTVendorNewToBlocked','isGuardCondition');
       	return true;
    }
}