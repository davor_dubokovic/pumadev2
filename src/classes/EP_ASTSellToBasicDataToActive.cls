/*
*  @Author <Accenture>
*  @Name EP_ASTSellToBasicDataToActive
*/
public class EP_ASTSellToBasicDataToActive extends EP_AccountStateTransition{
/*
*  @Author <Accenture>
*  @Name EP_ASTSellToBasicDataToActive
*/
    public EP_ASTSellToBasicDataToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }
/*
*  @Author <Accenture>
*  @Name isGuardCondition
*/
    public override boolean isGuardCondition(){
    	//L4_45352_Start
        return true;// this.account.localaccount.EP_Is_Dummy__c;
        //L4_45352_End
    }

}