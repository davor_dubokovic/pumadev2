/**
 * @author <Prity Sangwan>
 * @name <EP_CreditExceptionTest>
 * @createDate <27/10/2016>
 * @description <This class is a test for deletion of Three strike rule > 
 * @version <1.0>
 */
@isTest
public without sharing class EP_CreditExceptionTest {
   public static Account sellTo,billTo,nonVMIship,storageLoc,vendor1;
  public static EP_Freight_Matrix__c f1 = new EP_Freight_Matrix__c();
  public static EP_Stock_Holding_Location__c supplyOption;
  public static EP_Country__c country1 = new EP_Country__c();
  public static String COUNTRY_NAME = 'Australia';
  public static String COUNTRY_CODE = 'AU';
  public static String COUNTRY_REGION = 'Australia';
  public static String REGION_NAME = 'North-Australia';
  public static EP_Tank__c tankObj;
 // public static Contract contract1 = new Contract();
  public static string VENDOR_ACCOUNT_RT = 'Vendor';
  public static final string VENDORTYPE = 'Transporter';
  public static final String NAV_VENDOR_ID_1 = '00001';
  public static EP_Route__c route1;
  public static  EP_Run__c run1;
  public static EP_Route_Allocation__c routeAllocation;
  public static EP_Supply_Location_Transporter__c SLT;
  public static PricebookEntry prodPricebookEntry,prodSellToPricebookEntry,customProdPricebookEntry;
  public static Product2 productObj1,productObj2;
  public static EP_Stock_holding_location__c supplyOption1;
  public static EP_Order_Configuration__c orderConfig,orderConfigNonMixing;
  public static EP_Inventory__c primaryInventory;
  public static Pricebook2 customPB1,customPB2;
  /*
  Creating Order
  */
  /*public static testMethod void creationOrder(){
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
           
             EP_OrderTriggerHandler.isExecuteBeforeUpdate = False;
             EP_OrderTriggerHandler.isExecutedBeforeInsert = True;
             EP_OrderTriggerHandler.isExecutedAfterInsert = True;
             EP_OrderTriggerHandler.isExecuted = True;
             EP_OrderTriggerHandler.isSyncWithNav = True;
            createTestData();
            
            Test.loadData(EP_Integration_Status_Update__c.SobjectType,'EP_IntegrationRecords_CS');
            Test.loadData(EP_Order_Fieldset_Sfdc_Nav_Intg__c.SobjectType,'EP_OrderStatusUpdate_FieldSet');
            Test.loadData(EP_PROCESS_NAME_CS__c.SobjectType,'PROCESS_NAME_CS');
            Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.SobjectType,'EP_INTEGRATION_CUSTOM_SETTING');
            
            EP_OrderTriggerHandler.isExecuteAfterUpdate = true;
            
            PageReference pageRef = Page.EP_PortalOrderPage;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', sellTo.Id);
            EP_PortalOrderPageController portalOrder =new EP_PortalOrderPageController();
            portalOrder.loadAccountOrderDetails();
            portalOrder.newOrderRecord.EP_Order_Category__c = 'Sales Order';
            portalOrder.newOrderRecord.EP_Order_Epoch__c = 'Current';
            portalOrder.newOrderRecord.EP_Order_Product_Category__c = 'Bulk';
            portalOrder.strSelectedDeliveryType = 'Delivery';
            portalOrder.newOrderRecord.EP_Requested_Delivery_Date__c = System.today();
            portalOrder.next();
            portalOrder.strSelectedShipToID= nonVMIship.Id;
            portalOrder.strSelectedProductID = productObj1.id;
            portalOrder.strSelectedPricebookID= prodPricebookEntry.id;
            portalOrder.newOrderRecord.EP_ShipTo__c = nonVMIship.Id;
            system.assertNotEquals(portalOrder.strSelectedPricebookID,NULL);
            system.assertNotEquals(portalOrder.newOrderRecord.CurrencyIsoCode,NULL);
            system.assertNotEquals(portalOrder.newOrderRecord.EP_Order_Product_Category__c,NULL);
           /* PriceBookEntry priceBookEntries = [SELECT ID, Product2Id, PriceBook2.Name, Product2.Name, Product2.EP_Product_Sold_As__c, Product2.EP_Unit_Of_Measure__c,
                            Pricebook2Id, UnitPrice, EP_Unit_Of_Measure__c
                            FROM PriceBookEntry WHERE PriceBook2Id =: portalOrder.strSelectedPricebookID
                            AND Product2.IsActive = TRUE
                            AND CurrencyIsoCode =: portalOrder.newOrderRecord.CurrencyIsoCode 
                            AND Product2.EP_Product_Sold_As__c =: portalOrder.newOrderRecord.EP_Order_Product_Category__c];            
            system.assertEquals(priceBookEntries,NULL);*/

            /*system.assertEquals(portalOrder.strSelectedShipToID,nonVMIship.Id);
            
            
            portalOrder.next();
            List<Apexpages.Message> pageMessages = ApexPages.getMessages();
            //system.assertEquals(pageMessages ,NULL);
            
            portalOrder.next();
            
           // system.assert(portalOrder.blnAccountSetupError);
            portalOrder.strSelectedPaymentTerm = 'Credit';
            portalOrder.newOrderRecord.Stock_Holding_Location__c = supplyOption1.Id;
            portalOrder.strTransporterId = SLT.Id;
            portalOrder.strSelectedDate = String.valueOf(System.Now()+7);
           // portalOrder.aOrderWrapper[0].oliTanksID = tankObj.Id;
            portalOrder.aOrderWrapper[0].oliQuantity = 20;
            
            
            portalOrder.next();
            portalOrder.submit();
           
            system.assertNotEquals(NULL,portalOrder.newOrderRecord.Id);
            system.debug('--neworder--'+portalOrder.newOrderRecord.Id);
            portalOrder.saveOrderAsDraft();
            Double orderCost = portalOrder.totalOrderCost;
            
            system.assertNotEquals(NULL,portalOrder.newOrderRecord.Id);
             Test.StartTest();
            Order o = [Select Id,EP_Bill_To__c,EP_Sell_To__c,EP_Duty__c,Status,EP_Payment_Term_Value__c,AccountId from Order Where Id=: portalOrder.newOrderRecord.Id];
            system.debug('--neworder--'+o);
            
            
            list<EP_Credit_Exception_Request__c> credList = new list<EP_Credit_Exception_Request__c>();
            
            String creditExcReqId1 = EP_PortalOrderUtil.createCreditExcRequest(o.Id,o.AccountId);
            System.debug('creditExcReqId1##'+creditExcReqId1);
            System.assertNotEquals(creditExcReqId1 ,NULL);
           
            String creditExcReqId2 = EP_PortalOrderUtil.createCreditExcRequest(o.Id,o.AccountId);
            System.debug('creditExcReqId2##'+creditExcReqId2);
            System.assertNotEquals(creditExcReqId2 ,NULL);
           
            String creditExcReqId3 = EP_PortalOrderUtil.createCreditExcRequest(o.Id,o.AccountId);
            System.debug('creditExcReqId3##'+creditExcReqId3);
            System.assertNotEquals(creditExcReqId3 ,NULL);
             
            String creditExcReqId4 = EP_PortalOrderUtil.createCreditExcRequest(o.Id,o.AccountId);
            System.assertNotEquals(creditExcReqId4 ,NULL);
           
            //credList.add(exc4); 
            //update credList;
           // System.assertEquals(credList.size(),4);
            Test.StopTest(); 
      }
   }*/
     /*
    Creating initial data
    */
    public static void createTestData(){
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterInsert = true;
        //EP_AccountTriggerHandler.isExecuteBeforeInsert = true; 
        
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isRecordOnInsert = true;
        //EP_OrderTriggerHandler.isExecuted = True;
        //EP_OrderUpdateHandlerAfterUpdateOrder = False;
        //EP_OrderTriggerHandler.isExecuteOnDraftStatus = False;
            
        id orderConfigCountryRangeRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER_CONFIG_OBJ,
                                                                                        EP_Common_Constant.COUNTRY_RANGE);
                                                                                        
        id orderConfigNonMixingRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER_CONFIG_OBJ,
                                                                                        EP_Common_Constant.NON_MIXING_RANGE);
                                                                                                                                                                           
        EP_Region__c region;
        EP_Payment_Method__c paymentMethod = EP_TestDataUtility.createPaymentMethod('Cash Payment'
                                                                                        ,'CP');
        insert paymentMethod;
        
        EP_Payment_Term__c paymentTerm =  EP_TestDataUtility.createPaymentTerm();
        insert paymentTerm;
        
        Company__c comp = EP_TestDataUtility.createCompany('AUN');
        comp.EP_Disallow_DD_for_PP_customers__c = true;
        insert comp;
        
        country1 = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        insert country1;
        
        region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country1.Id);  
        insert region;
        
        Id pricebookId = Test.getStandardPricebookId(); 
        ////////// Create Product //////////
        List<Product2> prodList = new List<Product2>();
        productObj1 = new product2(Name = 'P1', CurrencyIsoCode = 'AUD', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Unit_of_Measure__c='LT',EP_Company_Lookup__c=comp.Id,ProductCode='10005');  
        productObj1.EP_Product_Sold_As__c = 'Bulk';                 
       
        
        productObj2 = new product2(Name = 'P2', CurrencyIsoCode = 'AUD', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Unit_of_Measure__c='LT',EP_Company_Lookup__c=comp.Id,ProductCode='30005');  
        productObj2.EP_Product_Sold_As__c = 'Bulk';  
        
        prodList.add(productObj1);
        prodList.add(productObj2);      
        insert prodList;      
       
       /* prodPricebookEntry = [SELECT Id,CurrencyIsoCode,Product2Id, PriceBook2.Name, Product2.Name, Product2.EP_Unit_Of_Measure__c,
                        Pricebook2Id, UnitPrice, EP_Unit_Of_Measure__c,Product2.EP_Product_Sold_As__c FROM PricebookEntry WHERE Product2.id =: productObj1.id LIMIT : EP_Common_Constant.ONE];*/
        //system.assertEquals(NULL,prodPricebookEntry);
        // Create a custom price book //////
        
       
        customPB1 = new Pricebook2(Name='myPB123', isActive=true,EP_Company__c=comp.ID);
        insert customPB1; 
        prodPricebookEntry = new PricebookEntry(
        Pricebook2Id = customPB1.Id, Product2Id = productObj1.Id,
        UnitPrice = 12000, IsActive = true,EP_Is_Sell_To_Assigned__c=true);
        insert prodPricebookEntry;
        
       /* customPB2 = new Pricebook2(Name='myPBSellTo123', isActive=true,EP_Company__c=comp.ID);
        insert customPB2; 
        prodSellToPricebookEntry = new PricebookEntry(
        Pricebook2Id = customPB2.Id, Product2Id = productObj2.Id,
        UnitPrice = 12000, IsActive = true,EP_Is_Sell_To_Assigned__c=true);
        insert prodSellToPricebookEntry; */
      //Bill-to
       billTo = EP_TestDataUtility.createBillToAccount();
       billTo.EP_Country__c = country1.id;
       billTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
       billTo.EP_Puma_Company__c = comp.id;
       billTo.EP_Composite_ID__c = 'AUN-111';
      //billTo.EP_PriceBook__c = pricebookId;
       billTo.EP_PriceBook__c  = customPB1.id;
       billTo.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
       insert billTo;
       billTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
       update billTo;
       billTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
       update billTo;
       
       f1 = EP_TestDataUtility.createFreightMatrix();
       insert f1;
       
       sellTo = EP_TestDataUtility.createSellToAccount(billTo.Id,f1.Id);
       sellTo.EP_Country__c = country1.id;
       sellTo.EP_Payment_Term_Lookup__c = paymentTerm.Id;
       sellTo.EP_Puma_Company__c = comp.id;
       sellTo.EP_Payment_Method__c = paymentMethod.id;
       sellTo.EP_Requested_Payment_Terms__c = 'Credit';
       sellTo.EP_Recommended_Credit_Limit__c = 100;
       sellTo.EP_Composite_ID__c = 'AUN-202';
       
       sellTo.EP_Alternative_Payment_Method__c = 'Paylink';
       sellTo.EP_Excise_duty__c = 'Excise Free';
      // sellTo.EP_PriceBook__c = customPB2.id;
      // sellTo.EP_PriceBook__c = pricebookId;
       sellTo.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
       insert sellTo;
       sellTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
       update sellTo;
       sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
       update sellTo;
        
  
     // create country range order configuration record
        orderConfig =  new EP_Order_Configuration__c();
        orderConfig.RecordTypeId = orderConfigCountryRangeRecordType;
        orderConfig.EP_Country__c = country1.id;
        orderConfig.EP_Max_Quantity__c = 200;
        orderConfig.EP_Min_Quantity__c = 1;
        orderConfig.EP_Product__c = productObj1.id;
        orderConfig.EP_Volume_UOM__c = 'LT';
        Database.insert(orderConfig);
        
        orderConfigNonMixing =  new EP_Order_Configuration__c();
        orderConfigNonMixing.RecordTypeId = orderConfigNonMixingRecordType;
        orderConfigNonMixing.EP_Country__c = country1.id;
        orderConfigNonMixing.EP_Max_Quantity__c = 200;
        orderConfigNonMixing.EP_Min_Quantity__c = 1;
        orderConfigNonMixing.EP_Product__c = productObj1.id;
        orderConfigNonMixing.EP_Volume_UOM__c = 'LT';
        Database.insert(orderConfigNonMixing); 
           
        Id recordtypeid = EP_Common_Util.fetchRecordTypeId('Account','Non-VMI Ship To');
        nonVMIship = EP_TestDataUtility.createShipToAccount(sellTo.id,recordtypeid);
        nonVMIship.name = 'NVMI1';
        nonVMIship.EP_Puma_Company__c = comp.id;
        nonVMIship.EP_Country__c = country1.id;
        nonVMIship.EP_VMI_Suggestion__c = false;
        //nonVMIship.EP_PriceBook__c = pricebookId;
        nonVMIship.EP_PriceBook__c = customPB1.id;
        nonVMIship.EP_Ship_To_Type__c = 'Consignment';
        nonVMIship.EP_Duty__c = 'Excise Free';
        nonVMIship.EP_Composite_ID__c = 'AUN-202-41001';
        nonVMIship.EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
        insert nonVMIship;
        nonVMIship.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update nonVMIship;
        nonVMIship.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update nonVMIship;
        
        tankObj= new EP_Tank__c();
        tankObj.EP_Ship_To__c =nonVMIship.Id;
        tankObj.EP_Safe_Fill_Level__c=60;
        tankObj.EP_Deadstock__c=20;
        tankObj.EP_Tank_Status__c='New';
        tankObj.EP_Tank_Dip_Entry_Mode__c='Portal Dip Entry';
        tankObj.EP_Capacity__c=80;                
        tankObj.EP_Tank_Code__c = '1';
        tankObj.EP_Product__c = productObj2.Id;
       // tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL;
        insert tankObj; 
        
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;
        
        system.assertEquals(tankObj.EP_Tank_Code__c,'1');
        tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL;
        Database.update(tankObj); 
   
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true LIMIT : EP_Common_Constant.ONE];
        
        storageLoc = EP_TestDataUtility.createStorageLocAccount(country1.Id,bhrs.Id);
        insert storageLoc;
        storageLoc.EP_Country__c = country1.id;
        storageLoc.EP_Puma_Company__c = comp.id;
        storageLoc.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update storageLoc;
        storageLoc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        storageLoc.EP_Nav_Stock_Location_Id__c = '155454';
        update storageLoc;
        
        // Adding Run and Route
        route1 =  new EP_Route__c(EP_Status__c = 'Active');
        route1.EP_Storage_Location__c = storageLoc.Id;
        Database.insert(route1);
        run1 =  new EP_Run__c(EP_Route__c = route1.Id);
        run1.EP_Run_End_Date__c = System.Today() + 60;
        run1.EP_Run_Start_Date__c = System.Today();
        Database.Insert(run1);
        routeAllocation = new EP_Route_Allocation__c();
        routeAllocation.Delivery_Window_End_Date__c = 10;
        routeAllocation.EP_Delivery_Window_Start_Date__c = 5;
        routeAllocation.EP_Route__c = route1.Id;
        routeAllocation.EP_Ship_To__c = nonVMIship.Id;
        Database.Insert(routeAllocation); 
        
        vendor1 = createTestVendor(VENDORTYPE, NAV_VENDOR_ID_1,comp);
        vendor1 .EP_Status__c = EP_Common_Constant.STATUS_PROSPECT;
        insert vendor1 ;
        vendor1 .EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update vendor1 ;
        vendor1 .EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update vendor1 ;
        
        
        list<EP_Stock_holding_location__c> SHLlist= new list<EP_Stock_holding_location__c>();
        
        Id supplyLocSellToRecordtypeid = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.EX_RACK_REC_TYPE_NAME);
        supplyOption1 = EP_TestDataUtility.createSellToStockLocation(sellTo.id,true,storageLoc.Id,supplyLocSellToRecordtypeid); //Add one SHL
        supplyOption1.EP_Trip_Duration__c = 1;
        supplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
        supplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
        supplyOption1.EP_Is_Pickup_Enabled__c = True;
        supplyOption1.EP_Duty__c = 'Excise Paid';       
        insert supplyOption1;
       // SHLlist.add(supplyOption1);
        
        Id supplyLocDeliveryRecordtypeid = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.DLVRY_REC_TYPE_NAME);
        EP_Stock_holding_location__c supplyOption2 = EP_TestDataUtility.createShipToStockLocation(nonVMIship.id,true,storageLoc.Id,supplyLocDeliveryRecordtypeid ); //Add one SHL
        supplyOption2.EP_Trip_Duration__c = 1;
        supplyOption2.EP_Ship_To__c = nonVMIship.Id;
        supplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
        supplyOption2.EP_Use_Managed_Transport_Services__c = 'N/A';
        supplyOption2.EP_Is_Pickup_Enabled__c = True;
        supplyOption2.EP_Transporter__c = vendor1.id;
        supplyOption2.EP_Duty__c = 'Excise Paid';    
        insert supplyOption2;
       // SHLlist.add(supplyOption2);
        insert SHLlist;
        
        primaryInventory = createInventory(storageLoc,productObj2);
        insert primaryInventory;
        
      /* contract1 = EP_TestDataUtility.createContract(storageLoc.Id,comp.Id,paymentTerm.Id,vendor1);
        Database.insert(contract1,true);
        contract1.Status = EP_Common_Constant.EP_CONTRACT_STATUS_ACTIVE;
        Database.update(contract1,true); */
        
        SLT = createSupplyLocationTransporter(vendor1.Id, supplyOption2.Id, true );
        Insert(SLT);
    }
    
    /*
        This method is for creating vendor
    */
    public static Account createTestVendor(String vendorTypee, String NAV_VendorId,Company__c company)  {
     Id VENDOR_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(VENDOR_ACCOUNT_RT).getRecordTypeId();
        Account vendorAcc = new Account( 
                                Name = 'Transporter 1',
                                EP_Status__c = '05-Active', 
                                RecordTypeId = VENDOR_RT,  
                                /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
                                EP_VendorType__c = vendorTypee,
                                EP_Source_Entity_ID__c = NAV_VendorId,
                                /* TFS fix 45559,45560,45567,45568 end*/
                                EP_Puma_Company__c = company.Id,
                                BillingStreet = '123 st',
                                BillingCity = 'Gurgaon',
                                BillingState = 'Haryana',
                                BillingPostalCode = '123456',
                                BillingCountry = 'India',
                                Phone = '12345'
                            );
        return vendorAcc ;
    }
     /*
        This method is for activatng ship to
    */ 
    public static EP_Supply_Location_Transporter__c createSupplyLocationTransporter( Id TransporterId, Id supplyLocId, Boolean isDefault ) 
    {
       SLT = new EP_Supply_Location_Transporter__c( 
                EP_Is_Default__c = isDefault,
                EP_Transporter__c = TransporterId,
                EP_Supply_Location__c = supplyLocId
            );
        return SLT;
    }
      /*
        This method is for Inventory creation
    */ 
    public static EP_Inventory__c createInventory( Account storageLocation,Product2 product) 
    {
       primaryInventory = new EP_Inventory__c( 
                
                EP_Storage_Location__c=storageLocation.id,
                EP_Product__c=product.Id,
                EP_Stock_Label__c = 'Third_Party',
                EP_Status__c = 'Active',
                EP_Inventory_Availability__c = 'Good',
                EP_SKU_Unique_Key__c = '10001'

            );
      return primaryInventory;
    }
  }