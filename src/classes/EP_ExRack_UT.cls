@isTest
public class EP_ExRack_UT
{

static testMethod void updateTransportService_test() {
    EP_ExRack localObj = new EP_ExRack();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getExRackOrder();
    objOrder.EP_Use_Managed_Transport_Services__c = 'FALSE';
    // **** TILL HERE ~@~ *****
    Test.startTest();
    localObj.updateTransportService(objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals('FALSE',objOrder.EP_Use_Managed_Transport_Services__c );
    // **** TILL HERE ~@~ *****
}
static testMethod void getInventoryDetails_test() {
    EP_ExRack localObj = new EP_ExRack();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c newOrderRecord = EP_TestDataUtility.getExRackOrder();
    LIST<csord__Order_Line_Item__c> oiList = EP_TestDataUtility.getOrderItems(newOrderRecord.Id);
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Map<id,String> result = localObj.getInventoryDetails(oiList,newOrderRecord);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null,result);
    // **** TILL HERE ~@~ *****
}

static testMethod void findPriceBookEntries_test() {
    EP_ExRack localObj = new EP_ExRack();
    // **** IMPLEMENT THIS SECTION ~@~ *****
   
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getExRackOrder();
    
    Id pricebookId = [Select Id, EP_Pricebook__c from Account WHere Id=: objOrder.AccountId__c].Id;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    LIST<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}

static testMethod void showCustomerPO_PositiveScenariotest() {
    EP_ExRack localObj = new EP_ExRack();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getOrderPositiveTestScenario().getOrder();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.showCustomerPO(objOrder);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void showCustomerPO_NegativeScenariotest() {
   EP_ExRack localObj = new EP_ExRack();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getOrderNegativeTestScenario().getOrder();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.showCustomerPO(objOrder);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void findRecordType_test() {
    EP_ExRack localObj = new EP_ExRack();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_OrderEpoch epochType = new EP_OrderEpoch();
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_VendorManagement vmType = new EP_VendorManagement();
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_ConsignmentType consignmentType = new EP_ConsignmentType();
    // **** TILL HERE ~@~ *****
    Id nonVMIOrdId = Schema.getGlobalDescribe().get(EP_Common_Constant.ORDER).getDescribe().getRecordTypeInfosByName().get(EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME).getRecordTypeId();
    Test.startTest();
    Id result = localObj.findRecordType(epochType,vmType,consignmentType);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(nonVMIOrdId,result);
    // **** TILL HERE ~@~ *****
}

}