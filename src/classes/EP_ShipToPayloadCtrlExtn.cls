/* 
   @Author 			Accenture
   @name 			EP_ShipToPayloadCtrlExtn
   @CreateDate 		02/15/2017
   @Description		Controller Extension to generate XML for ship to sync
   @Version 		1.0
*/
public with sharing class EP_ShipToPayloadCtrlExtn { 
    private string messageId;
    private string messageType;
    public String shipToSeqId {get;set;}  
    public Account AccountObject {get;set;}
    private string secretCode;
    //USED FOR WINDMS SYNC
    public List<tanksWrapper> tanks {get;set;}
    public List<supplyLocationsWrapper> supplyLocations {get;set;}
    
    /**
	* @author 			Accenture
	* @name				shipToStatus
	* @date 			02/15/2017
	* @description 		Sets the ship to status based on the account status
	* @param 			NA
	* @return 			string
	*/
	public string shipToStatus {
        get {
        	//Defect fix for #57771 start
            //To set the value of shipToStatus for WINDMS
            string shipToStatusValue = ((EP_Common_Constant.STATUS_BLOCKED.equalsIgnoreCase(String.valueOf(this.AccountObject.EP_Status__c))
                                         || EP_Common_Constant.STATUS_INACTIVE.equalsIgnoreCase(String.valueOf(this.AccountObject.EP_Status__c))
                                        ) ? EP_Common_Constant.STRING_NO_LS :  EP_Common_Constant.STRING_YES_LS );
			return shipToStatusValue ; } set;
			//Defect fix for #57771 end 
    }
    
	/* 
	   @Author 			Accenture
	   @name 			tanksWrapper
	   @CreateDate 		02/15/2017
	   @Description		Wrapper class to hold tankSeqId with its tank records
	   @Version 		1.0
	*/     
	public class tanksWrapper {
        public EP_Tank__c Tank {get;set;}
        public string tankSeqId {get;set;}
    }
    
	/* 
	   @Author 			Accenture
	   @name 			supplyLocationsWrapper
	   @CreateDate 		02/15/2017
	   @Description		Wrapper class to hold supplyLocationSeqId with its supply location records
	   @Version 		1.0
	*/ 
	public class supplyLocationsWrapper {
        public EP_Stock_Holding_Location__c supplyLocation {get;set;}
        public string supplyLocationSeqId {get;set;}
    }
    
	/**
	* @author 			Accenture
	* @name				EP_ShipToPayloadCtrlExtn
	* @date 			03/13/2017
	* @description 		The extension constructor initializes the members variable AccountObject by using the getRecord method from the standard controller. This method will be use setup the values for Header in XML message
	* @param 			ApexPages.StandardController
	* @return 			NA
	*/     
	public EP_ShipToPayloadCtrlExtn (ApexPages.StandardController stdController) {
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        if (!Test.isRunningTest()) stdController.addFields(new List<String>{EP_Common_Constant.EP_STATUS,EP_Common_Constant.LASTMOIDIFIEDBYALIAS}); 
        this.AccountObject = (Account) stdController.getRecord();
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        shipToSeqId = EP_IntegrationUtil.reCreateSeqId(this.messageId, this.AccountObject.Id);
        this.setShipToAddressForLS();
    }
    
	/**
	* @author 			Accenture
	* @name				setShipToAddressForLS
	* @date 			03/13/2017
	* @description 		This will get the shipToaddresses along with Tanks and SupplyLocations and will setup the wrappers for WINDMS request
	* @param 			NA
	* @return 			NA
	*/     
	public void setShipToAddressForLS() {
        EP_GeneralUtility.Log('Public','EP_ShipToPayloadCtrlExtn','setShipToAddressForLS');
        this.tanks = new List<tanksWrapper>();
        this.supplyLocations = new List<supplyLocationsWrapper>();
        EP_AccountMapper accMapper = new EP_AccountMapper();
        for(Account shipToAdd : accMapper.getShipToWithTanksExcludingSellToById(AccountObject.Id,EP_Common_Constant.STATUS_SET_UP)) {
            for(EP_Tank__c epTank : shipToAdd.Tank__r){
                tanksWrapper tankWrap = new tanksWrapper();
                tankWrap.tankSeqId = EP_IntegrationUtil.reCreateSeqId( this.messageId, epTank.Id);
                tankWrap.Tank = epTank;
                this.tanks.add(tankWrap);
            }
            for(EP_Stock_Holding_Location__c  epSupplyLocation : shipToAdd.Stock_Holding_Locations1__r){
                supplyLocationsWrapper supplyWrap = new supplyLocationsWrapper();
                supplyWrap.supplyLocationSeqId = EP_IntegrationUtil.reCreateSeqId( this.messageId, epSupplyLocation.Id);
                supplyWrap.supplyLocation  = epSupplyLocation;
                this.supplyLocations.add(supplyWrap);
            }
        }
    }
    
	/**
	* @author 			Accenture
	* @name				checkPageAccess
	* @date 			03/13/2017
	* @description 		This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
	* @param 			NA
	* @return 			NA
	*/
	public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_ShipToPayloadCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}