@isTest
public class EP_OutboundMessageService_UT
{
 	//#60147 User Story Start
	@testSetup static void init() {
		List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
      	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
      	List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
      	List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }
    //#60147 User Story End
    
    static testMethod void Constructor_WithoutRecordId() {
	    Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    String companyName= sellToAcc.EP_Puma_Company_Code__c;
	    string exMessage ='';
	    Test.startTest();
	    try {
	    	EP_OutboundMessageService localObj = new EP_OutboundMessageService(null,messageType,companyName);Test.stopTest();
	    } catch(Exception ex){
	    	exMessage =ex.getMessage();
	    }
	    System.AssertEquals(true,exMessage==System.Label.Ep_RecordId_Missing); 
	}
	
	static testMethod void Constructor_WithoutMessageType() {
	    Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = sellToAcc.id;
	    String companyName= sellToAcc.EP_Puma_Company_Code__c;
	    string exMessage ='';
	    Test.startTest();
	    try {
	    	EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,EP_Common_Constant.BLANK,companyName);Test.stopTest();
	    } catch(Exception ex){
	    	exMessage =ex.getMessage();
	    }
	    System.AssertEquals(true,exMessage==System.Label.Ep_MessageType_Missing); 
	}
	
	static testMethod void Constructor_WithoutCompanyName() {
	    Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = sellToAcc.id;
	    String companyName= sellToAcc.EP_Puma_Company_Code__c;
	    string exMessage ='';
	    Test.startTest();
	    try {
	    	EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,EP_Common_Constant.BLANK);Test.stopTest();
	    } catch(Exception ex){
	    	exMessage =ex.getMessage();
	    }
	    System.AssertEquals(true,exMessage==System.Label.Ep_CompanyName_Missing); 
	}
	
	static testMethod void Constructor_WithoutWrongType() {
	    Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
	    String messageType = 'SFDC_TO_NAV_CUSTOMER_CREATION_2' ;
	    Id recordId = sellToAcc.id;
	    String companyName= sellToAcc.EP_Puma_Company_Code__c;
	    string exMessage ='';
	    Test.startTest();
	    try {
	    	EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);Test.stopTest();
	    } catch(Exception ex){
	    	exMessage =ex.getMessage();
	    }
	    System.AssertEquals(true,exMessage.contains('Cannot instantiate outbound message service for this Message Type')); 
	}

	static testMethod void setTransactionId_test() {
	    Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = sellToAcc.id;
	    String companyName= sellToAcc.EP_Puma_Company_Code__c;
	    EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);    
	    String transactionId=EP_Common_Constant.TEMPSTRINGWITHHYPHEN ;     
	    Test.startTest();
	    localObj.setTransactionId(transactionId);
	    Test.stopTest();
	    System.AssertEquals(true,localObj.transactionId==transactionId); 
	}
	
	static testMethod void checkIfDummy_test() {
	    Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = sellToAcc.id;
	    String companyName= sellToAcc.EP_Puma_Company_Code__c;
	    EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);    
	    String transactionId=EP_Common_Constant.TEMPSTRINGWITHHYPHEN ;     
	    Test.startTest();
	    boolean result = localObj.checkIfDummyAccount(recordId);
	    Test.stopTest();
	    System.AssertEquals(false,result); 
	}
	
	static testMethod void sendOutboundMessage_test() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    Account shipToAccount =EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();  
	    Account accObject =[select id, Parent.Id,Parent.EP_Puma_Company_Code__c from Account where id=:shipToAccount.Id];
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = accObject.Parent.Id;
	    String companyName= accObject.Parent.EP_Puma_Company_Code__c;
	    EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);     
	    String transactionType =EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    LIST<id> childIdList = new List<Id>(); 
	    childIdList.add(shipToAccount.Id);
	    Test.startTest();
	    localObj.sendOutboundMessage(transactionType,childIdList);
	    Test.stopTest();
	    System.AssertEquals(true,localObj.transactionType==transactionType);
	}
	
	static testMethod void sendOutboundMessageAsync_XML_test() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
	    Order orderObj = [Select id, Account.EP_Puma_Company_Code__c From Order Where Id =: obj.getOrder().Id];
	    String messageType = 'SFDC_TO_NAV_ORDER_SYNC';
	    Id recordId = orderObj.Id;
	    string messageId= EP_Common_Constant.TEMPSTRINGWITHHYPHEN; 
	    String companyName= orderObj.Account.EP_Puma_Company_Code__c;	    
	    String transactionType ='ORDER_UPDATE';
	    LIST<id> childIdList = new List<Id>(); 
	    childIdList.add(orderObj.Id);
	    
	    Set<id> integrationRecords = new Set<Id>();
		//move to test data utility
		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c='123';
		record.EP_Object_Type__c ='Order';
		record.EP_Status__c='SYNC';
		record.EP_Source__c='Source';
		record.EP_Target__c='Target';
		Database.insert(record);
		integrationRecords.add(record.Id);
		
	    Test.startTest();
	    EP_OutboundMessageService.sendOutboundMessageAsync(recordId,messageId,messageType,integrationRecords,companyName);
	    Test.stopTest();
	    EP_IntegrationRecord__c intRecord = [select id,EP_XML_Message__c from EP_IntegrationRecord__c where id=:record.Id];
	    
	    System.AssertEquals(true,intRecord.EP_XML_Message__c!=null);
	}
	
	static testMethod void sendOutboundMessageAsync_test() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    Account shipToAccount =EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();  
	    Account accObject =[select id, Parent.Id,Parent.EP_Puma_Company_Code__c from Account where id=:shipToAccount.Id];
	    
	    String messageType ='SFDC_TO_WINDMS_CUSTOMER_CREATION';// EP_Common_Constant.SFDC_TO_WINDMS_CUSTOMER_CREATION ;
	    Id recordId = accObject.Parent.Id;
	    string messageId= EP_Common_Constant.TEMPSTRINGWITHHYPHEN; 
	    String companyName= accObject.Parent.EP_Puma_Company_Code__c;
	    
	    String transactionType =EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    LIST<id> childIdList = new List<Id>(); 
	    childIdList.add(shipToAccount.Id);
	    
	    Set<id> integrationRecords = new Set<Id>();
		//move to test data utility
		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c='123';
		record.EP_Object_Type__c ='Account';
		record.EP_Status__c='SYNC';
		record.EP_Source__c='Source';
		record.EP_Target__c='Target';
		//record.Payload_VF_Page_Name__c = 'SFDC_TO_WINDMS_CUSTOMER_CREATION';
		Database.insert(record);
		integrationRecords.add(record.Id);
		
	    Test.startTest();
	    //EP_OutboundMessageService.messageTypeSet= new set<string>{'SFDC_TO_WINDMS_CUSTOMER_CREATION'};//EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION};
	    EP_OutboundMessageService.sendOutboundMessageAsync(recordId,messageId,messageType,integrationRecords,companyName);
	    Test.stopTest();
	    EP_IntegrationRecord__c intRecord = [select id,EP_XML_Message__c from EP_IntegrationRecord__c where id=:record.Id];
	    
	    System.AssertEquals(true,intRecord.EP_XML_Message__c!=null);
	}
	static testMethod void sendOutboundMessageAsync_testelse() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    Account shipToAccount =EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();  
	    Account accObject =[select id, Parent.Id,Parent.EP_Puma_Company_Code__c from Account where id=:shipToAccount.Id];
	    
	    String messageType = 'SFDC_TO_WINDMS_TANK_CREATION' ;
	    Id recordId = accObject.Parent.Id;
	    string messageId= EP_Common_Constant.TEMPSTRINGWITHHYPHEN; 
	    String companyName= accObject.Parent.EP_Puma_Company_Code__c;
	    
	    String transactionType =EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    LIST<id> childIdList = new List<Id>(); 
	    childIdList.add(shipToAccount.Id);
	    
	    Set<id> integrationRecords = new Set<Id>();
		//move to test data utility
		EP_IntegrationRecord__c  record = new EP_IntegrationRecord__c ();
		record.EP_Message_ID__c='123';
		record.EP_Object_Type__c ='Account';
		record.EP_Status__c='SYNC';
		record.EP_Source__c='Source';
		record.EP_Target__c='Target';
		Database.insert(record);
		integrationRecords.add(record.Id);
		
	    Test.startTest();
	    EP_OutboundMessageService.sendOutboundMessageAsync(recordId,messageId,messageType,integrationRecords,companyName);
	    Test.stopTest();
	    EP_IntegrationRecord__c intRecord = [select id,EP_XML_Message__c from EP_IntegrationRecord__c where id=:record.Id];
	    
	    System.AssertEquals(true,intRecord.EP_XML_Message__c!=null);
	}
	
	static testMethod void generateMessageId_test() {
	    Account sellToAcc =EP_TestDataUtility.getSellToASBasicDataSetUp();  
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = sellToAcc.id;
	    String companyName= sellToAcc.EP_Puma_Company_Code__c;
	    EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);
	    Test.startTest();
	    String result = localObj.generateMessageId();
	    Test.stopTest();
	    System.AssertEquals(true,result!=null);
	}
	static testMethod void generateIntegrationRecords_test() {
	    Account shipToAccount =EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();  
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Account accObject =[select id, Parent.Id,Parent.EP_Puma_Company_Code__c from Account where id=:shipToAccount.Id];
	    Id recordId = accObject.Parent.Id;
	    String companyName= accObject.Parent.EP_Puma_Company_Code__c;
	    EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);
	    String messageId=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
	    LIST<id> childIdList = new List<Id>(); 
	    childIdList.add(shipToAccount.Id);
	    Test.startTest();
	    LIST<EP_IntegrationRecord__c> result = localObj.generateIntegrationRecords(messageId,companyName,childIdList);
	    Test.stopTest();
	    System.AssertEquals(true,!result.isEmpty());
	}
	static testMethod void doCallOut_test() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    String endPoint =EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
	   	String reqMethod=EP_Common_Constant.POST;
	   	String requestXML=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
	   	//#60147 User Story Start
	   	String subscriptionKey=EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
	    EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, reqMethod, requestXML,subscriptionKey);
	    Integer numberOfRetries=2;
	    EP_CS_OutboundMessageSetting__c objOutboundCustomSetting =  EP_CS_OutboundMessageSetting__c.getInstance('SEND_ORDER_PRICING_REQUEST');
	    //#60147 User Story End
	    Test.startTest();
	    EP_IntegrationServiceResult result = EP_OutboundMessageService.doCallOut(intService,objOutboundCustomSetting);
	    Test.stopTest();
	    //System.AssertEquals(true,result!=null);
	    String actualValue = result.getResponse().getBody();
		String expectedValue = '{"message":"success"}';
		System.assertEquals(actualValue, expectedValue);
	}
	//#60147 User Story Start
	static testMethod void doEnqueueJobCallOut() {
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
	    Account shipToAccount =EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();  
	    Account accObject =[select id, Parent.Id,Parent.EP_Puma_Company_Code__c from Account where id=:shipToAccount.Id];
	    String messageType = EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    Id recordId = accObject.Parent.Id;
	    String companyName= accObject.Parent.EP_Puma_Company_Code__c;
	    EP_OutboundMessageService localObj = new EP_OutboundMessageService(recordId,messageType,companyName);     
	    String transactionType =EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION ;
	    LIST<id> childIdList = new List<Id>(); 
	    childIdList.add(shipToAccount.Id);
	    Test.startTest();
	    localObj.setEnqueueJob(true);
	    localObj.sendOutboundMessage(transactionType,childIdList);
	    Test.stopTest();
	    System.AssertEquals(true,localObj.transactionType==transactionType);
	}
	//#60147 User Story End

	//#59186 User Story Start
	static testMethod void isCommunicationDisabledNegativePositive_Test(){
		Test.startTest();
		EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
		Test.stopTest();
		System.assertEquals(EP_OutboundMessageService.isCommunicationDisabled(msgSetting),false);
		msgSetting.Disable_Communications__c = true;
		update msgSetting;
		System.assertEquals(EP_OutboundMessageService.isCommunicationDisabled(msgSetting),true);
		

	}

	static testMethod void doCallOutPostive_test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
		EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
		EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(intRecord.EP_Endpoint_URL__c, EP_Common_Constant.POST, intRecord.EP_XML_Message__c, '');
		EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
		EP_IntegrationServiceResult result = EP_OutboundMessageService.doCallOut(intService,msgSetting);
		Test.stopTest();
		System.assertEquals(result.getResponse().getStatusCode(),200);
		
	}

	static testMethod void doCallOutNegative_test() {
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
		EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
		EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(intRecord.EP_Endpoint_URL__c, EP_Common_Constant.POST, intRecord.EP_XML_Message__c, '');
		EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
		msgSetting.Disable_Communications__c = true;
		update msgSetting;
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
		EP_IntegrationServiceResult result = EP_OutboundMessageService.doCallOut(intService,msgSetting);
		Test.stopTest();
		System.assertEquals(result.getResponse().getStatusCode(),EP_Common_Constant.HTTP_ERROR_CODE_503);
	}

	static testMethod void setATRFields_test() {
		Test.startTest();
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
		EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
		EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
	    EP_OutboundMessageService objOMS = new EP_OutboundMessageService(obj.getOrder().Id,'SFDC_TO_NAV_ORDER_SYNC','AAF');
		objOMS.setATRFields(new List<EP_IntegrationRecord__c>{intRecord});
		System.assertEquals(intRecord.EP_Queuing_Retry__c,msgSetting.EP_Queuing_Retry__c);
		Test.stopTest();
	}

	static testMethod void performImmediateCalloutsPositive_Test() {
		
		EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
		EP_IntegrationRecord__c intRecord = EP_TestDataUtility.createIntegrationRecforATR(obj.getOrder().Id,null); 
		EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(intRecord.EP_Endpoint_URL__c, EP_Common_Constant.POST, intRecord.EP_XML_Message__c, '');
		EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
		EP_IntegrationServiceResult result = EP_OutboundMessageService.performImmediateCallouts(intService,msgSetting);
		Test.stopTest();
		System.assertEquals(result.getResponse().getStatusCode(),200);
		
	}

	static testMethod void performImmediateCalloutsNegative_Test() {
		Test.startTest();
		Boolean isNullPointerException = false;
		try {
			EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting('SFDC_TO_NAV_ORDER_SYNC');
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
			EP_IntegrationServiceResult result = EP_OutboundMessageService.performImmediateCallouts(null,msgSetting);
		}
		catch(NullPointerException exp){
			isNullPointerException = true;
		}
		System.assertEquals(isNullPointerException,true);
		Test.stopTest();
	}
	//#59186 User Story End
}