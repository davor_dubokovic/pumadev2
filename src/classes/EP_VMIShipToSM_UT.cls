@isTest
public class EP_VMIShipToSM_UT
{
	@testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }

	static testMethod void getAccountState_test() {
		EP_VMIShipToSM localObj = new EP_VMIShipToSM();
		EP_AccountDomainObject obj = new EP_AccountDomainObject(EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario());
		localobj.setAccountDomainObject(obj);
		EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
		localobj.accountEvent = oe;
		Test.startTest();
		EP_AccountState result = localObj.getAccountState();
		Test.stopTest();
		// **** IMPLEMENT ASSERT ~@~ *****
		Boolean instanceofresult = result instanceof EP_VMIShipToASAccountSetup;
    	System.assertEquals(true, instanceofresult);
	}
}