/*
*  @Author <Aravindhan Ramalingam>
*  @Name <EP_OSTVmiConsignPlannedToPlanned >
*  @CreateDate <03/02/2017>
*  @Description <Handles order status change for VMI Order status change to Planned>
*  @Version <1.0>
*/

public class EP_OSTVmiConsignPlannedToPlanned extends EP_OrderStateTransition{
    
    static string classname = 'EP_OSTVmiConsignPlannedToPlanned ';

    public EP_OSTVmiConsignPlannedToPlanned(){
        finalState =EP_OrderConstant.OrderState_Planned;
    }
    
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OSTVmiConsignPlannedToPlanned ','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OSTVmiConsignPlannedToPlanned ','isRegisteredForEvent');

        return super.isRegisteredForEvent();       
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OSTVmiConsignPlannedToPlanned ','isGuardCondition');
         if(!this.order.getOrder().EP_Sync_with_NAV__c){
            this.order.getOrder().EP_Order_Credit_Status__c = EP_Common_Constant.Blank;
         }else{
            this.order.getOrder().EP_Order_credit_status__c = EP_Common_constant.Credit_Okay;
         }
         
        return true;
    }
}