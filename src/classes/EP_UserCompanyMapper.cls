/*  
   @Author <Krishan Gopal>
   @name <EP_ActionShareUtility>
   @CreateDate <24/11/2017>
   @Description <This class is Helper class of action trigger>  
   @Novasuite Fix - Required Documentation 
   @Version <1.0>
*/
public with sharing class EP_UserCompanyMapper {

    private EP_User_CompanyDomainObject UserCompanyDomainObject;
    
    /**
    * @name         EP_UserCompanyMapper
    * @date         11/26/2017
    * @description  This is a  parametrized constructor for setting EP_User_CompanyDomainObject 
    * @param        EP_User_CompanyDomainObject
    * @return      	NA
    */
    public EP_UserCompanyMapper(EP_User_CompanyDomainObject UserCompanyDomainObject) {
    	EP_GeneralUtility.Log('Public','EP_UserCompanyMapper','EP_UserCompanyMapper');
        this.UserCompanyDomainObject = UserCompanyDomainObject;  
    }
    
     /**
    * @name         EP_UserCompanyMapper
    * @date         11/26/2017
    * @description  This is a non parametrized constructor 
    * @param        NA
    * @return      	NA
    */
    public EP_UserCompanyMapper(){
    	EP_GeneralUtility.Log('Public','EP_UserCompanyMapper','EP_UserCompanyMapper');
    }  
    
    /**This method is used to get Users associated with list of Companies
    *  @name            getAssociatedUserList
    *  @param           set<id> companylist
    *  @return          list<User>
    *  @throws          NA
    */
    public set<User> getAssociatedUserList(set<id> companylist){
    	EP_GeneralUtility.Log('Public','EP_UserCompanyMapper','getAssociatedUserList');
        set<User> userList = new set<User>();
        if(companylist.isempty())
            return userList;
        for(EP_User_Company__c User_Company :  [select User__c, User__r.profileid,User__r.id from EP_User_Company__c where Company__c in:companylist]){
            userList.add(User_Company.User__r);
        }
        return userList;
        
    }
    
}