/*
   @Author <Vinay Jaiswal>
   @name <EP_InvoiceTriggerHelper>
   @Description <This class handles requests from EP_InvoiceTriggerHandler class>
   @Version <1.0>
*/
public with sharing class EP_InvoiceTriggerHelper {

    private static final String AWAITING_PROCESSING = 'Awaiting Processing';
    private static final string CLASS_NAME = 'EP_InvoiceTriggerHelper';
    private static final string VALIDATE_INVOICE = 'validateInvoice';
    private static final string SET_ORDER_STATUS = 'setOrderStatus';
    
   /*
      This method is used to validate invoice for Prepayment
   */ 
   public static void validateInvoice(List<EP_Invoice__c> lNewInvoice){
       try{
           for (EP_Invoice__c oInvoice : lNewInvoice) {
                if(oInvoice.EP_Type__c <> NULL && oInvoice.EP_Type__c.equalsIgnoreCase(EP_Common_Constant.PREPAYMENT_INVOICE) ){
                    oInvoice.EP_Is_Proforma_Invoice__c =  true;
                    oInvoice.EP_Invoice_Type__c = EP_Common_Constant.Proforma;
                }
           } 
           }
       catch(Exception ex){
             EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, VALIDATE_INVOICE, 
                        CLASS_NAME,apexPages.severity.ERROR);
       }        
   }
}