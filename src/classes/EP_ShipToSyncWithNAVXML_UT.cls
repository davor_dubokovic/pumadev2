@isTest
public with sharing class EP_ShipToSyncWithNAVXML_UT {
    static testMethod void init_testPositive() {
    	EP_AccountDomainObject objAccDomain = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario();        
        EP_ShipToSyncWithNAVXML objLocal = new EP_ShipToSyncWithNAVXML();
        objLocal.recordId = objAccDomain.localAccount.Id;
        Test.startTest();
        objLocal.init();
        Test.stopTest();        
        System.assert(objLocal.objAccount!=null);         
    }
	static testMethod void createPayload_testPositive() {
    	EP_AccountDomainObject objAccDomain = EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_ShipToSyncWithNAVXML objLocal = new EP_ShipToSyncWithNAVXML();
        objLocal.recordId = objAccDomain.localAccount.Id;
        Test.startTest();
        objLocal.init();
        objLocal.isEncryptionEnabled = true;
        objLocal.createPayload();
        Test.stopTest();
        System.assertEquals( objAccDomain.localAccount.Id,objLocal.objAccount.Id);        
    }	
}