public virtual class EP_ProductSoldAs {

    public EP_ProductSoldAs() {
        
    }

     /** This method is shipTos
      *  @date      05/02/2017
      *  @name      getShipTos
      *  @param     Id accountId
      *  @return    List<Account> 
      *  @throws    NA
      */
public virtual List<Account> getShipTos(List<Account> lstAccount) {
      EP_GeneralUtility.Log('Public','EP_ProductSoldAs','getShipTos');

      return lstAccount;
    }
    
    
    /** This method is used to get Tanks for the Bulk Products in pricebook
      *  @date      05/02/2017
      *  @name      getTanksForBulkProduct
      *  @param     Map<Id,EP_Tank__c> mapOperationalTanks
      *  @return    Map<Id,EP_Tank__c> 
      *  @throws    NA
      */
    public virtual Map<Id,EP_Tank__c> getTanksForBulkProduct(Map<Id,EP_Tank__c> mapOperationalTanks){
        EP_GeneralUtility.Log('Public','EP_ProductSoldAs','getTanksForBulkProduct');
        return mapOperationalTanks;
    }
}