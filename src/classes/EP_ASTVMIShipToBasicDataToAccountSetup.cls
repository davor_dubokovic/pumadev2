/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToBasicDataToAccountSetup>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 02-Basic Data Setup to 04-Account Set-up>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToBasicDataToAccountSetup extends EP_AccountStateTransition {

    public EP_ASTVMIShipToBasicDataToAccountSetup() {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToAccountSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToAccountSetup','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToAccountSetup','isGuardCondition');
        //L4_45352_START
        EP_AccountService service =  new EP_AccountService(this.account);
        /*
          // #sprintRouteWork starts //
        if(!service.isRouteAllocationAvailable()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Route_Not_Allocated);
            return false;
        }
        // #sprintRouteWork ends //
        */
        //L4_45352_END
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToAccountSetup','doOnExit');

    }
}