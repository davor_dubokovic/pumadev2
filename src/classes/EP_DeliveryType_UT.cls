@isTest
public class EP_DeliveryType_UT
{

static testMethod void updateTransportService_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder(); // method check  
    objOrder.EP_Use_Managed_Transport_Services__c = 'TRUE';
    // **** TILL HERE ~@~ *****
    Test.startTest();
    localObj.updateTransportService(objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals('N/A',objOrder.EP_Use_Managed_Transport_Services__c);
    // **** TILL HERE ~@~ *****
}
static testMethod void getInventoryDetails_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    LIST<csord__Order_Line_Item__c> oiList = new List<csord__Order_Line_Item__c>();
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c newOrderRecord = EP_TestDataUtility.getCurrentOrder();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Map<Id,String> result = localObj.getInventoryDetails(oiList,newOrderRecord);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void getShipTos_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Id accountId = EP_TestDataUtility.getSellTo().id;

    // **** TILL HERE ~@~ *****
    Test.startTest();
    LIST<Account> result = localObj.getShipTos(accountId);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void getAvailableProducts_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
   
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder(); 
    Id storageLocId = objOrder.EP_Stock_Holding_Location__c;
    System.debug('test##'+objOrder.EP_Stock_Holding_Location__c);
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Set<Id> result = localObj.getAvailableProducts(storageLocId,objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void findPriceBookEntries_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder(); 
    Id pricebookId = objOrder.csord__Account__r.EP_PriceBook__c;
    System.debug('pricebookId##'+pricebookId);
    // **** TILL HERE ~@~ *****
    Test.startTest();
    LIST<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,objOrder);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void getOperationalTanks_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    String accountId = EP_TestDataUtility.getShipTo().Id;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Map<Id, EP_Tank__c> result = localObj.getOperationalTanks(accountId);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void getAllRoutesOfShipToAndLocation_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    String shipId = EP_TestDataUtility.getShipTO().Id;
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    String shlId = EP_TestDataUtility.getStorageLocation().Id;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    LIST<EP_Route__c> result = localObj.getAllRoutesOfShipToAndLocation(shipId,shlId);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertNotEquals(null,result);
    // **** TILL HERE ~@~ *****
}
static testMethod void showCustomerPO_PositiveScenariotest() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder();  
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.showCustomerPO(objOrder);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void showCustomerPO_NegativeScenariotest() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder();  
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.showCustomerPO(objOrder);
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void checkForNoTransporter_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder();  
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    String transporterName = EP_Common_Constant.NO_TRANSPORTER;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    csord__Order__c result = localObj.checkForNoTransporter(objOrder,transporterName);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(EP_Common_Constant.PIPELINE,result.EP_Delivery_Type__c);
    // **** TILL HERE ~@~ *****
}
static testMethod void isOrderEntryValid_PositiveScenariotest() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Integer numOfTransportAccount = 5;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isOrderEntryValid_NegativeScenariotest() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    Integer numOfTransportAccount = 5;
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void findRecordType_test() {
    EP_DeliveryType localObj = new EP_DeliveryType();
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_OrderEpoch epochType = new EP_OrderEpoch();
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_VendorManagement vmType = new EP_VendorManagement();
    // **** TILL HERE ~@~ *****
    // **** IMPLEMENT THIS SECTION ~@~ *****
    EP_ConsignmentType consignmentType = new EP_ConsignmentType();
    // **** TILL HERE ~@~ *****
    Test.startTest();
    Id result = localObj.findRecordType(epochType,vmType,consignmentType);
    Test.stopTest();
    // **** IMPLEMENT ASSERT ~@~ *****
    System.AssertEquals(null,result);
    // **** TILL HERE ~@~ *****
}
}