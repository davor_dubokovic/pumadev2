/*********************************************************************************************
            @Author <>
            @name <TQAppController >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/
global with sharing class TQUploadRequestItem {
    public String objectApiName {get; set;}
    public String Id {get; set;}
    public String record {get; set;}
    public String changeDate {get; set;}
    
    // Commenting the below constructor to remove the isssue from Nova Suite.
    /* public TQUploadRequestItem(){
    } */
/**************************************************************************************
      Constructor 
**********************************************************************************/    
    public TQUploadRequestItem(String objectApiName, String objectId, String record){
        this(objectApiName, objectId, record, null);
    }
/*********************************************************************************************
            @Author <>
            @name <TQAppController >
            @CreateDate <>
            @Description <>  
            @Version <1.0>
    *********************************************************************************************/        
    public TQUploadRequestItem(String objectApiName, String objectId, String record, String changeDate){
        this.objectApiName = objectApiName;
        this.Id = objectId;
        this.record = record;
        this.changeDate = changeDate;
    }
}