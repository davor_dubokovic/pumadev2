/**
*  @Author <Kamal Garg>
*  @Name <EP_ManageOrderConfirmationDocument>
*  @CreateDate <05/09/2016>
*  @Description <This is the apex class used to generate PDF for Order Confirmation record>
*  @Version <1.0>
*/
public with sharing class EP_ManageOrderConfirmationDocument{

    private static final String GENERATE_ATTACHMENT_FOR_ORDER_CONFIRMATION = 'generateAttachmentForOrderConfirmation';
    /**
    * @author <Kamal Garg>
    * @description <This method is used to generate document for Order Confirmation>
    * @name <generateAttachmentForOrderConfirmation>
    * @date <05/09/2016>
    * @param EP_ManageDocument.Documents
    * @return void
    */
    public static void generateAttachmentForOrderConfirmation(EP_ManageDocument.Documents documents){
    	EP_GeneralUtility.Log('Public',GENERATE_ATTACHMENT_FOR_ORDER_CONFIRMATION,'generateAttachmentForOrderConfirmation');
        Map<String, List<Attachment>> mapOrderNumberWithListOfAttachments = new Map<String, List<Attachment>>();
        //set containg 'OrderNumber' field values of 'Order' standard object received from Input JSON
        Set<String> setOrderNumbers = new Set<String>();
       	list<EP_AttachmentNotificationService.attachmentWrapper> attachmentWrapperList = new list <EP_AttachmentNotificationService.attachmentWrapper>();//L4 - #45419, #45420, #45421 - Notification
        
        try{    
            List<Attachment> listAttachments = null;
            for(EP_ManageDocument.Document documentObject : documents.document){
                EP_ManageOrderConfirmationDocument.OrderConfirmationWrapper orderConfirmationWrapper = EP_DocumentUtil.fillOrderConfirmationWrapper(documentObject.documentMetaData.metaDatas);
                setOrderNumbers.add(orderConfirmationWrapper.orderId);
                // "generateAttachment" api invoked from "EP_DocumentUtil" apex class to get "Attachment" record
                Attachment attachmentObject = EP_DocumentUtil.generateAttachment(documentObject);
                //L4 - #45419, #45420, #45421 - Start
		        setAttachmentName(attachmentObject,orderConfirmationWrapper.docTitle);
                //L4 - #45419, #45420, #45421 - End
                // populating map containing list of Attachments as values against "OrderNumber" as key
                if(!(mapOrderNumberWithListOfAttachments.containsKey(orderConfirmationWrapper.orderId))){
                    listAttachments = new List<Attachment>();
                    mapOrderNumberWithListOfAttachments.put(orderConfirmationWrapper.orderId, listAttachments);
                }
                mapOrderNumberWithListOfAttachments.get(orderConfirmationWrapper.orderId).add(attachmentObject);
            }
            // "getOrderNumberIdMap" api invoked from "EP_DocumentUtil" apex class to get map containing "OrderNumber" as key and "Order Id" as value 
            Map<String, csord__Order__c> mapOrderNumberWithOrder = EP_DocumentUtil.getOrderNumberIdMap(setOrderNumbers);
            List<Attachment> listOfAttachmentsToBeInserted = new List<Attachment>();
            //L4 - #45419, #45420, #45421 - Start
            string attchmentName;
            for(String key : mapOrderNumberWithListOfAttachments.keySet()){
                for(Attachment attachmentObject : mapOrderNumberWithListOfAttachments.get(key)){
                    attachmentObject.ParentId = mapOrderNumberWithOrder.get(key).Id;
                    listOfAttachmentsToBeInserted.add(attachmentObject);
					//defect 78905 start csord__Account__c added
                    attachmentWrapperList.add(new EP_AttachmentNotificationService.attachmentWrapper(attachmentObject , mapOrderNumberWithOrder.get(key).csord__Account__c, getDocTitle(attachmentObject.Name)));
                	
                }
            }
           
            //To iterate over the list of attachments and delete existing attachments record corresponding to parent id associtated with each attachment in iteration
            EP_DocumentUtil.deleteAttachmentsByName(listOfAttachmentsToBeInserted);
            //L4 - #45419, #45420, #45421 - End
            // To perform Insert DML opeartion for list of Attachment's records
            Database.SaveResult[] results = Database.insert(listOfAttachmentsToBeInserted, false);
            for(Integer i = 0; i < results.size(); i++){
                if(results[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    EP_ManageDocument.successRecIdMap.put(listOfAttachmentsToBeInserted[i].Name, results[i].getId());
                }else{
                    // Operation failed, so get all errors
                    for(Database.Error error : results[i].getErrors()){
                        EP_ManageDocument.errDesMap.put(listOfAttachmentsToBeInserted[i].Name, error.getMessage());
                        EP_ManageDocument.errCodeMap.put(listOfAttachmentsToBeInserted[i].Name, String.valueOf(error.getStatusCode()));
                    }
                }
            }
            EP_AttachmentNotificationService.sendAttachmentNotification(attachmentWrapperList);//L4 - #45419, #45420, #45421 - Notification
        }
        catch(Exception e){
            EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, GENERATE_ATTACHMENT_FOR_ORDER_CONFIRMATION , EP_ManageOrderConfirmationDocument.Class.getName(), apexPages.severity.ERROR);
            throw e;
        }
    }
    
    /**
    *  @author <Kamal Garg>
    *  @name <OrderConfirmationWrapper>
    *  @createDate <05/09/2016>
    *  @description <This is the wrapper class containing 'Order Confirmation' record information>
    *  @version <1.0>
    */
    public with sharing class OrderConfirmationWrapper{
        public String orderId;
        public String shipTo;
        public String sellTo;
        public String docDate;
        public String customerName;
        public String issuedFromId;
        public String docTitle;//L4 - #45419, #45420, #45421
    }
    
	//L4 - #45419, #45420, #45421 - Start
    /**
    *  @author <Accenture>
    *  @name <setAttachmentName>
    *  @createDate <18/01/2018>
    *  @description <This is class to set the Attachment Name from DocTitle property of JSON message>
    *  @version <1.0>
    */
    @TestVisible
    private static void setAttachmentName(Attachment attachmentObject, string docTitle) {
    	if(string.isNotEmpty(docTitle)) {
        	attachmentObject.Name = docTitle+getFileExtension(attachmentObject.Name);
        }
    }
   /**
    *  @author <Accenture>
    *  @name <getDocTitle>
    *  @createDate <18/01/2018>
    *  @description <This is class to get the DocTitle from the Attachment Name>
    *  @version <1.0>
    */
    @TestVisible
    private static string getDocTitle(string attchmentName) {
        List<String> docName = attchmentName.split(EP_Common_Constant.SPLIT);
        return  docName[0];
    }
     /**
    *  @author <Accenture>
    *  @name <getFileExtension>
    *  @createDate <18/01/2018>
    *  @description <This is class to get the FileExtension from the Attachment Name>
    *  @version <1.0>
    */
    @TestVisible
    private static string getFileExtension(string attachmentName) {
    	string fileExt;
    	List<String> docName = attachmentName.split(EP_Common_Constant.SPLIT);
        if(docName.size() > 1 ) {
        	fileExt =  EP_Common_Constant.DOT+docName[1];
        }
        return fileExt;
    }
    //L4 - #45419, #45420, #45421 - End
}