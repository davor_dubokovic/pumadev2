/* 
   @Author      :Accenture
   @name        :EP_PortalOrderPageController
   @CreateDate  :12/01/2015
   @Description :This class is used as a controller class of Portal Order 
   @Version     :<1.1>
*/
  
public without sharing class EP_PortalOrderPageController {

    public static final String CLASSNAME = 'EP_PortalOrderPageController';
    public String strCreditStatus = EP_Common_Constant.BLANK;
    public EP_OrderPageContext ctx {get;set;}
    public EP_OrderControllerHelper hlp;
    public EP_OrderLinesAndSummaryHelper hlpstep3And4;

    public EP_PortalOrderPageController() {
       init();
    }

    @TestVisible
    private void init(){
        if (Apexpages.currentpage().getparameters().get(EP_Common_Constant.ID) != NULL) {
            String localAccountID = Apexpages.currentpage().getparameters().get(EP_Common_Constant.ID);
            ctx = new EP_OrderPageContext(localAccountID);
            hlp = new EP_OrderControllerHelper(ctx);
            hlpstep3And4 = new EP_OrderLinesAndSummaryHelper(ctx);
            ctx.strSelectedAccountID = localAccountID;
        }
    }

   /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns supplylocations related to the Epuma Company of Order's Account in Selectoptions List
    * @param        :NA
    * @return       :List<SelectOption>
    */                                        
    public List<SelectOption> listSupplyLocPricingOptions{ 
        get{
            return ctx.getSupplyLocPricing();            
        }
        set;
    }

    /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns TransportPricing  related to the Epuma Company of Order's Account in Selectoptions List 
    * @param        :NA
    * @return       :List<SelectOption>
    */ 
    public List<SelectOption> listTransportPricingOptions{ 
        get{  
            return ctx.getTransporterPricingList();
        }
        set;
    }

    /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns applicable Payment terms for the order
    * @param        :NA
    * @return       :List<SelectOption>
    */ 
    public List < SelectOption > listPaymentTermsOptions {        
        get {
           return ctx.listPaymentTermsOptions;
        }
        set;
    }
 
    /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns applicable Delivery Type for the order
    * @param        :NA
    * @return       :List<SelectOption>
    */ 
    public List < SelectOption > listDeliveryTypeSelectOptions {
        get {
            return ctx.listDeliveryTypeSelectOptions;
        }
        set;
    }


    /** returns true if Page has any Errors
    *  @date      07/03/2017
    *  @name      isErrorInPage
    *  @param     NA
    *  @return    Boolean
    *  @throws    NA
    */ 
    public Boolean isErrorInPage {
        get {
            return ctx.isErrorInPage;
        }
    }

    /** returns true if PrePayment Customer-- We are not using it in the Page
    *  @date      07/03/2017
    *  @name      isPrepaymentCustomer
    *  @param     NA
    *  @return    Boolean
    *  @throws    NA
    */ 
    public Boolean isPrepaymentCustomer {
        get {
            return ctx.isPrepaymentCustomer;
        }
    }

    /** returns true if Order is Delivery
    *  @date      07/03/2017
    *  @name      isDeliveryOrder
    *  @param     NA
    *  @return    Boolean
    *  @throws    NA
    */ 
    public Boolean isDeliveryOrder {
        get {
            return ctx.isDeliveryOrder;
        }
    }

    public Boolean showAddNewLineButton {
        get {
                return (ctx.isOprtnlTankAvail && hlpstep3And4.getListShipToTanks().size() > 1) || (!ctx.isOprtnlTankAvail && hlpstep3And4.getProductOptions().size() > 1);
        }
        set; 
    }

    public Boolean showNextButton {
        get {
        	Boolean proceed = ((ctx.intOrderCreationStepIndex > 0 && ctx.intOrderCreationStepIndex < 4) ||( ctx.intOrderCreationStepIndex ==4 && ctx.isOrderEntryValid && (ctx.isConsignmentOrder || ctx.isDummy) && !(ctx.isConsumptionOrder))) ;
            return (proceed && !ctx.hasAccountSetupError && !isErrorInPage);
        } 
        set;
    } 

    public Boolean showSpinButton{
        get {
            Boolean proceed = (ctx.intOrderCreationStepIndex == 4 && ctx.isPriceCalculated && (!(ctx.isConsignmentOrder || ctx.isDummy)||ctx.isConsumptionOrder));
            return (proceed && !ctx.hasAccountSetupError && !isErrorInPage);
        }
        set;
    } 

    public Boolean showBackButton {
        get {
            return (ctx.intOrderCreationStepIndex > 1 && ctx.intOrderCreationStepIndex < 6);
        }
        set;
    }   

    public Boolean showCalculateButton  {
        get {
        	return (ctx.intOrderCreationStepIndex == 4  && ctx.isOrderEntryValid && !ctx.isPriceCalculated && (!(ctx.isConsignmentOrder || ctx.isDummy) || ctx.isConsumptionOrder)); // added ctx.isConsumptionOrder in CCO Work
        }
        set;
    }

    public Boolean showCancelButton {
        get {
            return (ctx.intOrderCreationStepIndex < 6);
        }
        set;
    }

    public Double totalOrderCost {
        get {
           return ctx.totalOrderCost;
        }
    }

    public List < EP_OrderPageContext.OrderSummaryWrapper > orderSummaryItems {
        get {
            return hlp.getOrderSummaryItems();
        }
    }

    
    public void getPricing(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','getPricing');
        hlp.getPricing();
    }

    public void getSlots(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','getSlots');
        ctx.isPriceCalculated =  False;
        showCalculateButton = false;
    }

    public void setAccountOrderDetails() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','setAccountOrderDetails');
        try{
            if (String.isNotBlank(ctx.strSelectedAccountID)) {
                ctx.onLoadActionForNewOrder();
        		addNewLine();
            }
        }
        catch( Exception Ex ){
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setAccountOrderDetails', CLASSNAME, ApexPages.Severity.ERROR);
        }                  
    }   

     public void setShipToTank(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','setShipToTank');
        try{
            ctx.mapShipToOperationalTanks = ctx.orderService.getOperationalTanks( ctx.strSelectedShipToID );
            //system.debug('*****'+ctx.mapShipToOperationalTanks.size()+'**********:-'+ctx.mapShipToOperationalTanks);
        }
        catch( Exception Ex ){
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setShipToTank', CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
   

    public void doDeleteActions(Id OrderId) {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doDeleteActions');
        try{
            // Query Goes in Mapper-- method "doActionDelete"
            ctx.orderDomainObj.deleteOrderLineItems();
        }
        catch( Exception Ex ){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doDeleteActions', CLASSNAME, ApexPages.Severity.ERROR);
        }
    }

    /*
    *This method is used to take one step back in the wizard.
    */
    public void doActionBack() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doActionBack');
        // Move 1 step back
        try {
                ctx.hasAccountSetupError = false; // #defectFix 57019
            ctx.setWizardIndexonBack(); 
            
            // Reset any errors that occurred during the previous step
           // ctx.hasAccountSetupError = true;
        } 
        catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBack', CLASSNAME, ApexPages.Severity.ERROR);
        }
    }

    /*
    This method is used to go one step ahead.
    */
    public PageReference doActionNext() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doActionNext');
        try {

            ctx.orderDomainObj.setOrder(ctx.newOrderRecord);
            //ctx.orderService = new EP_OrderService(ctx.orderDomainObj);  

            if (ctx.intOrderCreationStepIndex == 1) {
                hlp.loadStep1();
            }
            // Step #2 - Select Ship-To
            if (ctx.intOrderCreationStepIndex == 2) {
                hlp.loadStep2();
            } 
            // Step #3 - Select Supply Location
            if (ctx.intOrderCreationStepIndex == 3) {
                hlpstep3And4.loadStep3();                
            }
            // Step #4 - Enter Order Details
            if (ctx.intOrderCreationStepIndex == 4) {
                hlpstep3And4.loadStep4();
            } 
            // Move one step ahead
            ctx.intOrderCreationStepIndex++;
            //system.debug('ctx.intOrderCreationStepIndex++;'+ ctx.intOrderCreationStepIndex);
        }catch (Exception ex) {
            //system.debug('******Exception:-'+ex.getMessage()+'****'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,ex.getMessage()+'***'+ex.getStackTraceString()));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionNext', CLASSNAME, ApexPages.Severity.ERROR);
        }
        return null;
    }
       /*
    Method to set Run on order
    */    
    public void setRun() {
        try
        {
            EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','setRun');
            ctx.setRun();
        }catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setRun', CLASSNAME, ApexPages.Severity.ERROR);
        }
    } 


    /*
    This method is used remove order line item.
    */
     public void removeOrderLineItem() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','removeOrderLineItem');
        try {
            hlp.removeOrderLineItem();
            ctx.isPriceCalculated = false;
            hlpstep3And4.updateOrderItemLinesfornewOrder();
            
         // End selected order line item check
        } catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'removeOrderLineItem', CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
        
    
    /*
    This method is used to add new order line item.
    */
    public void addNewLine() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','addNewLine');
        try {
        	hlpstep3And4.addNewLine();
        } catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'addNewLine', CLASSNAME, ApexPages.Severity.ERROR);
        }
    }

    /*
    This method is used to cancel the order
    */
    public PageReference doCancel() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doCancel');
        PageReference ref ;
        try {
            ref = hlp.doCancel();
        } catch (Exception ex) {
            ref = null;
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doCancel', CLASSNAME, ApexPages.Severity.ERROR);
        }
        return ref;
    }
    /*
    This method is used to cancel the order when available funds is not available
    */
    public PageReference doCancelOrder() {
     	pagereference ref;
     	try {
        	EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doCancelOrder');
        	ref = hlp.setOrderStatustoCancelled();
        } catch (Exception ex) {
                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
                EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.CANCEL_ORD_MTD, CLASSNAME, ApexPages.Severity.ERROR);
            }
        return ref;
    }
 
    /*
    This method is used to submit the order.
    */
    public PageReference doSubmit() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doSubmit');
        ctx.deliveryType = ctx.strSelectedDeliveryType;
        PageReference ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);
        SavePoint sp = Database.setSavepoint();
        List <Database.SaveResult> results = new List <Database.SaveResult>();
        try {
            hlp.setOrderDetails();  
            ctx.orderDomainObj.setRetroOrderDetails();                      
            hlp.doSubmitActionforNewOrder();
            
            hlp.setOrderItemsinDomain();           
            EP_OrderEvent orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
            ctx.orderService.setOrderStatus(orderEvent);
            ctx.newOrderRecord.Status__c = ctx.orderDomainObj.getStatus();
            ref = hlp.updateOrder(orderEvent);
            ctx.orderService.doPostStatusChangeActions();
        }catch (Exception ex) {
            Database.rollback(sp);            
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, ex.getMessage() + EP_Common_Constant.ERR_OCCURED));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.SUBMIT_MTD, CLASSNAME, ApexPages.Severity.ERROR);
            return null;
        }
        return ref.setRedirect(false);
    }
   /*
    This Method is Used to save order with draft status
    */
    public PageReference saveOrderAsDraft(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','saveOrderAsDraft');
        return hlp.saveOrderAsDraft();
    }
    
    /* Needs to be removed as the current code will never work becuase ctx.intOliIndex will always be null
    /*
    Method to update Supplier Contracts
    */    
    public void doUpdateSupplierContracts() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doUpdateSupplierContracts');
        try {
        	hlp.doUpdateSupplierContracts();    
         
        } catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doUpdateSupplierContracts', CLASSNAME, ApexPages.Severity.ERROR);
        }
        
    }
    /* 
    CCO Work
    */
    public void setDataForConsumptionOrder() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','setDataForConsumptionOrder');
        try{ 
             ctx.setDataForConsumptionOrder(); 
        }catch(Exception ex){
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.SET_DATA_FOR_CONSUM_ORDER, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }   
    
    /*
    Calculate Price
    */
    public void doCalculatePrice() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','doCalculatePrice');
        try {
            PageReference ref = NULL;
            hlp.doCalculatePrice();
        }catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doCalculatePrice', CLASSNAME, ApexPages.Severity.ERROR);
        }         
    }
  
    /*
    Validate Operational Tanks
    */
    public void validateOperationalTanks(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','validateOperationalTanks');
        try {
        	ctx.mapShipToTanks = ctx.orderService.getTanks( ctx.strSelectedShipToID );
            ctx.mapShipToOperationalTanks = EP_PortalOrderUtil.getOperationalTanks( ctx.mapShipToTanks.values() );
            System.debug(ctx.isBulkOrder+'**'+!ctx.mapShipToTanks.isEmpty()+'***'+ctx.mapShipToOperationalTanks.isEmpty()+'***'+!ctx.strSelectedDeliveryType.equalsIgnoreCase(EP_Common_Constant.EX_RACK));
	        if(ctx.isBulkOrder && !ctx.mapShipToTanks.isEmpty() && ctx.mapShipToOperationalTanks.isEmpty()  && !ctx.strSelectedDeliveryType.equalsIgnoreCase(EP_Common_Constant.EX_RACK)) { 
	            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_No_Operational_Tanks)); // Tank Work
	            ctx.intOrderCreationStepIndex = 2;
	            return; 
	        }    
         // End selected order line item check
        } catch (Exception ex) {
            // apexPages.addMessages(e);
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'removeOrderLineItem', CLASSNAME, ApexPages.Severity.ERROR);
        }
        
    }

    /*
    Need Pricing
    */
    public void needPricing(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','needPricing');
        ctx.isPriceCalculated = false;
    }   


    /*
    Display Product name and safe fill level on selection of tank
    */
    public void displayProductNameAndSafeFillLevel() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','displayProductNameAndSafeFillLevel');
        try {
        	ctx.setProductNameandTankDetails();    
        }catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'displayProductNameAndSafeFillLevel', CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    /*
    This method is used to perform validations on order line item.
    */
    public void updateOrderItemLines() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderPageController','updateOrderItemLines');
        try {
        	ctx.isPriceCalculated = false;
        	hlpstep3And4.updateOrderItemLinesfornewOrder();
         } catch (Exception ex) {
            //system.debug('*****Exception:-'+ex.getMessage()+'******'+ex.getStackTraceString());
           ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'updateOrderItemLines', CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
}