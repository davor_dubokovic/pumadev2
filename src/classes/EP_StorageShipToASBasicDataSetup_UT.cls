@isTest
public class EP_StorageShipToASBasicDataSetup_UT
{
	static final string EVENT_NAME = '02-BasicDataSetupTo02-BasicDataSetup';

	static testMethod void setAccountDomainObject_test() {
		EP_StorageShipToASBasicDataSetup localObj = new EP_StorageShipToASBasicDataSetup();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASBasicDataSetupDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.setAccountDomainObject(obj);
		Test.stopTest();
		system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
	}
	//Method has no implementation, hence adding dummy assert  
	static testMethod void doOnEntry_test() {
		EP_StorageShipToASBasicDataSetup localObj = new EP_StorageShipToASBasicDataSetup();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASBasicDataSetupDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnEntry();
		Test.stopTest();
		System.assertEquals(true, true);
	}
	//Method has no implementation, hence adding dummy assert  
	static testMethod void doOnExit_test() {
		EP_StorageShipToASBasicDataSetup localObj = new EP_StorageShipToASBasicDataSetup();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASBasicDataSetupDomainObject();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		localObj.doOnExit();
		Test.stopTest();
		System.assertEquals(true, true);
	}
	static testMethod void doTransition_PositiveScenariotest() {
		List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
		List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
		EP_StorageShipToASBasicDataSetup localObj = new EP_StorageShipToASBasicDataSetup();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASBasicDataSetupDomainObjectPositiveScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.doTransition();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
	static testMethod void doTransition_NegativeScenariotest() {
		EP_StorageShipToASBasicDataSetup localObj = new EP_StorageShipToASBasicDataSetup();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASBasicDataSetupDomainObjectNegativeScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		string errormsg = null;
		try{
			Boolean result = localObj.doTransition();
		}catch (exception e){
			errormsg = e.getmessage();
		}
		Test.stopTest();
		System.AssertEquals(true,errormsg != null);
	}
	//No negative scenario as the supper class always returns false; 
	static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
		EP_StorageShipToASBasicDataSetup localObj = new EP_StorageShipToASBasicDataSetup();
		EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASBasicDataSetupDomainObjectPositiveScenario();
		EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
		localObj.setAccountContext(obj,oe);
		Test.startTest();
		Boolean result = localObj.isInboundTransitionPossible();
		Test.stopTest();
		System.AssertEquals(true,result);
	}
}