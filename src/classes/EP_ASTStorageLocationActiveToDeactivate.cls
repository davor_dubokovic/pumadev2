/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationActiveToDeactivate>
*  @CreateDate <20/12/2017>
*  @Description <Handles Storage Ship To Account status change from 05-Active to 06-Deactivate >
*  @Version <1.0>
*/
public class EP_ASTStorageLocationActiveToDeactivate extends EP_AccountStateTransition {
	
	/**
    * @author           Accenture
    * @name             EP_ASTStorageLocationActiveToDeactivate
    * @date             20/12/2017
    * @description      Constructor
    * @param            NA
    * @return           NA 
    */
    public EP_ASTStorageLocationActiveToDeactivate() { 
        // start Defect 80359
        finalState = EP_AccountConstant.DEACTIVATE;
        // End Defect 80359
    }
    
    /**
    * @author           Accenture
    * @name             isTransitionPossible
    * @date             20/12/2017
    * @description      Check wether transitionposoble or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToDeactivate','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    /**
    * @author           Accenture
    * @name             isRegisteredForEvent
    * @date             20/12/2017
    * @description      Check wether Registered For Event or not
    * @param            NA
    * @return           boolean 
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToDeactivate', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    /**
    * @author           Accenture
    * @name             isGuardCondition
    * @date             20/12/2017
    * @description      Check all validation
    * @param            NA
    * @return           boolean 
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationActiveToDeactivate',' isGuardCondition');        
        return true;
    }
}