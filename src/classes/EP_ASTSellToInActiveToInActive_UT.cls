@isTest
public class EP_ASTSellToInActiveToInActive_UT
{

   static final string EVENT_NAME = '07-InactiveTo07-Inactive';
   static final string INVALID_EVENT_NAME = '08-RejectedTo05-Active';
   
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
   
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectPositiveScenario();
        obj.localAccount.EP_Status__c = EP_AccountConstant.INACTIVE;
        update obj.localAccount; 
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToInActiveToInActive ast = new EP_ASTSellToInActiveToInActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectNegativeScenario();
        obj.localAccount.EP_Status__c = EP_AccountConstant.INACTIVE;
        update obj.localAccount; 
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTSellToInActiveToInActive ast = new EP_ASTSellToInActiveToInActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }


    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectPositiveScenario();
        obj.localAccount.EP_Status__c = EP_AccountConstant.INACTIVE;
        update obj.localAccount; 
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToInActiveToInActive ast = new EP_ASTSellToInActiveToInActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectNegativeScenario();
        obj.localAccount.EP_Status__c = EP_AccountConstant.INACTIVE;
        update obj.localAccount; 
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTSellToInActiveToInActive ast = new EP_ASTSellToInActiveToInActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASActiveDomainObjectPositiveScenario();
        obj.localAccount.EP_Status__c = EP_AccountConstant.INACTIVE;
        update obj.localAccount; 
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTSellToInActiveToInActive ast = new EP_ASTSellToInActiveToInActive();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
       
    
    }