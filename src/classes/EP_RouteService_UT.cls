@isTest
public class EP_RouteService_UT{

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
    public static final String ROUTE_INACTIVE = 'Inactive';
    
    static testMethod void EP_RouteService_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        Test.startTest();
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        Test.stopTest();
        System.assert(routeService != NULL);
   }
   
   static testMethod void isRouteNameEditable_Positive_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        Test.startTest();
        Set<Id> result = routeService.isRouteNameEditable();
        Test.stopTest();
        System.assert(result != NULL);
   }
   
    static testMethod void isRouteNameEditable_Negative_test() { 
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c cloneRoute = routeRecords.values()[0].clone(true,true);
        cloneRoute.EP_Route_Name__c = 'test'+cloneRoute.EP_Route_Name__c;
        Map<Id,EP_Route__c> clonedRouteRecords = new  Map<Id,EP_Route__c>{cloneRoute.Id=>cloneRoute};
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,clonedRouteRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        List<EP_Route__c> routeList = new List<EP_Route__C>();
        for(EP_Route__c route : routeService.newRouteMap.values()){
            routeList.add(route);
        }
        Order ord = [SELECT Id,EP_Route__c,Status FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Route__c = routeList[0].id;
        update ord;      
       
        Test.startTest();
        Set<Id> result = routeService.isRouteNameEditable();
        Test.stopTest();
        System.assert(result.size() > 0);
   }
   
   static testMethod void isRouteCodeEditable_Positive_test() {
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,routeRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        Test.startTest();
        Set<Id> result = routeService.isRouteCodeEditable();
        Test.stopTest();
        System.assert(result != NULL);
   }
   
    static testMethod void isRouteCodeEditable_Negative_test() { // TODO
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c cloneRoute = routeRecords.values()[0].clone(true,true);
        cloneRoute.EP_Route_Code__c = 'test'+cloneRoute.EP_Route_Code__c;
        Map<Id,EP_Route__c> clonedRouteRecords = new  Map<Id,EP_Route__c>{cloneRoute.Id=>cloneRoute};
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,clonedRouteRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        
        List<EP_Route__c> routeList = new List<EP_Route__C>();
        for(EP_Route__c route : routeService.newRouteMap.values()){
            routeList.add(route);
        }
        Order ord = [SELECT Id,EP_Route__c,Status FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Route__c = routeList[0].id;
        update ord;  
        
        Test.startTest();
        Set<Id> result = routeService.isRouteCodeEditable();
        Test.stopTest();
        System.assert(result.size() > 0);
   }
   
   
    static testMethod void isRouteDeletable_Negative_test() { 
        List<EP_Run__c> runRecords = EP_TestDataUtility.getRunObjects();
        
        Order ord = [SELECT Id,EP_Route__c,EP_Run__c,Status FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.Status = EP_Common_Constant.SUBMITTED;
        ord.EP_Route__c = runRecords[0].EP_Route__c;
        ord.EP_Run__c = runRecords[0].Id;
        update ord; 
        
        Order ord2 = [SELECT Id,EP_Route__c,EP_Run__c,Status FROM Order LIMIT : EP_Common_Constant.ONE];
        System.debug('route##'+ord2.EP_Route__c+'##run##'+ord2.EP_Run__c);
        Map<Id,EP_Route__c> routeRec = new Map<Id,EP_Route__c>{runRecords[0].EP_Route__c => runRecords[0].EP_Route__r};
        
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRec);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
 
        Test.startTest();
        //Map<id,boolean> result = routeService.isRouteDeletable();
        Test.stopTest();
       // System.assert(false,result.get(routeRec.values()[0].Id));
   }
   
   static testMethod void isRouteDeletable_Positive_test() { 
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        Test.startTest();
        Set<Id> result = routeService.isRouteDeletable();
        Test.stopTest();
        System.assert(result != NULL);
   }
   
   static testMethod void doActionBeforeUpdate_test() { // TODO
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c cloneRoute = routeRecords.values()[0].clone(true,true);
        cloneRoute.EP_Route_Code__c = 'test'+cloneRoute.EP_Route_Code__c;
        cloneRoute.EP_Route_Name__c = 'test'+cloneRoute.EP_Route_Name__c;
        cloneRoute.EP_Status__c = ROUTE_INACTIVE;
        Map<Id,EP_Route__c> clonedRouteRecords = new  Map<Id,EP_Route__c>{cloneRoute.Id=>cloneRoute};
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,clonedRouteRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        
        List<EP_Route__c> routeList = new List<EP_Route__C>();
        for(EP_Route__c route : routeService.newRouteMap.values()){
            routeList.add(route);
        }
        Order ord = [SELECT Id,EP_Route__c,Status FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Route__c = routeList[0].id;
        update ord;  
        
        Test.startTest();
        routeService.doActionBeforeUpdate();
        Test.stopTest();
        // This method does not need assert since its return type is void
        System.assert(true);
   }

   static testMethod void doActionBeforeDelete_test() { 
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(5);
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        Test.startTest();
        routeService.doActionBeforeDelete();
        Test.stopTest();
        
         // This method does not need assert since its return type is void
        System.assert(true);
   }
   
   static testMethod void throwErrorRouteNameEditable_test() { 
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c cloneRoute = routeRecords.values()[0].clone(true,true);
        cloneRoute.EP_Route_Name__c = 'test'+cloneRoute.EP_Route_Name__c;
        Map<Id,EP_Route__c> clonedRouteRecords = new  Map<Id,EP_Route__c>{cloneRoute.Id=>cloneRoute};
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,clonedRouteRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        List<EP_Route__c> routeList = new List<EP_Route__C>();
        for(EP_Route__c route : routeService.newRouteMap.values()){
            routeList.add(route);
        }
        Order ord = [SELECT Id,EP_Route__c,Status FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Route__c = routeList[0].id;
        update ord;      
       
       Set<Id> isRouteNameEditableResult = routeService.isRouteNameEditable();
        Test.startTest();
        routeService.throwErrorRouteNameEditable(isRouteNameEditableResult,cloneRoute);
        Test.stopTest();
        
         // This method does not need assert since its return type is void
        System.assert(true);
   }
   
   static testMethod void throwErrorRouteCodeEditable_test() { 
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c cloneRoute = routeRecords.values()[0].clone(true,true);
        cloneRoute.EP_Route_Code__c = 'test'+cloneRoute.EP_Route_Code__c;
        Map<Id,EP_Route__c> clonedRouteRecords = new  Map<Id,EP_Route__c>{cloneRoute.Id=>cloneRoute};
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,clonedRouteRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        List<EP_Route__c> routeList = new List<EP_Route__C>();
        for(EP_Route__c route : routeService.newRouteMap.values()){
            routeList.add(route);
        }
        Order ord = [SELECT Id,EP_Route__c,Status FROM Order LIMIT : EP_Common_Constant.ONE];
        ord.EP_Route__c = routeList[0].id;
        update ord;      
       
       Set<Id> isRouteCodeEditableResult = routeService.isRouteCodeEditable();
        Test.startTest();
        routeService.throwErrorRouteCodeEditable(isRouteCodeEditableResult,cloneRoute);
        Test.stopTest();
        
         // This method does not need assert since its return type is void
        System.assert(true);
   }
   
   static testMethod void throwErrorcanInactiveDefaultRoute_test() { 
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        EP_Route__c cloneRoute = routeRecords.values()[0].clone(true,true);
        cloneRoute.EP_Status__c = ROUTE_INACTIVE;
        Map<Id,EP_Route__c> clonedRouteRecords = new  Map<Id,EP_Route__c>{cloneRoute.Id=>cloneRoute};
        EP_RouteDomainObject routeDomObj = new EP_RouteDomainObject(routeRecords,clonedRouteRecords);
        EP_RouteService routeService = new EP_RouteService(routeDomObj);
        List<EP_Route__c> routeList = new List<EP_Route__C>();
        for(EP_Route__c route : routeService.newRouteMap.values()){
            routeList.add(route);
        }
        Order ord = [SELECT Id,EP_ShipTo__r.Id FROM Order LIMIT : EP_Common_Constant.ONE];
        EP_AccountMapper accMapper = new EP_AccountMapper();
        Account acc = accMapper.getAccountRecord(ord.EP_ShipTo__r.Id);  
        EP_Route__c route = routeList[0];
        route.EP_Company__c = acc.EP_Puma_Company__c;
        update route;
        acc.EP_Default_Route__c = routeList[0].id;
        update acc;         
       
       Set<Id> isRouteStatusEditableResult = routeService.canInactiveDefaultRoute();
        Test.startTest();
        routeService.throwErrorcanInactiveDefaultRoute(isRouteStatusEditableResult,cloneRoute);
        Test.stopTest();
        
         // This method does not need assert since its return type is void
        System.assert(true);
   }
   
   
   
   
   
   }