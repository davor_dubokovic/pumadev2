/* 
   @Author Accenture
   @name <EP_ExceptionLog_BatchClass>
   @CreateDate <20/11/2015>
   @Description <this is the batch class for deleting EP_Exception_Log__c records older than 30 days>
   @Version <1.0>
 
*/
global class EP_ExceptionLog_BatchClass implements Database.Batchable<sObject>{

    private static final String QUERY_STRING = 'Select id, CreatedDate from EP_Exception_Log__c where CreatedDate <= LAST_N_DAYS:30';
    private static final String EXECUTE_STRING = 'execute';
    private static final String EXCEPTIONLOG_BATCHCLASS_STRING = 'EP_ExceptionLog_BatchClass';
        
/*
*************************************************************************
*@Description : This is the Start method of BATCH class wherein the       *
                records older than 30 days are fetched.                   *
*@Params      : none                                                      *
*@Return      : Database.getQueryLocator()                                *    
*************************************************************************
*/ 
        global Database.QueryLocator start(Database.BatchableContext BC){
            
            return Database.getQueryLocator(QUERY_STRING);

        }
/*
*************************************************************************
*@Description : This is the Execute method of BATCH class wherein the     *
                delete operation is being performed                       *
*@Params      : none                                                      *
*@Return      : void                                                      *    
*************************************************************************
*/
       global void execute(Database.BatchableContext BC, List<EP_Exception_Log__c> scope){

              if(scope.size() >0){
        try{
            delete scope;
         }
         
    catch(Exception e){
             EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, EXECUTE_STRING, EXCEPTIONLOG_BATCHCLASS_STRING, ApexPages.Severity.ERROR);
             
       }
    }   
}       
/*
*************************************************************************
*@Description : This is the Finish method of BATCH class wherein the      *
                any post deletion operation is performed                  *
*@Params      : none                                                      *
*@Return      : void                                                      *    
*************************************************************************
*/
       global void finish(Database.BatchableContext BC){
        
       // Remove Comments to send the Email notification to user about completion of job 
       /*
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =: BC.getJobId()];

       // Send an email to the Apex job's submitter notifying of job completion.

       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

       String[] toAddresses = new String[] {a.CreatedBy.Email};

       mail.setToAddresses(toAddresses);

       mail.setSubject('Record Clean Up Status: ' + a.Status);

       mail.setPlainTextBody

       ('The batch Apex job processed ' + a.TotalJobItems +

       ' batches with '+ a.NumberOfErrors + ' failures.');

       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       */
   }
}