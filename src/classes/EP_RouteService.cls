/* 
   @Author      Accenture
   @name        EP_RouteService
   @CreateDate  05/05/2017
   @Description Service class for Route Object
   @Version     1.0
*/
 public class EP_RouteService{
    EP_RouteDomainObject routeDomainObject;
    EP_RouteMapper mapper;
    @testvisible Map<Id,EP_Route__c> oldRouteMap;
    @testvisible Map<Id,EP_Route__c> newRouteMap;
    public static final String ROUTE_INACTIVE = 'Inactive';

    
     /**
    * @author       Accenture
    * @name         EP_RouteService
    * @date         05/05/2017
    * @description  Constructor for setting routeDomain
    * @param        EP_AccountDomainObject
    * @return       NA
    */  
    public EP_RouteService(EP_RouteDomainObject routeDomainObject){
        this.routeDomainObject = routeDomainObject;
        this.oldRouteMap = routeDomainObject.getOldRoutes(); 
        this.newRouteMap = routeDomainObject.getNewRoutes();   
        mapper = new EP_RouteMapper();
    }
    
    /**
    * @author       Accenture
    * @name         doActionBeforeUpdate
    * @date         05/05/2017
    * @description  Method to perform validations before update of the records
    * @param        Map<Id,List<Order>>
    * @return       NA
    */  
    public void doActionBeforeUpdate(){
        Set<Id> isRouteNameEditableResult = isRouteNameEditable();
        
        Set<Id> isRouteCodeEditableResult = isRouteCodeEditable();
                
        Set<Id> canInactiveDefaultRouteResult = canInactiveDefaultRoute();

        for(EP_Route__c route : newRouteMap.values()) {
            throwErrorRouteNameEditable(isRouteNameEditableResult,route);
            throwErrorRouteCodeEditable(isRouteCodeEditableResult,route);
            throwErrorcanInactiveDefaultRoute(canInactiveDefaultRouteResult,route);
        }
    }

    public void throwErrorRouteNameEditable(Set<Id> isRouteNameEditableSet,EP_Route__c route){
        if(isRouteNameEditableSet.contains(route.Id) ){
            route.adderror(Label.EP_RouteName_Modification_Error);
        }
    }

    public void throwErrorRouteCodeEditable(Set<Id> isRouteCodeEditableSet,EP_Route__c route) {
        if(isRouteCodeEditableSet.contains(route.Id)){
            route.adderror(Label.EP_RouteCode_Modification_Error);
        }
    }

     //Defect 58468--Start
    public void throwErrorcanInactiveDefaultRoute(Set<Id> canInactiveDefaultRouteSet,EP_Route__c route) {
        if (canInactiveDefaultRouteSet.contains(route.Id)) {
          route.addError(Label.EP_isDefault_Ruote_Error);
        }
    }
     //Defect 58468--End


    
    /**
    * @author       Accenture
    * @name         doActionBeforeDelete
    * @date         05/05/2017
    * @description  Method to validate deletion of route
    * @param        NA
    * @return       NA
    */  
    public void doActionBeforeDelete(){
        Set<id> isRouteDeletableResult = isRouteDeletable();                
        for(EP_Route__c route : oldRouteMap.values()) {
            if(isRouteDeletableResult.contains(route.id)){
                route.adderror(Label.EP_Route_Deletion_Error);
            }
        }
    }
    
    
    
    /**
    * @author       Accenture
    * @name         isRouteNameEditable
    * @date         05/05/2017
    * @description  Method to validate changes in Route Name
    * @param        NA
    * @return       Set<Id>
    */  
    public Set<Id> isRouteNameEditable(){
      Set<Id> validationSet = new Set<Id>(); 
      for(EP_Route__c route : newRouteMap.values()) {
        if(routeDomainObject.isRouteNameChanged(route, oldRouteMap.get(route.id))) {
          validationSet.add(route.Id);
        }
      }
      return  mapper.getRoutesWithOrdersForNameCodeChange(validationSet).keySet();
    }

    

    
     /**
    * @author       Accenture
    * @name         isRouteCodeEditable
    * @date         05/05/2017
    * @description  Method to validate whether route code is editable
    * @param        NA
    * @return      Set<Id> 
    */  
    public Set<Id> isRouteCodeEditable(){
      Set<Id> validationSet = new Set<Id>(); 
      for(EP_Route__c route : newRouteMap.values()) {
        if(routeDomainObject.isRoutecodeChanged(route, oldRouteMap.get(route.id) )){
          validationSet.add(route.Id);
        }
      }
      return  mapper.getRoutesWithOrdersForNameCodeChange(validationSet).keySet();
    }
    
    
     /**
    * @author       Accenture
    * @name         CanInactiveDefaultRoute
    * @date         05/22/2017
    * @description  Method to validate when route status changes to InActive
    * @param        NA
    * @return       Set<Id> 
    */  
    public Set<Id>  canInactiveDefaultRoute(){
      Set<id> validationSet = new Set<id>();
      for(EP_Route__c route : newRouteMap.values()) {
        if(checkRouteStatusChangeAndInactive(route)){
          validationSet.add(route.id);
        }
      }
      return mapper.getRoutesWithAccountForStatusChange(validationSet);
    }


    
    /**
    * @author       Accenture
    * @name         isRouteDeletable
    * @date         05/05/2017
    * @description  Method to validate whether route can be deleted 
    * @param        NA
    * @return       Set<Id>
    */  
    public Set<Id> isRouteDeletable(){
       return mapper.getAssociatedOrdersWithRoute(oldRouteMap.keySet()).keySet();
    }
    

    /**
    * @author       Accenture
    * @name         checkRouteStatusChangeAndInactive
    * @date         05/05/2017
    * @description  Method to validate routes status change
    * @param        EP_Route__c
    * @return       Boolean
    */  
    @testvisible private boolean checkRouteStatusChangeAndInactive(EP_Route__c route){
      return (route.EP_Status__c == EP_Common_Constant.ROUTE_INACTIVE_STATUS && routeDomainObject.isRouteStatusChanged(route, oldRouteMap.get(route.id)));
    }
       
 }