/*   
     @Author <Accenture>
     @name <EP_FE_OrderListEndpointTest.cls>     
     @Description <This class is used as parent response controller class>   
     @Version <1.1> 
*/
@isTest
public class EP_FE_OrderListEndpointTest {

    private static Order od;
    private static User usrd;
        
   /*********************************************************************************************
    *@Description : This is the Test method Create Order.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    @testSetup static void testCreateOrder(){
               
        od = EP_FE_TestDataUtility.createNonVMIOrder();    
    }       
   /*********************************************************************************************
    *@Description : This is the Test method to return Order of Past Type.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/   
    static testMethod void testFetchOrderPast() {
       
       usrd= EP_TestDataUtility.createRunAsUser();               
       Database.insert (usrd);        
       User usr = [select id,IsActive from user where id = :usrd.id limit 1];
       system.runas(usr){
       csord__Order__c odd = [select  id, AccountId__c, EP_Sell_To__c, EP_Bill_To__c, EP_ShipTo__c, EP_Is_Pre_Pay__c  from csord__Order__c limit 1];
       EP_FE_OrderUtils.evaluatePriceVisibility(odd);
       EP_FE_Utils.returnTerminalInfo();
       Test.startTest();
       RestRequest req = new RestRequest();   
       req.httpMethod = 'Get';  
       req.requestURI = '/services/apexrest/FE/V1/order/list/';  
       RestContext.request = req;
       
       req.params.put('offset', '0');
       req.params.put('limit', '10');          
       req.params.put('type', 'past');
       
       
    
       EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
       EP_FE_OrderListEndpoint.getQueryFields(true);
       EP_FE_OrderListEndpoint.getQueryFields(false);
   
       // System.assertEquals(odd.id,results.orderList[0].id);
       Test.stopTest();
       }
   }   
    /*********************************************************************************************
    *@Description : This is the Test mehod to return Order of Past Type.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/   
    static testMethod void testFetchOrderActive() {
       
       usrd= EP_TestDataUtility.createRunAsUser();               
       Database.insert (usrd);
       User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
       system.runas(usr){
       Order odd = [select id from order limit 1];      
       Test.startTest();
       RestRequest req = new RestRequest();   
       req.httpMethod = 'Get';  
       req.requestURI = '/services/apexrest/FE/V1/order/list/';  
       RestContext.request = req;
       
       req.params.put('offset', '0');
       req.params.put('limit', '10');          
       req.params.put('type', 'active');
       
    
       EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
       
       Test.stopTest();
       System.assertEquals(results.status ,-4);
       }
   }    
       
   /*********************************************************************************************
    *@Description : This is the Test mehod to return Order of Active Type.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
      static testMethod void testFetechOrderActiveQuerLimit() { 
       
       usrd= EP_TestDataUtility.createRunAsUser();               
       Database.insert (usrd);
       User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
       system.runas(usr){
       Order odd = [select id from order limit 1];
 
       Test.startTest();

       RestRequest req = new RestRequest();   
       req.httpMethod = 'Get';  
       req.requestURI = '/services/apexrest/FE/V1/order/list/';  
       RestContext.request = req;
       
       req.params.put('offset', '0');
       req.params.put('limit', '100000');          
       req.params.put('type', 'active');
       
    
       EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList(); 
       Test.stopTest();
       System.assertEquals(results.status , -2);
       
       }

   }
   
   /*********************************************************************************************
    *@Description : This is the Test mehod to return Order of Active Type.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
    static testMethod void testFetechOrderActiveNegative() { 
       
       usrd= EP_TestDataUtility.createRunAsUser();               
       Database.insert (usrd);
       User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
       system.runas(usr){
       Order odd = [select id from order limit 1];
       
       Test.startTest();

       RestRequest req = new RestRequest();   
       req.httpMethod = 'Get';  
       req.requestURI = '/services/apexrest/FE/V1/order/list/';  
       RestContext.request = req;
       
       req.params.put('offset', '-1');
       req.params.put('limit', '100');          
       req.params.put('type', 'active');
       
    
       EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
       Test.stopTest();
       System.assertEquals(results.status , -1);
       
       }       
   }     
   
    /*********************************************************************************************
    *@Description : This is the Test mehod to return Order of Active Type.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
    static testMethod void testFetechOrderNotActive() { 
       
       usrd= EP_TestDataUtility.createRunAsUser();               
       Database.insert (usrd);
       User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
       system.runas(usr){ 
       Order odd = [select id from order limit 1];
      
       Test.startTest();

       RestRequest req = new RestRequest();   
       req.httpMethod = 'Get';  
       req.requestURI = '/services/apexrest/FE/V1/order/list/';  
       RestContext.request = req;
       
       req.params.put('offset', '1');
       req.params.put('limit', '100');          
       req.params.put('type', 'notactive');
       
    
       EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
       Test.stopTest();
       System.assertEquals(results.status , -3);
       
       }
   } 
   
          /*********************************************************************************************
    *@Description : This is the Test mehod to return Order of Active Type.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
    static testMethod void testFetechOrderAlert() { 
       
       usrd= EP_TestDataUtility.createRunAsUser();               
       Database.insert (usrd);
       User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
       system.runas(usr){ 
       Order odd = [select id from order limit 1];
       Test.startTest();

       RestRequest req = new RestRequest();   
       req.httpMethod = 'Get';  
       req.requestURI = '/services/apexrest/FE/V1/order/list/';  
       RestContext.request = req;
       
       req.params.put('offset', '1');
       req.params.put('limit', '100');          
       req.params.put('type', 'alert');
       
    
       EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
       Test.stopTest();
       System.assertEquals(results.status ,-4); 
       
       }       
   } 
          /*********************************************************************************************
    *@Description : This is the Test mehod to return Order of blanket Type.                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/    
    static testMethod void testFetechOrderblanket() { 
       
       usrd= EP_TestDataUtility.createRunAsUser();               
       Database.insert (usrd);
       User usr = [select id,IsActive  from user where id = :usrd.id limit 1];
       system.runas(usr){
       Order odd = [select id from order limit 1];
   
       Test.startTest();

       RestRequest req = new RestRequest();   
       req.httpMethod = 'Get';  
       req.requestURI = '/services/apexrest/FE/V1/order/list/';  
       RestContext.request = req;
       
       req.params.put('offset', '1');
       req.params.put('limit', '100');          
       req.params.put('type', 'blanket');
       
    
       EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
          
       Test.stopTest();
       System.assertEquals(results.status , 0);   
       }
   }    
   
 }