/* @Author <Amit Singh>
   @name <EP_ActionTrigger_Test>
   @CreateDate <24/04/2016>
   @Description <This is the Test class for Action trigger>
   @Version <1.0>
*/
@isTest
private with sharing class EP_ActionTrigger_Test{
    
    private static EP_Country__c country;
    private static EP_Region__c region;
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    private static List<EP_Action__c> actions2; 
    private static List<EP_Action__c> actions;
    private static Account sellToAccount ;
    private static integer nrows = EP_Common_Util.getQueryLimit();
    private static EP_Tank__c tank;
     private static Account billToAccount,nonVmiShipToAccount,nonVmiShipToAccount2,storageShipToAcc,storageShipToAcc2,transporter;
    private static Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.NON_VMI_SHIP_TO);
    private static Account storageLocAccPrimary,storageLocAccSecondary,storageLocAccAlternate;
    private static EP_Stock_Holding_Location__c primaryStockHolding,secondaryStockHolding,alternateStockHolding,primaryStockHoldingSellTo,secondaryStockHoldingSellTo,alternateStockHoldingSellTo;
    private static EP_Inventory__c primaryInventory,secondaryInventory,alterNateInventory;
    private static EP_Tank__c tankInstance; 
    private static Id strNonVMIRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, 'Non-VMI Ship To');
    //private static Id strStorageShipToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, 'Storage Ship-To  ');
   // private static Id strNonVMIShipToRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, 'Non-VMI Ship To');
    private static Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
    private static  User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
    private static  PricebookEntry pbEntryPetrol,pbEntryDiesel;
    private static Product2 productPetrolObj,productDieselObj;
    //Test class Fix Start
   //private static Profile stockAccountantProfile = [Select id from Profile Where Name = 'EP - Stock Accountant_R1' Limit : EP_Common_Constant.ONE];
    //private static  User stockAcctUser = EP_TestDataUtility.createUser(stockAccountantProfile.id);
    //private static Profile stockControllerProfile = [Select id from Profile Where Name = 'EP - Stock Controller_R1' Limit : EP_Common_Constant.ONE];
    //private static  User stockControllerUser = EP_TestDataUtility.createUser(stockAccountantProfile.id);
    //Test class Fix End
    private static Product2 taxProduct;
    private static Product2 freightProduct;
    private static Company__c company1;
    
    
    /*
     * Method for checking Actoion creation on changing basic data setup 
     **/
    private static testmethod void unitTest1(){
        Profile SMAgent = [Select id from Profile
                            Where Name = 'Puma SM Agent_R1' 
                            limit :EP_Common_Constant.ONE];
        User SMTMuser = EP_TestDataUtility.createUser(SMAgent.id);
        
        Profile sysAdmin = [Select id from Profile
                            Where Name = 'System Administrator' 
                            limit :EP_Common_Constant.ONE];
        User sysAdminUser = EP_TestDataUtility.createUser(sysAdmin.id);
                  
        system.runAs(SMTMuser){
        
            EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
            EP_Region__c reg ;
            EP_Freight_Matrix__c freightMatrix;
            List<EP_Stock_Holding_Location__c>lStockHlodingLocation = new list<EP_Stock_Holding_Location__c>();
            DataBase.SaveResult[] lSHLSaveResult = new list<DataBase.SaveResult>(); 
            String sellTo = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ
                                                                ,EP_Common_Constant.SELL_TO);
            Test.startTest();
            
            //CREATE FRIEGHT MATRIX
            database.insert(contry1);
            freightMatrix = EP_TestDataUtility.createFreightMatrix();
            database.insert(freightMatrix);
            //CREATE CUSTOMER
            Account parentAcc = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
            parentAcc.EP_Country__c = contry1.id;
            parentAcc.EP_Delivery_Pickup_Country__c = contry1.id;
            reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
            database.insert(reg); 
            parentAcc.EP_Delivery_Pickup_Country__c = contry1.id;
            parentAcc.EP_Cntry_Region__c = reg.id;
            parentAcc.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;//Insert Sell To Delivery Type
           // parentAcc.EP_Provider_Managed_Transport_Services__c = 'FALSE';
            database.insert(parentAcc);
            
           
            parentAcc.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status
            Database.update(parentAcc);
            
            Test.stopTest();
            
            country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
            Database.insert(country,false);
            Company__c comp = EP_TestDataUtility.createCompany('EPUMA');
            comp.EP_BSM_GM_Review_Required__c = true;
            database.insert(comp);
            region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
            Database.insert(region,false);
            sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
            sellToAccount.EP_Puma_company__c = comp.id;
            Database.insert(sellToAccount, false);
            Contact contactRec = EP_TestDataUtility.createTestRecordsForContact(sellToAccount);
            
            EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
            Database.insert(SHL1,false);
            
            
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Sell TO status to basic data setup
            sellToAccount.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.saveResult sv = Database.update(sellToAccount,false);
            
            Account vmiShipTo = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null);
            vmiShipTo.EP_Country__c = country.id;
            vmiShipTo.EP_Transportation_Management__c = 'Own';
            Database.insert(vmiShipTo, false);
            
            //Update ship to stats to basic data setup
            vmiShipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            database.update(vmiShipTo,false);
            
            Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
            
            //create tank
            tank = EP_TestDataUtility.createTestEP_Tank(vmiShipTo.id,prod.id);
            database.insert(tank);
            
            system.debug('++++++tank+++++++++++'+tank);
            
            actions = [Select id, Name,RecordTypeId, EP_Record_Type_Name__c, EP_Status__c from EP_Action__c 
                            where EP_Account__c =: sellToAccount.id order by EP_Record_Type_Name__c ASC limit: nrows];
                            system.debug('++++++actions+++++++++++'+actions);
            
            vmiShipTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Update Ship TO status
            vmiShipTo.EP_Indicative_Total_Pur_Value_Per_Yr__c = '< 250k USD';
            Database.update(vmiShipTo);
            
            
            actions2 = [Select id, Name, EP_Record_Type_Name__c, RecordTypeId, EP_Status__c from EP_Action__c where EP_Tank__c =: tank.id limit: nrows];
           // system.assert(actions2.size() == 1);
            
            system.debug('++++++actions2old+++++++++++'+actions2);
            //check tank status has been changed to basic data setu when ship to status is changed
            EP_Tank__c tc = [SELECT id,EP_Tank_Status__c FROM EP_Tank__c WHERE id =: tank.id limit 1];
            //system.assertEquals(EP_Common_Constant.BASIC_DATA_SETUP,tc.EP_Tank_Status__c);
            system.debug('&&&&&&&&&&&&&'+tc.EP_Tank_Status__c);
            //system.assertEquals('Basic Data Setup' ,tc.EP_Tank_Status__c);
        }
        
        system.runas(sysAdminUser){List<EP_Action__c> finalActions = new List<EP_Action__c>();
            for(EP_Action__c eAction : actions2){
                eAction.OwnerId = sysAdminUser.id;
                eAction.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
                finalActions.add(eAction);
            }
            update actions2;
            
            for(EP_Action__c eAction : actions){
                eAction.OwnerId = sysAdminUser.id;
                eAction.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
                finalActions.add(eAction);
            }
            update finalActions;
            
            
            
            system.debug('++++++actions2New+++++++++++'+actions2);
            
           system.debug('++++++finalActions+++++++++++'+finalActions);
        }
        
        system.runAs(SMTMuser){
            actions = [Select id, Name,RecordTypeId, EP_Record_Type_Name__c, EP_Status__c from EP_Action__c 
                            where EP_Account__c =: sellToAccount.id order by EP_Record_Type_Name__c ASC limit: nrows];
                            system.debug('++++++final actions+++++++++++'+actions);
                            
            actions2 = [Select id, Name, EP_Record_Type_Name__c, RecordTypeId, EP_Status__c from EP_Action__c where EP_Tank__c =: tank.id limit: nrows];
            system.debug('++++++final actions222+++++++++++'+actions2);
        }
        //system.assert(actions.size() == 2);
        //system.assertEquals('CSC Review', actions[0].EP_Record_Type_Name__c);
        //system.assertEquals('CSC Review', actions[0].Name);
        //system.assertEquals('Sell-To Logistics Review', actions[1].EP_Record_Type_Name__c);
    }
    
    private static testMethod void unitTest2(){ 
        Test.startTest();
        Account acc = new Account(Name = 'testAccount', billingCountry = 'US' );
        insert acc;
        List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.ADMIN_PROFILE];
        List<User> userList = [Select id from user where isActive = true and profileId in :profiles limit 2 ];
        EP_Action__c action = new EP_Action__c();
        action.EP_Account__c = acc.Id;
        action.ownerId = userList[0].Id;
        action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS.toUpperCase();
        insert action;
        
        action.EP_Status__c = EP_Common_Constant.ACT_APPROVED_STATUS.toUpperCase();
        action.ownerId = userList[1].Id;
        try {
            update action;
        } catch (Exception e) {
            //system.assertEquals(true, e.getMessage().contains(Label.EP_Cmpltd_RVw_Asgn_Err+EP_Common_Constant.SPACE));
        }
        
        //allowTanksLgstcsRvwCmplt();
        Test.stopTest();
        
        EP_Action__c action2 = new EP_Action__c();
        action2.EP_Account__c = acc.Id;
        
        List<QueueSobject> queueObjects = [Select QueueId from QueueSobject where SobjectType = 'Action' limit 1];
        if(queueObjects.size() > 0) {
            List<Group> queList = [Select Id from Group where type = 'Queue' and id = :queueObjects[0].Id limit 1];
            action2.EP_System_Queue__c = queList[0].Id;
        }       
        
        action2.ownerId = userList[0].Id;        
        insert action2;
        action2.ownerId = userList[1].Id;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;
        action2.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
        Id tankLgstcsRvwType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACTION_OBJ
                                                                    ,EP_Common_Constant.ACT_TANK_LOGISTICS_REVIEW_RT);
        action2.recordTypeId = tankLgstcsRvwType;
        Database.SaveResult result = Database.update(action2, false);
        //system.assert(!result.success);
        
    }
   
    private static testMethod void unitTest3(){ 
        Test.startTest();
        transporter = EP_TestDataUtility.createTestVendor('Transporter','40001');
         /**** Defect- 57023 Fix START******/
        //transporter.EP_Use_Managed_Transport_Services__c = 'N/A';
        transporter.EP_Provider_Managed_Transport_Services__c = 'TRUE';
        insert transporter;
        /**** Defect- 57023 Fix END******/
        
        id transporterId= transporter.id;
         Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
            EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('India','IN', 'Asia');
            contry1.EP_Mixing_Allowed__c = True;
            insert contry1;
            
            company1 = EP_TestDataUtility.createCompany('AUN1234');
            insert company1;
        
            //create PriceBook  
            Pricebook2 customPB = new Pricebook2(Name='Standard Price Book3', isActive=true,EP_Company__c =company1.Id);
            insert customPB;
            
            //create Product        
            productPetrolObj = new product2(Name = 'petrol', CurrencyIsoCode = 'GBP', isActive = TRUE,family='Hydrocarbon Liquid',Eligible_for_Rebate__c = true,EP_Blended__c = false,EP_Company_Lookup__c=company1.Id);                           
            insert productPetrolObj;
            
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = productPetrolObj.Id,
            UnitPrice = 10000, IsActive = true,EP_Is_Sell_To_Assigned__c=true);
            database.insert(standardPrice,false);
            
            pbEntryPetrol = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = productPetrolObj.Id,
            UnitPrice = 1, IsActive = true, CurrencyIsoCode = 'GBP',EP_Is_Sell_To_Assigned__c = True);
            database.insert(pbEntryPetrol,true); 
            
            // create freight matrix      
            EP_Freight_Matrix__c FreightMat = EP_TestDataUtility.createFreightMatrix();
            insert FreightMat;
            
            // create freight price
            EP_Freight_Price__c  FreightPrice = EP_TestDataUtility.createFreight(FreightMat.id,productPetrolObj.id);       
            insert FreightPrice;
            
            //create Sellto Account
            //create prepayment term
            EP_Payment_Term__c prepaymentTerm = EP_TestDataUtility.createPaymentTerm();
            prepaymentTerm.name = EP_Common_Constant.PREPAYMENT;
            insert prepaymentTerm;

            sellToAccount =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
            sellToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
            sellToAccount.EP_Puma_Company__c = company1.id;
            sellToAccount.EP_Country__c=  contry1.id;
            sellToAccount.EP_Freight_Matrix__c= FreightMat.id;
            sellToAccount.EP_PriceBook__c  = customPB.Id;
            /*CAM 2.7 start*/
            sellToAccount.EP_AvailableFunds__c=1000;
            /*CAM 2.7 end*/
            sellToAccount.EP_Payment_Term_Lookup__c = prepaymentTerm.id;
            insert sellToAccount;
            
            //updateAccountStatus();
            sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;    
            update sellToAccount;
            
           nonVmiShipToAccount =  EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strNonVMIRecordTypeId);
           nonVmiShipToAccount.EP_Pumps__c= 10;
           nonVmiShipToAccount.EP_Freight_Matrix__c= FreightMat.id;
           nonVmiShipToAccount.EP_PriceBook__c = customPB.Id;
           nonVmiShipToAccount.EP_Ship_To_Type__c =  'Consignment';
           nonVmiShipToAccount.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
           nonVmiShipToAccount.EP_Transportation_Management__c = 'Own';
         
           nonVmiShipToAccount.EP_Country__c = contry1.Id;
           
           insert nonVmiShipToAccount;
           
        
           nonVmiShipToAccount2 =  EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strNonVMIRecordTypeId);
           nonVmiShipToAccount2.EP_Pumps__c= 10;
           nonVmiShipToAccount2.EP_Freight_Matrix__c= FreightMat.id;
           nonVmiShipToAccount2.EP_PriceBook__c = customPB.Id;
           nonVmiShipToAccount2.EP_Ship_To_Type__c =  'Consignment';
           nonVmiShipToAccount2.EP_Delivery_Type__c = EP_Common_Constant.DELIVERY;
           nonVmiShipToAccount2.EP_Transportation_Management__c = 'Own';
           insert nonVmiShipToAccount2;
               
           List<EP_Tank__c> tanksToBeInsertedList = new List<EP_Tank__c>();
           EP_Tank__c tankObj = EP_TestDataUtility.createTestEP_Tank(nonVmiShipToAccount.Id, productPetrolObj.Id);
           tanksToBeInsertedList.add(tankObj);        
           database.insert(tanksToBeInsertedList);
                
            id orderConfigMixingRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER_CONFIG_OBJ,EP_Common_Constant.MIXING_RANGE);
            id orderConfigCountryRangeRecordType = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER_CONFIG_OBJ,EP_Common_Constant.COUNTRY_RANGE);
            
            // create country range order configuration record
            EP_Order_Configuration__c orderConfig =  new EP_Order_Configuration__c();
            orderConfig.RecordTypeId = orderConfigCountryRangeRecordType;
            orderConfig.EP_Country__c = contry1.id;
            orderConfig.EP_Max_Quantity__c = 20;
            orderConfig.EP_Min_Quantity__c = 1; 
            orderConfig.EP_Product__c = productPetrolObj.id;
            orderConfig.EP_Volume_UOM__c = 'LT';
            insert orderConfig;
            
            // create non mixing order configuration record
            EP_Order_Configuration__c orderConfig1 =  new EP_Order_Configuration__c();
            orderConfig1.RecordTypeId = orderConfigMixingRecordType ;
            orderConfig1.EP_Country__c = contry1.id;
            orderConfig1.EP_Max_Quantity__c = 20;
            orderConfig1.EP_Min_Quantity__c = 1;
            orderConfig1.EP_Product__c = productPetrolObj.id;
            orderConfig1.EP_Volume_UOM__c = 'LT';
            insert orderConfig1;
            
            // create Storage Location type Account
            List<Account> storageLocAcc = new List<Account>();
            storageLocAccPrimary = EP_TestDataUtility.createStockHoldingLocation();
            storageLocAccSecondary = EP_TestDataUtility.createStockHoldingLocation();
            storageLocAccAlternate = EP_TestDataUtility.createStockHoldingLocation();
            storageLocAcc.add(storageLocAccPrimary);
            storageLocAcc.add(storageLocAccSecondary);
            storageLocAcc.add(storageLocAccAlternate);
            Database.insert(storageLocAcc);
            
            if(transporterId==null){
            transporter = EP_TestDataUtility.createTestVendor('Transporter','40001');
            /**** Defect- 57023 Fix START******/
            //transporter.EP_Use_Managed_Transport_Services__c = 'N/A';
            transporter.EP_Provider_Managed_Transport_Services__c = 'TRUE';
            /**** Defect- 57023 Fix END******/
            insert transporter;
            transporterId = transporter.id;
            }
            //create stock holding location
            List<EP_Stock_Holding_Location__c> stockholdingLocs = new List<EP_Stock_Holding_Location__c>();
            
            primaryStockHoldingSellTo = new EP_Stock_Holding_Location__c(EP_Use_Managed_Transport_Services__c ='FALSE',EP_Location_Type__c = 'Primary',EP_Sell_To__c=sellToAccount.id,EP_Transporter__c = transporterId ,Stock_Holding_Location__c=storageLocAccPrimary.id);
            secondaryStockHoldingSellTo = new EP_Stock_Holding_Location__c(EP_Use_Managed_Transport_Services__c ='FALSE',EP_Location_Type__c = 'Secondary',EP_Sell_To__c=sellToAccount.id,EP_Transporter__c = transporterId ,Stock_Holding_Location__c=storageLocAccSecondary.id);
            alternateStockHoldingSellTo = new EP_Stock_Holding_Location__c(EP_Use_Managed_Transport_Services__c ='FALSE',EP_Location_Type__c = 'Alternate',EP_Sell_To__c=sellToAccount.id,EP_Transporter__c = transporterId ,Stock_Holding_Location__c=storageLocAccAlternate.id);
            
            stockholdingLocs.add(primaryStockHoldingSellTo);
            stockholdingLocs.add(secondaryStockHoldingSellTo);
            stockholdingLocs.add(alternateStockHoldingSellTo);
            //Database.insert(stockholdingLocs);
            stockholdingLocs = new List<EP_Stock_Holding_Location__c>();
            
            primaryStockHolding = new EP_Stock_Holding_Location__c(EP_Use_Managed_Transport_Services__c ='FALSE',EP_Location_Type__c = 'Primary',EP_Ship_To__c=nonVmiShipToAccount .id,EP_Transporter__c = transporterId ,Stock_Holding_Location__c=storageLocAccPrimary.id);
            secondaryStockHolding = new EP_Stock_Holding_Location__c(EP_Use_Managed_Transport_Services__c ='FALSE',EP_Location_Type__c = 'Secondary',EP_Ship_To__c=nonVmiShipToAccount.id,EP_Transporter__c = transporterId ,Stock_Holding_Location__c=storageLocAccSecondary.id);
            alternateStockHolding = new EP_Stock_Holding_Location__c(EP_Use_Managed_Transport_Services__c ='FALSE',EP_Location_Type__c = 'Alternate',EP_Ship_To__c=nonVmiShipToAccount .id,EP_Transporter__c = transporterId ,Stock_Holding_Location__c=storageLocAccAlternate.id);
            
            stockholdingLocs.add(primaryStockHolding);
            stockholdingLocs.add(secondaryStockHolding);
            stockholdingLocs.add(alternateStockHolding);
            //Database.insert(stockholdingLocs);
            
            //update NonVMIship to accounts status to active
            nonVmiShipToAccount.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            update nonVmiShipToAccount;
            
            nonVmiShipToAccount.EP_Status__c=EP_Common_Constant.STATUS_ACTIVE;
            update nonVmiShipToAccount;
            
            //create Inventories
            List<EP_Inventory__c> inventories = new List<EP_Inventory__c>();
         
            primaryInventory = EP_TestDataUtility.createInventory(storageLocAccPrimary.id,productPetrolObj.id);
          
            inventories.add(primaryInventory);
         
            Database.insert(inventories);
        
        map<Id,EP_Action__c>mNewActions = new map<Id,EP_Action__c>();
        map<Id,EP_Action__c>mOldActions = new map<Id,EP_Action__c>();
        Account acc = new Account(Name = 'testAccount', billingCountry = 'US');
       
        insert acc;
      
        sellToAccount.EP_Integration_Status__c=EP_Common_Constant.ERROR_SYNC_STATUS;
        update sellToAccount;
        
        EP_TestDataUtility.createBankAccount(sellToAccount.id);
        
        List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.ADMIN_PROFILE];
        List<User> userList = [Select id from user where isActive = true and profileId in :profiles limit 2 ];
        EP_Action__c action = new EP_Action__c();
        action.EP_Account__c = sellToAccount.Id;
        action.ownerId = userList[0].Id;
        action.recordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Action__c','Change Request Review');
        action.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
        insert action;
        
         EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;
         EP_ActionTriggerHandler.isExecuteAfterUpdate = false;
         //Test class Fix Start
         try{
          action.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
          action.recordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Action__c', 'CSC Review');
          
          action.EP_Record_Type_Name__c = 'CSC Review';
          update action;
         } catch(exception ex){
         }
         //Test class Fix End
        Test.stopTest();
    }
    
    //assignQueueAndRecordType
    private static testMethod void unitTest4(){ 
        Test.startTest();
        List<EP_Action_Country_Queue_Mapping__mdt> countryQueueList = [Select EP_Queue_Name__c, EP_Region__c from EP_Action_Country_Queue_Mapping__mdt 
                                                                       limit 10];
        //system.assert(false, countryQueueList);
        /*    new List<EP_Action_Country_Queue_Mapping__mdt>();
        countryQueueList.add(new EP_Action_Country_Queue_Mapping__mdt(
            EP_Is_Change_Request__c = false,
            EP_Queue_Name__c = 'testQueue',
            EP_Region__c = 'testRegion'
        ));
        insert countryQueueList;
        */
        
        Account acc = new Account(Name = 'testAccount', billingCountry = 'US' );
        insert acc;
        EP_Action__c action = new EP_Action__c();
        action.EP_Account__c = acc.Id;
        action.recordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Action__c','Tank CSC Review');
        action.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS.toUpperCase();
        action.EP_Record_Type_Name__c = 'EP_Tank_CSC_Review';
        action.EP_Country__c = 'USA';
        EP_ActionTriggerHelper.assignQueueAndRecordType(new List<EP_Action__c>{action});
        EP_ActionTriggerHelper.queryRecordTypeBasedOnRtypeName(new Set<string> {'test'});
        EP_ActionTriggerHelper.queryCountryQueueMapping (new Set<string> {'test'});
        Test.stopTest();
    }
/***L4-45352 start****/
    private static Account SellTo;
    private static Account ShipTo;
    private static EP_Action__c VarActionPricing;
    private static Pricebook2 priceBookObj;
    private static Product2 product1;
    private static Product2 product2;
    private static PricebookEntry priceBookEntry1;
    private static PricebookEntry priceBookEntry2;
    private static EP_Action__c VarActionPriceBook;//
    
    static void dataSetup()
    {
        priceBookObj = EP_TestDataUtility.createPricebook2(true);
        SellTo = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        insert SellTo; 
        ShipTo = EP_TestDataUtility.createShipToAccount(SellTo.Id, null);        
        product1 = EP_TestDataUtility.createProduct2(false);
        product1.EP_Is_System_Product__c = true;
        Insert product1 ;
        product2 = EP_TestDataUtility.createProduct2(false);
        product2.EP_Is_System_Product__c = true;
        Insert product2 ;
        priceBookEntry1 = EP_TestDataUtility.createPricebookEntry(product1.Id, priceBookObj.id);
        priceBookEntry1.EP_Is_Sell_To_Assigned__c = true;
        priceBookEntry1.IsActive = false;
        insert priceBookEntry1;
        priceBookEntry1.IsActive = true;
        update priceBookEntry1;           
    }
/***This method covers method assignQueueAndRecordType and DeactivateAllPriceBookEntries ****/
    private static testMethod void assignQueueAndRecordType_Test()
    {      
         dataSetup();   
         EP_ActionTriggerHandler.isExecuteAfterInsert = true;
         VarActionPriceBook = EP_TestDataUtility.createPriceBookAction(priceBookObj.id);
         EP_ActionTriggerHelper.DeactivateAllPriceBookEntries(new List<EP_Action__c>{VarActionPriceBook});
         EP_ActionTriggerHandler.isExecuteAfterInsert = false;
         system.assert(VarActionPriceBook!=null);
    }
/***This method covers ActivateAllPriceBookEntries method****/    
    
    private static testMethod void ActivateAllPriceBookEntries_Test()
    {  
         dataSetup();       
         priceBookEntry2= EP_TestDataUtility.createPricebookEntry(product2.Id, priceBookObj.id);
         priceBookEntry2.EP_Is_Sell_To_Assigned__c = true;
         priceBookEntry2.IsActive = false;
         insert priceBookEntry2;
         EP_Action__c OldAction;
         OldAction = EP_TestDataUtility.createPriceBookAction(priceBookObj.id);
         OldAction.OwnerId = system.UserInfo.getuserId();
         update OldAction;
         OldAction.EP_Status__c = '03-Completed';
         update OldAction;
         set<id> pblistids = new set<id>();
         pblistids.add(OldAction.EP_Product_List__c);
         EP_ActionTriggerHelper.ActivateAllPriceBookEntries(pblistids);
         EP_ActionTriggerHandler.isExecuteAfterUpdate = false;
         System.AssertEquals('03-Completed',OldAction.EP_Status__c);
    }
/***This method covers isBSMGMRequired method****/    
    private static testMethod void isBSMGMRequired_Test()
    {    
         dataSetup(); 
         string recname = 'Sell-To Pricing Review';    
         VarActionPricing = EP_TestDataUtility.createCustomerAction(recname,SellTo.id);
         insert VarActionPricing;
         VarActionPricing.OwnerId = system.UserInfo.getuserId();
         update VarActionPricing;
         VarActionPricing.EP_Status__c = '03-Completed';
         Update VarActionPricing; 
         Map<id,id> mapForParam = new Map<id,id>();
         mapforparam.put(VarActionPricing.id,VarActionPricing.EP_Account__c);
         EP_ActionTriggerHelper.isBSMGMRequired(mapforparam);
         EP_AccountMapper.getAllShipToAndActionsFromSellTo(mapForParam.values());
         EP_ActionTriggerHandler.isExecuteAfterUpdate = false;
         System.Assert(mapforparam.size()>0);
         System.Assert(mapforparam.values().size()>0);
         System.AssertEquals('03-Completed',VarActionPricing.EP_Status__c);
    } 
    private static testMethod void updateShipToStatusToAccountSetup_Test()
    {    
         dataSetup();
         insert ShipTo;     
         string recname = 'Ship-To Pricing Review';    
         VarActionPricing = EP_TestDataUtility.createCustomerAction(recname,ShipTo.id);
         insert VarActionPricing;
         VarActionPricing.OwnerId = system.UserInfo.getuserId();
         update VarActionPricing;
         VarActionPricing.EP_Status__c = '03-Completed';
         EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
         update VarActionPricing;
         system.debug('VarActionPricing>>'+VarActionPricing.EP_Status__c);
         EP_ActionTriggerHelper.updateShipToStatusToAccountSetup(new list<EP_Action__c> {VarActionPricing});
         EP_ActionTriggerHandler.isExecuteAfterUpdate = false;
    }
/***L4-45352 end****/     
}