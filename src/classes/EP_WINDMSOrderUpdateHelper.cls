/* 
   @Author 			Accenture
   @name 			EP_WINDMSOrderUpdateHelper
   @CreateDate 		02/08/2017
   @Description		Helper class to populate fields, calculated fields and lookup fields in wrapper instance
   @Version 		1.0
*/   
public class EP_WINDMSOrderUpdateHelper {
    @TestVisible private set<string> orderNumberSet = new set<string>();
    private static final String NO_ORDER_FOUND_ERROR = 'No Orders are found for orderIdSf = ';
    private static final String NO_OF_ORDER_ITEM_ERROR = 'Number of Line Items don\'t match in Salesforce with the number of line items provided in JSON.';
    private static final String WINDMS_ID_ERROR = 'Windms Order ID is not same';
    @TestVisible private map<string, csord__Order__c> orderNumberOrderMap = new map<string, csord__Order__c>();
    @TestVisible private map<string, csord__Order__c> orderWithStandardItemsMap = new  map<string, csord__Order__c>();
    @TestVisible private EP_OrderState orderState;
    @TestVisible private map<id, EP_OrderService> orderServiceMap = new map<id, EP_OrderService>();
    
    /**
	* @author 			Accenture
	* @name				createDataSets
	* @date 			02/08/2017
	* @description 		Method used to create datasets from the wrapper instance
	* @param 			list<EP_WINDMSOrderUpdateStub.OrderWrapper> 
	* @return 			NA
	*/
    @TestVisible
    private void createDataSets(list<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHelper','createDataSets');
        for(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper : orderWrapperList){
            if(string.isNotBlank(orderWrapper.identifier.orderIdSf)) {
                orderNumberSet.add(orderWrapper.identifier.orderIdSf);
            }
        }
    }
    
    /**
	* @author 			Accenture
	* @name				createDataMaps
	* @date 			02/08/2017
	* @description 		Method used to create maps for the set of records received from the wrapper instance
	* @param 			list<EP_WINDMSOrderUpdateStub.OrderWrapper> 
	* @return 			NA
	*/
    @TestVisible
    private void createDataMaps(list<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHelper','createDataMaps');
        createDataSets(orderWrapperList);
        EP_OrderMapper orderMapper = new EP_OrderMapper();
        orderNumberOrderMap = orderMapper.getCsOrderMapByOrderNumber(orderNumberSet);
        orderWithStandardItemsMap  = orderMapper.getCsOrderWithStandardItems(orderNumberSet);
    }
    
    /**
	* @author 			Accenture
	* @name				setJSONAttributes
	* @date 			02/08/2017
	* @description 		Method used to set the values to the order object from the wrapper instance
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper 
	* @return 			NA
	*/
    public void setJSONAttributes(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Public','EP_WINDMSOrderUpdateHelper','setJSONAttributes');
        orderWrapper.sfOrder = orderNumberOrderMap.get(orderWrapper.identifier.orderIdSf);
        orderWrapper.sfOrder.EP_SeqId__c = orderWrapper.seqId;
        orderWrapper.sfOrder.EP_WinDMS_Status__c = orderWrapper.orderStatusWinDms;
        orderWrapper.sfOrder.csord__Status2__c  = orderWrapper.orderStatusWinDms;
        orderWrapper.sfOrder.EP_Reason_For_Non_Delivery__c = orderWrapper.reason;
        orderWrapper.sfOrder.EP_WinDMS_Order_Version_Number__c = orderWrapper.Identifier.orderRefNr;
    }
    
    /**
	* @author 			Accenture
	* @name				setOrderAttributes
	* @date 			02/08/2017
	* @description 		Method used to populate calculated and lookup fields of order from wrapper instance
	* @param 			list<EP_WINDMSOrderUpdateStub.OrderWrapper> , string
	* @return 			NA
	*/
    public void setOrderAttributes(list<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList, string tripId) {
        EP_GeneralUtility.Log('Public','EP_WINDMSOrderUpdateHelper','setOrderAttributes');
        createDataMaps(orderWrapperList);
        
        for(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper : orderWrapperList){
            if(isValidOrder(orderWrapper)) {
                setJSONAttributes(orderWrapper);
                setOrderId(orderWrapper);
                setDeliveryDate(orderWrapper);
                setOrderLoadCode(orderWrapper, tripId);
                setOrderStatus(orderWrapper.sfOrder, orderWrapper.sfOrder.EP_WinDMS_Status__c);
                
            }
        }
    }
    
    /**
	* @author 			Accenture
	* @name				isValidOrder
	* @date 			02/08/2017
	* @description 		Checks if the order is valid or not
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper
	* @return 			boolean
	*/
    @TestVisible
    private boolean isValidOrder(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper) {
        boolean isValidOrder = true; 
        EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','isValidOrder');
        if(isOrderNumberExists(orderWrapper)) {
            if(!hasValidLineItems(orderWrapper) || !isValidWINDMSID(orderWrapper)) {
                isValidOrder = false;
            }
        } else {
            isValidOrder = false;
        }
        return isValidOrder;
    }
    
    /**
	* @author 			Accenture
	* @name				isOrderNumberExists
	* @date 			02/08/2017
	* @description 		Checks if the order number exists, else adds error message
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper
	* @return 			boolean
	*/
    @TestVisible
    private boolean isOrderNumberExists(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper) {
        EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','isOrderNumberExists');
        boolean isOrderNumberExists = true;
        system.debug('**isOrderNumberExists' + orderNumberOrderMap);
        if(!orderNumberOrderMap.containsKey(orderWrapper.identifier.orderIdSf)){
            orderWrapper.errorDescription =  NO_ORDER_FOUND_ERROR+orderWrapper.identifier.orderIdSf;
            isOrderNumberExists = false;
        }
        return isOrderNumberExists;
    }
    
    /**
	* @author 			Accenture
	* @name				hasValidLineItems
	* @date 			02/08/2017
	* @description 		Checks if the wrapper has valid line items, else adds error message
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper
	* @return 			boolean
	*/
    @TestVisible
    private boolean hasValidLineItems(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper) {
        boolean hasValidLineItems = true;
        EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','hasValidLineItems');
       
        csord__Order__c orderObjectSF = orderWithStandardItemsMap.get(orderWrapper.identifier.orderIdSf);
        system.debug('**hasValidLineItems' + orderObjectSF);
        if(string.valueof(orderObjectSF.csord__Order_Line_Items__r.size()) <> orderWrapper.noOfLineItems){
            orderWrapper.errorDescription =  NO_OF_ORDER_ITEM_ERROR;
            hasValidLineItems = false;
        }
        return hasValidLineItems;
    }
    
    /**
	* @author 			Accenture
	* @name				isValidWINDMSID
	* @date 			02/08/2017
	* @description 		Checks if thw WINDMS Id is valid or not, else adds error message
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper
	* @return 			boolean
	*/
    @TestVisible
    private boolean isValidWINDMSID(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper) {
        boolean isValidWINDMSID = true;
        EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','isValidWINDMSID');
        csord__Order__c orderObjectSF = orderNumberOrderMap.get(orderWrapper.identifier.orderIdSf);
        if(orderObjectSF.EP_WinDMS_Order_Number__c != null && orderObjectSF.EP_WinDMS_Order_Number__c <> orderWrapper.identifier.orderIdWinDMS ){
            orderWrapper.errorDescription =  WINDMS_ID_ERROR;
            isValidWINDMSID = false;
        }
        return isValidWINDMSID;
    }
    
    /**
	* @author 			Accenture
	* @name				setOrderId
	* @date 			02/08/2017
	* @description 		Method used to set the order Id 
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper
	* @return 			NA
	*/
    @TestVisible
    private void setOrderId(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderNewHelper','setOrderId');
        orderWrapper.sfOrder.Id = orderNumberOrderMap.get(orderWrapper.identifier.orderIdSf).id;                      
    }
    
    /**
	* @author 			Accenture
	* @name				setOrderStatus
	* @date 			02/08/2017
	* @description 		Method used to set the order status
	* @param 			order, String
	* @return 			NA
	*/
    @TestVisible
    private void setOrderStatus(csord__Order__c sfOrder, String newStatus) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHelper','setOrderStatus');
        system.debug('**sfOrder Status' + sfOrder.status__c);
        system.debug('**newStatus' + newStatus);
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(sfOrder);
        //Defect Fix Start - 57216
        //Calling this method Since WINDMS is Sending ETA in Planned status and Previous status is also Planned, And Status Machine will update only if order status is different.
        if(EP_Common_Constant.ORDER_PLANNED_STATUS.equalsIgnoreCase(newStatus)) {
        	setETAonOrder(orderDomain);
        }
        //Defect Fix End - 57216
        
        //Defect Fix Start - 57012
        if(sfOrder.status__c == newStatus) {
        	return;
        }
        //Defect Fix End - 57012
        
        EP_OrderEvent orderEvent = new EP_OrderEvent(orderDomain.getStatus()+ EP_OrderConstant.OrderEvent_ToString + newStatus);
        EP_OrderService orderService = new EP_OrderService(orderDomain);
        if(orderService.setOrderStatus(orderEvent)){
            orderServiceMap.put(sfOrder.id,orderService);
        }
    }
    
   /**
	* @author 			Accenture
	* @name				setETAonOrder
	* @date 			04/27/2017
	* @description 		Method used to set the ETA on Order
	* @param 			EP_OrderDomainObject
	* @return 			NA
	*/
    private void setETAonOrder(EP_OrderDomainObject orderDomain){
        EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','setETAonOrder');
        Account selltoAcc = orderDomain.getSellToAccountDomain().getAccount();
        // Calculating ETA Range, calls General utility method.
        orderDomain.getOrder().EP_Planned_Delivery_Date_Time__c = orderDomain.getOrder().EP_Estimated_Delivery_Date_Time__c;
        if(EP_GeneralUtility.isDateTimeValid(string.valueOf(orderDomain.getOrder().EP_Planned_Delivery_Date_Time__c))){
             orderDomain.getOrder().EP_Estimated_Time_Range__c = EP_GeneralUtility.getETARangeOnOrder(string.valueOf(orderDomain.getOrder().EP_Planned_Delivery_Date_Time__c), 
             integer.valueOf(selltoAcc.EP_Puma_Company__r.EP_Window_Start_Hours__c),
             integer.valueOf(selltoAcc.EP_Puma_Company__r.EP_Window_End_Hours__c),
             orderDomain.getOrder().Status__c);
        }
    }
    
    /**
	* @author 			Accenture
	* @name				doPostActions
	* @date 			02/08/2017
	* @description 		Does the post status change action i.e. invokes the order state machine
	* @param 			set<id>
	* @return 			NA
	*/
    public void doPostActions(set<id> orderIdSet){
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHelper','doPostActions');
        for(Id orderId : orderIdSet) {
            if(orderServiceMap.containskey(orderId)) {
                 orderServiceMap.get(orderId).doPostStatusChangeActions();
            }
        }
    }
    
    /** TODO Need to remove this method
    /**
	* @author 			Accenture
	* @name				getOrdersByOrderNumber
	* @date 			02/08/2017
	* @description 		Gets the order map by querying on order based on order number set
	* @param 			set<string>
	* @return 			map<string, Order>
	*/
    public static map<string, csord__Order__c> getOrdersByOrderNumber(set<string> orderNumberSet) {
        EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','getOrdersByOrderNumber');
        map<string, csord__Order__c> orderNumberOrderMap = new  map<string, csord__Order__c>();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        for(csord__Order__c OrdObj : orderMapperObj.getCSRecordsByOrderNumber(orderNumberSet)) {
            orderNumberOrderMap.put(OrdObj.OrderNumber__c,OrdObj);
        }
        return orderNumberOrderMap;
    }
    
    /**
	* @author 			Accenture
	* @name				setDeliveryDate
	* @date 			02/08/2017
	* @description 		Sets the delivery date in the order instance from the wrapper instance
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper
	* @return 			NA
	*/
    @TestVisible
    private void setDeliveryDate(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper) {
         EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','setDeliveryDate');
        Decimal estDeliveryDate = calculateEstimatedDeliveryDate(orderWrapper.orderDlvryStartDt);
        orderWrapper.sfOrder.EP_Estimated_Delivery_Date_Time__c = estDeliveryDate;   
    }
    
    /**
	* @author 			Accenture
	* @name				setOrderLoadCode
	* @date 			02/08/2017
	* @description 		Sets the order load code in the order instance from the wrapper instance
	* @param 			EP_WINDMSOrderUpdateStub.OrderWrapper, string
	* @return 			NA
	*/
    @TestVisible
    private void setOrderLoadCode(EP_WINDMSOrderUpdateStub.OrderWrapper orderWrapper,string tripId) {
        EP_GeneralUtility.Log('private','EP_WINDMSOrderUpdateHelper','setOrderLoadCode');
        csord__Order__c orderObjectSF = orderNumberOrderMap.get(orderWrapper.identifier.orderIdSf);
        system.debug('**tripId**' +tripId);
        system.debug('**orderObjectSF**' +orderObjectSF);
        system.debug('**orderObjectSF.EP_Delivery_Type__c**' +orderObjectSF.EP_Delivery_Type__c);
        if(EP_Common_Constant.EX_RACK.equalsIgnoreCase(orderObjectSF.EP_Delivery_Type__c)){
            orderWrapper.sfOrder.EP_Order_Load_Code__c = tripId;
        }
    }
    
    /**
	* @author 			Accenture
	* @name				calculateEstimatedDeliveryDate
	* @date 			02/08/2017
	* @description 		This method constructs and returns a detailed delivery date based on the date passed from WINDMS order
	* @param 			string
	* @return 			Decimal
	*/
    @TestVisible
    private Decimal calculateEstimatedDeliveryDate(string orderDlvryStartDt) {
        EP_GeneralUtility.Log('Private','EP_WINDMSOrderUpdateHelper','calculateEstimatedDeliveryDate');
        list<string> dateList = new  list<string>();
        list<string> dateTimeList = new  list<string>();
        if(string.isBlank(orderDlvryStartDt)) {
            return null;
        }
        dateTimeList = orderDlvryStartDt.split(EP_Common_Constant.SPACE_STRING);
        String dateStr = dateTimeList[0];
        String timeStr = dateTimeList[1];
        dateList = dateStr.split(EP_Common_Constant.SPLIT);
        String deliveryDate = dateList[2]+dateList[1]+dateList[0]+
        timeStr.split(EP_Common_Constant.TIME_SEPARATOR_SIGN)[0]+
        timeStr.split(EP_Common_Constant.TIME_SEPARATOR_SIGN)[1];
        return Decimal.valueOf(deliveryDate);
    }
}