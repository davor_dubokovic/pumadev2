/*
   @Author          CloudSense
   @Name            SendOrderConfirmationHandler
   @CreateDate      04/01/2017
   @Description     This class is responsible for initiating the Order Confirmation
   @Version         1.1
 
*/

global class SendOrderConfirmationHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         04/01/2017
    * @Description  Method to process the step and call order confirmation
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                                            
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
            //mark step Status, Completed Date, and write optional step Message
          
        }
        
                                          
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            
            Id crId = orderIdList[0];
            
            try{
                
                generateOrderConfirmation(crId);
                
            }Catch(Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'SendOrderConfirmationHandler',apexPages.severity.ERROR);
            }
          
           step.CSPOFA__Status__c ='Complete';
           step.CSPOFA__Completed_Date__c=Date.today();
           step.CSPOFA__Message__c = 'Custom step succeeded';
           result.add(step);
            
        }
     return result;        
        
        
    }

    public void generateOrderConfirmation(Id orderId) {
        csord__Order__c orderObj = [SELECT EP_Pricing_Status__c, EP_Sell_To__r.EP_Display_Price__c,EP_Document_Title__c, Status__c, EP_Puma_Company_Code__c, csord__Identification__c, (SELECT Id FROM csord__Order_Line_Items__r) FROM csord__Order__c WHERE Id = :orderId LIMIT:EP_Common_Constant.ONE];
                
        String companyName = orderObj.EP_Puma_Company_Code__c;        
        list<id> orderLineIdList = new list<id>();
              
        for(csord__Order_Line_Item__c orderLineItem : orderObj.csord__Order_Line_Items__r){
            orderLineIdList.add(orderLineItem.id);
        }
        orderObj.EP_Document_Title__c = EP_Common_Constant.ORDERCONFIRM_TITLE;
        upsert orderObj;      
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(orderObj.Id, 'SEND_ORDER_CONFIRMATION_REQUEST', companyName);        
        outboundService.sendOutboundMessage(EP_OrderConfirmationConstant.ORDER_CONFIRMATION,orderLineIdList);
       
    }
}