/* 
  @Author <Nicola Tassini>
   @name <EP_FE_UrlRewriter>
   @CreateDate <20/04/2016>
   @Description <  >  
   @Version <1.0>
*/
global with sharing class EP_FE_UrlRewriter implements Site.UrlRewriter {
    String pumaPortal = '/EP_FE_Portal';
   /*********************************************************************************************
    *@Description : This is the Method.                  
    *@Params      :                    
    *@Return      : PageReference                                                                             
    *********************************************************************************************/     
    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        String url = myFriendlyUrl.getUrl();
        if(url.startsWith('/apexremote')
           || url.contains('/csmle__Editor')
           || url.contains('Editor')
           || url.containsIgnoreCase('services/oauth2') 
           || url.containsIgnoreCase('/app') 
           || url.startsWithIgnoreCase('/ep_fe_dictionary')
           || url.startsWithIgnoreCase('/PumaPortal_') 
           || url.containsIgnoreCase('/EP_FE_') 
           || url.startsWithIgnoreCase('/_ui/networks')
           || url.startsWithIgnoreCase('/_ui/system')
           || url.startsWithIgnoreCase('/_ui/')
           || url.containsIgnoreCase('/profilephoto/')
           || url.containsIgnoreCase('/secur/')
           || url.startsWithIgnoreCase('/servlet/servlet')
           || url.containsIgnoreCase('Testing2FA')
           || url.containsIgnoreCase('powerbi')
           || url.containsIgnoreCase('customconfiguration')
           || url.containsIgnoreCase('/CreateBasketAndOffer')
           || url.containsIgnoreCase('ScalableProductModels')
           || url.containsIgnoreCase('NonCommercialSchema')
           || url.startsWithIgnoreCase('/login?c')){
           
            return myFriendlyUrl;
        } else {
            return new PageReference(pumaPortal);
        }
    }
    
/*********************************************************************************************
     @Author <>
     @name <generateUrlFor>
     @CreateDate <>
     @Description <  >  
     @Version <1.0>
    *********************************************************************************************/
    global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls){
        List<PageReference> resultPages = new List<PageReference>();
        for (PageReference friendlyPage : mySalesforceUrls){
            resultPages.add(mapRequestUrl(friendlyPage));
        }
        return resultPages;
        //return null;
    }

}