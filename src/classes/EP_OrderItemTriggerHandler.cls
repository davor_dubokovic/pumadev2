/**
 * @author <Accenture>
 * @name <EP_OrderItemTriggerHandler>
 * @createDate <21/12/2015>
 * @description <This class handles requests from OrderItem trigger> 
 * @version <1.0>
 */
public with sharing class EP_OrderItemTriggerHandler{
    
    public static Boolean isExecuteAfterInsert = false;
    public static Boolean isExecuteAfterUpdate = false;
    public static Boolean createFreightPrice = true;
    public static Boolean isCreditCheck = FALSE;//boolean to update orderitems for credit check flow only jyotsna 1.178
    public static Boolean doExecuteOrderTrigger = True;
    
    /*
      This method handles before insert requests from OrderItem trigger.
     */
    public static void doBeforeInsert(List<OrderItem> lNewOrderItem){
        doExecuteOrderTrigger = False;
        EP_OrderItemTriggerHelper.setOrderItemLocation(lNewOrderItem);
    }
    
    /**
     * This method handles after insert requests from OrderItem trigger.
     */
    public static void doAfterInsert(map<Id,OrderItem>mOldOrderItem
                                     ,Map<Id,OrderItem>mNewOrderItem){
        EP_OrderItemTriggerHelper.updatePriorQuantity(mOldOrderItem,mNewOrderItem,true);
  
        EP_OrderItemTriggerHelper.setOrderLocation(mNewOrderItem.values()); 
         
    }
    
    /**
     * This method handles before update requests from OrderItem trigger. //Jyotsna 1.178
     */
    public static void doBeforeUpdate(map<Id,OrderItem>mOldOrderItem
                                     ,Map<Id,OrderItem>mNewOrderItem){
       // if(isCreditCheck){
            EP_OrderItemTriggerHelper.updatePriorQuantity(mOldOrderItem,mNewOrderItem,false);
       // }
    }
}