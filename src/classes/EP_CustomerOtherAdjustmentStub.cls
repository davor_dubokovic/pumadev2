/* 
   @Author 			Accenture
   @name 			EP_CustomerOtherAdjustmentStub
   @CreateDate 		03/10/2017
   @Description		Stub class for customer other adjustment inbound interface
   @NovaSuite Fix -- comments added
   @Version 		1.0
*/
public class EP_CustomerOtherAdjustmentStub {
	public MSG MSG;
    /**
    * @Author <Accenture>
    * constructor of class EP_CustomerOtherAdjustmentStub 
    * @Name EP_CustomerOtherAdjustmentStub
    **/
    public EP_CustomerOtherAdjustmentStub(){
        MSG = new MSG();
    }
    /**
    * @Author <Accenture>
    * Inner class MSG 
    * @Name MSG
    **/
    public class MSG {
        public EP_MessageHeader HeaderCommon;
        public payload payload;
        public String StatusPayload;
        /**
        * @Author <Accenture>
        * constructor of Inner class MSG 
        * @Name MSG
        **/
        public MSG(){
            HeaderCommon = new EP_MessageHeader();
            payload = new payload();
        }
    }
    /**
    * @Author <Accenture>
    * Inner class payload 
    * @Name payload
    **/    
    public class payload {
    	public any0 any0;
        /**
        * @Author <Accenture>
        * constructor of Inner class payload 
        * @Name payload
        **/
    	public payload(){
    		any0 = new any0();
    	}
    }
    /**
    * @Author <Accenture>
    * Inner class any0 
    * @Name any0
    **/
    public class any0 {
    	public documents documents;
        /**
        * @Author <Accenture>
        * Inner class any0 
        * @Name any0
        **/
    	public any0(){
    		documents = new documents();
    	}
    }
    /**
    * @Author <Accenture>
    * Inner class documents 
    * @Name documents
    **/
    public class documents {
    	public List<document> document;
        /**
        * @Author <Accenture>
        * constructor of Inner class documents 
        * @Name documents
        **/
    	public documents(){
    		document = new List<document>();
    	}
    }
    /**
    * @Author <Accenture>
    * Inner class document 
    * @Name document
    **/
    public class document{
    	public EP_Customer_Other_Adjustment__c SFCustomerOtherAdjustment;
    	public string errorDescription;
    	public string seqId;
    	public identifier identifier;
    	public string paymentMethod;
    	public string docDate;
    	public string submissionDate;
    	public string referenceNr;
    	public string amount;
    	public string remainingAmount;
    	public string currencyId;
    	public string paymentTerm;
    	public string reasonCode;
    	public string description;
    	public string docState;
 		//L4 #45318 and #45304 Changes Start
    	public string isBadDebtWriteOff;
    	public string isReversal;
    	//L4 #45318 and #45304 Changes End
		//L4 #45312 Changes Start
        public string dueDate;
        public string sellTo;
        //L4 #45312 Changes End
        /**
        * @Author <Accenture>
        * constructor of Inner class document 
        * @Name document
        **/
    	public document(){
    		identifier = new identifier();
    	}
    }
    /**
    * @Author <Accenture>
    * Inner class identifier 
    * @Name identifier
    **/
    public class identifier{
    	public string docId;
    	public string documentType;
    	public string clientId;
    	public string billTo;
    	//L4 #45304 Changes Start
    	public string entryNr;
    	//L4 #45304 Changes End
    }
}