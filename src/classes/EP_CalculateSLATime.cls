/* 
@Author      Accenture
@name        EP_CalculateSLATime
@CreateDate  04/1/2018
@Description This class calculates the SLA Time
@Version     1.0
*/
global class EP_CalculateSLATime implements Support.MilestoneTriggerTimeCalculator {
    
    private static final String CLASSNAME ='EP_CalculateSLATime';
    private static final String CALCULATEMILESTONETRIGGERTIME ='calculateMilestoneTriggerTime';
    /** This method returns timecalculation
    *  @name            calculateMilestoneTriggerTime
    *  @param           id
    *  @return          integer
    *  @throws          NA
    */
    global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId){
        integer slaTime =0;
        try{
            case objCase = EP_CaseMapper.getRecordsByIds(caseId) ;
            system.debug('objCase :::-->>>'+objCase );
            EP_Company_Case_configuration__c objComCaseConf =EP_CompanyCaseConfigurationMapper.FetchRecord(objCase.Account.EP_Puma_Company__c,objCase.type,objCase.EP_FE_Sub_Type__c);
            system.debug('objComCaseConf :::-->>>'+objComCaseConf );
            slaTime = timeCalculation(objComCaseConf );
        }
        catch(exception ex){
             EP_LoggingService.logHandledException( ex, EP_Common_Constant.EPUMA, CALCULATEMILESTONETRIGGERTIME, CLASSNAME, ApexPages.Severity.ERROR );
        }
        return slaTime;
    }
    
    /** This method returns slaTime
    *  @name            timeCalculation
    *  @param           id
    *  @return          integer
    *  @throws          NA
    */
    @ testvisible
    private integer timeCalculation(EP_Company_Case_configuration__c objComCaseConf ){
        integer slaTime =0;
        if( objComCaseConf  <> null && objComCaseConf.EP_Company__c <> null && objComCaseConf.EP_Company__r.EP_Business_Hours__c<> null){
            BusinessHours objBusinessHour = EP_BusinessHoursMapper.getRecordsByIds(objComCaseConf.EP_Company__r.EP_Business_Hours__c);
            integer hoursinaDay = CalculateTimeInaDay(objBusinessHour);
            slaTime =integer.valueof( objComCaseConf.EP_SLA_days__c<> null  ?  objComCaseConf.EP_SLA_days__c*hoursinaDay  : objComCaseConf.EP_SLA_hours__c*60); 
        }
        return slaTime;
    }
    
    /** This method returns slaTime
    *  @name            CalculateTimeInaDay
    *  @param           BusinessHours
    *  @return          integer
    *  @throws          NA
    */
    @ testvisible
    private integer CalculateTimeInaDay(BusinessHours objBusinessHours){
        Integer timeDiff =0;
        integer endHour     = objBusinessHours.MondayEndTime.hour();
        integer endminute    = objBusinessHours.MondayEndTime.minute();
        integer endtime =(endHour*60)+endminute;
        integer startHour   = objBusinessHours.MondayStartTime.hour();
        integer startminute   = objBusinessHours.MondayStartTime.minute();
        integer starttime =(startHour*60)+startminute;
        timeDiff = integer.valueof(endtime-starttime )>0?integer.valueof(endtime-starttime ):24;
        return  timeDiff;       
    }

}