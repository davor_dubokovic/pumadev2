/**
 *  @Author <Accenture>
 *  @Name <EP_NAVOrderNewWS>
 *  @CreateDate <26/07/2016>
 *  @Description <This is the apex RESTful WebService to generate Orders>
 *  @Version <1.0>
 */
@RestResource(urlMapping='/v1/OrderSyncWithNav/*')
global without sharing class EP_NAVOrderNewWS {

    /**
       This method is used to sync order update request from nav to salesforce
     */
    @HttpPost
    global static void processRequest() {
        EP_GeneralUtility.Log('global','EP_NAVOrderNewWS','processRequest');
        RestRequest request = RestContext.request;
        String requestBody = request.requestBody.toString();
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        EP_IntegrationService service = new EP_IntegrationService();
        string response = service.handleRequest(EP_Common_Constant.NAV_ORDER_NEW,requestBody); 
        RestContext.response.responseBody = Blob.valueOf(response);
    }
}