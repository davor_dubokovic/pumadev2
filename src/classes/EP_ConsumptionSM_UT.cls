@isTest
public class EP_ConsumptionSM_UT
{

	@testSetup static void init() {
      	List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
	static testMethod void getOrderState_test() {
		EP_ConsumptionSM localObj = new EP_ConsumptionSM();
		EP_OrderDomainObject obj = EP_TestDataUtility.getConsumptionOrderDomainObject();
		localObj.setOrderDomainObject(obj);
		EP_OrderEvent currentEvent = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
		Test.startTest();
		EP_OrderState result = localObj.getOrderState(currentEvent);
		Test.stopTest();
		System.AssertNotEquals(null,result);
	}
}