/* 
   @Author 			Accenture
   @name 			EP_ShipToPricingEnginePayloadCtrlExtn
   @CreateDate 		2/13/2017
   @Description		This class will be used to setup the data for Ship to pricing engine outbound messages
   @Version 		1.0
*/
public with sharing class EP_ShipToPricingEnginePayloadCtrlExtn { 
    private string messageId;
    private string messageType;
    public String custSeqId {get;set;}  
    public String shipToSeqId {get;set;}
    public Account AccountObject {get;set;}
    private string secretCode;
    public string paymentTermCode {get;set;}
    public string packagedPaymentTermCode {get;set;}
     
	/**
	* @author 			Accenture
	* @name				deferUpperLimit
	* @date 			2/13/2017
	* @description 		To hold the value of deferUpperLimit as the XML only takes the numberic value and not null
	* @param 			NA
	* @return 			Integer
	*/          
	public Integer deferUpperLimit {
        get {
          Integer deferUpperLimitValue = Integer.ValueOf(string.isblank(this.AccountObject.EP_Defer_Upper_Limit__c) ? '0' : this.AccountObject.EP_Defer_Upper_Limit__c);
          return deferUpperLimitValue;
        } set;
    }
     
	/**
	* @author 			Accenture
	* @name				EP_CustomerPricingEnginePayloadCtrlExtn
	* @date 			2/13/2017
	* @description 		The extension constructor initializes the members variable AccountObject by using the getRecord method from the standard controller. This method will be use setup the values for Header in XML message
	* @param 			ApexPages.StandardController
	* @return 			NA
	*/                       
	public EP_ShipToPricingEnginePayloadCtrlExtn(ApexPages.StandardController stdController) {
        secretCode = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_SECRETCODE);
        if (!Test.isRunningTest()) stdController.addFields(new List<String>{EP_Common_Constant.COMPANY_CODE_FIELD ,EP_Common_Constant.DEFER_UPPER_LIMIT_FIELD,EP_Common_Constant.PARENT_ACCOUNT_EP_REQUESTED_PAYMENT_TERMS ,EP_Common_Constant.PARENT_ACCOUNT_EP_REQUESTED_PACKAGED_PAYMENT_TERM}); 
        this.AccountObject = (Account) stdController.getRecord();
        this.messageType = ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGE_TYPE);
        this.messageId =  ApexPages.currentPage().getParameters().get(EP_Common_Constant.PARAM_MESSAGEID);
        custSeqId = EP_IntegrationUtil.reCreateSeqId(this.messageId, this.AccountObject.ParentId);
        shipToSeqId = EP_IntegrationUtil.reCreateSeqId(this.messageId, this.AccountObject.Id);
        EP_PaymentTermMapper paymentMapper = new EP_PaymentTermMapper();
        if(string.isNotBlank(this.AccountObject.Parent.EP_Requested_Payment_Terms__c)){
        	EP_Payment_Term__c paymentTerm = paymentMapper.getRecordsByName(this.AccountObject.Parent.EP_Requested_Payment_Terms__c) ;
        	this.paymentTermCode = paymentTerm == null ? EP_Common_Constant.BLANK : paymentTerm.EP_Payment_Term_Code__c;
        }
    	if(string.isNotBlank(this.AccountObject.Parent.EP_Requested_Packaged_Payment_Term__c)){
    		EP_Payment_Term__c packagedPaymentTerm = paymentMapper.getRecordsByName(this.AccountObject.Parent.EP_Requested_Payment_Terms__c) ;
        	this.packagedPaymentTermCode = packagedPaymentTerm == null ? EP_Common_Constant.BLANK : packagedPaymentTerm.EP_Payment_Term_Code__c; 
    	}
	}
    
	/**
	* @author 			Accenture
	* @name				checkPageAccess
	* @date 			2/13/2017
	* @description 		This method will be use to redirect user at Error Page if they are trying to access this page without passing secret Code.
	* @param 			NA
	* @return 			PageReference
	*/
	public PageReference checkPageAccess() {
        EP_GeneralUtility.Log('Public','EP_ShipToPricingEnginePayloadCtrlExtn','checkPageAccess');
        PageReference pageRef = null;
        if(! EP_OutboundMessageUtil.isAuthorized(this.secretCode)) {
            pageRef =  EP_OutboundMessageUtil.redirectToErrorPage();
        }
        return pageRef;
    }
}