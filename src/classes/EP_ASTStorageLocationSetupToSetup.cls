/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageLocationSetupToSetup>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 04-Account Set-up to 04-AccountSet-up>
*  @Version <1.0>
*/
public class EP_ASTStorageLocationSetupToSetup extends EP_AccountStateTransition {

    public EP_ASTStorageLocationSetupToSetup () {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationSetupToSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationSetupToSetup', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageLocationSetupToSetup',' isGuardCondition');        
        return true;
    }

    public override void doOnExit(){

    }
}