@isTest
public class EP_ASTStorageShipToProspectToProspect_UT
{

    static final string EVENT_NAME = '01-ProspectTo01-Prospect';
    static final string INVALID_EVENT_NAME = '05-ProspectTo08-Rejected';
    
     /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
        
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationProspectToProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTStorageShipToProspectToProspect ast = new EP_ASTStorageShipToProspectToProspect();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationProspectToProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTStorageShipToProspectToProspect ast = new EP_ASTStorageShipToProspectToProspect();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationProspectToProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTStorageShipToProspectToProspect ast = new EP_ASTStorageShipToProspectToProspect();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationProspectToProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(INVALID_EVENT_NAME);
        EP_ASTStorageShipToProspectToProspect ast = new EP_ASTStorageShipToProspectToProspect();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getASTStorageLocationProspectToProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(EVENT_NAME);
        EP_ASTStorageShipToProspectToProspect ast = new EP_ASTStorageShipToProspectToProspect();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
}