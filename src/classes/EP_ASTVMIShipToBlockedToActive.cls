/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToBlockedToActive>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 06-Blocked to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToBlockedToActive extends EP_AccountStateTransition {

    public EP_ASTVMIShipToBlockedToActive() {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBlockedToActive','doOnExit');

    }
}