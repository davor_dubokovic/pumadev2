@isTest
public class EP_OrderStateAccepted_UT
{
    public static String EVENT_NAME = 'AcceptedAcknowledged';
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_OrderStateAccepted.getTextValue();
        Test.stopTest();
        System.AssertEquals(EP_OrderConstant.OrderState_Accepted, result);     
    }
    static testMethod void doOnEntry_test() {
        EP_OrderStateAccepted localObj = new EP_OrderStateAccepted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Accepted);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();  
        System.Assert(true);   
        // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
    }
    static testMethod void doOnExit_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_OrderStateAccepted localObj = new EP_OrderStateAccepted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();
        obj.previousStatus = EP_Common_Constant.SUBMITTED;
        obj.getOrder().EP_Use_Managed_Transport_Services__c = 'False';
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Accepted);
        localObj.setOrderContext(obj,oe);
        system.debug('@@@@'+obj.getOrder().EP_Use_Managed_Transport_Services__c+'----'+obj.isRetrospective()+'@@@@'+obj.previousStatus);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();     
        List<EP_IntegrationRecord__c> intRec = new List<EP_IntegrationRecord__c>([SELECT Id FROM EP_IntegrationRecord__c]);
        System.assert(intRec.size() > 0);
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_OrderStateAccepted localObj = new EP_OrderStateAccepted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Accepted);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    static testMethod void doTransition_PositiveScenariotest() {
        EP_OrderStateAccepted localObj = new EP_OrderStateAccepted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EVENT_NAME);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    // There is no Negative test for doTransition()
    /*static testMethod void doTransition_NegativeScenariotest() {
        EP_OrderStateAccepted localObj = new EP_OrderStateAccepted();
        EP_OrderDomainObject obj = getNonVMINonConsignmentOrderNegativeScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(false,result);
    }*/
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateAccepted localObj = new EP_OrderStateAccepted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.OrderState_Accepted);
        localObj.setOrderContext(obj,oe);    
        EP_OrderDomainObject currentOrder = obj;  
        Test.startTest();
        localObj.setOrderDomainObject(currentOrder);
        Test.stopTest();
        System.assertEquals(true, localObj.order == currentOrder);
    }

}