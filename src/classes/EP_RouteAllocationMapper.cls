/** 
   @Author          Shakti Mehtani
   @name            EP_RouteAllocationMapper
   @CreateDate      12/29/2016
   @Description     This class contains all SOQLs related to Route Allocation Mapper Object
   @Version         1.0
   @reference       NA
*/

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    
    -------------------------------------------------------------------------------------------------------------
    *************************************************************************************************************
*/
public with sharing class EP_RouteAllocationMapper {
    /**
    *  Constructor. 
    *  @name            EP_RouteAllocationMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */
    public EP_RouteAllocationMapper() {
        
    } 
    
    
   /**  Returns Not Null Ship To Route Allocation records
    *  @author          Shakti Mehtani
    *  @date            12/29/2016
    *  @name            getNotNullRouteAllocShipToRecs
    *  @param           set<id>
    *  @return          list<EP_Route_Allocation__c>
    *  @throws          NA
    */
    public list<EP_Route_Allocation__c> getNotNullRouteAllocShipToRecs(set<Id> idSet) {
        list<EP_Route_Allocation__c> routeAList = new list<EP_Route_Allocation__c>(); 
        
        for (list<EP_Route_Allocation__c> routeAlloctObjList: [     SELECT 
                                                                    id
                                                                    , EP_Ship_To__c
                                                                    , EP_Route__c 
                                                                FROM EP_Route_Allocation__c 
                                                                WHERE EP_Ship_To__c = :idSet 
                                                                AND EP_Route__c != NULL
                                                            ] ) {
                routeAList.addAll(routeAlloctObjList); 
        }       
        return routeAList; 
    }      
}