/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToBlockedToBlocked>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 06-Blocked to 06-Blocked>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToBlockedToBlocked extends EP_AccountStateTransition {

    public EP_ASTStorageShipToBlockedToBlocked () {
        finalState = EP_AccountConstant.BLOCKED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBlockedToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBlockedToBlocked', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBlockedToBlocked',' isGuardCondition');        
        return true;
    }
}