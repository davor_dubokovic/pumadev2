@isTest
public class EP_ShipToPricingEnginePayloadXML_UT {
    static testMethod void init_testPositive() {
    	EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_ShipToPricingEnginePayloadXML objLocal = new EP_ShipToPricingEnginePayloadXML();
        objLocal.recordId = objAccDomain.localAccount.id;
        Test.startTest();
        objLocal.init();
        Test.stopTest();        
        System.assertEquals( true,objLocal.lstShipToAddresses.size()>0);         
    }
	static testMethod void createPayload_testPositive() {
    	EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_ShipToPricingEnginePayloadXML objLocal = new EP_ShipToPricingEnginePayloadXML();
        objLocal.recordId =objAccDomain.localAccount.Id;
        Test.startTest();
        objLocal.init();
        objLocal.isEncryptionEnabled =true;
        objLocal.createPayload();
        Test.stopTest();         
        System.assertEquals( true,objLocal.lstShipToAddresses.size()>0);        
    }
	static testMethod void addShipToAddresses_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_ShipToPricingEnginePayloadXML objLocal = new EP_ShipToPricingEnginePayloadXML();
        objLocal.recordId = objAccDomain.localAccount.id;
        Test.startTest();
        objLocal.init();
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.addShipToAddresses(customersNode);
        Test.stopTest();         
        System.assertEquals( true,customersNode.getChildren().size()>=0);        
    }
	static testMethod void addCustBankNode_testPositive() {
        EP_AccountDomainObject objAccDomain =EP_TestDataUtility.getNonVMIShipToASBasicDataSetupDomainObjectPositiveScenario() ;
        EP_TestDataUtility.createBankAccount(objAccDomain.localAccount.ParentId);
        EP_ShipToPricingEnginePayloadXML objLocal = new EP_ShipToPricingEnginePayloadXML();
        objLocal.recordId = objAccDomain.localAccount.id;
        Test.startTest();
        objLocal.init(); 
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        objLocal.addCustBankNode(customersNode);
        Test.stopTest();
        System.assertEquals( true,customersNode.getChildElements().size()>=0);
    }

}