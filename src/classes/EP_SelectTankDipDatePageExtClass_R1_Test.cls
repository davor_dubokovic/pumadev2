/* 
  @Author <Abhay Aror>
   @name <EP_SelectTankDipDatePageExtClass_R1_Test>
   @CreateDate <10/16/2015>
   @Description <This class is used by the users to select the site that they want to submit a tank dip for>
   @Version <1.0>
 
*/
@isTest
private class EP_SelectTankDipDatePageExtClass_R1_Test{
    
    private static Account accObj; 
    private static List<EP_Tank_Dip__c> tankListDips; 
    private static EP_Tank__c TankInstance; 
    
    private static final String RET_PARAM = 'ret';
    
    /**************************************************************************
    *@Description : This method is used to create Data.                       *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/    
    private static void init(){
        EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=EP_TestDataUtility.createCustomerHierarchyForNAV(2);
        accObj = customHierarchy.lShipToAccounts[0];
        TankInstance = customHierarchy.lTanks[0];
        Account acc = [SELECT Id FROM Account WHERE id=:accObj.id LIMIT 1];
         
        updateAccountStatus();
        accObj.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;    
        update accObj;
        
        //Updating the tank record to be 'Basic Data Setup'
        TankInstance=customHierarchy.lTanks[0];
        TankInstance.EP_Tank_Status__c= EP_Common_Constant.BASIC_DATA_SETUP;
        update TankInstance;
        
        //Updating the tank record to be 'Operational'
        TankInstance.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        update TankInstance;
         
        acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        acc.EP_Tank_Dips_Schedule_Time__c = '09:00';
        Database.update(acc, FALSE);
         
        tankListDips = EP_TestDataUtility.createTestEP_Tank_Dip(new List<EP_Tank_Dip__c >{new EP_Tank_Dip__c(),
                                                                                           new EP_Tank_Dip__c(),
                                                                                           new EP_Tank_Dip__c()},
                                                                                                    TankInstance);
    }
    
    /**************************************************************************
    *@Description : This method is used to .                                  *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/        
    private static testMethod void testDatePageRedirect(){ 
        String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get('Placeholder').getRecordTypeId();   
        
        init();   
        
        Test.startTest();
            //EP_AccountTriggerHandler.isExecuteAfterUpdate = TRUE;
            //EP_AccountTriggerHandler.isExecuteBeforeUpdate = TRUE;
            Account acc =[SELECT Id FROM Account WHERE Id = :accObj.Id LIMIT 1];
            acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            acc.EP_Tank_Dips_Schedule_Time__c = '09:00';
            Database.update(acc, FALSE);
            tankListDips[0].EP_Reading_Date_Time__c = System.today()-4;//Updating tank date as today minus 4days
            tankListDips[0].EP_Tank__c = tankInstance.id;
            Database.update(tankListDips[0], FALSE);
            tankListDips[1].EP_Reading_Date_Time__c = System.today()-20;//updating tank date as today minus 20days
            tankListDips[1].RecordTypeId = recType;
            Database.update(tankListDips[1], FALSE);
            tankListDips[2].EP_Reading_Date_Time__c=System.today()-6;//Updating tank date as today minus 6days
            tankListDips[2].RecordTypeId = recType;
            Database.update(tankListDips[2], FALSE);
            PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
            PageReference pgRef=controller.retrieveMissingSiteDates();
        Test.stopTest();  
        
        System.assertEquals(true,tankListDips.size()==3); //Asserting actual tank dip size
        System.assertEquals(true,controller.tankDipDates.size()==3); //Assering missing tank dips
    } 
    
    /**************************************************************************
    *@Description : This method is used to test the cancel button functionality *
    *@Params      : none                                                        *
    *@Return      : void                                                        *    
    ****************************************************************************/  
    private static testMethod void testTankDipCancel(){ 
        PageReference pgRef;
        init(); 
          
        Test.startTest();
            Account acc =[SELECT Id FROM Account WHERE Id = :accObj.id LIMIT 1];
            acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(acc, FALSE);
            
            PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            ApexPages.currentPage().getParameters().put('date', '20151102');
            ApexPages.currentPage().getParameters().put('pageNumber', '1');
            ApexPages.currentPage().getParameters().put(RET_PARAM, EP_Common_Constant.ACCOUNT_OBJ);
            
            EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
            controller.retrieveMissingSiteDates();
            
            pgRef = controller.cancel();
            System.assertEquals('/home/home.jsp', pgRef.getUrl()); //Asserting url after clicking on cancel button
            
            // Fix for Defect 29809 - Start
            pgRef = controller.back();
            System.assertNotEquals(NULL, pgRef); //Asserting url after clicking on back button
            System.assertEquals(1, controller.intPageNumber);
            System.assertEquals(TRUE, controller.blnRedirectToAccountPage);
            // Fix for Defect 29809 - End
            
            // Fix for Defect 30169 - Start
            System.assertNotEquals(NULL, controller.isSiteMissingDipsForToday);
            // Fix for Defect 30169 - End
        Test.stopTest(); 
        
    }
    
    /**************************************************************************
    *@Description : This method is used to test tje date error handling         *
    *@Params      : none                                                        *
    *@Return      : void                                                        *    
    ****************************************************************************/  
    private static testMethod void testDateErrorHandling(){ 
        PageReference pgRef;
        init(); 
          
        Test.startTest();
            Account acc =[SELECT Id FROM Account WHERE Id = :accObj.id LIMIT 1];
            acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(acc, FALSE);
            
            PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
            Test.setCurrentPage(pageRef);//Applying page context here  
            ApexPages.currentPage().getParameters().put('id', accObj.Id);
            ApexPages.currentPage().getParameters().put('date', '201aa102'); // Set invalid date
            
            // Fix for Defect 29809 - Start
            EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
            controller.retrieveMissingSiteDates();
            // The system should set the selected date to NULL
            System.assertEquals(NULL, controller.dSelectedDate);
            // Fix for Defect 29809 - End
            
        Test.stopTest(); 
        
    }
    
    /**************************************************************************
    *@Description : This method is used to test the back button functionality *
    *@Params      : none                                                      *
    *@Return      : void                                                      *  
    *@Author      : Jai                                                       *
    *@Date        : 25/05/2016                                                *
    **************************************************************************/  
    private static void updateAccountStatus()
    {          
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;                         
    }
    
    /**************************************************************************
    *@Description : This method is used to test the back button functionality *
    *@Params      : none                                                      *
    *@Return      : void                                                      *    
    **************************************************************************/  
     private static testMethod void testTankDipback(){ 
         init();   
         Test.startTest();
             Account acc =[Select id from account where id=:accObj.id LIMIT 1];
             acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
             Database.update(acc, false);
             PageReference pageRef = Page.EP_SelectTankDipDatePage_R1;
             Test.setCurrentPage(pageRef);//Applying page context here  
             ApexPages.currentPage().getParameters().put('id', accObj.Id);
             // Fix for Defect 29809 - Start
             ApexPages.currentPage().getParameters().put('date', '20151102084124');
             // Fix for Defect 29809 - End
             
             EP_SelectTankDipDatePageExtnClass_R1 controller = new EP_SelectTankDipDatePageExtnClass_R1();
             
             controller.back();
             
         Test.stopTest(); 
         System.assertEquals('/apex/ep_selecttankdipdatepage_r1?date=20151102084124&id='+accObj.Id,pageRef.getUrl());//Aserting url after clicking on back button 
    }      
}