@isTest
public class EP_StorageLocationASActive_UT
{
    static final string EVENT_NAME = '05-ActiveTo05-Active';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
        
    static testMethod void setAccountDomainObject_test() {
        EP_StorageLocationASActive localObj = new EP_StorageLocationASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);         
        EP_AccountDomainObject currentAccount = obj  ;         
        Test.startTest();
        localObj.setAccountDomainObject(currentAccount);
        Test.stopTest();        
        System.Assert(localObj.account != NULL);
         
    }
    static testMethod void doOnEntry_test() {
        EP_StorageLocationASActive localObj = new EP_StorageLocationASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.Assert(true);
        // **** IMPLEMENT ASSERT ~@~ *****
        //System.AssertEquals(true,<asset conditions>);
        // **** TILL HERE ~@~ *****
    }
    static testMethod void doOnExit_test() {
        EP_StorageLocationASActive localObj = new EP_StorageLocationASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.Assert(true);
        // **** IMPLEMENT ASSERT ~@~ *****
        //System.AssertEquals(true,<asset conditions>);
        // **** TILL HERE ~@~ *****
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_StorageLocationASActive localObj = new EP_StorageLocationASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_StorageLocationASActive localObj = new EP_StorageLocationASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASActiveDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size()>0);
    }

    /* isInboundTransitionPossible does not have negative scenerio because it does not have implementation. It always returns true */         
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_StorageLocationASActive localObj = new EP_StorageLocationASActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
  }