@isTest
/**
 * @author <Spiros Markantonatos>
 * @name EP_OrderExportControllerTest 
 * @description <This class is the test class of the EP_OrderExportController class> 
 * @version <1.0>
*/
public class EP_OrderExportControllerTest {
    
    private static Company__c testCompany;
    private static Account testSellToAccount;
    private static Account testShipToAccount;
    private static Account testTransporterAccount;
    private static Account testTransporterAccount2;
    private static User testCSCUser;
    private static User testSystemAdminUser;
    private static Order testOrder;
    private static Order testOrder2;
    private static Product2 testProduct;
    private static Pricebook2 testPricebook;
    private static PricebookEntry testPricebookEntry;
    
    // Const
    private static final String TRANSPORTER_TYPE = 'Transporter';
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    
    /*
     This method will create all the related test data to test the controller
    */
    private static void createTestData() {
        
        List<Profile> lTestProfiles = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.CSC_AGENT_PROFILE];
        
        System.assertEquals(1, lTestProfiles.size());
        
        testCSCUser = EP_TestDataUtility.createUser(lTestProfiles[0].Id);
        
        lTestProfiles = [SELECT Id FROM Profile WHERE Name = :EP_Common_Constant.ADMIN_PROFILE];
        
        System.assertEquals(1, lTestProfiles.size());
        
        testSystemAdminUser = EP_TestDataUtility.createUser(lTestProfiles[0].Id);
        
        System.runAs(testSystemAdminUser) {
        
            //EP_OrderTriggerHandler.isExecuted = FALSE;
            //EP_OrderUpdateHandler.AfterUpdateOrder = FALSE;
            //EP_OrderTriggerHandler.isExecuteOnDraftStatus = FALSE;
            
            Id orderConfigCountryRangeRecordType = 
                                    EP_Common_Util.fetchRecordTypeId(
                                                EP_Common_Constant.ORDER_CONFIG_OBJ,
                                                            EP_Common_Constant.COUNTRY_RANGE);
                                                                                        
            Id orderConfigNonMixingRecordType = 
                                    EP_Common_Util.fetchRecordTypeId(
                                                EP_Common_Constant.ORDER_CONFIG_OBJ,
                                                            EP_Common_Constant.NON_MIXING_RANGE);
            
            EP_Payment_Method__c testPaymentMethod = 
                                    EP_TestDataUtility.createPaymentMethod('Cash Payment', 'CP');
            Database.insert(testPaymentMethod);
        
            EP_Payment_Term__c testPaymentTerm =  EP_TestDataUtility.createPaymentTerm();
            Database.insert(testPaymentTerm);
        
            testCompany = EP_TestDataUtility.createCompany('AUN');
            testCompany.EP_Disallow_DD_for_PP_customers__c = TRUE;
            Database.insert(testCompany);
        
            EP_Country__c testCountry = 
                                EP_TestDataUtility.createCountryRecord(
                                                                COUNTRY_NAME, 
                                                                    COUNTRY_CODE, 
                                                                        COUNTRY_REGION);
            Database.insert(testCountry);
        
            EP_Region__c testRegion = EP_TestDataUtility.createCountryRegion(REGION_NAME, testCountry.Id);  
            Database.insert(testRegion);
        
            // Create Product Structure
            EP_TestDataUtility.ProductStructureClass testProductStructure = 
                                                            EP_TestDataUtility.createTestProductStructure(
                                                                                                    testCompany.Id);
            
            testProduct = testProductStructure.product;
            testPricebook = testProductStructure.pricebook;
            testPricebookEntry = testProductStructure.pbe;
            //EP_AccountTriggerHandler.isExecuteAfterInsert = true;
            //EP_AccountTriggerHandler.isExecuteBeforeInsert = true;
            EP_UserTriggerHandler_R1.isExecuteBeforeInsert = false;
            EP_UserTriggerHandler_R1.isExecuteAfterInsert = false;
            EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy = 
                                                                    EP_TestDataUtility.createCustomerHierarchyForNAV(1);
            
            
            testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
            testShipToAccount = testAccountHierarchy.lShipToAccounts[0];
            //Test Class Fix Start
            testShipToAccount.EP_Puma_Company__c = testCompany.Id;
            update testShipToAccount;
            //Test Class Fix End
            // Create country range order configuration record
            EP_Order_Configuration__c testOrderConfigurationRecord = 
                                            EP_TestDataUtility.createOrderConfigRecord(
                                                                        'TEST ORDER CONFIG', 
                                                                            orderConfigCountryRangeRecordType, 
                                                                                0,
                                                                                    100000,
                                                                                        'LT',
                                                                                            testCountry.Id,
                                                                                                testProduct.Id);
                                                                                            
            BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours 
                                                    WHERE IsActive = TRUE 
                                                        AND IsDefault = TRUE 
                                                            LIMIT :EP_Common_Constant.ONE];
        
            Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);
            
            Database.insert(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(testStorageLocation);
            
            // Adding Run and Route
            EP_Route_Allocation__c testRouteAllocation = 
                                        EP_TestDataUtility.createTestRouteAllocation(
                                                                        testStorageLocation.Id, 
                                                                            testShipToAccount.Id);
            
            // Create a supply location
            
            Id strSupplyLocationSellToRecordTypeId = 
                                EP_Common_Util.fetchRecordTypeId(
                                                    'EP_Stock_Holding_Location__c', 
                                                        EP_Common_Constant.EX_RACK_REC_TYPE_NAME);
            
            
            EP_Stock_Holding_Location__c testSupplyOption1 = 
                                            EP_TestDataUtility.createSellToStockLocation(
                                                testSellToAccount.Id,
                                                    TRUE,
                                                        testStorageLocation.Id,
                                                            strSupplyLocationSellToRecordTypeId);
            testSupplyOption1.EP_Trip_Duration__c = 1;
            testSupplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption1.EP_Is_Pickup_Enabled__c = True;
            testSupplyOption1.EP_Duty__c = 'Excise Free';
            Database.insert(testSupplyOption1);
            
            testTransporterAccount = 
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE, 
                                                                    '123456NAV', 
                                                                        testCompany.Id);
            Database.insert(testTransporterAccount);
            
            Id strSupplyLocationDeliveryRecordTypeId = 
                                    EP_Common_Util.fetchRecordTypeId(
                                                    'EP_Stock_Holding_Location__c',
                                                        EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSupplyOption2 = 
                                                EP_TestDataUtility.createShipToStockLocation(
                                                    testShipToAccount.Id,
                                                        TRUE,
                                                            testStorageLocation.Id,
                                                                strSupplyLocationDeliveryRecordTypeId);
            testSupplyOption2.EP_Trip_Duration__c = 1;
            testSupplyOption2.EP_Ship_To__c = testShipToAccount.Id;
            testSupplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption2.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption2.EP_Is_Pickup_Enabled__c = TRUE;
            testSupplyOption2.EP_Duty__c = 'Excise Free';
            testSupplyOption2.EP_Transporter__c = testTransporterAccount.Id;
            Database.insert(testSupplyOption2);
            
            EP_Inventory__c testInventory = EP_TestDataUtility.createInventory(testStorageLocation.Id, testProduct.Id);
            Database.insert(testInventory);
            
            // Create a separate transporter
            testTransporterAccount2 = 
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE, 
                                                                    '789NAV', 
                                                                        testCompany.Id);
            Database.insert(testTransporterAccount2);
            
            EP_Supply_Location_Transporter__c 
                                        testSupplyLocationTransporter = 
                                                EP_TestDataUtility.createSupplyLocationTransporter(
                                                                                testTransporterAccount.Id,
                                                                                    testSupplyOption2.Id,
                                                                                        TRUE);
            
            Database.insert(testSupplyLocationTransporter);
            
        }
            
    }
    
    /*
     This method will create a test order record
    */
    private static void createTestOrder() {
        System.runAs(testCSCUser) {
            // Create a test order record
            String strNonVMIOrderRecordTypeId = 
                                EP_Common_Util.fetchRecordTypeId(
                                                EP_Common_Constant.ORDER, 
                                                    EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);
            
            testOrder = EP_TestDataUtility.createOrder(testSellToAccount.Id, 
                                                                strNonVMIOrderRecordTypeId, 
                                                                    testPricebook.Id);
            Database.insert(testOrder);
            
            OrderItem testOrderItem = new OrderItem(PricebookEntryId = testPricebookEntry.Id, 
                                                            UnitPrice = 1, 
                                                                Quantity = 1, 
                                                                    orderId = testOrder.Id);
            
            Database.insert(testOrderItem);
        }
    }
    
    /*
     This method will create a second test order with a separate transproter. This will be used to test the transporter error
    */
    private static void createSecondTestOrder() {
        System.runAs(testCSCUser) {
            //EP_OrderTriggerHandler.isExecuted = FALSE;
            //EP_OrderUpdateHandlerAfterUpdateOrder = FALSE;
            //EP_OrderTriggerHandler.isExecuteOnDraftStatus = FALSE;
            
            // Create a test order record
            String strNonVMIOrderRecordTypeId = 
                                EP_Common_Util.fetchRecordTypeId(
                                                EP_Common_Constant.ORDER, 
                                                    EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);
            
            testOrder2 = EP_TestDataUtility.createOrder(testSellToAccount.Id, 
                                                                strNonVMIOrderRecordTypeId, 
                                                                    testPricebook.Id);
            Database.insert(testOrder2);
            
            testOrder2.EP_Transporter__c = testTransporterAccount2.Id;
            Database.update(testOrder2);
            
            OrderItem testOrderItem = new OrderItem(PricebookEntryId = testPricebookEntry.Id, 
                                                            UnitPrice = 1, 
                                                                Quantity = 1, 
                                                                    orderId = testOrder2.Id);
            
            Database.insert(testOrderItem);
        }
    }
    
    /*
     This method will test the basic initiation of the page controller
    */
    private static testMethod void testOrderExportControllerInitiation() {
        
        createTestData();
            
        Test.startTest();
            System.runAs(testCSCUser) {
                createTestOrder();
                
                List<Order> lOrders = new List<Order>();
                lOrders.add(testOrder);
                
                Test.setCurrentPageReference(Page.EP_OrderExportPage); 
                System.currentPageReference().getParameters().put(
                                                    Label.EP_Selected_Order_Variable_Name, 
                                                            testOrder.Id);
                
                ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lOrders);
                
                EP_OrderExportController exportController = new EP_OrderExportController(sc);
                
                // Ensure that the contoller variables initialise correctly
                System.assertEquals(FALSE, exportController.blnDisplayExportButton); 
                System.assertEquals(FALSE, exportController.blnDisplayEmailButton);
                System.assertNotEquals(NULL, exportController.strEmailTemplateID); // The email template must be deployed to the system
            }
        Test.stopTest();
    }
    
    /*
     This method will test the loading of a single order
    */
    private static testMethod void testOrderExportOrderLoad() {
        createTestData();
        
        Test.startTest();
            System.runAs(testCSCUser) {
                createTestOrder();
                
                List<Order> lOrders = new List<Order>();
                lOrders.add(testOrder);
                ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lOrders);
                
                Test.setCurrentPageReference(Page.EP_OrderExportPage); 
                System.currentPageReference().getParameters().put(Label.EP_Selected_Order_Variable_Name, testOrder.Id);
                
                EP_OrderExportController exportController = new EP_OrderExportController(sc);
                exportController.initialiseExportController();
                
                // The system should be in a position to trigger the file export
                System.assertEquals(TRUE, exportController.blnDisplayExportButton);
                exportController.generateCSV();
                
                // The system should the same number of files as the number of file types 
                // with the order scope in the metadata object
                
                List<EP_Export_File_Type__mdt> lExistingFileTypes = [SELECT Id FROM EP_Export_File_Type__mdt 
                                                                        WHERE EP_Scope__c = :exportController.strExportScope];
                System.assertEquals(lExistingFileTypes.size(), exportController.lExportFileTypes.size());
                
                // The system should allow the user to email the files
                exportController.emailExportFiles();
                
                // After successfull export of the orders, the system should update the order record
                // However if the sandbox has email deliverability switched off, we cannot test this scenario as an E2E test
                // To test we will call the order update separately
                System.assertEquals(TRUE, exportController.updateOrders());
                
                List<Order> lUpdatedOrders = [SELECT Id, EP_3rd_Party_Managed_Exported__c 
                                                    FROM Order 
                                                        WHERE Id = :testOrder.Id];
                
                System.assertEquals(TRUE, lUpdatedOrders[0].EP_3rd_Party_Managed_Exported__c);
                
                // Prepare email
                System.assertNotEquals(NULL, exportController.prepareEmail());
                
                // Test order update exception
                // Intentially set the wrong ID to the order record
                exportController.lSelectedOrders[0].EP_Transporter__c = testCompany.Id;
                System.assertEquals(FALSe, exportController.updateOrders());
                
                // Defect 45019 - Start
                // Ensure that the method marking orders as exported returns NULL
                // The logic responsible for actually marking orders as exported is tested in the previous assert statement
                System.assertEquals(NULL, exportController.markOrdersAsExported());
                // Defect 45019 - End
                
                // Test email error handling
                List<Messaging.SendEmailResult> eMailResults = new List<Messaging.SendEmailResult>();
                System.assertEquals(FALSE, exportController.handleEmailErrors(eMailResults));
                
                // Ensure that the method displaying the process error is displaying the correct values
                try {
                    exportController.displayOrderUpdateConfirmation(TRUE);
                } catch (exception e) {
                    Boolean blnExpectedError = e.getMessage().contains(Label.EP_Third_Party_Order_Export_Email_Success);
                    System.AssertEquals(TRUE, blnExpectedError);
                }
                
                try {
                    exportController.displayOrderUpdateConfirmation(FALSE);
                } catch (exception e) {
                    Boolean blnExpectedError = e.getMessage().contains(Label.EP_Third_Party_Order_Update_Fail_Message);
                    System.AssertEquals(TRUE, blnExpectedError);
                }
            }
        Test.stopTest();
    }
    
    /*
     This method will test the loading of two orders with different transporters
    */
    private static testMethod void testOrderExportDoubleOrderTransportMismatchError() {
        createTestData();
        
        Test.startTest();
            System.runAs(testCSCUser) {
                createTestOrder();
                createSecondTestOrder();
                
                List<Order> lOrders = new List<Order>();
                lOrders.add(testOrder);
                lOrders.add(testOrder2);
                
                ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lOrders);
                
                Test.setCurrentPageReference(Page.EP_OrderExportPage); 
                System.currentPageReference().getParameters().put(
                                                    Label.EP_Selected_Order_Variable_Name, 
                                                        testOrder.Id + EP_Common_Constant.COMMA + testOrder2.Id);
                
                EP_OrderExportController exportController = new EP_OrderExportController(sc);
                exportController.initialiseExportController();
                
                // The controller should return FALSE, as the transporters do not match
                System.assertEquals(FALSE, exportController.blnDisplayExportButton);
            }
        Test.stopTest();
    }
    
    /*
     This method will test that the page controller can handle the scenario where no file definition metadata can be found
    */
    private static testMethod void testOrderExportMissingDefinitionErrorHandling() {
        createTestData();
        
        Test.startTest();
            System.runAs(testCSCUser) {
                createTestOrder();
                
                List<Order> lOrders = new List<Order>();
                lOrders.add(testOrder);
                ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lOrders);
                
                Test.setCurrentPageReference(Page.EP_OrderExportPage); 
                System.currentPageReference().getParameters().put(Label.EP_Selected_Order_Variable_Name, testOrder.Id);
                
                EP_OrderExportController exportController = new EP_OrderExportController(sc);
                
                // Set a random scope to simulate the scenario where the controller is unable to find any file definitions
                exportController.strExportScope = 'RANDOM123';
                exportController.initialiseExportController();
                
                System.assertEquals(FALSE, exportController.blnDisplayExportButton);
            }
        Test.stopTest();
    }
    
}