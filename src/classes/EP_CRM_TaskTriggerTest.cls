/* ================================================
 * @Class Name : EP_CRM_TaskTriggerTest
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_TaskTriggertrigger.
 * @created date: 17/04/2016
 ================================================*/
@isTest(seealldata=false)
private class EP_CRM_TaskTriggerTest{
     /* This method is used to create all test data and test before insert tasks schenario.
    * @param : 
    * @return: 
    */  
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Map<String,Trigger_Settings__c> trigerObj = Trigger_Settings__c.getAll();
        if(trigerObj.size() == 0){
            Trigger_Settings__c accountTriggerSetting = new Trigger_Settings__c(Name='EP_Account_Trigger', IsActive__c = false);
            database.insert(accountTriggerSetting); 
        }   
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    public static testMethod void testTaskInsertUpdateTrigger(){
        
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
           
            Database.SaveResult saveUer = Database.insert(u);
            System.runAs(u){
   
            // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            
            Database.SaveResult saveacc = Database.insert(acc);
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Closed Won';
            opp.Account=acc;
            opp.EP_CRM_Product_Interest_Group__c= 'Bulk Fuel';
            opp.EP_CRM_Primary_Product_Interest__c = 'Bulk Fuel';
            opp.EP_CRM_Current_Supplier__c = 'Freedom Fuels';
            opp.EP_CRM_Price_Type__c = 'Formula Price';
            opp.EP_CRM_Industry__c = 'Agriculture/Forestry';
            opp.EP_CRM_Margin_unit_of_measure__c = 123;
            opp.EP_CRM_Pricing_Approval__c = true;
            opp.EP_CRM_KYC_Done__c = true;
            opp.EP_CRM_Volume__c = 123;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            Database.SaveResult saveopp = Database.insert(opp);
            
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;

            Database.SaveResult savetempTask = Database.insert(tempTask);
            
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
            
            //update Event........
            tempTask.Subject = 'test subject';
            
            Database.SaveResult updatetempTask = Database.update(tempTask);
            Test.startTest();
             //Check System assert---------
            Opportunity tempOpp1 = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
            System.Assert(tempOpp1.EP_CRM_Last_Activity_Date__c != null);
                
            test.stopTest();
            }
        }
    /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testBulkTasktDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            Database.SaveResult saveUser = Database.insert(u);
            
            System.runAs(u){
            
             // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            Database.SaveResult saveacc = Database.insert(acc);
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Closed Won';
            opp.Account=acc;
            opp.EP_CRM_Product_Interest_Group__c= 'Bulk Fuel';
            opp.EP_CRM_Primary_Product_Interest__c = 'Bulk Fuel';
            opp.EP_CRM_Current_Supplier__c = 'Freedom Fuels';
            opp.EP_CRM_Price_Type__c = 'Formula Price';
            opp.EP_CRM_Industry__c = 'Agriculture/Forestry';
            opp.EP_CRM_Margin_unit_of_measure__c = 123;
            opp.EP_CRM_Pricing_Approval__c = true;
            opp.EP_CRM_KYC_Done__c = true;
            opp.EP_CRM_Volume__c = 123;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            Database.SaveResult saveopp = Database.insert(opp);
            
            //insert Event..............
            list<Task> lstTask = new list<Task>();
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;
            
            Task tempTask1 = new Task();
            tempTask1.OwnerId = UserInfo.getUserId();
            tempTask1.Subject='Donni';
            tempTask1.whatId = opp.Id;
            
            lstTask.add(tempTask );
            lstTask.add(tempTask1);
            
            Database.SaveResult[] savelstTask = Database.insert(lstTask);
            
            //delete Event........
            Database.deleteResult deletetempTask = Database.delete(tempTask);
            
            Test.startTest();
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
            test.stopTest();
            }
        }
        catch(Exception ex){}
    }
    
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testSingleTaskDeleteTrigger(){
        try{
            //Run as Sales User
            Profile p = [SELECT Id FROM Profile WHERE Profile.Name Like 'EP CRM Sales%' LIMIT 1]; 
            User u=new User();
            u.firstName='Test First';
            u.lastname='Sales CRM';
            u.profileid=p.id;
            u.Alias = 'salesman';
            u.Email='standardusercrm@accenture.com'; 
            u.EmailEncodingKey='UTF-8';
            u.LanguageLocaleKey='en_US'; 
            u.LocaleSidKey='en_AU';
            u.TimeZoneSidKey='Australia/Brisbane';
            u.UserName='standardusercrm@accenture.com';
            u.CurrencyIsoCode='AUD';
            Database.SaveResult saveUser = Database.insert(u);
            
            System.runAs(u){
                
             // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            Database.SaveResult saveacc = Database.insert(acc);
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            opp.Stagename='Closed Won';
            opp.Account=acc;
            opp.EP_CRM_Product_Interest_Group__c= 'Bulk Fuel';
            opp.EP_CRM_Primary_Product_Interest__c = 'Bulk Fuel';
            opp.EP_CRM_Current_Supplier__c = 'Freedom Fuels';
            opp.EP_CRM_Price_Type__c = 'Formula Price';
            opp.EP_CRM_Industry__c = 'Agriculture/Forestry';
            opp.EP_CRM_Margin_unit_of_measure__c = 123;
            opp.EP_CRM_Pricing_Approval__c = true;
            opp.EP_CRM_KYC_Done__c = true;
            opp.EP_CRM_Volume__c = 123;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            Database.SaveResult saveaopp = Database.insert(opp);
            
            //insert Event..............
           
            Task tempTask = new Task();
            tempTask.OwnerId = UserInfo.getUserId();
            tempTask.Subject='Donni';
            tempTask.whatId = opp.Id;
            
            Database.SaveResult savetempTask= Database.insert(tempTask);
            
            //delete Event........
            delete tempTask ;
            Database.saveResult deletetempTask = Database.insert(tempTask);
            
             Test.startTest();
            //Check System assert---------
            Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id limit 1];
            
            //validate through system Assert 
            System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c == null);
            test.stopTest();
            }
        }
        catch(Exception ex){}
    }
    //Defect 45313--Start
    public static testMethod void editTaskPermissionCheck_NegativeTest(){
        User kycUser = EP_TestDataUtility.createUserWithProfile(EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE);         
        System.runAs(kycUser){
            Account acc =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
            Database.insert(acc);
            Task tempTask = EP_TestDataUtility.createTask(acc.id, EP_Common_Constant.CREDIT_LIMIT_APPROVED_SUBJECT+' '+acc.name+'/'+acc.AccountNumber);    
            Database.insert(tempTask);   
            String errorMsg;
            try{
                Test.startTest();
                tempTask.status = EP_Common_Constant.COMPLETE;
                Database.update(tempTask);
                Test.stopTest();
            }catch(Exception exp){
                errorMsg = exp.getMessage();
            }
            System.assert(String.isNotEmpty(errorMsg));
        }
    }

    public static testMethod void deleteTaskPermissionCheck_NegativeTest(){
        User kycUser = EP_TestDataUtility.createUserWithProfile(EP_Common_Constant.PUMA_KYC_REVIEWER_PROFILE);   
        System.runAs(kycUser){
            Account acc =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
            Database.insert(acc);
            Task tempTask = EP_TestDataUtility.createTask(acc.id, EP_Common_Constant.CREDIT_LIMIT_APPROVED_SUBJECT+' '+acc.name+'/'+acc.AccountNumber);
            Database.insert(tempTask);   
            String errorMsg;
            try{
                Test.startTest();
                Database.delete(tempTask);
                Test.stopTest();
            }catch(Exception exp){
                errorMsg = exp.getMessage();
            }
            System.assert(String.isNotEmpty(errorMsg));
        }
    }
    public static testMethod void editTaskPermissionCheck_PostiveTest(){
        User cscUser = EP_TestDataUtility.createUserWithProfile(EP_Common_Constant.CSC_AGENT_PROFILE);    
        System.runAs(cscUser){
            Account acc =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
            Database.insert(acc);
            Task tempTask = EP_TestDataUtility.createTask(acc.id, EP_Common_Constant.CREDIT_LIMIT_APPROVED_SUBJECT+' '+acc.name+'/'+acc.AccountNumber);
            Database.insert(tempTask);   
            String errorMsg;
            try{
                Test.startTest();
                tempTask.status = EP_Common_Constant.COMPLETE;
                Database.update(tempTask);
                Test.stopTest();
            }catch(Exception exp){
                errorMsg = exp.getMessage();
            }
            Task taskIns = [Select id,status from Task where id =: tempTask.id];
            System.assert(String.isEmpty(errorMsg));
            System.assertEquals(EP_Common_Constant.COMPLETE, taskIns.status);
        }
    }
    public static testMethod void deleteTaskPermissionCheck_PostiveTest(){
        User cscUser = EP_TestDataUtility.createUserWithProfile(EP_Common_Constant.CSC_AGENT_PROFILE);   
        System.runAs(cscUser){
            Account acc =  EP_TestDataUtility.createSellToAccount(NULL, NULL);
            Database.insert(acc);

            Task tempTask = EP_TestDataUtility.createTask(acc.id, EP_Common_Constant.CREDIT_LIMIT_APPROVED_SUBJECT+' '+acc.name+'/'+acc.AccountNumber);
            Database.insert(tempTask);   
            String errorMsg;
            try{
                Test.startTest();
                tempTask.status = EP_Common_Constant.COMPLETE;
                Database.delete(tempTask);
                Test.stopTest();
            }catch(Exception exp){
                errorMsg = exp.getMessage();
            }            
            System.assert(String.isEmpty(errorMsg));
        }
    }
    public static testMethod void isSubjecthasCreditLimitCheck_PostiveTest(){
        Boolean creditCheck;
        Test.startTest();
        creditCheck = EP_CRM_TaskTriggerHandler.isSubjecthasCreditLimitCheck(EP_Common_Constant.CREDIT_LIMIT_APPROVED_SUBJECT);
        Test.stopTest();
        System.assertEquals(true,creditCheck);
    }
    public static testMethod void isSubjecthasCreditLimitCheck_NegativeTest(){
        Boolean creditCheck;
        Test.startTest();
        creditCheck = EP_CRM_TaskTriggerHandler.isSubjecthasCreditLimitCheck(EP_Common_Constant.Primary_Test);
        Test.stopTest();
        System.assertEquals(false,creditCheck);
    }
    public static testMethod void isProfileHasTaskModificationCheck_PostiveTest(){
        Boolean profileCheck;
        Test.startTest();
        profileCheck = EP_CRM_TaskTriggerHandler.isProfileHasTaskModificationCheck(EP_Common_Constant.CSC_AGENT_PROFILE);
        Test.stopTest();
        System.assertEquals(false, profileCheck);
    }
    public static testMethod void isProfileHasTaskModificationCheck_NegativeTest(){
        Boolean profileCheck;
        Test.startTest();
        profileCheck = EP_CRM_TaskTriggerHandler.isProfileHasTaskModificationCheck(EP_Common_Constant.CSC_AGENT_PROFILE);
        Test.stopTest();
        System.assertEquals(false, profileCheck);
    }
    //Defect 45313--END
}