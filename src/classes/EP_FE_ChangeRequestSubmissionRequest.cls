/*********************************************************************************************
     @Author <Jyutsana >
     @name <EP_FE_ChangeRequestSubmissionRequest >
     @CreateDate <12/04/2016>
     @Description < >  
     @Version <1.0>
    *********************************************************************************************/
global with sharing class EP_FE_ChangeRequestSubmissionRequest {

    global static final Integer ERROR_REQUEST_CANNOT_BE_NULL = -1;
    global static final Integer ERROR_CHANGE_REQUEST_CANNOT_BE_NULL = -2;
    global static final Integer ERROR_CHANGE_REQUEST_LINEITEM_CANNOT_BE_NULL = -3;

    global static final String DESCRIPTION1 = ' Request can not be Null';
    global static final String DESCRIPTION2 = ' Change Request can noy be Null';
    global static final String DESCRIPTION3 = ' Change Request LineItem can not be Null';

    public EP_ChangeRequest__c changeRequest  {get; set;}
    
    public List<EP_ChangeRequestLine__c> changeRequestLineItems {get; set;}
    
}