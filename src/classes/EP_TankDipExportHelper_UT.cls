@isTest
public class EP_TankDipExportHelper_UT
{
    //#60147 User Story Start
    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }
    //#60147 User Story End
    //need to review
    static testMethod void setTankDipAttributes_test() {
        LIST<EP_Tank_Dip__c> tankDipRecords = new LIST<EP_Tank_Dip__c>{EP_testDataUtility.createTankDip(true)};
        List<Product2> products = [SELECT Id, ProductCode from Product2];
        for(integer i =0;i<products.size();i++){
        	products[i].ProductCode=string.valueOf(i+1500);
        }
        update products;
        tankDipRecords = [Select Id, EP_Tank__r.EP_Ship_To__r.EP_Puma_Company_Code__c, EP_Tank__r.EP_Ship_To__r.Parent.AccountNumber, EP_Tank__r.EP_Ship_To__r.AccountNumber,LastModifiedDate, 
            EP_Tank_Nr__c, EP_Tank__r.EP_Product__r.ProductCode, EP_Ambient_Quantity__c, EP_Unit_Of_Measure__c, EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c, LastModifiedBy.Alias from EP_Tank_Dip__c];
		
            
        Test.startTest();
        EP_TankDipExportStub result = EP_TankDipExportHelper.setTankDipAttributes(tankDipRecords);
        Test.stopTest();
        System.AssertEquals(result.MSG.payload.any0.tankDips.tankDip.size(), 1);
    }
}