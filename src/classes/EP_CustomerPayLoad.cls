/**
   @Author          Sandeep Kumar
   @name            EP_CustomerPayLoad
   @CreateDate      01/13/2017
   @Description     This class contains payload methods for customer (account) object
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    2/2/2017            Sandeep Kumar           1. Simlified navBulkCustomersPayLoad using payService.createPayload
                                                2. Added EP_PayLoadHandler.EXTERNAL_SYSTEM = EP_Common_Constant.NAV_CUSTOMER; in constructor instead of methods
                                                3. Declare customerWrapperFields, shipTOWrapperFields, bankAccountWrapperFields variables at class level instead of methods.
                                                4. Removed try catch from private method
                                                5. Documentation done for methods with proper description
                                                6. Removed createNavCustomerEditPayLoad method as it was same as createNavCustomerPayLoad method. 
    *************************************************************************************************************
    */
    public with sharing class EP_CustomerPayLoad {
        private EP_PayLoadHelper payLoadHelper = null;
        private EP_BankAccountPayLoad bankAccountPayLoad = null;
        private EP_SObjectPayLoad sObjPayLoad = null;
        private EP_ShipToPayLoad shipToPayLoad = null;
        private EP_PayLoadService payLoadService = null;
        private map<String,String> mPaymentTerms = null;
        private EP_PayLoadHelper.WrapperFields customerWrapperFields = null;
        private EP_PayLoadHelper.WrapperFields shipTOWrapperFields = null;
        private EP_PayLoadHelper.WrapperFields bankAccountWrapperFields = null;  

    /**
    *  Constructor
    *  @name    EP_CustomerPayLoad
    *  @param   NA
    *  @return  NA
    *  @throws  NA
    */
    public EP_CustomerPayLoad() {
        payLoadHelper = new EP_PayLoadHelper();
        bankAccountPayLoad = new EP_BankAccountPayLoad();
        sObjPayLoad = new EP_SObjectPayLoad();
        shipToPayLoad = new EP_ShipToPayLoad();
        payLoadService = new EP_PayLoadService();
        EP_PayLoadHandler.EXTERNAL_SYSTEM = EP_Common_Constant.NAV_CUSTOMER;
        mPaymentTerms = payLoadHelper.createPaymentTermCodeMap();
        customerWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.ACCOUNT_OBJ, EP_PayLoadConstants.NAV_CUSTOMER_FIELDS);
        shipTOWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.ACCOUNT_OBJ                                       
           ,EP_PayLoadConstants.NAV_SHIP_TO_FIELDS);
        bankAccountWrapperFields = bankAccountWrapperFields = payLoadHelper.createFieldsWrapper(EP_Common_Constant.BANK_ACCOUNT_OBJ
            ,EP_PayLoadConstants.NAV_BANK_FIELDS);                                   
    }
    
    /**
    *  This method creates payload for customers whose ids are in list sCustomerId. 
    *  -Fields of customer, shipTo and BankAccount are fetched through fieldsets i.e EP_PayLoadConstants.NAV_CUSTOMER_FIELDS, EP_PayLoadConstants.NAV_SHIP_TO_FIELDS and EP_PayLoadConstants.NAV_BANK_FIELDS respectivily
    *  @name createNavBulkCustomerPayLoad
    *  @param set<Id> sCustomerId
    *  @return String
    *  @throws Generic Exception
    */
    public String createNavBulkCustomerPayLoad(set<Id> sCustomerId){
        EP_GeneralUtility.Log('Public','EP_CustomerPayLoad','createNavBulkCustomerPayLoad');
        String navBulkCustomersPayLoad = EP_Common_Constant.BLANK;
        String navBulkShipToAddresses = EP_Common_Constant.BLANK;
        String navBulkBankAccounts = EP_Common_Constant.BLANK;
        String statusSetUp = EP_Common_Constant.STATUS_SET_UP;
        String query = EP_Common_Constant.SELECT_STRING;
        List<String> shipTOAdressTypes = new List<String>{EP_Common_Constant.VMI_SHIP_TO_DEV_NAME, EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME}; 
        try{            
            //CREATE CUSTOMER HIERARCHY FOR NAV CUSTOMER        
            query += customerWrapperFields.fieldString 
            + EP_Common_Constant.COMMA_STRING
            + EP_PayLoadConstants.EP_ACCOUNT_COMPANY_NAME
            + EP_Common_Constant.COMMA_STRING 
            + EP_Common_Constant.LEFT_PARENTHESIS 
            + EP_Common_Constant.SELECT_STRING        
            + shipTOWrapperFields.fieldString 
            + EP_PayLoadConstants.FROM_CUSTOMER_SHIP_TOS  
            + EP_Common_Constant.LEFT_PARENTHESIS
            + EP_Common_Constant.SELECT_STRING
            + bankAccountWrapperFields.fieldString 
            + EP_PayLoadConstants.FROM_CUSTOMER_BANK_ACCOUNTS;

            for(Account customer : DataBase.Query(query) ){
                navBulkShipToAddresses = shipToPayLoad.createNavBulkShipToPayload(customer.ChildAccounts
                    ,shipTOWrapperFields.lFieldAPINames);
                
                navBulkBankAccounts = bankAccountPayLoad.createNavBulkBankAccountPayLoad(customer.Bank_Accounts__r
                    ,bankAccountWrapperFields.lFieldAPINames);

                payLoadHelper.mapPaymentTermCode(customer,mPaymentTerms);
                navBulkCustomersPayLoad += createNavCustomerPayLoad(customer
                    ,customerWrapperFields.lFieldAPINames
                    ,navBulkShipToAddresses
                    ,navBulkBankAccounts);

            }
            navBulkCustomersPayLoad =  payLoadService.createPayload(navBulkCustomersPayLoad, EP_Common_Constant.CUSTOMERS, null);

            //TEMP SOLUTION
            navBulkCustomersPayLoad =   navBulkCustomersPayLoad.replace(EP_Common_Constant.Identifier
                ,EP_Common_Constant.Identifier.toLowerCase());                          
            } catch (Exception handledException){
                throw handledException;
            }
            system.debug('====navBulkCustomersPayLoad===='+navBulkCustomersPayLoad);
            return navBulkCustomersPayLoad;
        }    

    /**
    *  This method creates pricing engine payload i.e payload for customer where its ids are in sCustomerId set 
       and its children shipto accounts where its ids are in sShipTo set.
    *  @name createPricingEnginePayLoad
    *  @param set<Id> sCustomerId, set<Id> sShipTo
    *  @return WrapperFields
    *  @throws Generic Exception
    */
    public String createPricingEnginePayLoad(set<Id>sCustomerId, set<Id>sShipTo){
        EP_GeneralUtility.Log('Public','EP_CustomerPayLoad','createPricingEnginePayLoad');

        String navBulkCustomersPayLoad = EP_Common_Constant.BLANK;
        String query = EP_Common_Constant.SELECT_STRING;
        
        String navBulkShipToAddresses = EP_Common_Constant.BLANK;
        String navBulkBankAccounts = EP_Common_Constant.BLANK; 
        String statusSetUp = EP_Common_Constant.STATUS_SET_UP;
        
        try{
            query += customerWrapperFields.fieldString 
            + EP_Common_Constant.COMMA_STRING
            + EP_PayLoadConstants.EP_ACCOUNT_COMPANY_NAME
            + EP_Common_Constant.COMMA_STRING 
            + EP_Common_Constant.LEFT_PARENTHESIS 
            + EP_Common_Constant.SELECT_STRING
            + shipTOWrapperFields.fieldString 
            + EP_PayLoadConstants.FROM_PRCNG_SHIP_TOS;

            for(Account customer : DataBase.Query(query) ){
                navBulkShipToAddresses = shipToPayLoad.createNavBulkShipToPayload(customer.ChildAccounts
                    ,shipTOWrapperFields.lFieldAPINames);            
                /** TEMPORARY FIX START **/
                payLoadHelper.mapPaymentTermCode(customer,mPaymentTerms);
                /** TEMPORARY FIX END **/
                navBulkCustomersPayLoad += createNavCustomerPayLoad(customer
                    ,customerWrapperFields.lFieldAPINames
                    ,navBulkShipToAddresses
                    ,EP_Common_Constant.BLANK);        
            }
            navBulkCustomersPayLoad =  payLoadService.createPayload(navBulkCustomersPayLoad, EP_Common_Constant.CUSTOMERS, null);
            
            /** TEMPORARY FIX START **/
            navBulkCustomersPayLoad =   navBulkCustomersPayLoad.replace(EP_Common_Constant.Identifier
                ,EP_Common_Constant.Identifier.toLowerCase());                          
            /** TEMPORARY FIX END **/
            } catch(Exception handledException){
                throw handledException;
            }        
            return navBulkCustomersPayLoad;
        }
        
    /**
    *  This method creates payload for customers edit Nav i.e payload for customer accounts whose ids are in sCustomer set.
    *  @name createNAVBulkCstmrEditPayLoad
    *  @param set<Id>sCustomer
    *  @return String
    *  @throws Generic Exception
    */
    public String createNAVBulkCstmrEditPayLoad(set<Id> sCustomer){
        EP_GeneralUtility.Log('Public','EP_CustomerPayLoad','createNAVBulkCstmrEditPayLoad');
        String query;
        String navBulkCustomersPayLoad;
        try{
            navBulkCustomersPayLoad = EP_Common_Constant.BLANK;
            query = EP_Common_Constant.SELECT_STRING
            + customerWrapperFields.fieldString
            + EP_PayLoadConstants.FROM_CUSTOMER_EDIT_NAV;
            for(Account customer : DataBase.query(query)){
                payLoadHelper.mapPaymentTermCode(customer,mPaymentTerms);

                navBulkCustomersPayLoad +=  createNavCustomerPayLoad(customer
                    ,customerWrapperFields.lFieldAPINames
                    ,payLoadHelper.openingTag(EP_Common_Constant.SHIP_TO_ADDRESSES)
                    ,payLoadHelper.openingTag(EP_Common_Constant.BANK_ACCOUNTS));
                
            }                
            navBulkCustomersPayLoad = payLoadService.createPayload(navBulkCustomersPayLoad, EP_Common_Constant.CUSTOMERS, null);               
            //TEMP SOLUTION
            navBulkCustomersPayLoad = navBulkCustomersPayLoad.replace(EP_Common_Constant.Identifier
                ,EP_Common_Constant.Identifier.toLowerCase()); 
            /** TEMPORARY FIX END **/            
            }catch(Exception handledException){
                throw handledException;
            }            
            return navBulkCustomersPayLoad;
        }

    /**
    *  Returns xml payload structure of customer for Sending request to IPASS  
    *  @name createCustomerRequest
    *  @param String msgId, String interfaceName, String srcCompany, String payload, String objectName                                             
    *  @return String
    *  @throws Generic Exception
    */
    public String createCustomerRequest(String msgId
       ,String interfaceName
       ,String srcCompany
       ,String payload
       ,String objectName
       ){
        EP_GeneralUtility.Log('Public','EP_CustomerPayLoad','createCustomerRequest');
        Integer nRows = EP_Common_Util.getQueryLimit();
        String sourceUpdateAddressInstance;
        XmlStreamWriter XmlSWriter;
        try{
            sourceUpdateAddressInstance = EP_COMMON_CONSTANT.SOURCE_RESPONSE_ADD_LS;/*EP_Common_Constant.CCL.equalsIgnoreCase(interfaceName)? EP_COMMON_CONSTANT.SOURCE_RESPONSE_ADD_LS
            :EP_COMMON_CONSTANT.SOURCE_RESP_ADD;  */     
            XmlSWriter = new XmlStreamWriter();
            XmlSWriter.writeStartDocument(null, EP_PayLoadConstants.ONE_POINT_ZERO);
            Map<String, String> nodeValueMap = new Map<String, String>();
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.INTERFACE_TYPE, interfaceName);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.SOURCE_COMPANY, srcCompany);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.SOURCE_RESP_ADD, EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(sourceUpdateAddressInstance).EP_Value__c);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.SOURCE_UPDATE_STTS_ADD, EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(sourceUpdateAddressInstance).EP_Value__c);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.TRANSPORT_STATUS, EP_PayLoadConstants.NEW_STR);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.CONT_ON_ERR_STTS, EP_Common_Constant.STRING_TRUE);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.COMP_LGNG, EP_Common_Constant.STRING_TRUE);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.UPDT_DEST_ON_DLVRY, EP_Common_Constant.STRING_TRUE);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.CALL_DEST_FOR_PRCSNG, 'true');
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.UPDT_SRC_ON_DLVRY, EP_Common_Constant.STRING_FALSE);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.UPDT_SRC_ON_RCV, EP_Common_Constant.STRING_FALSE);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.UPDT_SRC_AFTER_PRCSNG, EP_Common_Constant.STRING_FALSE);
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.OBJECT_TYPE, EP_Common_Constant.TABLE_STRING);                    
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.OBJECT_NAME, EP_INTEGRATION_CUSTOM_SETTING__c.getInstance(objectName).EP_Value__c);                        
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.COMMUNICATION_TYPE, EP_Common_Constant.ASYNC);           
            payLoadHelper.putValue(nodeValueMap, EP_Common_Constant.MSG_ID, msgId);           

            
            EP_ObjectFieldNodeSettingMapper nodeSettingMapper = new EP_ObjectFieldNodeSettingMapper();
            for(EP_ObjectFieldNodeSetting__mdt node : nodeSettingMapper.getRecByNodeName(nRows)){                    
                if(EP_Common_Constant.MSG.equalsIgnorecase(node.EP_NODE_NAME__c)
                    || EP_COMMON_CONSTANT.HEADER_COMMON.equalsIgnorecase(node.EP_NODE_NAME__c)){
                    XmlSwriter.writeStartElement(null,node.EP_NODE_NAME__c,null);
                }  
                else if(EP_COMMON_CONSTANT.PAYLOAD.equalsIgnoreCase(node.EP_NODE_NAME__c) 
                    && String.isNotBlank(payload)){
                    XmlSwriter.writeEndElement();
                    XmlSwriter.writeStartElement(null,node.EP_NODE_NAME__c,null);
                    XmlSwriter.writeAttribute(null,null,EP_Common_Constant.TYPE_String,EP_Common_Constant.REQUEST_STRING);
                    XmlSwriter.writeAttribute(null,null,EP_Common_Constant.ENCRYPTED_NODE,EP_Common_Constant.STRING_NO_LS.toLowercase());   
                }
                else if(EP_Common_Constant.ANY_NODE.equalsIgnoreCase(node.EP_NODE_NAME__c) 
                    && String.isNotBlank(payload)){
                    XmlSwriter.writeStartElement(null,node.EP_NODE_NAME__c,null);
                    XmlSwriter.writeCharacters(payload);
                    XmlSwriter.writeEndElement();
                    XmlSwriter.writeEndElement();   
                    } else {
                        payLoadHelper.addElement(xmlSWriter, nodeValueMap, node.EP_NODE_NAME__c);
                    }
                }
                XmlSwriter.writeEndDocument();
                }catch(Exception handledException){
                  throw handledException;
              }
              return XmlSWriter.getXMLString();            
          }

    /**
    *  This method creates customer payload for NAV
    *  @name createNavCustomerPayLoad
    *  @param Account customer, List<String>lFielsAPINames, String navBulkShipToAddresses, String navBulkBankAccounts){
    *  @return String
    *  @throws Generic Exception
    */
    private String createNavCustomerPayLoad(Account customer
        ,List<String>lFielsAPINames
        ,String navBulkShipToAddresses
        ,String navBulkBankAccounts){
        EP_GeneralUtility.Log('Private','EP_CustomerPayLoad','createNavCustomerPayLoad');
        String navCustomerPayLoad;
        String payloadWithIdentifier_str;

        String splitPoint;
        String splitPoint2 = EP_COMMON_CONSTANT.MONTHLY_TAG;
        list<String>lpayloadWithIdentifier = new list<String>();
        list<String>lPayload2 = new list<String>();
        try{
            //CREATE NAV CUSTOMER PAYLOAD
            //TO DO - SHIP-TO ADDRESSES AND BANKS
            payloadWithIdentifier_str= payLoadHelper.createPayLoadWithIdentifier(customer,lFielsAPINames);
            if(payloadWithIdentifier_str.containsIgnoreCase(EP_COMMON_CONSTANT.ALW_RLS_TAG)){
                splitPoint = EP_COMMON_CONSTANT.ALW_RLS_TAG;
            }
            else if(payloadWithIdentifier_str.containsIgnoreCase(EP_COMMON_CONSTANT.ALW_RLS_TAG_EMPTY)){
                splitPoint = EP_COMMON_CONSTANT.ALW_RLS_TAG_EMPTY;
                }else{

                }
                if(String.isNotBlank(payloadWithIdentifier_str)){
                    lpayloadWithIdentifier = payloadWithIdentifier_str.split(splitPoint);
                }

                if(lpayloadWithIdentifier.size()==2){

                    lPayload2 = lpayloadWithIdentifier[1].split(splitPoint2);

                    payloadWithIdentifier_str= lpayloadWithIdentifier[0] 
                    + (String.isNotBlank(splitPoint)?splitPoint:EP_Common_Constant.BLANK)  
                    + EP_COMMON_CONSTANT.AC_FRQ_TAG 
                    + lPayload2[0]
                    + splitPoint2 
                    + EP_COMMON_CONSTANT.AC_FRQ_TAG_CLOSE
                    + lPayload2[1];               
                }            
                String additionalTags = navBulkShipToAddresses
                + navBulkBankAccounts    
                + payLoadService.createPayload(customer.EP_Puma_Company_Code__c, 
                  EP_Common_Constant.CLIENT_ID, null);                                
                navCustomerPayLoad = payLoadService.createPayload(payloadWithIdentifier_str,
                    EP_Common_Constant.CUSTOMER, additionalTags);
                } catch(Exception handledException){
                    throw handledException;
                }       
                return navCustomerPayLoad;
            }  
        }