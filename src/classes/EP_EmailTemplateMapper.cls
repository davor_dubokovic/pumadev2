/**
   @Author          Swapnil Gorane
   @name            EP_EmailTemplateMapper 
   @CreateDate      11/01/2017 
   @Description     This class contains all SOQLs related to EmailTemplate Object and also the database operational methods
   @Version         1.0
   @reference       NA
   */

/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    11/01/2017          Swapnil Gorane          Added Method -> getEmailTemplateIdAsPerName(String),
                                                                getEmailTemplateDetails(String)
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
    */
    public with sharing class EP_EmailTemplateMapper {
    /**  Constructor.
    *  @name             EP_EmailTemplateMapper 
    *  @param            NA
    *  @return           NA
    *  @throws           NA
    */  
    public EP_EmailTemplateMapper () {
        
    }
    
    /** This method is used to get email template Id as per given developer name
    *  @date            13/02/2017
    *  @name            getEmailTemplateIdAsPerName
    *  @param           String 
    *  @return          String 
    *  @throws          NA
    */
    public EmailTemplate getEmailTemplateAsPerName(String emailTempName) {
        EP_GeneralUtility.Log('Public','EP_EmailTemplateMapper','getEmailTemplateAsPerName');
        
        EmailTemplate emailTempObj =  [SELECT Id, Subject, HtmlValue, Body 
        FROM EmailTemplate 
        WHERE DeveloperName = :emailTempName LIMIT :EP_Common_Constant.ONE];
        return emailTempObj;
    }

    /** This method is used to get email template from id
    *  @date            29/12/2017
    *  @name            getEmailTemplatesById
    *  @param           Id 
    *  @return          EmailTemplate Object 
    */
    public static EmailTemplate getEmailTemplatesById(Id templateId){
        EP_GeneralUtility.Log('Public','EP_EmailTemplateMapper','getEmailTemplatesById');
        return([SELECT Id, Subject, HtmlValue, Body,TemplateType 
                                          FROM EmailTemplate WHERE id=:templateId]);
    }
    
}