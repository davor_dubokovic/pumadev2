/**
 * @author <Jai Singh>
 * @name <EP_ContractTriggerHelper>
 * @createDate <01/08/2016>
 * @description <This is helper class for Contract trigger>
 * @version <1.0>
 */
public with sharing class EP_ContractTriggerHelper{
	
	private static final String CHECK_OPEN_ORDER  = 'checkOpenOrder';
	
	/**
	 * @author <Jai Singh>
	 * @description <Methdod to check open orders>
	 * @name <checkOpenOrder>
	 * @date <01/08/2016>
	 * @param Set<Id>, Map<Id, Contract>
	 * @return void
	 */
	public static void checkOpenOrder(Set<Id> setContractIds, Map<Id, Contract> mapNewContractIdContracts){
		try{
			Set<Id> setContractIdsWithOpenOrder = new Set<Id>();
			Integer nRows = EP_Common_Util.getQueryLimit();
			for(OrderItem orderItemObject : [Select Id, EP_Contract__c from OrderItem where Order.Status =: EP_Common_Constant.ORDER_STATUS_SUBMITTED AND Order.EP_Order_Epoch__c =: EP_Common_Constant.EPOC_RETROSPECTIVE AND EP_Contract__c IN: setContractIds Limit :nRows]){
				setContractIdsWithOpenOrder.add(orderItemObject.EP_Contract__c);
			}
			for(Id contractId : setContractIds){
				if(setContractIdsWithOpenOrder.contains(contractId)){
					mapNewContractIdContracts.get(contractId).Status.addError(Label.EP_PurchaseContractOnHoldError);
				}
			}
		}catch(Exception ex){
			EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, CHECK_OPEN_ORDER, EP_ContractTriggerHelper.class.getName(), ApexPages.Severity.ERROR);
		}
	}
}