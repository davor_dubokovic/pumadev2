/*
 *  @Author <Bhushan Adhikari>
 *  @Name <EP_SellToASTBasicDataSetupToAccountSetUp>
 *  @CreateDate <08/02/2017>
 *  @Description <Handles Account status change from Basic Data Setup to Account setup>
 *  @Version <1.0>
 */
 public with sharing class EP_SellToASTBasicDataSetupToAccountSetUp extends EP_AccountStateTransition{
    public EP_SellToASTBasicDataSetupToAccountSetUp() {
        finalState = '04-Account Set-up';
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_SellToASTBasicDataSetupToAccountSetUp','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_SellToASTBasicDataSetupToAccountSetUp','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_SellToASTBasicDataSetupToAccountSetUp','isGuardCondition');
        return false;
    }
    
}