@isTest
public class EP_Delivery_UT
{

    static testMethod void sendEmailForUnDelivered_test() {
        EP_Delivery localObj = new EP_Delivery();
        Integer emailLimits = Limits.getEmailInvocations();
        csord__Order__c objOrder =  EP_TestDataUtility.getCurrentOrder();
        objOrder.EP_Reason_For_Non_Delivery__c=EP_Common_Constant.TEMPEMAILBODYSTR;
        objOrder.EP_Action_for_Non_Delivery__c = EP_Common_Constant.TO_BE_DECIDED;
        update objOrder;
        Test.startTest();
        localObj.sendEmailForUnDelivered(objOrder);
        Test.stopTest();
        System.assert(true);
        //No assert  required, as this method delegates to other methods
    }
    static testMethod void updateTransportService_test() {
        EP_Delivery localObj = new EP_Delivery();
        csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder();
        EP_Stock_Holding_Location__c stockLocationObj = [Select EP_Use_Managed_Transport_Services__c from EP_Stock_Holding_Location__c where Id =:objOrder.Stock_Holding_Location__c limit 1];
   		stockLocationObj.EP_Use_Managed_Transport_Services__c = 'FALSE';
    	update stockLocationObj;
        Test.startTest();
        localObj.updateTransportService(objOrder);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.STRING_FALSE,objOrder.EP_Use_Managed_Transport_Services__c);
    }
    static testMethod void getShipTos_test() {
        EP_Delivery localObj = new EP_Delivery();
        Id accountId = EP_TestDataUtility.getCurrentOrder().accountId__c;
        Test.startTest();
        LIST<Account> result = localObj.getShipTos(accountId);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void findPriceBookEntries_test() {
        EP_Delivery localObj = new EP_Delivery();
        csord__Order__c objOrder = EP_TestDataUtility.getCurrentOrder();
        Test.startTest();
        LIST<PriceBookEntry> result = localObj.findPriceBookEntries(objOrder.priceBook2Id__c,objOrder);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void getOperationalTanks_test() {
        EP_Delivery localObj = new EP_Delivery();
        String strSelectedShipToId = EP_TestDataUtility.getCurrentOrder().EP_ShipTo__c;
        Test.startTest();
        Map<ID, EP_Tank__c> result = localObj.getOperationalTanks(strSelectedShipToId);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void getAllRoutesOfShipToAndLocation_test() {
        EP_Delivery localObj = new EP_Delivery();
        csord__Order__c orderObject = EP_TestDataUtility.getCurrentOrder();
        String shipId = orderObject.EP_ShipTo__c;
        String shlId = orderObject.EP_Stock_Holding_Location__c;
        Test.startTest();
        LIST<EP_Route__c> result = localObj.getAllRoutesOfShipToAndLocation(shipId,shlId);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void showCustomerPO_PositiveScenariotest() {
        EP_Delivery localObj = new EP_Delivery();
        csord__Order__c orderObject = EP_TestDataUtility.getCurrentOrderPositiveTestScenario();
        Test.startTest();
        Boolean result = localObj.showCustomerPO(orderObject);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void showCustomerPO_NegativeScenariotest() {
        EP_Delivery localObj = new EP_Delivery();
        csord__Order__c orderObject = EP_TestDataUtility.getCurrentOrderNegativeTestScenario();
        Test.startTest();
        Boolean result = localObj.showCustomerPO(orderObject);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isOrderEntryValid_PositiveScenariotest() {
        EP_Delivery localObj = new EP_Delivery();
        csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrderPositiveTestScenario();
        List<Account> listTransportAccount = [SELECT Id FROM Account WHERE id=:orderObj.EP_Transporter__c];
        Integer numOfTransportAccount = listTransportAccount.size();
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isOrderEntryValid_NegativeScenariotest() {
        EP_Delivery localObj = new EP_Delivery();
        csord__Order__c orderObj = EP_TestDataUtility.getCurrentOrderNegativeTestScenario();
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(0);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
}