/* billToId
@Author <Jyotsna Tiwari>
@name <EP_FE_SubmitOrderEndpoint>
@CreateDate <25/04/2016>
@Description <This class updates the existing Order if the status of Order is "Draft"  >  
@Version <1.0>
*/
@RestResource(urlMapping = '/FE/V1/transaction/list/*')
global with sharing class EP_FE_AccountStatementEndpoint {

    /*
    *   Max difference in months, to be checked against the provided date range
    */
    private final static Integer MAX_DAYS_DIFFERENCE = 60;
    global static final String strfunctionality = 'Customer Account Statement';
    
    /*
    *  Return the list of account statement items, for a defined time period and type
    */
    @HttpPost
    global static EP_FE_AccountStatementResponse listCustomAccountStatement(EP_FE_AccountStatementRequest request) {
        System.debug('EP_FE_AccountStatementEndpoint.listCustomAccountStatement: ' + request);
        
        EP_FE_AccountStatementResponse response = new EP_FE_AccountStatementResponse();
        if (request == null) {
            EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_AccountStatementResponse.CLASSNAME, 
                        EP_FE_AccountStatementResponse.METHOD, EP_FE_AccountStatementResponse.SEVERITY, 
                        EP_FE_AccountStatementResponse.TRANSACTION_ID, EP_FE_AccountStatementResponse.DESCRIPTION1), 
                        response, EP_FE_AccountStatementResponse.ERROR_MISSING_REQUEST);
        }
        else {
            if(request.dateFrom == null || request.dateTo == null || request.billToId == null 
                || request.dateTo > system.today() || request.dateFrom > system.today() || request.dateFrom > request.dateTo) {
                EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_AccountStatementResponse.CLASSNAME, 
                        EP_FE_AccountStatementResponse.METHOD, EP_FE_AccountStatementResponse.SEVERITY, 
                        EP_FE_AccountStatementResponse.TRANSACTION_ID, EP_FE_AccountStatementResponse.DESCRIPTION2), 
                        response, EP_FE_AccountStatementResponse.ERROR_MISSING_REQUEST);
            } else {
                // Compute the difference between the dates in the provided range and block the user if greater than 2 months
                /*if(request.dateFrom.daysBetween(request.dateTo) > MAX_DAYS_DIFFERENCE){
                    EP_FE_Utils.logError(EP_FE_Utils.createLogErrorObject(EP_FE_AccountStatementResponse.CLASSNAME, 
                        EP_FE_AccountStatementResponse.METHOD, EP_FE_AccountStatementResponse.SEVERITY, 
                        EP_FE_AccountStatementResponse.TRANSACTION_ID, EP_FE_AccountStatementResponse.DESCRIPTION3), 
                        response, EP_FE_AccountStatementResponse.ERROR_MISSING_REQUEST);
                } else {*/
                    // Parse the bill To associated to the running user
                    Id billToId = null;
                    try {
                        billToId = (Id) request.billToId;
                    } catch (Exception e) {
                        EP_FE_Utils.logError(EP_FE_AccountStatementResponse.CLASSNAME, EP_FE_AccountStatementResponse.METHOD, 
                            EP_FE_AccountStatementResponse.TRANSACTION_ID, strfunctionality,
                            EP_FE_AccountStatementResponse.DESCRIPTION4, e, EP_FE_AccountStatementResponse.GENERIC_EXCEPTION_UTILITY, response);
                    }

                    if(request.type == null || request.type.length() == 0) {
                        //request.type = EP_CustomerAccountStatementUtility.TYPE_ALL;
                        request.type = EP_CustomerAccountStatementUtility.DEFAULT_FILTERS;
                    }

                    // Invoke the utility
                    if(billToId != null) {
                        try {
                        	System.debug('>>>>>>>>>>>'+billToId+'***'+ 
                                request.dateFrom+'****'+ request.dateTo+'****'+ request.type);
                            response.customerAccountStatement = EP_CustomerAccountStatementUtility.getAccountStatementItemListEnhanced(billToId, 
                                request.dateFrom, request.dateTo, request.type);
                        } catch(Exception e) {
                            EP_FE_Utils.logError(EP_FE_AccountStatementResponse.CLASSNAME, EP_FE_AccountStatementResponse.METHOD, 
                                EP_FE_AccountStatementResponse.TRANSACTION_ID, strfunctionality,
                                EP_FE_AccountStatementResponse.DESCRIPTION5, e, EP_FE_AccountStatementResponse.GENERIC_EXCEPTION_UTILITY, response);
                        }
                    }
               // }
            }
        }

        return response;
    }
        
}