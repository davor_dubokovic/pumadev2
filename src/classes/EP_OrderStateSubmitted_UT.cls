@isTest
public class EP_OrderStateSubmitted_UT
{

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    static testMethod void getTextValue_test() {
        Test.startTest();
        String result = EP_OrderStateSubmitted.getTextValue();
        Test.stopTest();
        System.AssertEquals(EP_OrderConstant.OrderState_Submitted , result);
        
    }

    static testMethod void doOnEntry_test() {
        EP_OrderStateSubmitted localObj = new EP_OrderStateSubmitted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.assert(True);
        //Dummy Assert, No operation performs on this method        
    }

    static testMethod void doOnExit_test() {
        EP_OrderStateSubmitted localObj = new EP_OrderStateSubmitted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assertNotEquals(Null,localObj.orderService);
        
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_OrderStateSubmitted localObj = new EP_OrderStateSubmitted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_OrderStateSubmitted localObj = new EP_OrderStateSubmitted();
        EP_OrderDomainObject obj = new EP_OrderDomainObject(EP_TestDataUtility.getSalesOpenOrderScenario());
        EP_OrderEvent oe = new EP_OrderEvent('UserEdit');
        EP_OrderType orderTypeObj = new EP_OrderType();
        orderTypeObj.setOrderDomain(obj); 
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_OrderStateSubmitted localObj = new EP_OrderStateSubmitted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectNegativeScenario();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        localObj.setOrderContext(obj,oe);
        Test.startTest();
        string errormsg;
        try{
            Boolean result = localObj.doTransition();
        }Catch(Exception e){
            errormsg = e.getMessage();
        }
        Test.stopTest();
        System.Assert(errormsg.contains('Order Transitions not allowed'));
    }
    static testMethod void setOrderContext_test() {
        EP_OrderStateSubmitted localObj = new EP_OrderStateSubmitted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);    
        EP_OrderDomainObject currentOrder = EP_TestDataUtility.getOrderStateSubmittedDomainObject();     
        EP_OrderEvent currentEvent = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);    
        Test.startTest();
        localObj.setOrderContext(currentOrder,currentEvent);
        Test.stopTest();
        System.AssertEquals(true,localObj.order != null);
    }
    static testMethod void setOrderDomainObject_test() {
        EP_OrderStateSubmitted localObj = new EP_OrderStateSubmitted();
        EP_OrderDomainObject obj = EP_TestDataUtility.getOrderStateSubmittedDomainObject();
        EP_OrderEvent oe = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
        localObj.setOrderContext(obj,oe);    
        EP_OrderDomainObject currentOrder = EP_TestDataUtility.getOrderStateSubmittedDomainObject();  
        Test.startTest();
        localObj.setOrderDomainObject(currentOrder);
        Test.stopTest();
        
        System.AssertEquals(currentOrder.getOrder().id, localObj.order.getOrder().Id);
        
    }
}