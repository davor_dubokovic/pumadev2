/**
 * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
 * @name        : EP_NonConsignment
 * @CreateDate  : 05/02/2017
 * @Description : This class executes logic for Non Consignment
 * @Version     : <1.0>
 * @reference   : N/A
 */
 public class EP_NonConsignment extends EP_ConsignmentType{

    EP_OrderMapper orderMapper = new EP_OrderMapper();
    public EP_NonConsignment () {
    }

    /** This method is used for credit check
     *  @date      05/02/2017
     *  @name      hasCreditIssue
     *  @param     Order orderObj
     *  @return    NA
     *  @throws    NA
     */  
     public override boolean hasCreditIssue(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_NonConsignment','hasCreditIssue');
        System.debug('*******getCreditAmount*'+getCreditAmount(orderObj));
        System.debug('*******orderObj.TotalAmount *'+orderObj.TotalAmount__c);
        Double creditAmount = getCreditAmount(orderObj);
        //Defect:-57816--Start
        if(orderObj.TotalAmount__c != null){
            creditAmount = orderObj.TotalAmount__c  + creditAmount;    
        }  
        return !isFundAvailable(orderObj,creditAmount) && !(EP_Common_Constant.PREPAYMENT.equalsIgnoreCase(orderObj.EP_Payment_Term__c));
        //Defect:-57816--End
     }

    /** return sum of Amount for all the Orders of the billTo
     *  @date      02/03/2017
     *  @name      getCreditAmount
     *  @param     Order orderObj
     *  @return    NA
     *  @throws    NA
     */    
     public override Double getCreditAmount(csord__Order__c orderObj) {//Defect 57278
        EP_GeneralUtility.Log('Private','EP_NonConsignment','getCreditAmount');
        List<AggregateResult> billToGroupedResults = new List<AggregateResult>();
        billToGroupedResults = orderMapper.getCsOrderSumAmountbyBillTo(orderObj);
        if (billToGroupedResults.isEmpty()) {
            return Double.valueOf(0);
        }
        else {
            return Double.valueOf(billToGroupedResults[0].get('billToAmount'));
        }
     }

    /**  Check if Avaiable funds are more then the sum  total Order of related orders of account 
     *  @date      02/03/2017
     *  @name      isFundAvailable
     *  @param     Order objOrder
     *  @return    NA
     *  @throws    NA
     */
     private boolean isFundAvailable(csord__Order__c orderObj,Double creditAmount) {
        EP_GeneralUtility.Log('Private','EP_NonConsignment','isFundAvailable');
        System.debug('******'+orderObj.EP_Bill_To__r+'********'+creditAmount);
        Double availableCreditLimit;
        /*CAM 2.7 start*/
        if (orderObj.EP_Bill_To__r.EP_AvailableFunds__c != null) {

            availableCreditLimit = orderObj.EP_Bill_To__r.EP_AvailableFunds__c - creditAmount;
        }
        else {
            availableCreditLimit = 0 - creditAmount;
        }
        /*CAM 2.7 end*/
        return availableCreditLimit > 0 ? true : false;     
     }


    /**  to fetch the record type of order
     *  @date      02/03/2017
     *  @name      findRecordType
     *  @param     EP_VendorManagement vmType
     *  @return    Id 
     *  @throws    NA
     */
     public override Id findRecordType(EP_VendorManagement vmType) {
        EP_GeneralUtility.Log('Public','EP_NonConsignment','findRecordType');
        return vmType.findRecordType();
     }


    /**  Calculate Price by sending request to the Pricing engine.
     *  @date      28/02/2017
     *  @name      calculatePrice
     *  @param     Order objOrder
     *  @return    NA
     *  @throws    NA
     */ 
     public override void calculatePrice(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_NonConsignment','calculatePrice');
        List<id> orderLineIdList = new List<Id>();
        Map<String,String> orderLItemOrderMap = new Map<String,String>();
        String messageType = 'SEND_ORDER_PRICING_REQUEST';
        String transactionType = 'PRICING_REQUEST';
        String companyName = orderObj.csord__Account__r.EP_Puma_Company_Code__c;
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(orderObj.Id,messageType,companyName);
        orderObj = new EP_OrderMapper().getCsOrderListWithStandardOrderItemsDetails(new Set<Id>{orderObj.Id})[0];
        for(csord__Order_Line_Item__c orderItemObj : orderObj.csord__Order_Line_Items__r){
            orderLineIdList.add(orderItemObj.id);
        }
        outboundService.sendOutboundMessage(transactionType,orderLineIdList);
        //Defect Fix Start : #57374
        //Update Pricing_Status on Order after sending the Pricing Request to NAV
        orderObj.EP_Pricing_Status__c = 'PRICING REQUEST SENT';
        if(orderObj.Id != null) {
             update orderObj;
        }
        //Defect Fix End : #57374
     }
    }