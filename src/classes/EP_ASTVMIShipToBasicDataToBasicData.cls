/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToBasicDataToBasicData>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 02-Basic Data Setup to 02-Basic Data Setup>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToBasicDataToBasicData extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class <EP_ASTVMIShipToBasicDataToBasicData>
    */
    public EP_ASTVMIShipToBasicDataToBasicData() {
        finalState = EP_AccountConstant.BASICDATASETUP;
    }
    /**
    *  @Author <Accenture>
    *  @Name isTransitionPossible
    **/
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToBasicData','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /**
    *  @Author <Accenture>
    *  @Name isRegisteredForEvent
    **/
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToBasicData','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /**
    *  @Author <Accenture>
    *  @Name isGuardCondition
    **/
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToBasicData','isGuardCondition');
        EP_AccountService service = new EP_AccountService(this.account); 
        //L4_45352_START
        /*
        if(this.account.localAccount.EP_CSC_Review_Required__c){
            system.debug('## 1. Checking csc review is completed on shipTo .....'); 
            if(!service.isShipToCSCReviewCompleted()){
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_CSC_Review_is_not_completed);
                return false;
            }           
            system.debug('## 2. Checking csc review is completed on Tanks .....');      
            if(!service.isTankCSCReviewCompleted()){ 
                accountEvent.isError = true;
                accountEvent.setEventMessage(system.label.EP_Cmplt_Tnk_CSC_Rvw_Bfr_basic_Data_Setup);
                return false;
            }           
        }        
        // #sprintRouteWork starts //
        if(!service.isRouteAllocationAvailable()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Route_Not_Allocated);
            return false;
        }
        // #sprintRouteWork ends //
        */
        system.debug('## 1. Checking if primary supply option is attached'); 
        if(!service.isPrimarySupplyOptionAttached()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(System.label.EP_One_Primary_Location_Reqd);
            return false;
        }
        return true;
    }
    /**
    *  @Author <Accenture>
    *  @Name doOnExit
    **/
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToBasicDataToBasicData','doOnExit');

    }
}