/* 
    @Author <Ram Rai>
    @name <EP_FE_GetActiveTanksRequest>
    @CreateDate <25/04/2016>
    @Description <This is Wrapper Class used in EP_FE_GetActiveTanksRequest>  
    @Version <1.0>
*/
global with sharing class EP_FE_GetActiveTanksRequest {
    public String shipToId {get; set;} //ship to identifier
    public Date dipDate {get; set;}
}