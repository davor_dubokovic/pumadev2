/**
  * @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
  * @name        : EP_Retrospective
  * @CreateDate  : 04/02/2017
  * @Description : This is Class Contains logic for Retrospective Orders 
  * @Version     : <1.0>
  * @reference   : N/A
  */
  public class EP_Retrospective extends EP_OrderEpoch {
    
    public EP_Retrospective () {
      
    }

   /** This method is returns NonVMI OR VMI ShipTos for Retrospective Order
     *  @date      15/02/2017
     *  @name      getShipTos
     *  @param     List<Account> lstAccount
     *  @return    List<Account>
     *  @throws    NA
     */
     public override List<Account> getShipTos(List<Account> lstAccount,EP_ProductSoldAs productSoldAsType) {
      EP_GeneralUtility.Log('Public','EP_Retrospective','getShipTos');
      List<Account> lstShipTos = new List<Account>();
      for (Account objAccount : lstAccount) {
        if (objAccount.RecordType.DeveloperName == EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME 
          || objAccount.RecordType.DeveloperName == EP_Common_Constant.VMI_SHIP_TO_DEV_NAME) {
          lstShipTos.add(objAccount);
        }
      }
      lstShipTos = productSoldAsType.getShipTos(lstShipTos);
      return lstShipTos;
    }
    
    
    /** This method validates the Loading Date on Order
     *  @date      15/02/2017
     *  @name      isValidLoadingDate
     *  @param     Order orderObj
     *  @return    Boolean
     *  @throws    NA
     */
     public override Boolean isValidLoadingDate(csord__Order__c orderObj){
      EP_GeneralUtility.Log('Public','EP_Retrospective','isValidLoadingDate');
      return (orderObj.EP_Loading_Date__c == NULL || orderObj.EP_Order_Date__c == NULL
        || orderObj.EP_Loading_Date__c.date() > System.today() 
        || orderObj.EP_Loading_Date__c.date() < orderObj.EP_Order_Date__c.date());
    }


    /** This method validates the Expected Date on Order
     *  @date      15/02/2017
     *  @name      isValidExpectedDate
     *  @param     Order orderObj
     *  @return    Boolean
     *  @throws    NA
     */
     public override Boolean isValidExpectedDate(csord__Order__c orderObj){
      EP_GeneralUtility.Log('Public','EP_Retrospective','isValidExpectedDate');
      return (orderObj.EP_Loading_Date__c == NULL || orderObj.EP_Expected_Delivery_Date__c == NULL
        ||orderObj.EP_Expected_Delivery_Date__c.date() > System.today() 
        || orderObj.EP_Expected_Delivery_Date__c.date() < orderObj.EP_Loading_Date__c.date());
    }
    
    
    /** This method returns the Id of order record type
     *  @date      15/02/2017
     *  @name      findRecordType
     *  @param     EP_VendorManagement vmType,EP_ConsignmentType consignmentType
     *  @return    Id
     *  @throws    NA
     */
     public override Id findRecordType(EP_VendorManagement vmType,EP_ConsignmentType consignmentType) {
      EP_GeneralUtility.Log('Public','EP_Retrospective','findRecordType');
      return vmType.findRecordType();
    }
    
    
    /** This method validates the Start Date on Order
     *  @date      15/02/2017
     *  @name      isValidExpectedDate
     *  @param     Order orderObj
     *  @return    Boolean
     *  @throws    NA
     */
    /*Defect 53419 Start*/
    public override Boolean isValidOrderDate(csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Public','EP_Retrospective','isValidOrderDate');
        return (orderObj.EP_Order_Date__c == NULL || orderObj.EP_Order_Date__c.date() > System.today());
    }
     /*Defect 53419 End*/
    

  public override void setRetroOrderDetails(csord__Order__c orderObj){
        EP_GeneralUtility.Log('Public','EP_Retrospective','setRetroOrderDetails');
        //Defect 57286 Start
        orderObj.EP_Expected_Loading_Date__c = EP_PortalLibClass_R1.returnLocalDate(orderObj.EP_Loading_Date__c);
        if (orderObj.EP_Expected_Delivery_Date__c != null) {
            String strRoDeliveryDate = String.valueOfGmt(orderObj.EP_Expected_Delivery_Date__c);
            orderObj.EP_Actual_Delivery_Date_Time__c = EP_GeneralUtility.convertDateTimeToDecimal(strRoDeliveryDate);  
        } 
        //Defect 57286 End
    }
      
}