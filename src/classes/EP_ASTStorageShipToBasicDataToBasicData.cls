/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToBasicDataToBasicData>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 02-Basic Data Setup to 02-BasicDataSetup>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToBasicDataToBasicData extends EP_AccountStateTransition {

    public EP_ASTStorageShipToBasicDataToBasicData () {
        finalState = EP_AccountConstant.BASICDATASETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToBasicData','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToBasicData', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToBasicDataToBasicData',' isGuardCondition');        
        return true;
    }
}