@isTest
public class EP_OrderModificationControllerHelper_UT
{
@testSetup 
static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
}

static testMethod void getTotalOrderCost_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    Double result = localObj.getTotalOrderCost();
    Test.stopTest();
    System.Assert(result<>null);
}
static testMethod void loadStep2_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ApexPages.StandardController sc = new ApexPages.StandardController(ord );
    EP_OrderModificationPageController contlr = new EP_OrderModificationPageController(sc);
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(contlr.ctx);

    Test.startTest();
    localObj.loadStep2();
    Test.stopTest();
    System.AssertEquals(true,localobj.ctx.mapOfShipTo.size() == 1 );
}
static testMethod void loadStep3_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ApexPages.StandardController sc = new ApexPages.StandardController(ord );
    EP_OrderModificationPageController contlr = new EP_OrderModificationPageController(sc);
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(contlr.ctx);
    localObj.ctx.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
    localobj.ctx.strSelectedShipToID = ord.EP_ShipTo__c;
    localObj.loadStep2();
    Test.startTest();
    localObj.loadStep3();
    Test.stopTest();
    
    System.AssertEquals(true,localobj.ctx.newOrderRecord.EP_Stock_Holding_Location__c == ord.EP_Stock_Holding_Location__c);
    
}
static testMethod void loadStep4_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    localObj.loadStep4();
    Test.stopTest();
    //Delegates to helper class method. Adding a dummy assert
    System.Assert(true);
    
}

// do cancel method has bugs and requires a fix after code freeze is removed.
/*
static testMethod void doCancel_test() {
    Order ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    ctx.loadAccountOrderDetails();
    System.debug('********Context********'+ctx);
    System.debug('*******Context OrderItems After Context*********'+ctx.oldOrderRecord.OrderItems);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    //to avoid null pointer
    ctx.oldOrderRecord = new Order();
    //System.debug('*******Context OrderItems After Help*********'+ctx.oldOrderRecord.OrderItems);
    Test.startTest();
    localObj.doCancel();
    Test.stopTest();
    system.assertNotEquals(null, ctx.oldOrderRecord.id);
}*/

static testMethod void loadExistingProductSet_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    localObj.loadExistingProductSet();
    Test.stopTest();
    
    System.AssertEquals(true,ctx.existingOrderProductSet.size() == 1);
    
}
static testMethod void setOrderWrapperAttributes_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ApexPages.StandardController sc = new ApexPages.StandardController(ord );
    EP_OrderModificationPageController contlr = new EP_OrderModificationPageController(sc);
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(contlr.ctx);
    localObj.ctx.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
    localobj.ctx.strSelectedShipToID = ord.EP_ShipTo__c;
    localObj.loadStep2();
    localObj.loadStep3();
    localObj.ctx.loadExistingOrderItems();
    EP_OrderPageContext.OrderWrapper orWrapper = localObj.ctx.listofOrderWrapper[0];
    
    Test.startTest();
    localObj.setOrderWrapperAttributes(orWrapper);
    Test.stopTest();
    
    System.AssertEquals(true,orWrapper.orderLineItem.EP_Is_Standard__c);
    
}
static testMethod void displayProductNameAndSafeFillLevel_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    localObj.displayProductNameAndSafeFillLevel();
    Test.stopTest();
    
    System.AssertEquals(false,ctx.isPriceCalculated);
    
}
static testMethod void setTanksOrderDetails_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ApexPages.StandardController sc = new ApexPages.StandardController(ord );
    EP_OrderModificationPageController contlr = new EP_OrderModificationPageController(sc);
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(contlr.ctx);
    localObj.ctx.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
    localobj.ctx.strSelectedShipToID = ord.EP_ShipTo__c;
    localObj.loadStep2();
    localObj.loadStep3();
    localObj.ctx.loadExistingOrderItems();
    EP_OrderPageContext.OrderWrapper orWrapper = localObj.ctx.listofOrderWrapper[0];
    Test.startTest();
    localObj.setTanksOrderDetails(orWrapper);
    Test.stopTest();
    system.assertEquals((localObj.ctx.intOrderLineIndex - 1), orWrapper.oliIndex);
    
}
static testMethod void updatePricebookEntryId_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ApexPages.StandardController sc = new ApexPages.StandardController(ord );
    EP_OrderModificationPageController contlr = new EP_OrderModificationPageController(sc);
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(contlr.ctx);
    localObj.ctx.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
    localobj.ctx.strSelectedShipToID = ord.EP_ShipTo__c;
    localObj.loadStep2();
    localObj.loadStep3();
    localObj.ctx.loadExistingOrderItems();
    EP_OrderPageContext.OrderWrapper orWrapper = localObj.ctx.listofOrderWrapper[0];
    Test.startTest();
    localObj.updatePricebookEntryId(orWrapper);
    Test.stopTest();
    //Delegates to other methods. Adding dummy assert
    System.Assert(true);
}
static testMethod void setPriceBookEntryIdByTankID_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ApexPages.StandardController sc = new ApexPages.StandardController(ord );
    EP_OrderModificationPageController contlr = new EP_OrderModificationPageController(sc);
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(contlr.ctx);
    localObj.ctx.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
    localobj.ctx.strSelectedShipToID = ord.EP_ShipTo__c;
    localObj.loadStep2();
    localObj.loadStep3();
    localObj.ctx.loadExistingOrderItems();
    EP_OrderPageContext.OrderWrapper orWrapper = localObj.ctx.listofOrderWrapper[0];
    csord__Order_Line_Item__c OrderItemObj = [SELECT Id, PricebookEntryId__c, Orderid__c FROM csord__Order_Line_Item__c WHERE Orderid__c =:ord.id][0];
    Test.startTest();
    localObj.setPriceBookEntryIdByTankID(orWrapper,OrderItemObj);
    Test.stopTest();
    
    System.AssertEquals(true,OrderItemObj.PricebookEntryId__c <> null);
}
static testMethod void updateOrderItemLines_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    localObj.updateOrderItemLines();
    Test.stopTest();
    //Delegates to other methods. Adding dummy assert
    System.Assert(true);

}
static testMethod void isDeliveryDateChanged_PositiveScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    Boolean result = localObj.isDeliveryDateChanged();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isDeliveryDateChanged_NegativeScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    ctx.newOrderRecord.EP_Requested_Delivery_Date__c = null;
    Test.startTest();
    Boolean result = localObj.isDeliveryDateChanged();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void isDeliveryDateMissing_PositiveScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    ctx.newOrderRecord.EP_Requested_Delivery_Date__c=null;
    Test.startTest();
    Boolean result = localObj.isDeliveryDateMissing();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isDeliveryDateMissing_NegativeScenariotest() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderNegativeScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    Boolean result = localObj.isDeliveryDateMissing();
    Test.stopTest();
    System.AssertEquals(false,result);
}
static testMethod void validatePrepaymentCustomer_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    ctx.strSelectedPaymentTerm = EP_Common_Constant.PREPAYMENT;
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    Test.startTest();
    localObj.validatePrepaymentCustomer();
    Test.stopTest();
    //Delegates to EP_PortalOrderUtil.checkPrepayForPoNumber. Adding dummy assert
    System.Assert(true);
    
}
static testMethod void validateinventory_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    ApexPages.StandardController sc = new ApexPages.StandardController(ord );
    EP_OrderModificationPageController contlr = new EP_OrderModificationPageController(sc);
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(contlr.ctx);
    localObj.ctx.strSelectedDeliveryType = EP_Common_Constant.DELIVERY;
    localobj.ctx.strSelectedShipToID = ord.EP_ShipTo__c;
    localObj.loadStep2();
    localObj.loadStep3();
    localObj.ctx.loadExistingOrderItems();
    EP_OrderPageContext.OrderWrapper orWrapper = localObj.ctx.listofOrderWrapper[0];
    Test.startTest();
    localObj.validateinventory(orWrapper);
    Test.stopTest();
    //Delegates to OrderlinesSummaryHelper methods. Adding dummy assert
    System.Assert(true);
    
}
static testMethod void deletelineitems_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);

    LIST<csord__Order_Line_Item__c> listItemsToDelete = [SELECT Id, PricebookEntryId__c, Orderid__c FROM csord__Order_Line_Item__c WHERE Orderid__c =:ord.id];

    Test.startTest();
    localObj.deletelineitems(listItemsToDelete);
    Test.stopTest();
    LIST<OrderItem> afterDelete = [SELECT Id,PricebookEntryId,Orderid FROM OrderItem WHERE Orderid =:ord.id];
    
    System.AssertEquals(true,afterDelete.size() == 0);
    
}
static testMethod void updateContextWrapper_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);

    LIST<EP_OrderPageContext.OrderWrapper> updatedOrderWrapper = new LIST<EP_OrderPageContext.OrderWrapper>();
    EP_OrderPageContext.OrderWrapper orWrapper;
    EP_OrderPageContext.OrderWrapper oWrapper;
    Test.startTest();
    localObj.updateContextWrapper(updatedOrderWrapper,orWrapper,oWrapper);
    Test.stopTest();
    
    System.AssertEquals(true,localObj.ctx.listofOrderWrapper.size() == 0);
    
}
static testMethod void populateProductInventory_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    LIST<csord__Order_Line_Item__c> listValidatedOrderLines = [SELECT Id, PricebookEntryId__c, Orderid__c FROM csord__Order_Line_Item__c WHERE Orderid__c =:ord.id];
    Test.startTest();
    localObj.populateProductInventory(listValidatedOrderLines);
    Test.stopTest();
    
    System.AssertEquals(true,localobj.ctx.productInventoryMap <> null);
    
}
static testMethod void dateChange_test() {
    csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
    EP_OrderPageContext ctx = new EP_OrderPageContext(ord.id);
    EP_OrderControllerHelper hlp = new EP_OrderControllerHelper(ctx); 
    EP_OrderModificationControllerHelper localObj = new EP_OrderModificationControllerHelper(ctx);
    DateTime d = Date.Today() ;
    String dateStr =  d.format('dd/MM/yyyy') ;
    localObj.ctx.strSelectedDate =dateStr;

    Test.startTest();
    localObj.dateChange();
    Test.stopTest();
    
    System.AssertEquals(Date.today(),localobj.ctx.newOrderRecord.EP_Requested_Delivery_Date__c);
    
}
}