@isTest
public class EP_NAVOrderUpdateWS_UT
{
	@testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
    
	static testMethod void updateOrder_test() {
		EP_NAVOrderUpdateStub stub = EP_TestDataUtility.createNAVOrderUpdateStub();
        string jsonRequest = JSON.serialize(stub);
        Test.startTest();
        RestRequest req = new RestRequest(); 
   		RestResponse res = new RestResponse();
   		req.requestURI = '/services/apexrest/v1/OrderStatusUpdate/';  
		req.httpMethod = EP_Common_Constant.POST;
		req.requestBody = Blob.valueof(jsonRequest);
		RestContext.request = req;
		RestContext.response= res;
   		EP_NAVOrderUpdateWS.processRequest();
   		string response = res.responseBody.toString();
   		Test.stopTest();
   		system.assertEquals(true, response !=null);
    } 
}