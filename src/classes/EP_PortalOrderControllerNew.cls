public class EP_PortalOrderControllerNew {
    
    public EP_OrderPageContext_UX ctx {get;set;}
    public EP_OrderControllerHelper_UX hlp;
    public EP_OrderLinesAndSummaryHelper_UX ordLinesHlpr;
  //  public EP_OrderModificationControllerHelper_UX hlpModify;

    public Account selltoAccount {get;set;}

    public boolean showDetail{get;set;}
    public boolean validateUILogic {get;set;}
        
    public EP_PortalOrderControllerNew(){
        init();
    }
    
    @TestVisible
    private void init(){
        try{
            EP_GeneralUtility.Log('Private','EP_PortalOrderControllerNew','init');
            if (Apexpages.currentpage().getparameters().get(EP_Common_Constant.ID) != NULL) {
                String localSObjectId = Apexpages.currentpage().getparameters().get(EP_Common_Constant.ID);
                ctx = new EP_OrderPageContext_UX(localSObjectId);
                hlp = new EP_OrderControllerHelper_UX(ctx);
                ordLinesHlpr = new EP_OrderLinesAndSummaryHelper_UX(ctx);
                ctx.newOrderRecord.csord__Account__r = ctx.accountDomainObj.localAccount;

                // setting by default false
                showdetail = false;
                validateUILogic = false;

                if(ctx.isModification){
                    setOrderEditDetails();
                }
                
            }
        }
        catch( Exception Ex ){
            system.debug('##########Exception:-'+ex.getMessage()+'#####'+ex.getStackTraceString());
        } 
    }


    public void setAccountOrderDetails() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','setAccountOrderDetails');
        try{
            sellToAccount = ctx.orderAccount;
        }
        catch( Exception Ex ){
            system.debug('##########Exception:-'+ex.getMessage()+'#####'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            //EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setAccountOrderDetails', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }                  
    }  

    public void setOrderEditDetails() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','setOrderEditDetails');
        try{
            showdetail = true;
            ctx.setOrderDetailsForModification();
            //Get Ship To accounts
            ctx.listOfShipTos = new List<Account>();
            ctx.listOfShipTos = ctx.orderDomainObj.getShipToAccounts();
            system.debug('ctx.listOfShipTos ' + ctx.listOfShipTos);
            ctx.setMapOfShipToAccounts();
            ctx.setshiptodetailsonOrder();
            hlp.loadSupplyOptionsAndTanks();

            hlp.setPickupDetails();
            hlp.setOrderRecordType();
            ordLinesHlpr.prepareProductDetails();
            ordLinesHlpr.loadExisitingOrderItems();
            ordLinesHlpr.newOrderLines();
            ordLinesHlpr.removeDuplicateOrderLines();
            ordLinesHlpr.addNewLine();
            ordLinesHlpr.prepareInventoryMap();
            if(ctx.isPricingAvailable){
                ctx.showSubmitButton = true;
                ctx.isPriceCalculated = true;
            }

            system.debug('ctx.isTransfer ' + ctx.isTransfer);
            if(ctx.isConsignment || ctx.isTransfer){
                ctx.showCalculatePriceButton = false;
                ctx.showSubmitButton = true;
            }
        }
        catch( Exception Ex ){
            system.debug('##########Exception:-'+ex.getMessage()+'#####'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
          //  EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setAccountOrderDetails', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }                  
    }  
    // called when order header is changed
    public void processOrderHeader(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','processOrderHeader');
        try{
            system.debug('ctx.newOrderRecord ****$'+ ctx.newOrderRecord);
            showDetail = true;
            ctx.setDataForConsumptionOrder();
           // ctx.strSelectedDeliveryType = ctx.orderAccount.EP_Delivery_Type__c;
            //ctx.newOrderRecord.EP_Delivery_Type__c = ctx.orderAccount.EP_Delivery_Type__c;
            ctx.orderDomainObj = new EP_OrderDomainObject(ctx.newOrderRecord);

            /*ctx.strSelectedDeliveryType = ctx.orderDomainObj.getDeliveryTypes()[0];
            ctx.newOrderRecord.EP_Delivery_Type__c = ctx.strSelectedDeliveryType;*/
            if(ctx.isExrackOrder){
                ctx.newOrderRecord.EP_ShipTo__c = null;
            }


            ctx.initializeObjects(ctx.orderDomainObj);
            system.debug('ctx.isConsignment ' + ctx.isConsignment);
            system.debug('ctx.newOrderRecord.EP_ShipTo__c ' + ctx.newOrderRecord.EP_ShipTo__c );
            system.debug('ctx.strSelectedShipToID ' + ctx.strSelectedShipToID);
            system.debug('***isDeliveryOrder'+ isDeliveryOrder);
            system.debug('ctx.newOrderRecord ****$'+ ctx.newOrderRecord);
            // while changing from delivery to exrack and returning to supply location id
            // the supply location is not changed, hence making it blank when order header changes
            ctx.strSelectedPickupLocationID = '';
            //Get Ship To accounts
            ctx.listOfShipTos = new List<Account>();
            ctx.listOfShipTos = ctx.orderDomainObj.getShipToAccounts();

            // For first page load, requires ship to and supply location prepopulated
            if(String.isblank(ctx.strSelectedShipToID) && listShipToOptions.size()>0){
                ctx.strSelectedShipToID = listShipToOptions[0].getvalue();
            }

            ctx.setshiptodetailsonOrder();
            system.debug('ctx.strSelectedShipToID ' + ctx.strSelectedShipToID);
            hlp.loadSupplyOptionsAndTanks();
            hlp.setPickupDetails();
            hlp.setOrderRecordType();

            system.debug('ctx.isConsignment ' + ctx.isConsignment);
            if((ctx.isConsignment && !ctx.isConsumptionOrder)|| ctx.isTransfer){
                ctx.showCalculatePriceButton = false;
                ctx.showSubmitButton = true;
            }

            ordLinesHlpr.prepareProductDetails();
            ordLinesHlpr.newOrderLines();
            ordLinesHlpr.addNewLine();
            ordLinesHlpr.populatesupplierOptions();
            ordLinesHlpr.prepareInventoryMap();

            ctx.showCalculatePriceButton = true;
            system.debug('ctx.isConsignment ' + ctx.isConsignment);
            system.debug('ctx.isTransfer ' + ctx.isTransfer);
            if(ctx.isConsignment || ctx.isTransfer){
                ctx.showCalculatePriceButton = false;
                ctx.showSubmitButton = true;
            }

        }catch( Exception Ex ){
            system.debug('##########Exception:-'+ex.getMessage()+'#####'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
          //  EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setAccountOrderDetails', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }        
    }

    

    public boolean isModification{
        get{
            return ctx.isModification;
        }
    }

    public boolean isDeliveryOrder{
        get{
            return ctx.isDeliveryOrder;
        }
    }

    public boolean isExrackOrder{
        get{
            return ctx.isExrackOrder;
        }
    }

    public boolean isRetroOrder{
        get{
            return ctx.isRoOrder;
        }
    }

    public boolean isConsumptionOrder{
        get{
            return ctx.isConsumptionOrder;
        }
    }

    public boolean isConsignmentOrder{
        get{
            return ctx.isConsignment;
        }
    }
    
     /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns applicable Delivery Type for the order
    * @param        :NA
    * @return       :List<SelectOption>
    */ 
    public List <SelectOption> listDeliveryTypeSelectOptions {
        get {
            return ctx.listDeliveryTypeSelectOptions;
        }
        set;
    }

     /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns applicable Delivery Type for the order
    * @param        :NA
    * @return       :List<SelectOption>
    */ 
    public List <SelectOption> listShipToOptions {
        get {
            return ctx.getlistOfShipToOptions();
        }
        set;
    }

    /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns Supply locations related to the Epuma Company of Order's Account in Selectoptions List
    * @param        :NA
    * @return       :List<SelectOption>
    */     
    public List<SelectOption> listSupplyLocationOptions{ 
        get{
            if(ctx.listOfSupplyLocationOptions == null || ctx.listOfSupplyLocationOptions.isEmpty()){
                hlp.loadSupplyOptionsAndTanks();
            }

            hasMessages = Apexpages.hasMessages();
            return ctx.listOfSupplyLocationOptions;   
        }
        set;
    }

    /* @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns applicable Payment terms for the order
    * @param        :NA
    * @return       :List<SelectOption>
    */ 
    public List <SelectOption> listPaymentTermsOptions {        
        get {
           return ctx.listPaymentTermsOptions;
        }
        set;
    }

    /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns TransportPricing  related to the Epuma Company of Order's Account in Selectoptions List 
    * @param        :NA
    * @return       :List<SelectOption>
    */ 
    public List<SelectOption> listTransportPricingOptions{ 
        get{  
            return ctx.getTransporterPricingList();
        }
        set;
    }

    /**
    * @author       :Accenture
    * @date         :03/07/2017
    * @description  :returns supplylocations related to the Epuma Company of Order's Account in Selectoptions List
    * @param        :NA
    * @return       :List<SelectOption>
    */                                        
    public List<SelectOption> listSupplyLocPricingOptions{ 
        get{
            system.debug('Supplylocationpricing called');
            return ctx.getSupplyLocPricing();            
        }
        set;
    }

    public boolean isPriceCalculated{
        get{
            return ctx.isPriceCalculated;
        }
    }

    public Double totalOrderCost {
        get {
           return ctx.totalOrderCost;
        }
    }

    public boolean hasMessages {
        get{
            return Apexpages.hasmessages();
        }
        set;
    }

    public Boolean supplyLocTransportEditable{
        get{
            return (EP_Common_Constant.CSC_PROFILES.contains(ctx.profileName) || EP_Common_Constant.BSM_GM_PROFILES.contains(ctx.profileName) || EP_Common_Constant.ADMIN_PROFILES.contains(ctx.profileName)  );
        }set;
    }  

    /*
    * Method to set Run on order
    */    
    public void setRun() {
        try
        {
            EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','setRun');
            ctx.setRun();
        }catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'setRun', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
    } 

    //TODO: Move to helper

     /*
    Validate Operational Tanks
    */
    public void validateOperationalTanks(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','validateOperationalTanks');
        try {
            system.debug('*** strSelectedShipToID' + ctx.strSelectedShipToID);
            processOrderHeader();
            system.debug('*** showSubmitButton' + ctx.showSubmitButton);
            system.debug('*** ctx' + ctx);
            ctx.mapShipToTanks = ctx.orderService.getTanks( ctx.strSelectedShipToID );
            ctx.mapShipToOperationalTanks = EP_PortalOrderUtil.getOperationalTanks( ctx.mapShipToTanks.values() );
            System.debug(ctx.isBulkOrder);
            system.debug('***'+!ctx.mapShipToTanks.isEmpty());
            system.debug('***'+ctx.mapShipToOperationalTanks.isEmpty());
            system.debug('***isDeliveryOrder'+ isDeliveryOrder);
            if(ctx.isBulkOrder && !ctx.mapShipToTanks.isEmpty() && ctx.mapShipToOperationalTanks.isEmpty()  && isDeliveryOrder) { 
                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, Label.EP_No_Operational_Tanks)); // Tank Work
                return; 
            }    
            hlp.loadSupplyOptionsAndTanks();


         // End selected order line item check
        } catch (Exception ex) {
            system.debug('##########Exception:-'+ex.getMessage()+'#####'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'removeOrderLineItem', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
        
    }

     /*
    Calculate Price
    */
    public void doCalculatePrice() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','doCalculatePrice');
        try {
            PageReference ref = NULL;
            
            system.debug('Order Category is' + ctx.newOrderRecord.EP_Order_Category__c);
            system.debug('Order Epoch is' + ctx.newOrderRecord.EP_Order_Epoch__c);
            system.debug('deliveryType is' + ctx.newOrderRecord.EP_Delivery_Type__c);
            system.debug('Product Category is' + ctx.newOrderRecord.EP_Order_Product_Category__c);
            system.debug('Selected date is '+ctx.strSelectedDate);
            system.debug('Other dates are ' + ctx.newOrderRecord.EP_Order_Date__c + ' ' + ctx.newOrderRecord.EP_Loading_Date__c + ' ' + ctx.newOrderRecord.EP_Expected_Delivery_Date__c + '***');
            system.debug('listofNewOrderWrapper size is ' + ctx.listofNewOrderWrapper.size());
            system.debug('listofNewOrderWrapper is ' + ctx.listofNewOrderWrapper);
            system.debug('Order record is ' + ctx.newOrderRecord);

            ctx.newOrderRecord.EP_Pricing_Status__c = EP_Common_Constant.Blank;


            hlp.doCalculatePrice();

        }catch (Exception ex) {
            system.debug('Exception is #######' +ex.getMessage()+'#####'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doCalculatePrice', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }         
    }

    // polls if pricing response is recieved from Nav
    public void getPricing(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','getPricing');
        try{
            Order orderObj = ctx.orderMapper.getOrderPricingwithorderitems(ctx.newOrderRecord.Id);            
            system.debug('orderObj ' + orderObj);
            if(EP_Common_Constant.PRICED.equalsIgnoreCase(orderObj.EP_Pricing_Status__c)) {    
                ctx.isPricingRecieved = true;  
                ctx.startPolling = false;  
                ordLinesHlpr.refetchOrderLines();
                ctx.showSubmitButton = true;
                ctx.showCalculatePriceButton = false;
            }
           
            if(EP_Common_Constant.FAILURE.equalsIgnoreCase(orderObj.EP_Pricing_Status__c)){
                ctx.newOrderRecord.EP_Error_Description__c  = orderObj.EP_Error_Description__c.split(EP_Common_Constant.SPLIT)[0];       
                ctx.startPolling = false;
            }        
        }catch (Exception ex) {
            system.debug('Exception is #######' +ex.getMessage()+'#####'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'getPricing', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
        
    }

    public void addNewLine(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','addNewLine');
        try{
            ordLinesHlpr.addNewLine();    
        }catch (Exception ex) {
            system.debug('Exception is #######' +ex.getMessage()+'#####'+ex.getStackTraceString());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'addNewLine', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
        
    }

    public void removeNewLine(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','removeNewLine');  
        try{
            String index = Apexpages.currentPage().getParameters().get('index');
            system.debug('index is ' + index);
            ordLinesHlpr.removeNewLine(Integer.valueOf(index));
        }catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'addNewLine', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
    }

    /*
    Method to update Supplier Contracts
    */    
    public void doUpdateSupplierContracts() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','doUpdateSupplierContracts');
        try {

            hlp.doUpdateSupplierContracts();    
         
        } catch (Exception ex) {
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doUpdateSupplierContracts', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
        
    }


    /*
    This method is used to submit the order.
    */
    public PageReference doSubmit() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','doSubmit');

        system.debug('Order Category is' + ctx.newOrderRecord.EP_Order_Category__c);
        system.debug('Order Epoch is' + ctx.newOrderRecord.EP_Order_Epoch__c);
        system.debug('deliveryType is' + ctx.strSelectedDeliveryType);
        system.debug('Product Category is' + ctx.newOrderRecord.EP_Order_Product_Category__c);
        system.debug('Selected date is '+ctx.strSelectedDate);
        system.debug('Other dates are ' + ctx.orderDateStr + ' ' + ctx.loadingDateStr + ' ' + ctx.deliveredDateStr+ '***');
        system.debug('OrderWrapper size is ' + ctx.listofOrderWrapper.size());
        system.debug('OrderWrapper is ' + ctx.listofOrderWrapper);

        PageReference ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);
        SavePoint sp = Database.setSavepoint();
        List <Database.SaveResult> results = new List <Database.SaveResult>();
        try {

            if(!ctx.isPriceCalculated && !(ctx.isConsignment && !ctx.isConsumptionOrder) && !ctx.isTransfer){
                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Pricing needs to be recalculated'));
                return null;
            }

            if(isModification && String.isBlank(ctx.newOrderRecord.EP_Reason_For_Change__c)){
                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Please provide the reason for this change'));
            }

            hlp.setOrderDetails();  
            ctx.orderDomainObj.setRetroOrderDetails(); 
            system.debug('Other dates are ' + ctx.newOrderRecord.EP_Order_Date__c + ' ' + ctx.newOrderRecord.EP_Loading_Date__c + ' ' + ctx.newOrderRecord.EP_Expected_Delivery_Date__c + '***');
            system.debug('Ctx.orderdomainobj ' + ctx.orderdomainobj.getOrder());
            //system.debug('Loading date ' +ctx.orderdomainobj.getOrder().EP_Loading_Date__c.date());
            
            if(!hlp.isvalidOrder()){
                return null;
            }

            if(hlp.hasInValidDates()){
                String errMsg = ctx.isExrackOrder ? 'Request Preffered Date is required' : 'Preffered Delivery date is manadatory';
                ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, errMsg));
                return null;
            }

            if(isConsignmentOrder || ctx.isTransfer){
                /*if(ctx.orderWrapperMap.isEmpty() || ctx.orderWrapperMap.get(0).orderLineItem.quantity == null){
                    ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, 'Add atleast one product'));
                    return null;
                }*/

                ordLinesHlpr.setAndValidateOrderLines();

                if(ctx.hasinvalidorderlines){
                    return null;
                }
            }
            

            hlp.doSubmitActionforNewOrder();
            
            

            hlp.setOrderItemsinDomain();  
            EP_OrderEvent orderEvent;         
            if(isModification){
                orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_EDIT);
            }else{
                orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
            }
            
            ctx.orderService.setOrderStatus(orderEvent);
            ctx.newOrderRecord.Status__c = ctx.orderDomainObj.getStatus();
            ref = updateOrder(orderEvent);
            ctx.orderService.doPostStatusChangeActions();
            validateUILogic = true;
        }catch (Exception ex) {
            Database.rollback(sp);            
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error, ex.getMessage() + EP_Common_Constant.ERR_OCCURED));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.SUBMIT_MTD, 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
            return null;
        }
        return ref.setRedirect(true);
    }

    // As pagereference is different, copied the methods from helper
    //TODO: Make the necessary change in the helper
    public pagereference updateOrder(EP_OrderEvent orderEvent){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','updateOrder');
        //PageReference ref = new PageReference(EP_Common_Constant.SLASH + ctx.newOrderRecord.Id);           
        PageReference ref = new PageReference('/apex/EP_PortalOrderSummaryNew?id=' + ctx.newOrderRecord.Id);           
        if(ctx.newOrderRecord.Status__c == EP_Common_Constant.ORDER_DRAFT_STATUS) {
            ctx.hasOverdueInvoices = orderEvent.getEventMessage() == EP_OrderConstant.OVERDUE_MESSAGE;
            ctx.hasCreditIssues = orderEvent.getEventMessage() == EP_OrderConstant.CREDITISSUE_MESSAGE;
            if((ctx.hasCreditIssues || ctx.hasOverdueInvoices) && !ctx.isRoOrder) {                    
                //ref = setPageReferenece();
                ref = null;
                //Defect 57277 START
                ctx.newOrderRecord.EP_Credit_Status__c = EP_OrderConstant.CREDIT_FAIL;
                //Defect 57277 END
                System.debug('ctx.hasCreditIssues****'+ctx.hasCreditIssues);
                
            }
        }
        else if (ctx.isPrepaymentCustomer && !ctx.isConsignmentOrder){
            ctx.newOrderRecord.EP_Payment_Term__c = EP_Common_Constant.PREPAYMENT; 
        }
        // to update orderStatus
        update ctx.newOrderRecord;

        return ref;    
    }

    /*
    This Method is Used to save order with draft status
    */
    public PageReference saveOrderAsDraft(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','saveOrderAsDraft');
        try{
            return hlp.saveOrderAsDraft();    
        }
        catch( Exception Ex ){
            system.debug('##########Exception:-'+ex.getMessage()+'#####'+ex.getStackTraceString());
            return null;
        } 
    }

    //possible status for the order type
     public List<String> applicableOrderStatus{
        get{
            List<String> applicableOrderStatusList = new List<String>();
            set<String>  applicableOrderStatusSet= new set<String>();
            String ordertype = ctx.orderDomainObj.getOrderTypeClassification();
            system.debug('ordertype is ' + ordertype);
            for(EP_Order_State_Mapping__c orderStateMap : [select Order_Status__c from EP_Order_State_Mapping__c where Order_Type__c=:ordertype]){
                applicableOrderStatusSet.add(orderStateMap.Order_Status__c);
            }
            system.debug('applicableOrderStatusSet size is ' + applicableOrderStatusSet.size());
            applicableOrderStatusList.addAll(applicableOrderStatusSet);
            return applicableOrderStatusList;
        }
     }

     // For retrieving order status history
     public List<String> statusHistory{
        get{
            set<String> orderHistorySet = new set<String>();
            List<String> orderHistoryList = new List<String>();
            for(OrderHistory ordHistory : [select oldvalue, newvalue from orderhistory where field='status' and OrderId =:ctx.newOrderRecord.id order by createddate]){
                orderHistorySet.add((String)ordHistory.oldvalue);
            }
            orderHistoryList.addAll(orderHistorySet);
            return orderHistoryList;
        }
     }

     public String currentStatus{
        get{
            return ctx.orderDomainObj.getStatus();
        }
     }

     /*
    This method is used to cancel the order
    */
    public PageReference doCancel() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','doCancel');
        PageReference ref ;
        try {
            ref = hlp.setOrderStatustoCancelled();
        } catch (Exception ex) {
            ref = null;
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doCancel', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
        system.debug('Url is ' + ref.getUrl());
        return ref.setRedirect(true);
    }

    public PageReference doCancelAndClose() {
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','doCancelAndClose');
        PageReference ref ;
        SavePoint sp = Database.setSavepoint();
        try {
            ref = hlp.doCancelAndClose();
        } catch (Exception ex) {
            ref = null;
            database.rollback(sp);
            system.debug('### Exception is ' + ex.getMessage());
            ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doCancel', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
        return ref;
    }


    public void runQuantityValidation(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','runQuantityValidation');
        try {
            String index = Apexpages.currentPage().getParameters().get('index');
            String packtype = Apexpages.currentPage().getParameters().get('type');
            system.debug('index is-' +index +'--type--' + packtype +'--');
            ordLinesHlpr.validateQuantity(index,packtype);
        } catch (Exception ex) {
            system.debug('### Exception is ' + ex.getMessage());
    //        ApexPages.addMessage(EP_Common_Util.createApexMessage(ApexPages.Severity.Error,Label.EP_Data_missing_error));
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doCancel', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
    }

    public void runValidation(){

    }
    public void updateUOMinOrderLines(){
        EP_GeneralUtility.Log('Public','EP_PortalOrderControllerNew','updateUOMinOrderLines');
        try{
            String index = Apexpages.currentPage().getParameters().get('oWrapperIndex');
            system.debug('Index is ' + index);
            ordLinesHlpr.updateUOMinOrderLines(index);
         } catch (Exception ex) {
            system.debug('### Exception is ' + ex.getMessage());
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doCancel', 'EP_PortalOrderControllerNew', ApexPages.Severity.ERROR);
        }
        
    }

}