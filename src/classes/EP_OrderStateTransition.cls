/*
 *  @Author <Aravindhan Ramalingam>
 *  @Name <EP_OrderStateTransition >
 *  @CreateDate <03/02/2017>
 *  @Description <Order State transitions has virtual methods to get registered events and transitions from the custom setting.
 *                Validates if the state transition is possible>
 *  @Version <1.0>
 */
 
 public virtual class EP_OrderStateTransition {
    public EP_OrderDomainObject order;
    public EP_OrderEvent orderEvent;
    Integer priority;
    public String finalState;

    
    public EP_OrderStateTransition(){

    }
    
    //To override recommendedFinalState
    public EP_OrderStateTransition(String resultState){
        this.finalState = resultState;
    }
    
    /**
     * @author Aravindhan
     * @date <02/05/2017>
     * @description: Validate whether the transition is possible, event is registered and all guard conditions are satisfied
     * @param 
     * @return boolean
     */                              
     public virtual boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_OrderStateTransition','isTransitionPossible');
        //Evaluate the Guard conditions of whether this transition should be fired or not.
        if (isRegisteredForEvent())
        {
       //     system.debug(this + ' IS registered for ' + orderEvent.getEventName());
       return isGuardCondition();
   }
      //  system.debug(this + ' is **NOT** registered for ' + orderEvent.getEventName());
      return false;
      
  }
  
     /**
     * @author Aravindhan
     * @date <02/05/2017>
     * @description: Future implementation; Assuming all the events are registered in the custom settings
     * @param 
     * @return boolean
     */     
     public virtual boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_OrderStateTransition','isRegisteredForEvent');
      //  system.debug('In Super class isRegisteredForEvent');
      return true;       
  }

    /**
     * @author Aravindhan
     * @date <02/05/2017>
     * @description: Check if the destination state allows this transition
     * @param 
     * @return boolean
     */    
     public virtual boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_OrderStateTransition','isGuardCondition');
        //system.debug('In Super class isGuardCondition');
        
        return true;
    }

    /**
     * @author Aravindhan
     * @date <02/05/2017>
     * @description: Sets the order domain object and orderevent in this super class.
     *               All the extended class can access this object for further processing.
     * @param OrderDomainObject and OrderEvent
     * @return boolean
     */    
     public void setOrderContext(EP_OrderDomainObject currentOrder,EP_OrderEvent currentEvent){
        EP_GeneralUtility.Log('Public','EP_OrderStateTransition','setOrderContext');
        order = currentOrder;
        orderEvent = currentEvent;
    }
    
    /**
     * @author Aravindhan
     * @date <02/05/2017>
     * @description: Setting priority variable
     * @param interger value
     */
     public void setPriority(Integer newValue){
        EP_GeneralUtility.Log('Public','EP_OrderStateTransition','setPriority');
        priority = newValue;
    }

    /**
     * @author Aravindhan
     * @date <02/05/2017>
     * @description: returns priority variable
     * @return interger value
     */
     public Integer getPriority(){
        EP_GeneralUtility.Log('Public','EP_OrderStateTransition','getPriority');
        return priority;
    }
}