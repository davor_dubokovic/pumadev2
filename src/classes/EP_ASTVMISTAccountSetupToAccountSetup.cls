/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToAccountSetupToAccountSetup>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 04-Account Set-up to 04-Account Set-up>
*  @Version <1.0>
*/
public class EP_ASTVMISTAccountSetupToAccountSetup extends EP_AccountStateTransition {

    public EP_ASTVMISTAccountSetupToAccountSetup () {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMISTAccountSetupToAccountSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMISTAccountSetupToAccountSetup','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMISTAccountSetupToAccountSetup','isGuardCondition');
        EP_AccountService service =  new EP_AccountService(this.account);
        //L4_45352_Start
        /*
        system.debug('## 2. Checking all review action created for Ship to is completed .....');
        if(!service.isCompletedAllReviewActions()||!service.isCompletedReviewActionsOnTanks()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Review_Action_Pending_on_ShipTo_Error_Message);
            return false;
        }
        system.debug('## 3. Checking if primary supply option is attached'); 
        if(!service.isPrimarySupplyOptionAttached()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Review_Action_Pending_on_ShipTo_Error_Message);
            return false;
        }       
        
        //#sprintRouteWork starts
        if(!service.isRouteAllocationAvailable()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Route_Not_Allocated);
            return false;
        }
        //#sprintRouteWork ends
        */
        //L4_45352_End
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMISTAccountSetupToAccountSetup','doOnExit');
    }
}