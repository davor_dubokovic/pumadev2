/**
 * @author: Jai Singh
 * Test Class for Retry enhansement of Integration framework (Retry TT_SF_009)
 */
@isTest
private class EP_Manual_RetryController_Test {


    //static member variable to be used as test data
    private static Account acc;
    private static String txId;
    private static String msgId;
    private static String company;
    private static DateTime dt_sent; 
    private static final String TESTREC = 'test';
    private static final String ACCOUNTS_STRING = 'Accounts';
    private static final string REQUEST_TIMEOUT = 'Request TimeOut';
    private static final string REQUEST_TIMEOUT_VALUE = '120000';
    private static final string RETRY_MAX_RECORD_COUNT_VALUE = '50';
    private static final string MULT_ATTEMPT_COUNT = 'Multiple Attempt Max Count';
    private static final string MULT_ATTEMPT_COUNT_VALUE = '3';
    
    private static final Integer LMT = Limits.getLimitQueries();

    /*
     * Description: creating the sample test data
     * @author: Jai Singh
    */
    private static void createSampleData(){
        //#59186 User Story Start
        Trigger_Settings__c accountTriggerSetting = new Trigger_Settings__c(Name='EP_Account_Trigger', IsActive__c = false);
        database.insert(accountTriggerSetting);
        //#59186 User Story End
        EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
        insert country;
        acc = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
        BillingCountry='in',
        ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in');
        insert acc; 
        txId = EP_IntegrationUtil.getTransactionID(TESTREC, TESTREC);    
        msgId = EP_IntegrationUtil.getMessageId(TESTREC); 
        company = TESTREC;   
        dt_sent = DateTime.now();  
        
        list<EP_INTEGRATION_CUSTOM_SETTING__c> intCustomSettinglist = new list<EP_INTEGRATION_CUSTOM_SETTING__c>
        { 
            new EP_INTEGRATION_CUSTOM_SETTING__c(Name = EP_Common_Constant.RETRY_MAX_RECORD_COUNT, EP_Value__c = RETRY_MAX_RECORD_COUNT_VALUE),
            new EP_INTEGRATION_CUSTOM_SETTING__c(Name = REQUEST_TIMEOUT, EP_Value__c = REQUEST_TIMEOUT_VALUE),
            new EP_INTEGRATION_CUSTOM_SETTING__c(Name = MULT_ATTEMPT_COUNT, EP_Value__c = MULT_ATTEMPT_COUNT_VALUE)
        };
        Database.insert(intCustomSettinglist);
        
        List<EP_Error_Handler_CS__c> lSeverities = new List<EP_Error_Handler_CS__c>
        {
        	new EP_Error_Handler_CS__c(Name = 'CONFIRM', 
        									Severity__c = 'CONFIRM',
        										Value__c = 1),
			new EP_Error_Handler_CS__c(Name = 'ERROR', 
        									Severity__c = 'ERROR',
        										Value__c = 1),
			new EP_Error_Handler_CS__c(Name = 'FATAL', 
        									Severity__c = 'FATAL',
        										Value__c = 1),
			new EP_Error_Handler_CS__c(Name = 'INFO', 
        									Severity__c = 'INFO',
        										Value__c = 1),
			new EP_Error_Handler_CS__c(Name = 'Threshold', 
        									Severity__c = 'Threshold',
        										Value__c = 1),
			new EP_Error_Handler_CS__c(Name = 'WARNING', 
        									Severity__c = 'WARNING',
        										Value__c = 1)
        };
        Database.insert(lSeverities);
        
    }
    
    
    /*
    @Description Method to test reSubmit
    */
    static testMethod void reSubmit_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            
            List<EP_IntegrationRecord__c> intRecList = EP_IntegrationUtil.sendBulkError(    new List<ID>{ acc.Id }, 
                                                                                                new List<String> {ACCOUNTS_STRING}, 
                                                                                                    txId, 
                                                                                                        msgId, 
                                                                                                            TESTREC, 
                                                                                                                TESTREC, 
                                                                                                                    TESTREC, 
                                                                                                                        dt_Sent, 
                                                                                                                            TESTREC, 
                                                                                                                                TESTREC);
            system.assertEquals(intRecList.size(),1);
            system.assertEquals(intRecList[0].EP_Resend__c,True);
              
            Pagereference pr = Page.EP_Manually_Resend_Int_Record;
            pr.getParameters().put('Id', txId);
            test.setCurrentpage(pr);
            
            EP_Manual_RetryController EPMRC = new EP_Manual_RetryController();
            EPMRC.getIntRecords();
            
            pr.getParameters().put(EP_Common_Constant.SELECTED_PARAMETER_NAME,intRecList[0].Id);
            
            
            EPMRC.reSubmit();
                
        }
        
     }
     
     /*
    @Description Method to test callSfdcToIPASS
    */
    static testMethod void callSfdcToIPASS_Test(){
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            createSampleData();
            EP_TestDataUtility.createStatusList();
            
            EP_IntegrationUtil.callSfdcToIPASS( TESTREC,
                                                    EP_Common_Constant.POST,
                                                        TESTREC,
                                                            new List<ID>{ acc.Id }, 
                                                                new List<String> {ACCOUNTS_STRING}, 
                                                                    txId, 
                                                                        msgId, 
                                                                            TESTREC, 
                                                                                TESTREC,
                                                                                     dt_Sent,
                                                                                        new map<String,String>(), 
                                                                                            TESTREC, 
                                                                                                TRUE
                                            );
            
                
        }
    }
    
    
    
    
}