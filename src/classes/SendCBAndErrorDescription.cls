/*
   @Author          CloudSense
   @Name            SendCBAndErrorDescription
   @CreateDate      28/02/2018
   @Description     
   @Version         1
 
*/

global class SendCBAndErrorDescription implements CSPOFA.ExecutionHandler {
    @TestVisible private static List<EP_AcknowledgementStub.dataSet> ackResponseList = new list <EP_AcknowledgementStub.dataSet> ();
    @TestVisible private static boolean processingFailed = false;
    @TestVisible private static EP_WINDMSOrderNewHelper VMIOrderHelper =  new EP_WINDMSOrderNewHelper();

    public List<sObject> process(List<SObject> data) {
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        List<CSPOFA__Orchestration_Step__c> extendedList = [SELECT Id, CSPOFA__Orchestration_Process__r.Order__c,
                                                        CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,
                                                        CSPOFA__Status__c, CSPOFA__Completed_Date__c,
                                                        CSPOFA__Message__c
                                                        FROM 
                                                        CSPOFA__Orchestration_Step__c 
                                                        WHERE 
                                                        Id IN :stepList];

        for(CSPOFA__Orchestration_Step__c step : extendedList) {
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
        }
        

        List<csord__Order__c> orderList = [SELECT id,EP_SeqId__c,EP_Error_Description__c from csord__Order__c where id in :orderIdList];
                                    
        for(CSPOFA__Orchestration_Step__c step : extendedList) {

            try {
                for(csord__Order__c currentOrder:orderList) {                   
                    if(currentOrder.EP_Error_Product_Codes__c!=null) {
                        currentOrder.Credit_Check_Reason_Code_XML__c=currentOrder.Credit_Check_Reason_Code_XML__c + ' - ' + currentOrder.EP_Error_Product_Codes__c;
                    }
                    EP_OrderService orderService =  new EP_OrderService(new EP_OrderDomainObject(currentOrder));
                    orderService.doSyncCreditCheckInProgressWithWINDMS();
                    }
                }
            
             catch(Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'SendCBAndErrorDescription', apexPages.severity.ERROR);
            }
            step.CSPOFA__Status__c = 'Complete';
            step.CSPOFA__Completed_Date__c = Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
        }
        return result;
    }

}