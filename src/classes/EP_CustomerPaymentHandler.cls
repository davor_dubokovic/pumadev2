/**
 * @author <Pooja Dhiman>
 * @name EP_CustomerPaymentHandler 
 * @description <This class used to create 'Customer Payment' and 'Customer Account Statement Ttem' records For Payment> 
 * @version <1.0>
 */
 public with sharing class EP_CustomerPaymentHandler{

 	private static EP_GenericFinance_Json.EP_AcknowlegmentWrapper acknowledgementWrapper = NULL;
 	private static final String METHOD_NAME = 'syncCustomerPayment';
 	private static final String METHOD_NAME1 = 'createCustomerAccountStatementItem';
    /*Novasuite Fix start*/
    private static final String KEY = 'PA';
    /*Novasuite Fix end*/
	/**
	 * @author <Vinay Jaiswal>
	 * @description <This method is used to sync "Customer Payment" custom object recods in SFDC from NAV>
	 * @date <06/12/2016>
	 * @param List<EP_GenericFinance_Json.Document>
	 * @return EP_GenericFinance_Json.EP_AcknowlegmentWrapper
	 */
	 public static EP_GenericFinance_Json.EP_AcknowlegmentWrapper syncCustomerPayment(List<EP_GenericFinance_Json.Document> listOfDocument){
	 	EP_GeneralUtility.Log('Public','EP_CustomerPaymentHandler','syncCustomerPayment');
	 	acknowledgementWrapper = new EP_GenericFinance_Json.EP_AcknowlegmentWrapper();
		//list of documents to be be inserted in SFDC
		List<EP_GenericFinance_Json.Document> listOfDocumentsToBeCreated = new List<EP_GenericFinance_Json.Document>();
		//list of documents to be be updated in SFDC
		List<EP_GenericFinance_Json.Document> listOfDocumentsToBeUpdated  = new List<EP_GenericFinance_Json.Document>();
		try{
			for(EP_GenericFinance_Json.Document document : listOfDocument){
				if(EP_Common_Constant.CREATE.equals(document.docState)){
					listOfDocumentsToBeCreated.add(document);
					} else {
						if(EP_Common_Constant.APPLICATION.equals(document.docState)){
							listOfDocumentsToBeUpdated.add(document);
						}	
					}
				}
				if(!listOfDocumentsToBeCreated.isEmpty()){
					upsertCustomerPayment(listOfDocumentsToBeCreated);
				}
				if(!listOfDocumentsToBeUpdated.isEmpty()){
					upsertCustomerPayment(listOfDocumentsToBeUpdated);
				}
			}
			catch(Exception ex){
				Database.rollback(EP_GenericFinance_Json.sp);
				Map<String, String> mapSeqIdErrorDescription = new Map<String, String>();
				for(EP_GenericFinance_Json.Document paymentDocument : listOfDocument) {
					mapSeqIdErrorDescription.put(paymentDocument.seqId, ex.getMessage());
				}
				EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, EP_CustomerPaymentHandler.class.getName(), METHOD_NAME, ApexPages.Severity.ERROR);
				acknowledgementWrapper = EP_GenericFinance_Json.fillSendAcknowledgementWrapper(ex, NULL, mapSeqIdErrorDescription);
			}
			return acknowledgementWrapper;
		}

	/**
	* @author <Vinay Jaiswal>
	* @description <This method is used to sync customer payment>
	* @date <06/12/2016>
	* @param List<EP_GenericFinance_Json.Document>
	* @return void
	*/
	private static void upsertCustomerPayment(List<EP_GenericFinance_Json.Document> listOfDocument){
		EP_GeneralUtility.Log('Private','EP_CustomerPaymentHandler','upsertCustomerPayment');
		Integer count;
		String compositkey ;
		List<Database.UpsertResult> listOfUpsertResults;
		Set<String> setPaymentKeys = new Set<String>();
		Map<String, ID> mapExistingPaymentKeyId = new Map<String, ID>();
		Map<String,String> mapNavSeqIdErrorDescription = new Map<String,String>();
		Map<String,String> mapSeqIdCustomerPaymentId = new Map<String,String>();
		Set<String> setAccountNumbers = new Set<String>();
		Id paymentRecordTypeId  = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERPAYMENT_OBJECT,EP_Common_Constant.PAYMENT_RECORD_TYPE_NAME);
		Integer noOfRows = EP_Common_Util.getQueryLimit();
		if(!listOfDocument.isEmpty()){
			for(EP_GenericFinance_Json.Document paymentDocument : listOfDocument){
				 //L4 #45304 Changes Start
				setPaymentKeys.add(EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentDocument.identifier.billTo, paymentDocument.identifier.docId, paymentDocument.identifier.clientId, paymentDocument.identifier.entryNr}));
				 //L4 #45304 Changes End
				compositkey = paymentDocument.identifier.clientId + EP_Common_Constant.HYPHEN +paymentDocument.identifier.billTo;
				setAccountNumbers.add(compositkey);
			}
			if(!setPaymentKeys.isEmpty()){
				for(EP_Customer_Payment__c customerPayment : [Select Id, EP_Payment_Key__c from EP_Customer_Payment__c where EP_Payment_Key__c IN : setPaymentKeys limit: noOfRows ]){
					mapExistingPaymentKeyId.put(customerPayment.EP_Payment_Key__c , customerPayment.Id);
				}
			}
			//map containing containing 'AccountNumber' as key and 'Id' as value corresponding to a Set of specific RecordType 'DeveloperName' 
			Map<String, Id> acctNumIdMap = getAccountNumberIdMap(setAccountNumbers, new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
			
			EP_Customer_Payment__c customerPaymentObject;
			List<EP_Customer_Payment__c> listOfCustomerPaymentObjectsToBeUpserted = new List<EP_Customer_Payment__c>();
			for(EP_GenericFinance_Json.Document paymentDocument : listOfDocument){
				if(paymentDocument.amount.contains(EP_Common_Constant.COMMA)){
					paymentDocument.amount = paymentDocument.amount.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK);
				}
				if(paymentDocument.remainingAmount.contains(EP_Common_Constant.COMMA)){
					paymentDocument.remainingAmount = paymentDocument.remainingAmount.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK);
				}
				if(!mapExistingPaymentKeyId.isEmpty()){
					//L4 #45304 Changes Start
					if(mapExistingPaymentKeyId.containsKey(EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentDocument.identifier.billTo, 
							paymentDocument.identifier.docId, paymentDocument.identifier.clientId, paymentDocument.identifier.entryNr}))) {
							Id recordId = (Id)mapExistingPaymentKeyId.get(EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentDocument.identifier.billTo, 
								paymentDocument.identifier.docId, paymentDocument.identifier.clientId, paymentDocument.identifier.entryNr}));
							//L4 #45304 Changes End
							customerPaymentObject = generateCustomerPaymentRecord(paymentDocument, recordId, paymentRecordTypeId, acctNumIdMap);
						}
					} else{
						customerPaymentObject = generateCustomerPaymentRecord(paymentDocument, NULL, paymentRecordTypeId, acctNumIdMap);
					}
					listOfCustomerPaymentObjectsToBeUpserted.add(customerPaymentObject);
				}
				listOfUpsertResults = Database.Upsert(listOfCustomerPaymentObjectsToBeUpserted, EP_Customer_Payment__c.fields.EP_Payment_Key__c, true);
				for(count = 0 ; count < listOfUpsertResults.size(); count++){
					if(listOfUpsertResults[count].isSuccess()){
						mapSeqIdCustomerPaymentId.put(listOfCustomerPaymentObjectsToBeUpserted[count].EP_NavSeqId__c,listOfCustomerPaymentObjectsToBeUpserted[count].Id);
					}
					else{
						for(Database.Error err : listOfUpsertResults[count].getErrors()){
							mapNavSeqIdErrorDescription.put(listOfCustomerPaymentObjectsToBeUpserted[count].EP_NavSeqId__c, err.getMessage());
						}
					}
				}
			}
			acknowledgementWrapper = EP_GenericFinance_Json.fillSendAcknowledgementWrapper(NULL, mapSeqIdCustomerPaymentId, mapNavSeqIdErrorDescription);
		}

	/**
	* @author <Kamal Garg>
	* @description <This method is used to create "EP_Customer_Payment__c" custom object record>
	* @date <11/01/2017>
	* @param EP_GenericFinance_Json.Document, Id, Id, Map<String, Id>
	* @return EP_Customer_Payment__c
	*/
	private static EP_Customer_Payment__c generateCustomerPaymentRecord(EP_GenericFinance_Json.Document paymentDocument, Id recordId, Id paymentRecordTypeId, Map<String, Id> acctNumIdMap) {
		EP_GeneralUtility.Log('Private','EP_CustomerPaymentHandler','generateCustomerPaymentRecord');
		EP_Customer_Payment__c customerPaymentObject = new EP_Customer_Payment__c();
		
		if(recordId != NULL){
			customerPaymentObject.Id = recordId;
			customerPaymentObject.EP_NavSeqId__c = paymentDocument.seqId;
			customerPaymentObject.EP_Payment_Remaining_Balance__c = Decimal.ValueOf(paymentDocument.remainingAmount);
			/** TR Datetime 59846 Start **/
			customerPaymentObject.EP_Payment_Date__c = EP_DateTimeUtility.convertStringToDate(paymentDocument.submissionDate);//Date.ValueOf(paymentDocument.submissionDate);
			/** TR Datetime 59846 End **/
			customerPaymentObject.EP_Amount_Paid__c = Decimal.ValueOf(paymentDocument.amount);
		} else{
			customerPaymentObject.EP_NavSeqId__c = paymentDocument.seqId;
			customerPaymentObject.EP_Payment_Remaining_Balance__c = Decimal.ValueOf(paymentDocument.remainingAmount);
			/** TR Datetime 59846 Start **/
			customerPaymentObject.EP_Payment_Date__c = EP_DateTimeUtility.convertStringToDate(paymentDocument.submissionDate);//Date.ValueOf(paymentDocument.submissionDate);
			/** TR Datetime 59846 End **/
			customerPaymentObject.EP_Amount_Paid__c = Decimal.ValueOf(paymentDocument.amount);
			customerPaymentObject.Name = paymentDocument.identifier.docId;
			customerPaymentObject.EP_Payment_Method__c = paymentDocument.paymentMethod;
			customerPaymentObject.RecordTypeId = paymentRecordTypeId;
			customerPaymentObject.CurrencyIsoCode = paymentDocument.currencyId;
			String compositKey = paymentDocument.identifier.clientId + EP_Common_Constant.HYPHEN + paymentDocument.identifier.billTo;
			if(acctNumIdMap.containsKey(compositKey)){
				customerPaymentObject.EP_Bill_To__c = acctNumIdMap.get(compositKey);
			}
			customerPaymentObject.EP_Payment_Reference_Number__c = paymentDocument.referenceNr;
		}
		//L4 #45304 Changes Start
		customerPaymentObject.EP_Payment_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{paymentDocument.identifier.billTo, paymentDocument.identifier.docId, paymentDocument.identifier.clientId, paymentDocument.identifier.entryNr});
		customerPaymentObject.EP_Is_Reversal__c = paymentDocument.isReversal != null && paymentDocument.isReversal.equalsIgnoreCase(EP_COMMON_CONSTANT.YES) ? true : false ;
		customerPaymentObject.EP_Entry_Number__c = paymentDocument.identifier.entryNr;
		customerPaymentObject.EP_Amount_Paid__c  = reverseAmountValue(customerPaymentObject.EP_Amount_Paid__c);
		customerPaymentObject.EP_Payment_Remaining_Balance__c = reverseAmountValue(customerPaymentObject.EP_Payment_Remaining_Balance__c);
		//L4 #45304 Changes End
		return customerPaymentObject;
	}
	//L4 #45304 Changes Start
	/**
	* @author <Rahul Jain>
	* @description <Utility method to negate the value of Amount>
	* @date <11/03/2017>
	* @param Decimal
	* @return Decimal
	*/
	private static Decimal reverseAmountValue(Decimal amtValue) {
		if(amtValue != null && amtValue != 0) {
		    amtValue = amtValue * (-1);
		}
		return amtValue;
	}
	//L4 #45304 Changes End	
	/**
	* @author <Vinay Jaiswal>
	* @description <This method is used to get Map containing 'AccountNumber' as key and 'Id' as value corresponding to a Set of specific RecordType 'DeveloperName'>
	* @date <21/06/2016>
	* @param Set<String>, Set<String>
	* @return Map<String, Id>
	*/
	private static Map<String, Id> getAccountNumberIdMap(Set<String> setAccountNumbers, Set<String> setRecordTypeDevloperNames){
		EP_GeneralUtility.Log('Private','EP_CustomerPaymentHandler','getAccountNumberIdMap');
		Map<String, Id> acctNumIdMap = new Map<String, Id>();
		
		for(Account acct :[SELECT Id, AccountNumber, EP_Composite_Id__c FROM Account WHERE EP_Composite_Id__c IN:setAccountNumbers 
			AND RecordType.DeveloperName IN:setRecordTypeDevloperNames LIMIT 1]){
			acctNumIdMap.put(acct.EP_Composite_Id__c.toUpperCase(), acct.Id);
		}
		return acctNumIdMap;
	}
	
	/**
	 * @author <Vinay Jaiswal>
	 * @description <This method is used to perform insert operation for 'Customer Account Statement Item' custom object records>
	 * @date <21/06/2016>
	 * @param List<EP_Customer_Payment__c>
	 * @return void
	 */
	 public static void createCustomerAccountStatementItem(List<EP_Customer_Payment__c> listOfCustomerPayment){
	 	EP_GeneralUtility.Log('Public','EP_CustomerPaymentHandler','createCustomerAccountStatementItem');
        /*Novasuite Fix*/
	 	EP_Customer_Account_Statement_Item__c customerAccountStatementItemObject;
	 	try{
	 		Id paymentInRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERACCOUNTSTATEMENTITEM_OBJECT
	 			,EP_Common_Constant.PAYMENTIN_RECORD_TYPE_NAME);
	 		List<EP_Customer_Account_Statement_Item__c> listOfCustomerAccountStatementItems = new List<EP_Customer_Account_Statement_Item__c>();
	 		for(EP_Customer_Payment__c customerPayment : listOfCustomerPayment) {
	 			customerAccountStatementItemObject = new EP_Customer_Account_Statement_Item__c();
	 			customerAccountStatementItemObject.RecordTypeId = paymentInRecordTypeId;
	 			customerAccountStatementItemObject.EP_NAV_Statement_Item_ID__c = customerPayment.Name;
	 			customerAccountStatementItemObject.EP_Bill_To__c = customerPayment.EP_Bill_To__c;
	 			customerAccountStatementItemObject.EP_Statement_Item_Issue_Date__c = customerPayment.EP_Payment_Date__c;
	 			customerAccountStatementItemObject.EP_Credit__c = customerPayment.EP_Amount_Paid__c;
	 			customerAccountStatementItemObject.CurrencyIsoCode = customerPayment.CurrencyIsoCode;
	 			customerAccountStatementItemObject.EP_Customer_Payment__c = customerPayment.Id;
	 			customerAccountStatementItemObject.EP_SeqId__c = customerPayment.EP_NavSeqId__c;
	 			customerAccountStatementItemObject.EP_Customer_Account_Statement_Item_Key__c = KEY + EP_Common_Constant.UNDERSCORE_STRING + customerPayment.EP_Bill_To__c + EP_Common_Constant.UNDERSCORE_STRING +customerPayment.Name+ EP_Common_Constant.UNDERSCORE_STRING +customerPayment.EP_Payment_Date__c;
	 			listOfCustomerAccountStatementItems.add(customerAccountStatementItemObject);
	 		}
	 		if(!listOfCustomerAccountStatementItems.isEmpty()){
	 			Database.insert(listOfCustomerAccountStatementItems);
	 		}
	 		}catch(Exception ex){
	 			EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_CustomerPaymentHandler.class.getName(), METHOD_NAME1, ApexPages.Severity.ERROR);
	 		}
	 	}
	 }