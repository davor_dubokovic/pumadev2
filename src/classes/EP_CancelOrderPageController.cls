public with sharing class EP_CancelOrderPageController {

    Private csord__Order__c orderObject;
    
    public EP_CancelOrderPageController(ApexPages.StandardController controller) {
        orderObject = (csord__Order__c) controller.getRecord(); 
    }
    
    public void saveOrder(){
    
        EP_OrderDomainObject orderDomain = new EP_OrderDomainObject(orderObject.Id);
        EP_OrderEvent orderEvent = new EP_OrderEvent(EP_OrderConstant.USER_CANCEL);
        EP_OrderService orderService = new EP_OrderService(orderDomain);
        orderService.setOrderStatus(orderEvent);
        orderObject = orderDomain.getOrder();
        update orderObject;
        orderService.doPostStatusChangeActions(); 
    }
}