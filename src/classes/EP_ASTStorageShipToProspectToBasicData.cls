/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToProspectToBasicData>
*  @CreateDate <08/03/2017>
*  @Description <Handles Storage Ship To Account status change from prospect to Basic Data setup>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToProspectToBasicData extends EP_AccountStateTransition{
    public EP_ASTStorageShipToProspectToBasicData () {
        finalState = EP_AccountConstant.BASICDATASETUP;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToProspectToBasicData','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToProspectToBasicData','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToProspectToBasicData','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToProspectToBasicData','doOnExit');

    }
}