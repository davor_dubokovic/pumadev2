/*   
     @Author Aravindhan Ramalingam
     @name <EP_OrderStateLoaded .cls>     
     @Description <Order State for Loaded status, common for all order types>   
     @Version <1.1> 
     */

     public class EP_OrderStateLoaded extends EP_OrderState {
        
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        public override boolean doTransition(){
            EP_GeneralUtility.Log('Public','EP_OrderStateLoaded','doTransition');
            return super.doTransition();
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_OrderStateLoaded','doOnEntry');
            
        }
        
        public override boolean isInboundTransitionPossible(){
            EP_GeneralUtility.Log('Public','EP_OrderStateLoaded','isInboundTransitionPossible');
            //Override this with all possible guard conditions that permits the state transition possible
            //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
            return super.isInboundTransitionPossible();
        }

        public static String getTextValue()
        {
            return EP_OrderConstant.OrderState_Loaded;
        }
}