/**
 * @author <Accenture>
 * @name <PricingRecordTriggerTest>
 * @createDate <23/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_PricingRecordTriggerTest {

    @testSetup static void init() {
         EP_TestDummyData.intializeVMIOrder();
    }    
    
    static testMethod void testPricingRecordTrigger() {
        List<Order> orderList = [Select Id,RecordType.DeveloperName from order where RecordType.DeveloperName = 'EP_VMI_Orders'];
        System.debug('$$$$$$$$$$$$'+orderList);
        List<OrderItem> orderItemList = [Select Id from OrderItem where OrderId =: orderList[0].id];
        System.assert(True,!orderList.isEmpty());
        System.assert(True,!orderItemList.isEmpty());
        EP_Pricing_Engine__c pricingEngineRecord = new EP_Pricing_Engine__c();
        if(orderList.size() > 0) { 
            pricingEngineRecord.EP_Order__c = orderList[0].Id;
        }
        insert pricingEngineRecord;
        Test.startTest();
            pricingEngineRecord.EP_Sequence_Id__c = 'newTestRecord';
            update pricingEngineRecord;            
        Test.stopTest();
        List<EP_Pricing_Engine__c> priceRecList = new List<EP_Pricing_Engine__c>();
        priceRecList = [Select id,EP_Sequence_Id__c from EP_Pricing_Engine__c where EP_Sequence_Id__c ='newTestRecord'];
        System.AssertEquals('newTestRecord',priceRecList[0].EP_Sequence_Id__c);
    }
    //Negative scenarion - catch block test
    static testMethod void testPricingRecordTriggerNegativeScenario() {
        Map<Id,EP_Pricing_Engine__c> oldPricMap;
        Map<Id,EP_Pricing_Engine__c> newMap;
        Test.startTest();
            EP_PricingRecordTriggerHelper.sendResponseToLS(oldPricMap,newMap);
        Test.stopTest();
        List<EP_Exception_Log__c> excepLogList = new List<EP_Exception_Log__c>();
        excepLogList = [Select Id,EP_Exception_Description__c from EP_Exception_Log__c];
        System.debug('&&&&&&&&'+excepLogList);
        System.assert(TRUE,!excepLogList.isEmpty());
        System.assert(TRUE,excepLogList[0].EP_Exception_Description__c.contains('Attempt to de-reference a null object'));        
    }
}