@isTest
public class EP_OSTANYToCancelled_UT
{
    static final String event='Planned to cancelled';
    static final String invalidEvent='cancelled To Planned';

    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
    }
    
static testMethod void isTransitionPossible_positive_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(event);
    EP_OSTANYToCancelled ost = new EP_OSTANYToCancelled();
    csord__Order__c ord = obj.getOrder();
    ord.EP_WinDMS_Status__c = EP_Common_Constant.STATUS_CANCELLED;
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(true,result);
}
static testMethod void isTransitionPossible_negative_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderNegativeTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(invalidEvent);
    EP_OSTANYToCancelled ost = new EP_OSTANYToCancelled();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isTransitionPossible();
    Test.stopTest();
    System.AssertEquals(false,result);
}


static testMethod void isRegisteredForEvent_positive_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(event);
    EP_OSTANYToCancelled ost = new EP_OSTANYToCancelled();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isRegisteredForEvent();
    Test.stopTest();
    System.AssertEquals(true,result);
}


static testMethod void isGuardCondition_positive_creditFail_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(event);
    EP_OSTANYToCancelled ost = new EP_OSTANYToCancelled();
    csord__Order__c ord = obj.getOrder();
    ord.EP_Credit_Status__c = EP_Common_Constant.CREDIT_FAIL;
    ord.EP_Credit_Status__c = EP_Common_Constant.OVERDUE_INVOICE;
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(true,result);
}


static testMethod void isGuardCondition_positive_windmsCancelled_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderPositiveTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(event);
    EP_OSTANYToCancelled ost = new EP_OSTANYToCancelled();
    csord__Order__c ord = obj.getOrder();
    ord.EP_WinDMS_Status__c = EP_Common_Constant.STATUS_CANCELLED;
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(true,result);
}

static testMethod void isGuardCondition_negative_test() {
    EP_OrderDomainObject obj = EP_TestDataUtility.getOrderNegativeTestScenario();
    EP_OrderEvent oe = new EP_OrderEvent(invalidEvent);
    EP_OSTANYToCancelled ost = new EP_OSTANYToCancelled();
    ost.setOrderContext(obj,oe);
    Test.startTest();
    boolean result = ost.isGuardCondition();
    Test.stopTest();
    System.AssertEquals(false,result);
}


}