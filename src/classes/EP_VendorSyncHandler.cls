/* 
  @Author <Jai Singh>
   @name <EP_ProductSyncHandler>
   @CreateDate <22/04/2016>
   @Description  <This apex class is to handle JSON body sent to EP_VendorSyncNAVtoSFDC REST RESOURCE> 
   @Version <1.0>
   */
   public with sharing class EP_VendorSyncHandler {

    public static String responseBody = EP_Common_Constant.BLANK;
    public static final string VENDOR_SYNC = 'VENDOR SYNC FROM NAV TO SFDC';
    public static final string CLASS_NAME = 'EP_VendorSyncHandler';
    
    public static final string STR_SEQID_ERROR = ' seqId is blank'+EP_Common_Constant.COMMA;
    public static final string STR_CLIENTID_ERROR = ' clientid is blank'+EP_Common_Constant.COMMA;
    public static final string STR_VENDORID_ERROR = ' vendorId is blank'+EP_Common_Constant.COMMA;
    public static final string STR_NAME_ERROR = ' name is blank'+EP_Common_Constant.COMMA;
    public static final string STR_ADDRESS_ERROR = ' address is blank'+EP_Common_Constant.COMMA;
    public static final string STR_CITY_ERROR = ' city is blank.'+EP_Common_Constant.COMMA;
    public static final string STR_POSTCODE_ERROR = ' postCode is blank'+EP_Common_Constant.COMMA;
    public static final string STR_COUNTRYCODE_ERROR = ' cntryCode is blank'+EP_Common_Constant.COMMA;
    public static final string STR_VENDORTYPE_ERROR = ' vendorType is blank or invalid'+EP_Common_Constant.COMMA;
    public static final string STR_BLOCKED_ERROR = ' blocked is blank'+EP_Common_Constant.COMMA;
    public static final string STR_BLOCKED_UNSUPPORTED_ERROR1 = ' unsupported value ';
    public static final string STR_BLOCKED_UNSUPPORTED_ERROR2 = ' in blocked. Supportd values are YES or NO'+EP_Common_Constant.COMMA;
    public static final string STR_CURRENCY_ERROR = ' currencyId is blank'+EP_Common_Constant.COMMA;
    public static final string STR_DUTIES_ERROR = ' duties is blank'+EP_Common_Constant.COMMA;
    public static final string VENDOR_ACCOUNT_RT = 'Vendor';
    public static final string VENDOR_TYPE_NAV = 'ThirdPartyStockSupplier';
    public static final string VENDOR_TYPE_SFDC_3RD = '3rd Party Stock Supplier';
    public static final string VENDOR_TYPE_SFDC_TRSN = 'Transporter';
    public static final string DASH_STRING = '-';
    public static final string SEMICOLON_STRING = ';';
    public static final String VENDOR_NODE = 'Vendor';
    /*
    Method to parse request JSON, create or update product, initiate and send acknowledgement back
    to requester service
    */
    public static String createUpdateVendor( String requestBody ) 
    {
        EP_GeneralUtility.Log('Public','EP_VendorSyncHandler','createUpdateVendor');
        list<WrapperVendor> vendorList = new list<WrapperVendor>(); 
        map<String,String> ErrorDescriptionMap = new map<String,String>();
        map<String,String> ErrorCodeMap = new map<String,String>();
        map<String,String> SuccessMap = new map<String,String>();
        EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon = new EP_AcknowledmentGenerator.cls_HeaderCommon();

        //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        //1: Get Header Details from the JSON String
        Map<String, String> mapOfHeaderCommonDetails = EP_IntegrationService.getRequestHeaderDetails(requestBody);
        
        //2: Check for Idempotency - Check the message Id in integration Record if already exists then return old Response
        if(EP_IntegrationService.isIdempotent(mapOfHeaderCommonDetails.get(EP_Common_Constant.MSG_ID))) {
            return EP_IntegrationService.processResponse;
        }
        //3: Log Request - Log the JSON String and other details from Header into the integration record for inbound Request        
        EP_IntegrationRecord__c integrationRecord = EP_IntegrationService.logInboundRequest('VENDOR_SYNC',mapOfHeaderCommonDetails,requestBody);
        //Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)

        try{
            system.debug(requestBody);
            JSONParser parser = JSON.createParser( requestBody );

            parser.nextToken();
            while( parser.hasCurrentToken() )
            {
                system.debug( 'current Token:'+parser.getCurrentToken() +' '+parser.getCurrentName());
                if(parser.getCurrentToken() == JSONTOKEN.FIELD_NAME){
                    //TO GET HEADER FROM REQUEST
                    if(EP_Common_Constant.HEADER_COMMON.equalsIgnoreCase(parser.getCurrentName())){
                        parser.nextToken();
                        headerCommon = (EP_AcknowledmentGenerator.cls_HeaderCommon)parser.readValueAs(EP_AcknowledmentGenerator.cls_HeaderCommon.class);
                        parser.skipChildren(); 
                    }
                }
                
                //look for vendors object
                if( EP_Common_Constant.VENDOR_NODE.equalsIgnoreCase(parser.getCurrentName() ) )
                {
                    parser.nextToken();
                    //if list of vendrs
                    if( parser.getCurrentToken() == JSONTOKEN.START_ARRAY ) 
                    {
                        //parse vendors object into WrapperVendors
                        list<WrapperVendor> obj =  (list<WrapperVendor>)parser.readValueAs(list<WrapperVendor>.class);  
                        vendorList.addAll(obj);
                        //come out of the while loop
                        break;
                    }
                    //if only one vendr
                    else if( parser.getCurrentToken() == JSONTOKEN.START_OBJECT ) 
                    {
                        //parse vendors object into WrapperVendor
                        WrapperVendor obj =  (WrapperVendor)parser.readValueAs(WrapperVendor.class);  
                        vendorList.add(obj);
                        break; 
                        //come out of the while loop
                    }
                    
                }
                else
                {
                    //go to nextt token in JSON
                    parser.nextToken();
                    
                }
            }
            String strErrorAll = EP_Common_Constant.BLANK;
            list<Account> lstVendor = new list<Account>();
            list<WrapperVendor> vendorForDML = new list<WrapperVendor>();
            system.debug( 'vendorList'+vendorList);
            for( WrapperVendor wv : vendorList )
            {
                Account vendor = new Account();
                vendor.RecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(VENDOR_ACCOUNT_RT).getRecordTypeId();
                String strError = EP_Common_Constant.BLANK;
                if( isBlank( wv.seqId ) )
                {
                    strError += STR_SEQID_ERROR;
                }
                else
                {
                    vendor.EP_Source_Seq_Id__c = wv.seqId;
                }
                
                WrapperIdentifier identifier = wv.identifier;
                if( isBlank( identifier.clientId )  )
                {
                    strError += STR_CLIENTID_ERROR;
                }
                else
                {
                    vendor.EP_Puma_Company__r = new Company__c( EP_Company_Code__c = identifier.clientId );
                }
                
                if( isBlank( identifier.vendorId )  )
                {
                    strError += STR_VENDORID_ERROR;
                }
                else
                {
                    /* TFS fix 45559 start EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c*/
                    vendor.EP_Source_Entity_ID__c = identifier.vendorId;
                    /* TFS fix 45559 end*/
                }
                
                if( isBlank( wv.name ) )
                {
                    strError += STR_NAME_ERROR;
                }
                else
                {
                    vendor.Name = wv.name;
                }
                
                if( !isBlank( wv.name2 ))
                {
                    vendor.Description += wv.name2;
                }
                
                if( isBlank( wv.address ) )
                {
                    strError += STR_ADDRESS_ERROR;
                }
                else
                {
                    vendor.BillingStreet = wv.address;
                }
                
                /*** Defect #54234********/
                /*
                if( !isBlank( wv.address2 ) )
                {
                    vendor.BillingStreet += EP_Common_Constant.COMMA + wv.address2;
                }
                */
                /*** Defect #54234********/
                
                if( isBlank( wv.city ) )
                {
                    strError += STR_CITY_ERROR;
                }
                else
                {
                    vendor.BillingCity = wv.city;
                }
                
                if( isBlank( wv.postCode ) )
                {
                    strError += STR_POSTCODE_ERROR;
                }
                else
                {
                    vendor.BillingPostalCode = wv.postCode;
                }
                
                if( isBlank( wv.cntryCode ) )
                {
                    strError += STR_COUNTRYCODE_ERROR;
                }
                else
                {
                    vendor.BillingCountry = wv.cntryCode;
                }
                
                /*#45559_Start*/              
                list<WrapperVendorType> vendorTypes = wv.vendorTypes;
                if(vendorTypes.size()==0){
                    strError += STR_VENDORTYPE_ERROR;
                }
                /*#45559_END*/
                else{
                    /* TFS 45559,45560,45567,45568 fix start  EP_Vendor_Type__c is replaced by EP_VendorType__c*/
                    string strVendorType = getVendorType(vendorTypes);
                    if(!isBlank(strVendorType)){
                         vendor.EP_VendorType__c = strVendorType;
                    }
                    else{
                        strError += STR_VENDORTYPE_ERROR;
                    }
                    /* TFS 45559,45560,45567,45568 fix end*/
                }

                        if( isBlank( wv.blocked ) )
                        {
                            strError += STR_BLOCKED_ERROR;
                        }
                        else
                        {
                            if( wv.blocked.equalsIgnoreCase( EP_Common_Constant.STRING_YES_LS ) )
                            {
                                vendor.EP_Status__c = EP_Common_Constant.STATUS_BLOCKED;
                            }
                            else if( wv.blocked.equalsIgnoreCase( EP_Common_Constant.STRING_NO_LS ) )
                            {
                                vendor.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
                            }
                            else
                            {
                                strError += STR_BLOCKED_UNSUPPORTED_ERROR1+ wv.blocked + STR_BLOCKED_UNSUPPORTED_ERROR2;
                            }

                        }

                        /* L4 - 45352 -- unique field changes start*/
                        if( !isBlank( wv.paymentTerm ) )
                        {
                            //BUG FIX - 71688//
                            vendor.EP_Payment_Term_Lookup__r = new EP_Payment_Term__c(EP_Payment_Company_Code__c = wv.paymentTerm+EP_Common_Constant.STRING_HYPHEN+ identifier.clientId);
                            //BUG FIX - 71688//                 
                        }
                        /* L4 - 45352 -- unique field changes end*/

                        if( isBlank( wv.currencyId ) )
                        {
                            strError += STR_CURRENCY_ERROR;
                        }
                        else
                        {
                            vendor.CurrencyIsoCode = wv.currencyId;
                        }

                        if( !isBlank( wv.transportMgmt ) )
                        {
                            vendor.EP_Provider_Managed_Transport_Services__c = wv.transportMgmt;
                        }

                        vendor.EP_Duty__c = wv.duties;


                        if( !isBlank( strError ) )
                        {
                            strError = strError.removeEnd(EP_Common_Constant.COMMA.trim());
                            ErrorDescriptionMap.put( wv.seqId, strError);
                        }
                        else
                        {
                            /* TFS fix 45559,45560,45567,45568 start EP_Vendor_Unique_Id__c replaced by EP_Enterprise_ID__c */
                            //vendor.EP_Vendor_Unique_Id__c = wv.identifier.vendorId + DASH_STRING + wv.identifier.clientId;
                            vendor.EP_Enterprise_ID__c    = wv.enterpriseId;
                            /* TFS fix 45559,45560,45567,45568 end*/
                            lstVendor.add( vendor );
                            vendorForDML.add( wv );
                        }
                    }

                    if( !lstVendor.isEmpty() )
                    {
                        system.debug('---'+lstVendor);
                        /* TFS fix 45559,45560,45567,45568 start EP_Vendor_Unique_Id__c replaced by EP_Enterprise_ID__c */
                        //Schema.DescribeFieldResult Field = Account.EP_Vendor_Unique_Id__c.getDescribe();
                        Schema.DescribeFieldResult Field = Account.EP_Enterprise_ID__c.getDescribe();
                         /* TFS fix 45559,45560,45567,45568 end*/   
                        Schema.sObjectField vendoeExternalId = Field.getSObjectField();

                        list<Database.upsertResult> upsertResult = database.upsert(lstVendor,vendoeExternalId,false);
                //iterate over the upsert result and prepare success map and Error Map
                for( integer index = 0 ; index < upsertResult.size() ; index++ )
                {
                    if( upsertResult[index].isSuccess()  )
                    {
                        SuccessMap.put( vendorForDML[index].seqId, EP_Common_Constant.BLANK );
                    }
                    else
                    {
                        for( Database.Error err : upsertResult[index].getErrors() )
                        {
                            ErrorDescriptionMap.put( vendorForDML[index].seqId , err.getMessage() );    
                            ErrorCodeMap.put( vendorForDML[index].seqId , String.valueOf( err.getStatusCode() ) );
                        }
                    }
                }
                /*** Fix for Defect #54162 Start****/
            }
            //initiate acknowledgement creation
            sendAckmowledgment( SuccessMap, ErrorDescriptionMap, ErrorCodeMap, vendorList, headerCommon, null );
            system.debug(responseBody);
            // }
            /*** Fix for Defect #54162 Start****/
        }
        catch( Exception ex )
        {
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), 
                EP_Common_constant.EPUMA, VENDOR_SYNC, CLASS_NAME, 
                EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, 
                EP_Common_Constant.BLANK);
            String exceptionMessage = ex.getMessage();
            //initiate acknowledgement creation
            sendAckmowledgment( SuccessMap, ErrorDescriptionMap, ErrorCodeMap, vendorList, headerCommon, exceptionMessage );
            system.debug(' ------ responseBody ------ ' + responseBody);
            
        }

        //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        //4: Log the Proccessing Response 
        EP_IntegrationService.logProcessResponse(integrationRecord, responseBody);
        //Code changes End for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        return responseBody; 
    }
    
    private static Boolean isBlank( String str )
    {
        return String.isBlank( str );
    }
    
    /*
    Method to prepare acknowledgement JSON
    */
    public static void sendAckmowledgment(  map<String, String> SuccessMap, 
        map<String, String> ErrorDescriptionMap, 
        map<String, String> ErrorCodeMap, 
        list<WrapperVendor> vendorList, 
        EP_AcknowledmentGenerator.cls_HeaderCommon headerCommon, 
        String exceptionMessage )
    {
        EP_GeneralUtility.Log('Public','EP_VendorSyncHandler','sendAckmowledgment');
        //list to create acknoledgement data
        List<EP_AcknowledmentGenerator.cls_dataset> lstDataset = new List<EP_AcknowledmentGenerator.cls_dataset>();
        //map to create response JSON header 
        map<String, String> mSendHeaderCommon = new map<String, String>();
        try{

            //create message generate name to prepare messageid
            EP_Message_Id_Generator__c oMsgIdGenerator = new EP_Message_Id_Generator__c();
            insert oMsgIdGenerator;
            String strMsgGeneratorName = [Select Name from EP_Message_Id_Generator__c where Id =: oMsgIdGenerator.Id Limit 1].Name;
            String sMsgID = EP_IntegrationUtil.getMessageId(EP_Common_Constant.strSource,
             EP_Common_Constant.strLocation,
             EP_Common_Constant.STRPRODUCT,
             System.NOW(),
             strMsgGeneratorName); 

            mSendHeaderCommon.put(EP_Common_Constant.MSG_ID,sMsgID);            
            
            //if there was any Error in JSON parsing add PROCESS_STATUS 
            //and ERROR_DESCRIPTION in Header of response JSON
            if( String.isNotBlank( exceptionMessage ))
            {
                mSendHeaderCommon.put(EP_Common_Constant.ERROR_DESCRIPTION, exceptionMessage );
                mSendHeaderCommon.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.FAILURE );
            }
            else if( !ErrorDescriptionMap.isEmpty() )
            {
                mSendHeaderCommon.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.FAILURE );
                mSendHeaderCommon.put(EP_Common_Constant.ERROR_DESCRIPTION,headerCommon.ErrorDescription);
            }
            else if( !SuccessMap.isEmpty() )
            {
                mSendHeaderCommon.put(EP_Common_Constant.PROCESS_STATUS, EP_Common_Constant.SUCCESS );
                mSendHeaderCommon.put(EP_Common_Constant.ERROR_DESCRIPTION,headerCommon.ErrorDescription);
            }
            
            mSendHeaderCommon.put(EP_Common_Constant.CORR_ID,headerCommon.msgId);
            mSendHeaderCommon.put(EP_Common_Constant.INTERFACE_TYPE,headerCommon.InterfaceType);
            mSendHeaderCommon.put(EP_Common_Constant.SOURCE_GROUP_COMPANY,headerCommon.SourceGroupCompany);
            mSendHeaderCommon.put(EP_Common_Constant.DESTINATION_GROUP_COMPANY,headerCommon.DestinationGroupCompany);
            mSendHeaderCommon.put(EP_Common_Constant.SOURCE_COMPANY,headerCommon.SourceCompany);
            mSendHeaderCommon.put(EP_Common_Constant.DESTINATION_COMPANY,headerCommon.DestinationCompany);
            mSendHeaderCommon.put(EP_Common_Constant.DESTINATION_ADDRESS,headerCommon.DestinationAddress);
            mSendHeaderCommon.put(EP_Common_Constant.SOURCE_RESP_ADD,headerCommon.SourceResponseAddress);
            mSendHeaderCommon.put(EP_Common_Constant.SOURCE_UPDATE_STTS_ADD,headerCommon.SourceUpdateStatusAddress);
            mSendHeaderCommon.put(EP_Common_Constant.DESTINATION_UPDATE_STATUS_ADDRESS,headerCommon.DestinationUpdateStatusAddress);
            mSendHeaderCommon.put(EP_Common_Constant.MIDDLEWARE_URL_FOR_PUSH,headerCommon.MiddlewareUrlForPush);
            mSendHeaderCommon.put(EP_Common_Constant.EMAIL_NOTIFICATION,headerCommon.EmailNotification);
            mSendHeaderCommon.put(EP_Common_Constant.ACK_ERROR_CODE,headerCommon.ErrorCode);
            mSendHeaderCommon.put(EP_Common_Constant.PROCESSING_ERROR_DESCRIPTION,headerCommon.ProcessingErrorDescription);
            mSendHeaderCommon.put(EP_Common_Constant.CONT_ON_ERR_STTS,headerCommon.ContinueOnError);
            mSendHeaderCommon.put(EP_Common_Constant.COMP_LGNG,headerCommon.ComprehensiveLogging);
            mSendHeaderCommon.put(EP_Common_Constant.TRANSPORT_STATUS,headerCommon.TransportStatus);
            mSendHeaderCommon.put(EP_Common_Constant.UPDT_SRC_ON_RCV,headerCommon.UpdateSourceOnReceive);
            mSendHeaderCommon.put(EP_Common_Constant.UPDT_SRC_ON_DLVRY,headerCommon.UpdateSourceOnDelivery);
            mSendHeaderCommon.put(EP_Common_Constant.UPDT_SRC_AFTER_PRCSNG,headerCommon.UpdateSourceAfterProcessing);
            mSendHeaderCommon.put(EP_Common_Constant.UPDT_DEST_ON_DLVRY,headerCommon.UpdateDestinationOnDelivery);
            mSendHeaderCommon.put(EP_Common_Constant.CALL_DEST_FOR_PRCSNG,headerCommon.CallDestinationForProcessing);
            mSendHeaderCommon.put(EP_Common_Constant.OBJECT_TYPE,headerCommon.ObjectType);
            mSendHeaderCommon.put(EP_Common_Constant.OBJECT_NAME,headerCommon.ObjectName);
            mSendHeaderCommon.put(EP_Common_Constant.COMMUNICATION_TYPE,headerCommon.CommunicationType);
            mSendHeaderCommon.put(EP_Common_Constant.OBJECT_NAME,headerCommon.ObjectName);

            //prepare acknowledgement data
            for(WrapperVendor wv : vendorList )
            {  
                EP_AcknowledmentGenerator.cls_dataset oDataset = new EP_AcknowledmentGenerator.cls_dataset();
                oDataset.name = VENDOR_NODE;
                oDataset.seqId = wv.seqId;
                if( SuccessMap != null && SuccessMap.containsKey( wv.seqId ) )
                {
                    oDataset.errorCode = EP_Common_Constant.BLANK;
                    oDataset.errorDescription = EP_Common_Constant.BLANK;
                    
                }
                else if( ErrorDescriptionMap != null && ErrorDescriptionMap.containsKey( wv.seqId ) )
                {
                    oDataset.errorCode = ErrorCodeMap != null && ErrorCodeMap.containsKey( wv.seqId ) ? ErrorCodeMap.get( wv.seqId ) : EP_Common_Constant.BLANK;
                    oDataset.errorDescription = ErrorDescriptionMap.get( wv.seqId );
                }
                lstDataset.add( oDataset );
            }
            //send header and acknowledgement data to convert into JSON string
            responseBody = EP_AcknowledmentGenerator.createacknowledgement( mSendHeaderCommon , lstDataset, null );
        }
        catch( Exception ex )
        {
            EP_LoggingService.logServiceException(ex, UserInfo.getOrganizationId(), 
                EP_Common_constant.EPUMA, VENDOR_SYNC, CLASS_NAME, 
                EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF, EP_Common_Constant.BLANK, 
                EP_Common_Constant.BLANK);
            //send header and acknowledgement data to convert into JSON string
            responseBody = EP_AcknowledmentGenerator.createacknowledgement( mSendHeaderCommon , lstDataset, null );
        }
    }
    /*TFS fix 45559,45560,45567,45568 start*/ 
    /**
    * @author       Accenture
    * @name         getVendorType
    * @date         02/11/2017
    * @description  getting vendor type from list of values coming from NAV and returning semicolon seperated value for mapping to Vendor Type
    * @param        list<WrapperVendorType> vendorTypes
    * @return       string
    */  
    
     @TestVisible private static String getVendorType(list<WrapperVendorType> vendorTypes){
        string VendorTypeValues;
        for(WrapperVendorType VendorType : vendorTypes)
        {
            if(isTransporter(VendorType.vendorTyp) || isSupplier(VendorType.vendorTyp)){   
                if(VendorTypeValues != null){
                    VendorTypeValues = VendorTypeValues + SEMICOLON_STRING + VendorType.vendorTyp;
                }
                else{
                    VendorTypeValues = VendorType.vendorTyp;
                }
            }
        }
        return VendorTypeValues;
    }
    /* @author       Accenture
    * @name         isTransporter
    * @date         03/11/2017
    * @description  to check transporter vendor type from below method
    * @param        string VendorType
    * @return       boolean
    */  
     @TestVisible private static boolean isTransporter(string VendorType){
		boolean isTransporterValue = false;
        if(VENDOR_TYPE_SFDC_TRSN.equals(VendorType)){
            isTransporterValue = true;
        }
        return isTransporterValue;
	}
       
    /* @author       Accenture
    * @name         isTransporter
    * @date         03/11/2017
    * @description  to check supplier vendor type from below method
    * @param        string VendorType
    * @return       boolean
    */  
   	 @TestVisible private static boolean isSupplier(string VendorType){
       boolean isSupplierValue = false;
       if(VENDOR_TYPE_SFDC_3RD.equals(VendorType)){
           isSupplierValue = true;
       }
       return isSupplierValue;
	}  
    /*TFS fix 45559,45560,45567,45568 end*/
       
    //identifier wrapper
    public with sharing class WrapperIdentifier{
        public string vendorId;
        public string clientId;
        
    }
	/*L4_45559_Start*/
	//VendorType wrapper
    public with sharing class WrapperVendorType{
        public string vendorTyp;
    }
    /*L4_45559_END*/
    //vendor wrapper
    public with sharing class WrapperVendor{
        public string seqId;
        public WrapperIdentifier identifier;
        /*TFS fix 45559,45560,45567,45568 start added new  enterpriseId variable*/
        public string enterpriseId;
		public list<WrapperVendorType> vendorTypes;
        /* TFS fix 45559,45560,45567,45568 End*/
        public string name;
        public string name2;
        public string address;
        public string address2;
        public string city;
        public string postCode;
        public string cntryCode;
        public string blocked;
        public string paymentTerm;
        public string currencyId;
        public string transportMgmt;
        public string duties;
    }
    
    //top level wrapper ( vendors )
    public with sharing class WrapperVendors{
        public list<WrapperVendor> vendor;
    }
    
    public with sharing class WrapperVendorCls{
        public WrapperVendor vendor;
    }
    
}