@isTest
public class EP_ShipToPayloadCtrlExtn_UT
{
    static testMethod void setShipToAddressForLS_test() {
        Account acc = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        PageReference pageRef = Page.EP_ShipToSyncWithNAVXML;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE, '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, String.valueOf(acc.Id));
        EP_ShipToPayloadCtrlExtn localObj = new EP_ShipToPayloadCtrlExtn(sc);
        Test.startTest();
        localObj.setShipToAddressForLS();
        Test.stopTest();
        System.AssertEquals(true,!localObj.supplyLocations.isEmpty());
        System.AssertEquals(true,!localObj.tanks.isEmpty());
    }
    
    static testMethod void checkPageAccess_negativetest() {
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        EP_ShipToPayloadCtrlExtn localObj = new EP_ShipToPayloadCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result!=null);
        System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
    }
    
     static testMethod void checkPageAccess_positivetest() {
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        
        PageReference pageRef = Page.EP_ShipToSyncWithNAVXML;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.PARAM_SECRETCODE, '1234');
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID, String.valueOf(acc.Id));
        
        EP_ShipToPayloadCtrlExtn localObj = new EP_ShipToPayloadCtrlExtn(sc);
        Test.startTest();
        string shipToStatus = localObj.shipToStatus;
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result==null);
    }
}