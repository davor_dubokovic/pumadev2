@isTest
public class EP_ASTStorageLocationNewToDeactivate_UT
{
    static final String event='NEW_RECORD';
    static final String invalidEvent='01-DeactivateToNew';
    /*  
    @description: method to intialise data
    */
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    static testMethod void isTransitionPossible_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(event);
        EP_ASTStorageLocationNewToDeactivate ast = new EP_ASTStorageLocationNewToDeactivate();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isTransitionPossible_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(invalidEvent);
        EP_ASTStorageLocationNewToDeactivate ast = new EP_ASTStorageLocationNewToDeactivate();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isTransitionPossible();
        Test.stopTest();
        System.AssertEquals(false,result);
    }


    static testMethod void isRegisteredForEvent_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(event);
        EP_ASTStorageLocationNewToDeactivate ast = new EP_ASTStorageLocationNewToDeactivate();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isRegisteredForEvent_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(invalidEvent);
        EP_ASTStorageLocationNewToDeactivate ast = new EP_ASTStorageLocationNewToDeactivate();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isRegisteredForEvent();
        Test.stopTest();
        System.AssertEquals(false,result);
    }


    static testMethod void isGuardCondition_positive_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASProspectDomainObjectPositiveScenario();
        EP_AccountEvent ae = new EP_AccountEvent(event);
        EP_ASTStorageLocationNewToDeactivate ast = new EP_ASTStorageLocationNewToDeactivate();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isGuardCondition_negative_test() {
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageLocationASProspectDomainObjectNegativeScenario();
        EP_AccountEvent ae = new EP_AccountEvent(invalidEvent);
        EP_ASTStorageLocationNewToDeactivate ast = new EP_ASTStorageLocationNewToDeactivate();
        ast.setAccountContext(obj,ae);
        Test.startTest();
        boolean result = ast.isGuardCondition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }


}