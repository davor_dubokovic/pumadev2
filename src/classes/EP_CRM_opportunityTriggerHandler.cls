public with sharing class EP_CRM_opportunityTriggerHandler{
    private static set<id> accIds = new set<id>();
    public static Boolean isBeforeInsertRecursiveTrigger = False;
    public static Boolean isAfterInsertRecursiveTrigger = False;
    public static Boolean isBeforeUpdatetRecursiveTrigger = False;
    public static Boolean isAfterUpdateRecursiveTrigger = False;
    public static void afterInsertOpportunity(List<Opportunity> TriggerNew){
        try{
            updateAccount(TriggerNew);
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
    public static void afterUpdateOpportunity(List<Opportunity> TriggerNew){
       try{
            updateAccount(TriggerNew);
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
    public static void beforeInsertAndUpdateOpportunity(List<Opportunity> TriggerNew){
        updateOppor(TriggerNew);
    }
    
    private static void updateOppor(List<Opportunity> TriggerNew){
        map<id,Account> mapIdToAccount = new map<id,Account>();
        list<Account> updateAcc = new list<Account>();
        
        for(Opportunity objOpp : TriggerNew){
            if(objOpp.AccountId != null){
                accIds.add(objOpp.AccountId);
            }
        }
        for(Account objAcc : [select id,EP_Status__c,EP_Payment_Method__c,EP_Payment_Term_Lookup__c,EP_Billing_Method__c,EP_Delivery_Type__c from Account where id IN : accIds]){
            mapIdToAccount.put(objAcc.id,objAcc);
        }
        for(Opportunity objOpport : TriggerNew){
                if(objOpport.StageName != 'Closed Won' && objOpport.StageName != 'Closed Lost'){
                    
                    if(mapIdToAccount.get(objOpport.AccountId) != null){
                        if(objOpport.Payment_Term__c == null){
                            objOpport.Payment_Term__c = mapIdToAccount.get(objOpport.AccountId).EP_Payment_Term_Lookup__c;
                        }
                        if(objOpport.Billing_Method__c == null){
                            objOpport.Billing_Method__c = mapIdToAccount.get(objOpport.AccountId).EP_Billing_Method__c;
                        }
                        if(objOpport.EP_CRM_Payment_Method__c == null){
                            objOpport.EP_CRM_Payment_Method__c = mapIdToAccount.get(objOpport.AccountId).EP_Payment_Method__c;
                        }
                        if(objOpport.Delivery_Type__c == null){
                            objOpport.Delivery_Type__c = mapIdToAccount.get(objOpport.AccountId).EP_Delivery_Type__c;
                        }
                    }
                }
            }
    }
    
    private static void updateAccount(List<Opportunity> TriggerNew){
        try{
            map<id,Account> mapIdToAccount = new map<id,Account>();
            list<Account> updateAcc = new list<Account>();
            
            for(Opportunity objOpp : TriggerNew){
                if(objOpp.AccountId != null){
                    accIds.add(objOpp.AccountId);
                }
            }
            for(Account objAcc : [select id,EP_Status__c,EP_Payment_Method__c,EP_Payment_Term_Lookup__c,EP_Billing_Method__c,EP_Delivery_Type__c from Account where id IN : accIds]){
                mapIdToAccount.put(objAcc.id,objAcc);
            }
            for(Opportunity objOpport : TriggerNew){
                if(objOpport.StageName != 'Closed Won' && objOpport.StageName != 'Closed Lost'){
                    
                    if(mapIdToAccount.get(objOpport.AccountId) != null){
                        
                        boolean isCheck;
                        if(mapIdToAccount.get(objOpport.AccountId).EP_Payment_Method__c != objOpport.EP_CRM_Payment_Method__c || mapIdToAccount.get(objOpport.AccountId).EP_Payment_Term_Lookup__c != objOpport.Payment_Term__c || mapIdToAccount.get(objOpport.AccountId).EP_Billing_Method__c != objOpport.Billing_Method__c || mapIdToAccount.get(objOpport.AccountId).EP_Delivery_Type__c != objOpport.Delivery_Type__c){
                            if(objOpport.Payment_Term__c != null){
                                mapIdToAccount.get(objOpport.AccountId).EP_Payment_Term_Lookup__c = objOpport.Payment_Term__c;
                                isCheck = true;
                            }
                            if(objOpport.Billing_Method__c != null){
                                mapIdToAccount.get(objOpport.AccountId).EP_Billing_Method__c = objOpport.Billing_Method__c;
                                isCheck = true;
                            }
                            if(objOpport.EP_CRM_Payment_Method__c != null){
                                 mapIdToAccount.get(objOpport.AccountId).EP_Payment_Method__c = objOpport.EP_CRM_Payment_Method__c;
                                 isCheck = true;
                            }
                            if(objOpport.Delivery_Type__c != null){
                                mapIdToAccount.get(objOpport.AccountId).EP_Delivery_Type__c = objOpport.Delivery_Type__c;
                                isCheck = true;
                            } 
                            if(isCheck){
                                 updateAcc.add(mapIdToAccount.get(objOpport.AccountId));
                            }
                           
                        }
                        
                    }
                }
            }
            if(updateAcc != null && !updateAcc.isEmpty()){
                database.SaveResult[] updateAccSaveResult = Database.update(updateAcc);
            }
        }
        catch(exception ex){
            ex.getmessage();
        }
    }
}