/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToProspectToProspect>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 01-Prospect to 01-Prospect>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToProspectToProspect extends EP_AccountStateTransition {

    public EP_ASTVMIShipToProspectToProspect() {
        finalState = EP_AccountConstant.PROSPECT;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToProspectToProspect','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToProspectToProspect','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToProspectToProspect','isGuardCondition');
        
        /* #sprintRouteWork starts */
        EP_AccountService service =  new EP_AccountService(this.account);
        if(!service.isRouteAllocationAvailable()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Route_Not_Allocated);
            return false;
        }
        /* #sprintRouteWork ends */
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToProspectToProspect','doOnExit');

    }
}