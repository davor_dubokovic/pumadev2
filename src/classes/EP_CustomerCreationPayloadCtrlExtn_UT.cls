@isTest
public class EP_CustomerCreationPayloadCtrlExtn_UT
{
    static testMethod void setShipToAddressesForLS_test() {
        Account acc = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        Account sellTo = [select id from Account where Id =: acc.ParentId];
        sellTo.EP_Requested_Payment_Terms__c=EP_Common_Constant.PREPAYMENT;
        sellTo.EP_Requested_Packaged_Payment_Term__c=EP_Common_Constant.PREPAYMENT;
        update sellTo;
        PageReference pageRef = Page.EP_CustomerCreationWithWINDMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(sellTo);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , sellTo.Id);
        EP_CustomerCreationPayloadCtrlExtn localObj = new EP_CustomerCreationPayloadCtrlExtn(sc);
        Test.startTest();
        localObj.setShipToAddressesForLS();
        string shipToStatus  = localObj.shipToAddressesLS[0].shipToStatus;
        Integer deferUpperLimit = localObj.deferUpperLimit ; 
        Test.stopTest();
        System.AssertEquals(true,!localObj.shipToAddressesLS.isEmpty());
    }
    static testMethod void setShipToAddressesForLS_ForDummy() {
        EP_AccountDomainObject accDomain = EP_TestDataUtility.getStorageShipToASAccountSetupDomainObject();
        EP_AccountMapper accMapper = new EP_AccountMapper();
        Account acc = accDomain.getAccount();
        Account sellTo = accMapper.getAccountRecord(acc.ParentId);
        PageReference pageRef = Page.EP_CustomerCreationWithWINDMSXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(sellTo);
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , sellTo.Id);
        EP_CustomerCreationPayloadCtrlExtn localObj = new EP_CustomerCreationPayloadCtrlExtn(sc);
        Test.startTest();
        localObj.setShipToAddressesForLS();
        Integer deferUpperLimit = localObj.deferUpperLimit ; 
        Test.stopTest();
        System.AssertEquals(true,!localObj.shipToAddressesLS.isEmpty());
    }
    
    static testMethod void setBankAccounts_test() {
        Account acc = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        PageReference pageRef = Page.EP_CustomerCreationWithNAVXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(new account(Id=acc.ParentId));
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , acc.ParentId);
        EP_CustomerCreationPayloadCtrlExtn localObj = new EP_CustomerCreationPayloadCtrlExtn(sc);
        Test.startTest();
        localObj.setBankAccounts();
        Test.stopTest();
        System.AssertEquals(true,!localObj.bankAccounts.isEmpty());
    }
    static testMethod void setShipToAddresses_test() {
        Account acc = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        PageReference pageRef = Page.EP_CustomerCreationWithNAVXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(new account(Id=acc.ParentId));
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , acc.ParentId);
        EP_CustomerCreationPayloadCtrlExtn localObj = new EP_CustomerCreationPayloadCtrlExtn(sc);
        Test.startTest();
        localObj.setShipToAddresses();
        Test.stopTest();
        System.AssertEquals(true,!localObj.shipToAddresses.isEmpty());
    }
    static testMethod void checkPageAccess_test() {
        Account acc = EP_TestDataUtility.getVMIShipToASAccountSetupInPositiveScenario();
        PageReference pageRef = Page.EP_CustomerCreationWithNAVXML;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(new account(Id=acc.ParentId));
        ApexPages.currentPage().getParameters().put(EP_Common_Constant.ID , acc.ParentId);
        EP_CustomerCreationPayloadCtrlExtn localObj = new EP_CustomerCreationPayloadCtrlExtn(sc);
        Test.startTest();
        PageReference result = localObj.checkPageAccess();
        Test.stopTest();
        System.AssertEquals(true,result != NULL);
        System.AssertEquals(true,result.getURL().toUpperCase().contains(EP_Common_Constant.UNAUTHORIZEDACCESSPAGESTR.toUpperCase()));
    }
}