/**
    **L4-45352 start***
   @Author Accenture CR Team
   @name <EP_ActionMapper_UT>
   @CreateDate 12/27/2106
   @Description This class will be use to test/cover unit test cases for EP_ActionMapper_UT class
   @Version <1.0>
   @reference <Referenced program names>
*/
  
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
@isTest
private class EP_ActionMapper_UT {
    private static final integer BULK_COUNT = 1;

    public static testMethod void getOpenActionCountInSellToHierarchy_Test(){
        Account acc=EP_TestDataUtility.getSellTo();
        System.debug('**********'+acc);
        Test.startTest();
        Integer count= EP_ActionMapper.getOpenActionCountInSellToHierarchy(acc.Id);
        Test.stopTest();
        System.assertEquals(0, count); 
    }

    public static testMethod void getKYCRejectedReviewCount_Test(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        Account acc= EP_TestDataUtility.getSellToASBasicDataSetUpWithKYCReviewRequired();
        Test.startTest();
        Integer count= actionObj.getKYCRejectedReviewCount(acc.Id);
        Test.stopTest();
        System.assertEquals(0, count);
    }

    public static testMethod void getCountOfCompletedReviewsOnShipTo_Test(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        EP_AccountDomainObject ado = EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        account acc = ado.getAccount();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
        Test.startTest();
        Integer count= actionObj.getCountOfCompletedReviewsOnShipTo(acc);
        Test.stopTest();
        System.assertEquals(2, count);

    }
    
    public static testMethod void getCountOfCSCReviewCompletedOnShipTo(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        EP_AccountDomainObject ado;
        ado = EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        Test.startTest();
        Integer count = actionObj.getCountOfCSCReviewCompletedOnShipTo(ado.getAccount());
        Test.stopTest();
        System.assertEquals(0, count);

    }

    public static testMethod void getCountOfCSCReviewCompletedOnTanks(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        Account account;
        account = EP_TestDataUtility.getShipToPositiveScenario();
        Test.startTest();
        Integer count = actionObj.getCountOfCSCReviewCompletedOnTanks(account);
        Test.stopTest();
        System.assertEquals(0, count);
    }
    public static testMethod void getCountOfIncompleteReviewActionsonShipTo(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        EP_AccountDomainObject ado = EP_TestDataUtility.getVMIShipToASBasicDataSetupDomainObjectPositiveScenario();
        account acc = ado.getAccount();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
        Test.startTest();
        Integer count= actionObj.getCountOfIncompleteReviewActionsonShipTo(acc);
        Test.stopTest();
        System.assertEquals(true,count > 0);

    }
    public static testMethod void getKYCReview_test(){
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
        Test.startTest();
        List<EP_Action__c> actions = EP_ActionMapper.getKYCReview(acc.id);
        Test.stopTest();
        System.assertEquals(actions.size(), 0);
    }

    public static testMethod void getCountOfActionsforSellTo_test(){
        EP_ActionMapper actionObj =new EP_ActionMapper();    
        set<id> idSet;
        set<string> stringSet;
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        Test.startTest();
        List<AggregateResult> result = actionObj.getCountOfActionsforSellTo(idSet, stringSet);
        Test.stopTest();
        System.assertEquals(0, result.size());
    }

    public static testMethod void getActionsByActionNameAndParentId_test(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        Id accountId;
        Set<String> actionNames;
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        Test.startTest();
        List<EP_Action__c> result = actionObj.getActionsByActionNameAndParentId(accountId, actionNames);
        Test.stopTest();
        System.assertEquals(0, result.size());
    }
    public static testMethod void getActionsAssociatedToPriceBook_test(){
        Pricebook2 Pricebook = EP_TestDataUtility.createPricebook2(true);
        EP_action__c PBAction = EP_TestDataUtility.createPriceBookAction(pricebook.id);
        Test.startTest();
        List<EP_Action__c> result = EP_ActionMapper.getActionsAssociatedToPriceBook(Pricebook.id);
        Test.stopTest();
        System.assertEquals(true, result.size()>0);
    }
    public static testMethod void getActionRecordsByAccountID_test(){
        EP_ActionMapper actionObj;
        set<id> idSet = new set<id>();
        Account acc = EP_TestDataUtility.getShipToPositiveScenario();
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
        system.debug('acc>>>'+acc);
        Test.startTest();
        actionObj =new EP_ActionMapper(new set<id>{acc.id}); 
        List<EP_Action__c> result = actionObj.getActionRecordsByAccountID(new set<id>{acc.id});
        Test.stopTest();
        System.assertEquals(true, result.size()>0);
    }
    public static testMethod void getKycDetailsForSellTo_test(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        Account acc = EP_TestDataUtility.getSellToPositiveScenario();
        acc.EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
        List<Ep_action__c> action = [select id,EP_Account__c,EP_Status__c,EP_Tank__c,Action_Type__c from ep_action__c where EP_Account__c=:acc.id];
        system.debug('action>>>>'+action);
        
        Test.startTest();
        List<EP_Action__c> actions = actionObj.getKycDetailsForSellTo(acc.id);
        Test.stopTest();
        System.assert(actions.size()>0);
    }
    
    public static testMethod void getActionsforUserCompanyAssociation_test(){
        EP_ActionMapper actionObj =new EP_ActionMapper();
        Account acc = EP_TestDataUtility.getSellToPositiveScenario();
        acc.EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;
        //List<Ep_action__c> action = [select id,EP_Account__c,EP_Status__c,EP_Tank__c,Action_Type__c from ep_action__c where EP_Account__c=:acc.id];
        //system.debug('action>>>>'+action);
        
        Test.startTest();
        List<EP_Action__c> actions = actionObj.getActionsforUserCompanyAssociation('EP_KYC_Review',acc.EP_Puma_Company__c);
        Test.stopTest();
        System.assert(true, actions.size()>0);
    }
     /***L4-45352 end****/

     static testMethod void getConsignmentReview_test(){
        Account account = EP_TestDataUtility.getShipToPositiveScenario();
        EP_Action__c actObj = EP_TestDataUtility.createNewAction(account);
        insert actObj;
        Test.startTest();
        List<EP_Action__c> actions = EP_ActionMapper.getConsignmentReview(account.id);
        Test.stopTest();
        System.assert(true, actions.size()>0);
     }

}