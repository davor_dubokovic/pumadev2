@isTest
public class EP_PowerBIControllerTest {   
    
    public static PageReference pageRef = Page.EP_PowerBI_Report;
    public static EP_OAuthApp_pbi__c app;
    public static Account account = new Account();
    public static ApexPages.StandardController sc = new ApexPages.StandardController(account);
    public static EP_PowerBIController pbicontroller = new EP_PowerBIController(sc );
        
    @testSetup public static void setUp()
    {   
        app = new EP_OAuthApp_pbi__c();
        app.Name = 'PowerBI2';
        app.EP_Token_Expires_On__c = '0';
        app.EP_Client_Id__c = 'clientId';
        app.EP_Client_Secret__c = 'clientSecret';
        app.EP_Authorization_URL__c = 'https://login.windows.net/common/oauth2/authorize';
        app.EP_Access_Token_URL__c = 'https://login.microsoftonline.com/common/oauth2/token';
        app.EP_Resource_URI__c = 'https://analysis.windows.net/powerbi/api';
        insert app;

        pbicontroller.application_name = 'PowerBI2';
    }
        
    public static testMethod void powerBiControllerNotNull()
    {
        System.assertNotEquals(pbicontroller, null);
    }
    
    public static testMethod void getValidateResultReturnsNotNull()
    {
        pbicontroller.validateResult = 'testResult';
        String validate = pbicontroller.getValidateResult();
        System.assertEquals('testResult', pbicontroller.getValidateResult());       
    }
    
    public static testMethod void callRedirect()
    {
        PageReference page = pbicontroller.redirectOnCallback();        
    }
    
    public static testMethod void callRefreshToken()
    {   Test.startTest();
        Test.setMock(HttpCalloutMock.class, new EP_MockOAuthResponseGenerator());
        
        pbicontroller.application_name = 'PowerBI2';
        Test.setCurrentPage(pageRef);       

        PageReference page = pbicontroller.refreshAccessToken();
                
        String accessCookie = pbicontroller.PBIAccess_token;
        String refreshCookie =  pbicontroller.PBIRefresh_token;
        
        System.assertEquals('accessCookieToken',accessCookie);
        System.assertEquals('refreshCookieToken',refreshCookie);    
        Test.stopTest();
    }
}