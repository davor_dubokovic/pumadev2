@isTest
public class EP_StorageShipToASProspect_UT
{
    static final string EVENT_NAME = '01-ProspectTo01-Prospect';
    static testMethod void setAccountDomainObject_test() {
        EP_StorageShipToASProspect localObj = new EP_StorageShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASProspectDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        // **** IMPLEMENT THIS SECTION ~@~ *****
            // **** TILL HERE ~@~ *****
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        // **** IMPLEMENT ASSERT ~@~ *****
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
        // **** TILL HERE ~@~ *****
    }
    static testMethod void doOnEntry_test() {
        EP_StorageShipToASProspect localObj = new EP_StorageShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASProspectDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        System.Assert(true);
        // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
        // **** IMPLEMENT ASSERT ~@~ *****
        //System.AssertEquals(true,<asset conditions>);
        // **** TILL HERE ~@~ *****
    }
    static testMethod void doOnExit_test() {
        EP_StorageShipToASProspect localObj = new EP_StorageShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASProspectDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.Assert(true);
        // This method does not need assertion since it is just logging the method (EP_GeneralUtility.Log())
        // **** IMPLEMENT ASSERT ~@~ *****
        //System.AssertEquals(true,<asset conditions>);
        // **** TILL HERE ~@~ *****
    }
    static testMethod void doTransition_PositiveScenariotest() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        EP_StorageShipToASProspect localObj = new EP_StorageShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_StorageShipToASProspect localObj = new EP_StorageShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        string errormsg = null;
        try{
            Boolean result = localObj.doTransition();
        }catch (exception e){
            errormsg = e.getmessage();
        }
        Test.stopTest();
        System.AssertEquals(true,errormsg != null);
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_StorageShipToASProspect localObj = new EP_StorageShipToASProspect();
        EP_AccountDomainObject obj = EP_TestDataUtility.getStorageShipToASProspectDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
}