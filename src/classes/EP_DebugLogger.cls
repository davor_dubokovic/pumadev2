/* 
      @Author <Sandeep Kumar>
      @name <EP_SystemUtil>
      @CreateDate <22/12/2016>
      @Description <EP System class which overrides standard salesforce functionality>
      @note <Please don't add try catch block in this class. There is no chances of dynamic error>
      @Version <1.0>
     
*/
global class EP_DebugLogger {
    @testVisible private static Object DEBUG_LOG_MESSAGE = NULL; //for assertion purpose in test class
    private static Map<String, EP_Debug_Log_Switch_Setting__c> debugSwitchMap = EP_Debug_Log_Switch_Setting__c.getall(); 
    
    /* 
     * @Description : Use this method to print debug logs based on custom setting.
     */
    global static void printDebug(Object message) {
        DEBUG_LOG_MESSAGE = null;
        if(debugSwitchMap.get(EP_Common_Constant.DEBUG_LOG) != null && debugSwitchMap.get(EP_Common_Constant.DEBUG_LOG).Enable__c) {
            DEBUG_LOG_MESSAGE = message;
            system.debug(DEBUG_LOG_MESSAGE);
        }
    }
    
    /* 
     * @Description : Use this method to print debug logs based on custom setting.
     */
    global static void printDebug(LoggingLevel logLevel, Object message) {        
        DEBUG_LOG_MESSAGE = null;
        if(debugSwitchMap.get(EP_Common_Constant.DEBUG_LOG) != null && debugSwitchMap.get(EP_Common_Constant.DEBUG_LOG).Enable__c) {
            DEBUG_LOG_MESSAGE = message;
            system.debug(logLevel, DEBUG_LOG_MESSAGE);
        }
    }    
}