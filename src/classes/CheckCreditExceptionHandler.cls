/*
   @Author          CloudSense
   @Name            CreateCreditExceptionHandler
   @CreateDate      22/03/2018
   @Description     This class is responsible for checking field status on last created EP_Credit_Exception_Request__c
   @Version         1.1
 
*/
global class CheckCreditExceptionHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         22/03/2018
    * @Description  Method to process the step
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        List<Id> orderIdList = new List<Id>();

        system.debug('StepList is :' +stepList);
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_AvailableFunds__c,
                                                                CSPOFA__Orchestration_Process__r.Order__r.TotalAmount__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.CurrencyIsoCode, CSPOFA__Orchestration_Process__r.Order__r.CreatedBy.FederationIdentifier,
                                                                CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Overdue_Amount__c, CSPOFA__Orchestration_Process__r.Order__r.csord__Account__r.EP_Balance__c,
                                                                CSPOFA__Orchestration_Process__r.Order__r.EP_Order_Comments__c,CSPOFA__Orchestration_Process__r.Order__r.EP_Is_Credit_Exception_Cancelled_Sync__c,CSPOFA__Orchestration_Process__r.Order__r.Cancellation_Check_Done__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                                            
        
                                                            
        system.debug('extended list is :' +extendedList);
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);

        }
        
        List<EP_Credit_Exception_Request__c> lstCreditExceptions = [SELECT 
                                                                        Id, Name, EP_Integration_Status__c, EP_CS_Order__c 
                                                                    FROM EP_Credit_Exception_Request__c
                                                                    where EP_CS_Order__c IN :orderIdList 
                                                                    ORDER BY LastModifiedDate DESC];
                                                                    
        Map<Id,List<EP_Credit_Exception_Request__c>> mapStepToRequest = new Map<Id,List<EP_Credit_Exception_Request__c>>();
        
        for(CSPOFA__Orchestration_Step__c step : extendedList){ 
            for ( EP_Credit_Exception_Request__c currCreditException : lstCreditExceptions) {
                if (currCreditException.EP_CS_Order__c == step.CSPOFA__Orchestration_Process__r.Order__c) {
                    if( mapStepToRequest.get( step.Id ) != null ){
            			mapStepToRequest.get( step.Id ).add( currCreditException );
            		} else{
            			mapStepToRequest.put( step.Id, new List<EP_Credit_Exception_Request__c>{ currCreditException });
            		}
            		break;
                }
            }
        }
        
        for(CSPOFA__Orchestration_Step__c step : extendedList){ 
            if ( mapStepToRequest.get(step.Id).get(0).EP_Integration_Status__c == 'SYNC' ) {
                step.CSPOFA__Status__c ='Complete';
                step.CSPOFA__Completed_Date__c=Date.today();
                step.CSPOFA__Message__c = 'Custom step succeeded';
                result.add(step);
            }
		}
        return result; 
    }
}