@isTest
private class EP_StorageLocationTriggerHandler_test{

    static testMethod void sellToAttachSHLExRackSM(){
        Integer count;
        EP_Freight_Matrix__c freightMatrix;
        String sellTo = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ
                                                            ,EP_Common_Constant.SELL_TO);
        Profile SM_PROFILE = [Select id from Profile
                                Where Name = :EP_Common_Constant.SM_AGENT_PROFILE
                                limit :EP_Common_Constant.ONE];
            User smUser = EP_TestDataUtility.createUser(SM_PROFILE.id);
            Test.startTest();
            System.runAs(smUser){  
            //CREATE FRIEGHT MATRIX
            freightMatrix = EP_TestDataUtility.createFreightMatrix();
            database.insert(freightMatrix);
            //CREATE CUSTOMER
            Account parentAcc = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
            parentAcc.EP_Delivery_Type__c = EP_Common_Constant.EX_RACK;//Insert Sell To Delivery Type as Ex-Rack
            database.insert(parentAcc); 
            EP_Stock_holding_location__c SHL = EP_TestDataUtility.createSellToStockLocation(parentAcc.id,true,null,null); //Add one SHL
            Database.insert(SHL,false);
            system.assertEquals(SHL.EP_Sell_To__c,parentAcc.id);
            parentAcc.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP; //Change status to Basic Data setup 
            Database.update(parentAcc,false);
            system.assertEquals(EP_Common_Constant.STATUS_BASIC_DATA_SETUP,parentAcc.EP_Status__c);
            Account acc = [select id,EP_Allow_Only_Manual_Invoicing__c from Account where id =: parentAcc.id];
            system.assertEquals(false,acc.EP_Allow_Only_Manual_Invoicing__c);
            parentAcc.EP_Allow_Only_Manual_Invoicing__c = true;
            Database.update(parentAcc,false);
            system.assertEquals(true,parentAcc.EP_Allow_Only_Manual_Invoicing__c);
            // Data to cover the code
            List<EP_Stock_Holding_Location__c> lstHolding = new List<EP_Stock_Holding_Location__c>();
            EP_StorageLocationTriggerHandler.doBeforeInsertUpdate(lstHolding );
            List<EP_Stock_Holding_Location__c>lOldStorageLocations = new List<EP_Stock_Holding_Location__c>();
            List<EP_Stock_Holding_Location__c>lNewStorageLocations = new List<EP_Stock_Holding_Location__c>();
            map<id,EP_Stock_Holding_Location__c>mOldStorageLocations = new map<id,EP_Stock_Holding_Location__c>();
            map<id,EP_Stock_Holding_Location__c>mNewStorageLocations = new map<id,EP_Stock_Holding_Location__c>();
          
            //EP_StorageLocationTriggerHandler.doBeforeUpdate(lNewStorageLocations ,mOldStorageLocations ,mNewStorageLocations );
            //EP_StorageLocationTriggerHandler.doBeforeDelete (lNewStorageLocations ,mOldStorageLocations );                        
        }  
        Test.stopTest();  
        
        }
        
/**L4_45352_start**/

    private static Account SellTo;
    private static Account ShipTo;
    Private static account Transporter;
    private static EP_Tank__c VarTank;//EP_Action__c
    private static EP_Action__c VarAction;//
    private static Account VarstorageLocationAccount;
    private static EP_Stock_Holding_Location__c ShipToVarstorageLocation;
    private static EP_Stock_Holding_Location__c VarstorageLocation;
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    
    static void dataSetup()
    {
        
        SellTo = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        insert SellTo; 
        ShipTo = EP_TestDataUtility.createShipToAccount(SellTo.Id, null);
        insert ShipTo;
        BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true LIMIT 1];
            
        EP_Country__c country1 = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country1); 
        
        Transporter = EP_TestDataUtility.createTestVendor('Transporter', '9752', '0098');
        insert Transporter; 
        system.debug('Transporter>>>'+Transporter);
        VarstorageLocationAccount = EP_TestDataUtility.createStorageLocAccount(country1.id,bhrs.id);
        insert VarstorageLocationAccount;
        ID sellToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Sell-To Supply Location');
        ID SHIPToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Ship-To Supply Location');
        VarstorageLocation = EP_TestDataUtility.createSellToStockLocation(SellTo.id,false,VarstorageLocationAccount.id,sellToSHLRTID);
        insert VarstorageLocation; 
        ShipToVarstorageLocation = EP_TestDataUtility.createShipToStockLocation(ShipTo.id,true,VarstorageLocationAccount.id,SHIPToSHLRTID);
        //insert ShipToVarstorageLocation; 
    }
    static testMethod void isSellToSupplyOptionPrimaryonShipTo_Test()
    {
        dataSetup();        
        delete VarstorageLocation;
        
    }
    static testMethod void isSellToSupplyOptionPrimaryonShipTonegative_Test()
    {
        dataSetup();
        
        ID SHIPToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Ship-To Supply Location');
        EP_Stock_Holding_Location__c shiptoStoLoc = new EP_Stock_Holding_Location__c(EP_Location_Type__c= EP_Common_Constant.PRIMARY,
        EP_Ship_To__c = ShipTo.id
        ,EP_Transporter__c = Transporter.id,
        EP_Use_Managed_Transport_Services__c = 'TRUE',
        Stock_Holding_Location__c = VarstorageLocationAccount.id,
        RecordTypeID = SHIPToSHLRTID);
        insert shiptoStoLoc;
        update shiptoStoLoc;
        try{
            delete VarstorageLocation;
        }catch(exception e)
        {
            system.debug('valid error'+e.getmessage());
        }
        system.assertEquals('Transporter',Transporter.EP_VendorType__c);
    }
/**L4_45352_end**/
}