/*
   @Author          CloudSense
   @Name            CreditExceptionCheckHandler
   @CreateDate      25/01/2018
   @Description     This class is responsible for Checking Credit Exception Status
   @Version         1.1
 
*/

global class CreditExceptionCheckHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         25/01/2018
    * @Description  Method to process the step check Credit Exceptions on Order
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        system.debug('mategr orch 1');
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        system.debug('mategr orch 2');
        system.debug('StepList is :' +stepList);
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                                            
        system.debug('extended list is :' +extendedList);
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c);
            system.debug('Order Id is :' +step.CSPOFA__Orchestration_Process__r.Order__c) ;
            //mark step Status, Completed Date, and write optional step Message
          
        }
        system.debug('mategr orch 4');
        system.debug('result is :' +result);
        system.debug('orderIdList is :' +orderIdList);
        system.debug('mategr orch 8');
        
                                          
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            system.debug('mategr orch 10');
            
            Id crId = orderIdList[0];
            
            try{
                system.debug('mategr orch 11');
                checkCreditExceptionStatus(crId);
            }Catch(Exception e){
                system.debug('mategr orch 12');
             system.debug('exception is :' + e);
             EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'CreditExceptionCheckHandler',apexPages.severity.ERROR);
            
          }
          system.debug('mategr orch 13');
          
           step.CSPOFA__Status__c ='Complete';
           step.CSPOFA__Completed_Date__c=Date.today();
           step.CSPOFA__Message__c = 'Custom step succeeded';
           result.add(step);
            
        }
        system.debug('mategr orch 14 ' + result);
        system.debug(' final result is :' +result);
     return result;        
        
        
    }

    public void checkCreditExceptionStatus(Id orderId) {
    	List<EP_Credit_Exception_Request__c> creditExcepList = new List<EP_Credit_Exception_Request__c>();
        EP_OrderMapper mapper = new EP_OrderMapper();
        csord__Order__c order = mapper.getCsRecordById(orderId);
        if(!order.Credit_Exception_Requests2__r.isEmpty()){
	        for(EP_Credit_Exception_Request__c creditExcep : order.Credit_Exception_Requests2__r){
	        	if(creditExcep.EP_Status__c == 'Approved' || creditExcep.EP_Status__c == 'Pending Approval'){
        			order.EP_Credit_Exceptions_Cancelled__c = true;
	        		creditExcep.EP_Status__c = 'Cancelled';
	        		creditExcepList.add(creditExcep);        	
	        	}
	        }
	        upsert creditExcepList;
	    }
    } 
}