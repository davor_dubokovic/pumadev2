/* 
  @Author <Spiros Markantonatos>
   @name <EP_SelectTankDipDatePageExtensionClass>
   @CreateDate <18/09/2015>
   @Description <This class is used by the users to select the date that they want to submit tank dip for>
   @Version <1.0>
 
*/
public with sharing class EP_SelectTankDipDatePageExtnClass_R1 {
    private static final String RET_STRING = 'ret';
    private static final string DATE_STRING = 'date';
    private static final string RETRIEVE_MISSING_SITE_DATES = 'retrieveMissingSiteDates';
    private static final string EP_SELECT_TANKDIP_DATE_PAGE = 'EP_SelectTankDipDatePageExtnClass_R1';
    private static final string DATE_FORMAT = 'yyyyMMddHHmmss';
    private static final String strTankDipSubUrl='/apex/EP_TankDipSubmitPage_R1?id=';
    private static final String strAndSymb = '&';
    private static final String strEqSymb = '=';
    private static final String strTnkDipSiteUrl='/apex/EP_SelectTankDipSitePage_R1';

    // Properties
    public List<tankDipDatesClass> tankDipDates {get;set;}
    public Date dSelectedDate {get;set;}         // Variable used to select a specific tank dip date
    public String strSelectedShipToID {get;set;} // Variable used to select the site that the user was viewing before, 
                                                 // when they click on the back button
    // Fix for Defect 30169 - Start
    // Const
    private static final String OPERATIONAL_TANK_STATUS = EP_Common_Constant.OPERATIONAL_TANK_STATUS;
    
    public Boolean isSiteMissingDipsForToday {
        get {
            Integer queryRows;
            if (isSiteMissingDipsForToday == NULL)
            {
                isSiteMissingDipsForToday = FALSE;
                
                // Check if the site is missing tank dips for today
                if (strSelectedShipToID != NULL)
                {
                    queryRows = EP_Common_Util.getQueryLimit();
                    List<EP_Tank__c> lTanks = [SELECT Id 
                                                FROM EP_Tank__c 
                                                    WHERE EP_Ship_To__c = :strSelectedShipToID
                                                        AND EP_Tank_Status__c = :OPERATIONAL_TANK_STATUS
                                                            AND EP_Tank_Missing_Dip_For_Today__c = TRUE Limit :queryRows];
                    
                    isSiteMissingDipsForToday = (!lTanks.isEmpty());
                }
            }
            
            return isSiteMissingDipsForToday;
        }
        set;
    }
    
    public DateTime siteCurrentDateTime {
        get {
            Integer queryRows;
            if (siteCurrentDateTime == NULL)
            {
                siteCurrentDateTime = System.Now();
                
                if (strSelectedShipToID != NULL)
                {
                   queryRows = EP_Common_Util.getQueryLimit();
                    for (Account a : (List<Account>)
                                            [SELECT EP_Ship_To_Current_Date_Time__c 
                                                FROM Account 
                                                    WHERE Id = :strSelectedShipToID Limit :queryRows])
                    {
                        siteCurrentDateTime = a.EP_Ship_To_Current_Date_Time__c;
                    }
                }
            }
            return siteCurrentDateTime;
        }
        set;
    }
    
    // Fix for Defect 30169 - End
    
    public String strSelectedShipToName {get;set;}
    private DateTime dtCurrentShipToDateTime {get;set;}
    
    public Integer intPageNumber {
        get {
            // Fix for Defect 29809 - Start
            if (intPageNumber == NULL)
            {
                if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER) != NULL)
                {
                    if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER).isNumeric())
                        intPageNumber = Integer.valueOf(ApexPages.currentPage().getParameters().get(EP_Common_Constant.PAGE_NUMBER));
                }
            }
            // Fix for Defect 29809 - End
            return intPageNumber;
        }
        set;
    }
    
    public Boolean blnRedirectToAccountPage {
        get {
            Boolean blnResult = FALSE;
            if (ApexPages.currentPage().getParameters().get(RET_STRING) != NULL) {
                blnResult = (ApexPages.currentPage().getParameters().get(RET_STRING) == EP_Common_Constant.ACCOUNT_OBJ);
            }
            return blnResult; 
        }
    }
    
    /*
        Constructor
    */
    public EP_SelectTankDipDatePageExtnClass_R1() {
        // Initialise variables
        strSelectedShipToID = NULL;
    }
    
    /*
        Functions
    */
    public PageReference retrieveMissingSiteDates() {
        
        PageReference ref = new PageReference(strTnkDipSiteUrl);
        String strShipToScheduledTime = NULL;
        String strURL = NULL;
        
    
        // Retrieve site ID from URL (if any)
        if (ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID) != NULL) {
            strSelectedShipToID = ApexPages.currentPage().getParameters().get(EP_Common_Constant.ID);
            
            // Smark
            // 13/07/2016
            // Check if the user navigated to the date selection page from the account view page
            strURL = strTankDipSubUrl + strSelectedShipToID + EP_Common_Constant.PAGE_NUM_PARAM + intPageNumber;
            
            if (ApexPages.currentPage().getParameters().get(RET_STRING) != NULL) {
                strURL += strAndSymb  + RET_STRING + strEqSymb  + ApexPages.currentPage().getParameters().get(RET_STRING);
            }
            
            ref = new PageReference(strURL);
            
            // Use the selected tank dip date to pre-populate the reading date/time
            // The URL parameter will always contain the site date (not the running user's date)
           
            List<Account> accounts = [SELECT ID, Name,EP_Ship_To_Current_Date_Time__c, EP_Tank_Dips_Schedule_Time__c 
                                                        FROM Account 
                                                            WHERE ID = :strSelectedShipToID LIMIT :EP_COMMON_CONSTANT.ONE];
            
            if (!accounts.isEmpty()) {
                strSelectedShipToName = accounts[0].Name;
                strShipToScheduledTime = accounts[0].EP_Tank_Dips_Schedule_Time__c;
                dtCurrentShipToDateTime = accounts[0].EP_Ship_To_Current_Date_Time__c; 
            }
            
            // Retrieve selected date from URL
            if (ApexPages.currentPage().getParameters().get(DATE_STRING) != NULL) {
                String strSelectedDate = ApexPages.currentPage().getParameters().get(DATE_STRING);
                
                // Convert string to date
                if (strSelectedDate.length() >= 8) {
                    try {
                        Integer intYear = Integer.valueOf(strSelectedDate.left(4));
                        Integer intMonth = Integer.valueOf(strSelectedDate.substring(4, 6));
                        Integer intDay = Integer.valueOf(strSelectedDate.substring(6, 8));
                        Time t = Time.newInstance(0, 0, 0, 0);
                        
                        if (strShipToScheduledTime != NULL){
                            t = EP_PortalLibClass_R1.returnTimeFromString(strShipToScheduledTime );
                        }
                        DateTime dSelectedDateTime = DateTime.newInstance(intYear, intMonth, intDay, t.Hour(), t.Minute(), t.Second());
                        dSelectedDate = Date.newInstance(intYear, intMonth, intDay);
                        
                    } catch (exception e) {
                        EP_LoggingService.logHandledException(e, EP_Common_Constant.EPUMA, RETRIEVE_MISSING_SITE_DATES, EP_SELECT_TANKDIP_DATE_PAGE, ApexPages.Severity.ERROR);
                        dSelectedDate = NULL;
                    }
                }
            } // End date check
            
            Map<String, String> missingDatesMap = new Map<String, String>();
            Integer queryRows = EP_Common_Util.getQueryLimit();
            String recType = Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.PLACEHOLDER).getRecordTypeId();
            // Retrieve missing tank dip dates
            // Fix for Defect 44600 - Start
            List<EP_Tank_Dip__c> missingTankDips = [SELECT EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c, EP_Tank__r.EP_Ship_To__r.EP_Ship_To_UTC_Timezone__c, EP_Tank_Dip_Entered_Today__c 
                                                        FROM EP_Tank_Dip__c 
                                                        WHERE EP_Tank__r.EP_Ship_To__c = :strSelectedShipToID AND
                                                            EP_Reading_Date_Time__c != NULL AND
                                                            EP_Tank_Dip_Entered_In_Last_14_Days__c = TRUE AND
                                                            RecordTypeId =: recType
                                                            ORDER BY EP_Reading_Date_Time__c ASC Limit :queryRows];
            // Fix for Defect 44600 - End
            
            tankDipDates = new List<tankDipDatesClass>();
            tankDipDatesClass tankDipDate;
            DateTime dtSiteDateTime;
            Double dblOffset;
            Boolean blnIsFirstLine = TRUE;
            String strKey;
            Double dblSiteOffset = EP_PortalLibClass_R1.retrieveSiteOffset(strSelectedShipToID);
            Double dblUserOffset = EP_PortalLibClass_R1.retrieveUserOffset(UserInfo.getUserID());
            // If there are any missing tank dips, then allow the user to select what tank dip to enter
            if (!missingTankDips.isEmpty()) {
                
                dblOffset = EP_PortalLibClass_R1.returnSiteRunningUserUTCOffset(missingTankDips[0].EP_Tank__r.EP_Ship_To__c);
                
                // Fix for Defect 29809 - Start
                DateTime dtNow = EP_PortalLibClass_R1.returnLocalDateTime(siteCurrentDateTime);
                Date dToday = EP_PortalLibClass_R1.returnLocalDate(dtNow);
                // Fix for Defect 29809 - End
                
                for (EP_Tank_Dip__c td : missingTankDips) {
                    // Fix for Defect 44600 - Start
                    dtSiteDateTime = td.EP_Tank_Dip_Date_Time_Ship_To_Time_Zone__c;
                    // Fix for Defect 44600 - End
                    
                    if (!missingDatesMap.containsKey(formatSelectedDate(dtSiteDateTime))) {
                        tankDipDate = populateTankDipClassItem(dtSiteDateTime, blnIsFirstLine);
                        blnIsFirstLine = FALSE;
                        // Fix for Defect 30169 - Start
                        tankDipDate.isToday = td.EP_Tank_Dip_Entered_Today__c;
                        // Fix for Defect 30169 - End 
                        missingDatesMap.put(formatSelectedDate(dtSiteDateTime), formatSelectedDate(dtSiteDateTime));
                    } // End date map check
                } // End for
                
                // Add today's date if missing
                
                if (!missingDatesMap.containsKey(formatSelectedDate(dtNow))) {
                    dtSiteDateTime = dtNow;
                    tankDipDate = populateTankDipClassItem(dtSiteDateTime, blnIsFirstLine);
                    // Fix for Defect 29809 - Start
                    tankDipDate.isToday = TRUE;
                    // Fix for Defect 29809 - End
                    blnIsFirstLine = FALSE;
                }
                
                // Check if only today's tank dip is missing - If only today's dip is missing then skip to the next page
                // The list should always have today's date. Therefore if list size = 1 the only missing date is today
                if (tankDipDates.size() > 1) {
                    ref = NULL;
                }
            } // End missing tank dip check
        } // End site Id parameter check
        
        return ref;
    }
    
    /*
    @Description: Populate tankDip date
    */
    private tankDipDatesClass populateTankDipClassItem(DateTime dDateTime, Boolean blnIsFirstLine) {
        
        Date dDate = EP_PortalLibClass_R1.returnLocalDate(dDateTime);
        
        tankDipDatesClass tankDipDate = new tankDipDatesClass();
              
        tankDipDate.dateDisplay = formatSelectedDate(dDateTime);
        tankDipDate.dateSelected = blnIsFirstLine;
        if ((dSelectedDate == NULL && blnIsFirstLine) || (formatSelectedDate(dSelectedDate) == formatSelectedDate(dDateTime))){
            tankDipDate.dateSelected = TRUE;
        }
        tankDipDate.dateText = dDateTime.format(DATE_FORMAT);
        tankDipDate.isToday = FALSE;
        tankDipDates.add(tankDipDate);
        
        return tankDipDate;
    }
   
    /*
    Return date in string format
    */ 
    private String formatSelectedDate(DateTime dDateTime) {
        String strDate = EP_Common_Constant.BLANK;
        
        if (dDateTime != NULL) {
            strDate = dDateTime.format();
            strDate = strDate.Left(10);
        }
        
        return strDate;
    }
    
    /*
    Calling back functionality of the page
    */
    public PageReference back() {
        // Fix for Defect 29809 - Start
        PageReference ref = new PageReference(EP_Common_Constant.TANKDIP_SITE_PAGE + strSelectedShipToID + EP_Common_Constant.PAGE_NUM_PARAM   + intPageNumber);
        // Fix for Defect 29809 - End
        try{
        // Fix for Defect 30169 - Start
            if (blnRedirectToAccountPage && strSelectedShipToID != NULL)
            {
                ref = new PageReference(EP_Common_Constant.SLASH + strSelectedShipToID);
            }
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }
        // Fix for Defect 30169 - End
        
        return ref;
    }
    
    /*
    Calling cancel functionality of the page
    */
    public PageReference cancel() {
        PageReference ref;
        try{
            ref = new PageReference(EP_Common_Constant.HOME_PAGE_URL);
        }
        catch (exception exp){
                string str= (exp.getMessage());  
          }
        return ref;
    }
    
    
    /*
     Inner class
    */
    public without sharing class tankDipDatesClass {
        public String dateDisplay {get;set;} // Variable used to format date according to the user locale
        public Boolean dateSelected {get;set;}
        public String dateText {get;set;} // Stores the date passed to the tank dip entry page in YYYYMMDD format
        // Fix for Defect 29809 - Start
        public Boolean isToday {get;set;} // Used to identify if date is today for site
        // Fix for Defect 29809 - End
    }
    
}