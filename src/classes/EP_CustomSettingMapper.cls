/* 
    @Author <Amit SIngh (Accenture)>
    @name <EP_CustomSettingMapper >
    @CreateDate <02/28/2017>
    @Description <This class is a mapper class for custom setting and custom metadata types>
    @Version <1.0>
    */
    public with sharing class EP_CustomSettingMapper {

         //Bulkification Implementation -- Start
        public List<EP_State_Transitions__c> listStateTransitions = new List<EP_State_Transitions__c>();

        public EP_CustomSettingMapper(){}
         //Bulkification Implementation -- End
        public List<EP_Address_Fields__mdt> queryAddressFieldCustomMetaDataTypes(string objName){
            EP_GeneralUtility.Log('Public','EP_CustomSettingMapper','queryAddressFieldCustomMetaDataTypes');
            return [Select EP_Field_API_Name__c
            , EP_Field_Type__c
            , EP_Object_Type__c
            , EP_Record_Type__c
            , EP_IsReferenced__c
            , EP_Field_Label__c
            From EP_Address_Fields__mdt 
            Where EP_Object_Type__c =:objName];
        }
        
    /**
    * @author       Accenture
    * @name         getStateTransitions
    * @date         03/29/2017
    * @description  This method is used to get state transitions from EP_State_Transitions__c custom setting
    * @param        NA
    * @return       string
    */
    public List<EP_State_Transitions__c> getStateTransitions(string entityType, string stateName, string event){
        EP_GeneralUtility.Log('Public','EP_CustomSettingMapper','getStateTransitions'); 
        System.debug('entityType##'+entityType+'##stateName##'+stateName+'##event##'+event);
         List<EP_State_Transitions__c> objStateTransitionsList =  new  List<EP_State_Transitions__c>();
         for(EP_State_Transitions__c transition:[select Transition_Class_Type__c,Transition_Priority__c from EP_State_Transitions__c 
                                                where EntityType__c =: entitytype 
                                                and Event__c=: event 
                                                and State_Name__c =: stateName 
                                                ORDER BY Transition_Priority__c DESC]){         
            objStateTransitionsList.add(transition);    
         }
         return objStateTransitionsList;
    } 

     //Bulkification Implementation -- Start
    /**
    * @author       Accenture
    * @name         getStateTransitions
    * @date         03/29/2017
    * @description  This method is used to get state transitions from EP_State_Transitions__c custom setting
    * @param        NA
    * @return       string
    */
    public List<EP_State_Transitions__c> getStateTransitionsAccount(string entityType, string stateName, string event){
        EP_GeneralUtility.Log('Public','EP_CustomSettingMapper','getStateTransitions'); 
        System.debug('entityType##'+entityType+'##stateName##'+stateName+'##event##'+event);
         List<EP_State_Transitions__c> objStateTransitionsList =  new  List<EP_State_Transitions__c>();
         if (listStateTransitions.isEmpty()) {
            listStateTransitions = EP_State_Transitions__c.getall().values();
         }

         for(EP_State_Transitions__c transition: listStateTransitions){         
            
            if(transition.EntityType__c == entitytype && transition.Event__c == event && transition.State_Name__c == stateName) {
                objStateTransitionsList.add(transition);   
            }
         }

         return objStateTransitionsList;
    } 
     //Bulkification Implementation -- End
    
    public List<EP_Address_Fields__mdt> getAddressDataByName(string name){
        List<EP_Address_Fields__mdt> addressFields = new List<EP_Address_Fields__mdt>();
        addressFields = [Select EP_Field_Label__c From EP_Address_Fields__mdt Where DeveloperName =: name];
        return addressFields;
    }
}