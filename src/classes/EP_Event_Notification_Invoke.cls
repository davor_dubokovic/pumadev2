/****************************************************************
* @author       Accenture                                       *
* @name         EP_Event_Notification_Invoke                    *
* @Created Date 28/12/2017                                      *
* @description  Invokable class to send notifications           *
****************************************************************/
global with sharing class EP_Event_Notification_Invoke {
    private static final String EPUMA = 'ePuma';
    private static final String METHOD_NAME = 'invokeNotificationToUser';
    private static final String CLASS_NAME = 'EP_Event_Notification_Invoke';
    static Id orgWideId;

/****************************************************************
* @author       Accenture                                       *
* @name         invokeNotificationToUser                        *
* @description  method to send notifications                    *
* @param        list<subscriptionInvokation>                    *
* @return       void                                            *
****************************************************************/
    @InvocableMethod(label='Send Notifications' description='Sends Emails to the subscribed users')
    global static void invokeNotificationToUser(list<subscriptionInvokation> Notificationobj){
        EP_GeneralUtility.Log('public','EP_Event_Notification_Invoke','invokeNotificationToUser');
        try{
            List<OrgWideEmailAddress> owa = [select id, Address from OrgWideEmailAddress 
                                                where Address=:label.EP_Notification_Framework_Org_Wide_Addresses];
            if(owa.isEmpty())
                orgWideId = owa[0].Id;
            List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
            EmailTemplate emailTemplateObj = EP_EmailTemplateMapper.getEmailTemplatesById(Notificationobj[0].usertemplateid);
            system.debug('Notificationobj[0] = '+Notificationobj[0]);
            if(!Notificationobj.isEmpty() && !String.isBlank(Notificationobj[0].userid))
                sendEmailToUsers(Notificationobj[0],emailTemplateObj,emailList);
            if(!Notificationobj.isEmpty() && !String.isBlank(Notificationobj[0].additionalUser))
                sendEmailToAdditionalUsers(processAdditionalUsers(Notificationobj[0].additionalUser),emailTemplateObj,emailList,Notificationobj[0]);
            if(!Notificationobj.isEmpty() && !String.isBlank(Notificationobj[0].contactid) && !String.isBlank(Notificationobj[0].whatId)
                         && !String.isBlank(Notificationobj[0].contacttemplateid))
                sendEmailToContacts(Notificationobj[0],emailList);
            if(!emailList.isEmpty())
                Messaging.SendEmail(emailList);
        }catch(Exception ex){
            EP_loggingService.loghandledException(ex,EPUMA, METHOD_NAME ,CLASS_NAME,apexPages.severity.ERROR);
            system.debug('Error = '+ ex.getMessage() + ' at line number = ' +ex.getLineNumber());
        }

    }

/****************************************************************
* @author       Accenture                                       *
* @name         invokeNotificationToUser                        *
* @description  method to split users based on semi colon       *
* @param        String                                          *
* @return       List<String>                                    *
****************************************************************/
    @TestVisible
    private static List<String> processAdditionalUsers(String additionalUsers){
        EP_GeneralUtility.Log('private','EP_Event_Notification_Invoke','processAdditionalUsers');
        return(additionalUsers.split(EP_Common_Constant.SEMICOLON));
    }

/****************************************************************
* @author       Accenture                                       *
* @name         sendEmailToContactUser                          *
* @description  method to send notifications to additional user *
* @param        List<String>,EmailTemplate,
                    List<Messaging.SingleEmailMessage>          *
* @return       void                                            *
****************************************************************/
    @TestVisible
    private static void sendEmailToAdditionalUsers(List<String> additionalUsers,EmailTemplate emailTemplateObj,
                                                                List<Messaging.SingleEmailMessage> emailList,subscriptionInvokation notificationObjVar){
        EP_GeneralUtility.Log('private','EP_Event_Notification_Invoke','sendEmailToAdditionalUsers');
        Messaging.SingleEmailMessage emailObj = new Messaging.SingleEmailMessage();
        emailObj.toAddresses = additionalUsers;
        if(emailTemplateObj.TemplateType.equalsIgnoreCase('Text'))
            emailObj.setPlainTextBody(emailTemplateObj.Body);
        if(emailTemplateObj.TemplateType.equalsIgnoreCase('HTML'))
            emailObj.setHtmlBody(emailTemplateObj.HtmlValue);
        emailObj.setSubject(emailTemplateObj.Subject);
        if(!String.isBlank(notificationObjVar.attachmentId))
            emailObj.setEntityAttachments(new List<String>{notificationObjVar.attachmentId});
        if(orgWideId!=null)
            emailObj.setOrgWideEmailAddressId(orgWideId);
        emailList.add(emailObj);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         sendEmailToContacts                             *
* @description  method to send notifications to Contacts        *
* @param        contactId,templateId,whatId,emailList           *
* @return       void                                            *
****************************************************************/
    @TestVisible
    private static void sendEmailToContacts(subscriptionInvokation notificationObjVar,List<Messaging.SingleEmailMessage> emailList){
        EP_GeneralUtility.Log('private','EP_Event_Notification_Invoke','sendEmailToContacts');
        Messaging.SingleEmailMessage emailObj = new Messaging.SingleEmailMessage();
        emailObj.setTargetObjectId(notificationObjVar.contactId);
        emailObj.setTemplateId(notificationObjVar.contacttemplateid);
        emailObj.setWhatId(notificationObjVar.whatId);
        emailObj.setSaveAsActivity(true);
        if(notificationObjVar.attachmentId!=null)
            emailObj.setEntityAttachments(new List<String>{notificationObjVar.attachmentId});
        if(orgWideId!=null)
            emailObj.setOrgWideEmailAddressId(orgWideId);
        emailList.add(emailObj);
    }
    
/****************************************************************
* @author       Accenture                                       *
* @name         sendEmailToUsers                                *
* @description  method to send notifications to users           *
* @param        userId,emailTemplateObj,emailList               *
* @return       void                                            *
****************************************************************/
    @TestVisible
    private static void sendEmailToUsers(subscriptionInvokation notificationObjVar,EmailTemplate emailTemplateObj,List<Messaging.SingleEmailMessage> emailList){
        EP_GeneralUtility.Log('private','EP_Event_Notification_Invoke','sendEmailToUsers');
        Messaging.SingleEmailMessage emailObj = new Messaging.SingleEmailMessage();
        system.debug('notificationObjVar.userId = '+notificationObjVar.userId);
        emailObj.setTargetObjectId(Id.valueOf(notificationObjVar.userId));
        if(emailTemplateObj.TemplateType.equalsIgnoreCase('Text'))
            emailObj.setPlainTextBody(emailTemplateObj.Body);
        if(emailTemplateObj.TemplateType.equalsIgnoreCase('HTML'))
            emailObj.setHtmlBody(emailTemplateObj.HtmlValue);
        emailObj.setSaveAsActivity(false);
        emailObj.setSubject(emailTemplateObj.Subject);
        if(notificationObjVar.attachmentId!=null)
            emailObj.setEntityAttachments(new List<String>{notificationObjVar.attachmentId});
        if(orgWideId!=null)
            emailObj.setOrgWideEmailAddressId(orgWideId);
        emailList.add(emailObj);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         subscriptionInvokation                          *
* @Created Date 28/12/2017                                      *
* @description  Invokable inner class to send notifications     *
****************************************************************/
    global class subscriptionInvokation{
        @InvocableVariable()
        global string attachmentId;

        @InvocableVariable()
        global string whatId;
        
        @InvocableVariable()
        global string contacttemplateid;
        
        @InvocableVariable()
        global string usertemplateid;
        
        @InvocableVariable()
        global string contactId;
    
        @InvocableVariable()
        global string userId;

        @InvocableVariable()
        global string additionalUser;
    }
}