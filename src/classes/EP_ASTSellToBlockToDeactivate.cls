/* 
@Author      Accenture
@name        EP_ASTSellToBlockToDeactivate
@CreateDate  11/29/2017
@Description Account(SellTo) State Transitions class for Block to Deactivate State - L4 #45361
@NovaSuite Fix -- comments added
@Version     1.0
*/
public with sharing class EP_ASTSellToBlockToDeactivate  extends EP_AccountStateTransition {
    /*
    *  @Author <Accenture>
    *  @Name constructor of class <EP_ASTVMIShipToBasicDataToBasicData>
    */
    public EP_ASTSellToBlockToDeactivate() {
        finalState = EP_AccountConstant.DEACTIVATE;
    }
/* 
@Author      Accenture
@name        isGuardCondition
**/    
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBlockToDeactivate','isGuardCondition');
        //No guard conditions for Active to Deactivate
        return true;
    }
}