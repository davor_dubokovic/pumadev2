/*   
     @Author <Jyotsna Tiwari>
     @name <EP_FE_ChangeRequestEndpointTest.cls>     
     @Description <This class is the test class for EP_FE_SubmitOrderEndpoint>   
     @Version <1.1> 
*/

@isTest
/*********************************************************************************************
     @Author <Soumya Raj>
     @name <EP_FE_BootstrapEndpoint>
     @CreateDate <12/04/2016>
     @Description <This class to get list of all the active sell to accounts and related shiptos associated with the current user  >  
     @Version <1.0>
    *********************************************************************************************/
Private class EP_FE_ChangeRequestEndpointTest {
    
    private static Account billTo =  EP_TestDataUtility.createBillToAccount(); // returns bill to account
    private static EP_Freight_Matrix__c freightMatrix =EP_TestDataUtility.createFreightMatrix();// returns freight matrix
    private static User sysAdmUser = EP_FE_TestDataUtility.getRunAsUser();
    
    /*********************************************************************************************
	*@Description : This is the Test method Create Order.                  
	*@Params      :                    
	*@Return      : Void                                                                             
	*********************************************************************************************/
    @testSetup 
    static void testCreatedata(){
  
		Account sellTo = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
		Database.insert (sellTo);
		sellTo.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
		Database.update (sellTo);
		sellTo.EP_Delivery_Type__c=EP_Common_Constant.DELIVERY;
		sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
		Database.update (sellTo);
		
	}
    
    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testRequestNullParam>
    @CreateDate <17/05/2016>
    @Description  <This Method tests Change Request when the Request is Null >  
    @Version <1.0>
    *********************************************************************************************/
    static testMethod void testRequestNullParam(){
        
        EP_FE_ChangeRequestSubmissionRequest objSubReq = null;  
        EP_FE_ChangeRequestSubmissionResponse response = new EP_FE_ChangeRequestSubmissionResponse(); 
        
        System.runAs(sysAdmUser){
            Test.startTest();
            response = EP_FE_ChangeRequestEndpoint.submitChangeRequest(objSubReq);
            Test.stopTest();
            System.assertequals(-1,response.status,'Missing Request');
                    
            RestRequest req = new RestRequest(); 
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/changeRequest/submit/' ;      
            RestContext.request = req; 
        }           
    }

    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testSubmitNullChangeRequest>
    @CreateDate <17/05/2016>
    @Description  <This Method tests Submit Order if the draftOrderId is Null  >  
    @Version <1.0>
    *********************************************************************************************/
    static testMethod void testSubmitNullChangeRequest(){
        
        EP_FE_ChangeRequestSubmissionRequest objSubReq = new EP_FE_ChangeRequestSubmissionRequest();   
        EP_FE_ChangeRequestSubmissionResponse response = new EP_FE_ChangeRequestSubmissionResponse();  


        System.runAs(sysAdmUser){
            Test.startTest();
            objSubReq.changeRequest = null;
            objSubReq.changeRequestLineItems = null;
            response = EP_FE_ChangeRequestEndpoint.submitChangeRequest(objSubReq);
            Test.stopTest();
            System.assertequals(-2,response.status,'Change Request is Null');

            RestRequest req = new RestRequest();  
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/order/submit/' ;      
            RestContext.request = req;  
        }          
    }

    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testSubmitNullChangeRequestLine>
    @CreateDate <17/05/2016>
    @Description  <This Method is to test Submit order when Order status is draft  >  
    @Version <1.0>
    *********************************************************************************************/
    static testMethod void testSubmitNullChangeRequestLine(){
        
        EP_FE_ChangeRequestSubmissionRequest objSubReq = new EP_FE_ChangeRequestSubmissionRequest();
        EP_FE_ChangeRequestSubmissionResponse response = new EP_FE_ChangeRequestSubmissionResponse(); 


        System.runAs(sysAdmUser){
            Test.startTest();
            
            Account sellTo = [Select id from Account limit 1];
            
            EP_ChangeRequest__c changeReq = EP_FE_TestDataUtility.createChangerequest(sellTo.Id);     
                          
            objSubReq.changeRequest = changeReq;        
            objSubReq.changeRequestLineItems = null;
            
            response = EP_FE_ChangeRequestEndpoint.submitChangeRequest(objSubReq);
            Test.stopTest();
            System.assertequals(-3,response.status,'Change Request Line Item is Null');

            RestRequest req = new RestRequest();
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/order/submit/' ;      
            RestContext.request = req;  
        }         
    }
       

    /*********************************************************************************************
    @Author <Jyotsna Tiwari>
    @name <testChangeRequest>
    @CreateDate <02/05/2016>
    @Description  <This Method is to test Submit Order if the status of Order is not 'Draft' >  
    @Version <1.0>
    *********************************************************************************************/
    static testMethod void testChangeRequest(){
        
        EP_FE_ChangeRequestSubmissionRequest objSubReq = new EP_FE_ChangeRequestSubmissionRequest();
        EP_FE_ChangeRequestSubmissionResponse response = new EP_FE_ChangeRequestSubmissionResponse();
        
        System.runAs(sysAdmUser){
            Test.startTest();
            Account sellTo = [Select id from Account limit 1];

            EP_ChangeRequest__c changeReq = EP_FE_TestDataUtility.createChangerequest(sellTo.Id); 
            system.debug('>>>> Change Request : ' + changeReq );
            EP_FE_OrderUtils ob = new EP_FE_OrderUtils();
            EP_FE_OrderUtils.getSellTo(sellTo.id);
            Account terminal = EP_TestDataUtility.createStockHoldingLocation();
            EP_FE_OrderUtils.getStockHoldingLocation(terminal.id);

            List <EP_ChangeRequestLine__c> changeRequestLine = [SELECT Id, EP_Change_Request__c, EP_New_Value__c,EP_Source_Field_Label__c,
                                            EP_Original_Value__c, EP_Source_Field_API_Name__c, EP_Request_Status__c 
                                            FROM EP_ChangeRequestLine__c 
                                            WHERE EP_Change_Request__c = : changeReq.Id limit 1 ];
                                            
            system.debug('>>>> Change Request Line : ' + changeRequestLine);
           
            objSubReq.changeRequest = changeReq;  
            system.debug('>>>> Change Request1 : ' + changeReq);      
            objSubReq.changeRequestLineItems = changeRequestLine;
            system.debug('>>>> Change Request Line2 : ' + changeRequestLine); 
            
            response = EP_FE_ChangeRequestEndpoint.submitChangeRequest(objSubReq);

            Test.stopTest();
            //System.assertequals(0,response.status,'Change Request Created');

            RestRequest req = new RestRequest();                 
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/order/submit/' ;      
            RestContext.request = req; 
        }           
    }      
}