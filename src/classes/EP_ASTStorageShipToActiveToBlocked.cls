/*
*  @Author <Accenture>
*  @Name <EP_ASTStorageShipToActiveToBlocked>
*  @CreateDate <15/3/2017>
*  @Description <Handles Storage Ship To Account status change from 05-Active to 06-Blocked>
*  @Version <1.0>
*/
public class EP_ASTStorageShipToActiveToBlocked extends EP_AccountStateTransition {

    public EP_ASTStorageShipToActiveToBlocked () {
        finalState = EP_AccountConstant.BLOCKED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToBlocked', 'isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTStorageShipToActiveToBlocked',' isGuardCondition');        
        return true;
    }
}