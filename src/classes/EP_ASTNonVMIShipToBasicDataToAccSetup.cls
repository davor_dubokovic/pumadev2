/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToBasicDataToAccSetup>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 02-Basic Data Setup to 04-Account Set-up>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToBasicDataToAccSetup extends EP_AccountStateTransition {
    /**
    * @author <Accenture>
    * @name constructor of class EP_ASTNonVMIShipToBasicDataToAccSetup
    */
    public EP_ASTNonVMIShipToBasicDataToAccSetup () {
        finalState = EP_AccountConstant.ACCOUNTSETUP;
    }
    /**
    * @author <Accenture>
    * @name isTransitionPossible
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToAccSetup','isTransitionPossible');
        return super.isTransitionPossible();
    }
    /**
    * @author <Accenture>
    * @name isRegisteredForEvent
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToAccSetup','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
    /**
    * @author <Accenture>
    * @name isGuardCondition
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToAccSetup','isGuardCondition');
        EP_AccountService service =  new EP_AccountService(this.account);
        //L4_45352_Start
        /*
        system.debug('## 2. Checking all review action created for Ship to is completed .....');
        if(!service.isCompletedAllReviewActions()||!service.isCompletedReviewActionsOnTanks()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Review_Action_Pending_on_ShipTo_Error_Message);
            return false;
        }
        system.debug('## 3. Checking if primary supply option is attached'); 
        if(!service.isPrimarySupplyOptionAttached()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(System.label.EP_One_Primary_Location_Reqd); 
            return false;
        }        
        
        // #sprintRouteWork starts 
        if(!service.isRouteAllocationAvailable()){
            accountEvent.isError = true;
            accountEvent.setEventMessage(system.label.EP_Route_Not_Allocated);
            return false;
        }
        //#sprintRouteWork ends 
        */
        //L4_45352_End
        return true;
    }
     /**
    * @author <Accenture>
    * @name doOnExit
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBasicDataToAccSetup','doOnExit');

    }
}