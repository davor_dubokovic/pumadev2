/* 
    @ Author <Ashok Arora> 
    @name <EP_ChangeRequestTriggerHelper> 
    @CreateDate <22/06/2016> 
    @Description <THIS CLASS HANDLES REQUEST FROM CHANGE REQUEST TRIGGER HANDLER> 
    @Version <1.0> 
*/
public without sharing class EP_ChangeRequestTriggerHelper {
    private static final String OBJ_ID = 'id';
    private static final String UPDATE_OPERATION = 'UPDATE';
    private static final String RECORDTYPEID_FIELD = 'RecordTypeId';
    private static final String COMMIT_COMPLETED_REQUEST_METHOD = 'commitCompletedRequest';
    private static final String CLASSNAME = 'EP_ChangeRequestTriggerHelper';
    private static final String UPDATE_SOBJECT_METHOD = 'updateSObject';
    private static final String MAP_COUNTRY_REGION_METHOD = 'mapDeliveryCountryAndRegion';
    private static final String SHIPTO = 'Ship';
    private static final String CHANGE_REQUEST_OBJ = 'EP_Change_Request__c';
    private static final String CHANGEProduct_REQ_RT_DEV = 'EP_Product_List_Change_Request';
    private static final String CREDIT_REVIEW = 'Credit Review';
    private static final String ADD = 'Add';
    private static final String STREET = 'Street';
    private static final String CITY = 'City';
    private static final String STATE = 'State';
    private static final String COUNTRY = 'Country';
    private static final String POSTCODE = 'PostCode';
    private static final String LATITUDE_S= 'latitude__s';
    private static final String LONGITUDE_S= 'longitude__s';
    private static final String C_LETTER = 'c';
    
    private static Map<String,EP_Address_Fields__mdt> mTypeField = new Map<String,EP_Address_Fields__mdt>();
    
    /**
    * @author <Ashok Arora> 
    * @name <commitCompletedRequest> 
    * @date <22/06/2016> 
    * @description <THIS METHOD COMMITS REQUEST CHANGES TO SOBJECT WHEN REQUEST IS COMPLETED> 
    * @version <1.0> 
    * @param Map<Id,EP_ChangeRequest__c>,Map<Id,EP_ChangeRequest__c> 
    * @return void
    */
    public static void commitCompletedRequest(Map<Id,EP_ChangeRequest__c> mOldRequests
    ,Map<Id,EP_ChangeRequest__c> mNewRequests){
        
        Integer limitQueryRows;
        limitQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
        Set<Id> setOfCompletedRequestIDs = new Set<Id>();
        Set<String> setOfObjectName = new Set<String>();
        Set<String> setOfRecordType = new Set<String>();
        List<EP_ChangeRequestLine__c> listOfChangeLineItems;
        List<sObject> listOfUpdatedRecords = new List<sObject>();
        Map<Id,List<EP_ChangeRequestLine__c>> mapOfChangeLineItems = new Map<Id,List<EP_ChangeRequestLine__c>>();
        Map<String, Schema.Sobjecttype> mapOfGlobalSObjectType;
        Map<String,Map<String, Schema.SObjectField>> mapOfSObjectField = new Map<String,Map<String, Schema.SObjectField>>();
        Map<String,Id> mapOfRecordTypeID = new Map<String,Id>();
        Map<String,String> mapOfSObjectParentField = new Map<String,String>();
        Id productChangeRequestRecordTypeID = EP_Common_Util.getRecordTypeIDForGivenSObjectAndName(EP_Common_Constant.CHANGE_REQUEST_OBJ, 
        EP_Common_Constant.CHANGEProduct_REQ_RT_DEV);
        
        try{
            //system.debug('----productChangeRequestRecordTypeID-----'+productChangeRequestRecordTypeID);
            mapOfGlobalSObjectType = Schema.getGlobalDescribe();
            for(Id rqstId : mNewRequests.keySet()){
                if(mNewRequests.get(rqstId).EP_Request_Status__c != mOldRequests.get(rqstId).EP_Request_Status__c
                        && EP_Common_Constant.Completed.equalsIgnoreCase(mNewRequests.get(rqstId).EP_Request_Status__c)){
                    setOfCompletedRequestIDs.add(rqstId);
                    if(!productChangeRequestRecordTypeID.equals(mNewRequests.get(rqstId).RecordTypeID)){
                        if(mNewRequests.get(rqstId).EP_IS_Address_CR__c){
                            EP_Common_Util.isAddressCR = mNewRequests.get(rqstId).EP_IS_Address_CR__c;
                        }
                        setOfObjectName.add(mNewRequests.get(rqstId).EP_Object_Type__c);
                        if(!mapOfSObjectField.containsKey(mNewRequests.get(rqstId).EP_Object_Type__c)){
                            
                            mapOfSObjectField.put(mNewRequests.get(rqstId).EP_Object_Type__c,mapOfGlobalSObjectType.get(mNewRequests.get(rqstId).EP_Object_Type__c)
                            .getDescribe().fields.getMap());
                        }
                        if(String.isNotblank(mNewRequests.get(rqstId).EP_Record_Type__c)
                                ){
                            setOfRecordType.add(mNewRequests.get(rqstId).EP_Record_Type__c);
                            /*mapOfRecordTypeID.put(mNewRequests.get(rqstId).EP_Record_Type__c
                                                ,EP_Common_Util.fetchRecordTypeId(mNewRequests.get(rqstId).EP_Object_Type__c, 
                                                                                    mNewRequests.get(rqstId).EP_Record_Type__c));*/
                        }  
                    }
                }
            }
            if(!setOfRecordType.isEmpty()){
                limitQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                for(RecordType recType : [Select id
                ,DeveloperName
                ,Name 
                From Recordtype 
                Where SObjectType IN:setOfObjectName
                And DeveloperName IN :setOfRecordType
                Limit : limitQueryRows]){
                    mapOfRecordTypeID.put(recType.developerName
                    ,recType.Id);
                }
            }
            //system.debug('=====setOfCompletedRequestIDs======='+setOfCompletedRequestIDs);
            if(!setOfCompletedRequestIDs.isEmpty()){
                limitQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                for(EP_ChangeRequestLine__c chngLineItem : [Select Id
                ,RecordTypeID
                ,EP_Source_Field_API_Name__c
                ,EP_New_Value__c
                ,EP_Change_Request__c
                ,EP_Request_Type__c
                ,EP_Product_Name__c
                ,EP_Action__r.EP_Action_Name__c
                ,EP_Reference_Value__c
                ,EP_Original_Reference_Value__c
                ,EP_Street__c
                ,EP_List_Price__c
                ,EP_City__c
                ,EP_State__c
                ,EP_Country__c
                ,EP_PostalCode__c
                ,EP_Position__Latitude__s
                ,EP_Position__Longitude__s
                ,currencyISOCode
                From EP_ChangeRequestLine__c
                Where EP_Change_Request__c IN :setOfCompletedRequestIDs
                //And EP_Request_Status__c = :EP_Common_Constant.CRLITEM_STATUS_APPROVED L4#60248
                //And EP_Do_not_commit_changes__c = false L4#60248
                limit :limitQueryRows]){
                    //system.debug('=====chngLineItem======='+chngLineItem);
                    //if(!CREDIT_REVIEW.equalsIgnoreCase(chngLineItem.EP_Action__r.EP_Action_Name__c)){
                    if(mapOfChangeLineItems.containsKey(chngLineItem.EP_Change_Request__c) ){
                        mapOfChangeLineItems.get(chngLineItem.EP_Change_Request__c).add(chngLineItem);
                    }
                    else{
                        listOfChangeLineItems = new List<EP_ChangeRequestLine__c>();
                        listOfChangeLineItems.add(chngLineItem);
                        mapOfChangeLineItems.put(chngLineItem.EP_Change_Request__c,listOfChangeLineItems);
                    }
                    
                }
                
                for(EP_Object_Parent_Id_Field_Mapping__mdt fldmapping :[Select Id
                , EP_Object_API_Name__c
                , EP_Parent_Field_API_Name__c
                From EP_Object_Parent_Id_Field_Mapping__mdt 
                Where EP_Object_API_Name__c IN: setOfObjectName 
                limit :limitQueryRows]){
                    mapOfSObjectParentField.put(fldmapping.EP_Object_API_Name__c
                    ,fldmapping.EP_Parent_Field_API_Name__c);
                }
                //system.debug('=====mapOfSObjectParentField======='+mapOfSObjectParentField);
                //system.debug('=====EP_Common_Util.isAddressCR======='+EP_Common_Util.isAddressCR);
                if(EP_Common_Util.isAddressCR){
                    //System.debug(setOfObjectName);
                    //System.debug(setOfRecordType);
                    limitQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                    for(EP_Address_Fields__mdt addressField : [Select EP_Field_API_Name__c
                    , EP_Field_Type__c
                    , EP_Object_Type__c
                    , EP_Record_Type__c
                    , EP_IsReferenced__c
                    From EP_Address_Fields__mdt 
                    Where EP_Object_Type__c IN :setOfObjectName 
                    Limit : limitQueryRows]){
                        if(addressField.EP_Record_Type__c.contains(EP_Common_Constant.COMMA)){
                            for(String recType : addressField.EP_Record_Type__c.split(EP_Common_Constant.COMMA)){
                                mTypeField.put((addressField.EP_Object_Type__c
                                + recType
                                + addressField.EP_Field_Type__c
                                ).toUpperCase()
                                ,addressField);
                            }
                        }
                        else{
                            mTypeField.put((addressField.EP_Object_Type__c
                            + addressField.EP_Record_Type__c
                            + addressField.EP_Field_Type__c
                            ).toUpperCase()
                            ,addressField);
                        }
                        
                    }
                }
                
            }
            List<PricebookEntry> pricebookEntriesToUpdate = new List<PricebookEntry>();
            //system.debug('----mapOfChangeLineItems-----'+mapOfChangeLineItems);
            //system.debug('----mTypeField-----'+mTypeField);
            if(!mapOfChangeLineItems.isEmpty()){
                Set<Id> pricebookIds = new Set<Id>();
                
                List<PricebookEntry> temppbeTOupdate = new List<PricebookEntry>();
                for(Id rqstId : mapOfChangeLineItems.keySet()){
                    if(productChangeRequestRecordTypeID.equals(mNewRequests.get(rqstId).RecordTypeId) && 
                            mNewRequests.get(rqstId).EP_Product_List__c != null){            
                        pricebookIds.add(mNewRequests.get(rqstId).EP_Product_List__c);
                    }
                }
                //system.debug('----pricebookIds-----'+pricebookIds);
                Map<Id, Map<Id, PriceBookEntry>> pbeMap = new Map<Id, Map<Id, PriceBookEntry>>();
                if(!pricebookIds.isEmpty()){
                    pbeMap = queryPriceBookEntry(pricebookIds);
                }
                //system.debug('----pbeMap-----'+pbeMap);
                for(Id rqstId : mapOfChangeLineItems.keySet()){
                    /*listOfUpdatedRecords.add(EP_ChangeRequestService.commitChangeRequest(mNewRequests.get(rqstId)
                                                                ,mapOfChangeLineItems.get(rqstId)));*/
                    //system.debug(productChangeRequestRecordTypeID+'====In for==='+mNewRequests.get(rqstId).RecordTypeId);
                    if(productChangeRequestRecordTypeID.equals(mNewRequests.get(rqstId).RecordTypeId)){
                        //system.debug('====In if1===');
                        if(pbeMap.ContainsKey(mNewRequests.get(rqstId).EP_Product_List__c)){
                            //system.debug('====In if1===');
                            temppbeToUpdate = findProductToUpdate(mNewRequests.get(rqstId),
                            mapOfChangeLineItems.get(rqstId), 
                            pbeMap.get(mNewRequests.get(rqstId).EP_Product_List__c));
                            //system.debug('====temppbeToUpdate======'+temppbeToUpdate);
                            if(!temppbeToUpdate.isEmpty()){
                                pricebookEntriesToUpdate.addAll(temppbeToUpdate);
                            }
                        }
                        
                    }
                    else{
                        listOfUpdatedRecords.add(updateSObject(mNewRequests.get(rqstId)
                        ,mapOfChangeLineItems.get(rqstId)
                        ,mapOfRecordTypeID.containsKey(mNewRequests.get(rqstId).EP_Record_Type__c)?mapOfRecordTypeID.get(mNewRequests.get(rqstId).EP_Record_Type__c)
                        : null
                        ,mapOfSObjectField.get(mNewRequests.get(rqstId).EP_Object_Type__c)
                        ,mapOfSObjectParentField.containsKey(mNewRequests.get(rqstId).EP_Object_Type__c)?mapOfSObjectParentField.get(mNewRequests.get(rqstId).EP_Object_Type__c)
                        : null
                        ,mapOfGlobalSObjectType.get(mNewRequests.get(rqstId).EP_Object_Type__c)));
                    } 
                }
                
            }
            EP_IntegrationUtil.isCallout = false;
            //system.debug('listOfUpdatedRecords==='+listOfUpdatedRecords);
            if(!listOfUpdatedRecords.isEmpty()){
                
                DataBase.update(listOfUpdatedRecords);
            }
            //system.debug('pricebookEntriesToUpdate==='+pricebookEntriesToUpdate);
            if(!pricebookEntriesToUpdate.isEmpty()){
                database.upsert(pricebookEntriesToUpdate);
            }
            
        }
        catch(Exception handledException){
            //system.debug('handledException==='+handledException);
            EP_LoggingService.logHandledException(handledException 
            ,EP_Common_Constant.EPUMA
            ,COMMIT_COMPLETED_REQUEST_METHOD 
            ,CLASSNAME
            ,ApexPages.Severity.ERROR);
        }
    }
    
    /**
    * @author <Ashok Arora> 
    * @name <queryPriceBookEntry> 
    * @date <22/06/2016> 
    * @description <This method returns the map of Pricebook and its priebookEntries> 
    * @version <1.0> 
    * @param Set<Id> 
    * @return Map<Id, Map<Id, PriceBookEntry>> 
    */
    private static Map<Id, Map<Id, PriceBookEntry>> queryPriceBookEntry(Set<Id> pricebookIds){
        Map<Id, Map<Id, PriceBookEntry>> mapOfPriceBooks = new Map<Id, Map<Id, PriceBookEntry>>();
        Map<Id, PriceBookEntry> mapOfPriceBookEntries = new Map<Id, PriceBookEntry>();
        for(PricebookEntry pbe : [SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive 
                                    FROM PricebookEntry
                                    WHERE Pricebook2Id in: pricebookIds Limit :EP_Common_Util.getQueryLimit()]){
            if(!mapOfPriceBooks.containsKey(pbe.Pricebook2Id)){
                mapOfPriceBooks.put(pbe.Pricebook2Id, mapOfPriceBookEntries ); //new Map<Id, PriceBookEntry>());
            }
            mapOfPriceBooks.get(pbe.Pricebook2Id).put(pbe.Product2Id, pbe);
        }
        return mapOfPriceBooks;
    }
    
    /**
    * @author <Ashok Arora> 
    * @name <findProductToUpdate> 
    * @date <22/06/2016> 
    * @description <This method returns the list of pricebook entry which needs to update from CR> 
    * @version <1.0> 
    * @param EP_ChangeRequest__c, List<EP_ChangeRequestLine__c>, Map<Id, PriceBookEntry> 
    * @return List<PricebookEntry> 
    */
    private static List<PricebookEntry> findProductToUpdate(EP_ChangeRequest__c requestHeader,
    List<EP_ChangeRequestLine__c> listOfRequestLines,
    Map<Id, PriceBookEntry> pbeMap
    ){
        List<PricebookEntry> pbeTOupdate = new List<PricebookEntry>();
        
        PricebookEntry pbe;
        for(EP_ChangeRequestLine__c crl : listOfRequestLines){
            if(crl.EP_Product_Name__c != null){
                
                if(pbeMap.containsKey(crl.EP_Product_Name__c)){
                    pbe = pbeMap.get(crl.EP_Product_Name__c);
                    pbe.isActive = ADD.equalsIgnoreCase(crl.EP_Request_Type__c) ? true: false;
                    pbe.UseStandardPrice = false;
                    pbe.UnitPrice = crl.EP_List_Price__c;
                }else{
                    pbe = NEW PriceBookEntry();
                    pbe.Product2ID = crl.EP_Product_Name__c;
                    pbe.PriceBook2Id = requestHeader.EP_Product_List__c ;
                    pbe.isActive = ADD.equalsIgnoreCase(crl.EP_Request_Type__c) ? true: false;
                    pbe.UseStandardPrice = false;
                    pbe.UnitPrice = crl.EP_List_Price__c;
                    pbe.currencyISOCode = crl.currencyISOCode;
                    
                }
                pbe.EP_Is_Sell_To_Assigned__c = true;
                pbeTOupdate.add(pbe);
            }
        }
        
        return pbeTOupdate;
    }
    
    /**
    * @author <Ashok Arora> 
    * @name <updateSObject> 
    * @date <22/06/2016> 
    * @description <THIS METHOD UPDATE SOBJECT VALUES
    * @version <1.0> 
    * @param EP_ChangeRequest__c,List<EP_ChangeRequestLine__c>,Id,Map<String, Schema.SObjectField>,String,Schema.sObjectType
    * @return sObject
    */
    public static sObject updateSObject(EP_ChangeRequest__c requestHeader
    ,List<EP_ChangeRequestLine__c> listOfRequestLines
    ,Id rtype
    ,Map<String, Schema.SObjectField> fieldMap
    ,String parentFieldName
    ,Schema.sObjectType sObjType){
        
        String addressKeyPrefix = requestHeader.EP_Object_Type__c + requestHeader.EP_Record_Type__c;
        
        sObject sObjRec = sObjType.newSObject();
        try{
            if(UPDATE_OPERATION.equalsIgnoreCase(requestHeader.EP_Operation_Type__c)){
                sObjRec.put(OBJ_ID, requestHeader.EP_Object_ID__c);
            }else{
                If(!String.isBlank(requestHeader.EP_Operation_Type__c) && 
                !String.isBlank(requestHeader.EP_Object_Type__c) && 
                !String.isBlank(requestHeader.EP_Record_Type__c)){   
                    
                    if(rtype != NULL){
                        sObjRec.put(RECORDTYPEID_FIELD, rtype);
                    }
                }
                
                //String parentFieldName = EP_ChangeRequestService.getParentIdFieldAPIName(requestHeader.EP_Object_Type__c);
                if(string.isNotBlank(parentFieldName)){
                    sObjRec.put(parentFieldName, requestHeader.EP_Object_ID__c);
                }
            }
            
            for (EP_ChangeRequestLine__c line : listOfRequestLines){
                if(String.isBlank(line.EP_Street__c)
                        && String.isBlank(line.EP_State__c)
                        && String.isBlank(line.EP_City__c)
                        && String.isBlank(line.EP_Country__c)
                        && String.isBlank(line.EP_PostalCode__c)
                        && line.EP_Position__Latitude__s == null
                        && line.EP_Position__Longitude__s == null
                        )
                {
                    if(String.isNotBlank(line.EP_Reference_Value__c)){
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec, line.EP_Source_Field_API_Name__c, line.EP_Reference_Value__c, fieldMap);
                    }
                    else{
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec, line.EP_Source_Field_API_Name__c, line.EP_New_Value__c, fieldMap);
                    }  
                }
                else{
                    EP_Common_Util.isValidAddress = true;
                    system.debug('----mTypeField-----'+mTypeField);
                    
                    if(mTypeField.containsKey((addressKeyPrefix + STREET).toUpperCase())){
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                        , mTypeField.get((addressKeyPrefix + STREET).toUpperCase()).EP_Field_API_Name__c
                        , line.EP_Street__c
                        , fieldMap);
                    }                   
                    
                    if(mTypeField.containsKey((addressKeyPrefix + CITY).toUpperCase())){
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                        , mTypeField.get((addressKeyPrefix + CITY).toUpperCase()).EP_Field_API_Name__c
                        , line.EP_City__c
                        , fieldMap);
                    }
                    
                    if(mTypeField.containsKey((addressKeyPrefix + STATE).toUpperCase())){
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                        , mTypeField.get((addressKeyPrefix  + STATE).toUpperCase()).EP_Field_API_Name__c
                        , line.EP_State__c
                        , fieldMap);
                    }
                    if(mTypeField.containsKey((addressKeyPrefix + COUNTRY).toUpperCase())){
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                        , mTypeField.get((addressKeyPrefix  + COUNTRY).toUpperCase()).EP_Field_API_Name__c
                        , line.EP_Country__c
                        , fieldMap);
                    }
                    
                    if(mTypeField.containsKey((addressKeyPrefix + POSTCODE).toUpperCase())){
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                        , mTypeField.get((addressKeyPrefix  + POSTCODE).toUpperCase()).EP_Field_API_Name__c
                        , line.EP_PostalCode__c
                        , fieldMap);
                    }
                    
                    if(mTypeField.containsKey((addressKeyPrefix + EP_Common_Constant.LATLANG_STR).toUpperCase())){
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                        , mTypeField.get((addressKeyPrefix + EP_Common_Constant.LATLANG_STR).toUpperCase()).EP_Field_API_Name__c.removeEnd(C_LETTER) + LATITUDE_S
                        , String.valueOf(line.EP_Position__Latitude__s)
                        , fieldMap);
                        EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                        , mTypeField.get((addressKeyPrefix  + EP_Common_Constant.LATLANG_STR).toUpperCase()).EP_Field_API_Name__c.removeEnd(C_LETTER) + LONGITUDE_S
                        , String.valueOf(line.EP_Position__Longitude__s)
                        , fieldMap);
                    }
                    else{ 
                        if(mTypeField.containsKey((addressKeyPrefix + EP_Common_Constant.LATITUDE_STR).toUpperCase())){
                            EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                            , mTypeField.get((addressKeyPrefix + EP_Common_Constant.LATITUDE_STR).toUpperCase()).EP_Field_API_Name__c
                            , String.valueOf(line.EP_Position__Latitude__s)
                            , fieldMap);
                        }
                        if(mTypeField.containsKey((addressKeyPrefix + EP_Common_Constant.LONGITUDE_STR).toUpperCase())){
                            EP_ChangeRequestService.addFieldValueInSObject(sObjRec
                            , mTypeField.get((addressKeyPrefix  + EP_Common_Constant.LONGITUDE_STR).toUpperCase()).EP_Field_API_Name__c
                            , String.valueOf(line.EP_Position__Longitude__s)
                            , fieldMap);
                        }
                        
                    }
                }
            } 
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException 
            ,EP_Common_Constant.EPUMA
            ,UPDATE_SOBJECT_METHOD 
            ,CLASSNAME
            ,ApexPages.Severity.ERROR);
        }                  
        return sObjRec;
    }
    
    /**
    * @author <Ashok Arora> 
    * @name <mapDeliveryCountryAndRegion> 
    * @date <22/06/2016> 
    * @description <THIS METHOD MAPS DELIVERY COUNTRY AND REGION FROM SELL TO ON CHANGE REQUEST> 
    * @version <1.0> 
    * @param List<EP_ChangeRequest__c> 
    * @return void
    */
    public static void mapDeliveryCountryAndRegion(List<EP_ChangeRequest__c> listOfNewChangeRequests){
        Integer limitQueryRows;
        
        Map<Id,EP_ChangeRequest__c> mapOfChangeRequests = new Map<Id,EP_ChangeRequest__c>();
        Set<Id> setOfTankIDs = new Set<Id>();
        Set<Id> setOfBankIDs = new Set<Id>();
        Set<Id> setOfAccountIDs = new Set<Id>();
        //Set<String> setOfRecordType = new Set<String>();
        try{
            for(EP_ChangeRequest__c chngRqst : listOfNewChangeRequests){
                if(chngRqst.EP_Account__c != null){
                    setOfAccountIDs.add(chngRqst.EP_Account__c);
                    mapOfChangeRequests.put(chngRqst.EP_Account__c,chngRqst);
                }
                if(chngRqst.EP_Bank_Account__c != null){
                    setOfBankIDs.add(chngRqst.EP_Bank_Account__c);
                    mapOfChangeRequests.put(chngRqst.EP_Bank_Account__c,chngRqst);
                }
                if(chngRqst.EP_Tank__c != null){
                    setOfTankIDs.add(chngRqst.EP_Tank__c);
                    mapOfChangeRequests.put(chngRqst.EP_Tank__c,chngRqst);
                }
                
            }
            if(!setOfTankIDs.isEmpty()){
                limitQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                for(EP_Tank__c tank : [Select Id
                ,EP_Ship_To__r.Parent.EP_Delivery_Pickup_Country__r.EP_Country_Code__c
                ,EP_Ship_To__r.Parent.EP_Cntry_Region__r.Name
                From EP_Tank__c 
                Where Id IN:setOfTankIDs
                limit :limitQueryRows 
                ]){
                    if(String.isNotBlank(tank.EP_Ship_To__r.Parent.EP_Delivery_Pickup_Country__r.EP_Country_Code__c)){
                        mapOfChangeRequests.get(tank.Id).EP_Delivery_Country__c = tank.EP_Ship_To__r.Parent.EP_Delivery_Pickup_Country__r.EP_Country_Code__c;
                    }
                    
                    if(String.isNotBlank(tank.EP_Ship_To__r.Parent.EP_Cntry_Region__r.Name)){
                        mapOfChangeRequests.get(tank.Id).EP_Region__c = tank.EP_Ship_To__r.Parent.EP_Cntry_Region__r.Name;
                    }
                    
                }
            }
            if(!setOfBankIDs.isEmpty()){
                limitQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                for(EP_Bank_Account__c bank : [Select Id
                ,EP_Account__r.EP_Delivery_Pickup_Country__r.EP_Country_Code__c
                ,EP_Account__r.EP_Cntry_Region__r.Name
                From EP_Bank_Account__c 
                Where Id IN:setOfBankIDs  
                limit :limitQueryRows]){
                    if(String.isNotBlank(bank.EP_Account__r.EP_Delivery_Pickup_Country__r.EP_Country_Code__c)){
                        mapOfChangeRequests.get(bank.Id).EP_Delivery_Country__c = bank.EP_Account__r.EP_Delivery_Pickup_Country__r.EP_Country_Code__c;
                    }
                    
                    if(String.isNotBlank(bank.EP_Account__r.EP_Cntry_Region__r.Name)){
                        mapOfChangeRequests.get(bank.Id).EP_Region__c = bank.EP_Account__r.EP_Cntry_Region__r.Name;
                    }
                    
                }
            }
            if(!setOfAccountIDs.isEmpty()){
                limitQueryRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
                for(Account account : [Select Id
                ,EP_Delivery_Pickup_Country__r.EP_Country_Code__c
                ,EP_Cntry_Region__r.Name
                ,parent.EP_Delivery_Pickup_Country__r.EP_Country_Code__c
                ,parent.EP_Cntry_Region__r.Name
                ,RecordType.Name
                From Account 
                Where Id IN:setOfAccountIDs 
                limit :limitQueryRows]){               
                    if(account.RecordType.Name.containsIgnoreCase(SHIPTO)){
                        if(String.isNotBlank(account.parent.EP_Delivery_Pickup_Country__r.EP_Country_Code__c)){
                            mapOfChangeRequests.get(account.Id).EP_Delivery_Country__c = account.parent.EP_Delivery_Pickup_Country__r.EP_Country_Code__c;
                        }
                        if(String.isNotBlank(account.parent.EP_Cntry_Region__r.Name)){
                            mapOfChangeRequests.get(account.Id).EP_Region__c = account.parent.EP_Cntry_Region__r.Name;
                        }
                        
                    }
                    else{
                        if(String.isNotBlank(account.EP_Delivery_Pickup_Country__r.EP_Country_Code__c)){
                            mapOfChangeRequests.get(account.Id).EP_Delivery_Country__c = account.EP_Delivery_Pickup_Country__r.EP_Country_Code__c;
                        }
                        if(String.isNotBlank(account.EP_Cntry_Region__r.Name)){
                            mapOfChangeRequests.get(account.Id).EP_Region__c = account.EP_Cntry_Region__r.Name;
                        }
                    }
                }
            }
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException 
            ,EP_Common_Constant.EPUMA
            ,MAP_COUNTRY_REGION_METHOD 
            ,CLASSNAME
            ,ApexPages.Severity.ERROR);
        }
    }
}