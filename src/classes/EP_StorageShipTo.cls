/* 
@Author      Accenture
@name        EP_StorageShipTo
@CreateDate  03/09/2017
@Description This is strategy class for storage shipTo and executes logic specific to storage shipTo Account
@Version     1.1
*/
public class EP_StorageShipTo extends EP_AccountType{
	
	EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
	EP_AccountMapper accountMapper = new EP_AccountMapper();
    EP_CountryMapper countryMapper = new EP_CountryMapper(); 
  
	
	/**
    * @author       Accenture
    * @name         doAfterInsertHandle
    * @date         03/09/2017
    * @description  This method handles logic which need to execute on trigger's after insert event 
    * @param        Account
    * @return       NA
    */
    public override void doAfterInsertHandle(Account storageShipToAccount){
        EP_GeneralUtility.Log('Public','EP_StorageShipTo','doAfterInsertHandle');
        EP_AccountShareUtility.insertManualSharingRecords(storageShipToAccount.Id);      
        
    }
	
	/**
    * @author       Accenture
    * @name         getSupplyLocationList
    * @date         03/09/2017
    * @description  This method is returns supply locations for a storage ship to 
    * @param        Id
    * @return       List<EP_Stock_Holding_Location__c>
    */
	public override List<EP_Stock_Holding_Location__c> getSupplyLocationList(Id storageShipToAccountId) {
        EP_GeneralUtility.Log('Public','EP_StorageShipTo','getSupplyLocationList');
        return stockHoldingLocationMapper.getStockHoldingLocationByIds(new set<Id> {storageShipToAccountId});
	}
	
	/**
    * @author       Accenture
    * @name         sendCustomerCreateRequest
    * @date         02/27/2017
    * @description  This method send create Request to LS 
    * @param        Account
    * @return       NA
    */
    public override void sendCustomerCreateRequest(Account storageShipToAccount){
        EP_GeneralUtility.Log('Public','EP_StorageShipTo','sendCustomerCreateRequest'); 
		if(isParentActive(storageShipToAccount)){
			sendShipToSyncRequestToLOMO(storageShipToAccount);   
			setNavWinDmsFlag(storageShipToAccount);     
		}
    }
    
    /**
    * @author       Accenture
    * @name         isParentActive
    * @date         02/27/2017
    * @description  This method returns true if ship To parent is in active status
    * @param        Account
    * @return       NA
    */
    @TestVisible
    private boolean isParentActive(Account storageshipToAccount){
        EP_GeneralUtility.Log('Private','EP_ShipTo','isParentActive');
        List<Account> acclist = accountMapper.getRecordsHavingActiveParent(new Set<Id>{storageshipToAccount.Id});
        return !acclist.isEmpty();
    }
    
    /**
    * @author       Accenture
    * @name         sendShipToSyncRequestToLOMO
    * @date         02/27/2017
    * @description  This method send customer creation request to LOMOSOFT
    * @param        Account
    * @return       NA
    */
    @TestVisible
    private void sendShipToSyncRequestToLOMO(Account storageShipToAccount){
        EP_GeneralUtility.Log('Private','EP_StorageShipTo','sendShipToSyncRequestToLOMO');
        List<Id> sellToChildListForLS = new List<Id>(); 
        List<id> childRecords = new List<id>();
        String companyName;
        List<account> storageShipToList = accountMapper.getShipToRecordsWithSupplyOptionAndTanks(new Set<Id>{storageShipToAccount.Id});
        if(!storageShipToList.isEmpty()){
            childRecords.addAll(getTankRecordIds(storageShipToList));
            childRecords.addAll(getStockHoldingLocationIds(storageShipToList));        
            companyName = storageShipToList[0].EP_Puma_Company_Code__c;
            EP_OutboundMessageService outservice = new EP_OutboundMessageService(storageShipToAccount.Id,EP_AccountConstant.SFDC_TO_WINDMS_SHIPTO_EDIT,companyName);
            outservice.sendOutboundMessage(EP_AccountConstant.SHIP_TO_EDIT,childRecords);
        }
    }
    
    
	/**
    * @author       Accenture
    * @name         getTankRecordIds
    * @date         02/27/2017
    * @description  This method is used to get the record Ids of the tanks attached to a Ship To
    * @param        List<account>
    * @return       List<id>
    */
    @TestVisible
    private List<id> getTankRecordIds(List<account> storageShipToList){
        EP_GeneralUtility.Log('Private','EP_StorageShipTo','getTankRecordIds');
        List<id> tankRecordIds = new List<id>();
        for(EP_Tank__c tank :storageShipToList[0].Tank__r){
            tankRecordIds.add(tank.Id);
        }
        return tankRecordIds;
    }
	
    
    /**
    * @author       Accenture
    * @name         getStockHoldingLocationIds
    * @date         02/27/2017
    * @description  This method is used to the record Ids of the supply location attached to a Ship To
    * @param        List<account>
    * @return       List<id>
    */
    @TestVisible
    private List<id> getStockHoldingLocationIds(List<account> storageShipToList){
        EP_GeneralUtility.Log('Private','EP_StorageShipTo','getStockHoldingLocationIds');
        List<id> StockHoldingLocationIds = new List<id>();
        for(EP_Stock_Holding_Location__c supply :storageShipToList[0].Stock_Holding_Locations1__r){
            StockHoldingLocationIds.add(supply.Id);
        }
        return StockHoldingLocationIds; 
    }
    
	/**
    * @author       Accenture
    * @name         getStockHoldingLocationIds
    * @date         02/27/2017
    * @description  This method is used to the record Ids of the supply location attached to a Ship To
    * @param        List<account>
    * @return       List<id>
    */
    @TestVisible
    private void setNavWinDmsFlag(Account storageShipToAccount){
        Account objAccount = [select id,EP_SENT_TO_NAV_WINDMS__c from account where id =: storageShipToAccount.Id];
        objAccount.EP_SENT_TO_NAV_WINDMS__c = true;
        update objAccount;
	}
	
	
	/**
    * @author       Accenture
    * @name         isPrimarySupplyOptionAttached
    * @date         02/27/2017
    * @description  Method to check if Ship to has primary supply location
    * @param        Account
    * @return       boolean
    */
    public override boolean isPrimarySupplyOptionAttached(Account shipToAccount){
        EP_GeneralUtility.Log('Public','EP_ShipTo','isPrimarySupplyOptionAttached');
        boolean isSupplyOptionAttached = false;
        integer supplyOptionCount = stockHoldingLocationMapper.getCountOfPrimaryStockHoldingLocations(shipToAccount);
        if(supplyOptionCount > 0){
            isSupplyOptionAttached = true;
        }
        return isSupplyOptionAttached;
    }
}