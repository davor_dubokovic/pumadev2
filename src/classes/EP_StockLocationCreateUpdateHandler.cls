/* 
   @Author          Accenture
   @name            EP_StockLocationCreateUpdateHandler 
   @CreateDate      15/12/2017
   @Description     Handler class to handle the stock location inbound interface
   @Version         1.0
*/
public class EP_StockLocationCreateUpdateHandler extends EP_InboundHandler{ 
	/**
    * @author           Accenture
    * @name             processRequest
    * @date             15/12/2017
    * @description      Will process the customer inbound request and sends back the response
    * @param            string
    * @return           string 
    */
    public override string processRequest(string jsonRequest){ 
        EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHandler','processRequest');
        string jsonResponse = EP_Common_Constant.BLANK;
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_MessageHeader HeaderCommon = new EP_MessageHeader();  
        EP_IntegrationUtil.isCallout = true;
        try {
        	 EP_StockLocationStub stub = parse(jsonRequest); 
             HeaderCommon = stub.MSG.HeaderCommon; 
             list<Account> lstStockLocation =EP_StockLocationCreateUpdateHelper.setStockLocationAttributes(stub.MSG.payload.any0.inventoryLocs.inventoryLoc);
             UpsertStorageLocation(lstStockLocation,ackDatasets);
        	 jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_STOCK_LOCATION_SYNC,false,EP_Common_Constant.BLANK,HeaderCommon,ackDatasets);
        } catch(Exception ex){        	
        	system.debug('KK-->'+ex);
            jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_STOCK_LOCATION_SYNC,true,ex.getMessage(),HeaderCommon,ackDatasets);
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.NAV_TO_SFDC_STOCK_LOCATION_SYNC,EP_StockLocationCreateUpdateHandler.class.getName(), ApexPages.Severity.ERROR);
        }
        return jsonResponse;
    }
    
    /**
    * @author           Accenture
    * @name             parse
    * @date             15/12/2017
    * @description      Method to parse i.e. deserialize the json string into EP_StockLocationStub stub class
    * @param            String
    * @return           EP_CustomerUpdateStub
    */
	@TestVisible
    private static EP_StockLocationStub parse(String json){ 
        EP_GeneralUtility.Log('private','EP_StockLocationCreateUpdateHandler','parse');
        return (EP_StockLocationStub) System.JSON.deserialize(json, EP_StockLocationStub.class);

    }    
	
	/**
	* @author 			Accenture
	* @name				UpsertStorageLocation
	* @date 			15/12/2017
	* @description 		Will perform the DML operations for storage location and sends back the appropriate acknowledgement datasets
	* @param 			List<Account>,List<EP_AcknowledgementStub.dataset>
	* @return 			boolean
	*/
	@TestVisible
    private static boolean UpsertStorageLocation(list<Account> AccountsToUpdate, List<EP_AcknowledgementStub.dataset> ackDatasets){
    	EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHandler','doUpsertDML');
    	boolean recordFailed = false;
    	EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
    	Schema.DescribeFieldResult fieldResult = Account.EP_Storage_Location_External_ID__c.getDescribe(); 
		Schema.sObjectField externalKey = fieldResult.getSObjectField();
    	if(!AccountsToUpdate.isEmpty()){
            List<Database.UpsertResult> results =  DataBase.upsert(AccountsToUpdate,externalKey,false);  
            recordFailed = generateAcnowledgement(AccountsToUpdate,results,ackDatasets);
        }
        return recordFailed;
    }
    
    /**
	* @author 			Accenture
	* @name				generateAcnowledgement
	* @date 			15/12/2017
	* @description 		this method generate the appropriate acknowledgement datasets
	* @param 			List<Account>,List<Database.UpsertResult>,List<EP_AcknowledgementStub.dataset>
	* @return 			boolean
	*/
	@TestVisible
    private static boolean generateAcnowledgement(list<Account> AccountsToUpdate,List<Database.UpsertResult> results, List<EP_AcknowledgementStub.dataset> ackDatasets){
    	EP_GeneralUtility.Log('Public','EP_StockLocationCreateUpdateHandler','generateAcnowledgement');
    	boolean recordFailed = false;
    	EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
    	string errorDescription= EP_Common_Constant.BLANK;
        for(Integer count = 0; count < results.size(); count++){ 
            if(!results[count].isSuccess()){
            	recordFailed= true;
            	errorDescription = EP_GeneralUtility.getDMLErrorMessage(results[count]);
            }
            ackDataset = EP_AcknowledgementUtil.createDataset(EP_COMMON_CONSTANT.CUSTOMER,AccountsToUpdate[count].EP_NavSeqId__c,errorDescription,errorDescription); 
            ackDatasets.add(ackDataset);
        }
        return recordFailed;
    }
    
}