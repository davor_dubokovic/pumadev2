/*
    @Author          Accenture
    @Name            EP_GenerateCreditHoldingRequestXML
    @CreateDate      11/2/2017
    @Description     This class  is used to generate outbound XML's of Credit Holding to Sync with NAV
    @Version         1.0
    @Reference       NA
*/
public virtual class EP_GenerateCreditHoldingRequestXML extends EP_GenerateRequestXML{
    
    public EP_Credit_Holding__c objCreditHolding; 
    public list<Account> lstAccount;

     /** 
    * @author           Accenture
    * @name             init
    * @date             11/02/2017
    * @description      This method is used to create Payload for Credit Holding to Sync with NAV
    * @param            NA
    * @return           NA
    */      
     public override void init(){
     	EP_GeneralUtility.Log('Public','EP_GenerateCreditHoldingRequestXML','init');
     	MSGNode = doc.createRootElement(EP_OrderConstant.MSG, null, null); 
	    lstAccount = EP_AccountMapper.getAccountsByCreditHoldingId(recordId);
	    objCreditHolding = EP_CreditHoldingMapper.getHoldingObject(recordId);  
     }
     
    
    
    /**
    * @author           Accenture
    * @name             createStatusPayLoad
    * @date             04/18/2017
    * @description      This method is used to create Status PayLoad Node
    * @param            NA
    * @return           NA
    */
    public override void createStatusPayLoad(){ 
        EP_GeneralUtility.Log('Public','EP_GenerateCreditHoldingRequestXML','createStatusPayLoad');
        MSGNode.addChildElement(EP_OrderConstant.STATUS_PAYLOAD,null,null).addTextNode(getValueforNode(objCreditHolding.EP_Status__c)); 
        
    }
}