/****************************************************************
@Author      Accenture                                          *
@name        EP_CreditHoldingService                            *
@CreateDate  1/11/2017                                          *
@Description Service class for Credit Holding Object            *
@Version     1.0                                                *
****************************************************************/
public class EP_CreditHoldingService{
    public EP_CreditHoldingDomainObject creditHoldingDomainObject;
    private List<EP_Credit_Holding__c> CreditHoldingList;
    private map<id,EP_Credit_Holding__c> mapOldCreditHolding;
    EP_CreditHoldingMapper creditHoldingMapper;
    EP_AccountMapper accountMapper = new EP_AccountMapper();
    private static final string EP_space = ' ';
    private static final string EP_FROM = 'From';
    private EP_Credit_Holding__c localCreditHoldingObj;

    public EP_CreditHoldingService(){
 
    }

/****************************************************************
* @author       Accenture                                       * 
* @name         EP_CreditHoldingService                         *
* @description  Constructor for setting creditholdingDomain     *
* @param        EP_CreditHoldingDomainObject                    *
* @return       NA                                              *
****************************************************************/ 
    public EP_CreditHoldingService(EP_CreditHoldingDomainObject creditHoldingDomainObject){
        this.creditHoldingDomainObject = creditHoldingDomainObject;
        this.CreditHoldingList = creditHoldingDomainObject.getCreditHolding();
        this.mapOldCreditHolding = creditHoldingDomainObject.getOldRecord();
        this.localCreditHoldingObj = creditHoldingDomainObject.getCreditHoldingObject();
        this.creditHoldingMapper = new EP_CreditHoldingMapper(creditHoldingDomainObject);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         doAfterInsertHandle                             *
* @description  method to execute logic for 
				After Insert of credit holding                 *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/ 
    Public void doAfterInsertHandle(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingService','doAfterInsertHandle');
		doSyncStatusWithNav();
    }
    
    /****************************************************************
* @author       Accenture                                       *
* @name         doBeforeInsertHandle                             *
* @description  method to execute logic for 
				Before Insert of credit holding                 *
* @param        NA                                              *
* @return       NA                                              *
****************************************************************/ 
    
    Public void doBeforeInsertHandle(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingService','doBeforeInsertHandle');
		updateCreditHoldingIntegrationStatus();
    }
    
/****************************************************************
* @author       Accenture                                       *
* @name         doBeforeUpdateHandler                           *
* @description  method to update credit holding record          *
* @param        NA                                              *
* @return       NA                                              * from to be replaced with Constant
****************************************************************/
    Public void doBeforeUpdateHandler(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingService','doBeforeUpdateHandler');
        for(EP_Credit_Holding__c holdingObj : CreditHoldingList){
            if(hasCustomerBlocked(holdingObj)){
                holdingObj.addError(label.EP_Validate_Status_Change1+EP_space+holdingObj.EP_Status__c+EP_space+label.EP_Validate_Status_Change_Error2
                                        +EP_space+holdingObj.EP_Status__c+EP_space+label.EP_Validate_Status_Change_Error3);
            }
            if(creditHoldingDomainObject.isStatusChanged(holdingObj) 
                        && !isStatusChangePossible(mapOldCreditHolding.get(holdingObj.id).EP_Status__c,holdingObj.EP_Status__c)){
                
                holdingObj.addError(label.EP_Validate_Status_Change1+EP_space+holdingObj.EP_Status__c+EP_space+EP_FROM
                                                    +EP_space+mapOldCreditHolding.get(holdingObj.id).EP_Status__c);
            }
            updateCreditHoldingStatus(holdingObj);
        }
    }
    
 /****************************************************************
* @author       Accenture                                       *
* @name         hasCustomerBlocked                              *
* @description  return status of customer                       *
* @param        account list, status                            *
* @return       set of status                                   *
****************************************************************/ 
    private boolean hasCustomerBlocked(EP_Credit_Holding__c holdingObj){
        EP_GeneralUtility.Log('Private','EP_CreditHoldingService','hasCustomerBlocked');
        return(mapOldCreditHolding.get(holdingObj.id).EP_Status__c != holdingObj.EP_Status__c 
                && (holdingObj.EP_Status__c == label.EP_Credit_Holding_Status_Blocked || holdingObj.EP_Status__c == label.EP_Credit_Holding_Status_Deactivated)
                && areAccountsStatusSame(creditHoldingMapper.retriveCreditHoldingAccounts(holdingObj.id),holdingObj.EP_Status__c));
    }


/**************************************************************** 
* @author       Accenture                                       *
* @name         areAccountsStatusSame                           *
* @description  return status of child customer of Holding      *
* @param        account list, status                            *
* @return       set of status                                   *
****************************************************************/     
    private boolean areAccountsStatusSame(List<Account> accList, string status){
        EP_GeneralUtility.Log('Private','EP_CreditHoldingService','areAccountsStatusSame');
        boolean flag = false;
        for(Account acc : accList){
            if(acc.EP_Status__c !=status){
                flag = true;
                break;
            }
        }
        return flag;
    }
 

/****************************************************************
* @author       Accenture                                       *
* @name         isStatusChangePossible                          *
* @description  to check if status change possible from
                     custom setting                             *remove debugs
* @param        new Credit Hoding, Old Credit Holding           *
* @return       boolean                                         *
****************************************************************/
    private boolean isStatusChangePossible(String Oldstatus, String newStatus){
        EP_GeneralUtility.Log('private','EP_CreditHoldingService','isStatusChangePossible');
        EP_Credit_Holding_Possible_Status_Change__c statusChange  = EP_Credit_Holding_Possible_Status_Change__c.getValues(Oldstatus);
        List<string> splitValues = statusChange.EP_Possible_Status_Change__c!=null && statusChange.EP_Possible_Status_Change__c.contains(',')?
                                        statusChange.EP_Possible_Status_Change__c.split(',') : new List<string>();
        splitValues.add(statusChange.EP_Possible_Status_Change__c);
        set<string> possibelStatusChange = new set<string>();
        possibelStatusChange.addAll(splitValues);
        return(possibelStatusChange.contains(newStatus));
    }

/****************************************************************
* @author       Accenture                                       *
* @name         rollupCreditLimit                               * remove DML 
* @description  rollup credit limit on credit holding object    *
* @param        creditHoldingId                                 *
* @return       NA                                              *
****************************************************************/
    public EP_Credit_Holding__c rollupCreditLimit(){
        EP_GeneralUtility.Log('Public','EP_CreditHoldingService','rollupCreditLimit');
        decimal creditLimit = 0;
        for(Account acc : EP_AccountMapper.getAccountsByCreditHoldingId(localCreditHoldingObj.id)){
            if(acc.EP_Status__c == 'Active'){
                creditLimit=+acc.EP_Credit_Limit__c;
            }
        }
        localCreditHoldingObj.EP_holding_credit_limit__c = creditLimit;
        return localCreditHoldingObj;
        //EP_Credit_Holding__c holdingObj = new EP_Credit_Holding__c(id = creditHoldingId,EP_holding_credit_limit__c = creditLimit);
       // Database.update(holdingObj,true);
    }
    

/****************************************************************
* @author       Accenture                                       *
* @name         doSyncStatusWithNav                             *
* @description  sync credit holding with with Nav               * move strings to constant
* @param        credit holding list                             *
* @return        Void                                           *
****************************************************************/      
     private void doSyncStatusWithNav() {
     	EP_GeneralUtility.Log('Public','EP_CreditHoldingService','doSyncStatusWithNav');
     	EP_Common_Util.objcreditHoldingDomainObject =  creditHoldingDomainObject;
        String companyName = EP_Common_Constant.BLANK;
        List<Id> lstIds = new List<Id>();
        for(EP_Credit_Holding__c objCreditHolding :CreditHoldingList){
        	EP_Credit_Holding__c objCreditHoldingCompCode = creditHoldingMapper.retriveCreditHolding(objCreditHolding.id);
        	companyName = objCreditHoldingCompCode.EP_company__c <> null ? objCreditHoldingCompCode.EP_company__r.EP_Company_Code__c:'';
            EP_OutboundMessageService outboundService = new EP_OutboundMessageService(objCreditHolding.Id, EP_Common_Constant.SFDCTONAVCREDITHOLDINGSYNC, companyName);
        	outboundService.sendOutboundMessage(EP_Common_Constant.CREDITHOLDINGUPDATE,lstIds);
        }        
    }

/****************************************************************
* @author       Accenture                                       *
* @name         updateCreditHoldingStatus                       *
* @description  update credit holding status to Ative
			    when sync with Nav                              *
* @param        credit holding list                            *
* @return       Void                                  *
****************************************************************/     
    private void updateCreditHoldingStatus (EP_Credit_Holding__c holdingObj){
        EP_GeneralUtility.Log('Private','EP_CreditHoldingService','updateCreditHoldingStatus');

    	if(mapOldCreditHolding <> null &&  mapOldCreditHolding.containskey(holdingObj.id) && mapOldCreditHolding.get(holdingObj.id).EP_integration_status__c != holdingObj.EP_integration_status__c 
    		&& holdingObj.EP_integration_status__c ==EP_Common_Constant.SYNC_STATUS && !creditHoldingMapper.isHoldingAssociationExists(holdingObj.id)){
    		
    		holdingObj.EP_Status__c =EP_Common_Constant.ACTIVE;		
    	}
    }
    
/****************************************************************
* @author       Accenture                                       *
* @name         updateCreditHoldingIntegrationStatus            *
* @description  update credit holding Integration status to 
			    SYNC                            				*
* @param        credit holding list                             *
* @return       Void                                  			*
****************************************************************/      
	 private void updateCreditHoldingIntegrationStatus (){
        EP_GeneralUtility.Log('Private','EP_CreditHoldingService','updateCreditHoldingStatus');

    	for(EP_Credit_Holding__c objCredHolding :CreditHoldingList){
    		objCredHolding.EP_integration_status__c =EP_Common_Constant.SENT_STATUS;
    	}
    }   
}