/* 
   @Author <Accenture>
   @name <EP_PricingResponseHelper>
   @CreateDate <03/01/2017>
   @Description <This is a class to Transform JSON string data into SOBject(order Item) for Pricing Response Rest Service Request from NAV> 
   @Version <1.0>
   */
public with sharing class EP_PricingResponseHelper {
    @TestVisible private csord__Order__c orderObject;
    @TestVisible private map<string, orderItem> orderItemMap = new map<string, orderItem>();
    @TestVisible private map<string, PriceBookEntry> priceBookEntryIdMap = new map<string, PriceBookEntry>();
    @TestVisible private list<EP_PricingResponseStub.LineItem> lineItems = new list<EP_PricingResponseStub.LineItem>();
    @TestVisible private EP_OrderService orderService;
    @TestVisible private EP_OrderDomainObject orderDomain;
    /**
    * @Author       Accenture
    * @Name         doBasicDataSetup
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    public void doBasicDataSetup() {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','doBasicDataSetup');        
        Id accountId = orderObject.EP_ShipTo__c == null ? orderObject.AccountId__c : orderObject.EP_ShipTo__c;
        createMissingPriceBookEntries(setProducts(accountId),orderObject);        
        setPriceBookEntries(orderObject.accountId__c);
    }
    
    public csord__Order__c getOrderObject() {
        return orderObject;
    }
    /**
    * @Author       Accenture
    * @Name         getPriceBookEntry
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    public void setPriceBookEntries(Id accountId){
        system.debug('**accountId**'+accountId);
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','getPriceBookEntry');
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(accountId);
        EP_AccountService accountService = new EP_AccountService(accountDomain);
        priceBookEntryIdMap = accountService.getPriceBookEntry(EP_Common_Constant.PRODUCTNAME);
        system.debug('**priceBookEntryIdMap**'+priceBookEntryIdMap);
    }
    /**
    * @Author       Accenture
    * @Name         createDataMaps
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void createDataMaps(list<EP_PricingResponseStub.LineItem> priceLineItems,String orderNumber) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','createDataMaps');
        EP_OrderMapper orderMapper = new EP_OrderMapper();
        map<string, csord__Order__c> orderNumberOrderMap = orderMapper.getCSOrderMapByOrderNumber(new set<string>{orderNumber});
        lineItems = priceLineItems;
        orderObject = orderNumberOrderMap.get(orderNumber);
        orderItemMap = getExistingOrderItems(orderNumber);
    }
    
    /**
    * @Author       Accenture
    * @Name         setOrderItemAttributes
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    public void setOrderItemAttributes(list<EP_PricingResponseStub.LineItem> priceLineItems,String orderNumber){
        EP_GeneralUtility.Log('Public','EP_PricingResponseHelper','setOrderItemAttributes');
        createDataMaps(priceLineItems,orderNumber);
        doBasicDataSetup();
        processOrderItems(priceLineItems, orderNumber);
    }
    
    /**
    * @Author       Accenture
    * @Name         setOrderItemJSONAttributes
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void setOrderItemJSONAttributes(EP_PricingResponseStub.LineItemInfo lineItemInfo) {
        EP_GeneralUtility.Log('private','EP_PricingResponseHelper','setOrderItemJSONAttributes');
        lineItemInfo.orderLineItem = new OrderItem();
        lineItemInfo.orderLineItem.EP_WinDMS_Line_ItemId__c = lineItemInfo.itemId;
        lineItemInfo.orderLineItem.Quantity = Integer.valueOf(EP_GeneralUtility.formatNumber(lineItemInfo.quantity));
        lineItemInfo.orderLineItem.EP_Pricing_Response_Unit_Price__c = Decimal.valueOf(lineItemInfo.unitPrice);
    }
    /**
    * @Author       Accenture
    * @Name         setInvoiceItemJSONAttributes
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void setInvoiceItemJSONAttributes(EP_PricingResponseStub.InvoiceComponent ordInvoiceItem) {
        EP_GeneralUtility.Log('private','EP_PricingResponseHelper','setInvoiceItemJSONAttributes');
        ordInvoiceItem.invoiceItem = new OrderItem();
        ordInvoiceItem.invoiceItem.EP_Invoice_Name__c = ordInvoiceItem.Name;
        ordInvoiceItem.invoiceItem.EP_Pricing_Total_Amount__c = Decimal.valueOf(EP_GeneralUtility.formatNumber(ordInvoiceItem.amount));
        ordInvoiceItem.invoiceItem.EP_Tax_Percentage__c = Decimal.valueOf(ordInvoiceItem.taxPercentage);
        ordInvoiceItem.invoiceItem.EP_Tax_Amount__c =  Decimal.valueOf(ordInvoiceItem.taxAmount);
        ordInvoiceItem.invoiceItem.UnitPrice = Decimal.valueOf(EP_GeneralUtility.formatNumber(ordInvoiceItem.totalAmount));
    }
    /**
    * @Author       Accenture
    * @Name         processOrderItems
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void processOrderItems(list<EP_PricingResponseStub.LineItem> priceLineItems,String orderNumber) {
        EP_GeneralUtility.Log('Public','EP_PricingResponseHelper','processOrderItems');
        for(EP_PricingResponseStub.LineItem priceLineItem : priceLineItems) {
            setOrderItemJSONAttributes(priceLineItem.lineItemInfo);
            setOrderItemId(priceLineItem);
            setInvoiceItem(priceLineItem, priceLineItem.lineItemInfo.lineItemId);
            setAccountComponentXML(priceLineItem);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         setOrderItemId
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void setOrderItemId(EP_PricingResponseStub.LineItem priceLineItem) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','setOrderItemId');
        orderItem existingOrderItem = orderItemMap.get(priceLineItem.lineItemInfo.lineItemId);
        priceLineItem.lineItemInfo.orderLineItem.Id = existingOrderItem.Id;
        
    }
    
    /**
    * @Author       Accenture
    * @Name         setInvoiceItem
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void setInvoiceItem(EP_PricingResponseStub.LineItem priceLineItem, string parentId) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','setInvoiceItem');
        orderItem existingOrderItem = orderItemMap.get(parentId);
        for(EP_PricingResponseStub.InvoiceComponent ordInvoiceItem: priceLineItem.lineItemInfo.invoiceDetails.InvoiceComponent) {
            setInvoiceItemJSONAttributes(ordInvoiceItem);
            ordInvoiceItem.invoiceItem.EP_Parent_Order_Line_Item__c = existingOrderItem.id;
            ordInvoiceItem.invoiceItem.OriginalOrderItemId = existingOrderItem.id;
            ordInvoiceItem.invoiceItem.quantity = priceLineItem.lineItemInfo.orderLineItem.quantity;
            ordInvoiceItem.invoiceItem.orderId = existingOrderItem.orderId;
            ordInvoiceItem.invoiceItem.priceBookEntryId = setPriceBookEntryId(ordInvoiceItem.type);
        }
        
    }
    
    /**
    * @Author       Accenture
    * @Name         setpriceBookEntryId
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private string setpriceBookEntryId(string productName) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','setpriceBookEntryId');
        string priceBookEntryId;
        String priceBookEntryKey = getPricebookEntryKey(productName);
        if(priceBookEntryIdMap.containsKey(priceBookEntryKey)) {
            priceBookEntryId = priceBookEntryIdMap.get(priceBookEntryKey).Id;
        }
        return priceBookEntryId;
    }
    
    /**
    * @Author       Accenture
    * @Name         getExistingOrderItems
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private map<string, orderItem> getExistingOrderItems(string orderNumber) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','getExistingOrderItems');
        map<string, orderItem> orderItemMap = new map<string, orderItem>();
        EP_OrderItemMapper orderItemMapper = new EP_OrderItemMapper();
        for (OrderItem oItem: orderItemMapper.getOrderItemsByOrderNumber(orderNumber)) {
            orderItemMap.put(String.valueOf(oItem.EP_OrderItem_Number__c),oItem);
        }
        return orderItemMap;
    }
    
    /**
    * @Author       Accenture
    * @Name         setAccountComponentXML
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void setAccountComponentXML(EP_PricingResponseStub.LineItem priceLineItem)  {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','setAccountComponentXML');
        string accTagString = EP_Common_Constant.BLANK;
        for (EP_PricingResponseStub.AcctComponent accComp: priceLineItem.accountingDetails.acctComponents.acctComponent) {
            accTagString += EP_Common_Constant.XML_ACC_COMPONENT_BEGIN;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_COMPONENTCODE  + EP_Common_Constant.ANG_RIGHT + ((accComp.componentCode<> null)?accComp.componentCode:EP_Common_Constant.BLANK) + EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_COMPONENTCODE + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_GLACCOUNT_NR + EP_Common_Constant.ANG_RIGHT + ((accComp.glAccountNr <> null)?accComp.glAccountNr:EP_Common_Constant.BLANK)  + EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_GLACCOUNT_NR + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_BASEAMT + EP_Common_Constant.ANG_RIGHT + ((accComp.baseAmount <> null)?accComp.baseAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_BASEAMT + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_TAXRATE + EP_Common_Constant.ANG_RIGHT + ((accComp.taxRate <> null)?accComp.taxRate :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_TAXRATE + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_TAXAMOUNT + EP_Common_Constant.ANG_RIGHT + ((accComp.taxAmount <> null)?accComp.taxAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_TAXAMOUNT + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.TOTALAMOUNT_TAG + EP_Common_Constant.ANG_RIGHT + ((accComp.totalAmount <> null)?accComp.totalAmount :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.TOTALAMOUNT_TAG + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.ANG_LEFT_OPEN + EP_Common_Constant.XML_IS_VAT + EP_Common_Constant.ANG_RIGHT + ((accComp.isVAT <> null)?accComp.isVAT :EP_Common_Constant.BLANK)+ EP_Common_Constant.ANG_LEFT_CLOSE + EP_Common_Constant.XML_IS_VAT + EP_Common_Constant.ANG_RIGHT;
            accTagString += EP_Common_Constant.XML_ACC_COMPONENT_CLOSE;    
        }
        priceLineItem.lineItemInfo.orderLineItem.EP_Accounting_Details__c = accTagString;
    }
    
    /**
    * @Author       Accenture
    * @Name         setProducts
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private list<Product2> setProducts(Id accountId) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','setProducts');
        list<Product2> productsForNewPBEntry = new list<Product2>(); 
        EP_ProductMapper productMapper = new EP_ProductMapper();
        string priceBookEntryKey;
        set<string> productNameSet = getProductsName();
        setPriceBookEntries(accountId);
        for(Product2 product : productMapper.getRecordsByName(productNameSet)) {
            productNameSet.remove(product.name);
            priceBookEntryKey = getPricebookEntryKey(product.Name);
            if(!priceBookEntryIdMap.containsKey(priceBookEntryKey)) {
                productsForNewPBEntry.add(product);
            }
        }
        productsForNewPBEntry.addAll(createNewProducts(productNameSet));
        return productsForNewPBEntry;
    }
    
    /**
    * @Author       Accenture
    * @Name         getPricebookEntryKey
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private string getPricebookEntryKey(string productName) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','getPricebookEntryKey');
        string priceBookEntryKey;
        return  priceBookEntryKey = productName + orderObject.pricebook2Id__c + orderObject.currencyIsoCode;
    }
    
    /**
    * @Author       Accenture
    * @Name         createPriceBookEntries
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private void createMissingPriceBookEntries(list<Product2> productsForNewPBEntry, csord__Order__c localOrderObject) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','createPriceBookEntries');
        PriceBookEntry pricebookEntry = new PriceBookEntry();
        List<PriceBookEntry> priceBookEntryList = new List<PriceBookEntry>();
        for(Product2 product : productsForNewPBEntry){
            pricebookEntry = new PriceBookEntry();
            pricebookEntry.PriceBook2Id = localOrderObject.pricebook2Id__c;
            pricebookEntry.Product2Id = product.id; 
            pricebookEntry.isActive = true;
            pricebookEntry.UnitPrice = 0.0;
            pricebookEntry.EP_Is_Sell_To_Assigned__c = True;
            pricebookEntry.currencyIsoCode = localOrderObject.currencyIsoCode;
            priceBookEntryList.add(pricebookEntry);
        }
        if(!priceBookEntryList.isEmpty()){
            Database.insert(priceBookEntryList);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         createNewProducts
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private List<Product2> createNewProducts(set<string> productNameSet) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','createNewProducts');
        Product2 newProductObj = new Product2();
        List<Product2> newProductList = new List<Product2>();
        for(String  productName : productNameSet){
            newProductObj = new Product2();
            newProductObj.Name = productName;
            newProductObj.IsActive = True;
            newProductObj.EP_Is_System_Product__c = true;
            newProductList.add(newProductObj);
        }
        if(!newProductList.isEmpty()){
            Database.insert(newProductList);
        }
        return newProductList;
    }
    
    /**
    * @Author       Accenture
    * @Name         getProductsName
    * @Date         03/28/2017
    * @Description  
    * @Param        
    * @return        
    */
    @TestVisible
    private set<string> getProductsName() {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','getProductsName');
        set<string> productNameSet = new set<string>();
        for(EP_PricingResponseStub.LineItem priceLineItem : lineItems) {
            for(EP_PricingResponseStub.InvoiceComponent invComp: priceLineItem.lineItemInfo.invoiceDetails.invoiceComponent) {
                productNameSet.add(invComp.type);
            }
        }
        return productNameSet;
    }
    
    /**
    * @Description: This method will get all order Line items and invoice line items from the JSON Object Class
    * @Date :03/03/2017
    * @Name :getOrderItemList
    * @param: msgTransformObj - Instance of EP_PricingResponseMsgTransform class
    * @return: order items 
    */
    public static list<orderItem> getOrderItemList(list<EP_PricingResponseStub.LineItem> priceLineItems) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseStub','getOrderItemList');
        list<orderItem> orderItemList = new list<orderItem>();
        for(EP_PricingResponseStub.LineItem priceLineItem : priceLineItems){
            orderItemList.add(priceLineItem.lineItemInfo.orderLineItem);
            for(EP_PricingResponseStub.InvoiceComponent invComp: priceLineItem.lineItemInfo.invoiceDetails.invoiceComponent) {
                orderItemList.add(invComp.invoiceItem);
            }
        }
        return orderItemList;
    }
    
    /**
    * @Description: This method use to get the Order Status based on specific set of conditions.
    * @Date :02/27/2017
    * @Name :setOrderStatus
    * @param: Status of orders
    * @return: void
    */
    public void setOrderStatus(csord__Order__c orderObject) {
        EP_GeneralUtility.Log('Private','EP_PricingResponseHelper','getOrderStatus');
        orderDomain = new EP_OrderDomainObject(orderObject);
        //Defect Fix Start #57591
        EP_OrderEvent orderEvent;
        //Order status should change only for VMI-Current Orders. Other type of orders will be handled in controller
        if(EP_Common_Constant.VMI.equalsIgnorecase(orderDomain.getVMIType()) && !orderDomain.isRetrospective()){

            orderEvent = new EP_OrderEvent(orderDomain.getStatus()+ EP_OrderConstant.OrderEvent_ToString + orderObject.Status__c);
        }
        
        //Handle Case for Imported orders
        if(orderDomain.getOrder().EP_Is_Imported_Order__c) {
            orderEvent = new EP_OrderEvent(EP_OrderConstant.IMPORTEDORDERPRICINGRESPONSE);
        }
        
        if(orderEvent != null) {
        	orderService = new EP_OrderService(orderDomain);
        	orderService.setOrderStatus(orderEvent);
        }
        //Defect Fix End #57591
    }
    

    public void doPostUpdateActions(){
        //Defect Fix Start #57591
        if((EP_Common_Constant.VMI.equalsIgnorecase(orderDomain.getVMIType()) && !orderDomain.isRetrospective()) 
            || orderDomain.getOrder().EP_Is_Imported_Order__c ){
            //Changes for #60147
            //Setting this variable to enqueue Job to send outbound message for OrderSync with Nav when Pricing Sync
          	//For VMI order Creation, A Pricing callout will be a future outbound call out and after successfully pricing sync, State machine will hit another future outbound callout for order sync with NAV and SFDC does not allow to do a future callout from futrue call out
           	orderService.enqueueJob = true;
            orderService.doPostStatusChangeActions();
        }
        //Defect Fix End #57591
    }
}