global class TrafiguraGetPrices implements csoe.IRemoteAction {

	global Map<String, Object> execute(Map<String, Object> inputMap) {
		Map<String, Object> returnMap = new Map<String, Object>();
		
		try {
			String basketId = (String) inputMap.get('BasketId');
			String orderJSON = (String) inputMap.get('OrderJSON');
			String accid = (String) inputMap.get('AccId');
			EP_GeneralUtility.Log('Public','EP_OutboundMessageService','sendOutboundPricingMessage');
			string xmlMessage = '';
			Id recordId = basketId;
			String messageType = 'SEND_ORDER_PRICING_REQUEST';
			string responseBody = '';
			CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
			Integer orderQuantity = 0;
			EP_OrderDataStub order;
			
			JSONParser parser = JSON.createParser(orderJSON);
			while (parser.nextToken() != null) {
				if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
					order = (EP_OrderDataStub)parser.readValueAs(EP_OrderDataStub.class);
				}
			}
			order.quantity = String.valueOf(orderQuantity);
			EP_OutboundMessageService.orderStub = order;
			String companyCode = order.lineItems[0].companyCode;
			string messageId = '8010l0000008l6qAAA-21082017220223-001780';

			Account acc = [SELECT EP_Currency__c from Account where id = :accid];
            order.currencyCode = acc.EP_Currency__c;

			//1. Get VF page name for given outbound Message Type from custom setting
			EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
			
			//2. get XML Message
			system.debug(JSON.serializePretty(inputMap));
			xmlMessage = EP_OutboundMessageService.generateXMLCS(recordId, messageId, messageType, msgSetting.Payload_VF_Page_Name__c, companyCode);
			system.debug('xmlMessage----------------');
			system.debug(JSON.serializePretty(xmlMessage));
			system.debug(LoggingLevel.ERROR, xmlMessage);
			//Get End Point
			string endPoint = msgSetting.End_Point__c.replace(EP_Common_Constant.COMPANY_CODE_URL_TOKAN, companyCode);
			
			//Defect Fix End - 57012
			//3. initate integration Services
			EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, EP_Common_Constant.POST, xmlMessage, msgSetting.EP_Subscription_Key__c);
			//4. Do Call out
			EP_IntegrationServiceResult result = EP_OutboundMessageService.doCallOutCS(intService, msgSetting);
			//5. Process result
			responseBody = result.getResponse().getBody();
			returnMap.put('status', result.getResponse().getStatus());
			if(responseBody.startsWith(EP_Common_Constant.BOM_STR)) {
			   responseBody = responseBody.replaceFirst(EP_Common_Constant.BOM_STR, EP_Common_Constant.BLANK);
			}
			List<Attachment> pricingResponseAtt = [
				select id
				from Attachment
				where Name = 'pricingResponse'
				and ParentId = :basketId
			];
			if (!pricingResponseAtt.isEmpty()) {
				delete pricingResponseAtt;
			}
			Attachment att = new Attachment(
				ParentId = basketId,
				Name = 'pricingResponse',
				Body = Blob.valueOf(responseBody)
			);
			insert att;
			returnMap.put('response', responseBody);
		} catch(Exception e) {
			returnMap.put('error', e.getStackTraceString());	
			returnMap.put('errormsg', e.getMessage());	
		}
		return returnMap;
	}
	
	public static void test() {
		Map<Id, List<csoe.SchemaWrapper>> wrap = csoe.API_1.decomposeSchema(new List<Id>{'a2V0k000000EKFFEA4'}); 
		Map<String,String> attributeNameValueMap = new Map<String,String>();
		for (csoe.SchemaWrapper wrapper : wrap.get('a2V0k000000EKFFEA4')) { 
			Map<Integer, List<csoe.SchemaWrapper.SimpleAttribute>> configs = wrapper.configurations; 
			for (Integer i : configs.keySet()) {   
				List<csoe.SchemaWrapper.SimpleAttribute> attributes = configs.get(i);
				for (csoe.SchemaWrapper.SimpleAttribute att : attributes) {
					attributeNameValueMap.put(att.name,att.value);
				}
			} 
		}
	}
	
}