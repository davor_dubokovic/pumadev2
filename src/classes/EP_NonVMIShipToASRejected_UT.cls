@isTest
public class EP_NonVMIShipToASRejected_UT
{

    static final string EVENT_NAME = '08-RejectedTo08-Rejected';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
      Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
      Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        System.AssertEquals(obj,localObj.account);
    }
    static testMethod void doOnEntry_test() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        Account acc = EP_TestDataUtility.getNONVMIShioToAsAccountSetup();
        acc.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_WinDMS__C = true;
        update acc;
        system.debug('obj.localAccount = '+acc);
        EP_AccountDomainObject obj1 = new EP_AccountDomainObject(acc.id);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj1,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doOnExit_test() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size() > 0);
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        EP_AccountDomainObject obj = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doOnEntry_test1() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        Account acc = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObjectPositiveScenario().localaccount;
        //acc.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        acc.EP_Synced_PE__c = false;
        acc.EP_Synced_NAV__c = false;
        acc.EP_Synced_WinDMS__C = false;
        updateSellToSynced(acc.ParentId);
        EP_AccountDomainObject obj1 = new EP_AccountDomainObject(acc);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj1,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
	static testMethod void doOnEntry_test2() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        Account acc = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObjectPositiveScenario().localaccount;
        //acc.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_NAV__c = false;
        acc.EP_Synced_WinDMS__C = false;
		updateSellToSynced(acc.ParentId);
        EP_AccountDomainObject obj1 = new EP_AccountDomainObject(acc);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj1,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
	static testMethod void doOnEntry_test3() {
        EP_NonVMIShipToASRejected localObj = new EP_NonVMIShipToASRejected();
        Account acc = EP_TestDataUtility.getNonVMIShipToASRejectedDomainObjectPositiveScenario().localaccount;
        //acc.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_WinDMS__C = false;
        updateSellToSynced(acc.ParentId);
        EP_AccountDomainObject obj1 = new EP_AccountDomainObject(acc);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj1,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //No Assertion as the method is empty ,hence adding a dummy assert.
        system.Assert(true);
    }
    
    private static void updateSellToSynced(Id parentId){
        Account sellToAccount = [select id,EP_Status__c from Account Where Id =: parentId Limit 1];
        sellToAccount.EP_Synced_PE__c = true;
        sellToAccount.EP_Synced_NAV__c = true;
        sellToAccount.EP_Synced_WinDMS__C = true;
        sellToAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update sellToAccount;
    }
    //L4_end

}