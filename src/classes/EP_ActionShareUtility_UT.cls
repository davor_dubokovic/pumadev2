/*  
   @Author <Accenture>
   @name <EP_ActionShareUtility_UT>
   @CreateDate <11/26/2017>
   @Description <This class will be use to test/cover unit test cases for EP_ActionShareUtility class>  
   @Version <1.0>
*/
@isTest
private class EP_ActionShareUtility_UT { 
	
	/*public static account acc;
	public static user objUser;
	@testSetup static void init() {
      	account acc = EP_TestDataUtility.getSellToPositiveScenario();
        acc.EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;  
        profile objProfile = [select id from profile where name= 'Puma KYC Reviewer_R1' limit 1];
        user objUser =EP_TestDataUtility.createUser(objProfile.id);
        insert objUser;
        EP_Common_Constant.RUNUSERCOMPANYTRIGGER =false;
        EP_TestDataUtility.createUserCompany(objUser.id,acc.EP_Puma_Company__c);
    }*/
	static testMethod void getValidUserList_TestPositive() {
		account acc = EP_TestDataUtility.getSellToPositiveScenario();
        acc.EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;  
        profile objProfile = [select id from profile where name= 'Puma KYC Reviewer_R1' limit 1];
        user objUser =EP_TestDataUtility.createUser(objProfile.id);
        insert objUser;
        EP_TestDataUtility.createUserCompany(objUser.id,acc.EP_Puma_Company__c);
		list<EP_Action__c> lstAction = [select id ,EP_Puma_Company__c,Action_Type__c from EP_Action__c where EP_Account__c=:acc.id ];
        system.debug('KK11-lstAction->'+lstAction);
        Test.startTest();
        
        EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
        map<id,list<id>> result = EP_ActionShareUtilityObj.getValidUserList(lstAction);
        system.debug('KK11-result->'+result);
        Test.stopTest();
        System.AssertEquals(lstAction.size(),result.size()); 
    }
    static testMethod void getValidUserList_TestNegative() {
        Test.startTest();
        EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
        map<id,list<id>> result = EP_ActionShareUtilityObj.getValidUserList(new list<EP_Action__c>());
        Test.stopTest();
        system.debug('KKK--->'+result);
        System.AssertEquals(true,result.size()==0); 
    }
    static testMethod void getReviewProfileMapping_Test() {
    	EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
    	Test.startTest();
        map<string,string> result = EP_ActionShareUtilityObj.getReviewProfileMapping();
        Test.stopTest(); 
        System.AssertEquals(true,result.size()>0);
    }
    
   static testMethod void setReviewersForAction_TestPositive() {
   		account acc = EP_TestDataUtility.getSellToPositiveScenario();
        acc.EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;  
        profile objProfile = [select id from profile where name= 'Puma KYC Reviewer_R1' limit 1];
        user objUser =EP_TestDataUtility.createUser(objProfile.id);
        insert objUser;
        EP_TestDataUtility.createUserCompany(objUser.id,acc.EP_Puma_Company__c);
        list<EP_Action__c> lstAction = [select id ,EP_Puma_Company__c,Action_Type__c from EP_Action__c where EP_Account__c=:acc.id ];
        Test.startTest();
        EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
        set<user> setUser = new set<user>();
        setUser.add(objUser);
        map<id,list<id>> result = EP_ActionShareUtilityObj.setReviewersForAction( setUser ,lstAction);
        Test.stopTest();
        system.debug('KKK--->'+result);
        System.AssertEquals(lstAction.size(),result.size()); 
    }
    static testMethod void setReviewersForAction_TestNegative() {
        Test.startTest();
        EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
        map<id,list<id>> result = EP_ActionShareUtilityObj.setReviewersForAction( new set<user>() ,new list <EP_Action__c>());
        Test.stopTest();
        system.debug('KKK--->'+result);
        System.AssertEquals(true,result.size()==0); 
    }
    
    static testMethod void getFilteredUsersOnProfile_TestNegative() {
        Test.startTest();
        
        EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
        list<id> result = EP_ActionShareUtilityObj.getFilteredUsersOnProfile( new set<user>() ,EP_Common_Constant.ACT_KYC_REVIEW_RT_DEV);
        Test.stopTest();
        system.debug('KKK--->'+result);
        System.AssertEquals(true,result.size()==0); 
    }
    
    static testMethod void getProfileReviewTypeMapping_Test() {
    	EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
    	Test.startTest();
        map<string,string> result = EP_ActionShareUtilityObj.getProfileReviewTypeMapping();
        Test.stopTest(); 
        System.AssertEquals(true,result.size()>0);
    }
    
    static testMethod void getProfileNameIdMapping_Test() {
    	EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
    	Test.startTest();
        map<string,string> result = EP_ActionShareUtilityObj.getProfileNameIdMapping();
        Test.stopTest(); 
        System.AssertEquals(true,result.size()>0);
    }
    
    static testMethod void shareRecords_Test() {
    	
    	account acc = EP_TestDataUtility.getSellToPositiveScenario();
        acc.EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc;  
        profile objProfile = [select id from profile where name= 'Puma KYC Reviewer_R1' limit 1];
        user objUser =EP_TestDataUtility.createUser(objProfile.id);
        insert objUser;
        EP_TestDataUtility.createUserCompany(objUser.id,acc.EP_Puma_Company__c);
        map<id,list<id>> maptemp = new map<id,list<id>>();
        list<id> lstuser = new list<id>();
        lstuser.add(objUser.id);
        
        list<EP_Action__c> lstAction = [select id ,EP_Puma_Company__c,Action_Type__c from EP_Action__c where EP_Account__c=:acc.id ];
        maptemp.put(lstAction[0].id,lstuser);
    	EP_ActionShareUtility EP_ActionShareUtilityObj = new EP_ActionShareUtility(); 
    	Test.startTest();
        map<string,string> result = EP_ActionShareUtilityObj.getProfileNameIdMapping();
        Test.stopTest(); 
        System.AssertEquals(true,result.size()>0);
    }
}