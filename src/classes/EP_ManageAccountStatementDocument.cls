/**
 *  @Author <Kamal Garg>
 *  @Name <EP_ManageAccountStatementDocument>
 *  @CreateDate <04/07/2016>
 *  @Description <This is the apex class used to generate PDF for Customer Account Statement record>
 *  @Version <1.0>
 */
public with sharing class EP_ManageAccountStatementDocument {

    private static final string CLASS_NAME = 'EP_ManageAccountStatementDocument ';
    private static final string GENERATE_AT_METHOD = 'generateAttachmentForAccountStatement';

    /**
     * @author <Pooja Dhiman>
     * @name <generateAttachmentForAccountStatement>
     * @date <04/07/2016>
     * @description <This method is used to generate document for Account Statement>
     * @version <1.0>
     * @param EP_ManageDocument.Documents
     */
    public static void generateAttachmentForAccountStatement(EP_ManageDocument.Documents documents) {
        
        List<EP_Customer_Account_Statement__c> lstCustAccStatment = new List<EP_Customer_Account_Statement__c>();
        Map<String, List<Attachment>> customerAccountAttachListMap = new Map<String, List<Attachment>>();
        try
        {
            List<Attachment> customerAccAttachment = null;
            for(EP_ManageDocument.Document doc : documents.document) {
                EP_ManageAccountStatementDocument.CustomerAccountStatementWrapper custAccountStmtWrap = EP_DocumentUtil.fillCustomerAccountStatementWrapper(doc.documentMetaData.metaDatas);
                EP_Customer_Account_Statement__c oCustAccStmt = generateCustomerAccountStatement(custAccountStmtWrap);
                lstCustAccStatment.add(oCustAccStmt);
                
                String customerAccStmtKey = oCustAccStmt.EP_Customer_Account_Statement_Key__c;
                Attachment att = EP_DocumentUtil.generateAttachment(doc);
                if(!(customerAccountAttachListMap.containsKey(customerAccStmtKey))) {
                    customerAccAttachment = new List<Attachment>(); 
                    customerAccountAttachListMap.put(customerAccStmtKey, customerAccAttachment);
                }
                customerAccountAttachListMap.get(customerAccStmtKey).add(att);
            }
            
            Schema.SObjectField f = EP_Customer_Account_Statement__c.Fields.EP_Customer_Account_Statement_Key__c;
            List<Database.upsertResult> uResults = Database.upsert(lstCustAccStatment, f, false);
            for(Database.upsertResult result : uResults) {
                if(result.isSuccess() && result.isCreated()) {
                    System.debug('Successfully inserted/updated CAS, CAS ID: ' + result.getId());
                } else {
                    for(Database.Error err : result.getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('CAS fields that affected this error: ' + err.getFields());
                    }
                }
            }
            
            Map<String, Id> CustAccstmtNrIdMap = new Map<String, Id>();
            Integer nRows = EP_Common_Util.getQueryLimit();
            for(EP_Customer_Account_Statement__c obj : [SELECT Id, EP_Customer_Account_Statement_Key__c FROM EP_Customer_Account_Statement__c  Limit :nRows]) {
                CustAccstmtNrIdMap.put(obj.EP_Customer_Account_Statement_Key__c, obj.Id);
            }
            List<Attachment> attachments = new List<Attachment>();
            for(String key : customerAccountAttachListMap.keySet()) {
                List<Attachment> attachList = customerAccountAttachListMap.get(key);
                for(Attachment att : attachList) {
                    att.ParentId = CustAccstmtNrIdMap.get(key);
                    attachments.add(att);
                }
            }
            
            EP_DocumentUtil.deleteAttachments(attachments);
            
            Database.SaveResult[] results = Database.insert(attachments, false);
            for (Integer i = 0; i < results.size(); i++) {
                if (results[i].isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted attachment. Attachment ID: ' + results[i].getId());
                    EP_ManageDocument.successRecIdMap.put(attachments[i].Name, results[i].getId());
                } else {
                    // Operation failed, so get all errors
                    for(Database.Error err : results[i].getErrors()) {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Attachment fields that affected this error: ' + err.getFields());
                        EP_ManageDocument.errDesMap.put(attachments[i].Name, err.getMessage());
                        EP_ManageDocument.errCodeMap.put(attachments[i].Name, String.valueOf(err.getStatusCode()));
                    }
                }
            }
        }
        catch(Exception e){
             EP_loggingService.loghandledException(e,EP_Common_Constant.EPUMA, GENERATE_AT_METHOD , CLASS_NAME,apexPages.severity.ERROR);
             throw e;
        }
    }
    
    /**
     * @author <Kamal Garg>
     * @name <generateCustomerAccountStatement>
     * @date <04/07/2016>
     * @description <This method is used to generate 'EP_Customer_Account_Statement__c' custom object record>
     * @version <1.0>
     * @param EP_ManageAccountStatementDocument.CustomerAccountStatementWrapper
     * @return EP_Customer_Account_Statement__c
     */
    private static EP_Customer_Account_Statement__c generateCustomerAccountStatement(EP_ManageAccountStatementDocument.CustomerAccountStatementWrapper custAcctStatWrap) {
        EP_Customer_Account_Statement__c acctStatObj = new EP_Customer_Account_Statement__c();
        acctStatObj.EP_Statement_Issue_Date__c = Date.valueOf(custAcctStatWrap.IssueDate);
        acctStatObj.EP_Statement_NAV_ID__c = custAcctStatWrap.SeqId;
        if(String.isNotBlank(custAcctStatWrap.type_Z) && EP_Common_Constant.ALL_TYPE.equals(custAcctStatWrap.type_Z)) {
            acctStatObj.EP_Statement_Start_Date__c = Date.valueOf(custAcctStatWrap.StatementStartDate);
            acctStatObj.EP_Statement_End_Date__c = Date.valueOf(custAcctStatWrap.StatementEndDate);
            acctStatObj.EP_Customer_Account_Statement_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{EP_Common_Constant.ALL_TYPE, 
                                                                                                                                    custAcctStatWrap.BillTo, 
                                                                                                                                    custAcctStatWrap.StatementStartDate, 
                                                                                                                                    custAcctStatWrap.StatementEndDate, 
                                                                                                                                    custAcctStatWrap.issuedFromId});
            acctStatObj.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_OBJECT, 
                                                                            EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_RECORD_TYPE_NAME);
            acctStatObj.EP_Closing_Balance__c = Decimal.valueOf(custAcctStatWrap.closingBalance.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Opening_Balance__c = Decimal.valueOf(custAcctStatWrap.openingBalance.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Total_Credits__c = Decimal.valueOf(custAcctStatWrap.totalCredits.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Total_Debits__c = Decimal.valueOf(custAcctStatWrap.totalDebits.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
        } else if(String.isNotBlank(custAcctStatWrap.type_Z) && EP_Common_Constant.OPEN_TYPE.equals(custAcctStatWrap.type_Z)) {
            acctStatObj.EP_Statement_Start_Date__c = NULL;
            acctStatObj.EP_Statement_End_Date__c = NULL;
            acctStatObj.EP_Customer_Account_Statement_Key__c = EP_DocumentUtil.generateUniqueKey(new Set<String>{EP_Common_Constant.OPEN_TYPE, 
                                                                                                                                    custAcctStatWrap.BillTo, 
                                                                                                                                    custAcctStatWrap.StatementEndDate, 
                                                                                                                                    custAcctStatWrap.issuedFromId});
            acctStatObj.RecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMER_ACCOUNT_STATEMENT_OBJECT, 
                                                                            EP_Common_Constant.CUSTOMER_OPEN_ITEM_STATEMENT_RECORD_TYPE_NAME);
            acctStatObj.EP_Overdue_Amount__c = Decimal.valueOf(custAcctStatWrap.totalOverdue.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Current_Amount__c = Decimal.valueOf(custAcctStatWrap.totalCurrent.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Total_Due_Amount__c = Decimal.valueOf(custAcctStatWrap.totalDueAmount.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
            acctStatObj.EP_Closing_Balance__c = NULL;
            acctStatObj.EP_Opening_Balance__c = NULL;
            acctStatObj.EP_Total_Credits__c = NULL;
            acctStatObj.EP_Total_Debits__c = NULL;
            acctStatObj.EP_Total_Orders_In_Progress__c = Decimal.valueOf(custAcctStatWrap.totalOrderInProgress.replace(EP_Common_Constant.COMMA, EP_Common_Constant.BLANK));
        }
        acctStatObj.EP_Bill_To__c = EP_DocumentUtil.getBillToId(custAcctStatWrap.BillTo, custAcctStatWrap.issuedFromId, 
                                                new Set<String>{EP_Common_Constant.RT_BILL_TO_DEV_NAME, EP_Common_Constant.SELL_TO_DEV_NAME});
        acctStatObj.EP_Statement_Pull_Status__c = EP_Common_Constant.COMPLETE;
        acctStatObj.CurrencyIsoCode = custAcctStatWrap.currencyId;
        return acctStatObj;
    }
    
    /**
     *  @Author <Kamal Garg>
     *  @Name <CustomerAccountStatementWrapper>
     *  @CreateDate <04/07/2016>
     *  @Description <This is the wrapper class containing 'Customer Account Statement' record information>
     *  @Version <1.0>
     */
    public with sharing class CustomerAccountStatementWrapper {
        public String BillTo;
        public String IssueDate;
        public String StatementStartDate;
        public String StatementEndDate;
        public String SeqId;
        public String customerName;
        public String type_Z;
        public String issuedFromId;
        public String openingBalance;
        public String totalCredits;
        public String totalDebits;
        public String closingBalance;
        public String totalOrderInProgress;
        public String totalCurrent;
        public String totalOverdue;
        public String totalDueAmount;
        public String currencyId;
    }
    
}