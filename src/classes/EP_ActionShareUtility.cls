/*  
   @Author <Krishan Gopal>
   @name <EP_ActionShareUtility>
   @CreateDate <24/11/2017>
   @Description <This class is Helper class of action trigger>  
   @Version <1.0>
*/
public without sharing class EP_ActionShareUtility {
    public EP_UserCompanyMapper EP_UserCompanyMapperobj = new EP_UserCompanyMapper();
    private static final string EP_Edit_Constant = 'Edit' ;
    
    /**
    * @name         getValidUserList
    * @date         11/26/2017
    * @description  This method is used to fetch users associated to actions and usercompany 
    * @param        List<EP_Action__c> 
    * @return       map<id,list<id>>
    */ 
    public map<id,list<id>> getValidUserList(list<EP_Action__c> actionList){
        EP_GeneralUtility.Log('Public','EP_ActionShareUtility','getValidUserList');
        set<id> companylist = new set<id>();
        for(EP_Action__c EP_Actionobj : actionList){
            companylist.add(EP_Actionobj.EP_Puma_Company__c);
        }
        set<User> AssociatedCompanyUsers = EP_UserCompanyMapperobj.getAssociatedUserList(companylist);
        return setReviewersForAction(AssociatedCompanyUsers,actionList);

    }
    
    /**
    * @name         getReviewProfileMapping
    * @date         11/26/2017
    * @description  This method is create a map for actiontype and profileid
    * @param        NA
    * @return       map<string,string>
    */
    @testvisible
    private map<string,string> getReviewProfileMapping(){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','getReviewProfileMapping');
        map<string,string> reviewprofilemapping = new map<string,string>();
        for(Profile_Review_Mapping__mdt ReviewProfileMappinglist : [SELECT DeveloperName, Profile_Name__c FROM Profile_Review_Mapping__mdt] ){ 
            reviewprofilemapping.put(ReviewProfileMappinglist.DeveloperName,ReviewProfileMappinglist.Profile_Name__c);
        }
        return reviewprofilemapping;
        
    }
    
    /**
    * @name         setReviewersForAction
    * @date         11/26/2017
    * @description  This method is used to map reviewer for each action record
    * @param        set<User> , list<EP_Action__c>
    * @return       map<id,list<id>>
    */
    @testvisible
    private map<id,list<id>> setReviewersForAction(set<User> AssociatedCompanyUsers, list<EP_Action__c> actionList){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','setReviewersForAction');
        map<id,list<id>> ValidReviewersforAction = new map<id,list<id>>();
        for(EP_Action__c EP_Actionobj : actionList){
            ValidReviewersforAction.put(EP_Actionobj.id,getFilteredUsersOnProfile(AssociatedCompanyUsers,EP_Actionobj.Action_Type__c));
        }
        return ValidReviewersforAction;
    }
    
    /**
    * @name         getFilteredUsersOnProfile
    * @date         11/26/2017
    * @description  This method is used to get users based on action type
    * @param        set<User> , string
    * @return      list<id>
    */
    @testvisible
    private list<id> getFilteredUsersOnProfile(set<User> AssociatedCompanyUsers , string ActionType){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','getFilteredUsersOnProfile');
        map<string,string> reviewprofilemapping = getReviewProfileMapping();
        Map<string,string> mapProfile = getProfileNameIdMapping();
        list<id> useridList = new list<id>();
        for(User userobj : AssociatedCompanyUsers){
            if(mapProfile.containskey(reviewprofilemapping.get(ActionType)) && userobj.profileid == mapProfile.get(reviewprofilemapping.get(ActionType))){
                useridList.add(userobj.id);
            }
        }
        return useridList;
    }   
    
    /**
    * @name         shareRecords
    * @date         11/26/2017
    * @description  This method is used to create a sharing records to share action with users
    * @param        map<id,list<id>>
    * @return      NA
    */
    public void shareRecords(map<id,list<id>> reviewUserList){
        EP_GeneralUtility.Log('public','EP_ActionShareUtility','shareRecords');
        list<EP_Action__Share> SharingrecordList = new list<EP_Action__Share>();
        for(id reviewid : ReviewUserList.keySet()){
            createSharingRecords(reviewid,ReviewUserList.get(reviewid),SharingrecordList);
        }
        insert SharingrecordList;
    }
    
    /**
    * @name         createSharingRecords
    * @date         11/26/2017
    * @description  This method is used to create a sharing records to share action with users
    * @param        id,list<id> , list<EP_Action__Share> 
    * @return      NA
    */
    @testvisible
    private void createSharingRecords(id reviewId,list<id> Userlist,list<EP_Action__Share> SharingrecordList){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','createSharingRecords');
        EP_Action__Share EP_ActionShareobj;
        for(id userid :Userlist){
            EP_ActionShareobj = new EP_Action__Share();
            EP_ActionShareobj.ParentId = reviewId;
            EP_ActionShareobj.UserOrGroupId = userid;
            EP_ActionShareobj.AccessLevel = EP_Edit_Constant;
            EP_ActionShareobj.RowCause = Schema.EP_Action__Share.rowCause.EP_Share_ReviewAction__c;
            SharingrecordList.add(EP_ActionShareobj);
        }
        
    }
    
    /**
    * @name         shareReviewRecords
    * @date         11/26/2017
    * @description  This method is used sharing all action records based on company id
    * @param        id , id
    * @return       NA
    */
    public void shareReviewRecords(id Companyid, id userId){
        EP_GeneralUtility.Log('public','EP_ActionShareUtility','shareReviewRecords');
        list<EP_Action__c> actionList = getReviewRecordsforCompanyandUserProfile(companyId,userId);
        list<EP_Action__Share> SharingrecordList = new list<EP_Action__Share>();
        for(EP_action__c actionObj :actionList){
            createSharingRecords(actionObj.id,new list<id> {userId},SharingrecordList);
        }
        database.insert(SharingrecordList,false);
    }
    
    /**
    * @name         getReviewRecordsforCompanyandUserProfile
    * @date         11/26/2017
    * @description  This method is used to fetch action records based on company id and user id
    * @param        id , id
    * @return       list<EP_Action__c>
    */
    @testvisible
    private list<EP_Action__c> getReviewRecordsforCompanyandUserProfile(id Companyid, id userId){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','getReviewRecordsforCompanyandUserProfile');
        map<string,string> profileReviewTypemapping = getProfileReviewTypeMapping();        
        list<EP_Action__c> actionList = new list<EP_Action__c>();
        EP_UserMapper EP_UserMapperobj = new EP_UserMapper();
        string profileName = EP_UserMapperobj.getUserProfileName(userId);         
        string actionType = profileReviewTypemapping.get(profileName);         
        if(actionType != null){
            EP_ActionMapper EP_ActionMapperobj = new EP_ActionMapper();
            actionList = EP_ActionMapperobj.getActionsforUserCompanyAssociation(actionType,Companyid);
        }
        return actionList;
    }
    
    /**
    * @name         getProfileReviewTypeMapping
    * @date         11/26/2017
    * @description  This method is used to create a map for profile id and action type
    * @param        NA
    * @return       map<string,string>
    */
    @testvisible
    private map<string,string> getProfileReviewTypeMapping(){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','getProfileReviewTypeMapping');
        map<string,string> profileReviewTypemapping = new map<string,string>();
        /*Intentional query for custom metadata types Profile_Review_Mapping__mdt as mapper cannot be created for custom metadata*/
        for(Profile_Review_Mapping__mdt ReviewProfileMappinglist : [SELECT DeveloperName, Profile_Name__c FROM Profile_Review_Mapping__mdt] ){
            profileReviewTypemapping.put(ReviewProfileMappinglist.Profile_Name__c,ReviewProfileMappinglist.DeveloperName);
        }
        return profileReviewTypemapping;
        
    }
    
    /**
    * @name         removesharedAccess
    * @date         11/26/2017
    * @description  This method is used to remove sharing from action record 
    * @param        id , id
    * @return       NA
    */
    public void removesharedAccess(id Companyid, id userId){
        EP_GeneralUtility.Log('public','EP_ActionShareUtility','removesharedAccess');
        list<EP_Action__c> actionList = getReviewRecordsforCompanyandUserProfile(companyId,userId);
        delete getSharingrecords(actionList,userId);
    }
    
    /**
    * @name         getSharingrecords
    * @date         11/26/2017
    * @description  This method is used fetch sharing record of action associated to given user
    * @param        list<EP_Action__c>  , id
    * @return       list<EP_Action__Share>
    */
    @testvisible
    private list<EP_Action__Share> getSharingrecords(list<EP_Action__c> actionList , id userId){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','getSharingrecords');
        /*Intentional query for action share record, as mapper cannot be created for this*/
        return [Select id from EP_Action__Share where ParentId in:actionList and UserOrGroupId=:userId and RowCause = :Schema.EP_Action__Share.rowCause.EP_Share_ReviewAction__c ];
        
    }
    
    /**
    * @name         getProfileNameIdMapping
    * @date         11/26/2017
    * @description  This method is used to create  profile name and Profileid  map
    * @param        NA
    * @return       Map<id,string>>
    */  
    @testvisible
    private Map<string,string> getProfileNameIdMapping(){
        EP_GeneralUtility.Log('private','EP_ActionShareUtility','getSharingrecords');
        map<string,string> mapProfileIdName = new map<string,string>();
        for(profile objProfile :[select id ,name from profile]){
            mapProfileIdName.put(objProfile.name,objProfile.id);
        }
        return mapProfileIdName;
    }
    
    
}