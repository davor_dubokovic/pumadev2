@isTest
public class EP_AccountDomainObject_UT
{

    public static final string EVENT_NAME = '01-ProspectTo01-Prospect';
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }

  static testMethod void getStatus_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct.id);
        Test.startTest();
        String result = localObj.getStatus();
        Test.stopTest();
        
        System.assertEquals(result, EP_Common_Constant.STATUS_PROSPECT);
        
    }
    static testMethod void setStatus_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        // **** IMPLEMENT THIS SECTION ~@~ *****
        String accountStatus = EP_Common_Constant.STATUS_PROSPECT;
        
        Test.startTest();
        localObj.setStatus(accountStatus);
        Test.stopTest();
        
        System.assertEquals(acct.EP_Status__c, EP_Common_Constant.STATUS_PROSPECT);
        
    }
    static testMethod void getOldRecord_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldRecord = acct.clone();
        oldRecord.EP_Email__c = 'test@test.com';
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldRecord);
        Test.startTest();
        Account result = localObj.getOldRecord();
        Test.stopTest();
        System.assertEquals(acct.Name, result.Name);
        System.AssertNotEquals(acct.EP_Email__c, result.Ep_Email__c);
    }
    
    static testMethod void getAccount_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        Account result = localObj.getAccount();
        Test.stopTest();
        System.assertEquals(acct.id, result.id);
        
    }
    
    static testMethod void getEventName_getActualEventpath_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAcct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAcct);
        Test.startTest();
        String result = localObj.getEventName();
        Test.stopTest();
        System.assertEquals(result, EVENT_NAME);
    }

    static testMethod void getAccountState_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        EP_AccountState result = localObj.getAccountState(EVENT_NAME);
        Test.stopTest();
        System.assertEquals(EVENT_NAME, result.accountEvent.getEventName());
    }
    
    static testMethod void doActionBeforeInsert_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        localObj.doActionBeforeInsert();
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods
    }
    
    static testMethod void doActionBeforeUpdate_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, acct);
        Test.startTest();
        localObj.doActionBeforeUpdate();
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods  
        
    }
    static testMethod void doActionInvokeAccountStateMachine_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, acct);
        EP_AccountEvent accountEvent = new EP_AccountEvent(localObj.getEventName());
        Test.startTest();
        localObj.doActionInvokeAccountStateMachine(accountEvent);
        Test.stopTest();
        //No Asserts requred , As this methdods delegates to other methods
    }
    
    
    static testMethod void isIntegrationStatusChangedToSync_PositiveScenariotest() {
        Account acct = EP_TestDataUtility.getSellTo();
        acct.EP_Integration_Status__c = EP_Common_Constant.SYNC_STATUS;
        Account oldAccount = acct.clone();
        oldAccount.EP_Integration_Status__c = EP_Common_Constant.ACKNWLDGD;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        Boolean result = localObj.isIntegrationStatusChangedToSync();
        Test.stopTest();
        System.assertEquals(true, result);
    }
    
    static testMethod void isIntegrationStatusChangedToSync_NegativeScenariotest() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        Boolean result = localObj.isIntegrationStatusChangedToSync();
        Test.stopTest();
        System.assertEquals(false, result);
    }
    //#L4 45526 Start
    static testMethod void doActionBeforeDelete_test() {
        Account acct = EP_TestDataUtility.createStockHoldingLocation();
        acct.EP_Status__c =EP_AccountConstant.DEACTIVATE ;
        insert acct;
        EP_AccountDomainObject accountDomainObject = new EP_AccountDomainObject(acct.id);
        Test.startTest();
        accountDomainObject.doActionBeforeDelete();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.   
    }
    //#L4 45526 End
    
    static testMethod void doActionAfterInsert_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        localObj.doActionAfterInsert();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.   
    }
    
    static testMethod void doActionAfterUpdate_test() {
        Account acct = EP_TestDataUtility.getshipTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, acct);
        Test.startTest();
        localObj.doActionAfterUpdate();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.  
    }

    static testMethod void getPriceBook_test() {
        EP_AccountDomainObject localObj = EP_TestDataUtility.getSellToASAccountSetupDomainObject();
        Test.startTest();
        Id result = localObj.getPriceBook();
        Test.stopTest();
        System.assertEquals(result, localObj.localaccount.EP_PriceBook__c);      
    }
    static testMethod void getBillTo_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        Account result = localObj.getBillTo();
        Test.stopTest();    
        System.assertEquals(result.id, acct.EP_Bill_To_Account__c);
        
    }
    static testMethod void setOwnerAlias_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        localObj.setOwnerAlias();
        Test.stopTest();
        String changedCustomerAlias = acct.EP_Cstmr_Owner_Alias__c;
        acct = [Select id, EP_Cstmr_Owner_Alias__c, owner.Alias from account where id = :acct.id];
        System.assertEquals(changedCustomerAlias, acct.owner.Alias);
    }
    static testMethod void isValidCustomer_PositiveScenariotest() {
        EP_AccountDomainObject localObj = EP_TestDataUtility.getSellToWithBillToASActiveDomainObject();
        Test.startTest();
        Boolean result = localObj.isValidCustomer();
        Test.stopTest();
        System.assertEquals(true, result);
    }
    static testMethod void isValidCustomer_NegativeScenariotest() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        Boolean result = localObj.isValidCustomer();
        Test.stopTest();
        System.assertEquals(false,result);
    }
    static testMethod void isBillToAccountChanged_PositiveScenariotest() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        oldAccount.EP_Bill_To_Account__c = null;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        Boolean result = localObj.isBillToAccountChanged();
        Test.stopTest();
        System.assertEquals(true, result);
    }
    static testMethod void isBillToAccountChanged_NegativeScenariotest() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct,acct);
        Test.startTest();
        Boolean result = localObj.isBillToAccountChanged();
        Test.stopTest();
        System.assertEquals(false,result);
    }
    static testMethod void getSupplyLocationPricingList_test() {
        Account acct = EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        LIST<Account> result = localObj.getSupplyLocationPricingList();
        Test.stopTest();
        System.assertEquals(1,result.size());
    }
    static testMethod void getTransporterPricingList_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        LIST<Account> result = localObj.getTransporterPricingList();
        Test.stopTest();
        System.assertEquals(1,result.size());
    }
    
    static testMethod void resetStatus_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        localObj.resetStatus();
        Test.stopTest();
        System.assertEquals(EP_AccountConstant.BASICDATASETUP, new EP_AccountMapper().getAccountRecord(acct.id).EP_Status__c);      
    }  
       
    static testMethod void isStatusChanged_PositiveScenariotest() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        boolean result = localObj.isStatusChanged();
        Test.stopTest();
        System.assertEquals(true, result);      
    }
    
    static testMethod void isStatusChanged_NegativeScenariotest() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        boolean result= localObj.isStatusChanged();
        Test.stopTest();
        System.assertEquals(false, result);      
    }
    
    static testMethod void getEventName_IntegrationEventPath_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        acct.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        acct.EP_SENT_TO_NAV_WINDMS__c = true;
        acct.EP_Synced_PE__c = true;
        acct.EP_Synced_NAV__c = true;
        acct.EP_Integration_Status__c = EP_AccountConstant.BASICDATASETUP;
        update acct;
        EP_AccountDomainObject localObj1 = new EP_AccountDomainObject(acct.id);
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(localObj1.localAccount,oldAccount);
        Test.startTest();
        String result = localObj.getEventName();
        Test.stopTest();
        
        System.assertEquals(result, '01-ProspectTo04-AccountSet-up');
        
    }

    static testMethod void getEventName_DummyEventPath_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        acct.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        acct.EP_Is_Dummy__c = true;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        String result = localObj.getEventName();
        Test.stopTest();
        
        System.assertEquals(result, EP_AccountConstant.DUMMY_BASICDATASETUP_TO_ACTIVE);
        
    }
    static testMethod void doActionBeforeUpdate_exceptionpath_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        acct.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        localObj.doActionBeforeUpdate();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.   
        
    }
    
    static testMethod void doActionBeforeUpdate_eventerrorpath_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        acct.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        localObj.doActionBeforeUpdate();
        Test.stopTest();        
        //No Assert Required, as this method delegates other methods.  
    }

    static testMethod void doActionBeforeInsertEventError_test() {
        Account acct = EP_TestDataUtility.getSellToWithWrongPricebook();
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct);
        Test.startTest();
        localObj.doActionBeforeInsert();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods. 
    }
    
    static testMethod void doActionBeforeInsertEvent_exceptionpath_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        Account oldAccount = acct.clone();
        acct.EP_Status__c = EP_AccountConstant.ACCOUNTSETUP;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(acct, oldAccount);
        Test.startTest();
        localObj.doActionBeforeInsert();
        Test.stopTest();
        //No Assert Required, as this method delegates other methods.   
    }

// 45361 and 45362 start
    static testMethod void isUnblocked_test(){
        Account newAccount = EP_TestDataUtility.getSellTo();
        newAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update newAccount;
        Account oldAccount = EP_TestDataUtility.getSellTo();
        oldAccount.EP_Status__c = EP_AccountConstant.BLOCKED;
        oldAccount.EP_Reason_Blocked__c  = EP_Common_Constant.MAINTENANCE;
        update oldAccount;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(newAccount, oldAccount);
        Test.startTest();
        Boolean result = localObj.isUnblocked();
        Test.stopTest();
        system.assert(result);
    }
    static testMethod void isblocked_test(){
        Account newAccount = EP_TestDataUtility.getSellTo();
        newAccount.EP_Status__c = EP_AccountConstant.BLOCKED;
        newAccount.EP_Reason_Blocked__c  = EP_Common_Constant.MAINTENANCE;
        update newAccount;
        Account oldAccount = EP_TestDataUtility.getSellTo();
        oldAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        oldAccount.EP_Reason_Blocked__c  = null;
        update oldAccount;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(newAccount, oldAccount);
        Test.startTest();
        Boolean result = localObj.isblocked();
        Test.stopTest();
        system.assert(result);
    }
// 45361 and 45362 end
    
  // Changes made for CUSTOMER MODIFICATION L4 Start  
  static testMethod void isUpdatedFromNAV_test(){
        Account newAccount = EP_TestDataUtility.getSellTo();
        newAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        newAccount.EP_Source_SEQ_ID__c = 'testnewId';
        update newAccount;
        Account oldAccount = EP_TestDataUtility.getSellTo();
        oldAccount.EP_Source_SEQ_ID__c = 'testoldId';
        update oldAccount;
        EP_AccountDomainObject localObj = new EP_AccountDomainObject(newAccount, oldAccount);
        Test.startTest();
        Boolean result = localObj.isUpdatedFromNAV();
        Test.stopTest();
        system.assert(result);
  }
  // Changes made for CUSTOMER MODIFICATION L4 End

}