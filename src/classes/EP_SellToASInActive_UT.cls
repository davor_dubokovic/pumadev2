@isTest
public class EP_SellToASInActive_UT{
    static final string EVENT_NAME = '07-InactiveTo07-Inactive';
    static final string INVALID_EVENT_NAME = '08-RejectedTo02-BasicDataSetup';
    
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_SellToASInActive localObj = new EP_SellToASInActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASInActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        System.AssertEquals(obj,localObj.account);
        
    }
    static testMethod void doOnEntry_test() {
        EP_SellToASInActive localObj = new EP_SellToASInActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASInActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //As this methdods delegates to other methods,hence adding a dummy assert.
        system.Assert(true);
    }
    static testMethod void doOnExit_test() {
        EP_SellToASInActive localObj = new EP_SellToASInActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASInActiveDomainObject();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        //Added Dummy Asserts , As this methdods does not have processing logic.
        system.Assert(true);
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_SellToASInActive localObj = new EP_SellToASInActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASInActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_SellToASInActive localObj = new EP_SellToASInActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASInActiveDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        try{Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        }catch(Exception ex){
            System.AssertEquals('AccountStateMachineException',ex.getTypeName());
        }
    }
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_SellToASInActive localObj = new EP_SellToASInActive();
        EP_AccountDomainObject obj = EP_TestDataUtility.getSellToASInActiveDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
}