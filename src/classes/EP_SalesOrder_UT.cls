@isTest
public class EP_SalesOrder_UT
{
    
    static testMethod void isValidLoadingDate_positivetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(ord);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isValidLoadingDate_negativetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidLoadingDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidOrderDate_positivetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isValidOrderDate_negativetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getApplicableDeliveryTypes_returnDeivery_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        List<String> result = localObj.getApplicableDeliveryTypes();
        Set<String> resultSet = new Set<String>();
        resultSet.addAll(result);
        Test.stopTest();
        System.AssertEquals(true,resultSet.contains(EP_Common_Constant.Delivery));
    }
    static testMethod void getApplicableDeliveryTypes_returnExrack_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        csord__Order__c Orderobj = EP_TestDataUtility.getExRackOrder();
        EP_OrderDomainObject obj = new EP_OrderDomainObject(Orderobj);
        localObj.setOrderDomain(obj);
        Test.startTest();
        List<String> result = localObj.getApplicableDeliveryTypes();
        Set<String> resultSet = new Set<String>();
        resultSet.addAll(result);
        Test.stopTest();
        System.AssertEquals(true,resultSet.contains(EP_Common_Constant.EX_RACK));
    }
    static testMethod void getApplicablePaymentTerms_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Account accountObject = new EP_accountMapper().getAccountRecord(obj.getOrder().accountid__c);
        Test.startTest();
        List<String> result = localObj.getApplicablePaymentTerms(accountObject);
        Test.stopTest();
        Set<String> resultSet = new Set<String>();
        resultSet.addAll(result);
        System.AssertEquals(true,resultSet.contains(EP_Common_Constant.PREPAYMENT));
    }
    static testMethod void doSubmitActions_test() {
        List<EP_Customer_Support_Settings__c> lCustomerSupportSettings = Test.loadData(EP_Customer_Support_Settings__c.sObjectType, 'EP_Customer_Support_Settings');
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Account acc = [select id, EP_Bill_To_Account__c,EP_Payment_Term_Lookup__r.name from Account where id=: ord.AccountId__c];
        Account billTo = [select id, EP_Payment_Term_Lookup__r.name from Account where id=: acc.EP_Bill_To_Account__c];
        billTo.EP_Payment_Term_Lookup__c= acc.EP_Payment_Term_Lookup__c;
        update billTo;
        Test.startTest();
        localObj.doSubmitActions();
        Test.stopTest();
        System.assertNotEquals(ord.EP_Payment_Method__c,null);
        System.assertNotEquals(ord.EP_Payment_Method__c,null);
    }
    static testMethod void setOrderDomain_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        localObj.setOrderDomain(obj);
        Test.stopTest();
        System.AssertEquals(obj,localObj.orderDomainObj);
    }
    static testMethod void doUpdatePaymentTermAndMethod_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Account acc = [select id, EP_Bill_To_Account__c,EP_Payment_Term_Lookup__r.name from Account where id=: ord.AccountId__c];
        Account billTo = [select id, EP_Payment_Term_Lookup__r.name from Account where id=: acc.EP_Bill_To_Account__c];
        billTo.EP_Payment_Term_Lookup__c= acc.EP_Payment_Term_Lookup__c;
        update billTo;
        Test.startTest();
        localObj.doUpdatePaymentTermAndMethod();
        Test.stopTest();
        System.assertNotEquals(ord.EP_Payment_Method__c,null);
        System.assertNotEquals(ord.EP_Payment_Method__c,null);
    }
    
    static testMethod void getShipTos_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Id accountId = obj.getOrder().AccountId__c;
        Test.startTest();
        List<Account> result = localObj.getShipTos(accountId);
        Test.stopTest();
        System.AssertEquals(true,result.size() == 1);
    }
    static testMethod void getOperationalTanks_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Id accountId = obj.getOrder().EP_ShipTo__c;
        Test.startTest();
        Map<Id, EP_Tank__c> result = localObj.getOperationalTanks(accountId);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    
    static testMethod void loadStrategies_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        localObj.loadStrategies(obj);
        Test.stopTest();
        System.AssertNotEquals(Null,localObj.consignmentType);
    }
    static testMethod void getAllRoutesOfShipToAndLocation_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c orderObj = [Select EP_ShipTo__c,EP_Stock_Holding_Location__c,EP_ShipTo__r.RecordType.DeveloperName from csord__Order__c where Id=:obj.getOrder().Id];
        Id shipToId = orderObj.EP_ShipTo__c;
        Id stockholdinglocationId = orderObj.EP_Stock_Holding_Location__c;
        system.debug('---'+[Select id from EP_route__C]);
        Test.startTest();
        List<EP_Route__c> result = localObj.getAllRoutesOfShipToAndLocation(shipToId,stockholdinglocationId);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    static testMethod void showCustomerPO_postivetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.showCustomerPO();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void showCustomerPO_negativetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.showCustomerPO();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void findRecordType_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Id result = localObj.findRecordType();
        Test.stopTest();
        Id vmiOrdersId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);
        System.AssertEquals(vmiOrdersId,result);
    }
    
    static testMethod void getDeliveryTypes_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        List<String> result = localObj.getDeliveryTypes();
        Test.stopTest();
        System.AssertEquals(2,result.size());
       
    }
    
    static testMethod void findPriceBookEntries_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);

        Account accObj = [SELECT Id,EP_Pricebook__c FROM Account WHERE Id =:obj.getOrder().AccountId__c LIMIT 1];
        Id pricebookId = accObj.EP_Pricebook__c;

        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        List<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,ord);
        Test.stopTest();
        System.AssertEquals(true,result.size() > 0);
    }
    
    static testMethod void setRequestedDateTime_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        String selectedDate = '25-03-2017';
        String availableSlots = '11:30:21';
        Test.startTest();
        csord__Order__c result = localObj.setRequestedDateTime(ord,selectedDate,availableSlots);
        Test.stopTest();
        //retrospective order does not have any delivery date set functionality so no value shall be set on this. 
        System.AssertEquals(null ,result.EP_Requested_Delivery_Date__c);
        
    }
    static testMethod void calculatePrice_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        localObj.calculatePrice();
        Test.stopTest();
        List<EP_IntegrationRecord__c> integRecords = [select id,EP_Error_Description__c,EP_Status__c,EP_SeqId__c from EP_IntegrationRecord__c];
        system.assertNotEquals(0, integRecords.size());
                
        
    }
    
    static testMethod void checkForNoTransporter_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        String transporterName = EP_Common_Constant.NO_TRANSPORTER;
        Test.startTest();
        csord__Order__c result = localObj.checkForNoTransporter(transporterName);
        Test.stopTest();
        System.AssertEquals(EP_Common_Constant.PIPELINE,obj.getOrder().EP_Delivery_Type__c );
        
    }
    static testMethod void setRetroOrderDetails_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        localObj.setRetroOrderDetails();
        Test.stopTest();
        System.AssertEquals(EP_PortalLibClass_R1.returnLocalDate(Date.valueOf(obj.getOrder().EP_Loading_Date__c)),obj.getOrder().EP_Expected_Loading_Date__c );
        DateTime dT = obj.getOrder().EP_Expected_Delivery_Date__c;
        Datetime deliveryDate = dateTime.newinstance(dT.year(), dT.month(), dT.day(),0,0,0);
        System.AssertEquals(EP_GeneralUtility.convertDateTimeToDecimal(String.valueOf(deliveryDate)),obj.getOrder().EP_Actual_Delivery_Date_Time__c );
    }
    static testMethod void isOrderEntryValid_positivetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        List<Account> listTransportAccount = [SELECT Id FROM Account WHERE id=:obj.getOrder().EP_Transporter__c];
        Integer numOfTransportAccount = listTransportAccount.size();
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isOrderEntryValid_negativetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        List<Account> listTransportAccount = [SELECT Id FROM Account WHERE id=:obj.getOrder().EP_Transporter__c];
        Integer numOfTransportAccount = listTransportAccount.size();
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void setConsumptionOrderDetails_test() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        localObj.setOrderDomain(obj);
        Test.startTest();
        localObj.setConsumptionOrderDetails();
        Test.stopTest();
        System.AssertEquals(Null,obj.getOrder().EP_Actual_Delivery_Date_Time__c);
        
    }
    
    
    static testMethod void hasCreditIssue_negativetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.hasCreditIssue();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void isValidExpectedDate_positivetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectNegativeScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void isValidExpectedDate_negativetest() {
        EP_SalesOrder localObj = new EP_SalesOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        csord__Order__c ord = obj.getOrder();
        Test.startTest();
        Boolean result = localObj.isValidExpectedDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    static testMethod void Constructor_test() {
        EP_OrderDomainObject obj = EP_TestDataUtility.getSalesOrderDomainObject();
        Test.startTest();
        EP_SalesOrder localObj = new EP_SalesOrder(obj);
        Test.stopTest();
        System.AssertEquals(true, localObj.epochType instanceof EP_Retrospective);
    }
     
}