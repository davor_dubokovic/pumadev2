public with sharing class EP_CutOffMatrixHelper {

    public List<DeliveryDay__c> deliveryDays {get;set;} // delivery days to be rendered on edit matrix page

    //public List<DeliveryDay__c> deliveryDaysRender {get;set;}

    public CutOffMatrix__c cutoffMatrix {get;set;}

    public List<CutOffMatrix__c> cutoffMatrices {get;set;}

    public List<DeliveryDay__c> deliveryDayList {get;set;} // all delivery days related to matrix

    public List<DeliveryDay__c> fullDeliveryDayList {get;set;} // all delivery days related to all matrices

    public List<Overload_matrix_entry__c> overrideMatrixEntries {get;set;}

    private Account account;

    public EP_CutOffMatrixHelper() {
    }

    public EP_CutOffMatrixHelper(Id accid) {
        // select matrix from matrices
        // get all matrices
        this.cutoffMatrix = getCutOffMatrixByAccountId(accid);

        if (this.cutoffMatrix == null) {
            return;
        }

        // get delivery days for all matrices
        this.deliveryDayList = getDeliveryDaysByMatrixId(this.cutoffMatrix.id);
        this.overrideMatrixEntries = getOverrideMatrixEntries(this.cutoffMatrix.Override_Matrix__c);//(this.account.Overload_matrix__c);
    }

    public EP_CutOffMatrixHelper(Id accid, Id pumaCompany, Id siteLocation, String deliveryType) {
        // select matrix from matrices
        // get all matrices
        this.cutoffMatrix = getCutOffMatrixByCompanySiteDelivery(accid, pumaCompany, siteLocation, deliveryType);
        system.debug('**this.cutoffMatrix.Override_Matrix__c'+this.cutoffMatrix.Override_Matrix__c);
        if (this.cutoffMatrix == null) {
            return;
        }
        
        // get delivery days for all matrices
        this.deliveryDayList = getDeliveryDaysByMatrixId(this.cutoffMatrix.id);
        this.overrideMatrixEntries = getOverrideMatrixEntries(this.cutoffMatrix.Override_Matrix__c);//(this.account.Overload_matrix__c);
    }

    public void initializeCutoffMatrixHelper(Id matrixId) {
        this.cutoffMatrix = getCutOffMatrixById(matrixId); 

        // if there is no cutoffmatrix for this company, create one
        if (this.cutoffMatrix == null) {
            this.cutoffMatrix = new CutOffMatrix__c();
            this.deliveryDayList = createDeliveryDaysList();
            return;
        }
        this.deliveryDayList = getDeliveryDaysByMatrixId(matrixId);
        this.overrideMatrixEntries = getOverrideMatrixEntries(this.cutoffMatrix.Override_Matrix__c);//(matrixId);
    }

    // convert Day name into Day number
    public Integer getTodayNum(String dayName) {
        System.debug('EP_CutOffMatrixHelper getTodayNum, dayName = ' + dayName);
            if (dayName.equals('Monday')) {
                return 1;
            } else if (dayName.equals('Tuesday')) {
                return 2;
            } else if (dayName.equals('Wednesday')) {
                return 3;
            } else if (dayName.equals('Thursday')) {
                return 4;
            } else if (dayName.equals('Friday')) {
                return 5;
            } else if (dayName.equals('Saturday')) {
                return 6;
            } else if (dayName.equals('Sunday')) {
                return 7;
            }

            return 0;
    }

    // convert day number into day name
    public String getDayName(Integer dayNumber) {
        System.debug('EP_CutOffMatrixHelper getDayName, dayNumber = ' + dayNumber);
            if (dayNumber == 1) {
                return 'Monday';
            } else if (dayNumber == 2) {
               return 'Tuesday';
            } else if (dayNumber == 3) {
                return 'Wednesday';
            } else if (dayNumber == 4) {
                return 'Thursday';
            } else if (dayNumber == 5) {
                return 'Friday';
            } else if (dayNumber == 6) {
                return 'Saturday';
            } else if (dayNumber == 7) {
                return 'Sunday';
            }

            return 'Monday';
    }

    // get cutoff time by delivery day name
    public Time getCutoffTime(String dayName, DeliveryDay__c dDay) {
        System.debug('EP_CutOffMatrixHelper getCutoffTime, dayName = ' + dayName + ', dDay = ' + dDay);
            if (dayName.equals('Monday')) {
                return dDay.Monday__c;
            } else if (dayName.equals('Tuesday')) {
                return dDay.Tuesday__c;
            } else if (dayName.equals('Wednesday')) {
                return dDay.Wednesday__c;
            } else if (dayName.equals('Thursday')) {
                return dDay.Thursday__c;
            } else if (dayName.equals('Friday')) {
                return dDay.Friday__c;
            } else if (dayName.equals('Saturday')) {
                return dDay.Saturday__c;
            } else if (dayName.equals('Sunday')) {
                return dDay.Sunday__c;
            }

            return dDay.Monday__c;
    }

    public List<DeliveryDay__c> createDeliveryDaysList() {
        List<DeliveryDay__c> ddList = new List<DeliveryDay__c>();

        for (Integer i = 1; i < 8; i++) {
            ddlist.add(new DeliveryDay__c(Order_Day_Name__c = getDayName(i), OrderDayNum__c = i));
        }

        return ddList;
    }

    // get all delivery days in cutoff matrix
    public List<DeliveryDay__c> getDeliveryDaysByMatrixId(Id cutOffMatrixId) {
        System.debug('EP_CutOffMatrixHelper getDeliveryDaysByMatrixId, cutOffMatrixId = ' + cutOffMatrixId);
        List<DeliveryDay__c> dd = null;

        try {
            Integer orderDay = getTodayNum(DateTime.now().format('EEEE'));

            System.debug('getDeliveryDaysByMatrixId orderDay = ' + orderDay);

            dd =   [select id, OrderDayNum__c, Order_Day_Name__c,  Monday__c, Tuesday__c, Wednesday__c, Thursday__c, 
                        Friday__c, Saturday__c, Sunday__c, Delivery_Type__c, Site_Location__c, Site_Location__r.id, Cut_Off_Matrix__c
                            from DeliveryDay__c 
                                where Cut_Off_Matrix__c = :cutOffMatrixId ORDER BY OrderDayNum__c];
        } catch (Exception e) {
            System.debug('getDeliveryDaysByMatrixId exception = ' + e.getMessage());
        }
        return dd;
    }

    public List<DeliveryDay__c> getDeliveryDaysByTerminalAndDeliveryType() {
        System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType');
        List<DeliveryDay__c> ddList = new List<DeliveryDay__c>();

        if (cutoffMatrix == null || deliveryDayList == null) {
            return null;
        }

        System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType not null');
        System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType cutoffMatrix = ' + cutoffMatrix);
        System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType deliveryDayList = ' + deliveryDayList);

        for (DeliveryDay__c dd : deliveryDayList) {
            System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType dd = ' + dd);
            System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType dd.Site_Location__c = ' + dd.Site_Location__c);
            System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType dd.Delivery_Type__c = ' + dd.Delivery_Type__c);
            System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType cutoffMatrix.Delivery_Type__c = ' + cutoffMatrix.Delivery_Type__c);
            System.debug('EP_CutOffMatrixHelper getDeliveryDaysByTerminalAndDeliveryType cutoffMatrix.Site_Location__c = ' + cutoffMatrix.Site_Location__c);
            
            ddList.add(dd);
        }

        return ddList;
    }

    public CutOffMatrix__c getCutOffMatrixById(Id comId) {
        CutOffMatrix__c com = null;
        try {
        com = [select id, Name, Delivery_Type__c, Puma_Company__c, Shift__c, Site_Location__c, Time_Zone__c, Override_Matrix__c
                                from CutOffMatrix__c where id = :comId];
        } catch (QueryException e) {
            System.debug('getCutOffMatrixById QueryException = ' + e.getMessage());
        }

        //deliveryDays = getDeliveryDaysByMatrixId(comId);

        System.debug('getCutOffMatrixById com = ' + com);

        return com;
    }

    private  CutOffMatrix__c getCutOffMatrixByAccountId(Id accid) {

        account = [select EP_Puma_Company__r.Cut_Off_Matrix__c from Account where id = :accid]; //, Overload_matrix__c

        CutOffMatrix__c com = [select id, Name, Delivery_Type__c, Puma_Company__c, Shift__c, Site_Location__c, Time_Zone__c, Override_Matrix__c
                                from CutOffMatrix__c where id = :account.EP_Puma_Company__r.Cut_Off_Matrix__c];

        System.debug('getCutOffMatrixByAccountId com = ' + com);

        return com;
    }

    private  CutOffMatrix__c getCutOffMatrixByCompanySiteDelivery(Id accid, Id pumaCompany, Id siteLocation, String deliveryType) {

        account = [select EP_Puma_Company__r.Cut_Off_Matrix__c, EP_Advance_Order_Entry_Days__c, EP_Puma_Company__c, 
                    EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c from Account where id = :accid];
        EP_Stock_Holding_Location__c supplyLocation = [select Stock_Holding_Location__c from EP_Stock_Holding_Location__c where id = :siteLocation];

        System.debug('getCutOffMatrixByCompanySiteDelivery supplyLocation = ' + supplyLocation);
        System.debug('getCutOffMatrixByCompanySiteDelivery supplyLocation.Stock_Holding_Location__c = ' + supplyLocation.Stock_Holding_Location__c);

        CutOffMatrix__c com = null;
        //PS:22/02/2018 Modify logic to avoid Query Exception-- Start
        List<CutOffMatrix__c> coms = new List<CutOffMatrix__c>();

        coms = [select id, Name, Delivery_Type__c, Puma_Company__c, Shift__c, Site_Location__c, Time_Zone__c, Override_Matrix__c
                                from CutOffMatrix__c where Puma_Company__c = :pumaCompany AND Delivery_Type__c = 'Any' AND Site_Location__c = :supplyLocation.Stock_Holding_Location__c limit 1];
        
        if (coms != null && !coms.isEmpty()) {
            System.debug('getCutOffMatrixByCompanySiteDelivery any com = ' + coms);
            com = coms[0];
            return com;
        }
        System.Debug('deliveryType -->'+deliveryType + 'stock-->'+supplyLocation.Stock_Holding_Location__c);
        coms = [select id, Name, Delivery_Type__c, Puma_Company__c, Shift__c, Site_Location__c, Time_Zone__c, Override_Matrix__c
                                from CutOffMatrix__c where Puma_Company__c = :pumaCompany AND Delivery_Type__c = :deliveryType AND Site_Location__c = :supplyLocation.Stock_Holding_Location__c limit 1];
        if (coms != null && !coms.isEmpty()) {
            com = coms[0];
        }                
        System.debug('getCutOffMatrixByCompanySiteDelivery done com = ' + coms);
        //PS:22/02/2018 End
        return com;
    }

    // get Day of the week name from date
    public String getDayOfTheWeek(String deliveryDate) {
        System.debug('getDayOfTheWeek deliveryDate = ' + deliveryDate);

        // deliveryDate = MM/DD/YYYY
        List<String> dd = deliveryDate.split('/');
        DateTime d = DateTime.newInstance(Integer.valueOf(dd[2]), Integer.valueOf(dd[0]), Integer.valueOf(dd[1]));

        String deliveryDay = d.format('EEEE');
        System.debug('getDayOfTheWeek deliveryDay = ' + deliveryDay);

        return deliveryDay;
    }

    // shift time from GMT+localGMTOffset to GMT+time specified in cutoff matrix
    public DateTime shiftTimeByGmtOffset(DateTime day, String localGMTOffset) {
        DateTime shiftDay = day;

        // if cutoff matrix is not defined do not check anything
        if (cutoffMatrix != null) {
            // get cutoff matrix GMT offset
            List<String> comGmtOffsetList = cutoffMatrix.Time_Zone__c.split(' ');

            // if GMT is selected set the offset to be 0 (GMT + 0)
            if (comGmtOffsetList == null || comGmtOffsetList.size() < 3) {
                comGmtOffsetList = new List<String>();
                comGmtOffsetList.add('GMT');
                comGmtOffsetList.add('+');
                comGmtOffsetList.add('00:00');
            }
            List<String> comHourMinuteOffsetList = comGmtOffsetList.get(2).split(':');

            // get local GMT offset
            Integer localoffsetMinutes = Integer.valueOf(localGMTOffset);
            Integer localHourOffset = localoffsetMinutes / 60;
            localoffsetMinutes = Math.mod(localoffsetMinutes, 60);

            // calculate difference between local gmt offset and cutoff matrix gmt offset as MatrixGMTOffset - LocalGMTOffset
            Integer totalHourOffset = Integer.valueOf(comGmtOffsetList.get(1) + comHourMinuteOffsetList.get(0)) - localHourOffset;
            Integer totalMinuteOffset = Integer.valueOf(comGmtOffsetList.get(1) + comHourMinuteOffsetList.get(1)) - localoffsetMinutes;

            // translate local time into cutoff matrix timezone by adding calculated minutes and hours to local time
            shiftDay = shiftDay.addHours(totalHourOffset);
            shiftDay = shiftDay.addMinutes(totalMinuteOffset);
        }   

        return shiftDay;

    }

    public Time getCutOffTimeByAccountId(Id accountId, String orderDate, String orderTime, String deliveryDate, String localGMTOffset) {

        Time cot = null;
        CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();

        System.debug('getCutOffTimeByAccountId accountId = ' + accountId);
        System.debug('getCutOffTimeByAccountId orderDate = ' + orderDate);
        System.debug('getCutOffTimeByAccountId orderTime = ' + orderTime);
        System.debug('getCutOffTimeByAccountId deliveryDate = ' + deliveryDate);
        System.debug('getCutOffTimeByAccountId localGMTOffset = ' + localGMTOffset);

        if (orderDate.equals('') || deliveryDate.equals('')) {
            return cot;
        }

        // orderTime = 23:45:26
        // orderDate = 02/28/2004

        List<String> localTimeList = orderTime.split(':');
        List<String> localDateList = orderDate.split('/');
        List<String> dueDateList = deliveryDate.split('/');

        Date startDate = Date.newInstance(Integer.valueOf(localDateList[2]), // year
                                          Integer.valueOf(localDateList[0]), // month
                                          Integer.valueOf(localDateList[1])); //day

        Date endDate = Date.newInstance(Integer.valueOf(dueDateList[2]), // year
                                        Integer.valueOf(dueDateList[0]), // month
                                        Integer.valueOf(dueDateList[1])); //day

        // you cannot make order for more than 6 days in advance by default
        Integer matrixDepth = Integer.valueOf(csOrderSetting.CutoffMatrixDefaultDepth__c);
        if (account != null) {
            if (account.EP_Advance_Order_Entry_Days__c != null) {
                matrixDepth = matrixDepth + Integer.valueOf(account.EP_Advance_Order_Entry_Days__c);
            } else if (account.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c != null) {
                matrixDepth = matrixDepth + Integer.valueOf(account.EP_Puma_Company__r.EP_Advance_Order_Entry_Days__c);
            }
        }

        if (startDate.daysBetween(endDate) > matrixDepth) {
            return cot;
        }

        // check if overload matrix exist
        cot = getOverloadCutOffTime(accountId, orderDate, deliveryDate);
        System.Debug('getCutOffTimeByAccountId getOverloadCutOffTime == ' + cot);

        // if it exists return time from cutoffmatrix for this delivery date
        if (cot != null) {
            return cot;
        }
        

        if (deliveryDayList == null) {
            return cot;
        }

        for (DeliveryDay__c dd : deliveryDayList) {
            if (dd.OrderDayNum__c == getTodayNum(getDayOfTheWeek(orderDate))) {
                cot = getCutoffTime(getDayOfTheWeek(deliveryDate), dd);
                break;
            }
        }

        return cot;
    }

    public List<Overload_matrix_entry__c> getOverrideMatrixEntries(Id overrideMatrixId) {
        List<Overload_matrix_entry__c> overloadTimes = [select Time__c, Date__c, Order_Date__c from Overload_matrix_entry__c 
                                                        where Overload_matrix__c = :overrideMatrixId];
        return overloadTimes; 
    }

    public Time getOverloadCutOffTime(Id accountId, String orderDate, String deliveryDate) {
        Time cot = null;

        // if Overload matrix does not exist no need for further checks
        if (overrideMatrixEntries == null) {
            return cot;
        }

        // deliveryDate = MM/DD/YYYY
        List<String> dd = deliveryDate.split('/');
        Date delivery = Date.newInstance(Integer.valueOf(dd[2]), Integer.valueOf(dd[0]), Integer.valueOf(dd[1]));

        List<String> od = orderDate.split('/');
        Date order = Date.newInstance(Integer.valueOf(od[2]), Integer.valueOf(od[0]), Integer.valueOf(od[1]));

        // if Overload matrix exists get time for delivery date
        for (Overload_matrix_entry__c entry : overrideMatrixEntries) {
            System.Debug('getOverloadCutOffTime entry.Date__c == ' + entry.Date__c);
            System.Debug('getOverloadCutOffTime entry.Time__c == ' + entry.Time__c);
            System.Debug('getOverloadCutOffTime delivery == ' + delivery);
            if (entry.Date__c == delivery) {

                if (entry.Order_Date__c != null && 
                    entry.Order_Date__c == order && 
                    entry.Time__c != null) {
                    cot = entry.Time__c;
                    break;
                }

                //cot = entry.Time__c;
                // if override entry exists but time is not defined it means no order can be made for this day
                // create time of (HH:MM:SS.sss) 01:01:01.111
                // these one millisecond swill indicate that no order can be made for this day because user cannot manually set milliseconds
                // milliseconds are indicator of "banned" order date
                if (entry.Time__c == null) {
                    cot = Time.newInstance(1, 1, 1, 111);
                    break;
                }
                //break;
            }
        }

        return cot;
    }

    public Boolean checkCutOffTimeOk(Time cutOffT, String localTime, String localDate, String localGMTOffset) {

        System.debug('checkCutOffTimeOk  localGMTOffset = ' + localGMTOffset);
        System.debug('checkCutOffTimeOk  cutoffMatrix = ' + cutoffMatrix);

        // localTime = 23:45:26
        // localDate = 02/28/2004

        List<String> localTimeList = localTime.split(':');
        List<String> localDateList = localDate.split('/');

        DateTime today = DateTime.newInstance(Integer.valueOf(localDateList[2]), // year
                                              Integer.valueOf(localDateList[0]), // month
                                              Integer.valueOf(localDateList[1]), //day
                                              Integer.valueOf(localTimeList[0]), //hour
                                              Integer.valueOf(localTimeList[1]), // minute
                                              Integer.valueOf(localTimeList[2])); // second

        today = shiftTimeByGmtOffset(today, localGMTOffset);

        Long now = today.getTime();
        Long cot = DateTime.newInstance(today.year(), today.month(), today.day(), cutOffT.hour(), cutOffT.minute(), cutOffT.second()).getTime();
        //Long cot = DateTime.newInstance(Integer.valueOf(localDateList[2]), Integer.valueOf(localDateList[0]), Integer.valueOf(localDateList[1]), cutOffT.hour(), cutOffT.minute(), cutOffT.second()).getTime();

         System.debug('checkCutOffTimeOk now = ' + now);
         System.debug('checkCutOffTimeOk cot = ' + cot);

         if (now > cot) {
            return false;
         }

         return true;
    }
}