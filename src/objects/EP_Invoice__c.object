<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ePuma - This object is used to capture the invoice information from Nav.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Account_Nr__c</fullName>
        <externalId>false</externalId>
        <formula>EP_Bill_To__r.AccountNumber</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Nr</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Bill_To__c</fullName>
        <description>Lookup to active Sell-To/ Bill-To account</description>
        <externalId>false</externalId>
        <label>Bill-To</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>(1 OR 3) AND 2</booleanFilter>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_Sell_To</value>
            </filterItems>
            <filterItems>
                <field>Account.EP_Status__c</field>
                <operation>equals</operation>
                <value>05-Active</value>
            </filterItems>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_Bill_To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Customer Invoices (Bill-To)</relationshipLabel>
        <relationshipName>Invoices</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_CSOrder__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>csord__Order__c</referenceTo>
        <relationshipLabel>Customer Invoices</relationshipLabel>
        <relationshipName>Customer_Invoices</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Credit_Memo__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Credit Memo</label>
        <referenceTo>EP_Customer_Credit_Memo__c</referenceTo>
        <relationshipLabel>Customer Invoices</relationshipLabel>
        <relationshipName>Customer_Invoices</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_CurrencyISOCode__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(CurrencyIsoCode)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Currency</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Dispute_Reason_Code__c</fullName>
        <externalId>false</externalId>
        <label>Dispute Reason Code</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Dispute_Reason_Description__c</fullName>
        <externalId>false</externalId>
        <label>Dispute Reason Description</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>EP_Entry_Number__c</fullName>
        <externalId>false</externalId>
        <label>Entry Number</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Invoice_Amount__c</fullName>
        <description>Total Invoice Amount</description>
        <externalId>false</externalId>
        <label>Invoice Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>3</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Invoice_Due_Date__c</fullName>
        <description>Field to store Invoice Due Date</description>
        <externalId>false</externalId>
        <label>Invoice Due Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Invoice_Issue_Date__c</fullName>
        <description>Field to store Invoice Issue Date</description>
        <externalId>false</externalId>
        <label>Invoice Issue Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Invoice_Key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Value generated on the fly:
Bill-To + “_” + Invoice Number + “_” + Invoice Issue Date + “_” + Invoice Due Date</description>
        <externalId>true</externalId>
        <inlineHelpText>Value generated on the fly:
Bill-To + “_” + Invoice Number + “_” + Invoice Issue Date + “_” + Invoice Due Date</inlineHelpText>
        <label>Customer Invoice Key</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Invoice_Pull_Status__c</fullName>
        <description>Default value is “Incomplete”</description>
        <externalId>false</externalId>
        <label>Invoice Pull Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Incomplete</fullName>
                    <default>true</default>
                    <label>Incomplete</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Invoice_Total__c</fullName>
        <externalId>false</externalId>
        <label>Invoice Total</label>
        <precision>17</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Invoice_Type__c</fullName>
        <description>Values: {Final Invoice, Proforma}
Default value is set to Final Invoice</description>
        <externalId>false</externalId>
        <label>Invoice Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Final Invoice</fullName>
                    <default>true</default>
                    <label>Final Invoice</label>
                </value>
                <value>
                    <fullName>Proforma</fullName>
                    <default>false</default>
                    <label>Proforma</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Invoice_creation__c</fullName>
        <description>Invoice creation date</description>
        <externalId>false</externalId>
        <label>Invoice creation</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Is_Disputed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Disputed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Is_Proforma_Invoice__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Default value is set to Unchecked</description>
        <externalId>false</externalId>
        <label>Is Proforma Invoice</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Is_Reversal__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Added this field as per L4 #45304</description>
        <externalId>false</externalId>
        <label>Is Reversal</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_NAV_Invoice_and_Company__c</fullName>
        <description>This is being used in invoice  sync to uniquely find invoice for a company</description>
        <externalId>false</externalId>
        <label>NAV Invoice and Company</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_NavSeqId__c</fullName>
        <description>Sequence Id</description>
        <externalId>false</externalId>
        <label>SeqId</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>Order</referenceTo>
        <relationshipName>Invoices</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Outstanding_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Outstanding Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Overdue__c</fullName>
        <description>Set field as true If “Invoice Due Date” is in the past &amp; Remaining Balance  &gt; 0 
This field is for Dev purpose only and should not be visible to Customer</description>
        <externalId>false</externalId>
        <formula>(EP_Invoice_Due_Date__c &lt; TODAY()  &amp;&amp; EP_Remaining_Balance__c &gt; 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Overdue?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Paid_Amount__c</fullName>
        <externalId>false</externalId>
        <formula>EP_Invoice_Total__c -  EP_Remaining_Balance__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Paid Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Payment_Term__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Payment Terms</label>
        <referenceTo>EP_Payment_Term__c</referenceTo>
        <relationshipLabel>Customer Invoices</relationshipLabel>
        <relationshipName>Customer_Invoices</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Payment_Terms__c</fullName>
        <description>Field to store Payment Terms</description>
        <externalId>false</externalId>
        <label>Payment Terms</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Payment_Type__c</fullName>
        <externalId>false</externalId>
        <label>Payment Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Puma_Company_Code__c</fullName>
        <externalId>false</externalId>
        <formula>EP_Bill_To__r.EP_Puma_Company_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Puma Company Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Remaining_Balance__c</fullName>
        <externalId>false</externalId>
        <label>Remaining Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>EP_Sell_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Filter by Sell-To Account record type</description>
        <externalId>false</externalId>
        <label>Sell-To</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR 2</booleanFilter>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_Bill_To</value>
            </filterItems>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_Sell_To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Customer Invoices (Sell-To)</relationshipLabel>
        <relationshipName>Invoices2</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Status__c</fullName>
        <description>The field will return:
Issued – If Amount Paid = 0

Paid – If Remaining Balance = 0</description>
        <externalId>false</externalId>
        <formula>IF((EP_Credit_Memo__c != NULL &amp;&amp; ISPICKVAL(EP_Credit_Memo__r.EP_Credit_Memo_Type__c, &quot;Items &amp; G/L&quot;)), &quot;Credited&quot;,	 
IF(
AND(RecordType.DeveloperName = &apos;EP_Pro_Forma_Invoice&apos;,EP_Paid_Amount__c = 0) ,&quot;Issued&quot;, 
IF(EP_Remaining_Balance__c &lt;= 0 , &quot;Paid&quot;, &quot;Issued&quot;) 
)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Customer Invoice</label>
    <listViews>
        <fullName>EP_All_Credited_Invoices</fullName>
        <columns>NAME</columns>
        <columns>EP_Invoice_Type__c</columns>
        <columns>EP_Invoice_Issue_Date__c</columns>
        <columns>EP_Invoice_Due_Date__c</columns>
        <columns>EP_Invoice_Total__c</columns>
        <columns>EP_Sell_To__c</columns>
        <columns>EP_Remaining_Balance__c</columns>
        <columns>EP_Status__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>EP_Status__c</field>
            <operation>equals</operation>
            <value>Credited</value>
        </filters>
        <label>3. All Credited Invoices</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Open_Invoices</fullName>
        <columns>NAME</columns>
        <columns>EP_Invoice_Type__c</columns>
        <columns>EP_Invoice_Issue_Date__c</columns>
        <columns>EP_Invoice_Due_Date__c</columns>
        <columns>EP_Invoice_Total__c</columns>
        <columns>EP_Sell_To__c</columns>
        <columns>EP_Remaining_Balance__c</columns>
        <columns>EP_Status__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>EP_Status__c</field>
            <operation>equals</operation>
            <value>Issued</value>
        </filters>
        <label>1. All Open Invoices</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Paid_Closed_Invoices</fullName>
        <columns>NAME</columns>
        <columns>EP_Invoice_Type__c</columns>
        <columns>EP_Invoice_Issue_Date__c</columns>
        <columns>EP_Invoice_Due_Date__c</columns>
        <columns>EP_Invoice_Total__c</columns>
        <columns>EP_Remaining_Balance__c</columns>
        <columns>EP_Sell_To__c</columns>
        <columns>EP_Status__c</columns>
        <columns>CURRENCY_ISO_CODE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>EP_Status__c</field>
            <operation>equals</operation>
            <value>Paid</value>
        </filters>
        <label>2. All Paid/Closed Invoices</label>
    </listViews>
    <nameField>
        <label>Invoice Number</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Customer Invoices</pluralLabel>
    <recordTypes>
        <fullName>EP_Pro_Forma_Invoice</fullName>
        <active>true</active>
        <description>To capture Pro-Forma Invoice record</description>
        <label>Pro-Forma Invoice</label>
        <picklistValues>
            <picklist>EP_Invoice_Pull_Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incomplete</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>EP_Invoice_Type__c</picklist>
            <values>
                <fullName>Final Invoice</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Proforma</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>EP_Sales_Invoice</fullName>
        <active>true</active>
        <description>To capture sales Invoice</description>
        <label>Sales Invoice</label>
        <picklistValues>
            <picklist>EP_Invoice_Pull_Status__c</picklist>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Incomplete</fullName>
                <default>true</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>EP_Invoice_Type__c</picklist>
            <values>
                <fullName>Final Invoice</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Proforma</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <searchResultsAdditionalFields>EP_Invoice_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Invoice_Issue_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Invoice_Due_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Invoice_Total__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Sell_To__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Remaining_Balance__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Status__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CURRENCY_ISO_CODE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
