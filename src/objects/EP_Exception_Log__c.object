<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>To log the Exception details.  Logging Service and Unhandled Exception Email handler components will use this Object.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Application_Name__c</fullName>
        <description>The name of the application which has caught the exception.</description>
        <externalId>false</externalId>
        <label>Application Name</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Browser__c</fullName>
        <description>PE-195</description>
        <externalId>false</externalId>
        <label>Browser</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Class_Name__c</fullName>
        <description>The name of the apex consumer class which has caught the exception.  E.g.  Class name, controller name and trigger.</description>
        <externalId>false</externalId>
        <label>Class Name</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Component__c</fullName>
        <description>PE-195</description>
        <externalId>false</externalId>
        <label>EP Component</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Device__c</fullName>
        <description>PE-195</description>
        <externalId>false</externalId>
        <label>Device</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Exception_Code__c</fullName>
        <description>Error Code or Salesforce.com standard exception code where applicable.</description>
        <externalId>false</externalId>
        <label>Exception Code</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Exception_Description__c</fullName>
        <description>A description of the exception</description>
        <externalId>false</externalId>
        <label>Exception Description</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Exception_Details__c</fullName>
        <description>Details of the exception (stack trace, etc.)</description>
        <externalId>false</externalId>
        <label>Exception Details</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>EP_Exception_Log_Timestamp__c</fullName>
        <description>Date&amp; Time for exception insert action.</description>
        <externalId>false</externalId>
        <label>Exception Log Timestamp</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>EP_Exception_Type__c</fullName>
        <externalId>false</externalId>
        <label>Exception Type</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Handled__c</fullName>
        <defaultValue>false</defaultValue>
        <description>True or False to identify the exceptions between handled and unhandled.</description>
        <externalId>false</externalId>
        <label>Handled</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Is_Exported__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This flag is used to determine which exceptions have been exported to the Middleware via the REST API. This field has been decommissioned since version 4 of the monitoring interface. Once the previous versions of the interface are removed, this field will be deleted from the solution</description>
        <externalId>false</externalId>
        <label>Is Exported</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Method_Name__c</fullName>
        <description>The name of the consumer method where the exception is caught and is using the logging service</description>
        <externalId>false</externalId>
        <label>Method Name</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_OperatingSystem__c</fullName>
        <description>PE-195</description>
        <externalId>false</externalId>
        <label>OperatingSystem</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_OrgID__c</fullName>
        <description>15 char organization Id per environment such as the sandbox.</description>
        <externalId>false</externalId>
        <label>Org ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_PerformedAction__c</fullName>
        <description>PE-195</description>
        <externalId>false</externalId>
        <label>PerformedAction</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Running_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>User performing invoked the services.</description>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Exception_Logs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Severity__c</fullName>
        <description>Possible examples: ERROR, INFO, DEBUG, FATAL, WARNING</description>
        <externalId>false</externalId>
        <label>Severity</label>
        <length>15</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_User_Locale__c</fullName>
        <description>This field is used to track the current locale of the running user</description>
        <externalId>false</externalId>
        <label>User Locale</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_User_Profile_ID__c</fullName>
        <description>This field is used to capture the profile ID of the running user</description>
        <externalId>false</externalId>
        <label>User Profile ID</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_User_Role_ID__c</fullName>
        <description>This field is used to capture the current role ID of the logged in user</description>
        <externalId>false</externalId>
        <label>User Role ID</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_User_Session_ID__c</fullName>
        <description>This field is used to track the session ID of the running user</description>
        <externalId>false</externalId>
        <label>User Session ID</label>
        <length>500</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>EP_WS_Audit_User__c</fullName>
        <description>Running user which threw the exception (from the web service itself)</description>
        <externalId>false</externalId>
        <label>EP_WS_Audit _User</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_WS_Calling_Application__c</fullName>
        <description>web service application which threw the exception</description>
        <externalId>false</externalId>
        <label>WS Calling Application</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_WS_Calling_Area__c</fullName>
        <description>web service calling area which threw the exception</description>
        <externalId>false</externalId>
        <label>EP_WS_Calling_Area</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_WS_Transaction_ID__c</fullName>
        <description>Transaction ID, available on SOAP response header.</description>
        <externalId>false</externalId>
        <label>EP_WS_Transaction_ID</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_WasHandled__c</fullName>
        <defaultValue>false</defaultValue>
        <description>PE-195</description>
        <externalId>false</externalId>
        <label>WasHandled</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Exception Log</label>
    <listViews>
        <fullName>All</fullName>
        <columns>UPDATEDBY_USER</columns>
        <columns>EP_Method_Name__c</columns>
        <columns>EP_Running_User__c</columns>
        <columns>EP_Class_Name__c</columns>
        <columns>EP_Exception_Details__c</columns>
        <columns>EP_Exception_Type__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>LAST_UPDATE</columns>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Exception_Logs</fullName>
        <columns>NAME</columns>
        <columns>EP_Application_Name__c</columns>
        <columns>EP_Class_Name__c</columns>
        <columns>EP_Exception_Code__c</columns>
        <columns>EP_Exception_Description__c</columns>
        <columns>EP_Exception_Type__c</columns>
        <columns>EP_Method_Name__c</columns>
        <columns>EP_OrgID__c</columns>
        <columns>EP_Severity__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>EP_Running_User__c</columns>
        <columns>EP_WS_Calling_Application__c</columns>
        <columns>EP_Exception_Log_Timestamp__c</columns>
        <filterScope>Everything</filterScope>
        <label>1. All Exception Logs</label>
    </listViews>
    <listViews>
        <fullName>EP_All_Integration_Exception_Logs</fullName>
        <columns>NAME</columns>
        <columns>EP_Application_Name__c</columns>
        <columns>EP_Class_Name__c</columns>
        <columns>EP_Exception_Code__c</columns>
        <columns>EP_Exception_Description__c</columns>
        <columns>EP_Exception_Type__c</columns>
        <columns>EP_Method_Name__c</columns>
        <columns>EP_OrgID__c</columns>
        <columns>EP_Severity__c</columns>
        <columns>CREATED_DATE</columns>
        <columns>EP_Running_User__c</columns>
        <columns>EP_WS_Calling_Application__c</columns>
        <columns>EP_Exception_Log_Timestamp__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>EP_WS_Transaction_ID__c</field>
            <operation>notEqual</operation>
        </filters>
        <label>2. All Integration Exception Logs</label>
    </listViews>
    <nameField>
        <displayFormat>EL-{00000000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Exception Logs</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
