<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is used to create transfer route from NAV through an automated interface</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Delivery_Type__c</fullName>
        <description>A Transfer Route can be set as “Delivery” or “Ex-Rack”.</description>
        <externalId>false</externalId>
        <label>Delivery Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Delivery</fullName>
                    <default>false</default>
                    <label>Delivery</label>
                </value>
                <value>
                    <fullName>Ex-Rack</fullName>
                    <default>false</default>
                    <label>Ex-Rack</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>EP_Destination__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This is a filtered lookup. Only storage location accounts can be selected.</description>
        <externalId>false</externalId>
        <label>Destination</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Please select a Storage Location</errorMessage>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Storage Location</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Transfer Routes (Account)</relationshipLabel>
        <relationshipName>Transfer_Routes_On_Destination</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Haulier__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This is a filtered lookup. Only haulier accounts can be select.</description>
        <externalId>false</externalId>
        <label>Haulier</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Please select a Haulier</errorMessage>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Haulier</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Transfer Routes (Haulier)</relationshipLabel>
        <relationshipName>Transfer_Routes_On_Haulier</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Is_Active__c</fullName>
        <defaultValue>true</defaultValue>
        <description>This field is used to check whether Transfer Route is Active or Inactive</description>
        <externalId>false</externalId>
        <label>Is Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_NAV_Destination_Code__c</fullName>
        <description>This field is used to map NAV Destination Code to Salesforce Storage Account</description>
        <externalId>false</externalId>
        <label>NAV Destination Code</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_NAV_Haulier_Code__c</fullName>
        <description>This field is used to map Shipping Agent Code (Haulier Code) to Salesforce Account</description>
        <externalId>false</externalId>
        <label>NAV Haulier Code</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_NAV_In_Transit_Code__c</fullName>
        <description>This field is created as per the thread 30a.</description>
        <externalId>false</externalId>
        <label>NAV In Transit Code</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_NAV_Seq_Id__c</fullName>
        <description>This field captures sequence id from NAV</description>
        <externalId>false</externalId>
        <label>NAV Seq Id</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_NAV_Source_Code__c</fullName>
        <description>This field is used to map NAV Source Code to Salesforce Account</description>
        <externalId>false</externalId>
        <label>NAV Source Code</label>
        <length>40</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Source__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This is a filtered lookup. Only storage location accounts can be selected.</description>
        <externalId>false</externalId>
        <label>Source</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Please select a Storage Location</errorMessage>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Storage Location</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Transfer Routes</relationshipLabel>
        <relationshipName>Transfer_Routes</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Transfer_Route_NAV_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>This field is the unique field and used for integration purpose.</description>
        <externalId>true</externalId>
        <label>Transfer Route NAV ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Transportation_Management__c</fullName>
        <description>This field is dependent on Delivery Type.
Values for “Ex-Rack” = &quot;None&quot;
Values for “Delivery” = “Own, 3rd Party”</description>
        <externalId>false</externalId>
        <label>Transportation Management</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>EP_Delivery_Type__c</controllingField>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Own</fullName>
                    <default>false</default>
                    <label>Own</label>
                </value>
                <value>
                    <fullName>3rd Party</fullName>
                    <default>false</default>
                    <label>3rd Party</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Delivery</controllingFieldValue>
                <valueName>Own</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Delivery</controllingFieldValue>
                <valueName>3rd Party</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <label>Transfer Route</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>TR-{00000}</displayFormat>
        <label>Transfer Route Id</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Transfer Routes</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
