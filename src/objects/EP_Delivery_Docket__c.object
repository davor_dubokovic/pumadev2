<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ePuma - This object is used to store the delivery dockets produced as part of a delivery orders. Dockets can be created automatically (for OilDroid enabled markets) or manually by a CSC (for non OilDroid enabled markets)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Delivery_Doc_Date__c</fullName>
        <description>Date</description>
        <externalId>false</externalId>
        <label>Delivery Doc Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EP_Delivery_Doc_Number__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Unique (value pushed from NAV
Text (50 characters)</description>
        <externalId>true</externalId>
        <label>Delivery Doc Number</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Order__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Order</label>
        <referenceTo>Order</referenceTo>
        <relationshipLabel>Delivery Dockets</relationshipLabel>
        <relationshipName>Customer_Delivery_Dockets</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Ship_To__c</fullName>
        <description>Master-Detail (Account)
Filtered – (Only Ship-To)</description>
        <externalId>false</externalId>
        <label>Ship-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_VMI_Ship_To,EP_Storage_Ship_To,EP_Non_VMI_Ship_To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipName>Customer_Delivery_Dockets</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Trip_Id__c</fullName>
        <description>Text(50 characters)</description>
        <externalId>false</externalId>
        <label>Trip Id</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Delivery Docket</label>
    <nameField>
        <displayFormat>DD-{000000}</displayFormat>
        <label>Delivery Doc ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Delivery Dockets</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
