<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Route Allocation</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Delivery_Window_End_Date__c</fullName>
        <externalId>false</externalId>
        <label>Delivery Window End</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Default_Route__c</fullName>
        <externalId>false</externalId>
        <formula>EP_Route__c  ==  EP_Ship_To__r.EP_Default_Route__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Default Route</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Delivery_Window_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Delivery Window Start</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Route__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Route</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>You cannot choose an Inactive Route. Please activate the Route before proceeding.</errorMessage>
            <filterItems>
                <field>EP_Route__c.EP_Status__c</field>
                <operation>equals</operation>
                <value>Active</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>EP_Route__c</referenceTo>
        <relationshipLabel>Route Allocations</relationshipLabel>
        <relationshipName>Route_Allocations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Ship_To__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Ship-To</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_VMI_Ship_To,EP_Non_VMI_Ship_To,EP_Storage_Ship_To</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Route Allocations</relationshipLabel>
        <relationshipName>Route_Allocations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Route Allocation</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>EP_Route__c</columns>
        <columns>EP_Ship_To__c</columns>
        <columns>EP_Delivery_Window_Start_Date__c</columns>
        <columns>Delivery_Window_End_Date__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Route Allocations</label>
    </listViews>
    <nameField>
        <displayFormat>RA-{00000000}</displayFormat>
        <label>Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Route Allocations</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>EP_Route__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EP_Ship_To__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EP_Default_Route__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Delivery_Window_End_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EP_Delivery_Window_Start_Date__c</lookupDialogsAdditionalFields>
        <searchFilterFields>EP_Ship_To__c</searchFilterFields>
        <searchFilterFields>EP_Route__c</searchFilterFields>
        <searchResultsAdditionalFields>EP_Default_Route__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Route__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Ship_To__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Delivery_Window_End_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>EP_Delivery_Window_Start_Date__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>EP_Delivery_Window_Start_End_Validation</fullName>
        <active>true</active>
        <description>Delivery Window Start and End value should be positive</description>
        <errorConditionFormula>OR(EP_Delivery_Window_Start_Date__c &lt;=0,Delivery_Window_End_Date__c &lt;= 0)</errorConditionFormula>
        <errorMessage>Delivery  Window Start Date and End Date should be positive values.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_Route_And_ShipTo_Company_Sam</fullName>
        <active>true</active>
        <description>This validation rule is used to make sure selected route&apos;s and ship to&apos;s company is same.</description>
        <errorConditionFormula>EP_Route__r.EP_Company__r.EP_Company_Code__c &lt;&gt; EP_Ship_To__r.EP_Puma_Company__r.EP_Company_Code__c</errorConditionFormula>
        <errorDisplayField>EP_Route__c</errorDisplayField>
        <errorMessage>Invalid Route allocation. You cannot allocate route associated with different company.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_ShipTo_Company_Route_Enable</fullName>
        <active>true</active>
        <description>This is used to validate whether ship to company has route enabled</description>
        <errorConditionFormula>NOT( EP_Ship_To__r.EP_Puma_Company__r.EP_Route_Enabled__c)</errorConditionFormula>
        <errorDisplayField>EP_Ship_To__c</errorDisplayField>
        <errorMessage>You cannot allocate Route as Route functionality is not enabled for the associated company of this Ship-To.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
