<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>ePuma - This object is used to the line items of a Contract record.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Contract_Product_Quantity__c</fullName>
        <defaultValue>0</defaultValue>
        <description>This field is used to store the max quantity that a customer can order through this contract</description>
        <externalId>false</externalId>
        <label>Contract Product Quantity</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EP_Contract__c</fullName>
        <description>This field is used to link the line item back to a contract</description>
        <externalId>false</externalId>
        <label>Contract</label>
        <referenceTo>Contract</referenceTo>
        <relationshipLabel>Contract Items</relationshipLabel>
        <relationshipName>EP_Contract_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field is used to link contracts back to available products</description>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Contract Items</relationshipLabel>
        <relationshipName>EP_Contract_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EP_Unique_ContractLineId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Uniquely identifies Contract Line( Contract Number-Contract Line Id-Company Code)</description>
        <externalId>true</externalId>
        <label>Unique_ContractLineId</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Unit_of_Measure__c</fullName>
        <description>This field is used to store the UoM of the blanket contract item</description>
        <externalId>false</externalId>
        <formula>TEXT(EP_Product__r.EP_Unit_of_Measure__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Unit of Measure</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OP_Contract_Line_Id__c</fullName>
        <description>NAV Contract Line Id</description>
        <externalId>false</externalId>
        <label>NAV Contract Line Id</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Contract Item</label>
    <nameField>
        <displayFormat>CI-{00000}</displayFormat>
        <label>Contract Item ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Contract Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
