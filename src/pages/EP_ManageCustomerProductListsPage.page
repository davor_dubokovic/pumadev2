<apex:page title="{!$Label.EP_Manage_Customer_Product_List_Page_Title}" standardController="Account" 
                    recordSetVar="a" extensions="EP_ManageProductListViewController" action="{!retrieveAccountProductListInformation}">
    
    <apex:stylesheet value="{!URLFOR($Resource.EP_IntegrationSidebarLib, 'style.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.EP_StyleLib, 'css/overlay.css')}" />
    <apex:stylesheet value="{!URLFOR($Resource.EP_jQuery, 'jquery-ui.css')}" />
    
    <apex:includeScript value="{!URLFOR($Resource.EP_jQuery, 'jquery.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.EP_jQuery, 'jquery-ui.min.js')}"/>
    
    <c:EP_PerformanceMonitor />
    
    <script>
        
        if(typeof jQuery != 'undefined'){
            $j = jQuery.noConflict();
        }
        
        $j(document).ready(function(){
            initialiseAutocomplete();
        });
        
        function initialiseAutocomplete(){
            $j(function() {            
                $j("input.productListField").autocomplete({
                    source: function(request, response){
                    getProductListData(response, request.term);
                    },
                    select: function(event, ui){
                    $j("input.productListField").val(ui.item.value);
                    $j('input.productListIdField').val(ui.item.id);               
                    return false;
                    }            
                });            
            });
        }
        
        function getProductListData(response, param){
            Visualforce.remoting.Manager.invokeAction(
                '{!$RemoteAction.EP_ManageProductListViewController.getProductLists}',
                param, 
                function(result, event){
                    if (event.status) {                        
                        var objList = [];
                        for(var i = 0; i < result.length; i++){
                            var obj = new Object();
                            obj.label = result[i].Name;
                            obj.value = result[i].Name;
                            obj.id = result[i].Id;
                            objList.push(obj);
                        }
                        response(objList);
                    }else {
                        alert(event.message);
                    }
                }, 
                {escape: true}
            );
        }
    </script>
    
    <apex:sectionHeader title="{!$Label.EP_Manage_Customer_Product_List_Page_Title}" subtitle="{!$ObjectType.Account.fields.EP_PriceBook__c.Label}"/>
    
    <apex:form id="main-form">
    
        <apex:actionStatus id="statusOverlay" startStyleClass="overlay"></apex:actionStatus> 
        
        <apex:outputPanel rendered="{!(lSelectedAccounts.size == 0)}">
            {!$Label.EP_No_Account_Selected_Product_List_Page_Error}: (<a href="/001/o">{!$ObjectType.Account.Label}</a>)
        </apex:outputPanel>
        
        <!-- Functions -->
        <apex:actionFunction name="rerenderFilterSection" reRender="filter-page-block" status="statusOverlay" oncomplete="initialiseAutocomplete();" />
        
        <apex:pageMessages id="message-panel" />
        
        <apex:pageBlock title="{!$Label.EP_Product_List_Filtered_Products_Label}" rendered="{!(lSelectedAccounts.size > 0)}" id="filter-page-block">
            <!-- Filters Section -->
            <apex:pageBlockSection showHeader="true" collapsible="false" title="{!$Label.EP_Product_List_Filters_Caption}" columns="3" id="filter-page-section">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.EP_Product_List_Select_Search_Method_Label}" />
                    <apex:selectRadio value="{!filterMethod}" onchange="rerenderFilterSection(); return false;">
                        <apex:selectOptions value="{!getavailableFilterOptions}"/>
                    </apex:selectRadio>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >&nbsp;</apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >&nbsp;</apex:pageBlockSectionItem>
                
                <!-- Product List Filter Controls -->
                <apex:pageBlockSectionItem rendered="{!NOT(blnSearchByProduct)}">
                    <apex:outputLabel value="{!$ObjectType.Pricebook2.Label}" />
                    
                    <apex:outputPanel >
                        <div class="filterOverview">
                            <div class="bFilterView">
                                <table border="0">                    
                                    <tr>        
                                        <td>
                                            <div class="ui-widget">
                                            <apex:inputText id="product-list-text-field" styleClass="productListField" value="{!strSelectedProductListName}" /> 
                                            <apex:inputText id="product-list-id-text-field" value="{!strSelectedProductListID}" styleClass="productListIdField" style="visibility:hidden" />                                
                                        </div>
                                        </td>                
                                    </tr>                
                                                                
                                </table>
                            </div>
                        </div>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <!-- Product Filter Controls -->
                <apex:pageBlockSectionItem rendered="{!blnSearchByProduct}">
                    <apex:outputLabel value="{!$ObjectType.Product2.fields.ProductCode.Label}" />
                    <apex:inputField value="{!filterProduct.ProductCode}" required="false"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!blnSearchByProduct}">
                    <apex:outputLabel value="{!$ObjectType.Product2.fields.Family.Label}" />
                    <apex:inputField value="{!filterProduct.Family}" required="false"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!blnSearchByProduct}">
                    <apex:outputLabel value="{!$ObjectType.Product2.fields.Name.Label}" />
                    <apex:inputField value="{!filterProduct.Name}" required="false"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!blnSearchByProduct}">
                    <apex:outputLabel value="{!$ObjectType.Product2.fields.EP_Product_Sold_As__c.Label}" />
                    <apex:inputField value="{!filterProduct.EP_Product_Sold_As__c}" required="false"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection showHeader="false" collapsible="false" columns="1" >
                <apex:commandButton value="{!$Label.EP_Statement_Search}" action="{!searchProducts}" status="statusOverlay" style="min-width:80px;" reRender="filtered-products-section,filtered-products-button-panel" oncomplete="initialiseAutocomplete();" />
            </apex:pageBlockSection>
            
            <!-- Products Section -->
            <apex:outputPanel id="filtered-products-section">
                <apex:pageBlockSection showHeader="true" collapsible="false" columns="1" title="{!$ObjectType.Product2.labelPlural}">
                    <apex:pageblocktable value="{!lSelectedProducts}" var="p" rendered="{!(lSelectedProducts != NULL && lSelectedProducts.size > 0)}">
                        <apex:column >
                            <apex:facet name="header"></apex:facet>
                            <apex:inputField value="{!p.Eligible_for_Rebate__c}"/>
                        </apex:column>
                        <apex:column value="{!p.Name}"/>
                        <apex:column value="{!p.ProductCode}"/>
                        <apex:column value="{!p.Family}"/>
                        <apex:column value="{!p.EP_Company__c}"/>
                        <apex:column value="{!p.EP_Unit_of_Measure__c}"/>
                    </apex:pageblocktable>
                    <apex:outputPanel rendered="{!(lSelectedProducts != NULL && lSelectedProducts.size == 0)}">{!$Label.EP_Product_List_No_Products_Found_Error}</apex:outputPanel>
                </apex:pageBlockSection>
            </apex:outputPanel>
            
            <!-- Products Control Section -->
            <apex:pageBlockSection showHeader="false" collapsible="false" columns="1" id="filtered-products-button-panel">
                <apex:outputPanel rendered="{!(lSelectedProducts != NULL && lSelectedProducts.size > 0)}">
                    <apex:commandButton value="{!$Label.EP_Select_All_Caption}" action="{!selectAllFilteredProducts}" status="statusOverlay" style="min-width:80px;" reRender="filtered-products-section" oncomplete="initialiseAutocomplete();" />
                    &nbsp;
                    <apex:commandButton value="{!$Label.EP_Deselect_All_Caption}" action="{!deselectAllFilteredProducts}" status="statusOverlay" style="min-width:80px;" reRender="filtered-products-section" oncomplete="initialiseAutocomplete();" />
                    &nbsp;
                    <apex:commandButton value="{!$Label.EP_Product_List_Add_Selected_Product_Caption}" action="{!addSelectedProductsToList}" status="statusOverlay" style="min-width:80px;" reRender="account-output-panel" oncomplete="initialiseAutocomplete();" />
                    &nbsp;
                    <apex:commandButton value="{!$Label.EP_Product_List_Remove_Selected_Products_Caption}" action="{!removeSelectedProductsToList}" status="statusOverlay" style="min-width:80px;" reRender="account-output-panel" oncomplete="initialiseAutocomplete();" />
                </apex:outputPanel>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <!-- Accounts Section -->
        <apex:pageBlock title="{!$ObjectType.Account.labelPlural}" rendered="{!(lSelectedAccounts.size > 0)}">
            <apex:pageblockButtons location="bottom">
                <apex:commandButton value="{!$Label.EP_Save}" action="{!updateAccountProductLists}" style="min-width:80px;" reRender="message-panel" />
                <apex:commandButton value="{!$Label.EP_Cancel}" style="min-width:80px;" />
            </apex:pageblockButtons>
            <apex:outputPanel id="account-output-panel">
                <apex:pageblocktable id="account-output-table" value="{!lAccountProductRecords}" var="item">
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.Account.Label}</apex:facet>
                        <a href="/{!item.accountItem.Id}" target="_blank">{!item.accountItem.Name}</a>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.Account.fields.EP_PriceBook__c.Label}</apex:facet>
                        <apex:outputPanel rendered="{!(item.strProductListName != NULL)}">
                            <a href="{!item.strProductListURL}" target="_blank">{!item.strProductListName}</a>
                        </apex:outputPanel>
                        <apex:inputField value="{!item.productListItem.Name}" rendered="{!(item.strProductListName == NULL && item.accountItem.EP_Puma_Company_Code__c != NULL)}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$ObjectType.Account.fields.EP_Puma_Company__c.Label}</apex:facet>
                        <apex:outputField value="{!item.accountItem.EP_Puma_Company_Code__c}" rendered="{!(item.accountItem.EP_Puma_Company_Code__c != NULL)}" />
                        <apex:outputText value="{!$Label.EP_Product_List_Assign_Company_Code_Warning}" rendered="{!(item.accountItem.EP_Puma_Company_Code__c == NULL)}" style="font-size: 80%; color: red;" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$Label.EP_Product_List_Total_Number_Of_Products}</apex:facet>
                        <apex:outputText rendered="{!(item.strProductListName != NULL)}">{!item.intNumberOfExistingProducts}</apex:outputText>
                        <apex:outputText rendered="{!(item.strProductListName == NULL)}" value="{!$Label.EP_Product_List_No_Product_List_Warning}" style="font-size: 80%; color: red;" />
                    </apex:column>
                    <apex:column style="color:green;">
                        <apex:facet name="header">{!$Label.EP_Product_List_New_Products_Added_Caption}</apex:facet>
                        <apex:outputText >{!item.intNumberOfProductsAdded}</apex:outputText>
                    </apex:column>
                    <apex:column style="color:red;">
                        <apex:facet name="header">{!$Label.EP_Product_List_Products_Removed_Caption}</apex:facet>
                        <apex:outputText >{!item.intNumberOfProductsRemoved}</apex:outputText>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">{!$Label.EP_Product_List_Number_Of_Products_After_Changes_Caption}</apex:facet>
                        <apex:outputText >{!item.intUpdatedNumberOfProducts}</apex:outputText>
                    </apex:column>
                </apex:pageblocktable>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
    
</apex:page>